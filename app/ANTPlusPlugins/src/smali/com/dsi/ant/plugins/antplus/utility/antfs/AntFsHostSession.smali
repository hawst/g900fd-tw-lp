.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "AntFsHostSession.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field closeTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;

.field private commandPipeSequenceNumber:I

.field private currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

.field private doWorkTaskQueue:Ljava/util/concurrent/Exchanger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Exchanger",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;",
            ">;"
        }
    .end annotation
.end field

.field private doWorkTaskResult:Ljava/util/concurrent/Exchanger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/Exchanger",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;",
            ">;"
        }
    .end annotation
.end field

.field private downloadedDataBuffer:[B

.field private elapsedTime:I

.field private mAntDeviceNumberOfClient:I

.field private mBeaconInterval:I

.field private mClientStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field private mLinkPeriod:I

.field private mLinkRf:I

.field private mRequestMutex:Ljava/util/concurrent/Semaphore;

.field private mSerialNumberOfHost:J

.field private mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

.field private utcRequestStartedTime:Ljava/util/GregorianCalendar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;JI)V
    .locals 3
    .param p1, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "serialNumberForHost"    # J
    .param p4, "antDeviceNumberToConnectTo"    # I

    .prologue
    const/4 v2, 0x0

    .line 153
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 121
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$1;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .line 133
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskQueue:Ljava/util/concurrent/Exchanger;

    .line 134
    new-instance v0, Ljava/util/concurrent/Exchanger;

    invoke-direct {v0}, Ljava/util/concurrent/Exchanger;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskResult:Ljava/util/concurrent/Exchanger;

    .line 137
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mRequestMutex:Ljava/util/concurrent/Semaphore;

    .line 138
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .line 146
    const/16 v0, 0x39

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkRf:I

    .line 147
    const/16 v0, 0x1000

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkPeriod:I

    .line 148
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mBeaconInterval:I

    .line 150
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->commandPipeSequenceNumber:I

    .line 333
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$7;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->closeTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;

    .line 154
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mClientStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    .line 155
    iput-wide p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mSerialNumberOfHost:J

    .line 156
    iput p4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mAntDeviceNumberOfClient:I

    .line 157
    return-void
.end method

.method static synthetic access$002(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 25
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    return-object p1
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mClientStatusReceiver:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    return-object v0
.end method

.method static synthetic access$1008(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->commandPipeSequenceNumber:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->commandPipeSequenceNumber:I

    return v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)J
    .locals 2
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mSerialNumberOfHost:J

    return-wide v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkRf:I

    return v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkPeriod:I

    return v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mBeaconInterval:I

    return v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$800(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)I
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    .prologue
    .line 25
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mAntDeviceNumberOfClient:I

    return v0
.end method

.method static synthetic access$902(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;[B)[B
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;
    .param p1, "x1"    # [B

    .prologue
    .line 25
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->downloadedDataBuffer:[B

    return-object p1
.end method

.method private performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 6
    .param p1, "task"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 407
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mRequestMutex:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v2

    if-nez v2, :cond_0

    .line 408
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_REQUEST_IN_PROGRESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 424
    :goto_0
    return-object v1

    .line 414
    :cond_0
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskQueue:Ljava/util/concurrent/Exchanger;

    const-wide/16 v3, 0x1

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v2, p1, v3, v4, v5}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 419
    :try_start_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskResult:Ljava/util/concurrent/Exchanger;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 424
    .local v1, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mRequestMutex:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 415
    .end local v1    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :catch_0
    move-exception v0

    .line 417
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_REQUEST_IN_PROGRESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 424
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mRequestMutex:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v2}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mRequestMutex:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    throw v2
.end method

.method private performTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 1
    .param p1, "task"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 395
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$9;

    invoke-direct {v0, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$9;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)V

    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    return-object v0
.end method

.method private runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 3
    .param p1, "task"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 382
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    invoke-virtual {p1, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 384
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t start "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->getTaskName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " in state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 385
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_WRONG_ANTFS_STATE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 390
    :goto_0
    return-object v0

    .line 388
    :cond_0
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 390
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;->getTaskResult()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    goto :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 449
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->enableMessageProcessing()V

    .line 456
    :cond_0
    :goto_0
    :try_start_0
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v7

    invoke-virtual {v7}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v5

    .line 462
    .local v5, "state":Lcom/dsi/ant/message/ChannelState;
    :goto_1
    sget-object v7, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v5, v7, :cond_2

    sget-object v7, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    if-eq v5, v7, :cond_2

    .line 465
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v7, v8, v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 533
    :cond_1
    :goto_2
    return-void

    .line 457
    .end local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    :catch_0
    move-exception v1

    .line 459
    .local v1, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACFE requesting channel status: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 460
    sget-object v5, Lcom/dsi/ant/message/ChannelState;->INVALID:Lcom/dsi/ant/message/ChannelState;

    .restart local v5    # "state":Lcom/dsi/ant/message/ChannelState;
    goto :goto_1

    .line 469
    .end local v1    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    const/4 v4, 0x0

    .line 472
    .local v4, "runnable":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    :try_start_1
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskQueue:Ljava/util/concurrent/Exchanger;

    const/4 v8, 0x0

    const-wide/16 v9, 0xa

    sget-object v11, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v7, v8, v9, v10, v11}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v7

    move-object v0, v7

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;

    move-object v4, v0
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    .line 482
    if-eqz v4, :cond_1

    .line 485
    const/4 v6, 0x0

    .line 490
    .local v6, "subTaskException":Landroid/os/RemoteException;
    :try_start_2
    new-instance v7, Ljava/util/GregorianCalendar;

    const-string v8, "UTC"

    invoke-static {v8}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v8

    invoke-direct {v7, v8}, Ljava/util/GregorianCalendar;-><init>(Ljava/util/TimeZone;)V

    iput-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->utcRequestStartedTime:Ljava/util/GregorianCalendar;

    .line 491
    invoke-interface {v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;->run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    .line 492
    .local v3, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    new-instance v7, Ljava/util/Date;

    invoke-direct {v7}, Ljava/util/Date;-><init>()V

    invoke-virtual {v7}, Ljava/util/Date;->getTime()J

    move-result-wide v7

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->utcRequestStartedTime:Ljava/util/GregorianCalendar;

    invoke-virtual {v9}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v9

    invoke-virtual {v9}, Ljava/util/Date;->getTime()J

    move-result-wide v9

    sub-long/2addr v7, v9

    long-to-double v7, v7

    const-wide v9, 0x408f400000000000L    # 1000.0

    div-double/2addr v7, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->ceil(D)D

    move-result-wide v7

    double-to-int v7, v7

    iput v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->elapsedTime:I
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_3

    .line 505
    :goto_3
    :try_start_3
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskResult:Ljava/util/concurrent/Exchanger;

    invoke-virtual {v7, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_3
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_4

    .line 522
    :goto_4
    if-eqz v6, :cond_3

    .line 523
    throw v6

    .line 473
    .end local v3    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .end local v6    # "subTaskException":Landroid/os/RemoteException;
    :catch_1
    move-exception v1

    .line 477
    .local v1, "e":Ljava/lang/InterruptedException;
    goto :goto_2

    .line 478
    .end local v1    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v1

    .line 480
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    goto/16 :goto_0

    .line 494
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    .restart local v6    # "subTaskException":Landroid/os/RemoteException;
    :catch_3
    move-exception v1

    .line 496
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    const-string v8, "RemoteException running ANTFS task"

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 497
    move-object v6, v1

    .line 499
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v9, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;->CONNECTION_LOST:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;

    invoke-interface {v7, v8, v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;->onAntFsStateUpdate(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostEvent;)V

    .line 500
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .restart local v3    # "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    goto :goto_3

    .line 506
    .end local v1    # "e":Landroid/os/RemoteException;
    :catch_4
    move-exception v1

    .line 511
    .local v1, "e":Ljava/lang/InterruptedException;
    :try_start_4
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->doWorkTaskResult:Ljava/util/concurrent/Exchanger;

    invoke-virtual {v7, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_5

    .line 519
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    goto :goto_4

    .line 512
    :catch_5
    move-exception v2

    .line 514
    .local v2, "e1":Ljava/lang/InterruptedException;
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    const-string v8, "Double interrupt exchanging task result, catastrophic."

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 516
    sget-object v3, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 517
    goto/16 :goto_2

    .line 525
    .end local v1    # "e":Ljava/lang/InterruptedException;
    .end local v2    # "e1":Ljava/lang/InterruptedException;
    :cond_3
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    sget-object v8, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-ne v7, v8, :cond_0

    .line 528
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->closeTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;

    invoke-interface {v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;->run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    .line 529
    sget-object v7, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ANT-FS host disconnected, result: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public getCurrentState()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;
    .locals 1

    .prologue
    .line 165
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->currentState:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    return-object v0
.end method

.method public getLastDownloadedData()[B
    .locals 1

    .prologue
    .line 367
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->downloadedDataBuffer:[B

    return-object v0
.end method

.method public getLastRequestDuration()I
    .locals 1

    .prologue
    .line 377
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->elapsedTime:I

    return v0
.end method

.method public getLastRequestStartedTimeUtc()Ljava/util/GregorianCalendar;
    .locals 1

    .prologue
    .line 372
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->utcRequestStartedTime:Ljava/util/GregorianCalendar;

    return-object v0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 538
    const-string v0, "AntFsHostSession"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 0

    .prologue
    .line 546
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 552
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 434
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    if-ne p1, v0, :cond_0

    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v0, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/message/EventCode;->RX_SEARCH_TIMEOUT:Lcom/dsi/ant/message/EventCode;

    if-ne v0, v1, :cond_0

    .line 437
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->closeTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;

    invoke-interface {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;->run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 439
    :cond_0
    return-void
.end method

.method public requestCloseSession()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 348
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$8;

    invoke-direct {v0, p0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$8;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;)V

    .line 358
    .local v0, "doDisconnectTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    return-object v1
.end method

.method public requestConnectToTransport(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 2
    .param p1, "passkeyDatabase"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 200
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;

    invoke-direct {v0, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;)V

    .line 223
    .local v0, "goToTransportTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    return-object v1
.end method

.method public requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
    .locals 3
    .param p1, "index"    # I
    .param p2, "downloadReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 228
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->downloadedDataBuffer:[B

    .line 231
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    invoke-direct {v1, v2, p2, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;I)V

    .line 232
    .local v1, "downloadTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;)V

    .line 245
    .local v0, "doDownloadTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    .line 246
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->getDownloadResult()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v2

    return-object v2
.end method

.method public requestErase(I)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 3
    .param p1, "index"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 298
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    invoke-direct {v1, v2, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;I)V

    .line 299
    .local v1, "eraseTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$5;

    invoke-direct {v0, p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$5;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostEraseChannelSubTask;)V

    .line 310
    .local v0, "doEraseTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v2

    return-object v2
.end method

.method public requestSetTime()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 7
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 287
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v3

    const-wide/16 v5, 0x3e8

    div-long/2addr v3, v5

    const-wide/32 v5, 0x259d4c00

    sub-long v0, v3, v5

    .line 291
    .local v0, "garminTime":J
    new-instance v3, Ljava/math/BigDecimal;

    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v5

    invoke-virtual {v4, v5, v6}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v4, Ljava/math/BigDecimal;

    const-string v5, "900000"

    invoke-direct {v4, v5}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    const/4 v5, 0x0

    sget-object v6, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v3, v4, v5, v6}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    .line 293
    .local v2, "timeZoneOffset":Ljava/math/BigDecimal;
    invoke-virtual {v2}, Ljava/math/BigDecimal;->byteValueExact()B

    move-result v3

    invoke-virtual {p0, v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestSetTime(JB)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v3

    return-object v3
.end method

.method public requestSetTime(JB)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 7
    .param p1, "newTime"    # J
    .param p3, "timeZoneOffset"    # B
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 269
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mStatusRecevierShim:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->commandPipeSequenceNumber:I

    move-wide v3, p1

    move v5, p3

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;IJB)V

    .line 271
    .local v0, "setTimeTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;
    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$4;

    invoke-direct {v6, p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$4;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSetTimeChannelSubTask;)V

    .line 282
    .local v6, "doSetTimeTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    return-object v1
.end method

.method public requestStartAntFsMode(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 2
    .param p1, "customTask"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 315
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;

    invoke-direct {v0, p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$6;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)V

    .line 330
    .local v0, "doCustomTask":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;
    invoke-direct {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->performComplexTaskOnDoWork(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v1

    return-object v1
.end method

.method public setLinkChannelParameters(II)V
    .locals 0
    .param p1, "radioFrequency"    # I
    .param p2, "channelPeriod"    # I

    .prologue
    .line 176
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkRf:I

    .line 177
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mLinkPeriod:I

    .line 178
    return-void
.end method

.method public setLinkChannelParameters(III)V
    .locals 0
    .param p1, "radioFrequency"    # I
    .param p2, "channelPeriod"    # I
    .param p3, "beaconInterval"    # I

    .prologue
    .line 187
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->setLinkChannelParameters(II)V

    .line 188
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->mBeaconInterval:I

    .line 189
    return-void
.end method
