.class public Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
.super Ljava/lang/Object;
.source "MultiDeviceSearch.java"

# interfaces
.implements Landroid/os/Parcelable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultiDeviceSearchResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final IPC_VERSION:I = 0x1


# instance fields
.field protected final mAlreadyConnected:Z

.field protected final mDeviceType:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

.field protected final mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field public final resultID:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 218
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V
    .locals 0
    .param p1, "resultID"    # I
    .param p2, "type"    # Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .param p3, "info"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p4, "alreadyConnected"    # Z

    .prologue
    .line 114
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 115
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->resultID:I

    .line 116
    iput-boolean p4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mAlreadyConnected:Z

    .line 117
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mDeviceType:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 118
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 119
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v3, 0x1

    .line 200
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 201
    new-instance v2, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;

    invoke-direct {v2, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;-><init>(Landroid/os/Parcel;)V

    .line 202
    .local v2, "unpacker":Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 203
    .local v1, "ipcVersion":I
    if-le v1, v3, :cond_0

    .line 205
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Loading DeviceInfo with ipcVersion "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " as a version 1 parcel."

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 208
    :cond_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    iput v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->resultID:I

    .line 209
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v4

    if-eqz v4, :cond_1

    :goto_0
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mAlreadyConnected:Z

    .line 210
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mDeviceType:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 211
    new-instance v0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;-><init>(Landroid/os/Parcel;)V

    .line 212
    .local v0, "innerunpack":Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
    const-class v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 213
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->finish()V

    .line 214
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->finish()V

    .line 215
    return-void

    .line 209
    .end local v0    # "innerunpack":Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
    :cond_1
    const/4 v3, 0x0

    goto :goto_0
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 180
    const/4 v0, 0x0

    return v0
.end method

.method public getAntDeviceNumber()I
    .locals 1

    .prologue
    .line 136
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    return v0
.end method

.method public getAntDeviceType()Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mDeviceType:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    return-object v0
.end method

.method public getDeviceDisplayName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 174
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    return-object v0
.end method

.method public isAlreadyConnected()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mAlreadyConnected:Z

    return v0
.end method

.method public isPreferredDevice()Z
    .locals 1

    .prologue
    .line 154
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isUserRecognizedDevice()Z
    .locals 1

    .prologue
    .line 163
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->device_dbId:Ljava/lang/Long;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 4
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    const/4 v2, 0x1

    .line 186
    new-instance v1, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;

    invoke-direct {v1, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;-><init>(Landroid/os/Parcel;)V

    .line 187
    .local v1, "packer":Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 188
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->resultID:I

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->writeInt(I)V

    .line 189
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mAlreadyConnected:Z

    if-eqz v3, :cond_0

    :goto_0
    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 190
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mDeviceType:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v2

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 193
    new-instance v0, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;-><init>(Landroid/os/Parcel;)V

    .line 194
    .local v0, "innerpacker":Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 195
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;->finish()V

    .line 196
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;->finish()V

    .line 197
    return-void

    .line 189
    .end local v0    # "innerpacker":Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method
