.class public Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_ScanForDevices.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field cancelled:Z

.field deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

.field private final geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

.field ieIndex:I

.field ieListNumDevs:I

.field msgsSinceFirstPageRequest:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;)V
    .locals 2
    .param p1, "geocacheReceiver"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .prologue
    const/4 v1, 0x0

    .line 32
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 36
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    .line 38
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 39
    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    .line 33
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .line 34
    return-void
.end method

.method private addToIeList(I)V
    .locals 5
    .param p1, "deviceID"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 338
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 339
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    .line 340
    iput v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 342
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v1, Lcom/dsi/ant/message/ChannelId;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v2}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v2, 0x13

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v3}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    invoke-direct {v1, p1, v2, v4}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    .line 345
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    const/4 v1, 0x4

    if-ge v0, v1, :cond_1

    .line 347
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    .line 348
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    .line 350
    :cond_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "IE list using "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " devices"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 351
    return-void
.end method

.method private closeIeList()V
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 357
    :try_start_0
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    if-lez v1, :cond_0

    .line 358
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    const/4 v2, 0x0

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 365
    :cond_0
    return-void

    .line 359
    :catch_0
    move-exception v0

    .line 361
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE trying to clear IE list: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 363
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1
.end method

.method private initIeList()V
    .locals 17
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;,
            Lcom/dsi/ant/channel/AntCommandFailedException;
        }
    .end annotation

    .prologue
    .line 277
    const/4 v12, -0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 278
    const/4 v12, 0x0

    move-object/from16 v0, p0

    iput v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    .line 280
    const/4 v1, -0x2

    .line 281
    .local v1, "candidateDeviceID":I
    const-wide/16 v4, -0x1

    .line 282
    .local v4, "lastFoundTime":J
    const/4 v8, -0x1

    .line 283
    .local v8, "lowestListIndex":I
    const/4 v12, 0x4

    new-array v11, v12, [I

    fill-array-data v11, :array_0

    .line 291
    .local v11, "toAddLowestFirst":[I
    :cond_0
    const/4 v12, 0x3

    if-ge v8, v12, :cond_5

    const/4 v12, -0x1

    if-eq v1, v12, :cond_5

    .line 293
    const/4 v1, -0x1

    .line 294
    const-wide/16 v6, 0x4e20

    .line 297
    .local v6, "lowest":J
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v12, v12, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget-object v12, v12, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v12

    if-eqz v12, :cond_2

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 299
    .local v2, "i":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getTimeSinceLastSeen()J

    move-result-wide v9

    .line 302
    .local v9, "timeSince":J
    cmp-long v12, v9, v4

    if-lez v12, :cond_1

    cmp-long v12, v9, v6

    if-gez v12, :cond_1

    .line 304
    move-wide v6, v9

    .line 305
    iget-object v12, v2, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v1, v12, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    goto :goto_0

    .line 309
    .end local v2    # "i":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    .end local v9    # "timeSince":J
    :cond_2
    if-ltz v1, :cond_3

    .line 311
    move-wide v4, v6

    .line 312
    add-int/lit8 v8, v8, 0x1

    aput v1, v11, v8

    .line 316
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v12, :cond_0

    .line 334
    .end local v3    # "i$":Ljava/util/Iterator;
    .end local v6    # "lowest":J
    :cond_4
    :goto_1
    return-void

    .line 321
    :cond_5
    move v2, v8

    .local v2, "i":I
    :goto_2
    if-ltz v2, :cond_6

    .line 323
    move-object/from16 v0, p0

    iget v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    .line 325
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v13, Lcom/dsi/ant/message/ChannelId;

    aget v14, v11, v2

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v15}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v15, 0x13

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v16, 0x0

    invoke-direct/range {v13 .. v16}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    move-object/from16 v0, p0

    iget v14, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    invoke-virtual {v12, v13, v14}, Lcom/dsi/ant/channel/AntChannel;->addIdToInclusionExclusionList(Lcom/dsi/ant/message/ChannelId;I)V

    .line 327
    move-object/from16 v0, p0

    iget-boolean v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-nez v12, :cond_4

    .line 321
    add-int/lit8 v2, v2, -0x1

    goto :goto_2

    .line 331
    :cond_6
    move-object/from16 v0, p0

    iget v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieIndex:I

    add-int/lit8 v12, v12, 0x1

    move-object/from16 v0, p0

    iput v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    .line 333
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    move-object/from16 v0, p0

    iget v13, v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->ieListNumDevs:I

    const/4 v14, 0x1

    invoke-virtual {v12, v13, v14}, Lcom/dsi/ant/channel/AntChannel;->configureInclusionExclusionList(IZ)V

    goto :goto_1

    .line 283
    :array_0
    .array-data 4
        -0x1
        -0x1
        -0x1
        -0x1
    .end array-data
.end method


# virtual methods
.method public doWork()V
    .locals 12
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 190
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/4 v8, 0x2

    invoke-virtual {v7, v8}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->setCurrentState(I)V

    .line 195
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->flushAndEnsureClosedChannel()V

    .line 196
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    if-eqz v7, :cond_0

    .line 268
    :goto_0
    return-void

    .line 202
    :cond_0
    :try_start_1
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->initIeList()V

    .line 203
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v7, :cond_1

    .line 205
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 212
    :catch_0
    move-exception v0

    .line 214
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_2
    sget-object v7, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "ACFE initializing channel: "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 215
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 261
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 263
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V

    .line 264
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 209
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_1
    :try_start_3
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v8, Lcom/dsi/ant/message/ChannelId;

    const/4 v9, 0x0

    iget-object v10, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v10, 0x13

    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v11}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v11, 0x0

    invoke-direct {v8, v9, v10, v11}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v7, v8}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 210
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->enableMessageProcessing()V

    .line 211
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v7}, Lcom/dsi/ant/channel/AntChannel;->open()V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_3 .. :try_end_3} :catch_1

    .line 219
    const-wide/16 v7, 0x2710

    :try_start_4
    invoke-static {v7, v8}, Ljava/lang/Thread;->sleep(J)V

    .line 220
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v7, :cond_2

    .line 222
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V

    goto :goto_0

    .line 226
    :cond_2
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->disableMessageProcessing()V

    .line 227
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->flushAndEnsureClosedChannel()V

    .line 228
    iget-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    if-eqz v7, :cond_3

    .line 230
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V

    goto :goto_0

    .line 237
    :cond_3
    const/4 v5, 0x1

    .line 238
    .local v5, "needDlTask":Z
    const/4 v1, 0x0

    .line 239
    .local v1, "firstNonOldDlDevice":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    move-object v2, p0

    .line 240
    .local v2, "headTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->deviceDataList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_4
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 243
    .local v3, "i":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getTimeSinceLastSeen()J

    move-result-wide v7

    const-wide/16 v9, 0x7530

    cmp-long v7, v7, v9

    if-lez v7, :cond_5

    .line 245
    new-instance v6, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/4 v8, 0x0

    invoke-direct {v6, v7, v3, v8}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;Z)V

    .line 246
    .local v6, "newTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    invoke-virtual {v2, v6}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 247
    move-object v2, v6

    .line 248
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v7

    if-ltz v7, :cond_4

    .line 249
    const/4 v5, 0x0

    goto :goto_1

    .line 251
    .end local v6    # "newTask":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
    :cond_5
    if-nez v1, :cond_4

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v7

    if-ltz v7, :cond_4

    .line 253
    move-object v1, v3

    goto :goto_1

    .line 256
    .end local v3    # "i":Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;
    :cond_6
    if-eqz v5, :cond_7

    if-eqz v1, :cond_7

    .line 257
    new-instance v7, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;

    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/4 v9, 0x0

    invoke-direct {v7, v8, v1, v9}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_BackgroundDownloadData;-><init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;Z)V

    invoke-virtual {v2, v7}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 260
    :cond_7
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->closeIeList()V
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_1

    goto/16 :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string v0, "Geocache Device Scan"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 56
    const v0, 0x7fffffff

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->handleInterruptRequest(I)Z

    .line 57
    return-void
.end method

.method public handleInterruptRequest(I)Z
    .locals 3
    .param p1, "interruptingTaskRank"    # I

    .prologue
    const/4 v2, 0x1

    .line 47
    sget-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v1, "Background scan cancelling"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    .line 49
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->disableMessageProcessing()V

    .line 50
    return v2
.end method

.method public initTask()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 182
    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 183
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->cancelled:Z

    .line 184
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->setNextTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    .line 185
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 64
    :try_start_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 171
    :cond_0
    :goto_0
    return-void

    .line 68
    :pswitch_0
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v4, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v4, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v4}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v4

    invoke-virtual {v4}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_1

    goto :goto_0

    .line 71
    :pswitch_1
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 72
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v4, Lcom/dsi/ant/message/ChannelId;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x13

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 73
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->open()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 155
    :catch_0
    move-exception v2

    .line 157
    .local v2, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v3

    sget-object v4, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v3, v4, :cond_5

    .line 161
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    const-string v4, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 79
    .end local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-eqz v3, :cond_0

    .line 81
    const/4 v3, 0x0

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 82
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v4, Lcom/dsi/ant/message/ChannelId;

    const/4 v5, 0x0

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v6}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v6, 0x13

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v7, 0x0

    invoke-direct {v4, v5, v6, v7}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 83
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->open()V

    goto :goto_0

    .line 92
    :pswitch_3
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v4, v4, 0x3

    rsub-int/lit8 v4, v4, 0x2

    add-int/2addr v3, v4

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 104
    :pswitch_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-nez v3, :cond_2

    .line 106
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->requestChannelId()Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    move-result-object v3

    invoke-virtual {v3}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;->getChannelId()Lcom/dsi/ant/message/ChannelId;

    move-result-object v0

    .line 107
    .local v0, "chanId":Lcom/dsi/ant/message/ChannelId;
    invoke-virtual {v0}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v1

    .line 110
    .local v1, "deviceID":I
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    invoke-virtual {v3, v1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 111
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-eqz v3, :cond_1

    .line 113
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3, p2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->DecodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 118
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->addToIeList(I)V

    .line 119
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->close()V

    goto/16 :goto_0

    .line 124
    :cond_1
    new-instance v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-direct {v3, v1}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;-><init>(I)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 125
    const/4 v3, -0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    .line 130
    .end local v0    # "chanId":Lcom/dsi/ant/message/ChannelId;
    .end local v1    # "deviceID":I
    :cond_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3, p2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->DecodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 131
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 133
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    .line 134
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    const/16 v4, 0xa

    if-le v3, v4, :cond_3

    .line 137
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->close()V

    goto/16 :goto_0

    .line 139
    :cond_3
    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v3, v3, 0x3

    if-eqz v3, :cond_0

    .line 141
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v5

    invoke-virtual {v4, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->composePageRequest(I)[B

    move-result-object v4

    invoke-virtual {v3, v4}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto/16 :goto_0

    .line 146
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->addDeviceToList(Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;)V

    .line 147
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->deviceAcquiringInfo:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->deviceId:I

    invoke-direct {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->addToIeList(I)V

    .line 148
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v3}, Lcom/dsi/ant/channel/AntChannel;->close()V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 165
    .restart local v2    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_5
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ScanForDevices;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "ACFE handling message: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 64
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 68
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method
