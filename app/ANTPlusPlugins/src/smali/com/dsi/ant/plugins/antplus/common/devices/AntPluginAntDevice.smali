.class public abstract Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
.source "AntPluginAntDevice.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field estTimestamp:J

.field eventFlags:J

.field public final mAntChannel:Lcom/dsi/ant/channel/AntChannel;

.field mMsgHandler:Lcom/dsi/ant/channel/IAntChannelEventHandler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V
    .locals 2
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 50
    const/4 v0, 0x1

    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;I)V

    .line 23
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->estTimestamp:J

    .line 24
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->eventFlags:J

    .line 51
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    .line 53
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->beginHandleMessage()V

    .line 54
    return-void
.end method

.method private beginHandleMessage()V
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 60
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice$1;

    invoke-direct {v2, p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelEventHandler(Lcom/dsi/ant/channel/IAntChannelEventHandler;)V

    .line 75
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->checkChannelState(Z)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 82
    return-void

    .line 76
    :catch_0
    move-exception v0

    .line 78
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException during initizalization"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 79
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->onChannelDeath()V

    .line 80
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
.end method


# virtual methods
.method public abstract checkChannelState(Z)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public closeDevice()V
    .locals 1

    .prologue
    .line 117
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->mAntChannel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntChannel;->release()V

    .line 118
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->closeDevice()V

    .line 119
    return-void
.end method

.method public getEstimatedTimestamp()J
    .locals 2

    .prologue
    .line 92
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->estTimestamp:J

    return-wide v0
.end method

.method public getEventFlags()J
    .locals 2

    .prologue
    .line 97
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->eventFlags:J

    return-wide v0
.end method

.method public handleNewAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 2
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 86
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->estTimestamp:J

    .line 87
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 88
    return-void
.end method

.method public onChannelDeath()V
    .locals 1

    .prologue
    .line 109
    const/16 v0, -0x64

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;->setCurrentState(I)V

    .line 111
    return-void
.end method

.method public abstract onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
.end method
