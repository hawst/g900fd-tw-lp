.class public Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
.super Ljava/lang/Object;
.source "AntPluginEvent.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mEventSubscribers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Landroid/os/Messenger;",
            ">;"
        }
    .end annotation
.end field

.field public final mEvent_Id:Ljava/lang/Integer;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Ljava/lang/Integer;)V
    .locals 1
    .param p1, "event_Id"    # Ljava/lang/Integer;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    .line 28
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    .line 29
    return-void
.end method


# virtual methods
.method public fireEvent(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "eventData"    # Landroid/os/Bundle;

    .prologue
    .line 71
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    monitor-enter v5

    .line 75
    :try_start_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .line 76
    .local v3, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/util/UUID;Landroid/os/Messenger;>;>;"
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 78
    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 79
    .local v2, "i":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/UUID;Landroid/os/Messenger;>;"
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 80
    .local v0, "data":Landroid/os/Message;
    const/4 v4, 0x1

    iput v4, v0, Landroid/os/Message;->what:I

    .line 81
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    iput v4, v0, Landroid/os/Message;->arg1:I

    .line 82
    invoke-virtual {v0, p1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 85
    :try_start_1
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/os/Messenger;

    invoke-virtual {v4, v0}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_1
    .catch Landroid/os/RemoteException; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 86
    :catch_0
    move-exception v1

    .line 93
    .local v1, "e":Landroid/os/RemoteException;
    :try_start_2
    sget-object v4, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Remote Exception sending event to remote response messenger with token "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", removed from subscription. Event ID: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 94
    invoke-interface {v3}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 97
    .end local v0    # "data":Landroid/os/Message;
    .end local v1    # "e":Landroid/os/RemoteException;
    .end local v2    # "i":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/util/UUID;Landroid/os/Messenger;>;"
    .end local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/util/UUID;Landroid/os/Messenger;>;>;"
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    throw v4

    .restart local v3    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Ljava/util/Map$Entry<Ljava/util/UUID;Landroid/os/Messenger;>;>;"
    :cond_0
    :try_start_3
    monitor-exit v5
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 98
    return-void
.end method

.method public hasSubscribers()Z
    .locals 1

    .prologue
    .line 33
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->size()I

    move-result v0

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z
    .locals 2
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "resultReceiver"    # Landroid/os/Messenger;

    .prologue
    .line 39
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    monitor-enter v1

    .line 42
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 44
    const/4 v0, 0x0

    monitor-exit v1

    .line 50
    :goto_0
    return v0

    .line 49
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    invoke-virtual {v0, p1, p2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 50
    const/4 v0, 0x1

    monitor-exit v1

    goto :goto_0

    .line 52
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public unsubscribeFromEvent(Ljava/util/UUID;)Z
    .locals 2
    .param p1, "token"    # Ljava/util/UUID;

    .prologue
    .line 58
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    monitor-enter v1

    .line 60
    :try_start_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEventSubscribers:Ljava/util/HashMap;

    invoke-virtual {v0, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    .line 62
    const/4 v0, 0x0

    monitor-exit v1

    .line 65
    :goto_0
    return v0

    .line 64
    :cond_0
    monitor-exit v1

    .line 65
    const/4 v0, 0x1

    goto :goto_0

    .line 64
    :catchall_0
    move-exception v0

    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method
