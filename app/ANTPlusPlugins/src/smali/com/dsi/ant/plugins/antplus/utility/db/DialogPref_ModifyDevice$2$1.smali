.class Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;
.super Ljava/lang/Object;
.source "DialogPref_ModifyDevice.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->onClick(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;)V
    .locals 0

    .prologue
    .line 173
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 178
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 181
    .local v0, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->removeDeviceFromList(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)V

    .line 182
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    const/4 v2, 0x0

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->callChangeListener(Ljava/lang/Object;)Z
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$400(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Ljava/lang/Object;)Z

    .line 183
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->val$d:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 184
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getContext()Landroid/content/Context;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Removed "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2$1;->this$1:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    # getter for: Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v3

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 188
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 190
    return-void

    .line 188
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v1
.end method
