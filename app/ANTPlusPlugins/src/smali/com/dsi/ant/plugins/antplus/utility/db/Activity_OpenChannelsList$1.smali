.class Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;
.super Ljava/lang/Object;
.source "Activity_OpenChannelsList.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;)V
    .locals 0

    .prologue
    .line 35
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "name"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 50
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    new-instance v2, Lcom/dsi/ant/AntService;

    invoke-direct {v2, p2}, Lcom/dsi/ant/AntService;-><init>(Landroid/os/IBinder;)V

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProvider:Lcom/dsi/ant/AntService;

    .line 51
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    const/4 v2, 0x1

    iput-boolean v2, v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->needsRefresh:Z

    .line 55
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->chanProvider:Lcom/dsi/ant/AntService;

    invoke-virtual {v2}, Lcom/dsi/ant/AntService;->getChannelProvider()Lcom/dsi/ant/channel/AntChannelProvider;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/channel/AntChannelProvider;->getNumChannelsAvailable()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->updateChannelsAvailableDisplay(Ljava/lang/Integer;)V
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->access$000(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;Ljava/lang/Integer;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 64
    :goto_0
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->TAG:Ljava/lang/String;

    const-string v2, "RemoteException occured while trying to query available channels"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->w(Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    const-string v2, "RemoteException querying ARS"

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->setError(Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "name"    # Landroid/content/ComponentName;

    .prologue
    .line 40
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->TAG:Ljava/lang/String;

    const-string v1, "AntChannelProvider binder disconnected"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 41
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    const-string v1, "Binder disconnected"

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->setError(Ljava/lang/String;)V

    .line 43
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->unbindFromArs()V

    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList$1;->this$0:Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_OpenChannelsList;->needsRefresh:Z

    .line 45
    return-void
.end method
