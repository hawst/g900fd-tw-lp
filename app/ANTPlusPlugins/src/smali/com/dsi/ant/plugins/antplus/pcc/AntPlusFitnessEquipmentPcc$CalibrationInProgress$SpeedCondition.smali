.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
.super Ljava/lang/Enum;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "SpeedCondition"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

.field public static final enum CURRENT_SPEED_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

.field public static final enum CURRENT_SPEED_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

.field public static final enum NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 931
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    const-string v1, "NOT_APPLICABLE"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    .line 936
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    const-string v1, "CURRENT_SPEED_TOO_LOW"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->CURRENT_SPEED_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    .line 941
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    const-string v1, "CURRENT_SPEED_OK"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->CURRENT_SPEED_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    .line 946
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    .line 926
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->NOT_APPLICABLE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->CURRENT_SPEED_TOO_LOW:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->CURRENT_SPEED_OK:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 951
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 952
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->intValue:I

    .line 953
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 971
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 973
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 979
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    :goto_1
    return-object v3

    .line 971
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 977
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    .line 978
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->intValue:I

    move-object v3, v4

    .line 979
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 926
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;
    .locals 1

    .prologue
    .line 926
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 961
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$CalibrationInProgress$SpeedCondition;->intValue:I

    return v0
.end method
