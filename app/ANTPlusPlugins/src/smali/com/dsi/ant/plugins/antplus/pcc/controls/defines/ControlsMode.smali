.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
.super Ljava/lang/Enum;
.source "ControlsMode.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

.field public static final enum AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

.field public static final enum GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

.field public static final enum UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

.field public static final enum VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;


# instance fields
.field private final longValue:J

.field private final reqAccValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x3

    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v2, 0x0

    .line 13
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    const-string v1, "UNRECOGNIZED_MODE_PRESENT"

    const-wide/16 v3, 0x1

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;-><init>(Ljava/lang/String;IJI)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 17
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    const-string v4, "AUDIO_MODE"

    const-wide/16 v6, 0x2

    const/16 v8, 0x12c

    move v5, v9

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;-><init>(Ljava/lang/String;IJI)V

    sput-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 21
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    const-string v4, "VIDEO_MODE"

    const-wide/16 v6, 0x4

    const/16 v8, 0x12d

    move v5, v10

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;-><init>(Ljava/lang/String;IJI)V

    sput-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 25
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    const-string v4, "GENERIC_MODE"

    const-wide/16 v6, 0x8

    const/16 v8, 0x12e

    move v5, v11

    invoke-direct/range {v3 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;-><init>(Ljava/lang/String;IJI)V

    sput-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 8
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    aput-object v1, v0, v9

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    aput-object v1, v0, v10

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    aput-object v1, v0, v11

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IJI)V
    .locals 0
    .param p3, "longValue"    # J
    .param p5, "reqAccValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(JI)V"
        }
    .end annotation

    .prologue
    .line 36
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 37
    iput-wide p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->longValue:J

    .line 38
    iput p5, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->reqAccValue:I

    .line 39
    return-void
.end method

.method public static getControlsModeFromRequestAccessValue(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .locals 6
    .param p0, "value"    # I

    .prologue
    .line 80
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 82
    .local v5, "result":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 84
    .local v3, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getRequestAccessModeValue()I

    move-result v4

    .line 86
    .local v4, "modeReqAccValue":I
    if-ne v4, p0, :cond_1

    .line 88
    move-object v5, v3

    .line 92
    .end local v3    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .end local v4    # "modeReqAccValue":I
    :cond_0
    return-object v5

    .line 82
    .restart local v3    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .restart local v4    # "modeReqAccValue":I
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static getControlsModesFromLong(J)Ljava/util/EnumSet;
    .locals 9
    .param p0, "longValue"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(J)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;"
        }
    .end annotation

    .prologue
    .line 59
    const-class v7, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-static {v7}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    .line 61
    .local v1, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v4, v0, v2

    .line 63
    .local v4, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v5

    .line 65
    .local v5, "modeValue":J
    and-long v7, v5, p0

    cmp-long v7, v7, v5

    if-nez v7, :cond_0

    .line 67
    invoke-virtual {v1, v4}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 68
    sub-long/2addr p0, v5

    .line 61
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 72
    .end local v4    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .end local v5    # "modeValue":J
    :cond_1
    const-wide/16 v7, 0x0

    cmp-long v7, p0, v7

    if-eqz v7, :cond_2

    .line 73
    sget-object v7, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->UNRECOGNIZED_MODE_PRESENT:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v1, v7}, Ljava/util/EnumSet;->add(Ljava/lang/Object;)Z

    .line 75
    :cond_2
    return-object v1
.end method

.method public static getLongFromControlsModes(Ljava/util/EnumSet;)J
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;)J"
        }
    .end annotation

    .prologue
    .line 97
    .local p0, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    const-wide/16 v1, 0x0

    .line 99
    .local v1, "longValue":J
    invoke-virtual {p0}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 100
    .local v3, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v4

    or-long/2addr v1, v4

    goto :goto_0

    .line 102
    .end local v3    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    :cond_0
    return-wide v1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 8
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .locals 1

    .prologue
    .line 8
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    return-object v0
.end method


# virtual methods
.method public getLongValue()J
    .locals 2

    .prologue
    .line 43
    iget-wide v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->longValue:J

    return-wide v0
.end method

.method public getRequestAccessModeValue()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->reqAccValue:I

    return v0
.end method
