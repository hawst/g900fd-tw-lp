.class Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "MultiDeviceSearch.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SearchPcc"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;


# direct methods
.method private constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)V
    .locals 0

    .prologue
    .line 360
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;
    .param p2, "x1"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$1;

    .prologue
    .line 360
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;
    .param p1, "x1"    # Landroid/content/Context;
    .param p2, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 360
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->requestAccess(Landroid/content/Context;Landroid/os/Bundle;)V

    return-void
.end method

.method private requestAccess(Landroid/content/Context;Landroid/os/Bundle;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "params"    # Landroid/os/Bundle;

    .prologue
    .line 366
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mAccessResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$400(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    move-result-object v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$500(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    move-result-object v4

    invoke-direct {v0, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)V

    .line 369
    .local v0, "releaser":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    .line 371
    new-instance v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;-><init>()V

    .line 373
    .local v1, "resultHandler":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler<Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;>;"
    invoke-virtual {v1, p0, v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;->setReturnInfo(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;)V

    .line 375
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    .line 377
    invoke-static {p1, p2, p0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->requestAccess_Helper_SubMain(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Handler;)V

    .line 378
    return-void
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 434
    const-string v0, "ANT+ Plugin: Multiple Device Search"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 392
    const/16 v0, 0x4eed

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 383
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 384
    .local v0, "i":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.multisearch.MultiSearchService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 386
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 7
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    const/4 v6, 0x0

    .line 398
    iget v4, p1, Landroid/os/Message;->arg1:I

    packed-switch v4, :pswitch_data_0

    .line 426
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$000()Ljava/lang/String;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Unrecognized event received: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 429
    :goto_0
    :pswitch_0
    return-void

    .line 402
    :pswitch_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 403
    .local v0, "b":Landroid/os/Bundle;
    const-class v4, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;

    invoke-virtual {v4}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v4

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 404
    const-string v4, "dev_Device"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;

    .line 406
    .local v2, "info":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mCallbacks:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;

    move-result-object v4

    invoke-interface {v4, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;->onDeviceFound(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;)V

    goto :goto_0

    .line 411
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "info":Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
    :pswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 412
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v4, "int_resultID"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v1

    .line 413
    .local v1, "id":I
    const-string v4, "int_rssi"

    invoke-virtual {v0, v4}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 414
    .local v3, "rssi":I
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mRssiCallback:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->access$600(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;

    move-result-object v4

    invoke-interface {v4, v1, v3}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;->onRssiUpdate(II)V

    goto :goto_0

    .line 420
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "id":I
    .end local v3    # "rssi":I
    :pswitch_3
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    check-cast v4, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;

    iget v5, p1, Landroid/os/Message;->arg2:I

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;

    move-result-object v5

    invoke-virtual {v4, v6, v5, v6}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;->onResultReceived(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestAccessResult;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceState;)V

    .line 423
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->close()V

    goto :goto_0

    .line 398
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method
