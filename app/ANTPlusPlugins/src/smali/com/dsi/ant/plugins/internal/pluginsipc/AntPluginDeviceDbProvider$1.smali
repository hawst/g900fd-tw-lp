.class final Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;
.super Landroid/os/Handler;
.source "AntPluginDeviceDbProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->executeDbRequest(Landroid/content/Context;Landroid/os/Bundle;)Landroid/os/Bundle;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$ht:Landroid/os/HandlerThread;

.field final synthetic val$requestFinishedExchange:Ljava/util/concurrent/Exchanger;


# direct methods
.method constructor <init>(Landroid/os/Looper;Ljava/util/concurrent/Exchanger;Landroid/os/HandlerThread;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Looper;

    .prologue
    .line 212
    iput-object p2, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$requestFinishedExchange:Ljava/util/concurrent/Exchanger;

    iput-object p3, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$ht:Landroid/os/HandlerThread;

    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 218
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$requestFinishedExchange:Ljava/util/concurrent/Exchanger;

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v2

    const-wide/16 v3, 0x64

    sget-object v5, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4, v5}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_1
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 228
    iget-object v1, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$ht:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    .line 230
    :goto_0
    return-void

    .line 219
    :catch_0
    move-exception v0

    .line 221
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_1
    # getter for: Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Handler result exchange interrupted"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 228
    iget-object v1, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$ht:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_0

    .line 222
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 224
    .local v0, "e":Ljava/util/concurrent/TimeoutException;
    :try_start_2
    # getter for: Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->access$000()Ljava/lang/String;

    move-result-object v1

    const-string v2, "Handler result exchange timed out"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 228
    iget-object v1, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$ht:Landroid/os/HandlerThread;

    invoke-virtual {v1}, Landroid/os/HandlerThread;->quit()Z

    goto :goto_0

    .end local v0    # "e":Ljava/util/concurrent/TimeoutException;
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;->val$ht:Landroid/os/HandlerThread;

    invoke-virtual {v2}, Landroid/os/HandlerThread;->quit()Z

    throw v1
.end method
