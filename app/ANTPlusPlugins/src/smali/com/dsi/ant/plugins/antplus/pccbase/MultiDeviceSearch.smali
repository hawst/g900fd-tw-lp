.class public Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;
.super Ljava/lang/Object;
.source "MultiDeviceSearch.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchReleaseHandle;,
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;,
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;,
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;,
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;,
        Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mAccessResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;",
            ">;"
        }
    .end annotation
.end field

.field private final mCallbacks:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;

.field private final mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

.field private final mRssiCallback:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;

.field private final mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 267
    const-class v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Ljava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p3, "callbacks"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;
    .param p4, "rssiCallback"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .line 308
    .local p2, "deviceTypes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 269
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$1;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$1;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mAccessResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    .line 283
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$2;

    invoke-direct {v6, p0}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$2;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    .line 293
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    const/4 v7, 0x0

    invoke-direct {v6, p0, v7}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;-><init>(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$1;)V

    iput-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    .line 309
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    if-nez p4, :cond_1

    .line 311
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    const-string v7, "Null parameter passed into MultiDeviceSearch constructor"

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 314
    :cond_1
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mCallbacks:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;

    .line 315
    iput-object p4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mRssiCallback:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;

    .line 316
    new-instance v4, Landroid/os/Bundle;

    invoke-direct {v4}, Landroid/os/Bundle;-><init>()V

    .line 317
    .local v4, "params":Landroid/os/Bundle;
    const-string v6, "int_RequestAccessMode"

    const/4 v7, 0x2

    invoke-virtual {v4, v6, v7}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 319
    invoke-virtual {p2}, Ljava/util/EnumSet;->size()I

    move-result v6

    new-array v5, v6, [I

    .line 320
    .local v5, "types":[I
    const/4 v2, 0x0

    .line 321
    .local v2, "index":I
    invoke-virtual {p2}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_2

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    .line 323
    .local v0, "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    add-int/lit8 v3, v2, 0x1

    .end local v2    # "index":I
    .local v3, "index":I
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getIntValue()I

    move-result v6

    aput v6, v5, v2

    move v2, v3

    .line 324
    .end local v3    # "index":I
    .restart local v2    # "index":I
    goto :goto_0

    .line 325
    .end local v0    # "dt":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    :cond_2
    const-string v6, "intarr_deviceTypeList"

    invoke-virtual {v4, v6, v5}, Landroid/os/Bundle;->putIntArray(Ljava/lang/String;[I)V

    .line 327
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->requestAccess(Landroid/content/Context;Landroid/os/Bundle;)V
    invoke-static {v6, p1, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->access$300(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;Landroid/content/Context;Landroid/os/Bundle;)V

    .line 328
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 29
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mCallbacks:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchCallbacks;

    return-object v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mAccessResultReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;

    return-object v0
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mStateChangeReceiver:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;

    return-object v0
.end method

.method static synthetic access$600(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mRssiCallback:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$RssiCallback;

    return-object v0
.end method

.method static synthetic access$700(Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;)Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;

    .prologue
    .line 29
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    return-object v0
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 335
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch;->mPcc:Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$SearchPcc;->mReleaseHandle:Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;->close()V

    .line 336
    return-void
.end method

.method public getMissingDependencyName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 353
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getMissingDependencyName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getMissingDependencyPackageName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 344
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->getMissingDependencyPackageName()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
