.class Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;
.super Ljava/lang/Object;
.source "WatchCommunicatorController.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->checkNeedsDownload()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;)V
    .locals 0

    .prologue
    .line 357
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 9

    .prologue
    .line 361
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    # getter for: Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->access$100(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-result-object v0

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->watchDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->this$0:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;

    # getter for: Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->parentService:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;->access$200(Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorService;

    move-result-object v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v3

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v5}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget v6, v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkRfFreq:I

    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller$1;->this$1:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;

    iget-object v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorController$ListenForNewActivitiesDownloadContoller;->deviceToConnectTo:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget v7, v7, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkPeriod:I

    const/16 v8, 0x1e

    invoke-static/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;

    .line 370
    return-void
.end method
