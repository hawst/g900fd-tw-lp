.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;
.super Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;
.source "AntPlusVideoRemoteControlPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final INVALIDCOMMANDDATA:I = 0xff

.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mVideoCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;

.field mVideoStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 171
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;-><init>()V

    .line 92
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 171
    return-void
.end method

.method public static requestAccessByDeviceNumber(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "antDeviceNumber"    # I
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 164
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;>;"
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;-><init>()V

    .local v6, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 167
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestRemoteControlAsyncScanController(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;
    .locals 2
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "searchProximityThreshold"    # I
    .param p3, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 120
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;-><init>()V

    .line 122
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;
    invoke-static {p0, p1, p2, v0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public RequestVideoCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V
    .locals 1
    .param p1, "videoCommandFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;
    .param p2, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .prologue
    .line 292
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->RequestVideoCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;I)V

    .line 293
    return-void
.end method

.method public RequestVideoCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;I)V
    .locals 6
    .param p1, "videoCommandFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;
    .param p2, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .param p3, "commandData"    # I

    .prologue
    .line 249
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 251
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestVideoCommand failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 283
    :goto_0
    return-void

    .line 255
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;

    .line 257
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 258
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e24

    iput v3, v0, Landroid/os/Message;->what:I

    .line 259
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 260
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 262
    const-string v3, "int_commandNumber"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    const-string v3, "int_commandData"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 265
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 267
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 269
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestVideoCommand died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 274
    :cond_1
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    .line 277
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd RequestVideoCommand failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 279
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "RequestVideoCommand cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 282
    :cond_2
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 176
    const-string v0, "ANT+ Plugin: Video Remote Control"

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 14
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 182
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 217
    :pswitch_0
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 220
    :cond_0
    :goto_0
    return-void

    .line 186
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    .line 190
    .local v9, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 191
    .local v1, "estTimestamp":J
    const-string v0, "int_volume"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v3

    .line 192
    .local v3, "volume":I
    const-string v0, "bool_muted"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 193
    .local v4, "muted":Z
    const-string v0, "int_timeRemaining"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 194
    .local v5, "timeRemaining":I
    const-string v0, "int_timeProgressed"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 195
    .local v6, "timeProgressed":I
    invoke-static {v9}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;->readFromBundle(Landroid/os/Bundle;)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;

    move-result-object v7

    .line 196
    .local v7, "videoCapabilities":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;
    const-string v0, "int_videoState"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;

    move-result-object v8

    .line 197
    .local v8, "videoStateCode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;

    invoke-interface/range {v0 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;->onNewVideoStatus(JIZIILcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;)V

    goto :goto_0

    .line 203
    .end local v1    # "estTimestamp":J
    .end local v3    # "volume":I
    .end local v4    # "muted":Z
    .end local v5    # "timeRemaining":I
    .end local v6    # "timeProgressed":I
    .end local v7    # "videoCapabilities":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceCapabilities;
    .end local v8    # "videoStateCode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/VideoDeviceState;
    .end local v9    # "b":Landroid/os/Bundle;
    :pswitch_2
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;

    if-eqz v0, :cond_0

    .line 206
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->release()V

    .line 208
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v9

    .line 209
    .restart local v9    # "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 210
    .restart local v1    # "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v12

    invoke-static {v12, v13}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v10

    .line 211
    .local v10, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "int_requestStatus"

    invoke-virtual {v9, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v11

    .line 212
    .local v11, "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;

    invoke-interface {v0, v1, v2, v10, v11}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoCommandFinishedReceiver;->onVideoCommandFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto :goto_0

    .line 182
    :pswitch_data_0
    .packed-switch 0xcc
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_2
    .end packed-switch
.end method

.method public subscribeVideoStatusEvent(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;)V
    .locals 1
    .param p1, "VideoStatusReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;

    .prologue
    const/16 v0, 0xcc

    .line 230
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->mVideoStatusReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc$IVideoStatusReceiver;

    .line 231
    if-eqz p1, :cond_0

    .line 233
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->subscribeToEvent(I)Z

    .line 238
    :goto_0
    return-void

    .line 236
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusVideoRemoteControlPcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method
