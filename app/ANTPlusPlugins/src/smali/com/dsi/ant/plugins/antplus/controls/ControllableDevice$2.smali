.class synthetic Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;
.super Ljava/lang/Object;
.source "ControllableDevice.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1008
    name = null
.end annotation


# static fields
.field static final synthetic $SwitchMap$com$dsi$ant$message$ChannelState:[I

.field static final synthetic $SwitchMap$com$dsi$ant$message$EventCode:[I

.field static final synthetic $SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

.field static final synthetic $SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 318
    invoke-static {}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->values()[Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_0
    .catch Ljava/lang/NoSuchFieldError; {:try_start_0 .. :try_end_0} :catch_a

    :goto_0
    :try_start_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_1
    .catch Ljava/lang/NoSuchFieldError; {:try_start_1 .. :try_end_1} :catch_9

    :goto_1
    :try_start_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_2
    .catch Ljava/lang/NoSuchFieldError; {:try_start_2 .. :try_end_2} :catch_8

    .line 322
    :goto_2
    invoke-static {}, Lcom/dsi/ant/message/EventCode;->values()[Lcom/dsi/ant/message/EventCode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    :try_start_3
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->TX:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_3
    .catch Ljava/lang/NoSuchFieldError; {:try_start_3 .. :try_end_3} :catch_7

    :goto_3
    :try_start_4
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    sget-object v1, Lcom/dsi/ant/message/EventCode;->CHANNEL_COLLISION:Lcom/dsi/ant/message/EventCode;

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_4
    .catch Ljava/lang/NoSuchFieldError; {:try_start_4 .. :try_end_4} :catch_6

    .line 205
    :goto_4
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->values()[Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    :try_start_5
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_5
    .catch Ljava/lang/NoSuchFieldError; {:try_start_5 .. :try_end_5} :catch_5

    :goto_5
    :try_start_6
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_6
    .catch Ljava/lang/NoSuchFieldError; {:try_start_6 .. :try_end_6} :catch_4

    :goto_6
    :try_start_7
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_7
    .catch Ljava/lang/NoSuchFieldError; {:try_start_7 .. :try_end_7} :catch_3

    .line 132
    :goto_7
    invoke-static {}, Lcom/dsi/ant/message/ChannelState;->values()[Lcom/dsi/ant/message/ChannelState;

    move-result-object v0

    array-length v0, v0

    new-array v0, v0, [I

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    :try_start_8
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x1

    aput v2, v0, v1
    :try_end_8
    .catch Ljava/lang/NoSuchFieldError; {:try_start_8 .. :try_end_8} :catch_2

    :goto_8
    :try_start_9
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->SEARCHING:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x2

    aput v2, v0, v1
    :try_end_9
    .catch Ljava/lang/NoSuchFieldError; {:try_start_9 .. :try_end_9} :catch_1

    :goto_9
    :try_start_a
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/ControllableDevice$2;->$SwitchMap$com$dsi$ant$message$ChannelState:[I

    sget-object v1, Lcom/dsi/ant/message/ChannelState;->ASSIGNED:Lcom/dsi/ant/message/ChannelState;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelState;->ordinal()I

    move-result v1

    const/4 v2, 0x3

    aput v2, v0, v1
    :try_end_a
    .catch Ljava/lang/NoSuchFieldError; {:try_start_a .. :try_end_a} :catch_0

    :goto_a
    return-void

    :catch_0
    move-exception v0

    goto :goto_a

    :catch_1
    move-exception v0

    goto :goto_9

    :catch_2
    move-exception v0

    goto :goto_8

    .line 205
    :catch_3
    move-exception v0

    goto :goto_7

    :catch_4
    move-exception v0

    goto :goto_6

    :catch_5
    move-exception v0

    goto :goto_5

    .line 322
    :catch_6
    move-exception v0

    goto :goto_4

    :catch_7
    move-exception v0

    goto :goto_3

    .line 318
    :catch_8
    move-exception v0

    goto/16 :goto_2

    :catch_9
    move-exception v0

    goto/16 :goto_1

    :catch_a
    move-exception v0

    goto/16 :goto_0
.end method
