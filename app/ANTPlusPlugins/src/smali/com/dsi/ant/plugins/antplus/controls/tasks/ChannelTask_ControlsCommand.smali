.class public Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_ControlsCommand.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field commandFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field isCommandSent:Z

.field isTransferInProgress:Z

.field private msgsSinceFirstCommand:I

.field remoteDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

.field private responseSent:Z

.field txBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V
    .locals 3
    .param p1, "remoteDevice"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "commandFinished"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .prologue
    const/4 v0, 0x0

    .line 41
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 30
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->responseSent:Z

    .line 33
    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isCommandSent:Z

    .line 36
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    .line 38
    const/16 v0, 0x8

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->txBuffer:[B

    .line 42
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->remoteDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    .line 43
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->commandFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->commandFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 45
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/16 v5, -0x28

    .line 180
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_0

    .line 182
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 183
    const/16 v1, -0x28

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    .line 211
    :goto_0
    return-void

    .line 187
    :cond_0
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 188
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->enableMessageProcessing()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 192
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_1

    .line 206
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isCommandSent:Z

    if-eqz v1, :cond_1

    .line 207
    const/4 v1, 0x0

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    goto :goto_0

    .line 193
    :catch_0
    move-exception v0

    .line 195
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    const-string v1, "ChannelTask_SendAcknowledgedDataPage"

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 198
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :catch_1
    move-exception v0

    .line 201
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntCommandFailedException in dowork(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 202
    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    .line 203
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 209
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_1
    invoke-virtual {p0, v5}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 216
    const-string v0, "Audio Command"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 222
    const/16 v0, -0x28

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    .line 223
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 173
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v3, 0x0

    .line 95
    :try_start_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 167
    :cond_0
    :goto_0
    return-void

    .line 99
    :pswitch_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 102
    :pswitch_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v2, "Search timeout occured"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 103
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->disableMessageProcessing()V

    .line 104
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 147
    :catch_0
    move-exception v0

    .line 149
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_2

    .line 153
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v2, "TRANSFER_IN_PROGRESS error sending ack msg"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 107
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    const/4 v1, 0x0

    :try_start_1
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isTransferInProgress:Z

    .line 108
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isCommandSent:Z

    .line 109
    const/4 v1, 0x0

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    .line 110
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 113
    :pswitch_3
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isTransferInProgress:Z

    .line 119
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    rem-int/lit8 v2, v2, 0x3

    rsub-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    goto :goto_0

    .line 127
    :pswitch_4
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isCommandSent:Z

    if-nez v1, :cond_0

    .line 129
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    .line 131
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    const/16 v2, 0x1e

    if-le v1, v2, :cond_1

    .line 133
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v2, "Tx retries exceeded sending command"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 134
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 135
    :cond_1
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->msgsSinceFirstCommand:I

    rem-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_0

    .line 137
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v2, "Tx retry command"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 138
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isTransferInProgress:Z

    .line 140
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->txBuffer:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 154
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_2
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_FAILED:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_3

    .line 156
    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->isTransferInProgress:Z

    goto/16 :goto_0

    .line 160
    :cond_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE handling message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 161
    const/16 v1, -0x28

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    .line 162
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->disableMessageProcessing()V

    .line 163
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 95
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_4
        :pswitch_4
    .end packed-switch

    .line 99
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public processRequest([B)V
    .locals 4
    .param p1, "txBuffer"    # [B

    .prologue
    .line 50
    const/4 v1, 0x0

    .line 52
    .local v1, "taskStarted":Z
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->txBuffer:[B

    .line 56
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->remoteDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 63
    :goto_0
    if-nez v1, :cond_0

    .line 65
    const/16 v2, -0x14

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->sendResponse(I)V

    .line 66
    sget-object v2, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v3, "channel task not started"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 71
    :goto_1
    return-void

    .line 57
    :catch_0
    move-exception v0

    .line 60
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 69
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->remoteDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->setCurrentState(I)V

    goto :goto_1
.end method

.method public sendResponse(I)V
    .locals 4
    .param p1, "statusCode"    # I

    .prologue
    .line 75
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->commandFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v2

    .line 77
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->responseSent:Z

    if-eqz v1, :cond_0

    .line 78
    monitor-exit v2

    .line 88
    :goto_0
    return-void

    .line 80
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 82
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_requestStatus"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->TAG:Ljava/lang/String;

    const-string v3, "Firing commandFinished event"

    invoke-static {v1, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 85
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->commandFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 86
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->responseSent:Z

    .line 87
    monitor-exit v2

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
