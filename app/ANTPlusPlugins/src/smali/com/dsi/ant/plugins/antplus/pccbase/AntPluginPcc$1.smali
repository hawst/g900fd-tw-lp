.class Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;
.super Ljava/lang/Object;
.source "AntPluginPcc.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->bindAndRequest(Landroid/content/Context;Landroid/os/Bundle;Landroid/os/Handler;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

.field final synthetic val$b:Landroid/os/Bundle;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Bundle;)V
    .locals 0

    .prologue
    .line 620
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->val$b:Landroid/os/Bundle;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 5
    .param p1, "arg0"    # Landroid/content/ComponentName;
    .param p2, "arg1"    # Landroid/os/IBinder;

    .prologue
    .line 625
    new-instance v2, Landroid/os/Messenger;

    invoke-direct {v2, p2}, Landroid/os/Messenger;-><init>(Landroid/os/IBinder;)V

    .line 626
    .local v2, "reqMsgr":Landroid/os/Messenger;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 627
    .local v1, "msg":Landroid/os/Message;
    const/4 v3, 0x0

    iput v3, v1, Landroid/os/Message;->what:I

    .line 628
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->val$b:Landroid/os/Bundle;

    invoke-virtual {v1, v3}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 631
    :try_start_0
    invoke-virtual {v2, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 638
    :goto_0
    return-void

    .line 632
    :catch_0
    move-exception v0

    .line 636
    .local v0, "e":Landroid/os/RemoteException;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->val$b:Landroid/os/Bundle;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->notifyBindAndRequestFailed(Landroid/os/Bundle;)V
    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 645
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->isInitialized:Z

    if-nez v0, :cond_0

    .line 647
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->val$b:Landroid/os/Bundle;

    # invokes: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->notifyBindAndRequestFailed(Landroid/os/Bundle;)V
    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$100(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Landroid/os/Bundle;)V

    .line 654
    :goto_0
    return-void

    .line 652
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$1;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    const-string v1, "OnServiceDisconnected fired"

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    goto :goto_0
.end method
