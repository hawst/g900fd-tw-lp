.class public Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "WeightScaleDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;
    }
.end annotation


# static fields
.field public static final TAG:Ljava/lang/String;


# instance fields
.field beaconCounter:I

.field fakedAnD_ManIdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field public mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

.field private serviceContext:Landroid/content/Context;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 43
    const-class v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;Landroid/content/Context;)V
    .locals 1
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 54
    invoke-direct {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 48
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->beaconCounter:I

    .line 56
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->serviceContext:Landroid/content/Context;

    .line 57
    return-void
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 15
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    .line 127
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->token_ClientMap:Ljava/util/HashMap;

    move-object/from16 v0, p1

    invoke-virtual {v1, v0}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 128
    .local v10, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v14

    .line 129
    .local v14, "response":Landroid/os/Message;
    move-object/from16 v0, p2

    iget v1, v0, Landroid/os/Message;->what:I

    iput v1, v14, Landroid/os/Message;->what:I

    .line 130
    move-object/from16 v0, p2

    iget v1, v0, Landroid/os/Message;->what:I

    packed-switch v1, :pswitch_data_0

    .line 187
    invoke-virtual {v14}, Landroid/os/Message;->recycle()V

    .line 188
    const/4 v14, 0x0

    .line 189
    invoke-super/range {p0 .. p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 192
    :goto_0
    return-void

    .line 134
    :pswitch_0
    new-instance v13, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;

    invoke-direct {v13, p0, v10}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;-><init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 136
    .local v13, "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;
    const/4 v1, 0x0

    iput v1, v14, Landroid/os/Message;->arg1:I

    .line 137
    invoke-virtual {p0, v10, v14}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 139
    invoke-virtual {v13}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->processRequest()V

    goto :goto_0

    .line 144
    .end local v13    # "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;
    :pswitch_1
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 145
    .local v11, "params":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v1

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 146
    const-string v1, "parcelable_UserProfile"

    invoke-virtual {v11, v1}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v12

    check-cast v12, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;

    .line 148
    .local v12, "profile":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;
    new-instance v13, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;

    invoke-direct {v13, p0, v10, v12}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;-><init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;)V

    .line 150
    .local v13, "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;
    const/4 v1, 0x0

    iput v1, v14, Landroid/os/Message;->arg1:I

    .line 151
    invoke-virtual {p0, v10, v14}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 153
    invoke-virtual {v13}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;->processRequest()V

    goto :goto_0

    .line 158
    .end local v11    # "params":Landroid/os/Bundle;
    .end local v12    # "profile":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$UserProfile;
    .end local v13    # "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_AdvancedMeasurement;
    :pswitch_2
    new-instance v13, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;

    invoke-direct {v13, p0, v10}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;-><init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 160
    .local v13, "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;
    const/4 v1, 0x0

    iput v1, v14, Landroid/os/Message;->arg1:I

    .line 161
    invoke-virtual {p0, v10, v14}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 163
    invoke-virtual {v13}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;->processRequest()V

    goto :goto_0

    .line 168
    .end local v13    # "req":Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_CapabilitiesRequest;
    :pswitch_3
    const/4 v1, 0x0

    iput v1, v14, Landroid/os/Message;->arg1:I

    .line 169
    invoke-virtual {p0, v10, v14}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 171
    invoke-virtual/range {p2 .. p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v11

    .line 172
    .restart local v11    # "params":Landroid/os/Bundle;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->serviceContext:Landroid/content/Context;

    invoke-direct {v2, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsPasskeyDatabase;-><init>(Landroid/content/Context;)V

    new-instance v3, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;

    const-string v4, "bool_UseAntFsProgressUpdates"

    invoke-virtual {v11, v4}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    invoke-direct {v3, p0, v4, v10}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice$WeightScaleDownloadAllController;-><init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;ZLcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->serviceContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getFourByteUniqueId(Landroid/content/Context;)J

    move-result-wide v4

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    const/16 v7, 0x39

    const/16 v8, 0x2000

    const/4 v9, 0x0

    invoke-static/range {v1 .. v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper;->processDownloadFilesRequest(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;Lcom/dsi/ant/plugins/antplus/utility/antfs/PluginDownloadFilesHelper$PluginDownloadController;JIIII)Ljava/lang/Thread;

    goto :goto_0

    .line 130
    :pswitch_data_0
    .packed-switch 0x4e21
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 230
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 231
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->closeDevice()V

    .line 232
    return-void
.end method

.method public getEventSet()Ljava/util/Set;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 108
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->getEventSet()Ljava/util/Set;

    move-result-object v1

    .line 112
    .local v1, "eventSet":Ljava/util/Set;, "Ljava/util/Set<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    invoke-interface {v1}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 114
    .local v0, "event":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    iget-object v3, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->mEvent_Id:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/16 v4, 0x64

    if-ne v3, v4, :cond_0

    .line 116
    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->fakedAnD_ManIdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 121
    .end local v0    # "event":Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;
    :cond_1
    return-object v1
.end method

.method public getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 96
    new-instance v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    .line 97
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 98
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 100
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 102
    return-object v0
.end method

.method public handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 6
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v5, -0x1

    const/4 v3, 0x1

    .line 63
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    if-ne v2, v3, :cond_0

    .line 68
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    const/16 v3, 0x43

    if-eq v2, v3, :cond_2

    .line 69
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->handleDataMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 91
    :cond_1
    :goto_0
    return-void

    .line 70
    :cond_2
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x3

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    if-nez v2, :cond_1

    .line 73
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->beaconCounter:I

    rem-int/lit8 v2, v2, 0x40

    if-nez v2, :cond_3

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->fakedAnD_ManIdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-eqz v2, :cond_3

    .line 76
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsMessageDefines$AntFsBeaconDefines;->getAntFsManufacturerId([B)I

    move-result v0

    .line 78
    .local v0, "antFsMfgId":I
    const/16 v2, 0x15

    if-ne v0, v2, :cond_3

    .line 80
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 81
    .local v1, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 82
    const-string v2, "long_EventFlags"

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 83
    const-string v2, "int_hardwareRevision"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 84
    const-string v2, "int_manufacturerID"

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 85
    const-string v2, "int_modelNumber"

    invoke-virtual {v1, v2, v5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 86
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->fakedAnD_ManIdEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 89
    .end local v0    # "antFsMfgId":I
    .end local v1    # "b":Landroid/os/Bundle;
    :cond_3
    iget v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->beaconCounter:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->beaconCounter:I

    goto :goto_0
.end method
