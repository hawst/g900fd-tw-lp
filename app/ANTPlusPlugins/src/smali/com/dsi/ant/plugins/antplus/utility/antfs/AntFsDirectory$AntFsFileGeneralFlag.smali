.class public final enum Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;
.super Ljava/lang/Enum;
.source "AntFsDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AntFsFileGeneralFlag"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum APPEND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum ARCHIVE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum CRYPTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

.field public static final enum WRITE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 97
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "READ"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 102
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "WRITE"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->WRITE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 107
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "ERASE"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 112
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "ARCHIVE"

    invoke-direct {v0, v1, v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ARCHIVE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 117
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "APPEND"

    invoke-direct {v0, v1, v7}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->APPEND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 122
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    const-string v1, "CRYPTO"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->CRYPTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    .line 92
    const/4 v0, 0x6

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->WRITE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ARCHIVE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->APPEND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->CRYPTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 92
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getFlagByteFromFlags(Ljava/util/EnumSet;)B
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;",
            ">;)B"
        }
    .end annotation

    .prologue
    .line 153
    .local p0, "flags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;>;"
    const/4 v0, 0x0

    .line 154
    .local v0, "value":B
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 155
    const/16 v1, 0x80

    int-to-byte v0, v1

    .line 156
    :cond_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->WRITE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 157
    or-int/lit8 v1, v0, 0x40

    int-to-byte v0, v1

    .line 158
    :cond_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 159
    or-int/lit8 v1, v0, 0x20

    int-to-byte v0, v1

    .line 160
    :cond_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ARCHIVE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 161
    or-int/lit8 v1, v0, 0x10

    int-to-byte v0, v1

    .line 162
    :cond_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->APPEND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 163
    or-int/lit8 v1, v0, 0x8

    int-to-byte v0, v1

    .line 164
    :cond_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->CRYPTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {p0, v1}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 165
    or-int/lit8 v1, v0, 0x4

    int-to-byte v0, v1

    .line 167
    :cond_5
    return v0
.end method

.method public static getFlagsFromFlagsByte(B)Ljava/util/EnumSet;
    .locals 2
    .param p0, "flagsByte"    # B
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(B)",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;",
            ">;"
        }
    .end annotation

    .prologue
    .line 131
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 132
    .local v0, "flags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;>;"
    and-int/lit16 v1, p0, 0x80

    if-lez v1, :cond_0

    .line 133
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->READ:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 134
    :cond_0
    and-int/lit8 v1, p0, 0x40

    if-lez v1, :cond_1

    .line 135
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->WRITE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 136
    :cond_1
    and-int/lit8 v1, p0, 0x20

    if-lez v1, :cond_2

    .line 137
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ERASE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 138
    :cond_2
    and-int/lit8 v1, p0, 0x10

    if-lez v1, :cond_3

    .line 139
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->ARCHIVE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 140
    :cond_3
    and-int/lit8 v1, p0, 0x8

    if-lez v1, :cond_4

    .line 141
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->APPEND:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 142
    :cond_4
    and-int/lit8 v1, p0, 0x4

    if-lez v1, :cond_5

    .line 143
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->CRYPTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 145
    :cond_5
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-eqz v1, :cond_6

    .line 146
    invoke-static {v0}, Ljava/util/EnumSet;->copyOf(Ljava/util/Collection;)Ljava/util/EnumSet;

    move-result-object v1

    .line 148
    :goto_0
    return-object v1

    :cond_6
    const-class v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-static {v1}, Ljava/util/EnumSet;->noneOf(Ljava/lang/Class;)Ljava/util/EnumSet;

    move-result-object v1

    goto :goto_0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 92
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;
    .locals 1

    .prologue
    .line 92
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->$VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;

    return-object v0
.end method
