.class public Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;
.super Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;
.source "AntPlusBikeSpdCadCommonPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultiDeviceSearchSpdCadResult"
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;",
            ">;"
        }
    .end annotation
.end field

.field private static final IPC_VERSION:I = 0x1


# instance fields
.field protected final mCadenceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 306
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult$1;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult$1;-><init>()V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->CREATOR:Landroid/os/Parcelable$Creator;

    return-void
.end method

.method public constructor <init>(ILcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V
    .locals 1
    .param p1, "resultID"    # I
    .param p2, "spdInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p3, "cadInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p4, "alreadyConnected"    # Z

    .prologue
    .line 236
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->BIKE_SPDCAD:Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    invoke-direct {p0, p1, v0, p2, p4}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;-><init>(ILcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    .line 237
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->mCadenceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 238
    return-void
.end method

.method protected constructor <init>(Landroid/os/Parcel;)V
    .locals 7
    .param p1, "source"    # Landroid/os/Parcel;

    .prologue
    const/4 v6, 0x1

    .line 283
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;-><init>(Landroid/os/Parcel;)V

    .line 284
    new-instance v2, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;

    invoke-direct {v2, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;-><init>(Landroid/os/Parcel;)V

    .line 286
    .local v2, "unpacker":Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    .line 289
    .local v1, "sourceIpcVersion":I
    new-instance v0, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;-><init>(Landroid/os/Parcel;)V

    .line 290
    .local v0, "innerunpack":Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;
    const-class v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {v3}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v3

    invoke-virtual {p1, v3}, Landroid/os/Parcel;->readParcelable(Ljava/lang/ClassLoader;)Landroid/os/Parcelable;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->mCadenceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 292
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->finish()V

    .line 295
    if-le v1, v6, :cond_0

    .line 297
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc;->access$000()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Decoding "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-class v5, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;

    invoke-virtual {v5}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " using version "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " decoder"

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    .line 302
    :cond_0
    invoke-virtual {v2}, Lcom/dsi/ant/plugins/utility/parcel/ParcelUnpacker;->finish()V

    .line 303
    return-void
.end method


# virtual methods
.method public isPreferredDevice()Z
    .locals 2

    .prologue
    .line 243
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->isPreferredForSpeed()Z

    move-result v0

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->isPreferredForCadence()Z

    move-result v1

    or-int/2addr v0, v1

    return v0
.end method

.method public isPreferredForCadence()Z
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->mCadenceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public isPreferredForSpeed()Z
    .locals 1

    .prologue
    .line 252
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->mInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v0, v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v0}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    return v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 3
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 267
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/pccbase/MultiDeviceSearch$MultiDeviceSearchResult;->writeToParcel(Landroid/os/Parcel;I)V

    .line 268
    new-instance v1, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;

    invoke-direct {v1, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;-><init>(Landroid/os/Parcel;)V

    .line 270
    .local v1, "packer":Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;
    const/4 v2, 0x1

    invoke-virtual {p1, v2}, Landroid/os/Parcel;->writeInt(I)V

    .line 274
    new-instance v0, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;

    invoke-direct {v0, p1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;-><init>(Landroid/os/Parcel;)V

    .line 275
    .local v0, "innerpacker":Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPlusBikeSpdCadCommonPcc$MultiDeviceSearchSpdCadResult;->mCadenceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {p1, v2, p2}, Landroid/os/Parcel;->writeParcelable(Landroid/os/Parcelable;I)V

    .line 276
    invoke-virtual {v0}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;->finish()V

    .line 278
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/utility/parcel/ParcelPacker;->finish()V

    .line 279
    return-void
.end method
