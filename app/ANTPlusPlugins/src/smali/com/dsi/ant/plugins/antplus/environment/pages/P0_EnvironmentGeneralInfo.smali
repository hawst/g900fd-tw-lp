.class public Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P0_EnvironmentGeneralInfo.java"


# instance fields
.field parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;)V
    .locals 0
    .param p1, "parentDevice"    # Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    .prologue
    .line 25
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 26
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;->parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    .line 27
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v3, 0x4

    .line 44
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;->parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    iget-boolean v0, v0, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->isPeriodSet:Z

    if-nez v0, :cond_0

    .line 46
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    aget-byte v0, v0, v3

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    and-int/lit8 v0, v0, 0x3

    packed-switch v0, :pswitch_data_0

    .line 55
    const-string v0, "EnvironmentDevice"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown transmission rate flags in trans info byte ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    aget-byte v2, v2, v3

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ") , defaulting to 1Hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 57
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;->parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    const v1, 0x8000

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->setPeriod(I)V

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 49
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;->parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    const v1, 0xffff

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->setPeriod(I)V

    goto :goto_0

    .line 52
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/environment/pages/P0_EnvironmentGeneralInfo;->parentDevice:Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;

    const/16 v1, 0x2000

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/environment/EnvironmentDevice;->setPeriod(I)V

    goto :goto_0

    .line 46
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getEventList()Ljava/util/List;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 38
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public onDropToSearch()V
    .locals 0

    .prologue
    .line 67
    return-void
.end method
