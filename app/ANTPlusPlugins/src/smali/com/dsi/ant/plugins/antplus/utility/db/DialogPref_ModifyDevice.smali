.class public final Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;
.super Landroid/preference/DialogPreference;
.source "DialogPref_ModifyDevice.java"


# static fields
.field private static final UNICODE_BLACK_STAR:Ljava/lang/String;


# instance fields
.field private mCheckBox_Preferred:Landroid/widget/CheckBox;

.field private final mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

.field private mEditText_Name:Landroid/widget/EditText;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/16 v0, 0x2605

    invoke-static {v0}, Ljava/lang/Character;->toChars(I)[C

    move-result-object v0

    invoke-static {v0}, Ljava/lang/String;->valueOf([C)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->UNICODE_BLACK_STAR:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "device"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 50
    const/4 v3, 0x0

    invoke-direct {p0, p1, v3}, Landroid/preference/DialogPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 54
    const/4 v3, 0x0

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setPersistent(Z)V

    .line 55
    const-string v3, "Modify Saved Device"

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setDialogTitle(Ljava/lang/CharSequence;)V

    .line 56
    const v3, 0x7f030007

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setDialogLayoutResource(I)V

    .line 59
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_3

    .line 60
    :cond_0
    const-string v2, "-no name saved-"

    .line 64
    .local v2, "title":Ljava/lang/String;
    :goto_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 65
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    sget-object v4, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->UNICODE_BLACK_STAR:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 68
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const/high16 v4, 0x20000000

    and-int/2addr v3, v4

    if-lez v3, :cond_4

    .line 70
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " (S&C)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 71
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    const v4, -0x20000001

    and-int v0, v3, v4

    .line 79
    .local v0, "devNum":I
    :goto_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "   ID <"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ">"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 80
    .local v1, "summary":Ljava/lang/String;
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 81
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", Preferred Device"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 85
    :cond_2
    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setTitle(Ljava/lang/CharSequence;)V

    .line 86
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setSummary(Ljava/lang/CharSequence;)V

    .line 87
    return-void

    .line 62
    .end local v0    # "devNum":I
    .end local v1    # "summary":Ljava/lang/String;
    .end local v2    # "title":Ljava/lang/String;
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    .restart local v2    # "title":Ljava/lang/String;
    goto/16 :goto_0

    .line 75
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .restart local v0    # "devNum":I
    goto :goto_1
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Landroid/widget/EditText;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mEditText_Name:Landroid/widget/EditText;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;)Landroid/widget/CheckBox;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mCheckBox_Preferred:Landroid/widget/CheckBox;

    return-object v0
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Ljava/lang/Object;)Z
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->callChangeListener(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method


# virtual methods
.method protected onBindDialogView(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 104
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onBindDialogView(Landroid/view/View;)V

    .line 105
    const v0, 0x7f060017

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/EditText;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mEditText_Name:Landroid/widget/EditText;

    .line 106
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mEditText_Name:Landroid/widget/EditText;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->visibleName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/EditText;->setText(Ljava/lang/CharSequence;)V

    .line 107
    const v0, 0x7f060018

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/CheckBox;

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mCheckBox_Preferred:Landroid/widget/CheckBox;

    .line 108
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mCheckBox_Preferred:Landroid/widget/CheckBox;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->mDevice:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->isPreferredDevice:Ljava/lang/Boolean;

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 109
    return-void
.end method

.method protected onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V
    .locals 1
    .param p1, "builder"    # Landroid/app/AlertDialog$Builder;

    .prologue
    .line 94
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->onPrepareDialogBuilder(Landroid/app/AlertDialog$Builder;)V

    .line 95
    const-string v0, "Save"

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 96
    const-string v0, "Cancel"

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNeutralButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 97
    const-string v0, "Forget Device"

    invoke-virtual {p1, v0, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 98
    return-void
.end method

.method protected showDialog(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Bundle;

    .prologue
    .line 115
    invoke-super {p0, p1}, Landroid/preference/DialogPreference;->showDialog(Landroid/os/Bundle;)V

    .line 118
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->getDialog()Landroid/app/Dialog;

    move-result-object v0

    check-cast v0, Landroid/app/AlertDialog;

    .line 121
    .local v0, "d":Landroid/app/AlertDialog;
    const/4 v1, -0x1

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;

    invoke-direct {v2, p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    const/4 v1, -0x2

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog;->getButton(I)Landroid/widget/Button;

    move-result-object v1

    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;

    invoke-direct {v2, p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice$2;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;Landroid/app/AlertDialog;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 196
    return-void
.end method
