.class public abstract Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;
.super Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;
.source "BaseRemoteControlSearchTask.java"


# instance fields
.field protected channelFailureOccurred:Z

.field protected mAudioModeRequested:Z

.field private mAudioPageReceived:Z

.field protected mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

.field protected mCapabilitiesDetermined:Z

.field protected mGenericModeRequested:Z

.field protected mModesMatchCapabilities:Z

.field protected mReceivedPageCounter:I

.field protected mVideoModeRequested:Z

.field private mVideoPageReceived:Z

.field protected searchResult:Lcom/dsi/ant/message/ChannelId;

.field protected searchResultRSSI:Ljava/lang/Integer;


# direct methods
.method public constructor <init>(Ljava/util/EnumSet;IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V
    .locals 7
    .param p2, "deviceNumber"    # I
    .param p3, "rfFreq"    # I
    .param p4, "period"    # I
    .param p5, "devType"    # I
    .param p6, "transType"    # I
    .param p7, "proximityThreshold"    # I
    .param p8, "resultReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;IIIIII",
            "Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    const/4 v6, 0x0

    .line 48
    new-instance v4, Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v4, p2, p5, p6}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    move-object v0, p0

    move v1, p3

    move v2, p4

    move v3, p7

    move-object v5, p8

    invoke-direct/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask;-><init>(IIILcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 50
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioModeRequested:Z

    .line 51
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoModeRequested:Z

    .line 52
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p1, v0}, Ljava/util/EnumSet;->contains(Ljava/lang/Object;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mGenericModeRequested:Z

    .line 53
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    .line 54
    iput v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mReceivedPageCounter:I

    .line 55
    iput-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    .line 57
    iput-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioPageReceived:Z

    .line 58
    iput-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoPageReceived:Z

    .line 59
    iput-boolean v6, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    .line 60
    return-void
.end method


# virtual methods
.method protected DetermineCapabilities(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 9
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const-wide/16 v1, 0x0

    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 64
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v3

    aget-byte v6, v3, v7

    .line 65
    .local v6, "pageNumber":B
    sparse-switch v6, :sswitch_data_0

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 69
    :sswitch_0
    new-instance v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;-><init>()V

    .local v0, "tempPage":Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;
    move-wide v3, v1

    move-object v5, p1

    .line 70
    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 71
    iget-object v1, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    .line 72
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    .line 76
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioModeRequested:Z

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getAudioControlSupported()Z

    move-result v2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoModeRequested:Z

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getVideoControlSupported()Z

    move-result v2

    if-ne v1, v2, :cond_2

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mGenericModeRequested:Z

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->getGenericControlSupported()Z

    move-result v2

    if-ne v1, v2, :cond_2

    .line 80
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    .line 115
    .end local v0    # "tempPage":Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;
    :cond_1
    :goto_1
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    if-nez v1, :cond_0

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mReceivedPageCounter:I

    const/16 v2, 0x8

    if-le v1, v2, :cond_0

    .line 119
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioModeRequested:Z

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioPageReceived:Z

    if-ne v1, v2, :cond_3

    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoModeRequested:Z

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoPageReceived:Z

    if-ne v1, v2, :cond_3

    .line 121
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    .line 122
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioPageReceived:Z

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setAudioControlSupported(Z)V

    .line 123
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoPageReceived:Z

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setVideoControlSupported(Z)V

    .line 124
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilities:Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;

    invoke-virtual {v1, v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$ControlDeviceCapabilities;->setGenericControlSupported(Z)V

    .line 131
    :goto_2
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    goto :goto_0

    .line 84
    .restart local v0    # "tempPage":Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;
    :cond_2
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    goto :goto_1

    .line 89
    .end local v0    # "tempPage":Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;
    :sswitch_1
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioPageReceived:Z

    .line 91
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mAudioModeRequested:Z

    if-nez v1, :cond_1

    .line 93
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    .line 94
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    goto :goto_0

    .line 100
    :sswitch_2
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoPageReceived:Z

    .line 102
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mVideoModeRequested:Z

    if-nez v1, :cond_1

    .line 104
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    .line 105
    iput-boolean v7, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mCapabilitiesDetermined:Z

    goto :goto_0

    .line 128
    :cond_3
    iput-boolean v8, p0, Lcom/dsi/ant/plugins/antplus/controls/tasks/BaseRemoteControlSearchTask;->mModesMatchCapabilities:Z

    goto :goto_2

    .line 65
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x7 -> :sswitch_2
    .end sparse-switch
.end method
