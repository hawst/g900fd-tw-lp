.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;
.super Ljava/lang/Object;
.source "AntPluginService.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

.field final synthetic val$msgr_ResultMessenger:Landroid/os/Messenger;

.field final synthetic val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;)V
    .locals 0

    .prologue
    .line 789
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExecutorDeath()V
    .locals 4

    .prologue
    .line 793
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v2

    const-string v3, "Plugin async controller scan failed: executor died"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 794
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 795
    .local v0, "response":Landroid/os/Message;
    const/4 v2, -0x4

    iput v2, v0, Landroid/os/Message;->what:I

    .line 796
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->val$prospectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v2, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    .line 797
    .local v1, "si":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    if-nez v1, :cond_0

    .line 798
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->val$msgr_ResultMessenger:Landroid/os/Messenger;

    invoke-virtual {v2, v3, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 801
    :goto_0
    return-void

    .line 800
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$4;->this$0:Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->getCurrentResultHandler()Landroid/os/Messenger;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0
.end method
