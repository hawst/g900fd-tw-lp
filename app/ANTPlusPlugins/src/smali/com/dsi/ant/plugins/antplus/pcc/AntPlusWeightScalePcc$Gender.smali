.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
.super Ljava/lang/Enum;
.source "AntPlusWeightScalePcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "Gender"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

.field public static final enum FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

.field public static final INTVALUE_FEMALE:I = 0x0

.field public static final INTVALUE_MALE:I = 0x1

.field public static final INTVALUE_UNASSIGNED:I = -0x1

.field public static final enum MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

.field public static final enum UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 396
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 400
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 404
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    const-string v1, "UNASSIGNED"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 391
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 391
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    .locals 2
    .param p0, "intValue"    # I

    .prologue
    .line 445
    packed-switch p0, :pswitch_data_0

    .line 454
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Undefined gender value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 448
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->FEMALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    .line 452
    :goto_0
    return-object v0

    .line 450
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->MALE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_0

    .line 452
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->UNASSIGNED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    goto :goto_0

    .line 445
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 391
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;
    .locals 1

    .prologue
    .line 391
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 2

    .prologue
    .line 425
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$AntPlusWeightScalePcc$Gender:[I

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$Gender;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 434
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Undefined gender value"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 428
    :pswitch_0
    const/4 v0, 0x0

    .line 432
    :goto_0
    return v0

    .line 430
    :pswitch_1
    const/4 v0, 0x1

    goto :goto_0

    .line 432
    :pswitch_2
    const/4 v0, -0x1

    goto :goto_0

    .line 425
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method
