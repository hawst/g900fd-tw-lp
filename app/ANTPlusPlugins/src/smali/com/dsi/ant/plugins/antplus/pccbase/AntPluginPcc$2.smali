.class Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;
.super Ljava/lang/Object;
.source "AntPluginPcc.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)V
    .locals 0

    .prologue
    .line 690
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 5
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v4, 0x1

    .line 694
    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$000()Ljava/lang/String;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Plugin Msg Handler received: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 695
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$200(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->tryLock()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 700
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    invoke-virtual {v1, p1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleNonCmdPluginMessage(Landroid/os/Message;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 704
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$200(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    .line 726
    :goto_0
    return v4

    .line 704
    :catchall_0
    move-exception v1

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    # getter for: Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommLock:Ljava/util/concurrent/locks/ReentrantLock;
    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->access$200(Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;)Ljava/util/concurrent/locks/ReentrantLock;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/locks/ReentrantLock;->unlock()V

    throw v1

    .line 713
    :cond_0
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommMsgExch:Ljava/util/concurrent/Exchanger;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;)Ljava/lang/Object;

    .line 714
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->mPluginCommProcessingBarrier:Ljava/util/concurrent/CyclicBarrier;

    invoke-virtual {v1}, Ljava/util/concurrent/CyclicBarrier;->await()I
    :try_end_1
    .catch Ljava/util/concurrent/BrokenBarrierException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    .line 715
    :catch_0
    move-exception v0

    .line 717
    .local v0, "e":Ljava/util/concurrent/BrokenBarrierException;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "BrokenBarrierException in mPluginMsgHandler trying to fwd message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    goto :goto_0

    .line 719
    .end local v0    # "e":Ljava/util/concurrent/BrokenBarrierException;
    :catch_1
    move-exception v0

    .line 721
    .local v0, "e":Ljava/lang/InterruptedException;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$2;->this$0:Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "InterruptedException in mPluginMsgHandler trying to fwd message "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p1, Landroid/os/Message;->what:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;->handleConnectionBroke(Ljava/lang/String;)V

    .line 722
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method
