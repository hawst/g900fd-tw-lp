.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;
.super Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;
.source "AntPlusGenericRemoteControlPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mGenericCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 27
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 159
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;-><init>()V

    .line 78
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 159
    return-void
.end method

.method public static requestAccessByDeviceNumber(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 7
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "antDeviceNumber"    # I
    .param p3, "searchProximityThreshold"    # I
    .param p5, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "II",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    .local p4, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;>;"
    new-instance v6, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;

    invoke-direct {v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;-><init>()V

    .local v6, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;
    move-object v0, p0

    move-object v1, p1

    move v2, p2

    move v3, p3

    move-object v4, p4

    move-object v5, p5

    .line 155
    invoke-static/range {v0 .. v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;IILcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method public static requestRemoteControlAsyncScanController(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;
    .locals 2
    .param p1, "bindToContext"    # Landroid/content/Context;
    .param p2, "searchProximityThreshold"    # I
    .param p3, "scanResultReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;",
            "Landroid/content/Context;",
            "I",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 106
    .local p0, "requestModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;-><init>()V

    .line 108
    .local v0, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;
    invoke-static {p0, p1, p2, v0, p3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->requestAccessRemoteControl_Helper(Ljava/util/EnumSet;Landroid/content/Context;ILcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$IRemoteControlAsyncScanResultReceiver;)Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc$RemoteControlAsyncScanController;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public RequestGenericCommand(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;)V
    .locals 6
    .param p1, "genericCommandFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;
    .param p2, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

    .prologue
    .line 199
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 201
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestGenericCommand failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 231
    :goto_0
    return-void

    .line 204
    :cond_0
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mGenericCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;

    .line 206
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 207
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e21

    iput v3, v0, Landroid/os/Message;->what:I

    .line 208
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 209
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 211
    const-string v3, "int_commandNumber"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 213
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 215
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 217
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestGenericCommand died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 218
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 222
    :cond_1
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    .line 225
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd requestGenericCommand failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 226
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 227
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "requestGenericCommand cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 230
    :cond_2
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method public RequestGenericCommandSequence([B)V
    .locals 6
    .param p1, "rawCommandBytes"    # [B

    .prologue
    .line 239
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 241
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestGenericCommandSequence failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    :goto_0
    return-void

    .line 245
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 246
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e22

    iput v3, v0, Landroid/os/Message;->what:I

    .line 247
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 248
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 250
    const-string v3, "arrayByte_rawCommandBytes"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putByteArray(Ljava/lang/String;[B)V

    .line 252
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 254
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 256
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd RequestGenericCommandSequence died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 257
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 261
    :cond_1
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    .line 264
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd RequestGenericCommandSequence failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 265
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    .line 266
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "RequestGenericCommandSequence cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 269
    :cond_2
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 164
    const-string v0, "ANT+ Plugin: Generic Remote Control"

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 7
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 170
    iget v5, p1, Landroid/os/Message;->arg1:I

    packed-switch v5, :pswitch_data_0

    .line 188
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseRemoteControlPcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 191
    :cond_0
    :goto_0
    return-void

    .line 174
    :pswitch_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mGenericCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;

    if-eqz v5, :cond_0

    .line 177
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v5}, Ljava/util/concurrent/Semaphore;->release()V

    .line 179
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 180
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 181
    .local v1, "estTimestamp":J
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v5

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 182
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v5, "int_requestStatus"

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;

    move-result-object v4

    .line 183
    .local v4, "status":Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc;->mGenericCommandFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;

    invoke-interface {v5, v1, v2, v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusGenericRemoteControlPcc$IGenericCommandFinishedReceiver;->onGenericCommandFinished(JLjava/util/EnumSet;Lcom/dsi/ant/plugins/antplus/pcc/defines/RequestStatus;)V

    goto :goto_0

    .line 170
    :pswitch_data_0
    .packed-switch 0xd0
        :pswitch_0
    .end packed-switch
.end method
