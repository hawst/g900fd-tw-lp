.class Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;
.super Ljava/lang/Object;
.source "AntFsHostSession.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsRunnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->requestDownload(ILcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsTransferProgressReceiver;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

.field final synthetic val$downloadTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->val$downloadTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    .locals 3
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 237
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->val$downloadTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    # invokes: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->runSubTask(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$700(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    move-result-object v0

    .line 239
    .local v0, "result":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->val$downloadTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->getDownloadResult()Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsDownloadResult;

    if-ne v1, v2, :cond_0

    .line 240
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->this$0:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$3;->val$downloadTask:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostDownloadChannelSubTask;->getDownloadData()[B

    move-result-object v2

    # setter for: Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->downloadedDataBuffer:[B
    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;->access$902(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession;[B)[B

    .line 241
    :cond_0
    return-object v0
.end method
