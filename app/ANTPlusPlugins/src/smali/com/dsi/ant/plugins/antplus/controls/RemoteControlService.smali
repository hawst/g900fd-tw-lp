.class public Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
.super Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.source "RemoteControlService.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

.field mProspectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

.field mRequestMode:I

.field mRequestedControlsModes:Ljava/util/EnumSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/EnumSet",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;",
            ">;"
        }
    .end annotation
.end field

.field mResultMessenger:Landroid/os/Messenger;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 33
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$300(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$400(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Ljava/util/UUID;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
    .param p1, "x1"    # Ljava/util/UUID;

    .prologue
    .line 33
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    return-void
.end method

.method static synthetic access$500(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 1
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;
    .param p1, "x1"    # I
    .param p2, "x2"    # Ljava/lang/String;

    .prologue
    .line 33
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->getDeviceInfoById(ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public createNewDeviceFromSearchResults(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 7
    .param p1, "connectedChannel"    # Lcom/dsi/ant/channel/AntChannel;
    .param p2, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .prologue
    .line 60
    invoke-static {p0}, Lcom/dsi/ant/plugins/utility/uuid/UniqueIdGenerator;->getTwoByteUniqueId(Landroid/content/Context;)I

    move-result v3

    .line 65
    .local v3, "serialNumber":I
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    if-nez v4, :cond_0

    .line 69
    :try_start_0
    new-instance v4, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    invoke-direct {v4, p2, v3, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;ILcom/dsi/ant/channel/AntChannel;)V

    iput-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;
    :try_end_0
    .catch Ljava/nio/channels/ClosedChannelException; {:try_start_0 .. :try_end_0} :catch_0

    .line 77
    :cond_0
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mRequestedControlsModes:Ljava/util/EnumSet;

    invoke-virtual {v4}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 79
    .local v2, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mProspectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v4, v2, v5}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->enableMode(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)V

    goto :goto_0

    .line 71
    .end local v1    # "i$":Ljava/util/Iterator;
    .end local v2    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    :catch_0
    move-exception v0

    .line 73
    .local v0, "e":Ljava/nio/channels/ClosedChannelException;
    const/4 v4, 0x0

    .line 84
    .end local v0    # "e":Ljava/nio/channels/ClosedChannelException;
    :goto_1
    return-object v4

    .line 82
    .restart local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "remote control device made with token "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mProspectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    goto :goto_1
.end method

.method protected getAlreadyConnectedDevice(ILjava/lang/String;)Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .locals 4
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "appNamePkg"    # Ljava/lang/String;

    .prologue
    .line 324
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mConnectedDevices:Ljava/util/ArrayList;

    monitor-enter v3

    .line 326
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 328
    .local v0, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    if-eqz p1, :cond_1

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    iget-object v2, v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;->antDeviceNumber:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ne v2, p1, :cond_0

    .line 333
    :cond_1
    if-eqz p2, :cond_2

    invoke-virtual {v0, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 336
    :cond_2
    monitor-exit v3

    .line 340
    .end local v0    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    :goto_0
    return-object v0

    .line 339
    :cond_3
    monitor-exit v3

    .line 340
    const/4 v0, 0x0

    goto :goto_0

    .line 339
    .end local v1    # "i$":Ljava/util/Iterator;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method

.method public getPluginDeviceSearchParamBundle()Landroid/os/Bundle;
    .locals 3

    .prologue
    .line 46
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 47
    .local v0, "deviceParams":Landroid/os/Bundle;
    const-string v1, "str_PluginName"

    const-string v2, "Controllable Devices"

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 48
    const-string v1, "predefinednetwork_NetKey"

    sget-object v2, Lcom/dsi/ant/channel/PredefinedNetwork;->ANT_PLUS:Lcom/dsi/ant/channel/PredefinedNetwork;

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 49
    const-string v1, "int_DevType"

    const/16 v2, 0x10

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 50
    const-string v1, "int_TransType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 51
    const-string v1, "int_Period"

    const/16 v2, 0x2000

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 52
    const-string v1, "int_RfFreq"

    const/16 v2, 0x39

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 53
    return-object v0
.end method

.method public handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z
    .locals 5
    .param p1, "requestMode"    # I
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 96
    iput p1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mRequestMode:I

    .line 97
    iput-object p3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mProspectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 98
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mProspectiveClient:Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v4

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 99
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mResultMessenger:Landroid/os/Messenger;

    .line 100
    const-string v3, "long_ControlsModes"

    invoke-virtual {p4, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v3

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getControlsModesFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mRequestedControlsModes:Ljava/util/EnumSet;

    .line 102
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mRequestedControlsModes:Ljava/util/EnumSet;

    invoke-virtual {v3}, Ljava/util/EnumSet;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .line 104
    .local v1, "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    invoke-static {v1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->isModeValid(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 105
    const/4 v3, 0x0

    .line 117
    .end local v1    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    :goto_0
    return v3

    .line 108
    .restart local v1    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    :cond_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->activeDevice:Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    invoke-virtual {v3, v1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->isModeInUse(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 110
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v2

    .line 111
    .local v2, "response":Landroid/os/Message;
    const/4 v3, -0x6

    iput v3, v2, Landroid/os/Message;->what:I

    .line 112
    invoke-virtual {p0, p2, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 113
    const/4 v3, 0x1

    goto :goto_0

    .line 117
    .end local v1    # "mode":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .end local v2    # "response":Landroid/os/Message;
    :cond_2
    invoke-super {p0, p1, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->handleAccessRequest(ILandroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Bundle;)Z

    move-result v3

    goto :goto_0
.end method

.method protected handleAsyncSearchControllerRequest(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 31
    .param p1, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p2, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p3, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 126
    invoke-static {}, Ljava/util/UUID;->randomUUID()Ljava/util/UUID;

    move-result-object v11

    move-object/from16 v0, p1

    iput-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    .line 127
    new-instance v27, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;

    move-object/from16 v0, v27

    move-object/from16 v1, p1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;-><init>(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 128
    .local v27, "si":Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;
    move-object/from16 v0, p2

    move-object/from16 v1, v27

    iput-object v0, v1, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->currentResultHandler:Landroid/os/Messenger;

    .line 129
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mToken_AsyncScanList:Ljava/util/concurrent/ConcurrentHashMap;

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v28, v0

    move-object/from16 v0, v28

    move-object/from16 v1, v27

    invoke-virtual {v11, v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v19

    .line 133
    .local v19, "initMsg":Landroid/os/Message;
    const/4 v11, 0x0

    move-object/from16 v0, v19

    iput v11, v0, Landroid/os/Message;->what:I

    .line 134
    new-instance v26, Landroid/os/Bundle;

    invoke-direct/range {v26 .. v26}, Landroid/os/Bundle;-><init>()V

    .line 135
    .local v26, "retInfo":Landroid/os/Bundle;
    const-string v11, "uuid_AccessToken"

    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v28, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v11, v1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 136
    const-string v11, "msgr_PluginComm"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mPccMsgHandler:Landroid/os/Messenger;

    move-object/from16 v28, v0

    move-object/from16 v0, v26

    move-object/from16 v1, v28

    invoke-virtual {v0, v11, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 137
    move-object/from16 v0, v19

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 140
    :try_start_0
    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 149
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v15

    .line 150
    .local v15, "deviceParams":Landroid/os/Bundle;
    const-string v11, "int_DevType"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 151
    .local v8, "devType":I
    const-string v11, "int_TransType"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 152
    .local v9, "transType":I
    const-string v11, "int_Period"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 153
    .local v7, "period":I
    const-string v11, "int_RfFreq"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v6

    .line 154
    .local v6, "rfFreq":I
    const-string v11, "predefinednetwork_NetKey"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v21

    check-cast v21, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 155
    .local v21, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    const-string v11, "int_ProximityBin"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 156
    .local v10, "proxThreshold":I
    const-string v11, "str_PluginName"

    invoke-virtual {v15, v11}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v23

    .line 158
    .local v23, "pluginName":Ljava/lang/String;
    if-ltz v10, :cond_0

    const/16 v11, 0xa

    if-le v10, v11, :cond_1

    .line 161
    :cond_0
    sget-object v11, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    new-instance v28, Ljava/lang/StringBuilder;

    invoke-direct/range {v28 .. v28}, Ljava/lang/StringBuilder;-><init>()V

    const-string v29, "Proximity threshold out of range, value: "

    invoke-virtual/range {v28 .. v29}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v28

    invoke-virtual/range {v28 .. v28}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v28

    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v24

    .line 163
    .local v24, "response":Landroid/os/Message;
    const/16 v11, -0x9

    move-object/from16 v0, v24

    iput v11, v0, Landroid/os/Message;->what:I

    .line 164
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, v24

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 317
    .end local v6    # "rfFreq":I
    .end local v7    # "period":I
    .end local v8    # "devType":I
    .end local v9    # "transType":I
    .end local v10    # "proxThreshold":I
    .end local v15    # "deviceParams":Landroid/os/Bundle;
    .end local v21    # "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    .end local v23    # "pluginName":Ljava/lang/String;
    .end local v24    # "response":Landroid/os/Message;
    :goto_0
    return-void

    .line 141
    :catch_0
    move-exception v16

    .line 143
    .local v16, "e":Landroid/os/RemoteException;
    sget-object v11, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    const-string v28, "RemoteException sending async scan init info."

    move-object/from16 v0, v28

    invoke-static {v11, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 144
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto :goto_0

    .line 169
    .end local v16    # "e":Landroid/os/RemoteException;
    .restart local v6    # "rfFreq":I
    .restart local v7    # "period":I
    .restart local v8    # "devType":I
    .restart local v9    # "transType":I
    .restart local v10    # "proxThreshold":I
    .restart local v15    # "deviceParams":Landroid/os/Bundle;
    .restart local v21    # "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    .restart local v23    # "pluginName":Ljava/lang/String;
    :cond_1
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mConnectedDevices:Ljava/util/ArrayList;

    move-object/from16 v28, v0

    monitor-enter v28

    .line 171
    :try_start_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mConnectedDevices:Ljava/util/ArrayList;

    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v20

    .line 172
    .local v20, "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_2
    :goto_1
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->hasNext()Z

    move-result v11

    if-eqz v11, :cond_3

    .line 174
    invoke-interface/range {v20 .. v20}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v18

    check-cast v18, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;

    .line 177
    .local v18, "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->appNamePkg:Ljava/lang/String;

    move-object/from16 v0, v18

    invoke-virtual {v0, v11}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->hasAccess(Ljava/lang/String;)Z

    move-result v11

    if-nez v11, :cond_2

    .line 180
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v25

    .line 181
    .local v25, "resultMsg":Landroid/os/Message;
    const/4 v11, 0x2

    move-object/from16 v0, v25

    iput v11, v0, Landroid/os/Message;->what:I

    .line 182
    new-instance v13, Landroid/os/Bundle;

    invoke-direct {v13}, Landroid/os/Bundle;-><init>()V

    .line 184
    .local v13, "b":Landroid/os/Bundle;
    new-instance v22, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;

    const/4 v11, 0x0

    move-object/from16 v0, v18

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;->deviceInfo:Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    move-object/from16 v29, v0

    const/16 v30, 0x1

    move-object/from16 v0, v22

    move-object/from16 v1, v29

    move/from16 v2, v30

    invoke-direct {v0, v11, v1, v2}, Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;-><init>(Ljava/util/UUID;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Z)V

    .line 189
    .local v22, "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    const-string v11, "parcelable_AsyncScanResultDeviceInfo"

    move-object/from16 v0, v22

    invoke-virtual {v13, v11, v0}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 190
    move-object/from16 v0, v25

    invoke-virtual {v0, v13}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 193
    :try_start_2
    move-object/from16 v0, p2

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_2
    .catch Landroid/os/RemoteException; {:try_start_2 .. :try_end_2} :catch_1
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_1

    .line 194
    :catch_1
    move-exception v16

    .line 196
    .restart local v16    # "e":Landroid/os/RemoteException;
    :try_start_3
    sget-object v11, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    const-string v29, "RemoteException sending async scan already connected devices, closing scan."

    move-object/from16 v0, v29

    invoke-static {v11, v0}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 197
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    .line 198
    monitor-exit v28

    goto/16 :goto_0

    .line 201
    .end local v13    # "b":Landroid/os/Bundle;
    .end local v16    # "e":Landroid/os/RemoteException;
    .end local v18    # "i":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;
    .end local v20    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    .end local v22    # "newResult":Lcom/dsi/ant/plugins/antplus/pccbase/AsyncScanController$AsyncScanResultDeviceInfo;
    .end local v25    # "resultMsg":Landroid/os/Message;
    :catchall_0
    move-exception v11

    monitor-exit v28
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    throw v11

    .restart local v20    # "it":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice;>;"
    :cond_3
    :try_start_4
    monitor-exit v28
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 205
    const/4 v11, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v21

    move-object/from16 v2, p2

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v11, v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v12

    .line 206
    .local v12, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-nez v12, :cond_4

    .line 208
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0

    .line 213
    :cond_4
    new-instance v14, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$1;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v14, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$1;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;)V

    .line 231
    .local v14, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_5
    new-instance v11, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-direct {v11, v12, v14}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V

    move-object/from16 v0, v27

    iput-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_5
    .catch Landroid/os/RemoteException; {:try_start_5 .. :try_end_5} :catch_2

    .line 239
    const-string v11, "long_ControlsModes"

    move-object/from16 v0, p3

    invoke-virtual {v0, v11}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v28

    invoke-static/range {v28 .. v29}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getControlsModesFromLong(J)Ljava/util/EnumSet;

    move-result-object v5

    .line 242
    .local v5, "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    new-instance v4, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;

    new-instance v11, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$2;

    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p1

    invoke-direct {v11, v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$2;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    invoke-direct/range {v4 .. v11}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;-><init>(Ljava/util/EnumSet;IIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 316
    .local v4, "scanTask":Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;
    move-object/from16 v0, v27

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$AsyncScanInfo;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    invoke-virtual {v11, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->setIdleTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;)V

    goto/16 :goto_0

    .line 232
    .end local v4    # "scanTask":Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlScannerTask;
    .end local v5    # "controlsModes":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;>;"
    :catch_2
    move-exception v17

    .line 234
    .local v17, "e1":Landroid/os/RemoteException;
    invoke-interface {v14}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    .line 235
    move-object/from16 v0, p1

    iget-object v11, v0, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->shutdownAndRemoveAsyncSearch(Ljava/util/UUID;)V

    goto/16 :goto_0
.end method

.method public startSearchByAntDeviceNumber(ILcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Messenger;Landroid/os/Bundle;)V
    .locals 21
    .param p1, "targetAntDeviceNumber"    # I
    .param p2, "prospectiveClient"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "msgr_ResultMessenger"    # Landroid/os/Messenger;
    .param p4, "reqParams"    # Landroid/os/Bundle;

    .prologue
    .line 349
    invoke-virtual/range {p0 .. p0}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->getPluginDeviceSearchParamBundle()Landroid/os/Bundle;

    move-result-object v15

    .line 351
    .local v15, "deviceParams":Landroid/os/Bundle;
    const-string v5, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "long_ControlsModes"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "predefinednetwork_NetKey"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_DevType"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_TransType"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_Period"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    const-string v5, "int_RfFreq"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_2

    .line 359
    :cond_0
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    const-string v6, "Bundle is missing parameters"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 360
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v20

    .line 361
    .local v20, "response":Landroid/os/Message;
    const/4 v5, -0x4

    move-object/from16 v0, v20

    iput v5, v0, Landroid/os/Message;->what:I

    .line 362
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    .line 476
    .end local v20    # "response":Landroid/os/Message;
    :cond_1
    :goto_0
    return-void

    .line 367
    :cond_2
    const-string v5, "predefinednetwork_NetKey"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v19

    check-cast v19, Lcom/dsi/ant/channel/PredefinedNetwork;

    .line 368
    .local v19, "netKey":Lcom/dsi/ant/channel/PredefinedNetwork;
    const/4 v5, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-virtual {v0, v1, v5, v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->acquireChannel_helper(Lcom/dsi/ant/channel/PredefinedNetwork;Lcom/dsi/ant/channel/Capabilities;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)Lcom/dsi/ant/channel/AntChannel;

    move-result-object v13

    .line 369
    .local v13, "antChannel":Lcom/dsi/ant/channel/AntChannel;
    if-eqz v13, :cond_1

    .line 372
    const-string v5, "int_DevType"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v9

    .line 373
    .local v9, "devType":I
    const-string v5, "int_TransType"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v10

    .line 374
    .local v10, "transType":I
    const-string v5, "int_Period"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    .line 375
    .local v8, "period":I
    const-string v5, "int_RfFreq"

    invoke-virtual {v15, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 376
    .local v7, "rfFreq":I
    if-eqz p1, :cond_4

    const/4 v11, 0x0

    .line 379
    .local v11, "proximityThreshold":I
    :goto_1
    if-ltz v11, :cond_3

    const/16 v5, 0xa

    if-le v11, v5, :cond_5

    .line 382
    :cond_3
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Proximity threshold out of range, value: "

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v20

    .line 384
    .restart local v20    # "response":Landroid/os/Message;
    const/16 v5, -0x9

    move-object/from16 v0, v20

    iput v5, v0, Landroid/os/Message;->what:I

    .line 385
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 376
    .end local v11    # "proximityThreshold":I
    .end local v20    # "response":Landroid/os/Message;
    :cond_4
    const-string v5, "int_ProximityBin"

    move-object/from16 v0, p4

    invoke-virtual {v0, v5}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v11

    goto :goto_1

    .line 389
    .restart local v11    # "proximityThreshold":I
    :cond_5
    new-instance v14, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p3

    invoke-direct {v14, v0, v1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$3;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Landroid/os/Messenger;)V

    .line 403
    .local v14, "deathHandler":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;
    :try_start_0
    new-instance v18, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-object/from16 v0, v18

    invoke-direct {v0, v13, v14}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;-><init>(Lcom/dsi/ant/channel/AntChannel;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_1

    .line 410
    .local v18, "executor":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    new-instance v4, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->mRequestedControlsModes:Ljava/util/EnumSet;

    new-instance v12, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, p3

    move-object/from16 v3, p2

    invoke-direct {v12, v0, v1, v2, v3}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService$4;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;Landroid/os/Messenger;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    move/from16 v6, p1

    invoke-direct/range {v4 .. v12}, Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;-><init>(Ljava/util/EnumSet;IIIIIILcom/dsi/ant/plugins/antplus/utility/search/AbstractSearchControllerTask$SearchResultReceiver;)V

    .line 467
    .local v4, "scanTask":Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;
    const/4 v5, 0x0

    :try_start_1
    move-object/from16 v0, v18

    invoke-virtual {v0, v4, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 468
    :catch_0
    move-exception v16

    .line 470
    .local v16, "e":Ljava/lang/InterruptedException;
    const/4 v5, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v5}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 471
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v20

    .line 472
    .restart local v20    # "response":Landroid/os/Message;
    sget-object v5, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->TAG:Ljava/lang/String;

    const-string v6, "Plugin search by deviceNumber failed to start task on executor"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 473
    const/4 v5, -0x4

    move-object/from16 v0, v20

    iput v5, v0, Landroid/os/Message;->what:I

    .line 474
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 404
    .end local v4    # "scanTask":Lcom/dsi/ant/plugins/antplus/controls/tasks/RemoteControlSingleSearchControllerTask;
    .end local v16    # "e":Ljava/lang/InterruptedException;
    .end local v18    # "executor":Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    .end local v20    # "response":Landroid/os/Message;
    :catch_1
    move-exception v17

    .line 406
    .local v17, "e1":Landroid/os/RemoteException;
    invoke-interface {v14}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;->onExecutorDeath()V

    goto/16 :goto_0
.end method
