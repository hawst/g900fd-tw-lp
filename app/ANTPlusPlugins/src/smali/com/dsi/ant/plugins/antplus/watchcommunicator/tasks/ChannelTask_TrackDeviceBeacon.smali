.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;
.super Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;
.source "ChannelTask_TrackDeviceBeacon.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)V
    .locals 0
    .param p1, "statusReceiver"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;
    .param p2, "deviceToFind"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 28
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostTaskBase;-><init>(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$IAntFsStateReceiver;)V

    .line 29
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .line 30
    return-void
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 69
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->flushAndEnsureUnassignedChannel()V

    .line 71
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/message/ChannelType;->BIDIRECTIONAL_SLAVE:Lcom/dsi/ant/message/ChannelType;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->assign(Lcom/dsi/ant/message/ChannelType;)V

    .line 72
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkRfFreq:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setRfFrequency(I)V

    .line 73
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkPeriod:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setPeriod(I)V

    .line 74
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;)V

    .line 75
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/message/ChannelId;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v3}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v3

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v4}, Lcom/dsi/ant/message/ChannelId;->getDeviceType()I

    move-result v4

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->devToFind:Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v5}, Lcom/dsi/ant/message/ChannelId;->getTransmissionType()I

    move-result v5

    invoke-direct {v2, v3, v4, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 78
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 84
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->SUCCESS:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1

    .line 104
    :goto_0
    return-void

    .line 93
    :catch_0
    move-exception v0

    .line 95
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE occurred: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_OTHER_DEVICE_COMMUNICATION_ERROR:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    goto :goto_0

    .line 98
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 100
    .local v0, "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 101
    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;->FAIL_EXECUTOR_CANCELLED_TASK:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/tasks/ChannelTask_TrackDeviceBeacon;->setTaskResult(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsRequestResult;)V

    .line 102
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 109
    const-string v0, "WatchCommunicator Track Device"

    return-object v0
.end method

.method public isAcceptableStartState(Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;)Z
    .locals 1
    .param p1, "state"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    .prologue
    .line 115
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;->NOT_CONNECTED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsHostSession$AntFsHostState;

    if-eq p1, v0, :cond_0

    .line 116
    const/4 v0, 0x0

    .line 118
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 0
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 61
    return-void
.end method
