.class Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase$DbHelper;
.super Landroid/database/sqlite/SQLiteOpenHelper;
.source "BloodPressureDownloadDatabase.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "DbHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 36
    const-string v0, "bpm_download.db"

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-direct {p0, p1, v0, v1, v2}, Landroid/database/sqlite/SQLiteOpenHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase$CursorFactory;I)V

    .line 37
    return-void
.end method


# virtual methods
.method public onCreate(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 2
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 52
    # getter for: Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/bloodpressure/BloodPressureDownloadDatabase;->access$000()Ljava/lang/String;

    move-result-object v0

    const-string v1, "Creating blood pressure ANTFS downloads database"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 53
    const-string v0, "PRAGMA foreign_keys = ON"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 55
    const-string v0, "CREATE TABLE AntFsDeviceInfo(AntFsDeviceInfo_Id INTEGER PRIMARY KEY,Passkey BLOB,AntFsSerialNumber INTEGER,AntFsManufacturerId INTEGER NOT NULL,AntFsDeviceType INTEGER NOT NULL,AntDeviceNumber INTEGER NOT NULL,UNIQUE (AntFsManufacturerId, AntFsDeviceType, AntFsSerialNumber, AntDeviceNumber))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 66
    const-string v0, "CREATE TABLE Applications(App_Id INTEGER PRIMARY KEY,AppPkgName STRING UNIQUE NOT NULL)"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "CREATE TABLE LastDownloadRecords(App_Id INTEGER REFERENCES Applications (App_Id),AntFsDeviceInfo_Id INTEGER REFERENCES AntFsDeviceInfo (AntFsDeviceInfo_Id),LastDownloadedGarminTime INTEGER DEFAULT 0,LastDeliveredMeasurementGarminTimestamp INTEGER DEFAULT 0,UNIQUE (App_Id, AntFsDeviceInfo_Id))"

    invoke-virtual {p1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 80
    return-void
.end method

.method public onUpgrade(Landroid/database/sqlite/SQLiteDatabase;II)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "oldVersion"    # I
    .param p3, "newVersion"    # I

    .prologue
    .line 45
    return-void
.end method
