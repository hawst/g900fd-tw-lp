.class public Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;
.super Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;
.source "AntPlusWatchDownloaderPcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$1;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;,
        Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;

.field mCommandLock:Ljava/util/concurrent/Semaphore;

.field mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;",
            ">;"
        }
    .end annotation
.end field

.field singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 2

    .prologue
    .line 405
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;-><init>()V

    .line 362
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;

    .line 364
    new-instance v0, Ljava/util/concurrent/Semaphore;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Ljava/util/concurrent/Semaphore;-><init>(I)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    .line 405
    return-void
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private handleDownloadEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;ILandroid/os/Bundle;)V
    .locals 9
    .param p1, "requestInfo"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;
    .param p2, "eventCode"    # I
    .param p3, "data"    # Landroid/os/Bundle;

    .prologue
    .line 479
    sparse-switch p2, :sswitch_data_0

    .line 507
    :cond_0
    :goto_0
    return-void

    .line 482
    :sswitch_0
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->downloadActivitiesFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;

    if-eqz v0, :cond_0

    .line 484
    const-string v0, "int_statusCode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;

    move-result-object v7

    .line 485
    .local v7, "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->downloadActivitiesFinishedReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;

    invoke-interface {v0, v7}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;->onNewDownloadActivitiesFinished(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;)V

    goto :goto_0

    .line 489
    .end local v7    # "statusCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsRequestStatus;
    :sswitch_1
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->fitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    if-eqz v0, :cond_0

    .line 491
    const-string v0, "arrayByte_rawFileBytes"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getByteArray(Ljava/lang/String;)[B

    move-result-object v6

    .line 494
    .local v6, "fileBytes":[B
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->fitFileDownloadedReceiver:Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    new-instance v8, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;

    invoke-direct {v8, v6}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;-><init>([B)V

    invoke-interface {v0, v8}, Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;->onNewFitFileDownloaded(Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$FitFile;)V

    goto :goto_0

    .line 498
    .end local v6    # "fileBytes":[B
    :sswitch_2
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->antFsProgressUpdateRecevier:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    if-eqz v0, :cond_0

    .line 500
    const-string v0, "int_stateCode"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;

    move-result-object v1

    .line 501
    .local v1, "stateCode":Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;
    const-string v0, "long_transferredBytes"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 502
    .local v2, "transferredBytes":J
    const-string v0, "long_totalBytes"

    invoke-virtual {p3, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    .line 503
    .local v4, "totalBytes":J
    iget-object v0, p1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;->antFsProgressUpdateRecevier:Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    invoke-interface/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;->onNewAntFsProgressUpdate(Lcom/dsi/ant/plugins/antplus/pcc/defines/AntFsState;JJ)V

    goto :goto_0

    .line 479
    nop

    :sswitch_data_0
    .sparse-switch
        0xbe -> :sswitch_2
        0xbf -> :sswitch_1
        0xca -> :sswitch_0
    .end sparse-switch
.end method

.method public static requestDeviceListAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "availableDeviceListRecevier"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;",
            ")",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 395
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;>;"
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 396
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    const/16 v3, 0x12c

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 398
    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;-><init>()V

    .line 399
    .local v2, "possibleRetObj":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;
    iput-object p3, v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;

    .line 401
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;

    invoke-direct {v3}, Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;-><init>()V

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public cancelListenForNewActivities(Ljava/util/UUID;)Z
    .locals 7
    .param p1, "targetDeviceUUID"    # Ljava/util/UUID;

    .prologue
    const/4 v3, 0x0

    .line 681
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 682
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v4, 0x4e25

    iput v4, v0, Landroid/os/Message;->what:I

    .line 683
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 684
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 686
    if-eqz p1, :cond_0

    .line 687
    const-string v4, "uuid_targetDeviceUUID"

    invoke-virtual {v1, v4, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 689
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 691
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 693
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v5, "Cmd cancelListenForNewActivities died in sendPluginCommand()"

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 710
    :goto_0
    return v3

    .line 697
    :cond_1
    iget v4, v2, Landroid/os/Message;->arg1:I

    if-eqz v4, :cond_2

    .line 700
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cmd cancelListenForNewActivities failed with code "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 704
    :cond_2
    if-eqz p1, :cond_3

    .line 705
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;

    invoke-virtual {v3, p1}, Ljava/util/HashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 709
    :goto_1
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    .line 710
    const/4 v3, 0x1

    goto :goto_0

    .line 707
    :cond_3
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;

    invoke-virtual {v3}, Ljava/util/HashMap;->clear()V

    goto :goto_1
.end method

.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 418
    const-string v0, "ANT+ Plugin: Watch Downloader"

    return-object v0
.end method

.method protected getRequiredServiceVersionForBind()I
    .locals 1

    .prologue
    .line 36
    const/16 v0, 0x2a30

    return v0
.end method

.method protected getServiceBindIntent()Landroid/content/Intent;
    .locals 4

    .prologue
    .line 410
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 411
    .local v0, "it":Landroid/content/Intent;
    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.dsi.ant.plugins.antplus"

    const-string v3, "com.dsi.ant.plugins.antplus.watchcommunicator.WatchCommunicatorService"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 412
    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 11
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 424
    iget v8, p1, Landroid/os/Message;->arg1:I

    sparse-switch v8, :sswitch_data_0

    .line 472
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Unrecognized event received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 475
    :cond_0
    :goto_0
    return-void

    .line 428
    :sswitch_0
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;

    if-eqz v8, :cond_0

    .line 431
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 432
    .local v0, "b":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 433
    const-string v8, "int_listUpdateCode"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;

    move-result-object v6

    .line 434
    .local v6, "updateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;
    const-string v8, "arrayParcelable_deviceInfos"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v5

    .line 435
    .local v5, "uncastDeviceInfos":[Landroid/os/Parcelable;
    array-length v8, v5

    new-array v2, v8, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .line 436
    .local v2, "deviceInfos":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    array-length v8, v5

    if-ge v3, v8, :cond_1

    .line 437
    aget-object v8, v5, v3

    check-cast v8, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    aput-object v8, v2, v3

    .line 436
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 438
    :cond_1
    const/4 v1, 0x0

    .line 439
    .local v1, "deviceChanging":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    const-string v8, "parcelable_changingDeviceInfo"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 440
    const-string v8, "parcelable_changingDeviceInfo"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v1

    .end local v1    # "deviceChanging":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    check-cast v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .line 441
    .restart local v1    # "deviceChanging":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    :cond_2
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mAvailableDeviceListReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;

    invoke-interface {v8, v6, v2, v1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IAvailableDeviceListReceiver;->onNewAvailableDeviceList(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;)V

    goto :goto_0

    .line 449
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "deviceChanging":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .end local v2    # "deviceInfos":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .end local v3    # "i":I
    .end local v5    # "uncastDeviceInfos":[Landroid/os/Parcelable;
    .end local v6    # "updateCode":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceListUpdateCode;
    :sswitch_1
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    if-eqz v8, :cond_0

    .line 452
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    iget v9, p1, Landroid/os/Message;->arg1:I

    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v10

    invoke-direct {p0, v8, v9, v10}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->handleDownloadEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;ILandroid/os/Bundle;)V

    .line 454
    iget v8, p1, Landroid/os/Message;->arg1:I

    const/16 v9, 0xca

    if-ne v8, v9, :cond_0

    .line 456
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    .line 457
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v8}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 464
    :sswitch_2
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 465
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v8, "uuid_targetDeviceUUID"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getSerializable(Ljava/lang/String;)Ljava/io/Serializable;

    move-result-object v4

    check-cast v4, Ljava/util/UUID;

    .line 466
    .local v4, "targetDevice":Ljava/util/UUID;
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;

    invoke-virtual {v8, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    .line 467
    .local v7, "wi":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;
    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-direct {p0, v7, v8, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->handleDownloadEvent(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;ILandroid/os/Bundle;)V

    goto/16 :goto_0

    .line 424
    nop

    :sswitch_data_0
    .sparse-switch
        0xbe -> :sswitch_1
        0xbf -> :sswitch_1
        0xc9 -> :sswitch_0
        0xca -> :sswitch_1
        0xcb -> :sswitch_2
    .end sparse-switch
.end method

.method public listenForNewActivities(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;)Z
    .locals 8
    .param p1, "targetDeviceUUID"    # Ljava/util/UUID;
    .param p2, "downloadActivitiesFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;
    .param p3, "fitFileDownloadedReceiver"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;

    .prologue
    const/4 v4, 0x0

    .line 644
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    const/4 v5, 0x0

    invoke-direct {v3, p0, p2, p3, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)V

    .line 646
    .local v3, "wi":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 647
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v5, 0x4e24

    iput v5, v0, Landroid/os/Message;->what:I

    .line 648
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 649
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 651
    const-string v5, "uuid_targetDeviceUUID"

    invoke-virtual {v1, v5, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 653
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 655
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_0

    .line 657
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v6, "Cmd requestlistenForNewActivities died in sendPluginCommand()"

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 670
    :goto_0
    return v4

    .line 661
    :cond_0
    iget v5, v2, Landroid/os/Message;->arg1:I

    if-eqz v5, :cond_1

    .line 664
    sget-object v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Cmd requestlistenForNewActivities failed with code "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 668
    :cond_1
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mListenForNewActivitiesDeviceToReceiverMap:Ljava/util/HashMap;

    invoke-virtual {v4, p1, v3}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 669
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    .line 670
    const/4 v4, 0x1

    goto :goto_0
.end method

.method public requestCurrentDeviceList()V
    .locals 5

    .prologue
    .line 517
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 518
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v2, 0x4e21

    iput v2, v0, Landroid/os/Message;->what:I

    .line 520
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v1

    .line 522
    .local v1, "ret":Landroid/os/Message;
    if-nez v1, :cond_0

    .line 524
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v3, "Cmd requestCurrentDeviceList died in sendPluginCommand()"

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 536
    :goto_0
    return-void

    .line 528
    :cond_0
    iget v2, v1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    .line 531
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Cmd requestCurrentDeviceList failed with code "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v1, Landroid/os/Message;->arg1:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 532
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "requestCurrentDeviceList cmd failed internally"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 535
    :cond_1
    invoke-virtual {v1}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method public requestDownloadAllActivities(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "targetDeviceUUID"    # Ljava/util/UUID;
    .param p2, "downloadActivitiesFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;
    .param p3, "fitFileDownloadedReceiver"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;
    .param p4, "antFsProgressUpdateRecevier"    # Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 549
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 551
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 583
    :goto_0
    return v5

    .line 555
    :cond_0
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    invoke-direct {v3, p0, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    .line 557
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 558
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e22

    iput v3, v0, Landroid/os/Message;->what:I

    .line 559
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 560
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 562
    const-string v3, "uuid_targetDeviceUUID"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 563
    const-string v6, "bool_UseAntFsProgressUpdates"

    if-eqz p4, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 565
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 567
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_2

    .line 569
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestDownloadAllActivities died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 570
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_1
    move v3, v5

    .line 563
    goto :goto_1

    .line 574
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_2
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_3

    .line 577
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cmd requestDownloadAllActivities failed with code "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 582
    :cond_3
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 583
    goto :goto_0
.end method

.method public requestDownloadNewActivities(Ljava/util/UUID;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)Z
    .locals 7
    .param p1, "targetDeviceUUID"    # Ljava/util/UUID;
    .param p2, "downloadActivitiesFinishedReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;
    .param p3, "fitFileDownloadedReceiver"    # Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;
    .param p4, "antFsProgressUpdateRecevier"    # Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 596
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->tryAcquire()Z

    move-result v3

    if-nez v3, :cond_0

    .line 598
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd failed to start because a local command is still processing."

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 630
    :goto_0
    return v5

    .line 602
    :cond_0
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    invoke-direct {v3, p0, p2, p3, p4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$IDownloadActivitiesFinishedReceiver;Lcom/dsi/ant/plugins/antplus/common/FitFileCommon$IFitFileDownloadedReceiver;Lcom/dsi/ant/plugins/antplus/common/AntFsCommon$IAntFsProgressUpdateReceiver;)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->singleRequestReceiver:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DownloadRequestActivitiesInfo;

    .line 604
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 605
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e23

    iput v3, v0, Landroid/os/Message;->what:I

    .line 606
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 607
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 609
    const-string v3, "uuid_targetDeviceUUID"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 610
    const-string v6, "bool_UseAntFsProgressUpdates"

    if-eqz p4, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v1, v6, v3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 612
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 614
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_2

    .line 616
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd requestDownloadNewActivities died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 617
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .end local v2    # "ret":Landroid/os/Message;
    :cond_1
    move v3, v5

    .line 610
    goto :goto_1

    .line 621
    .restart local v2    # "ret":Landroid/os/Message;
    :cond_2
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_3

    .line 624
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Cmd requestDownloadNewActivities failed with code "

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v6, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 625
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc;->mCommandLock:Ljava/util/concurrent/Semaphore;

    invoke-virtual {v3}, Ljava/util/concurrent/Semaphore;->release()V

    goto :goto_0

    .line 629
    :cond_3
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    move v5, v4

    .line 630
    goto :goto_0
.end method
