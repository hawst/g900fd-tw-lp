.class public Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_ProgramDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private PIN:J

.field private clearExistingData:Z

.field private currentAckReq:[B

.field private deviceID:I

.field private finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private finishedProgramming:Z

.field private final geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

.field private msgsSinceFirstPageRequest:I

.field private progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

.field private programmingPagesQueue:Ljava/util/Queue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Queue",
            "<[B>;"
        }
    .end annotation
.end field

.field private progress_pagesDone:I

.field private progress_pagesTotal:I

.field private final reqProgramFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private reqProgramProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private responseSent:Z

.field private targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 32
    const-class v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;IJZLcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Z)V
    .locals 3
    .param p1, "geocacheReceiver"    # Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    .param p3, "deviceID"    # I
    .param p4, "PIN"    # J
    .param p6, "clearExistingData"    # Z
    .param p7, "progData"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;
    .param p8, "useProgressUpdates"    # Z

    .prologue
    const/4 v2, 0x0

    .line 65
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 40
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcb

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xca

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 49
    const/4 v0, -0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    .line 50
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->currentAckReq:[B

    .line 52
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedProgramming:Z

    .line 54
    iput v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progress_pagesDone:I

    .line 57
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->responseSent:Z

    .line 66
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    .line 67
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    .line 68
    iput-wide p4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->PIN:J

    .line 69
    iput-boolean p6, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->clearExistingData:Z

    .line 70
    iput-object p7, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    .line 72
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 73
    if-eqz p8, :cond_0

    .line 74
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 75
    :cond_0
    return-void
.end method

.method private reportProgress(II)V
    .locals 2
    .param p1, "done"    # I
    .param p2, "total"    # I

    .prologue
    .line 94
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v1

    if-nez v1, :cond_0

    .line 101
    :goto_0
    return-void

    .line 97
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 98
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_workUnitsFinished"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 99
    const-string v1, "int_totalUnitsWork"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 100
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramProgress:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto :goto_0
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 304
    :try_start_0
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->flushAndEnsureClosedChannel()V

    .line 306
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 307
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V

    .line 308
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    new-instance v2, Lcom/dsi/ant/message/ChannelId;

    iget v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v4}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/16 v4, 0x13

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    invoke-virtual {v5}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    const/4 v5, 0x0

    invoke-direct {v2, v3, v4, v5}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->setChannelId(Lcom/dsi/ant/message/ChannelId;)V

    .line 310
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->currentAckReq:[B

    .line 312
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->enableMessageProcessing()V

    .line 313
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->open()V

    .line 315
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->await()V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_3
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_5
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 334
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 342
    :goto_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedProgramming:Z

    if-eqz v1, :cond_0

    .line 344
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 351
    :goto_1
    return-void

    .line 335
    :catch_0
    move-exception v0

    .line 338
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 316
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 318
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_2
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 334
    :try_start_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_2

    goto :goto_0

    .line 335
    :catch_2
    move-exception v0

    .line 338
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 320
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_3
    move-exception v0

    .line 322
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :try_start_4
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE initializing channel: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 323
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 334
    :try_start_5
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v3, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v1, v2, v3}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_5
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_5 .. :try_end_5} :catch_4

    goto/16 :goto_1

    .line 335
    :catch_4
    move-exception v0

    .line 338
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE resetting search timeout "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 325
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_5
    move-exception v0

    .line 327
    .local v0, "e":Landroid/os/RemoteException;
    :try_start_6
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 328
    throw v0
    :try_end_6
    .catchall {:try_start_6 .. :try_end_6} :catchall_0

    .line 332
    .end local v0    # "e":Landroid/os/RemoteException;
    :catchall_0
    move-exception v1

    .line 334
    :try_start_7
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->DEFAULT_LOW_SEARCH_TIMEOUT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v4, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v2, v3, v4}, Lcom/dsi/ant/channel/AntChannel;->setSearchTimeout(Lcom/dsi/ant/message/LowPrioritySearchTimeout;Lcom/dsi/ant/message/HighPrioritySearchTimeout;)V
    :try_end_7
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_7 .. :try_end_7} :catch_6

    .line 339
    :goto_2
    throw v1

    .line 335
    :catch_6
    move-exception v0

    .line 338
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v2, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "ACFE resetting search timeout "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_2

    .line 348
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->removeDeviceFromList(I)Z

    .line 349
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto/16 :goto_1
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 290
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Programming Device "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 356
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 357
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 297
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 4
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 194
    :try_start_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 285
    :cond_0
    :goto_0
    return-void

    .line 198
    :pswitch_0
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v2, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v2, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v2

    invoke-virtual {v2}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_1

    goto :goto_0

    .line 201
    :pswitch_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    const-string v2, "Timed out attempting to find device"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 268
    :catch_0
    move-exception v0

    .line 270
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v1, v2, :cond_3

    .line 274
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    const-string v2, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 206
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    const-string v2, "Channel closed"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 207
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->disableMessageProcessing()V

    .line 208
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 211
    :pswitch_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->size()I

    move-result v1

    if-lez v1, :cond_1

    .line 213
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progress_pagesDone:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progress_pagesDone:I

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progress_pagesTotal:I

    invoke-direct {p0, v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reportProgress(II)V

    .line 214
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    invoke-interface {v1}, Ljava/util/Queue;->remove()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->currentAckReq:[B

    .line 215
    const/4 v1, 0x0

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    .line 216
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->currentAckReq:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    goto :goto_0

    .line 221
    :cond_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iput-object v2, v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    .line 222
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    invoke-virtual {v1, v2}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->reportDeviceProgrammed(I)V

    .line 224
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedProgramming:Z

    .line 225
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->disableMessageProcessing()V

    .line 226
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 235
    :pswitch_4
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    iget v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v2, v2, 0x3

    rsub-int/lit8 v2, v2, 0x2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    goto/16 :goto_0

    .line 246
    :pswitch_5
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    .line 247
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    const/16 v2, 0x21

    if-le v1, v2, :cond_2

    .line 249
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    const-string v2, "Did not receive message after 10 requests"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 250
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 251
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->disableMessageProcessing()V

    .line 252
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 257
    :cond_2
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v1, v1, 0x3

    if-nez v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->currentAckReq:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 278
    .restart local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "ACFE handling message: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 279
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 280
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->disableMessageProcessing()V

    .line 281
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v1}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 194
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 198
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public processRequest()V
    .locals 7

    .prologue
    .line 106
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->deviceList:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;

    iget v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->deviceID:I

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/DeviceList;->getCurrentDeviceData(I)Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    move-result-object v3

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    .line 107
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    if-nez v3, :cond_0

    .line 109
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_NOT_IN_LIST:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    .line 187
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->getNextMissingPageNum()I

    move-result v1

    .line 115
    .local v1, "nextPageDownloadNum":I
    if-ltz v1, :cond_2

    const/4 v3, 0x2

    if-lt v1, v3, :cond_1

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->clearExistingData:Z

    if-nez v3, :cond_2

    .line 118
    :cond_1
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_DEVICE_DATA_NOT_DOWNLOADED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 123
    :cond_2
    iget-wide v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->PIN:J

    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v5, v5, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v3

    const-wide v5, 0xffffffffL

    cmp-long v3, v3, v5

    if-eqz v3, :cond_3

    .line 125
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_NO_PERMISSION:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto :goto_0

    .line 129
    :cond_3
    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->clearExistingData:Z

    if-nez v3, :cond_a

    .line 131
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    if-nez v3, :cond_4

    .line 132
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->identificationString:Ljava/lang/String;

    .line 133
    :cond_4
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    if-nez v3, :cond_5

    .line 134
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->PIN:Ljava/lang/Long;

    .line 135
    :cond_5
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    if-nez v3, :cond_6

    .line 136
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->latitude:Ljava/math/BigDecimal;

    .line 137
    :cond_6
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    if-nez v3, :cond_7

    .line 138
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->longitude:Ljava/math/BigDecimal;

    .line 139
    :cond_7
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    if-nez v3, :cond_8

    .line 140
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->hintString:Ljava/lang/String;

    .line 141
    :cond_8
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    if-nez v3, :cond_9

    .line 142
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->numberOfVisits:Ljava/lang/Integer;

    .line 143
    :cond_9
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    if-nez v3, :cond_a

    .line 144
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->targetDevice:Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ConnectedDeviceInfo;->deviceData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheDeviceData;->programmableData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    iget-object v4, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    iput-object v4, v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;->lastVisitTimestamp:Ljava/util/GregorianCalendar;

    .line 149
    :cond_a
    :try_start_0
    new-instance v3, Ljava/util/LinkedList;

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progData:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;

    const/4 v5, 0x1

    invoke-static {v4, v5}, Lcom/dsi/ant/plugins/antplus/geocache/devicelist/ProgramPageGenerator;->getAllProgrammingPages(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$ProgrammableGeocacheDeviceData;Z)Ljava/util/List;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/util/LinkedList;-><init>(Ljava/util/Collection;)V

    iput-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    .line 150
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    const/16 v4, 0x20

    if-le v3, v4, :cond_b

    .line 151
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Too many programmable pages, probably hint string too long"

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    .line 154
    :catch_0
    move-exception v0

    .line 156
    .local v0, "e":Ljava/lang/NullPointerException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/NullPointerException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 157
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_BAD_PARAMS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto/16 :goto_0

    .line 152
    .end local v0    # "e":Ljava/lang/NullPointerException;
    :cond_b
    :try_start_1
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->programmingPagesQueue:Ljava/util/Queue;

    invoke-interface {v3}, Ljava/util/Queue;->size()I

    move-result v3

    iput v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->progress_pagesTotal:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 167
    const/4 v2, 0x0

    .line 170
    .local v2, "taskStarted":Z
    :try_start_2
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    iget-object v3, v3, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->channelExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v4, 0x3e8

    invoke-virtual {v3, p0, v4}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_2
    .catch Ljava/lang/InterruptedException; {:try_start_2 .. :try_end_2} :catch_2

    move-result v2

    .line 177
    :goto_1
    if-nez v2, :cond_c

    .line 179
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto/16 :goto_0

    .line 160
    .end local v2    # "taskStarted":Z
    :catch_1
    move-exception v0

    .line 161
    .local v0, "e":Ljava/lang/IllegalArgumentException;
    sget-object v3, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 162
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->FAIL_BAD_PARAMS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    invoke-virtual {p0, v3}, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V

    goto/16 :goto_0

    .line 171
    .end local v0    # "e":Ljava/lang/IllegalArgumentException;
    .restart local v2    # "taskStarted":Z
    :catch_2
    move-exception v0

    .line 174
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 184
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_c
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->geocacheReceiver:Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;

    const/16 v4, 0x12c

    invoke-virtual {v3, v4}, Lcom/dsi/ant/plugins/antplus/geocache/GeocacheReceiver;->setCurrentState(I)V

    goto/16 :goto_0
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;)V
    .locals 4
    .param p1, "statusCode"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;

    .prologue
    .line 79
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v2

    .line 81
    :try_start_0
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->responseSent:Z

    if-eqz v1, :cond_0

    .line 82
    monitor-exit v2

    .line 90
    :goto_0
    return-void

    .line 84
    :cond_0
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 85
    .local v0, "b":Landroid/os/Bundle;
    const-string v1, "int_statusCode"

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusGeocachePcc$GeocacheRequestStatus;->getIntValue()I

    move-result v3

    invoke-virtual {v0, v1, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 87
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->reqProgramFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v1, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 88
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/geocache/tasks/ChannelTask_ProgramDevice;->responseSent:Z

    .line 89
    monitor-exit v2

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    :catchall_0
    move-exception v1

    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
