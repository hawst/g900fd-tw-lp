.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P19_TeAndPsData.java"


# instance fields
.field private eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

.field private psEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private teEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;)V
    .locals 2
    .param p1, "evtCntAcc"    # Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .prologue
    .line 28
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 22
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xce

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->teEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 23
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xcf

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->psEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 29
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    .line 30
    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 10
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 50
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x3

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    .line 51
    .local v2, "rxLeftTorqEff":I
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x4

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v4

    .line 53
    .local v4, "rxRightTorqEff":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->teEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 55
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 56
    .local v0, "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 57
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 60
    const-string v5, "long_powerOnlyUpdateEventCount"

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 63
    const/16 v5, 0xff

    if-eq v2, v5, :cond_2

    .line 64
    const-string v5, "decimal_leftTorqueEffectiveness"

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v2}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x1

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 71
    :goto_0
    const/16 v5, 0xff

    if-eq v4, v5, :cond_3

    .line 72
    const-string v5, "decimal_rightTorqueEffectiveness"

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v4}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x1

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 78
    :goto_1
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->teEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 81
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_0
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x5

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v1

    .line 82
    .local v1, "rxLeftPedSmth":I
    invoke-virtual {p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v5

    const/4 v6, 0x6

    aget-byte v5, v5, v6

    invoke-static {v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    .line 84
    .local v3, "rxRightPedSmth":I
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->psEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 86
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 87
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v5, "long_EstTimestamp"

    invoke-virtual {v0, v5, p1, p2}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 88
    const-string v5, "long_EventFlags"

    invoke-virtual {v0, v5, p3, p4}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 91
    const-string v5, "long_powerOnlyUpdateEventCount"

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->eventCountAccumulator:Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/common/pages/TimedAccumulator;->getValue()J

    move-result-wide v6

    invoke-virtual {v0, v5, v6, v7}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 94
    const/16 v5, 0xff

    if-eq v1, v5, :cond_4

    .line 95
    const-string v5, "decimal_leftOrCombinedPedalSmoothness"

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v1}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x1

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 102
    :goto_2
    const/16 v5, 0xfe

    if-ne v3, v5, :cond_5

    .line 103
    const-string v5, "bool_separatePedalSmoothnessSupport"

    const/4 v6, 0x0

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 107
    :goto_3
    const/16 v5, 0xff

    if-eq v3, v5, :cond_6

    const/16 v5, 0xfe

    if-eq v3, v5, :cond_6

    .line 108
    const-string v5, "decimal_rightPedalSmoothness"

    new-instance v6, Ljava/math/BigDecimal;

    invoke-direct {v6, v3}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v7, Ljava/math/BigDecimal;

    const/4 v8, 0x2

    invoke-direct {v7, v8}, Ljava/math/BigDecimal;-><init>(I)V

    const/4 v8, 0x1

    sget-object v9, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v6, v7, v8, v9}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v6

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 114
    :goto_4
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->psEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v5, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 116
    .end local v0    # "b":Landroid/os/Bundle;
    :cond_1
    return-void

    .line 67
    .end local v1    # "rxLeftPedSmth":I
    .end local v3    # "rxRightPedSmth":I
    .restart local v0    # "b":Landroid/os/Bundle;
    :cond_2
    const-string v5, "decimal_leftTorqueEffectiveness"

    new-instance v6, Ljava/math/BigDecimal;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_0

    .line 75
    :cond_3
    const-string v5, "decimal_rightTorqueEffectiveness"

    new-instance v6, Ljava/math/BigDecimal;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto/16 :goto_1

    .line 98
    .restart local v1    # "rxLeftPedSmth":I
    .restart local v3    # "rxRightPedSmth":I
    :cond_4
    const-string v5, "decimal_leftOrCombinedPedalSmoothness"

    new-instance v6, Ljava/math/BigDecimal;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_2

    .line 105
    :cond_5
    const-string v5, "bool_separatePedalSmoothnessSupport"

    const/4 v6, 0x1

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_3

    .line 111
    :cond_6
    const-string v5, "decimal_rightPedalSmoothness"

    new-instance v6, Ljava/math/BigDecimal;

    const/4 v7, -0x1

    invoke-direct {v6, v7}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v5, v6}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    goto :goto_4
.end method

.method public getEventList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 35
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 36
    .local v0, "eL":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;>;"
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->teEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 37
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P19_TeAndPsData;->psEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 38
    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 44
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/16 v2, 0x13

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
