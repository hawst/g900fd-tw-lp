.class public Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
.super Ljava/lang/Object;
.source "AntFsDirectory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;,
        Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    }
.end annotation


# static fields
.field public static final DIRECTORY_ENTRY_LENGTH:B = 0x10t

.field public static final DIRECTORY_HEADER_LENGTH:B = 0x10t

.field public static final DIRECTORY_VERSION_MAJOR:B = 0x0t

.field public static final DIRECTORY_VERSION_MINOR:B = 0x1t

.field public static final GARMIN_TIME_OFFSET:J = 0x92ee70e000L

.field public static final TIME_UNKNOWN:J = 0xfffffffL


# instance fields
.field public final dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

.field directoryDownloadTime:Ljava/util/GregorianCalendar;

.field public final fileEntryList:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 7

    .prologue
    const-wide/16 v5, 0x3e8

    .line 354
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 355
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    .line 356
    new-instance v0, Landroid/util/SparseArray;

    invoke-direct {v0}, Landroid/util/SparseArray;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    .line 358
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    const/4 v1, 0x0

    iput v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_major:I

    .line 359
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    const/4 v1, 0x1

    iput v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_minor:I

    .line 360
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    const/16 v1, 0x10

    iput v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirEntryLength:I

    .line 361
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->AUTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    iput-object v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->timeFormat:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 362
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v1

    div-long/2addr v1, v5

    iput-wide v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->currentSystemTimestamp:J

    .line 363
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getTime()J

    move-result-wide v1

    const-wide v3, 0x92ee70e000L

    sub-long/2addr v1, v3

    div-long/2addr v1, v5

    iput-wide v1, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    .line 364
    return-void
.end method

.method public constructor <init>([BLjava/util/GregorianCalendar;)V
    .locals 13
    .param p1, "directoryRawBytes"    # [B
    .param p2, "directoryDownloadTime"    # Ljava/util/GregorianCalendar;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/util/zip/DataFormatException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x10

    const/4 v12, 0x2

    const-wide/32 v10, 0xfffffff

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 286
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 287
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->directoryDownloadTime:Ljava/util/GregorianCalendar;

    .line 289
    new-instance v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    .line 290
    new-instance v2, Landroid/util/SparseArray;

    invoke-direct {v2}, Landroid/util/SparseArray;-><init>()V

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    .line 292
    array-length v2, p1

    if-ge v2, v4, :cond_0

    .line 293
    new-instance v2, Ljava/util/zip/DataFormatException;

    const-string v3, "Directory bytes don\'t contain full 16 byte header"

    invoke-direct {v2, v3}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 295
    :cond_0
    array-length v2, p1

    rem-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_1

    .line 296
    new-instance v2, Ljava/util/zip/DataFormatException;

    const-string v3, "Directory bytes not a multiple of 16"

    invoke-direct {v2, v3}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 298
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    aget-byte v3, p1, v8

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromUpper4Bits(B)I

    move-result v3

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_major:I

    .line 299
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    aget-byte v3, p1, v8

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromLower4Bits(B)I

    move-result v3

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_minor:I

    .line 300
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_minor:I

    if-ne v2, v9, :cond_2

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_major:I

    if-eqz v2, :cond_3

    .line 301
    :cond_2
    new-instance v2, Ljava/util/zip/DataFormatException;

    const-string v3, "Directory not version 1"

    invoke-direct {v2, v3}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 303
    :cond_3
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    aget-byte v3, p1, v9

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    iput v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirEntryLength:I

    .line 304
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v2, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirEntryLength:I

    if-eq v2, v4, :cond_4

    .line 305
    new-instance v2, Ljava/util/zip/DataFormatException;

    const-string v3, "Directory entries not 16 byte length"

    invoke-direct {v2, v3}, Ljava/util/zip/DataFormatException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 307
    :cond_4
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    aget-byte v3, p1, v12

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v3

    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->convertIntToTimeFormat(I)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    move-result-object v3

    iput-object v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->timeFormat:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 311
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    const/16 v3, 0x8

    invoke-static {p1, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->currentSystemTimestamp:J

    .line 313
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    const/16 v3, 0xc

    invoke-static {p1, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v3

    iput-wide v3, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    .line 316
    const/16 v1, 0x10

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_7

    .line 318
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;-><init>()V

    .line 319
    .local v0, "e":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    invoke-static {p1, v1}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    iput v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    .line 320
    add-int/lit8 v2, v1, 0x2

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    iput v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    .line 321
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    add-int/lit8 v3, v1, 0x3

    aget-byte v3, p1, v3

    aput-byte v3, v2, v8

    .line 322
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    add-int/lit8 v3, v1, 0x4

    aget-byte v3, p1, v3

    aput-byte v3, v2, v9

    .line 323
    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    add-int/lit8 v3, v1, 0x5

    aget-byte v3, p1, v3

    aput-byte v3, v2, v12

    .line 324
    add-int/lit8 v2, v1, 0x6

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v2

    iput v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataTypeSpecificFlags:I

    .line 325
    add-int/lit8 v2, v1, 0x7

    aget-byte v2, p1, v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->getFlagsFromFlagsByte(B)Ljava/util/EnumSet;

    move-result-object v2

    iput-object v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    .line 326
    add-int/lit8 v2, v1, 0x8

    invoke-static {p1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    .line 327
    add-int/lit8 v2, v1, 0xc

    invoke-static {p1, v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom4LeBytes([BI)J

    move-result-wide v2

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    .line 330
    iget-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    cmp-long v2, v2, v10

    if-lez v2, :cond_5

    .line 332
    iget-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    .line 346
    :goto_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    iget v3, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    invoke-virtual {v2, v3, v0}, Landroid/util/SparseArray;->append(ILjava/lang/Object;)V

    .line 316
    add-int/lit8 v1, v1, 0x10

    goto :goto_0

    .line 334
    :cond_5
    iget-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    cmp-long v2, v2, v10

    if-gez v2, :cond_6

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v2, v2, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->currentSystemTimestamp:J

    cmp-long v2, v2, v10

    if-eqz v2, :cond_6

    .line 337
    invoke-virtual {p2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/Date;->getTime()J

    move-result-wide v2

    const-wide v4, 0x92ee70e000L

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    .line 339
    iget-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v4, v4, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->currentSystemTimestamp:J

    iget-wide v6, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    sub-long/2addr v4, v6

    sub-long/2addr v2, v4

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    goto :goto_1

    .line 343
    :cond_6
    const-wide/16 v2, 0x0

    iput-wide v2, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->adjustedTimeStamp:J

    goto :goto_1

    .line 348
    .end local v0    # "e":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    :cond_7
    return-void
.end method


# virtual methods
.method public toByteArray()[B
    .locals 10

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x0

    .line 368
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    add-int/lit8 v5, v5, 0x1

    mul-int/lit8 v0, v5, 0x10

    .line 369
    .local v0, "dirLength":I
    new-array v4, v0, [B

    .line 371
    .local v4, "rawDirectory":[B
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v5, v5, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_major:I

    and-int/lit16 v5, v5, 0xf0

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v6, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirVersion_minor:I

    and-int/lit8 v6, v6, 0xf

    or-int/2addr v5, v6

    invoke-static {v4, v8, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 372
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget v6, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirEntryLength:I

    invoke-static {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 373
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-object v6, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->timeFormat:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->getValue()I

    move-result v6

    invoke-static {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 374
    aput-byte v8, v4, v9

    .line 375
    const/4 v5, 0x4

    aput-byte v8, v4, v5

    .line 376
    const/4 v5, 0x5

    aput-byte v8, v4, v5

    .line 377
    const/4 v5, 0x6

    aput-byte v8, v4, v5

    .line 378
    const/4 v5, 0x7

    aput-byte v8, v4, v5

    .line 379
    const/16 v5, 0x8

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v6, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->currentSystemTimestamp:J

    invoke-static {v4, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 380
    const/16 v5, 0xc

    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->dirHeader:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;

    iget-wide v6, v6, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsDirectoryHeader;->dirLastModifiedTimestamp:J

    invoke-static {v4, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 382
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v5}, Landroid/util/SparseArray;->size()I

    move-result v5

    if-ge v2, v5, :cond_0

    .line 384
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;->fileEntryList:Landroid/util/SparseArray;

    invoke-virtual {v5, v2}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;

    .line 385
    .local v1, "entry":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    add-int/lit8 v5, v2, 0x1

    mul-int/lit8 v3, v5, 0x10

    .line 386
    .local v3, "offset":I
    iget v5, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileIndex:I

    invoke-static {v4, v3, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn2LeBytes([BII)V

    .line 387
    add-int/lit8 v5, v3, 0x2

    iget v6, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataType:I

    invoke-static {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 388
    iget-object v5, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->identifier:[B

    add-int/lit8 v6, v3, 0x3

    invoke-static {v5, v8, v4, v6, v9}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 389
    add-int/lit8 v5, v3, 0x6

    iget v6, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->dataTypeSpecificFlags:I

    invoke-static {v4, v5, v6}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn1LeBytes([BII)V

    .line 390
    add-int/lit8 v5, v3, 0x7

    iget-object v6, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->generalFlags:Ljava/util/EnumSet;

    invoke-static {v6}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileGeneralFlag;->getFlagByteFromFlags(Ljava/util/EnumSet;)B

    move-result v6

    aput-byte v6, v4, v5

    .line 391
    add-int/lit8 v5, v3, 0x8

    iget-wide v6, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->fileSize:J

    invoke-static {v4, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 392
    add-int/lit8 v5, v3, 0xc

    iget-wide v6, v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;->timeStamp:J

    invoke-static {v4, v5, v6, v7}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->PutUnsignedNumIn4LeBytes([BIJ)V

    .line 382
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 395
    .end local v1    # "entry":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsFileEntry;
    .end local v3    # "offset":I
    :cond_0
    return-object v4
.end method
