.class public Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;
.super Ljava/lang/Object;
.source "AntPluginDeviceDbProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;,
        Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;,
        Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 120
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method public static dbsrvc_addDevice(Landroid/content/Context;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Landroid/os/Bundle;)V
    .locals 3
    .param p0, "currentContext"    # Landroid/content/Context;
    .param p1, "devInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "pluginName"    # Ljava/lang/String;
    .param p3, "deviceParams"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
        }
    .end annotation

    .prologue
    .line 166
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 167
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "int_RequestType"

    const/16 v2, 0x16

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 168
    const-string v1, "parcelable_DeviceDbInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 169
    const-string v1, "string_PluginName"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 170
    const-string v1, "bundle_PluginDeviceParams"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putBundle(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 172
    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->executeDbRequest(Landroid/content/Context;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 173
    return-void
.end method

.method public static dbsrvc_changeDeviceInfo(Landroid/content/Context;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Ljava/lang/String;Ljava/lang/Boolean;)V
    .locals 3
    .param p0, "currentContext"    # Landroid/content/Context;
    .param p1, "dev"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "chgVisibleName"    # Ljava/lang/String;
    .param p3, "chgIsPreferredDevice"    # Ljava/lang/Boolean;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
        }
    .end annotation

    .prologue
    .line 155
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 156
    .local v0, "req":Landroid/os/Bundle;
    const-string v1, "int_RequestType"

    const/16 v2, 0x14

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 157
    const-string v1, "parcelable_DeviceDbInfo"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 158
    const-string v1, "string_DISPLAYNAME"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 159
    const-string v1, "boolean_IsUserPreferredDevice"

    invoke-virtual {p3}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 161
    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->executeDbRequest(Landroid/content/Context;Landroid/os/Bundle;)Landroid/os/Bundle;

    .line 162
    return-void
.end method

.method public static dbsrvc_getDeviceInfo(Landroid/content/Context;ILjava/lang/String;)Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .locals 4
    .param p0, "currentContext"    # Landroid/content/Context;
    .param p1, "antDeviceNumber"    # I
    .param p2, "pluginName"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
        }
    .end annotation

    .prologue
    .line 144
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 145
    .local v0, "req":Landroid/os/Bundle;
    const-string v2, "int_RequestType"

    const/16 v3, 0x12

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 146
    const-string v2, "int_AntDeviceNumber"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 147
    const-string v2, "string_PluginName"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 149
    invoke-static {p0, v0}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->executeDbRequest(Landroid/content/Context;Landroid/os/Bundle;)Landroid/os/Bundle;

    move-result-object v1

    .line 150
    .local v1, "results":Landroid/os/Bundle;
    const-string v2, "parcelable_DeviceDbInfo"

    invoke-virtual {v1, v2}, Landroid/os/Bundle;->getParcelable(Ljava/lang/String;)Landroid/os/Parcelable;

    move-result-object v2

    check-cast v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    return-object v2
.end method

.method private static executeDbRequest(Landroid/content/Context;Landroid/os/Bundle;)Landroid/os/Bundle;
    .locals 21
    .param p0, "currentContext"    # Landroid/content/Context;
    .param p1, "req"    # Landroid/os/Bundle;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;
        }
    .end annotation

    .prologue
    .line 186
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v11

    .line 189
    .local v11, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    const-string v17, "com.dsi.ant.plugins.antplus"

    const/16 v18, 0x4

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-virtual {v11, v0, v1}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v10

    .line 190
    .local v10, "pi":Landroid/content/pm/PackageInfo;
    const/16 v16, 0x0

    .line 191
    .local v16, "serviceInstalled":Z
    iget-object v4, v10, Landroid/content/pm/PackageInfo;->services:[Landroid/content/pm/ServiceInfo;

    .local v4, "arr$":[Landroid/content/pm/ServiceInfo;
    array-length v9, v4

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v7, v4, v8

    .line 193
    .local v7, "i":Landroid/content/pm/ServiceInfo;
    const-string v17, "com.dsi.ant.plugins.antplus.utility.db.Service_DeviceDbProvider"

    iget-object v0, v7, Landroid/content/pm/ServiceInfo;->name:Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-virtual/range {v17 .. v18}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v17

    if-eqz v17, :cond_1

    .line 195
    const/16 v16, 0x1

    .line 199
    .end local v7    # "i":Landroid/content/pm/ServiceInfo;
    :cond_0
    if-nez v16, :cond_2

    .line 200
    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;

    const-string v18, "AntPlusPlugins not installed"

    const/16 v19, -0x2

    invoke-direct/range {v17 .. v19}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;-><init>(Ljava/lang/String;I)V

    throw v17
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 202
    .end local v4    # "arr$":[Landroid/content/pm/ServiceInfo;
    .end local v8    # "i$":I
    .end local v9    # "len$":I
    .end local v10    # "pi":Landroid/content/pm/PackageInfo;
    .end local v16    # "serviceInstalled":Z
    :catch_0
    move-exception v5

    .line 204
    .local v5, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;

    const-string v18, "AntPlusPlugins not installed"

    const/16 v19, -0x3

    invoke-direct/range {v17 .. v19}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;-><init>(Ljava/lang/String;I)V

    throw v17

    .line 191
    .end local v5    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .restart local v4    # "arr$":[Landroid/content/pm/ServiceInfo;
    .restart local v7    # "i":Landroid/content/pm/ServiceInfo;
    .restart local v8    # "i$":I
    .restart local v9    # "len$":I
    .restart local v10    # "pi":Landroid/content/pm/PackageInfo;
    .restart local v16    # "serviceInstalled":Z
    :cond_1
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 208
    .end local v7    # "i":Landroid/content/pm/ServiceInfo;
    :cond_2
    new-instance v12, Ljava/util/concurrent/Exchanger;

    invoke-direct {v12}, Ljava/util/concurrent/Exchanger;-><init>()V

    .line 209
    .local v12, "requestFinishedExchange":Ljava/util/concurrent/Exchanger;, "Ljava/util/concurrent/Exchanger<Landroid/os/Bundle;>;"
    new-instance v6, Landroid/os/HandlerThread;

    const-string v17, "AntPluginDeviceDb Result Handler"

    move-object/from16 v0, v17

    invoke-direct {v6, v0}, Landroid/os/HandlerThread;-><init>(Ljava/lang/String;)V

    .line 210
    .local v6, "ht":Landroid/os/HandlerThread;
    invoke-virtual {v6}, Landroid/os/HandlerThread;->start()V

    .line 211
    new-instance v15, Landroid/os/Messenger;

    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;

    invoke-virtual {v6}, Landroid/os/HandlerThread;->getLooper()Landroid/os/Looper;

    move-result-object v18

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v12, v6}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$1;-><init>(Landroid/os/Looper;Ljava/util/concurrent/Exchanger;Landroid/os/HandlerThread;)V

    move-object/from16 v0, v17

    invoke-direct {v15, v0}, Landroid/os/Messenger;-><init>(Landroid/os/Handler;)V

    .line 233
    .local v15, "resultHandler":Landroid/os/Messenger;
    const-string v17, "msgr_ResultReceiver"

    move-object/from16 v0, p1

    move-object/from16 v1, v17

    invoke-virtual {v0, v1, v15}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 234
    new-instance v7, Landroid/content/Intent;

    invoke-direct {v7}, Landroid/content/Intent;-><init>()V

    .line 235
    .local v7, "i":Landroid/content/Intent;
    const-string v17, "com.dsi.ant.plugins.antplus"

    const-string v18, "com.dsi.ant.plugins.antplus.utility.db.Service_DeviceDbProvider"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 236
    const-string v17, "com.dsi.ant.plugins.antplus.utility.db.devicedbrequest"

    move-object/from16 v0, v17

    move-object/from16 v1, p1

    invoke-virtual {v7, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Bundle;)Landroid/content/Intent;

    .line 238
    move-object/from16 v0, p0

    invoke-virtual {v0, v7}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 242
    const/16 v17, 0x0

    const-wide/16 v18, 0x5

    :try_start_1
    sget-object v20, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    move-object/from16 v0, v17

    move-wide/from16 v1, v18

    move-object/from16 v3, v20

    invoke-virtual {v12, v0, v1, v2, v3}, Ljava/util/concurrent/Exchanger;->exchange(Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, Landroid/os/Bundle;

    .line 243
    .local v13, "result":Landroid/os/Bundle;
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v17

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 244
    const-string v17, "int_ResultCode"

    move-object/from16 v0, v17

    invoke-virtual {v13, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v14

    .line 245
    .local v14, "resultCode":I
    if-eqz v14, :cond_3

    .line 246
    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;

    const-string v18, "Result returned failure"

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    invoke-direct {v0, v1, v14}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;-><init>(Ljava/lang/String;I)V

    throw v17
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_1 .. :try_end_1} :catch_2

    .line 248
    .end local v13    # "result":Landroid/os/Bundle;
    .end local v14    # "resultCode":I
    :catch_1
    move-exception v5

    .line 250
    .local v5, "e":Ljava/lang/InterruptedException;
    sget-object v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;

    const-string v18, "Main result exchange interrupted"

    invoke-static/range {v17 .. v18}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 251
    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;

    const-string v18, "Main result exchange interrupted"

    const/16 v19, -0x4

    invoke-direct/range {v17 .. v19}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;-><init>(Ljava/lang/String;I)V

    throw v17

    .line 252
    .end local v5    # "e":Ljava/lang/InterruptedException;
    :catch_2
    move-exception v5

    .line 254
    .local v5, "e":Ljava/util/concurrent/TimeoutException;
    sget-object v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider;->TAG:Ljava/lang/String;

    const-string v18, "Result exchange interrupted"

    invoke-static/range {v17 .. v18}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    new-instance v17, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;

    const-string v18, "DB request timed out"

    const/16 v19, -0x4

    invoke-direct/range {v17 .. v19}, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$AntPluginDeviceDbException;-><init>(Ljava/lang/String;I)V

    throw v17

    .line 247
    .end local v5    # "e":Ljava/util/concurrent/TimeoutException;
    .restart local v13    # "result":Landroid/os/Bundle;
    .restart local v14    # "resultCode":I
    :cond_3
    return-object v13
.end method
