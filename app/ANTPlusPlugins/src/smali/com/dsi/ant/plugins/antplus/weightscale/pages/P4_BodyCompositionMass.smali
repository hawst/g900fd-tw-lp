.class public final Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;
.super Ljava/lang/Object;
.source "P4_BodyCompositionMass.java"


# static fields
.field public static final COMPUTING:Ljava/math/BigDecimal;

.field public static final INVALID:Ljava/math/BigDecimal;


# instance fields
.field boneMass:Ljava/math/BigDecimal;

.field muscleMass:Ljava/math/BigDecimal;

.field userProfileID:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 15
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, -0x1

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->INVALID:Ljava/math/BigDecimal;

    .line 16
    new-instance v0, Ljava/math/BigDecimal;

    const/4 v1, -0x2

    invoke-direct {v0, v1}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v2}, Ljava/math/BigDecimal;->setScale(I)Ljava/math/BigDecimal;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->COMPUTING:Ljava/math/BigDecimal;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public decodePage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 7
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 28
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    invoke-static {v2, v5}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v2

    iput v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->userProfileID:I

    .line 29
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v3, 0x6

    invoke-static {v2, v3}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom2LeBytes([BI)I

    move-result v1

    .line 30
    .local v1, "rawMuscleMass":I
    const v2, 0xffff

    if-ne v1, v2, :cond_0

    .line 31
    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->muscleMass:Ljava/math/BigDecimal;

    .line 39
    :goto_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/16 v3, 0x8

    aget-byte v2, v2, v3

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v0

    .line 40
    .local v0, "rawBoneMass":I
    const/16 v2, 0xff

    if-ne v0, v2, :cond_2

    .line 41
    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->INVALID:Ljava/math/BigDecimal;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->boneMass:Ljava/math/BigDecimal;

    .line 48
    :goto_1
    return-void

    .line 32
    .end local v0    # "rawBoneMass":I
    :cond_0
    const v2, 0xfffe

    if-ne v1, v2, :cond_1

    .line 33
    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->COMPUTING:Ljava/math/BigDecimal;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->muscleMass:Ljava/math/BigDecimal;

    goto :goto_0

    .line 35
    :cond_1
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v1}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0x64

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v4, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v5, v4}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v5, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->muscleMass:Ljava/math/BigDecimal;

    goto :goto_0

    .line 42
    .restart local v0    # "rawBoneMass":I
    :cond_2
    const/16 v2, 0xfe

    if-ne v0, v2, :cond_3

    .line 43
    sget-object v2, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->COMPUTING:Ljava/math/BigDecimal;

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->boneMass:Ljava/math/BigDecimal;

    goto :goto_1

    .line 45
    :cond_3
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v0}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v3, Ljava/math/BigDecimal;

    const/16 v4, 0xa

    invoke-direct {v3, v4}, Ljava/math/BigDecimal;-><init>(I)V

    sget-object v4, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v3, v6, v4}, Ljava/math/BigDecimal;->divide(Ljava/math/BigDecimal;ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    sget-object v3, Ljava/math/RoundingMode;->HALF_DOWN:Ljava/math/RoundingMode;

    invoke-virtual {v2, v6, v3}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v2

    iput-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->boneMass:Ljava/math/BigDecimal;

    goto :goto_1
.end method

.method public getBoneMass()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->boneMass:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getMuscleMass()Ljava/math/BigDecimal;
    .locals 1

    .prologue
    .line 22
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->muscleMass:Ljava/math/BigDecimal;

    return-object v0
.end method

.method public getUserProfileID()I
    .locals 1

    .prologue
    .line 24
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P4_BodyCompositionMass;->userProfileID:I

    return v0
.end method
