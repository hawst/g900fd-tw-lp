.class public Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;
.super Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;
.source "P2_BikeParameters.java"


# instance fields
.field private crankEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;-><init>()V

    .line 25
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xd9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;->crankEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    return-void
.end method


# virtual methods
.method public decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 16
    .param p1, "estTimestamp"    # J
    .param p3, "eventFlags"    # J
    .param p5, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 42
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;->crankEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->hasSubscribers()Z

    move-result v2

    if-nez v2, :cond_0

    .line 83
    :goto_0
    return-void

    .line 45
    :cond_0
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v14, 0x2

    aget-byte v2, v2, v14

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v13

    .line 47
    .local v13, "subpage":I
    packed-switch v13, :pswitch_data_0

    goto :goto_0

    .line 50
    :pswitch_0
    new-instance v9, Landroid/os/Bundle;

    invoke-direct {v9}, Landroid/os/Bundle;-><init>()V

    .line 51
    .local v9, "b":Landroid/os/Bundle;
    const-string v2, "long_EstTimestamp"

    move-wide/from16 v0, p1

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 52
    const-string v2, "long_EventFlags"

    move-wide/from16 v0, p3

    invoke-virtual {v9, v2, v0, v1}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 54
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v14, 0x5

    aget-byte v2, v2, v14

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v10

    .line 57
    .local v10, "crankLengthValue":I
    const/16 v2, 0xff

    if-eq v10, v2, :cond_1

    .line 58
    new-instance v2, Ljava/math/BigDecimal;

    invoke-direct {v2, v10}, Ljava/math/BigDecimal;-><init>(I)V

    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "0.5"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/math/BigDecimal;->multiply(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    new-instance v14, Ljava/math/BigDecimal;

    const-string v15, "110"

    invoke-direct {v14, v15}, Ljava/math/BigDecimal;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v14}, Ljava/math/BigDecimal;->add(Ljava/math/BigDecimal;)Ljava/math/BigDecimal;

    move-result-object v2

    const/4 v14, 0x1

    sget-object v15, Ljava/math/RoundingMode;->HALF_UP:Ljava/math/RoundingMode;

    invoke-virtual {v2, v14, v15}, Ljava/math/BigDecimal;->setScale(ILjava/math/RoundingMode;)Ljava/math/BigDecimal;

    move-result-object v3

    .line 62
    .local v3, "trueCrankLengthValue":Ljava/math/BigDecimal;
    :goto_1
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v14, 0x6

    aget-byte v12, v2, v14

    .line 63
    .local v12, "sensorStatus":B
    invoke-static {v12}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromLower2BitsOfLowerNibble(B)I

    move-result v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;

    move-result-object v4

    .line 65
    .local v4, "crankLengthStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    invoke-static {v12}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromUpper2BitsOfLowerNibble(B)I

    move-result v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;

    move-result-object v5

    .line 67
    .local v5, "sensorSwMismatchStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    invoke-static {v12}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromLower2BitsOfUpperNibble(B)I

    move-result v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;

    move-result-object v6

    .line 69
    .local v6, "sensorAvailabilityStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;
    invoke-static {v12}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFromUpper2BitsOfUpperNibble(B)I

    move-result v2

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;

    move-result-object v7

    .line 72
    .local v7, "customCalStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;
    invoke-virtual/range {p5 .. p5}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v2

    const/4 v14, 0x7

    aget-byte v2, v2, v14

    invoke-static {v2}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v11

    .line 73
    .local v11, "sensorCapabilities":I
    and-int/lit8 v2, v11, 0x1

    if-eqz v2, :cond_2

    const/4 v8, 0x1

    .line 75
    .local v8, "isAutoCrankLengthSupported":Z
    :goto_2
    const-string v14, "parcelable_CrankParameters"

    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;

    invoke-direct/range {v2 .. v8}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankParameters;-><init>(Ljava/math/BigDecimal;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;Z)V

    invoke-virtual {v9, v14, v2}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 77
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;->crankEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    goto/16 :goto_0

    .line 60
    .end local v3    # "trueCrankLengthValue":Ljava/math/BigDecimal;
    .end local v4    # "crankLengthStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    .end local v5    # "sensorSwMismatchStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    .end local v6    # "sensorAvailabilityStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;
    .end local v7    # "customCalStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;
    .end local v8    # "isAutoCrankLengthSupported":Z
    .end local v11    # "sensorCapabilities":I
    .end local v12    # "sensorStatus":B
    :cond_1
    new-instance v3, Ljava/math/BigDecimal;

    const/4 v2, -0x1

    invoke-direct {v3, v2}, Ljava/math/BigDecimal;-><init>(I)V

    .restart local v3    # "trueCrankLengthValue":Ljava/math/BigDecimal;
    goto :goto_1

    .line 73
    .restart local v4    # "crankLengthStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CrankLengthStatus;
    .restart local v5    # "sensorSwMismatchStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorSoftwareMismatchStatus;
    .restart local v6    # "sensorAvailabilityStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$SensorAvailabilityStatus;
    .restart local v7    # "customCalStatus":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusBikePowerPcc$CustomCalibrationStatus;
    .restart local v11    # "sensorCapabilities":I
    .restart local v12    # "sensorStatus":B
    :cond_2
    const/4 v8, 0x0

    goto :goto_2

    .line 47
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method

.method public getEventList()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;",
            ">;"
        }
    .end annotation

    .prologue
    .line 30
    const/4 v0, 0x1

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/bikepower/pages/P2_BikeParameters;->crankEvt:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method

.method public getPageNumbers()Ljava/util/List;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 36
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/Integer;

    const/4 v1, 0x0

    const/4 v2, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-static {v0}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v0

    return-object v0
.end method
