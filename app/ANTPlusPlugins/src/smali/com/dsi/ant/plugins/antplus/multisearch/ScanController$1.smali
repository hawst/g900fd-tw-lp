.class Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;
.super Ljava/lang/Object;
.source "ScanController.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor$IDeathHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)V
    .locals 0

    .prologue
    .line 161
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onExecutorDeath()V
    .locals 6

    .prologue
    .line 166
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mState_LOCK:Ljava/lang/Object;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->access$000(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/lang/Object;

    move-result-object v4

    monitor-enter v4

    .line 168
    :try_start_0
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    const/4 v5, 0x0

    # setter for: Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mExecutor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    invoke-static {v3, v5}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->access$102(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    .line 170
    new-instance v2, Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;

    # getter for: Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->mReceivers:Ljava/util/HashSet;
    invoke-static {v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;->access$200(Lcom/dsi/ant/plugins/antplus/multisearch/ScanController;)Ljava/util/HashSet;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    .line 171
    .local v2, "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    monitor-exit v4
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 173
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;

    .line 175
    .local v1, "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    const/4 v3, -0x4

    invoke-interface {v1, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;->onScanStopped(I)V

    goto :goto_0

    .line 171
    .end local v0    # "i$":Ljava/util/Iterator;
    .end local v1    # "receiver":Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;
    .end local v2    # "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    :catchall_0
    move-exception v3

    :try_start_1
    monitor-exit v4
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v3

    .line 177
    .restart local v0    # "i$":Ljava/util/Iterator;
    .restart local v2    # "receivers":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/dsi/ant/plugins/antplus/multisearch/ScanController$ScanResultReceiver;>;"
    :cond_0
    return-void
.end method
