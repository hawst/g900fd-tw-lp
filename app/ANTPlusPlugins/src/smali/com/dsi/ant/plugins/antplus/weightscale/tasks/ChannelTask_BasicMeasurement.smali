.class public Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;
.super Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;
.source "ChannelTask_BasicMeasurement.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field finishedLatch:Ljava/util/concurrent/CountDownLatch;

.field private inChangingWeightState:Z

.field private invalidProfilePage:[B

.field isProfileSent:Z

.field private measurementInProgress:Z

.field private msgsSinceFirstPageRequest:I

.field profileDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

.field reqBasicMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

.field private responseSent:Z

.field scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

.field scaleUserProfileSupport:Z

.field seenCapabilities:Z

.field weightResult:Ljava/math/BigDecimal;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V
    .locals 3
    .param p1, "scaleDevice"    # Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;
    .param p2, "requestor"    # Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .prologue
    const/4 v2, 0x0

    .line 60
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;-><init>()V

    .line 37
    new-instance v0, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v1, 0xc9

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->reqBasicMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    .line 39
    new-instance v0, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    invoke-direct {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;-><init>()V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->profileDecoder:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P58_UserProfile;

    .line 40
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->responseSent:Z

    .line 41
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->inChangingWeightState:Z

    .line 42
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->measurementInProgress:Z

    .line 44
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleUserProfileSupport:Z

    .line 45
    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->seenCapabilities:Z

    .line 46
    const/4 v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    .line 47
    const/16 v0, 0x8

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->invalidProfilePage:[B

    .line 61
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    .line 62
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->reqBasicMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    iget-object v1, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->accessToken:Ljava/util/UUID;

    iget-object v2, p2, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v0, v1, v2}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->subscribeToEvent(Ljava/util/UUID;Landroid/os/Messenger;)Z

    .line 63
    return-void

    .line 47
    :array_0
    .array-data 1
        0x3at
        -0x1t
        -0x1t
        -0x80t
        -0x1t
        0x0t
        0x0t
        0x0t
    .end array-data
.end method


# virtual methods
.method public doWork()V
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 274
    :try_start_0
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    invoke-virtual {v1}, Lcom/dsi/ant/channel/AntChannel;->requestChannelStatus()Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;->getChannelState()Lcom/dsi/ant/message/ChannelState;

    move-result-object v1

    sget-object v2, Lcom/dsi/ant/message/ChannelState;->TRACKING:Lcom/dsi/ant/message/ChannelState;

    if-eq v1, v2, :cond_1

    .line 276
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Failed: Channel not tracking"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 359
    :cond_0
    :goto_0
    return-void

    .line 281
    :cond_1
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->weightResult:Ljava/math/BigDecimal;

    .line 282
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v1, v1, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->resetComputedWeight()V

    .line 283
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 284
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->enableMessageProcessing()V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 288
    :try_start_1
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_1
    .catch Ljava/lang/InterruptedException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    .line 296
    :goto_1
    :try_start_2
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->seenCapabilities:Z

    if-nez v1, :cond_2

    .line 298
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Timed out receiving capabilities"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 299
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V
    :try_end_2
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_0

    .line 315
    :catch_0
    move-exception v0

    .line 318
    .local v0, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "AntCommandFailedException in dowork(): "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v0}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 319
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 320
    new-instance v1, Landroid/os/RemoteException;

    invoke-direct {v1}, Landroid/os/RemoteException;-><init>()V

    throw v1

    .line 290
    .end local v0    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :catch_1
    move-exception v0

    .line 292
    .local v0, "e":Ljava/lang/InterruptedException;
    :try_start_3
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 293
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_1

    .line 303
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_2
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    const/4 v2, 0x1

    invoke-direct {v1, v2}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 306
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleUserProfileSupport:Z

    if-eqz v1, :cond_3

    .line 308
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->invalidProfilePage:[B

    invoke-virtual {v1, v2}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V
    :try_end_3
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_3 .. :try_end_3} :catch_0

    .line 326
    :goto_2
    :try_start_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_4
    .catch Ljava/lang/InterruptedException; {:try_start_4 .. :try_end_4} :catch_3

    .line 334
    :goto_3
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->weightResult:Ljava/math/BigDecimal;

    if-eqz v1, :cond_4

    .line 335
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 343
    :goto_4
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->weightResult:Ljava/math/BigDecimal;

    if-eqz v1, :cond_0

    .line 345
    new-instance v1, Ljava/util/concurrent/CountDownLatch;

    invoke-direct {v1, v5}, Ljava/util/concurrent/CountDownLatch;-><init>(I)V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    .line 346
    iput-boolean v5, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->measurementInProgress:Z

    .line 347
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->enableMessageProcessing()V

    .line 351
    :try_start_5
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    const-wide/16 v2, 0x3c

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-virtual {v1, v2, v3, v4}, Ljava/util/concurrent/CountDownLatch;->await(JLjava/util/concurrent/TimeUnit;)Z
    :try_end_5
    .catch Ljava/lang/InterruptedException; {:try_start_5 .. :try_end_5} :catch_2

    goto/16 :goto_0

    .line 353
    :catch_2
    move-exception v0

    .line 355
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 356
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto/16 :goto_0

    .line 312
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_3
    const/4 v1, 0x1

    :try_start_6
    iput-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->isProfileSent:Z
    :try_end_6
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_6 .. :try_end_6} :catch_0

    goto :goto_2

    .line 328
    :catch_3
    move-exception v0

    .line 330
    .restart local v0    # "e":Ljava/lang/InterruptedException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v2, "Interrupted waiting for result"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 331
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Thread;->interrupt()V

    goto :goto_3

    .line 336
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_4
    iget-boolean v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->inChangingWeightState:Z

    if-eqz v1, :cond_5

    .line 337
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_WEIGHT_VALUE_NOT_STABILIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto :goto_4

    .line 339
    :cond_5
    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    goto :goto_4
.end method

.method public getTaskName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 364
    const-string v0, "Basic Measurement Request"

    return-object v0
.end method

.method public handleExecutorShutdown()V
    .locals 1

    .prologue
    .line 370
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 371
    return-void
.end method

.method public initTask()V
    .locals 0

    .prologue
    .line 267
    return-void
.end method

.method public onReceiveMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 8
    .param p1, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p2, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    .line 116
    :try_start_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 261
    :cond_0
    :goto_0
    return-void

    .line 120
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement$1;->$SwitchMap$com$dsi$ant$message$EventCode:[I

    new-instance v1, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    invoke-direct {v1, p2}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V

    invoke-virtual {v1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;->getEventCode()Lcom/dsi/ant/message/EventCode;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/EventCode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_1

    goto :goto_0

    .line 123
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Search timeout occured"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 244
    :catch_0
    move-exception v6

    .line 246
    .local v6, "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->getFailureReason()Lcom/dsi/ant/channel/AntCommandFailureReason;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/channel/AntCommandFailureReason;->TRANSFER_IN_PROGRESS:Lcom/dsi/ant/channel/AntCommandFailureReason;

    if-ne v0, v1, :cond_8

    .line 250
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "TRANSFER_PROCESSING error sending ack msg"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 128
    .end local v6    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :pswitch_2
    :try_start_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Channel closed"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 130
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto :goto_0

    .line 133
    :pswitch_3
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->isProfileSent:Z

    .line 134
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->invalidProfilePage:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V

    .line 135
    const/4 v0, 0x0

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 143
    :pswitch_4
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    iget v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v1, v1, 0x3

    rsub-int/lit8 v1, v1, 0x2

    add-int/2addr v0, v1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    goto :goto_0

    .line 154
    :pswitch_5
    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x1

    aget-byte v0, v0, v1

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/common/pages/BitManipulation;->UnsignedNumFrom1LeByte(B)I

    move-result v7

    .line 156
    .local v7, "pageNumber":I
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->seenCapabilities:Z

    if-nez v0, :cond_1

    .line 158
    if-ne v7, v2, :cond_0

    .line 160
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 161
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getScaleUserProfileExchange()Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleUserProfileSupport:Z

    .line 162
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->seenCapabilities:Z

    .line 163
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 168
    :cond_1
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->isProfileSent:Z

    if-nez v0, :cond_3

    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_3

    .line 170
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    .line 171
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    const/16 v1, 0x1e

    if-le v0, v1, :cond_2

    .line 173
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Tx retries exceeded sending user profile"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 174
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 175
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 180
    :cond_2
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    rem-int/lit8 v0, v0, 0x3

    if-nez v0, :cond_3

    .line 182
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->invalidProfilePage:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->startSendAcknowledgedData([B)V

    .line 187
    :cond_3
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->measurementInProgress:Z

    if-eqz v0, :cond_6

    .line 189
    if-ne v7, v2, :cond_5

    .line 191
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 192
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getLastBroadcastWeight()Ljava/math/BigDecimal;

    move-result-object v0

    new-instance v1, Ljava/math/BigDecimal;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Ljava/math/BigDecimal;-><init>(I)V

    invoke-virtual {v0, v1}, Ljava/math/BigDecimal;->compareTo(Ljava/math/BigDecimal;)I

    move-result v0

    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 195
    :cond_4
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Weight scale measurement complete. Now computing"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 197
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 201
    :cond_5
    const/16 v0, 0x43

    if-ne v7, v0, :cond_0

    invoke-virtual {p2}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    const/4 v1, 0x3

    aget-byte v0, v0, v1

    if-nez v0, :cond_0

    .line 203
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    const-string v1, "Weight scale measurement complete.  Now beaconing"

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 204
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 205
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 214
    :cond_6
    if-ne v7, v2, :cond_7

    .line 216
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->inChangingWeightState:Z

    .line 217
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEstimatedTimestamp()J

    move-result-wide v1

    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->getEventFlags()J

    move-result-wide v3

    move-object v5, p2

    invoke-virtual/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->decodePage(JJLcom/dsi/ant/message/ipc/AntMessageParcel;)V

    .line 218
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v0

    if-eqz v0, :cond_7

    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getLastBroadcastWeight()Ljava/math/BigDecimal;

    move-result-object v0

    sget-object v1, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->WEIGHT_VALUE_COMPUTING:Ljava/math/BigDecimal;

    if-eq v0, v1, :cond_7

    .line 222
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->inChangingWeightState:Z

    .line 223
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v0, v0, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->mainWeightDecodePage:Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/weightscale/pages/P1_MainInfoAndWeight;->getComputedBodyWeight()Ljava/math/BigDecimal;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->weightResult:Ljava/math/BigDecimal;

    .line 224
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 225
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 230
    :cond_7
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleUserProfileSupport:Z

    if-eqz v0, :cond_0

    .line 232
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    .line 233
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->msgsSinceFirstPageRequest:I

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    .line 234
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->channel:Lcom/dsi/ant/channel/AntChannel;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->invalidProfilePage:[B

    invoke-virtual {v0, v1}, Lcom/dsi/ant/channel/AntChannel;->setBroadcastData([B)V
    :try_end_1
    .catch Lcom/dsi/ant/channel/AntCommandFailedException; {:try_start_1 .. :try_end_1} :catch_0

    goto/16 :goto_0

    .line 254
    .end local v7    # "pageNumber":I
    .restart local v6    # "e":Lcom/dsi/ant/channel/AntCommandFailedException;
    :cond_8
    sget-object v0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "ACFE handling message: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v6}, Lcom/dsi/ant/channel/AntCommandFailedException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 255
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_DEVICE_COMMUNICATION_FAILURE:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 256
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->disableMessageProcessing()V

    .line 257
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->finishedLatch:Ljava/util/concurrent/CountDownLatch;

    invoke-virtual {v0}, Ljava/util/concurrent/CountDownLatch;->countDown()V

    goto/16 :goto_0

    .line 116
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_5
        :pswitch_5
    .end packed-switch

    .line 120
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public processRequest()V
    .locals 4

    .prologue
    .line 68
    const/4 v1, 0x0

    .line 71
    .local v1, "taskStarted":Z
    :try_start_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    iget-object v2, v2, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/16 v3, 0x3e8

    invoke-virtual {v2, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->startTask(Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelTask;I)Z
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v1

    .line 78
    :goto_0
    if-nez v1, :cond_0

    .line 80
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->FAIL_ALREADY_BUSY_EXTERNAL:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    invoke-virtual {p0, v2}, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V

    .line 88
    :goto_1
    return-void

    .line 72
    :catch_0
    move-exception v0

    .line 75
    .local v0, "e":Ljava/lang/InterruptedException;
    invoke-static {}, Ljava/lang/Thread;->currentThread()Ljava/lang/Thread;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Thread;->interrupt()V

    goto :goto_0

    .line 85
    .end local v0    # "e":Ljava/lang/InterruptedException;
    :cond_0
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->scaleDevice:Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;

    const/16 v3, 0x12c

    invoke-virtual {v2, v3}, Lcom/dsi/ant/plugins/antplus/weightscale/WeightScaleDevice;->setCurrentState(I)V

    goto :goto_1
.end method

.method public sendResponse(Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;)V
    .locals 5
    .param p1, "status"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    .prologue
    .line 92
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->reqBasicMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    monitor-enter v3

    .line 94
    :try_start_0
    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->responseSent:Z

    if-eqz v2, :cond_0

    .line 95
    monitor-exit v3

    .line 108
    :goto_0
    return-void

    .line 97
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->getIntValue()I

    move-result v1

    .line 99
    .local v1, "statusCode":I
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 100
    .local v0, "b":Landroid/os/Bundle;
    const-string v2, "int_statusCode"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 102
    sget-object v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;->SUCCESS:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWeightScalePcc$WeightScaleRequestStatus;

    if-ne p1, v2, :cond_1

    .line 103
    const-string v2, "decimal_bodyWeight"

    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->weightResult:Ljava/math/BigDecimal;

    invoke-virtual {v0, v2, v4}, Landroid/os/Bundle;->putSerializable(Ljava/lang/String;Ljava/io/Serializable;)V

    .line 105
    :cond_1
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->reqBasicMeasurementFinished:Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    invoke-virtual {v2, v0}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;->fireEvent(Landroid/os/Bundle;)V

    .line 106
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/weightscale/tasks/ChannelTask_BasicMeasurement;->responseSent:Z

    .line 107
    monitor-exit v3

    goto :goto_0

    .end local v0    # "b":Landroid/os/Bundle;
    .end local v1    # "statusCode":I
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2
.end method
