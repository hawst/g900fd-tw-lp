.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
.super Ljava/lang/Object;
.source "AvailableDeviceList.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DetailedDeviceInfo"
.end annotation


# instance fields
.field public final antChannelId:Lcom/dsi/ant/message/ChannelId;

.field public final antLinkPeriod:I

.field public final antLinkRfFreq:I

.field public final baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

.field private lastSeenTime:J


# direct methods
.method constructor <init>(Lcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;I)V
    .locals 2
    .param p1, "antChannelId"    # Lcom/dsi/ant/message/ChannelId;
    .param p2, "baseDeviceInfo"    # Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .param p3, "channelPeriod"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 34
    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .line 35
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->lastSeenTime:J

    .line 36
    if-gez p3, :cond_0

    const/16 p3, 0x1000

    .end local p3    # "channelPeriod":I
    :cond_0
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkPeriod:I

    .line 37
    const/16 v0, 0x32

    iput v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antLinkRfFreq:I

    .line 38
    return-void
.end method


# virtual methods
.method public getTimeSinceLastSeen()J
    .locals 4

    .prologue
    .line 42
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->lastSeenTime:J

    sub-long/2addr v0, v2

    return-wide v0
.end method

.method public updateLastSeenToNow()V
    .locals 2

    .prologue
    .line 52
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->lastSeenTime:J

    .line 53
    return-void
.end method
