.class Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;
.super Landroid/os/Handler;
.source "MultiSearchService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;->sendConnectedDevices(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

.field final synthetic val$client:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;


# direct methods
.method constructor <init>(Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;)V
    .locals 0

    .prologue
    .line 198
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService;

    iput-object p2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;->val$client:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 8
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 202
    iget v7, p1, Landroid/os/Message;->what:I

    if-eqz v7, :cond_1

    .line 219
    :cond_0
    return-void

    .line 207
    :cond_1
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v1

    .line 208
    .local v1, "data":Landroid/os/Bundle;
    const-class v7, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    invoke-virtual {v7}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v7

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 210
    const-string v7, "DevType_int"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 212
    .local v4, "deviceTypeValue":I
    invoke-static {v4}, Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;

    move-result-object v3

    .line 214
    .local v3, "deviceType":Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;
    const-string v7, "DevDbInfoList_parcelableArray"

    invoke-virtual {v1, v7}, Landroid/os/Bundle;->getParcelableArray(Ljava/lang/String;)[Landroid/os/Parcelable;

    move-result-object v0

    .local v0, "arr$":[Landroid/os/Parcelable;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v2, v0, v5

    .line 217
    .local v2, "dev":Landroid/os/Parcelable;
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/multisearch/MultiSearchService$1;->val$client:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    check-cast v2, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .end local v2    # "dev":Landroid/os/Parcelable;
    invoke-virtual {v7, v2, v3}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;->onAlreadyConnectedDevice(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/plugins/antplus/pcc/defines/DeviceType;)V

    .line 214
    add-int/lit8 v5, v5, 0x1

    goto :goto_0
.end method
