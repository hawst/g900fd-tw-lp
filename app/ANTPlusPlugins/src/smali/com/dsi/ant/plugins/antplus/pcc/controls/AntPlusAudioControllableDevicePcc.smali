.class public Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;
.super Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseControllableDevicePcc;
.source "AntPlusAudioControllableDevicePcc.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;,
        Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IpcDefines;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field mAudioCommandReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 30
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 196
    invoke-direct {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseControllableDevicePcc;-><init>()V

    return-void
.end method

.method static synthetic access$000(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->subscribeAudioCommandEvent(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;)V

    return-void
.end method

.method static synthetic access$100(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;
    .param p1, "x1"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    .prologue
    .line 27
    invoke-direct {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->setAudioCapabilities(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;)V

    return-void
.end method

.method static synthetic access$200(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;ILcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V
    .locals 0
    .param p0, "x0"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;
    .param p1, "x1"    # I
    .param p2, "x2"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;
    .param p3, "x3"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->updateAudioCommandStatus(ILcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V

    return-void
.end method

.method public static requestAccess(Landroid/content/Context;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;I)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;
    .locals 6
    .param p0, "bindToContext"    # Landroid/content/Context;
    .param p2, "stateReceiver"    # Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;
    .param p3, "audioCommandReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;
    .param p4, "capabilities"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;
    .param p5, "channelDeviceNumber"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;",
            ">;",
            "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;",
            "I)",
            "Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle",
            "<",
            "Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;",
            ">;"
        }
    .end annotation

    .prologue
    .line 152
    .local p1, "resultReceiver":Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;, "Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver<Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;>;"
    invoke-static {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->getInstalledPluginsVersionNumber(Landroid/content/Context;)I

    move-result v0

    const/16 v3, 0x2710

    if-ne v0, v3, :cond_0

    .line 154
    const-string v0, "com.dsi.ant.plugins.antplus.controls.ControlsService"

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseControllableDevicePcc$IpcDefines;->PATH_ANTPLUS_CONTROLLABLEDEVICEPLUGIN_SERVICE:Ljava/lang/String;

    .line 156
    :cond_0
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 157
    .local v1, "b":Landroid/os/Bundle;
    const-string v0, "int_RequestAccessMode"

    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getRequestAccessModeValue()I

    move-result v3

    invoke-virtual {v1, v0, v3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 158
    const-string v0, "int_ChannelDeviceId"

    invoke-virtual {v1, v0, p5}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 160
    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;

    invoke-direct {v2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;-><init>()V

    .line 162
    .local v2, "potentialRetObj":Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;
    new-instance v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$1;

    invoke-direct {v3, v2, p3, p4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$1;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;)V

    move-object v0, p0

    move-object v4, p1

    move-object v5, p2

    invoke-static/range {v0 .. v5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->requestAccess_Helper_Main(Landroid/content/Context;Landroid/os/Bundle;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$RequestAccessResultHandler;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IPluginAccessResultReceiver;Lcom/dsi/ant/plugins/antplus/pccbase/AntPluginPcc$IDeviceStateChangeReceiver;)Lcom/dsi/ant/plugins/antplus/pccbase/PccReleaseHandle;

    move-result-object v0

    return-object v0
.end method

.method private setAudioCapabilities(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;)V
    .locals 6
    .param p1, "audioDeviceCapabilities"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;

    .prologue
    .line 308
    if-nez p1, :cond_0

    .line 334
    :goto_0
    return-void

    .line 311
    :cond_0
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 312
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e24

    iput v3, v0, Landroid/os/Message;->what:I

    .line 313
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 314
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 316
    invoke-virtual {p1, v1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceCapabilities;->writeToBundle(Landroid/os/Bundle;)V

    .line 318
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 320
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_1

    .line 322
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd setAudioCapabilities died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 326
    :cond_1
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_2

    .line 329
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd setAudioCapabilities failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 330
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "setAudioCapabilities cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 333
    :cond_2
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method

.method private subscribeAudioCommandEvent(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;)V
    .locals 1
    .param p1, "AudioCommandReceiver"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;

    .prologue
    const/16 v0, 0xca

    .line 291
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->mAudioCommandReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;

    .line 292
    if-eqz p1, :cond_0

    .line 294
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->subscribeToEvent(I)Z

    .line 300
    :goto_0
    return-void

    .line 298
    :cond_0
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->unsubscribeFromEvent(I)V

    goto :goto_0
.end method

.method private updateAudioCommandStatus(ILcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V
    .locals 6
    .param p1, "sequenceNumber"    # I
    .param p2, "commandStatus"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;
    .param p3, "commandNumber"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .prologue
    .line 344
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 345
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e22

    iput v3, v0, Landroid/os/Message;->what:I

    .line 346
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 347
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 349
    const-string v3, "int_sequenceNumber"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 350
    const-string v3, "int_commandStatus"

    invoke-virtual {p2}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 351
    const-string v3, "int_commandNumber"

    invoke-virtual {p3}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 353
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 355
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_0

    .line 357
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd updateAudioCommandStatus died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 369
    :goto_0
    return-void

    .line 361
    :cond_0
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    .line 364
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd updateAudioCommandStatus failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 365
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "updateAudioCommandStatus cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 368
    :cond_1
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method


# virtual methods
.method protected getPluginPrintableName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 203
    const-string v0, "ANT+ Plugin:  Audio Controls"

    return-object v0
.end method

.method protected handlePluginEvent(Landroid/os/Message;)V
    .locals 13
    .param p1, "eventMsg"    # Landroid/os/Message;

    .prologue
    .line 209
    iget v0, p1, Landroid/os/Message;->arg1:I

    packed-switch v0, :pswitch_data_0

    .line 236
    invoke-super {p0, p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/pccbase/AntPlusBaseControllableDevicePcc;->handlePluginEvent(Landroid/os/Message;)V

    .line 240
    :cond_0
    :goto_0
    return-void

    .line 213
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->mAudioCommandReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;

    if-eqz v0, :cond_0

    .line 216
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v8

    .line 217
    .local v8, "b":Landroid/os/Bundle;
    const-string v0, "long_EstTimestamp"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v1

    .line 218
    .local v1, "estTimestamp":J
    const-string v0, "long_EventFlags"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v11

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;->getEventFlagsFromLong(J)Ljava/util/EnumSet;

    move-result-object v3

    .line 219
    .local v3, "eventFlags":Ljava/util/EnumSet;, "Ljava/util/EnumSet<Lcom/dsi/ant/plugins/antplus/pcc/defines/EventFlag;>;"
    const-string v0, "int_serialNumber"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v4

    .line 220
    .local v4, "serialNumber":I
    const-string v0, "int_commandData"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v7

    .line 221
    .local v7, "commandData":I
    const-string v0, "int_sequenceNumber"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v5

    .line 222
    .local v5, "sequenceNumber":I
    const-string v0, "int_commandNumber"

    invoke-virtual {v8, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-static {v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v6

    .line 223
    .local v6, "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->mAudioCommandReceiver:Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;

    invoke-interface/range {v0 .. v7}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$IAudioCommandReceiver;->onNewAudioCommand(JLjava/util/EnumSet;IILcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;

    move-result-object v9

    .line 224
    .local v9, "status":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;
    new-instance v10, Ljava/lang/Thread;

    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$2;

    invoke-direct {v0, p0, v5, v9, v6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc$2;-><init>(Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;ILcom/dsi/ant/plugins/antplus/pcc/controls/defines/CommandStatus;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;)V

    invoke-direct {v10, v0}, Ljava/lang/Thread;-><init>(Ljava/lang/Runnable;)V

    .line 231
    .local v10, "t":Ljava/lang/Thread;
    invoke-virtual {v10}, Ljava/lang/Thread;->start()V

    goto :goto_0

    .line 209
    nop

    :pswitch_data_0
    .packed-switch 0xca
        :pswitch_0
    .end packed-switch
.end method

.method public updateAudioStatus(IIILcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;)V
    .locals 6
    .param p1, "volume"    # I
    .param p2, "totalTrackTime"    # I
    .param p3, "currentTrackTime"    # I
    .param p4, "audioState"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;
    .param p5, "repeatState"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;
    .param p6, "shuffleState"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;

    .prologue
    .line 253
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v0

    .line 254
    .local v0, "cmdMsg":Landroid/os/Message;
    const/16 v3, 0x4e25

    iput v3, v0, Landroid/os/Message;->what:I

    .line 255
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 256
    .local v1, "params":Landroid/os/Bundle;
    invoke-virtual {v0, v1}, Landroid/os/Message;->setData(Landroid/os/Bundle;)V

    .line 258
    const-string v3, "int_volume"

    invoke-virtual {v1, v3, p1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 259
    const-string v3, "int_totalTrackTime"

    invoke-virtual {v1, v3, p2}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 260
    const-string v3, "int_currentTrackTime"

    invoke-virtual {v1, v3, p3}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 261
    const-string v3, "int_audioState"

    invoke-virtual {p4}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioDeviceState;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 262
    const-string v3, "int_repeatState"

    invoke-virtual {p5}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioRepeatState;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 263
    const-string v3, "int_shuffleState"

    invoke-virtual {p6}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioShuffleState;->getIntValue()I

    move-result v4

    invoke-virtual {v1, v3, v4}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 265
    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->sendPluginCommand(Landroid/os/Message;)Landroid/os/Message;

    move-result-object v2

    .line 267
    .local v2, "ret":Landroid/os/Message;
    if-nez v2, :cond_0

    .line 269
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    const-string v4, "Cmd updateAudioStatus died in sendPluginCommand()"

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 281
    :goto_0
    return-void

    .line 273
    :cond_0
    iget v3, v2, Landroid/os/Message;->arg1:I

    if-eqz v3, :cond_1

    .line 276
    sget-object v3, Lcom/dsi/ant/plugins/antplus/pcc/controls/AntPlusAudioControllableDevicePcc;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cmd updateAudioStatus failed with code "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, v2, Landroid/os/Message;->arg1:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 277
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "updateAudioStatus cmd failed internally"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 280
    :cond_1
    invoke-virtual {v2}, Landroid/os/Message;->recycle()V

    goto :goto_0
.end method
