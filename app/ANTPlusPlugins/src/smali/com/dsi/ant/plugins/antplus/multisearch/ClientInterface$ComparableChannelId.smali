.class Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
.super Ljava/lang/Object;
.source "ClientInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ComparableChannelId"
.end annotation


# instance fields
.field public final deviceNumber:I

.field public final deviceType:I

.field final synthetic this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;


# direct methods
.method public constructor <init>(Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;II)V
    .locals 0
    .param p2, "devNumber"    # I
    .param p3, "devType"    # I

    .prologue
    .line 462
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->this$0:Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 463
    iput p2, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceNumber:I

    .line 464
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceType:I

    .line 465
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 477
    if-ne p0, p1, :cond_1

    .line 494
    :cond_0
    :goto_0
    return v1

    .line 482
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 484
    goto :goto_0

    .line 487
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    if-nez v3, :cond_3

    move v1, v2

    .line 489
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 492
    check-cast v0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;

    .line 494
    .local v0, "other":Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v3

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v4

    if-eq v3, v4, :cond_0

    move v1, v2

    goto :goto_0
.end method

.method public getId()I
    .locals 2

    .prologue
    .line 469
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceNumber:I

    .line 470
    .local v0, "id":I
    iget v1, p0, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->deviceType:I

    shl-int/lit8 v1, v1, 0x10

    add-int/2addr v0, v1

    .line 471
    return v0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 500
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/multisearch/ClientInterface$ComparableChannelId;->getId()I

    move-result v0

    return v0
.end method
