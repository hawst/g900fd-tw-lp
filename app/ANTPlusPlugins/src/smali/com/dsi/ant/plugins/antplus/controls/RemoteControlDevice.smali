.class public Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;
.super Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;
.source "RemoteControlDevice.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice$1;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

.field private mSequenceNumber:B

.field mToken_ModeList:Ljava/util/TreeMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/TreeMap",
            "<",
            "Ljava/lang/Long;",
            "Ljava/util/UUID;",
            ">;"
        }
    .end annotation
.end field

.field private page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

.field private page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

.field private page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

.field txBuffer:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const-class v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;ILcom/dsi/ant/channel/AntChannel;)V
    .locals 3
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .param p2, "serialNumber"    # I
    .param p3, "channel"    # Lcom/dsi/ant/channel/AntChannel;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/nio/channels/ClosedChannelException;
        }
    .end annotation

    .prologue
    .line 64
    invoke-direct {p0, p1, p3}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;-><init>(Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;Lcom/dsi/ant/channel/AntChannel;)V

    .line 44
    new-instance v1, Ljava/util/TreeMap;

    invoke-direct {v1}, Ljava/util/TreeMap;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    .line 54
    const/16 v1, 0x8

    new-array v1, v1, [B

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    .line 66
    const/4 v1, 0x0

    iput-byte v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    .line 68
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    .line 69
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput p2, v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mRemoteSerialNumber:I

    .line 71
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    .line 72
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput p2, v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mRemoteSerialNumber:I

    .line 74
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;-><init>()V

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    .line 75
    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    iput p2, v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mRemoteSerialNumber:I

    .line 79
    :try_start_0
    invoke-static {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDeviceIdleTask;->createPluginDeviceExecutor(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntDevice;)Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    move-result-object v1

    iput-object v1, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 87
    return-void

    .line 80
    :catch_0
    move-exception v0

    .line 82
    .local v0, "e":Landroid/os/RemoteException;
    sget-object v1, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    const-string v2, "Remote Exception during initizalization"

    invoke-static {v1, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 83
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->onChannelDeath()V

    .line 84
    new-instance v1, Ljava/nio/channels/ClosedChannelException;

    invoke-direct {v1}, Ljava/nio/channels/ClosedChannelException;-><init>()V

    throw v1
.end method

.method public static isModeValid(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z
    .locals 2
    .param p0, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .prologue
    .line 241
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 248
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 246
    :pswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 241
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V
    .locals 11
    .param p1, "token"    # Ljava/util/UUID;
    .param p2, "msgFromPcc"    # Landroid/os/Message;

    .prologue
    const/4 v10, 0x0

    .line 92
    const/4 v5, 0x0

    .line 94
    .local v5, "noPermission":Z
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->token_ClientMap:Ljava/util/HashMap;

    invoke-virtual {v8, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;

    .line 95
    .local v1, "client":Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 96
    .local v7, "response":Landroid/os/Message;
    iget v8, p2, Landroid/os/Message;->what:I

    iput v8, v7, Landroid/os/Message;->what:I

    .line 100
    iget v8, p2, Landroid/os/Message;->what:I

    sparse-switch v8, :sswitch_data_0

    .line 196
    invoke-virtual {v7}, Landroid/os/Message;->recycle()V

    .line 197
    const/4 v7, 0x0

    .line 198
    invoke-super {p0, p1, p2}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->HandleCmdFromPcc(Ljava/util/UUID;Landroid/os/Message;)V

    .line 203
    :goto_0
    if-eqz v5, :cond_0

    .line 207
    const/4 v8, -0x4

    :try_start_0
    iput v8, v7, Landroid/os/Message;->arg1:I

    .line 208
    iget-object v8, v1, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;->responseMessenger:Landroid/os/Messenger;

    invoke-virtual {v8, v7}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 216
    :cond_0
    :goto_1
    return-void

    .line 104
    :sswitch_0
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->AUDIO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v8, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 107
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 108
    .local v0, "b":Landroid/os/Bundle;
    const-string v8, "int_commandNumber"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v3

    .line 109
    .local v3, "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    const-string v8, "int_commandData"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 111
    .local v2, "commandData":I
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;

    new-instance v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v9, 0xce

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    invoke-direct {v6, p0, v1, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V

    .line 113
    .local v6, "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    iput v10, v7, Landroid/os/Message;->arg1:I

    .line 114
    invoke-virtual {p0, v1, v7}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 117
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput v2, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandData:I

    .line 118
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput-object v3, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 119
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iget-byte v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    iput-byte v9, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mSequenceNumber:B

    .line 120
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v8, v9}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->encodePage([B)V

    .line 122
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v6, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->processRequest([B)V

    .line 123
    iget-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    goto :goto_0

    .line 127
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "commandData":I
    .end local v3    # "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .end local v6    # "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    :cond_1
    const/4 v5, 0x1

    .line 130
    goto :goto_0

    .line 133
    :sswitch_1
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->VIDEO_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v8, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v8

    if-eqz v8, :cond_2

    .line 136
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 137
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v8, "int_commandNumber"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    move-result-object v3

    .line 138
    .restart local v3    # "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    const-string v8, "int_commandData"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v2

    .line 140
    .restart local v2    # "commandData":I
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;

    new-instance v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v9, 0xcf

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    invoke-direct {v6, p0, v1, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V

    .line 142
    .restart local v6    # "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    iput v10, v7, Landroid/os/Message;->arg1:I

    .line 143
    invoke-virtual {p0, v1, v7}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 146
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput v2, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandData:I

    .line 147
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iput-object v3, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;

    .line 148
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iget-byte v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    iput-byte v9, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mSequenceNumber:B

    .line 149
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v8, v9}, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->encodePage([B)V

    .line 151
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v6, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->processRequest([B)V

    .line 152
    iget-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    goto/16 :goto_0

    .line 155
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v2    # "commandData":I
    .end local v3    # "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/AudioVideoCommandNumber;
    .end local v6    # "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    :cond_2
    const/4 v5, 0x1

    .line 158
    goto/16 :goto_0

    .line 161
    :sswitch_2
    sget-object v8, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->GENERIC_MODE:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    invoke-virtual {p0, v8, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z

    move-result v8

    if-eqz v8, :cond_3

    .line 164
    invoke-virtual {p2}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 165
    .restart local v0    # "b":Landroid/os/Bundle;
    const-string v8, "int_commandNumber"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v8

    invoke-static {v8}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;->getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

    move-result-object v3

    .line 167
    .local v3, "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;
    new-instance v6, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;

    new-instance v8, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;

    const/16 v9, 0xd0

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    invoke-direct {v8, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;-><init>(Ljava/lang/Integer;)V

    invoke-direct {v6, p0, v1, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;-><init>(Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Lcom/dsi/ant/plugins/antplus/common/AntPluginEvent;)V

    .line 169
    .restart local v6    # "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    iput v10, v7, Landroid/os/Message;->arg1:I

    .line 170
    invoke-virtual {p0, v1, v7}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->sendClientMessage(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;Landroid/os/Message;)Z

    .line 173
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    iput-object v3, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mCommandNumber:Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;

    .line 174
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    iget-byte v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    iput-byte v9, v8, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->mSequenceNumber:B

    .line 175
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page73:Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;

    iget-object v9, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v8, v9}, Lcom/dsi/ant/plugins/antplus/controls/pages/P73_GenericCommand;->encodePage([B)V

    .line 177
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->txBuffer:[B

    invoke-virtual {v6, v8}, Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;->processRequest([B)V

    .line 178
    iget-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    add-int/lit8 v8, v8, 0x1

    int-to-byte v8, v8

    iput-byte v8, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mSequenceNumber:B

    goto/16 :goto_0

    .line 181
    .end local v0    # "b":Landroid/os/Bundle;
    .end local v3    # "commandNumber":Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/GenericCommandNumber;
    .end local v6    # "req":Lcom/dsi/ant/plugins/antplus/controls/tasks/ChannelTask_ControlsCommand;
    :cond_3
    const/4 v5, 0x1

    .line 184
    goto/16 :goto_0

    .line 189
    :sswitch_3
    invoke-virtual {p0, p1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->disableMode(Ljava/util/UUID;)V

    .line 190
    invoke-virtual {p0, v1}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->removeClient(Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginDevice$ClientInfo;)V

    .line 191
    iput v10, v7, Landroid/os/Message;->arg1:I

    goto/16 :goto_0

    .line 209
    :catch_0
    move-exception v4

    .line 213
    .local v4, "e":Landroid/os/RemoteException;
    sget-object v8, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Remote Exception sending cmd response to caller pcc with token "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_1

    .line 100
    nop

    :sswitch_data_0
    .sparse-switch
        0x2712 -> :sswitch_3
        0x4e21 -> :sswitch_2
        0x4e23 -> :sswitch_0
        0x4e24 -> :sswitch_1
    .end sparse-switch
.end method

.method public closeDevice()V
    .locals 2

    .prologue
    .line 310
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->executor:Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/dsi/ant/plugins/antplus/utility/executor/AntChannelExecutor;->shutdown(Z)Lcom/dsi/ant/channel/AntChannel;

    .line 311
    invoke-super {p0}, Lcom/dsi/ant/plugins/antplus/common/devices/AntPluginAntPlusReceiver;->closeDevice()V

    .line 312
    return-void
.end method

.method public disableMode(Ljava/util/UUID;)V
    .locals 4
    .param p1, "accessToken"    # Ljava/util/UUID;

    .prologue
    .line 296
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {v2, p1}, Ljava/util/TreeMap;->containsValue(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 298
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {v2}, Ljava/util/TreeMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 300
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/UUID;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    if-ne v2, p1, :cond_0

    .line 301
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/TreeMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_0

    .line 304
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/Long;Ljava/util/UUID;>;"
    .end local v1    # "i$":Ljava/util/Iterator;
    :cond_1
    return-void
.end method

.method public enableMode(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)V
    .locals 3
    .param p1, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .param p2, "accessToken"    # Ljava/util/UUID;

    .prologue
    .line 274
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1, p2}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 276
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice$1;->$SwitchMap$com$dsi$ant$plugins$antplus$pcc$controls$defines$ControlsMode:[I

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->ordinal()I

    move-result v1

    aget v0, v0, v1

    packed-switch v0, :pswitch_data_0

    .line 292
    :goto_0
    return-void

    .line 280
    :pswitch_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Audio:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    const/4 v1, 0x0

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mControllingVideo:Z

    .line 281
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Audio mode enabled with token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 284
    :pswitch_1
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->page16Video:Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;

    const/4 v1, 0x1

    iput-boolean v1, v0, Lcom/dsi/ant/plugins/antplus/controls/pages/P16_AudioVideoCommand;->mControllingVideo:Z

    .line 285
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Video mode enabled with token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 288
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Generic mode enabled with token "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 276
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getPageList()Ljava/util/List;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/List",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;",
            ">;"
        }
    .end annotation

    .prologue
    .line 221
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 223
    .local v0, "pages":Ljava/util/List;, "Ljava/util/List<Lcom/dsi/ant/plugins/antplus/common/pages/AntPlusDataPage;>;"
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P1_AudioUpdateData;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 224
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P2_DeviceAvailability;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    new-instance v1, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;

    invoke-direct {v1}, Lcom/dsi/ant/plugins/antplus/controls/pages/P7_VideoUpdateData;-><init>()V

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 227
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->getAllCommonPages()Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 229
    return-object v0
.end method

.method public isClientModeAllowed(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;Ljava/util/UUID;)Z
    .locals 4
    .param p1, "requestedMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;
    .param p2, "accessToken"    # Ljava/util/UUID;

    .prologue
    const/4 v1, 0x0

    .line 254
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "Mode not enabled"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 266
    :goto_0
    return v0

    .line 260
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/TreeMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/UUID;

    invoke-virtual {v0, p2}, Ljava/util/UUID;->compareTo(Ljava/util/UUID;)I

    move-result v0

    if-nez v0, :cond_1

    .line 262
    const/4 v0, 0x1

    goto :goto_0

    .line 265
    :cond_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->TAG:Ljava/lang/String;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Client not authorized for"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "mode"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    move v0, v1

    .line 266
    goto :goto_0
.end method

.method public isModeInUse(Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;)Z
    .locals 3
    .param p1, "requestMode"    # Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;

    .prologue
    .line 235
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/controls/RemoteControlDevice;->mToken_ModeList:Ljava/util/TreeMap;

    invoke-virtual {p1}, Lcom/dsi/ant/plugins/antplus/pcc/controls/defines/ControlsMode;->getLongValue()J

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/TreeMap;->containsKey(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
