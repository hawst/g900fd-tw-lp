.class public final enum Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
.super Ljava/lang/Enum;
.source "AntFsDirectory.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "AntFsTimeFormat"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

.field public static final enum AUTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

.field public static final enum DATE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

.field public static final enum SYSTEM:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;


# instance fields
.field private value:I


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    const-string v1, "AUTO"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->AUTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 41
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    const-string v1, "SYSTEM"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->SYSTEM:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 46
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    const-string v1, "DATE"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->DATE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 51
    new-instance v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    const-string v1, "UNRECOGNIZED"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 31
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->AUTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->SYSTEM:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->DATE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    aput-object v1, v0, v6

    sput-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->$VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "value"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 62
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 63
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->value:I

    .line 64
    return-void
.end method

.method public static convertIntToTimeFormat(I)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    .locals 1
    .param p0, "timeFormatCode"    # I

    .prologue
    .line 73
    packed-switch p0, :pswitch_data_0

    .line 82
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    .line 83
    .local v0, "unrecognized":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    iput p0, v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->value:I

    .line 84
    .end local v0    # "unrecognized":Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    :goto_0
    return-object v0

    .line 76
    :pswitch_0
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->AUTO:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    goto :goto_0

    .line 78
    :pswitch_1
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->SYSTEM:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    goto :goto_0

    .line 80
    :pswitch_2
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->DATE:Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    goto :goto_0

    .line 73
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 31
    const-class v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;
    .locals 1

    .prologue
    .line 31
    sget-object v0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->$VALUES:[Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;

    return-object v0
.end method


# virtual methods
.method public getValue()I
    .locals 1

    .prologue
    .line 58
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/utility/antfs/AntFsDirectory$AntFsTimeFormat;->value:I

    return v0
.end method
