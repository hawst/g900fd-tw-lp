.class Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;
.super Landroid/os/Handler;
.source "AntPluginService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "RequestAccessHandler"
.end annotation


# instance fields
.field private mService:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Lcom/dsi/ant/plugins/antplus/common/AntPluginService;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/dsi/ant/plugins/antplus/common/AntPluginService;)V
    .locals 1
    .param p1, "l"    # Landroid/os/Looper;
    .param p2, "service"    # Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    .prologue
    .line 179
    invoke-direct {p0, p1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 180
    new-instance v0, Ljava/lang/ref/WeakReference;

    invoke-direct {v0, p2}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;->mService:Ljava/lang/ref/WeakReference;

    .line 181
    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 11
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 186
    invoke-virtual {p1}, Landroid/os/Message;->getData()Landroid/os/Bundle;

    move-result-object v0

    .line 187
    .local v0, "b":Landroid/os/Bundle;
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Class;->getClassLoader()Ljava/lang/ClassLoader;

    move-result-object v8

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->setClassLoader(Ljava/lang/ClassLoader;)V

    .line 189
    invoke-static {}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->getDebugLevel()I

    move-result v8

    sget-object v9, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->DEBUG:Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;

    invoke-virtual {v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt$DebugLevel;->ordinal()I

    move-result v9

    if-lt v8, v9, :cond_0

    .line 191
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "ReqAcc Handler received: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget v10, p1, Landroid/os/Message;->what:I

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 192
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Requesting PluginLib reports as v."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "string_PluginLibVersion"

    invoke-virtual {v0, v10}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->d(Ljava/lang/String;Ljava/lang/String;)V

    .line 196
    :cond_0
    const-string v8, "msgr_ReqAccResultReceiver"

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->get(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Messenger;

    .line 197
    .local v3, "msgr_ResultMessenger":Landroid/os/Messenger;
    if-nez v3, :cond_2

    .line 199
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "\'MSG_REQACC_PARAM_msgrRESULTRECEIVER\' missing from intent bundle"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 262
    :cond_1
    :goto_0
    return-void

    .line 205
    :cond_2
    iget-object v8, p0, Lcom/dsi/ant/plugins/antplus/common/AntPluginService$RequestAccessHandler;->mService:Ljava/lang/ref/WeakReference;

    invoke-virtual {v8}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;

    .line 206
    .local v5, "service":Lcom/dsi/ant/plugins/antplus/common/AntPluginService;
    if-eqz v5, :cond_3

    iget-boolean v8, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mClosed:Z

    if-eqz v8, :cond_4

    .line 208
    :cond_3
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Reqacc msg handler rcvd msg, but service is dead"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 209
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v1

    .line 210
    .local v1, "deadMsg":Landroid/os/Message;
    const/4 v8, -0x4

    iput v8, v1, Landroid/os/Message;->what:I

    .line 213
    :try_start_0
    invoke-virtual {v3, v1}, Landroid/os/Messenger;->send(Landroid/os/Message;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 214
    :catch_0
    move-exception v2

    .line 216
    .local v2, "e":Landroid/os/RemoteException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "RemoteException sending service is dead message"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 221
    .end local v1    # "deadMsg":Landroid/os/Message;
    .end local v2    # "e":Landroid/os/RemoteException;
    :cond_4
    move-object v4, v5

    .line 224
    .local v4, "pluginContext":Landroid/content/Context;
    :try_start_1
    const-string v8, "com.dsi.ant.plugins.antplus"

    const/4 v9, 0x4

    invoke-virtual {v5, v8, v9}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;

    move-result-object v4

    .line 225
    invoke-static {v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->getDebugLevel(Landroid/content/Context;)I
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    .line 232
    :goto_1
    iget v8, p1, Landroid/os/Message;->what:I

    packed-switch v8, :pswitch_data_0

    .line 255
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    const-string v9, "Unhandled message rcvd in reqacc msg handler"

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->e(Ljava/lang/String;Ljava/lang/String;)V

    .line 256
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v7

    .line 257
    .local v7, "unrecognizedMsg":Landroid/os/Message;
    const v8, -0x5f5e0ff

    iput v8, v7, Landroid/os/Message;->what:I

    .line 258
    invoke-virtual {v5, v3, v7}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto :goto_0

    .line 227
    .end local v7    # "unrecognizedMsg":Landroid/os/Message;
    :catch_1
    move-exception v2

    .line 229
    .local v2, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    # getter for: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$000()Ljava/lang/String;

    move-result-object v8

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Package not found when reading debug level."

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->i(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1

    .line 236
    .end local v2    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :pswitch_0
    iget-object v8, v5, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->mArsComm:Lcom/dsi/ant/AntService;

    if-nez v8, :cond_5

    .line 237
    # invokes: Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->connectArsAndRepost(Landroid/os/Message;Landroid/os/Messenger;)V
    invoke-static {v5, p1, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->access$100(Lcom/dsi/ant/plugins/antplus/common/AntPluginService;Landroid/os/Message;Landroid/os/Messenger;)V

    goto :goto_0

    .line 239
    :cond_5
    invoke-virtual {v5, v0, v3}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->requestAccessToken(Landroid/os/Bundle;Landroid/os/Messenger;)V

    goto :goto_0

    .line 244
    :pswitch_1
    iget v8, p1, Landroid/os/Message;->arg1:I

    if-nez v8, :cond_1

    .line 246
    iget v8, p1, Landroid/os/Message;->arg2:I

    invoke-static {v8, v4}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->setDebugLevel(ILandroid/content/Context;)V

    .line 247
    invoke-static {}, Landroid/os/Message;->obtain()Landroid/os/Message;

    move-result-object v6

    .line 248
    .local v6, "successMsg":Landroid/os/Message;
    const/4 v8, 0x0

    iput v8, v6, Landroid/os/Message;->what:I

    .line 249
    invoke-virtual {v5, v3, v6}, Lcom/dsi/ant/plugins/antplus/common/AntPluginService;->dumbfireSendResult(Landroid/os/Messenger;Landroid/os/Message;)V

    goto/16 :goto_0

    .line 232
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method
