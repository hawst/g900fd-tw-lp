.class public Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;
.super Landroid/preference/PreferenceActivity;
.source "Activity_SavedDeviceList.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Landroid/preference/PreferenceActivity;-><init>()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/preference/PreferenceActivity;->onCreate(Landroid/os/Bundle;)V

    .line 23
    const v0, 0x7f030003

    invoke-virtual {p0, v0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->addPreferencesFromResource(I)V

    .line 24
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceScreen;->setOrderingAsAdded(Z)V

    .line 25
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->refreshList()V

    .line 26
    return-void
.end method

.method public refreshList()V
    .locals 10

    .prologue
    .line 31
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    invoke-virtual {v8}, Landroid/preference/PreferenceScreen;->removeAll()V

    .line 33
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;

    invoke-direct {v1, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;-><init>(Landroid/content/Context;)V

    .line 36
    .local v1, "db":Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;
    :try_start_0
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->getPluginNamesAndIds()Ljava/util/Map;

    move-result-object v7

    .line 37
    .local v7, "sortedDevices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;>;>;"
    invoke-interface {v7}, Ljava/util/Map;->isEmpty()Z

    move-result v8

    if-eqz v8, :cond_1

    .line 39
    const-string v8, "Device database is empty"

    const/4 v9, 0x0

    invoke-static {p0, v8, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v8

    invoke-virtual {v8}, Landroid/widget/Toast;->show()V

    .line 40
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->finish()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_0
    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    .line 74
    return-void

    .line 44
    :cond_1
    :try_start_1
    invoke-interface {v7}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v8

    invoke-interface {v8}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :cond_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_0

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Map$Entry;

    .line 46
    .local v2, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;>;>;"
    new-instance v0, Landroid/preference/PreferenceCategory;

    invoke-direct {v0, p0}, Landroid/preference/PreferenceCategory;-><init>(Landroid/content/Context;)V

    .line 47
    .local v0, "ctg":Landroid/preference/PreferenceCategory;
    invoke-interface {v2}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/CharSequence;

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceCategory;->setTitle(Ljava/lang/CharSequence;)V

    .line 48
    const/4 v8, 0x0

    invoke-virtual {v0, v8}, Landroid/preference/PreferenceCategory;->setPersistent(Z)V

    .line 49
    invoke-virtual {p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v8

    invoke-virtual {v8, v0}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 52
    invoke-interface {v2}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/util/List;

    invoke-interface {v8}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v5

    .local v5, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v5}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_2

    invoke-interface {v5}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;

    .line 54
    .local v3, "i":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    new-instance v6, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;

    invoke-direct {v6, p0, v3}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;-><init>(Landroid/content/Context;Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;)V

    .line 55
    .local v6, "pref":Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;
    const-string v8, "test"

    invoke-virtual {v6, v8}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setKey(Ljava/lang/String;)V

    .line 56
    new-instance v8, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList$1;

    invoke-direct {v8, p0}, Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList$1;-><init>(Lcom/dsi/ant/plugins/antplus/utility/db/Activity_SavedDeviceList;)V

    invoke-virtual {v6, v8}, Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;->setOnPreferenceChangeListener(Landroid/preference/Preference$OnPreferenceChangeListener;)V

    .line 65
    invoke-virtual {v0, v6}, Landroid/preference/PreferenceCategory;->addPreference(Landroid/preference/Preference;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 72
    .end local v0    # "ctg":Landroid/preference/PreferenceCategory;
    .end local v2    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Ljava/util/List<Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;>;>;"
    .end local v3    # "i":Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;
    .end local v5    # "i$":Ljava/util/Iterator;
    .end local v6    # "pref":Lcom/dsi/ant/plugins/antplus/utility/db/DialogPref_ModifyDevice;
    .end local v7    # "sortedDevices":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/util/List<Lcom/dsi/ant/plugins/internal/pluginsipc/AntPluginDeviceDbProvider$DeviceDbDeviceInfo;>;>;"
    :catchall_0
    move-exception v8

    invoke-virtual {v1}, Lcom/dsi/ant/plugins/antplus/utility/db/SavedDeviceDb;->close()V

    throw v8
.end method
