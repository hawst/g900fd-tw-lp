.class public Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;
.super Ljava/lang/Object;
.source "WatchCommunicatorDatabase.java"

# interfaces
.implements Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;
    }
.end annotation


# static fields
.field private static final DATABASE_NAME:Ljava/lang/String; = "watch_antfs.db"

.field private static final DATABASE_VERSION:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private dbContext:Landroid/content/Context;

.field private mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

.field private serialNumberMismatchOccured:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-class v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "contextForDb"    # Landroid/content/Context;

    .prologue
    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    .line 29
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    .line 96
    iput-object p1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->dbContext:Landroid/content/Context;

    .line 97
    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method private getDeviceDbId(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Landroid/database/sqlite/SQLiteDatabase;)J
    .locals 5
    .param p1, "device"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .param p2, "openDb"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 347
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "SELECT AntFsDeviceInfo_Id FROM AntFsDeviceInfo WHERE (PluginUUID == \'"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "\');"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {p2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 350
    .local v0, "cPlugin":Landroid/database/Cursor;
    const-wide/16 v1, -0x1

    .line 351
    .local v1, "dev_DbId":J
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 353
    const/4 v3, 0x0

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v1

    .line 355
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 356
    return-wide v1
.end method


# virtual methods
.method public close()V
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->close()V

    .line 109
    return-void
.end method

.method public getDevice(III)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    .locals 7
    .param p1, "antFsManufacturerId"    # I
    .param p2, "antFsDeviceType"    # I
    .param p3, "antDeviceNumber"    # I

    .prologue
    .line 251
    iget-object v5, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 252
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "SELECT AntFsDeviceInfo_Id, PluginUUID, DisplayName FROM AntFsDeviceInfo WHERE (AntFsManufacturerId == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND AntFsDeviceType == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AND AntDeviceNumber == "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ");"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    const/4 v6, 0x0

    invoke-virtual {v1, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 259
    .local v0, "cPlugin":Landroid/database/Cursor;
    const/4 v2, 0x0

    .line 260
    .local v2, "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 262
    const/4 v5, 0x1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Ljava/util/UUID;->fromString(Ljava/lang/String;)Ljava/util/UUID;

    move-result-object v4

    .line 263
    .local v4, "pluginUUID":Ljava/util/UUID;
    const/4 v5, 0x2

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 264
    .local v3, "displayName":Ljava/lang/String;
    new-instance v2, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    .end local v2    # "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    invoke-direct {v2, v4, p1, p2, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;-><init>(Ljava/util/UUID;IILjava/lang/String;)V

    .line 267
    .end local v3    # "displayName":Ljava/lang/String;
    .end local v4    # "pluginUUID":Ljava/util/UUID;
    .restart local v2    # "di":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;
    :cond_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 268
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 269
    return-object v2
.end method

.method public getDevice(Ljava/util/UUID;)Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .locals 10
    .param p1, "deviceUUID"    # Ljava/util/UUID;

    .prologue
    const/4 v9, 0x0

    .line 221
    iget-object v7, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v7}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v3

    .line 222
    .local v3, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "SELECT AntDeviceNumber, AntFsManufacturerId, AntFsDeviceType, DisplayName FROM AntFsDeviceInfo WHERE (PluginUUID == "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, ");"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x0

    invoke-virtual {v3, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    .line 227
    .local v2, "cPlugin":Landroid/database/Cursor;
    const/4 v5, 0x0

    .line 228
    .local v5, "di":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    invoke-interface {v2}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 230
    invoke-interface {v2, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 231
    .local v4, "deviceNumber":I
    const/4 v7, 0x1

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 232
    .local v1, "antFsManufacturerId":I
    const/4 v7, 0x2

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    .line 233
    .local v0, "antFsDeviceType":I
    const/4 v7, 0x3

    invoke-interface {v2, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 234
    .local v6, "displayName":Ljava/lang/String;
    new-instance v5, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .end local v5    # "di":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    new-instance v7, Lcom/dsi/ant/message/ChannelId;

    invoke-direct {v7, v4, v9, v9}, Lcom/dsi/ant/message/ChannelId;-><init>(III)V

    new-instance v8, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-direct {v8, p1, v1, v0, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;-><init>(Ljava/util/UUID;IILjava/lang/String;)V

    const/4 v9, -0x1

    invoke-direct {v5, v7, v8, v9}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;-><init>(Lcom/dsi/ant/message/ChannelId;Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;I)V

    .line 237
    .end local v0    # "antFsDeviceType":I
    .end local v1    # "antFsManufacturerId":I
    .end local v4    # "deviceNumber":I
    .end local v6    # "displayName":Ljava/lang/String;
    .restart local v5    # "di":Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    :cond_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    .line 238
    invoke-virtual {v3}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 239
    return-object v5
.end method

.method public getLastDownloadGarminTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J
    .locals 9
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "device"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 361
    iget-object v6, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v6}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 364
    .local v1, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, p2, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getDeviceDbId(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v2

    .line 366
    .local v2, "dev_DbId":J
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "SELECT LastDownloadedGarminTime FROM LastDownloadRecords, Applications WHERE (Applications.AppPkgName == \'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, "\'"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " AND Applications.App_Id == LastDownloadRecords.App_Id);"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v1, v6, v7}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    .line 373
    .local v0, "cPlugin":Landroid/database/Cursor;
    invoke-interface {v0}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 375
    const/4 v6, 0x0

    invoke-interface {v0, v6}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 381
    .local v4, "lastDownloadGarminTime":J
    :goto_0
    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 383
    sget-object v6, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->TAG:Ljava/lang/String;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "Db last time for "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget-object v8, p2, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v8}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v8

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v6, v7}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V

    .line 384
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 385
    return-wide v4

    .line 379
    .end local v4    # "lastDownloadGarminTime":J
    :cond_0
    const-wide/16 v4, 0x0

    .restart local v4    # "lastDownloadGarminTime":J
    goto :goto_0
.end method

.method public getPasskey(IIIJ)Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    .locals 14
    .param p1, "antFsManufacturerId"    # I
    .param p2, "antFsDeviceType"    # I
    .param p3, "antDeviceNumber"    # I
    .param p4, "antFsSerialNumber"    # J

    .prologue
    .line 121
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    .line 122
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v11

    .line 123
    .local v11, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SELECT AntFsDeviceInfo_Id, Passkey, AntFsSerialNumber FROM AntFsDeviceInfo WHERE (AntFsManufacturerId == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND AntFsDeviceType == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND AntDeviceNumber == "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move/from16 v0, p3

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " );"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v11, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 130
    .local v10, "cPlugin":Landroid/database/Cursor;
    const/4 v1, 0x0

    .line 131
    .local v1, "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 133
    const/4 v2, 0x0

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 136
    .local v12, "device_dbID":I
    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 138
    new-instance v13, Landroid/content/ContentValues;

    invoke-direct {v13}, Landroid/content/ContentValues;-><init>()V

    .line 139
    .local v13, "values":Landroid/content/ContentValues;
    const-string v2, "AntFsSerialNumber"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v13, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 140
    const-string v2, "AntFsDeviceInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AntFsDeviceInfo_Id == "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v5, 0x0

    invoke-virtual {v11, v2, v13, v3, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_1

    .line 142
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "SQL Updating ANTFS serial number failed to update exactly 1 row"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 150
    .end local v13    # "values":Landroid/content/ContentValues;
    :cond_0
    const/4 v2, 0x2

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    cmp-long v2, v2, p4

    if-eqz v2, :cond_1

    .line 152
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    .line 157
    :cond_1
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->isNull(I)Z

    move-result v2

    if-nez v2, :cond_2

    iget-boolean v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    if-nez v2, :cond_2

    .line 160
    const/4 v2, 0x1

    invoke-interface {v10, v2}, Landroid/database/Cursor;->getBlob(I)[B

    move-result-object v4

    .line 161
    .local v4, "passkey":[B
    new-instance v1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .end local v1    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    int-to-long v2, v12

    move v5, p1

    move/from16 v6, p2

    move/from16 v7, p3

    move-wide/from16 v8, p4

    invoke-direct/range {v1 .. v9}, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;-><init>(J[BIIIJ)V

    .line 165
    .end local v4    # "passkey":[B
    .end local v12    # "device_dbID":I
    .restart local v1    # "pi":Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;
    :cond_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    .line 166
    invoke-virtual {v11}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 167
    return-object v1
.end method

.method public insertDeviceIfNotExist(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;)J
    .locals 6
    .param p1, "deviceInfo"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;

    .prologue
    .line 274
    iget-object v4, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v4}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 276
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-direct {p0, p1, v0}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getDeviceDbId(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v1

    .line 277
    .local v1, "dbID":J
    const-wide/16 v4, -0x1

    cmp-long v4, v1, v4

    if-nez v4, :cond_0

    .line 279
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 280
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "AntFsManufacturerId"

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getAntfsManufacturerId()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 281
    const-string v4, "AntFsDeviceType"

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getAntfsDeviceType()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 282
    const-string v4, "AntDeviceNumber"

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v5}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 283
    const-string v4, "PluginUUID"

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDeviceUUID()Ljava/util/UUID;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 284
    const-string v4, "DisplayName"

    iget-object v5, p1, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->baseDeviceInfo:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;

    invoke-virtual {v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusWatchDownloaderPcc$DeviceInfo;->getDisplayName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 286
    const-string v4, "AntFsDeviceInfo"

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5, v3}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v1

    .line 288
    .end local v3    # "values":Landroid/content/ContentValues;
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 290
    return-wide v1
.end method

.method public insertOrUpdatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 7
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    .line 178
    const/4 v3, 0x0

    iput-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    .line 179
    iget-object v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 181
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v2, Landroid/content/ContentValues;

    invoke-direct {v2}, Landroid/content/ContentValues;-><init>()V

    .line 182
    .local v2, "values":Landroid/content/ContentValues;
    const-string v3, "Passkey"

    iget-object v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsPasskey:[B

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 183
    const-string v3, "AntFsDeviceInfo"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "AntFsManufacturerId == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsManufacturerId:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND AntFsDeviceType == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsDeviceType:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND AntDeviceNumber == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget v5, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antDeviceNumber:I

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    const-string v5, " AND AntFsSerialNumber == "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-wide v5, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->antFsSerialNumber:J

    invoke-virtual {v4, v5, v6}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v0, v3, v2, v4, v5}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    .line 189
    .local v1, "numMatches":I
    const/4 v3, 0x1

    if-eq v1, v3, :cond_1

    iget-boolean v3, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    if-eqz v3, :cond_0

    if-eqz v1, :cond_1

    .line 193
    :cond_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    const-string v4, "Attempted to add passkey to non existent device."

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 195
    :cond_1
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 196
    return-void
.end method

.method public invalidatePasskey(Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;)V
    .locals 6
    .param p1, "passkeyInfo"    # Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;

    .prologue
    .line 204
    iget-object v2, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v2}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    .line 205
    .local v0, "db":Landroid/database/sqlite/SQLiteDatabase;
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 206
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "Passkey"

    invoke-virtual {v1, v2}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 207
    const-string v2, "AntFsDeviceInfo"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "AntFsDeviceInfo_Id == "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-wide v4, p1, Lcom/dsi/ant/plugins/antplus/utility/antfs/IAntFsPasskeyDatabase$PasskeyInfo;->passkey_dbID:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v0, v2, v1, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    const/4 v3, 0x1

    if-eq v2, v3, :cond_0

    .line 209
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "SQL update() failed to clear exactly 1 passkey"

    invoke-direct {v2, v3}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 211
    :cond_0
    invoke-virtual {v0}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 212
    return-void
.end method

.method public open()V
    .locals 2

    .prologue
    .line 102
    new-instance v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    iget-object v1, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->dbContext:Landroid/content/Context;

    invoke-direct {v0, v1}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    .line 103
    return-void
.end method

.method public serialNumberMismatchOccured()Z
    .locals 1

    .prologue
    .line 392
    iget-boolean v0, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->serialNumberMismatchOccured:Z

    return v0
.end method

.method public setLastDownloadTime(Ljava/lang/String;Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;J)V
    .locals 14
    .param p1, "appPkgName"    # Ljava/lang/String;
    .param p2, "device"    # Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;
    .param p3, "garminTime"    # J

    .prologue
    .line 295
    iget-object v11, p0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->mDb:Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;

    invoke-virtual {v11}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase$DbHelper;->getWritableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 296
    .local v5, "db":Landroid/database/sqlite/SQLiteDatabase;
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->beginTransaction()V

    .line 300
    :try_start_0
    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "SELECT App_Id FROM Applications WHERE (AppPkgName == \'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, "\'"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ");"

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v4

    .line 306
    .local v4, "cPlugin":Landroid/database/Cursor;
    invoke-interface {v4}, Landroid/database/Cursor;->moveToNext()Z

    move-result v11

    if-eqz v11, :cond_1

    .line 308
    const/4 v11, 0x0

    invoke-interface {v4, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 309
    .local v2, "app_DbId":J
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 321
    :goto_0
    move-object/from16 v0, p2

    invoke-direct {p0, v0, v5}, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->getDeviceDbId(Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;Landroid/database/sqlite/SQLiteDatabase;)J

    move-result-wide v6

    .line 324
    .local v6, "dev_DbId":J
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 325
    .local v10, "values":Landroid/content/ContentValues;
    const-string v11, "LastDownloadedGarminTime"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 326
    const-string v11, "LastDownloadRecords"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "App_Id == "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " AND AntFsDeviceInfo_Id == "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    invoke-virtual {v5, v11, v10, v12, v13}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v11

    int-to-long v8, v11

    .line 328
    .local v8, "updateId":J
    const-wide/16 v11, 0x0

    cmp-long v11, v8, v11

    if-nez v11, :cond_0

    .line 330
    const-string v11, "App_Id"

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 331
    const-string v11, "AntFsDeviceInfo_Id"

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v10, v11, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 332
    const-string v11, "LastDownloadRecords"

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    .line 334
    :cond_0
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->setTransactionSuccessful()V

    .line 335
    sget-object v11, Lcom/dsi/ant/plugins/antplus/watchcommunicator/WatchCommunicatorDatabase;->TAG:Ljava/lang/String;

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Setting last time for "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-object/from16 v0, p2

    iget-object v13, v0, Lcom/dsi/ant/plugins/antplus/watchcommunicator/AvailableDeviceList$DetailedDeviceInfo;->antChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v13}, Lcom/dsi/ant/message/ChannelId;->getDeviceNumber()I

    move-result v13

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, " to "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    move-wide/from16 v0, p3

    invoke-virtual {v12, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Lcom/dsi/ant/plugins/utility/log/LogAnt;->v(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 339
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 340
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    .line 342
    return-void

    .line 313
    .end local v2    # "app_DbId":J
    .end local v6    # "dev_DbId":J
    .end local v8    # "updateId":J
    .end local v10    # "values":Landroid/content/ContentValues;
    :cond_1
    :try_start_1
    invoke-interface {v4}, Landroid/database/Cursor;->close()V

    .line 315
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 316
    .restart local v10    # "values":Landroid/content/ContentValues;
    const-string v11, "AppPkgName"

    invoke-virtual {v10, v11, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 317
    const-string v11, "Applications"

    const/4 v12, 0x0

    invoke-virtual {v5, v11, v12, v10}, Landroid/database/sqlite/SQLiteDatabase;->insertOrThrow(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-wide v2

    .restart local v2    # "app_DbId":J
    goto/16 :goto_0

    .line 339
    .end local v2    # "app_DbId":J
    .end local v4    # "cPlugin":Landroid/database/Cursor;
    .end local v10    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v11

    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->endTransaction()V

    .line 340
    invoke-virtual {v5}, Landroid/database/sqlite/SQLiteDatabase;->close()V

    throw v11
.end method
