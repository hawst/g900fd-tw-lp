.class public final enum Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
.super Ljava/lang/Enum;
.source "AntPlusFitnessEquipmentPcc.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "HeartRateDataSource"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

.field public static final enum ANTPLUS_HRM:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

.field public static final enum EM_5KHz:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

.field public static final enum HAND_CONTACT_SENSOR:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

.field public static final enum UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

.field public static final enum UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;


# instance fields
.field private intValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1897
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    const-string v1, "HAND_CONTACT_SENSOR"

    invoke-direct {v0, v1, v3, v6}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->HAND_CONTACT_SENSOR:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1901
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    const-string v1, "EM_5KHz"

    invoke-direct {v0, v1, v4, v5}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->EM_5KHz:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1905
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    const-string v1, "ANTPLUS_HRM"

    invoke-direct {v0, v1, v5, v4}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->ANTPLUS_HRM:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1909
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    const-string v1, "UNKNOWN"

    invoke-direct {v0, v1, v6, v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1914
    new-instance v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    const-string v1, "UNRECOGNIZED"

    const/4 v2, -0x1

    invoke-direct {v0, v1, v7, v2}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1892
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->HAND_CONTACT_SENSOR:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->EM_5KHz:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->ANTPLUS_HRM:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->UNKNOWN:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    aput-object v1, v0, v7

    sput-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "intValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 1919
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 1920
    iput p3, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->intValue:I

    .line 1921
    return-void
.end method

.method public static getValueFromInt(I)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    .locals 6
    .param p0, "intValue"    # I

    .prologue
    .line 1939
    invoke-static {}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 1941
    .local v3, "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    invoke-virtual {v3}, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->getIntValue()I

    move-result v5

    if-ne v5, p0, :cond_0

    .line 1947
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    :goto_1
    return-object v3

    .line 1939
    .restart local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1945
    .end local v3    # "source":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    :cond_1
    sget-object v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->UNRECOGNIZED:Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    .line 1946
    .local v4, "unrecognized":Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    iput p0, v4, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->intValue:I

    move-object v3, v4

    .line 1947
    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 1892
    const-class v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;
    .locals 1

    .prologue
    .line 1892
    sget-object v0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->$VALUES:[Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    invoke-virtual {v0}, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;

    return-object v0
.end method


# virtual methods
.method public getIntValue()I
    .locals 1

    .prologue
    .line 1929
    iget v0, p0, Lcom/dsi/ant/plugins/antplus/pcc/AntPlusFitnessEquipmentPcc$HeartRateDataSource;->intValue:I

    return v0
.end method
