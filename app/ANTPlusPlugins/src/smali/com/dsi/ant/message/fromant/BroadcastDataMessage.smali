.class public Lcom/dsi/ant/message/fromant/BroadcastDataMessage;
.super Lcom/dsi/ant/message/fromant/DataMessage;
.source "BroadcastDataMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 32
    sget-object v0, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p1}, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;->extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;-><init>([B)V

    .line 33
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 36
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/DataMessage;-><init>([B)V

    .line 37
    return-void
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 41
    sget-object v0, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method
