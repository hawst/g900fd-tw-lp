.class public final Lcom/dsi/ant/message/fromhost/LibConfigMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "LibConfigMessage.java"


# static fields
.field public static final BITMASK_CHANNEL_ID_OUTPUT:I = 0x80

.field public static final BITMASK_RSSI_OUTPUT:I = 0x40

.field public static final BITMASK_RX_TIMESTAMP:I = 0x20

.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_LIBCONFIG_FLAGS:I = 0x1

.field public static final SIZE_LIBCONFIG_FLAGS:I = 0x1


# instance fields
.field private final mLibConfig:Lcom/dsi/ant/message/LibConfig;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/LibConfig;)V
    .locals 0
    .param p1, "libConfig"    # Lcom/dsi/ant/message/LibConfig;

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    .line 55
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 5
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v4, 0x1

    .line 58
    new-instance v0, Lcom/dsi/ant/message/LibConfig;

    const/16 v1, 0x80

    invoke-static {v1, p1, v4}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v1

    const/16 v2, 0x40

    invoke-static {v2, p1, v4}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v2

    const/16 v3, 0x20

    invoke-static {v3, p1, v4}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v3

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LibConfig;-><init>(ZZZ)V

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;-><init>(Lcom/dsi/ant/message/LibConfig;)V

    .line 63
    return-void
.end method


# virtual methods
.method public getLibConfig()Lcom/dsi/ant/message/LibConfig;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    return-object v0
.end method

.method public getMessageContent(II)[B
    .locals 6
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    .line 75
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 77
    .local v0, "content":[B
    const-wide/16 v3, 0x0

    invoke-static {v3, v4, v0, v5, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 79
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v1}, Lcom/dsi/ant/message/LibConfig;->getEnableRxTimestampOutput()Z

    move-result v1

    if-eqz v1, :cond_1

    const/16 v1, 0x20

    :goto_0
    iget-object v3, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v3}, Lcom/dsi/ant/message/LibConfig;->getEnableRssiOutput()Z

    move-result v3

    if-eqz v3, :cond_2

    const/16 v3, 0x40

    :goto_1
    or-int/2addr v1, v3

    iget-object v3, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v3}, Lcom/dsi/ant/message/LibConfig;->getEnableChannelIdOutput()Z

    move-result v3

    if-eqz v3, :cond_0

    const/16 v2, 0x80

    :cond_0
    or-int/2addr v1, v2

    invoke-static {v1, v0, v5}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 87
    return-object v0

    :cond_1
    move v1, v2

    .line 79
    goto :goto_0

    :cond_2
    move v3, v2

    goto :goto_1
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 93
    sget-object v0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 98
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 101
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;->mLibConfig:Lcom/dsi/ant/message/LibConfig;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 103
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
