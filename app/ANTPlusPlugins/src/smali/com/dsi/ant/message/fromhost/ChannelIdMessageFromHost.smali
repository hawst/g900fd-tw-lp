.class public final Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ChannelIdMessageFromHost.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_CHANNEL_ID:I = 0x1


# instance fields
.field private final mChannelId:Lcom/dsi/ant/message/ChannelId;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ChannelId;)V
    .locals 0
    .param p1, "channelId"    # Lcom/dsi/ant/message/ChannelId;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 35
    iput-object p1, p0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 36
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    .line 38
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 39
    new-instance v0, Lcom/dsi/ant/message/ChannelId;

    const/4 v1, 0x1

    invoke-direct {v0, p1, v1}, Lcom/dsi/ant/message/ChannelId;-><init>([BI)V

    iput-object v0, p0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    .line 40
    return-void
.end method


# virtual methods
.method public getChannelId()Lcom/dsi/ant/message/ChannelId;
    .locals 1

    .prologue
    .line 48
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    return-object v0
.end method

.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x0

    .line 53
    const/4 v1, 0x5

    new-array v0, v1, [B

    .line 55
    .local v0, "messageContent":[B
    invoke-static {p1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 56
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->getMessageContent()[B

    move-result-object v1

    const/4 v2, 0x1

    const/4 v3, 0x4

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 59
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 65
    sget-object v0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 71
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 73
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;->mChannelId:Lcom/dsi/ant/message/ChannelId;

    invoke-virtual {v1}, Lcom/dsi/ant/message/ChannelId;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 76
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
