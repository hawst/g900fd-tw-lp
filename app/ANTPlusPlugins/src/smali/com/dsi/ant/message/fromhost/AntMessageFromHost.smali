.class public abstract Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageFromHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/AntMessageFromHost$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    .line 266
    return-void
.end method

.method protected static createAntMessage(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .locals 3
    .param p0, "messageType"    # Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .param p1, "messageContent"    # [B

    .prologue
    .line 112
    const/4 v0, 0x0

    .line 114
    .local v0, "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    sget-object v1, Lcom/dsi/ant/message/fromhost/AntMessageFromHost$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 269
    :goto_0
    :pswitch_0
    return-object v0

    .line 118
    :pswitch_1
    invoke-static {p0, p1}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->createDataMessageFromHost(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/DataMessageFromHost;

    move-result-object v0

    .line 119
    goto :goto_0

    .line 121
    :pswitch_2
    new-instance v0, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/AddChannelIdToListMessage;-><init>([B)V

    .line 122
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 130
    :pswitch_3
    new-instance v0, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/AssignChannelMessage;-><init>([B)V

    .line 131
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 133
    :pswitch_4
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ChannelIdMessageFromHost;-><init>([B)V

    .line 134
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 136
    :pswitch_5
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;-><init>([B)V

    .line 137
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 139
    :pswitch_6
    new-instance v0, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;-><init>([B)V

    .line 140
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 145
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/CloseChannelMessage;-><init>([B)V

    .line 146
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 151
    :pswitch_8
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ConfigureEventBufferMessage;-><init>([B)V

    .line 152
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 166
    :pswitch_9
    new-instance v0, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ConfigIdListMessage;-><init>([B)V

    .line 167
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 169
    :pswitch_a
    new-instance v0, Lcom/dsi/ant/message/fromhost/CrystalEnableMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/CrystalEnableMessage;-><init>([B)V

    .line 170
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 172
    :pswitch_b
    new-instance v0, Lcom/dsi/ant/message/fromhost/CwInitMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/CwInitMessage;-><init>([B)V

    .line 173
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 196
    :pswitch_c
    new-instance v0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;-><init>([B)V

    .line 197
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 202
    :pswitch_d
    new-instance v0, Lcom/dsi/ant/message/fromhost/LibConfigMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/LibConfigMessage;-><init>([B)V

    .line 203
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 208
    :pswitch_e
    new-instance v0, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/LowPrioritySearchTimeoutMessage;-><init>([B)V

    .line 209
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 211
    :pswitch_f
    new-instance v0, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/OpenChannelMessage;-><init>([B)V

    .line 212
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 214
    :pswitch_10
    new-instance v0, Lcom/dsi/ant/message/fromhost/OpenRxScanModeMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/OpenRxScanModeMessage;-><init>([B)V

    .line 215
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 217
    :pswitch_11
    new-instance v0, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ProximitySearchMessage;-><init>([B)V

    .line 218
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 220
    :pswitch_12
    new-instance v0, Lcom/dsi/ant/message/fromhost/RequestMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/RequestMessage;-><init>([B)V

    .line 221
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 223
    :pswitch_13
    new-instance v0, Lcom/dsi/ant/message/fromhost/ResetSystemMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/ResetSystemMessage;-><init>([B)V

    .line 224
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 226
    :pswitch_14
    new-instance v0, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/SearchTimeoutMessage;-><init>([B)V

    .line 227
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 232
    :pswitch_15
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/SetChannelTransmitPowerMessage;-><init>([B)V

    .line 233
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto :goto_0

    .line 243
    :pswitch_16
    invoke-static {p0, p1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->createSetNetworkKeyMessage(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    move-result-object v0

    .line 245
    goto/16 :goto_0

    .line 253
    :pswitch_17
    new-instance v0, Lcom/dsi/ant/message/fromhost/SleepMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/SleepMessage;-><init>([B)V

    .line 254
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto/16 :goto_0

    .line 259
    :pswitch_18
    new-instance v0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;-><init>([B)V

    .line 260
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto/16 :goto_0

    .line 262
    :pswitch_19
    new-instance v0, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromhost/UnassignChannelMessage;-><init>([B)V

    .line 263
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    goto/16 :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_0
        :pswitch_7
        :pswitch_0
        :pswitch_8
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_c
        :pswitch_0
        :pswitch_d
        :pswitch_0
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_0
        :pswitch_15
        :pswitch_0
        :pswitch_0
        :pswitch_16
        :pswitch_16
        :pswitch_0
        :pswitch_0
        :pswitch_17
        :pswitch_0
        :pswitch_18
        :pswitch_19
    .end packed-switch
.end method

.method public static createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;Lcom/dsi/ant/message/ChannelType;)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .locals 2
    .param p0, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .param p1, "channelType"    # Lcom/dsi/ant/message/ChannelType;

    .prologue
    .line 106
    invoke-static {p0, p1}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->create(Lcom/dsi/ant/message/ipc/AntMessageParcel;Lcom/dsi/ant/message/ChannelType;)Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v0

    .line 108
    .local v0, "messageType":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->createAntMessage(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getMessageContent()[B
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 47
    invoke-virtual {p0, v0, v0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    return-object v0
.end method

.method public getMessageContent(I)[B
    .locals 1
    .param p1, "channelNumber"    # I

    .prologue
    .line 59
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    return-object v0
.end method

.method public abstract getMessageContent(II)[B
.end method

.method public getMessageId()I
    .locals 1

    .prologue
    .line 28
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->getMessageId()I

    move-result v0

    return v0
.end method

.method public abstract getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
.end method

.method public getRawMessage(II)[B
    .locals 6
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v5, 0x0

    .line 83
    invoke-virtual {p0, p1, p2}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContent(II)[B

    move-result-object v0

    .line 84
    .local v0, "messageContent":[B
    array-length v2, v0

    .line 85
    .local v2, "messageLength":I
    add-int/lit8 v4, v2, 0x2

    new-array v3, v4, [B

    .line 86
    .local v3, "rawMessage":[B
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageId()I

    move-result v1

    .line 88
    .local v1, "messageId":I
    invoke-static {v2, v3, v5}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 89
    const/4 v4, 0x1

    invoke-static {v1, v3, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 90
    const/4 v4, 0x2

    invoke-static {v0, v5, v3, v4, v2}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 93
    return-object v3
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 277
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toStringHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 272
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;->getMessageIdString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
