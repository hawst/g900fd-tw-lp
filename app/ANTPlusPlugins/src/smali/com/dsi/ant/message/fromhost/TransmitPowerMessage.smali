.class public final Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "TransmitPowerMessage.java"


# static fields
.field public static final MIN_TRANSMIT_POWER_LEVEL:I = 0x0

.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_TX_POWER:I = 0x1

.field public static final SIZE_TX_POWER:I = 0x1


# instance fields
.field private final mOutputPowerLevelSetting:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(ILcom/dsi/ant/channel/Capabilities;)V
    .locals 2
    .param p1, "outputPowerLevelSetting"    # I
    .param p2, "capabilities"    # Lcom/dsi/ant/channel/Capabilities;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 43
    const/4 v0, 0x0

    invoke-virtual {p2}, Lcom/dsi/ant/channel/Capabilities;->getMaxOutputPowerLevelSetting()I

    move-result v1

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transmit power level setting is out of range."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 48
    :cond_0
    iput p1, p0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->mOutputPowerLevelSetting:I

    .line 49
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 51
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 52
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->mOutputPowerLevelSetting:I

    .line 53
    return-void
.end method


# virtual methods
.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 58
    const/4 v1, 0x2

    new-array v0, v1, [B

    .line 60
    .local v0, "content":[B
    const-wide/16 v1, 0x0

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 62
    iget v1, p0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->mOutputPowerLevelSetting:I

    int-to-byte v1, v1

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 64
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 70
    sget-object v0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 77
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 78
    const-string v1, "Transmit power setting="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/TransmitPowerMessage;->mOutputPowerLevelSetting:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 80
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
