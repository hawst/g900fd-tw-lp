.class final enum Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;
.super Ljava/lang/Enum;
.source "LibConfig.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/dsi/ant/message/LibConfig;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "LibConfigArrayIndex"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

.field public static final enum CHANNEL_ID:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

.field public static final enum NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

.field public static final enum RSSI:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

.field public static final enum RX_TIMESTAMP:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 247
    new-instance v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    const-string v1, "CHANNEL_ID"

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->CHANNEL_ID:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    .line 248
    new-instance v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    const-string v1, "RSSI"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RSSI:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    .line 249
    new-instance v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    const-string v1, "RX_TIMESTAMP"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RX_TIMESTAMP:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    .line 250
    new-instance v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    const-string v1, "NUMBER_OF_DETAILS"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    .line 246
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->CHANNEL_ID:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RSSI:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->RX_TIMESTAMP:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->NUMBER_OF_DETAILS:Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    aput-object v1, v0, v5

    sput-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->$VALUES:[Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 246
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 246
    const-class v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;
    .locals 1

    .prologue
    .line 246
    sget-object v0, Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->$VALUES:[Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/LibConfig$LibConfigArrayIndex;

    return-object v0
.end method
