.class public abstract Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.super Lcom/dsi/ant/message/AntMessage;
.source "AntMessageFromAnt.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromant/AntMessageFromAnt$1;
    }
.end annotation


# instance fields
.field protected mMessageContent:[B


# direct methods
.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/message/AntMessage;-><init>()V

    iput-object p1, p0, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->mMessageContent:[B

    return-void
.end method

.method protected static createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 3
    .param p0, "messageType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p1, "messageContent"    # [B

    .prologue
    .line 119
    const/4 v0, 0x0

    .line 121
    .local v0, "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    sget-object v1, Lcom/dsi/ant/message/fromant/AntMessageFromAnt$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 157
    :goto_0
    return-object v0

    .line 123
    :pswitch_0
    new-instance v0, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/BroadcastDataMessage;-><init>([B)V

    .line 124
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 126
    :pswitch_1
    new-instance v0, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/AcknowledgedDataMessage;-><init>([B)V

    .line 127
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 129
    :pswitch_2
    new-instance v0, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/BurstTransferDataMessage;-><init>([B)V

    .line 130
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 132
    :pswitch_3
    new-instance v0, Lcom/dsi/ant/message/fromant/AntVersionMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/AntVersionMessage;-><init>([B)V

    .line 133
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 135
    :pswitch_4
    new-instance v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;-><init>([B)V

    .line 136
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 138
    :pswitch_5
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelEventMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelEventMessage;-><init>([B)V

    .line 139
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 141
    :pswitch_6
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelIdMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelIdMessage;-><init>([B)V

    .line 142
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 144
    :pswitch_7
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;-><init>([B)V

    .line 145
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 147
    :pswitch_8
    new-instance v0, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelStatusMessage;-><init>([B)V

    .line 148
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 150
    :pswitch_9
    new-instance v0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;

    .end local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    invoke-direct {v0, p1}, Lcom/dsi/ant/message/fromant/SerialNumberMessage;-><init>([B)V

    .line 151
    .restart local v0    # "antMessage":Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    goto :goto_0

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
    .end packed-switch
.end method

.method public static createAntMessage(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 2
    .param p0, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 111
    if-nez p0, :cond_0

    const/4 v1, 0x0

    .line 115
    :goto_0
    return-object v1

    .line 113
    :cond_0
    invoke-static {p0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    .line 115
    .local v0, "messageType":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v1

    goto :goto_0
.end method

.method public static createAntMessage([B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
    .locals 11
    .param p0, "rawMessage"    # [B

    .prologue
    const/4 v10, 0x2

    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 83
    invoke-static {p0, v8}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v3

    .line 85
    .local v3, "messageLength":I
    array-length v5, p0

    add-int/lit8 v5, v5, -0x2

    if-eq v5, v3, :cond_0

    .line 86
    const-string v5, "Received message of length %d, expected %d. Contents: %s"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/Object;

    array-length v7, p0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v8

    add-int/lit8 v7, v3, 0x2

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v6, v9

    invoke-static {p0}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v10

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "info":Ljava/lang/String;
    new-instance v5, Ljava/lang/IllegalArgumentException;

    invoke-direct {v5, v0}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 92
    .end local v0    # "info":Ljava/lang/String;
    :cond_0
    invoke-static {p0, v9}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v2

    .line 93
    .local v2, "messageId":I
    new-array v1, v3, [B

    .line 94
    .local v1, "messageContent":[B
    invoke-static {p0, v10, v1, v8, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 96
    invoke-static {v2, v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(I[B)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v4

    .line 98
    .local v4, "messageType":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    invoke-static {v4, v1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->createAntMessage(Lcom/dsi/ant/message/fromant/MessageFromAntType;[B)Lcom/dsi/ant/message/fromant/AntMessageFromAnt;

    move-result-object v5

    return-object v5
.end method

.method protected static extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B
    .locals 4
    .param p0, "desiredType"    # Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 48
    invoke-static {p1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    .line 50
    .local v0, "actualType":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    if-eq p0, v0, :cond_0

    .line 51
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Message data is for incorrect type. Desired="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ". Actual="

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 55
    :cond_0
    invoke-virtual {p1}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    return-object v1
.end method


# virtual methods
.method public getMessageContent()[B
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->mMessageContent:[B

    return-object v0
.end method

.method public getMessageId()I
    .locals 1

    .prologue
    .line 27
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->getMessageId()I

    move-result v0

    return v0
.end method

.method public abstract getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 165
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageContentString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected toStringHeader()Ljava/lang/String;
    .locals 2

    .prologue
    .line 160
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;->getMessageIdString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
