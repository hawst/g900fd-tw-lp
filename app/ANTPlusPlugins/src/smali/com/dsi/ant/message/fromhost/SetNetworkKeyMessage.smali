.class public abstract Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "SetNetworkKeyMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage$1;
    }
.end annotation


# static fields
.field public static final OFFSET_NETWORK_KEY:I = 0x1

.field public static final OFFSET_NETWORK_NUMBER:I = 0x0

.field public static final SIZE_NETWORK_NUMBER:I = 0x1


# instance fields
.field private mRawNetworkKey:[B


# direct methods
.method public constructor <init>([B)V
    .locals 2
    .param p1, "networkKey"    # [B

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 54
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->isValidLength([B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 55
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Received network key with invalid length."

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 57
    :cond_0
    invoke-virtual {p1}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    iput-object v0, p0, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->mRawNetworkKey:[B

    .line 58
    return-void
.end method

.method protected static createSetNetworkKeyMessage(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
    .locals 3
    .param p0, "messageType"    # Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .param p1, "messageContent"    # [B

    .prologue
    .line 133
    const/4 v0, 0x0

    .line 135
    .local v0, "networkKeyMessage":Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    sget-object v1, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 147
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Message Type not a network key message"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 137
    :pswitch_0
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;

    .end local v0    # "networkKeyMessage":Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    const/16 v1, 0x8

    invoke-static {p1, v1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKey([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/SetShortNetworkKeyMessage;-><init>([B)V

    .line 150
    .restart local v0    # "networkKeyMessage":Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    :goto_0
    return-object v0

    .line 142
    :pswitch_1
    new-instance v0, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;

    .end local v0    # "networkKeyMessage":Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    const/16 v1, 0x10

    invoke-static {p1, v1}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKey([BI)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/SetLongNetworkKeyMessage;-><init>([B)V

    .line 145
    .restart local v0    # "networkKeyMessage":Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;
    goto :goto_0

    .line 135
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected static getNetworkKey([BI)[B
    .locals 3
    .param p0, "messageContent"    # [B
    .param p1, "networkKeyLength"    # I

    .prologue
    .line 106
    if-eqz p0, :cond_0

    array-length v1, p0

    add-int/lit8 v2, p1, 0x1

    if-eq v1, v2, :cond_1

    .line 108
    :cond_0
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Invalid message content size"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 111
    :cond_1
    new-array v0, p1, [B

    .line 112
    .local v0, "networkKey":[B
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2, p1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 113
    return-object v0
.end method

.method private isValidLength([B)Z
    .locals 3
    .param p1, "networkKey"    # [B

    .prologue
    .line 86
    const/4 v0, 0x0

    .line 88
    .local v0, "isValidLength":Z
    if-nez p1, :cond_1

    .line 89
    const/4 v0, 0x0

    .line 93
    :cond_0
    :goto_0
    return v0

    .line 90
    :cond_1
    array-length v1, p1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKeyLengthBytes()I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 91
    const/4 v0, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x0

    .line 68
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKeyLengthBytes()I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    new-array v0, v1, [B

    .line 70
    .local v0, "messageContent":[B
    invoke-static {p2, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 71
    iget-object v1, p0, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->mRawNetworkKey:[B

    const/4 v2, 0x1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getNetworkKeyLengthBytes()I

    move-result v3

    invoke-static {v1, v4, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 73
    return-object v0
.end method

.method protected abstract getNetworkKeyLengthBytes()I
.end method

.method public getRawNetworkKey()[B
    .locals 1

    .prologue
    .line 64
    iget-object v0, p0, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->mRawNetworkKey:[B

    invoke-virtual {v0}, [B->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 155
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 157
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 158
    const-string v1, "Key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SetNetworkKeyMessage;->getRawNetworkKey()[B

    move-result-object v2

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 160
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
