.class public final enum Lcom/dsi/ant/message/HighPrioritySearchTimeout;
.super Ljava/lang/Enum;
.source "HighPrioritySearchTimeout.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/HighPrioritySearchTimeout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field public static final DEFAULT:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field public static final enum DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field public static final enum FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field public static final MAX:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field public static final enum TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

.field private static final sValues:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 17
    new-instance v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v2, v2}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 18
    new-instance v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    const-string v1, "TWO_AND_A_HALF_SECONDS"

    invoke-direct {v0, v1, v3, v3}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 19
    new-instance v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    const-string v1, "FIVE_SECONDS"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 16
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    sget-object v1, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    aput-object v1, v0, v2

    sget-object v1, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    aput-object v1, v0, v4

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->$VALUES:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 24
    sget-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DEFAULT:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 27
    sget-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->MAX:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 32
    invoke-static {}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->values()[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 34
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/HighPrioritySearchTimeout;
    .locals 3
    .param p0, "milliseconds"    # I

    .prologue
    .line 52
    if-gtz p0, :cond_1

    sget-object v1, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 64
    :cond_0
    :goto_0
    return-object v1

    .line 54
    :cond_1
    sget-object v1, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->MAX:Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    .line 56
    .local v1, "timeout":Lcom/dsi/ant/message/HighPrioritySearchTimeout;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 58
    sget-object v2, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->convertToMilliseconds()I

    move-result v2

    if-gt p0, v2, :cond_2

    .line 59
    sget-object v2, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    aget-object v1, v2, v0

    .line 60
    goto :goto_0

    .line 56
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/HighPrioritySearchTimeout;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 16
    const-class v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/HighPrioritySearchTimeout;
    .locals 1

    .prologue
    .line 16
    sget-object v0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->$VALUES:[Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/HighPrioritySearchTimeout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/HighPrioritySearchTimeout;

    return-object v0
.end method


# virtual methods
.method public convertToMilliseconds()I
    .locals 1

    .prologue
    .line 74
    iget v0, p0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->mRawValue:I

    mul-int/lit16 v0, v0, 0x9c4

    return v0
.end method

.method public getRawValue()I
    .locals 1

    .prologue
    .line 40
    iget v0, p0, Lcom/dsi/ant/message/HighPrioritySearchTimeout;->mRawValue:I

    return v0
.end method
