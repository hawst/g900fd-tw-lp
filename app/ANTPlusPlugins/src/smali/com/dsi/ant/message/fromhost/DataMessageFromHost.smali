.class public abstract Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "DataMessageFromHost.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/DataMessageFromHost$1;
    }
.end annotation


# static fields
.field public static final LENGTH_STANDARD_PAYLOAD:I = 0x8

.field public static final OFFSET_PAYLOAD_START:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 72
    return-void
.end method

.method protected static createDataMessageFromHost(Lcom/dsi/ant/message/fromhost/MessageFromHostType;[B)Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    .locals 3
    .param p0, "messageType"    # Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .param p1, "messageContent"    # [B

    .prologue
    .line 50
    const/4 v0, 0x0

    .line 52
    .local v0, "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    sget-object v1, Lcom/dsi/ant/message/fromhost/DataMessageFromHost$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 75
    :goto_0
    :pswitch_0
    return-object v0

    .line 54
    :pswitch_1
    new-instance v0, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;

    .end local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/AcknowledgedDataMessageFromHost;-><init>([B)V

    .line 56
    .restart local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    goto :goto_0

    .line 58
    :pswitch_2
    new-instance v0, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;

    .end local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/dsi/ant/message/fromhost/BroadcastDataMessageFromHost;-><init>([B)V

    .line 60
    .restart local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    goto :goto_0

    .line 62
    :pswitch_3
    new-instance v0, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;

    .end local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v1

    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getStandardPayload([B)[B

    move-result-object v2

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/BurstTransferDataMessageFromHost;-><init>(I[B)V

    .line 66
    .restart local v0    # "dataMessage":Lcom/dsi/ant/message/fromhost/DataMessageFromHost;
    goto :goto_0

    .line 52
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_0
    .end packed-switch
.end method

.method protected static getStandardPayload([B)[B
    .locals 4
    .param p0, "messageContent"    # [B

    .prologue
    const/16 v3, 0x8

    .line 44
    new-array v0, v3, [B

    .line 45
    .local v0, "payload":[B
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 46
    return-object v0
.end method


# virtual methods
.method public abstract getPayload()[B
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 80
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 82
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 83
    const-string v1, "Payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/DataMessageFromHost;->getPayload()[B

    move-result-object v2

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
