.class public final enum Lcom/dsi/ant/message/fromant/MessageFromAntType;
.super Ljava/lang/Enum;
.source "MessageFromAntType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromant/MessageFromAntType$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromant/MessageFromAntType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final enum SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 26
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "BROADCAST_DATA"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 27
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "ACKNOWLEDGED_DATA"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 28
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "BURST_TRANSFER_DATA"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 29
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "CHANNEL_EVENT"

    invoke-direct {v0, v1, v6}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 30
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "CHANNEL_RESPONSE"

    invoke-direct {v0, v1, v7}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 31
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "CHANNEL_STATUS"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 32
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "CHANNEL_ID"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 33
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "ANT_VERSION"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 34
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "CAPABILITIES"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 35
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "SERIAL_NUMBER"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 36
    new-instance v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    const-string v1, "OTHER"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromant/MessageFromAntType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 24
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->$VALUES:[Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 24
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 149
    return-void
.end method

.method public static create(I[B)Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 3
    .param p0, "messageId"    # I
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v2, 0x1

    .line 64
    sparse-switch p0, :sswitch_data_0

    .line 100
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->OTHER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 104
    .local v1, "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :goto_0
    return-object v1

    .line 67
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_0
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 68
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 70
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_1
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 71
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 73
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_2
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 74
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 76
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_3
    invoke-static {p1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    .line 78
    .local v0, "initiatingMessage":I
    if-ne v2, v0, :cond_0

    .line 79
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_EVENT:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 81
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :cond_0
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 83
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 85
    .end local v0    # "initiatingMessage":I
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_4
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_STATUS:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 86
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 88
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_5
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_ID:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 89
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 91
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_6
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ANT_VERSION:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 92
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 94
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_7
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 95
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 97
    .end local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    :sswitch_8
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    .line 98
    .restart local v1    # "type":Lcom/dsi/ant/message/fromant/MessageFromAntType;
    goto :goto_0

    .line 64
    nop

    :sswitch_data_0
    .sparse-switch
        0x3e -> :sswitch_6
        0x40 -> :sswitch_3
        0x4e -> :sswitch_0
        0x4f -> :sswitch_1
        0x50 -> :sswitch_2
        0x51 -> :sswitch_5
        0x52 -> :sswitch_4
        0x54 -> :sswitch_7
        0x61 -> :sswitch_8
    .end sparse-switch
.end method

.method public static create(Lcom/dsi/ant/message/ipc/AntMessageParcel;)Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 2
    .param p0, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 48
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageId()I

    move-result v0

    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v1

    invoke-static {v0, v1}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->create(I[B)Lcom/dsi/ant/message/fromant/MessageFromAntType;

    move-result-object v0

    return-object v0
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 24
    const-class v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->$VALUES:[Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromant/MessageFromAntType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method


# virtual methods
.method public getMessageId()I
    .locals 3

    .prologue
    .line 115
    const/4 v0, 0x0

    .line 117
    .local v0, "messageId":I
    sget-object v1, Lcom/dsi/ant/message/fromant/MessageFromAntType$1;->$SwitchMap$com$dsi$ant$message$fromant$MessageFromAntType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/MessageFromAntType;->ordinal()I

    move-result v2

    aget v1, v1, v2

    packed-switch v1, :pswitch_data_0

    .line 152
    :goto_0
    return v0

    .line 119
    :pswitch_0
    const/16 v0, 0x4f

    .line 120
    goto :goto_0

    .line 122
    :pswitch_1
    const/16 v0, 0x3e

    .line 123
    goto :goto_0

    .line 125
    :pswitch_2
    const/16 v0, 0x4e

    .line 126
    goto :goto_0

    .line 128
    :pswitch_3
    const/16 v0, 0x50

    .line 129
    goto :goto_0

    .line 131
    :pswitch_4
    const/16 v0, 0x54

    .line 132
    goto :goto_0

    .line 134
    :pswitch_5
    const/16 v0, 0x40

    .line 135
    goto :goto_0

    .line 137
    :pswitch_6
    const/16 v0, 0x51

    .line 138
    goto :goto_0

    .line 140
    :pswitch_7
    const/16 v0, 0x40

    .line 141
    goto :goto_0

    .line 143
    :pswitch_8
    const/16 v0, 0x52

    .line 144
    goto :goto_0

    .line 146
    :pswitch_9
    const/16 v0, 0x61

    .line 147
    goto :goto_0

    .line 149
    :pswitch_a
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "Other message type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 117
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
    .end packed-switch
.end method
