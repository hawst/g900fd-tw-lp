.class public abstract Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "LoadOrStoreEncryptionKeyMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    }
.end annotation


# static fields
.field public static final MAX_KEY_INDEX:I = 0x4

.field public static final OFFSET_NVM_KEY_INDEX:I = 0x1

.field public static final OFFSET_OPERATION:I


# instance fields
.field protected mNvmKeyIndex:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public getNvmKeyIndex()I
    .locals 1

    .prologue
    .line 91
    iget v0, p0, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage;->mNvmKeyIndex:I

    return v0
.end method
