.class public final enum Lcom/dsi/ant/message/LowPrioritySearchTimeout;
.super Ljava/lang/Enum;
.source "LowPrioritySearchTimeout.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/LowPrioritySearchTimeout;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final DEFAULT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum DISABLED:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum FIFTEEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final MAX:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum SEVENTEEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum SEVEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TWELVE_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TWENTY_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TWENTY_TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field public static final enum TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

.field private static final sValues:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;


# instance fields
.field private final mRawValue:I


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 18
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "DISABLED"

    invoke-direct {v0, v1, v4, v4}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 19
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TWO_AND_A_HALF_SECONDS"

    invoke-direct {v0, v1, v5, v5}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 20
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "FIVE_SECONDS"

    invoke-direct {v0, v1, v6, v6}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 21
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "SEVEN_AND_A_HALF_SECONDS"

    invoke-direct {v0, v1, v7, v7}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->SEVEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 22
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TEN_SECONDS"

    invoke-direct {v0, v1, v8, v8}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 23
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TWELVE_AND_A_HALF_SECONDS"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWELVE_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 24
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "FIFTEEN_SECONDS"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIFTEEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 25
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "SEVENTEEN_AND_A_HALF_SECONDS"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->SEVENTEEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 26
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TWENTY_SECONDS"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 27
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TWENTY_TWO_AND_A_HALF_SECONDS"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 28
    new-instance v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    const-string v1, "TWENTY_FIVE_SECONDS"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;-><init>(Ljava/lang/String;II)V

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 17
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->SEVEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v1, v0, v7

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWELVE_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIFTEEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->SEVENTEEN_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_TWO_AND_A_HALF_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->$VALUES:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 34
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->FIFTEEN_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->DEFAULT:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 37
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->TWENTY_FIVE_SECONDS:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->MAX:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 42
    invoke-static {}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->values()[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    move-result-object v0

    sput-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;II)V
    .locals 0
    .param p3, "rawValue"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    iput p3, p0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->mRawValue:I

    return-void
.end method

.method public static create(I)Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .locals 3
    .param p0, "milliseconds"    # I

    .prologue
    .line 64
    if-gtz p0, :cond_1

    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->DISABLED:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 76
    :cond_0
    :goto_0
    return-object v1

    .line 66
    :cond_1
    sget-object v1, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->MAX:Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    .line 68
    .local v1, "timeout":Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    array-length v2, v2

    if-ge v0, v2, :cond_0

    .line 70
    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aget-object v2, v2, v0

    invoke-virtual {v2}, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->convertToMilliseconds()I

    move-result v2

    if-gt p0, v2, :cond_2

    .line 71
    sget-object v2, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->sValues:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    aget-object v1, v2, v0

    .line 72
    goto :goto_0

    .line 68
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 17
    const-class v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/LowPrioritySearchTimeout;
    .locals 1

    .prologue
    .line 17
    sget-object v0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->$VALUES:[Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/LowPrioritySearchTimeout;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/LowPrioritySearchTimeout;

    return-object v0
.end method


# virtual methods
.method public convertToMilliseconds()I
    .locals 1

    .prologue
    .line 86
    iget v0, p0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->mRawValue:I

    mul-int/lit16 v0, v0, 0x9c4

    return v0
.end method

.method public getRawValue()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/dsi/ant/message/LowPrioritySearchTimeout;->mRawValue:I

    return v0
.end method
