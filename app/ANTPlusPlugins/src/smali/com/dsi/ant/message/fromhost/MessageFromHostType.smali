.class public final enum Lcom/dsi/ant/message/fromhost/MessageFromHostType;
.super Ljava/lang/Enum;
.source "MessageFromHostType.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromhost/MessageFromHostType$1;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/dsi/ant/message/fromhost/MessageFromHostType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ADD_CHANNEL_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ADD_ENCRYPTION_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ADVANCED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CHANNEL_SEARCH_PRIORITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CLOSE_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIGURE_ADVANCED_BURST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIGURE_EVENT_FILTER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIGURE_SELECTIVE_DATA_UPDATES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIGURE_USER_NVM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIG_ENCRYPTION_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CRYSTAL_ENABLE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CW_INIT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum CW_TEST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ENABLE_EXTENDED_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ENABLE_LED:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum ENABLE_SINGLE_CHANNEL_ENCRYPTION:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum EXTENDED_ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum EXTENDED_BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum EXTENDED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum FREQUENCY_AGILITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum HIGH_DUTY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum LOAD_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum LOW_PRIORITY_SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum OPEN_RX_SCAN_MODE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum OTHER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum RESET_SYSTEM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SERIAL_NUMBER_SET_CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_128BIT_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_CHANNEL_TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_ENCRYPTION_INFO:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_SELECTIVE_DATA_UPDATE_MASK:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SET_USB_DESCRIPTOR_STRING:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum SLEEP_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum STORE_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final enum UNASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 27
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "UNASSIGN_CHANNEL"

    invoke-direct {v0, v1, v3}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->UNASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 28
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ASSIGN_CHANNEL"

    invoke-direct {v0, v1, v4}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 29
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CHANNEL_ID"

    invoke-direct {v0, v1, v5}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 30
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CHANNEL_PERIOD"

    invoke-direct {v0, v1, v6}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 31
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SEARCH_TIMEOUT"

    invoke-direct {v0, v1, v7}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 32
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CHANNEL_RF_FREQUENCY"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 33
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_NETWORK_KEY"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 34
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "TRANSMIT_POWER"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 35
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ADD_CHANNEL_ID_TO_LIST"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_CHANNEL_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 36
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ADD_ENCRYPTION_ID_TO_LIST"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_ENCRYPTION_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 37
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIG_ID_LIST"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 38
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIG_ENCRYPTION_ID_LIST"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ENCRYPTION_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 39
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_CHANNEL_TRANSMIT_POWER"

    const/16 v2, 0xc

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_CHANNEL_TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 40
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "LOW_PRIORITY_SEARCH_TIMEOUT"

    const/16 v2, 0xd

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOW_PRIORITY_SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 41
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SERIAL_NUMBER_SET_CHANNEL_ID"

    const/16 v2, 0xe

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SERIAL_NUMBER_SET_CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 42
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ENABLE_EXTENDED_RECEIVE_MESSAGES"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_EXTENDED_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 43
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ENABLE_LED"

    const/16 v2, 0x10

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_LED:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 44
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CRYSTAL_ENABLE"

    const/16 v2, 0x11

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CRYSTAL_ENABLE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 45
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "LIB_CONFIG"

    const/16 v2, 0x12

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 46
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "FREQUENCY_AGILITY"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->FREQUENCY_AGILITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 47
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "PROXIMITY_SEARCH"

    const/16 v2, 0x14

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 48
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIGURE_EVENT_BUFFER"

    const/16 v2, 0x15

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 49
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CHANNEL_SEARCH_PRIORITY"

    const/16 v2, 0x16

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_SEARCH_PRIORITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 50
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_128BIT_NETWORK_KEY"

    const/16 v2, 0x17

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_128BIT_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 51
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "HIGH_DUTY_SEARCH"

    const/16 v2, 0x18

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->HIGH_DUTY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 52
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIGURE_ADVANCED_BURST"

    const/16 v2, 0x19

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_ADVANCED_BURST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 53
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIGURE_EVENT_FILTER"

    const/16 v2, 0x1a

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_FILTER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 54
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIGURE_SELECTIVE_DATA_UPDATES"

    const/16 v2, 0x1b

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_SELECTIVE_DATA_UPDATES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 55
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_SELECTIVE_DATA_UPDATE_MASK"

    const/16 v2, 0x1c

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_SELECTIVE_DATA_UPDATE_MASK:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 56
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CONFIGURE_USER_NVM"

    const/16 v2, 0x1d

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_USER_NVM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 57
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ENABLE_SINGLE_CHANNEL_ENCRYPTION"

    const/16 v2, 0x1e

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_SINGLE_CHANNEL_ENCRYPTION:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 58
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_ENCRYPTION_KEY"

    const/16 v2, 0x1f

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 59
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_ENCRYPTION_INFO"

    const/16 v2, 0x20

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_INFO:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 60
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "LOAD_ENCRYPTION_KEY"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOAD_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 61
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "STORE_ENCRYPTION_KEY"

    const/16 v2, 0x22

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->STORE_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 62
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SET_USB_DESCRIPTOR_STRING"

    const/16 v2, 0x23

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_USB_DESCRIPTOR_STRING:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 63
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "RESET_SYSTEM"

    const/16 v2, 0x24

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->RESET_SYSTEM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 64
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "OPEN_CHANNEL"

    const/16 v2, 0x25

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 65
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CLOSE_CHANNEL"

    const/16 v2, 0x26

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CLOSE_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 66
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "OPEN_RX_SCAN_MODE"

    const/16 v2, 0x27

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_RX_SCAN_MODE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 67
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "REQUEST_MESSAGE"

    const/16 v2, 0x28

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 68
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "SLEEP_MESSAGE"

    const/16 v2, 0x29

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SLEEP_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 69
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "BROADCAST_DATA"

    const/16 v2, 0x2a

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 70
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ACKNOWLEDGED_DATA"

    const/16 v2, 0x2b

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 71
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "BURST_TRANSFER_DATA"

    const/16 v2, 0x2c

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 72
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "ADVANCED_BURST_DATA"

    const/16 v2, 0x2d

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADVANCED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 73
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CW_INIT"

    const/16 v2, 0x2e

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_INIT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 74
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "CW_TEST"

    const/16 v2, 0x2f

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_TEST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 75
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "EXTENDED_BROADCAST_DATA"

    const/16 v2, 0x30

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 76
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "EXTENDED_ACKNOWLEDGED_DATA"

    const/16 v2, 0x31

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 77
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "EXTENDED_BURST_DATA"

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 78
    new-instance v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    const-string v1, "OTHER"

    const/16 v2, 0x33

    invoke-direct {v0, v1, v2}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OTHER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 25
    const/16 v0, 0x34

    new-array v0, v0, [Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->UNASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_CHANNEL_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_ENCRYPTION_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ENCRYPTION_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_CHANNEL_TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOW_PRIORITY_SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SERIAL_NUMBER_SET_CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_EXTENDED_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_LED:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CRYSTAL_ENABLE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->FREQUENCY_AGILITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_SEARCH_PRIORITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_128BIT_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->HIGH_DUTY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_ADVANCED_BURST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_FILTER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_SELECTIVE_DATA_UPDATES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_SELECTIVE_DATA_UPDATE_MASK:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_USER_NVM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_SINGLE_CHANNEL_ENCRYPTION:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_INFO:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOAD_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->STORE_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_USB_DESCRIPTOR_STRING:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->RESET_SYSTEM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CLOSE_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_RX_SCAN_MODE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SLEEP_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2b

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2c

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2d

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADVANCED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2e

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_INIT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x2f

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_TEST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x30

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x31

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x32

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    const/16 v1, 0x33

    sget-object v2, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OTHER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->$VALUES:[Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 25
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 215
    return-void
.end method

.method public static create(Lcom/dsi/ant/message/ipc/AntMessageParcel;Lcom/dsi/ant/message/ChannelType;)Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 7
    .param p0, "antParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;
    .param p1, "validForChannelType"    # Lcom/dsi/ant/message/ChannelType;

    .prologue
    .line 90
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageId()I

    move-result v1

    .line 91
    .local v1, "messageId":I
    invoke-virtual {p0}, Lcom/dsi/ant/message/ipc/AntMessageParcel;->getMessageContent()[B

    move-result-object v0

    .line 93
    .local v0, "messageContent":[B
    sparse-switch v1, :sswitch_data_0

    .line 268
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OTHER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 272
    .local v4, "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :goto_0
    return-object v4

    .line 96
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_0
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->UNASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 97
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 99
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ASSIGN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 100
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 102
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 103
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 105
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_3
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 106
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 108
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_4
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 109
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 111
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_5
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_RF_FREQUENCY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 112
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 114
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_6
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 115
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 117
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_7
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 118
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 121
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_8
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADD_CHANNEL_ID_TO_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 122
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 124
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_9
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIG_ID_LIST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 135
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 137
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_a
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_CHANNEL_TRANSMIT_POWER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 138
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 140
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_b
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOW_PRIORITY_SEARCH_TIMEOUT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 141
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 143
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_c
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SERIAL_NUMBER_SET_CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 144
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 146
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_d
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_EXTENDED_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 147
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 149
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_e
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_LED:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 150
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 152
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_f
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CRYSTAL_ENABLE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 153
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 155
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_10
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LIB_CONFIG:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 156
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 158
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_11
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->FREQUENCY_AGILITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 159
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 161
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_12
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->PROXIMITY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 162
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 164
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_13
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_BUFFER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 165
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 167
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_14
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_SEARCH_PRIORITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 168
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 170
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_15
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_128BIT_NETWORK_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 171
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 173
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_16
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->HIGH_DUTY_SEARCH:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 174
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 176
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_17
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_ADVANCED_BURST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 177
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 179
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_18
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_EVENT_FILTER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 180
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 182
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_19
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_SELECTIVE_DATA_UPDATES:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 183
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 185
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1a
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_SELECTIVE_DATA_UPDATE_MASK:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 186
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 188
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1b
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CONFIGURE_USER_NVM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 189
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 191
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1c
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ENABLE_SINGLE_CHANNEL_ENCRYPTION:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 192
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 194
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1d
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 195
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 197
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1e
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_ENCRYPTION_INFO:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 198
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 200
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_1f
    const/4 v5, 0x0

    invoke-static {v0, v5}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v3

    .line 202
    .local v3, "operationValue":I
    invoke-static {v3}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->create(I)Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;

    move-result-object v2

    .line 205
    .local v2, "operation":Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    sget-object v5, Lcom/dsi/ant/message/fromhost/MessageFromHostType$1;->$SwitchMap$com$dsi$ant$message$fromhost$LoadOrStoreEncryptionKeyMessage$Operation:[I

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;->ordinal()I

    move-result v6

    aget v5, v5, v6

    packed-switch v5, :pswitch_data_0

    .line 214
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OTHER:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 215
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 207
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :pswitch_0
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->LOAD_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 208
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 210
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :pswitch_1
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->STORE_ENCRYPTION_KEY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 211
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 220
    .end local v2    # "operation":Lcom/dsi/ant/message/fromhost/LoadOrStoreEncryptionKeyMessage$Operation;
    .end local v3    # "operationValue":I
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_20
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SET_USB_DESCRIPTOR_STRING:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 221
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 223
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_21
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->RESET_SYSTEM:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 224
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto :goto_0

    .line 226
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_22
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 227
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 229
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_23
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CLOSE_CHANNEL:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 230
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 232
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_24
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->OPEN_RX_SCAN_MODE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 233
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 235
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_25
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->REQUEST_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 236
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 238
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_26
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SLEEP_MESSAGE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 239
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 241
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_27
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 242
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 244
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_28
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 245
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 247
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_29
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->BURST_TRANSFER_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 248
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 250
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2a
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ADVANCED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 251
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 253
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2b
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_INIT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 254
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 256
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2c
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_TEST:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 257
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 259
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2d
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BROADCAST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 260
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 262
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2e
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_ACKNOWLEDGED_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 263
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 265
    .end local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    :sswitch_2f
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->EXTENDED_BURST_DATA:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    .line 266
    .restart local v4    # "type":Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    goto/16 :goto_0

    .line 93
    :sswitch_data_0
    .sparse-switch
        0x41 -> :sswitch_0
        0x42 -> :sswitch_1
        0x43 -> :sswitch_3
        0x44 -> :sswitch_4
        0x45 -> :sswitch_5
        0x46 -> :sswitch_6
        0x47 -> :sswitch_7
        0x48 -> :sswitch_2c
        0x4a -> :sswitch_21
        0x4b -> :sswitch_22
        0x4c -> :sswitch_23
        0x4d -> :sswitch_25
        0x4e -> :sswitch_27
        0x4f -> :sswitch_28
        0x50 -> :sswitch_29
        0x51 -> :sswitch_2
        0x53 -> :sswitch_2b
        0x59 -> :sswitch_8
        0x5a -> :sswitch_9
        0x5b -> :sswitch_24
        0x5d -> :sswitch_2d
        0x5e -> :sswitch_2e
        0x5f -> :sswitch_2f
        0x60 -> :sswitch_a
        0x63 -> :sswitch_b
        0x65 -> :sswitch_c
        0x66 -> :sswitch_d
        0x68 -> :sswitch_e
        0x6d -> :sswitch_f
        0x6e -> :sswitch_10
        0x70 -> :sswitch_11
        0x71 -> :sswitch_12
        0x72 -> :sswitch_2a
        0x74 -> :sswitch_13
        0x75 -> :sswitch_14
        0x76 -> :sswitch_15
        0x77 -> :sswitch_16
        0x78 -> :sswitch_17
        0x79 -> :sswitch_18
        0x7a -> :sswitch_19
        0x7b -> :sswitch_1a
        0x7c -> :sswitch_1b
        0x7d -> :sswitch_1c
        0x7e -> :sswitch_1d
        0x7f -> :sswitch_1e
        0x83 -> :sswitch_1f
        0xc5 -> :sswitch_26
        0xc7 -> :sswitch_20
    .end sparse-switch

    .line 205
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 25
    const-class v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public static values()[Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 25
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->$VALUES:[Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    invoke-virtual {v0}, [Lcom/dsi/ant/message/fromhost/MessageFromHostType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method


# virtual methods
.method public getMessageId()I
    .locals 6

    .prologue
    const/16 v3, 0x83

    const/16 v2, 0x5a

    const/16 v1, 0x59

    .line 282
    const/4 v0, 0x0

    .line 284
    .local v0, "messageId":I
    sget-object v4, Lcom/dsi/ant/message/fromhost/MessageFromHostType$1;->$SwitchMap$com$dsi$ant$message$fromhost$MessageFromHostType:[I

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->ordinal()I

    move-result v5

    aget v4, v4, v5

    packed-switch v4, :pswitch_data_0

    .line 391
    .end local v0    # "messageId":I
    :goto_0
    return v0

    .line 286
    .restart local v0    # "messageId":I
    :pswitch_0
    const/16 v0, 0x41

    goto :goto_0

    .line 288
    :pswitch_1
    const/16 v0, 0x42

    goto :goto_0

    .line 290
    :pswitch_2
    const/16 v0, 0x51

    goto :goto_0

    .line 292
    :pswitch_3
    const/16 v0, 0x43

    goto :goto_0

    .line 294
    :pswitch_4
    const/16 v0, 0x44

    goto :goto_0

    .line 296
    :pswitch_5
    const/16 v0, 0x45

    goto :goto_0

    .line 298
    :pswitch_6
    const/16 v0, 0x46

    goto :goto_0

    .line 300
    :pswitch_7
    const/16 v0, 0x47

    goto :goto_0

    :pswitch_8
    move v0, v1

    .line 302
    goto :goto_0

    :pswitch_9
    move v0, v1

    .line 304
    goto :goto_0

    :pswitch_a
    move v0, v2

    .line 306
    goto :goto_0

    :pswitch_b
    move v0, v2

    .line 308
    goto :goto_0

    .line 310
    :pswitch_c
    const/16 v0, 0x60

    goto :goto_0

    .line 312
    :pswitch_d
    const/16 v0, 0x63

    goto :goto_0

    .line 314
    :pswitch_e
    const/16 v0, 0x65

    goto :goto_0

    .line 316
    :pswitch_f
    const/16 v0, 0x66

    goto :goto_0

    .line 318
    :pswitch_10
    const/16 v0, 0x68

    goto :goto_0

    .line 320
    :pswitch_11
    const/16 v0, 0x6d

    goto :goto_0

    .line 322
    :pswitch_12
    const/16 v0, 0x6e

    goto :goto_0

    .line 324
    :pswitch_13
    const/16 v0, 0x70

    goto :goto_0

    .line 326
    :pswitch_14
    const/16 v0, 0x71

    goto :goto_0

    .line 328
    :pswitch_15
    const/16 v0, 0x74

    goto :goto_0

    .line 330
    :pswitch_16
    const/16 v0, 0x75

    goto :goto_0

    .line 332
    :pswitch_17
    const/16 v0, 0x76

    goto :goto_0

    .line 334
    :pswitch_18
    const/16 v0, 0x77

    goto :goto_0

    .line 336
    :pswitch_19
    const/16 v0, 0x78

    goto :goto_0

    .line 338
    :pswitch_1a
    const/16 v0, 0x79

    goto :goto_0

    .line 340
    :pswitch_1b
    const/16 v0, 0x7a

    goto :goto_0

    .line 342
    :pswitch_1c
    const/16 v0, 0x7b

    goto :goto_0

    .line 344
    :pswitch_1d
    const/16 v0, 0x7c

    goto :goto_0

    .line 346
    :pswitch_1e
    const/16 v0, 0x7d

    goto :goto_0

    .line 348
    :pswitch_1f
    const/16 v0, 0x7e

    goto :goto_0

    .line 350
    :pswitch_20
    const/16 v0, 0x7f

    goto :goto_0

    :pswitch_21
    move v0, v3

    .line 352
    goto :goto_0

    :pswitch_22
    move v0, v3

    .line 354
    goto :goto_0

    .line 356
    :pswitch_23
    const/16 v0, 0xc7

    goto :goto_0

    .line 358
    :pswitch_24
    const/16 v0, 0x4a

    goto :goto_0

    .line 360
    :pswitch_25
    const/16 v0, 0x4b

    goto :goto_0

    .line 362
    :pswitch_26
    const/16 v0, 0x4c

    goto :goto_0

    .line 364
    :pswitch_27
    const/16 v0, 0x5b

    goto :goto_0

    .line 366
    :pswitch_28
    const/16 v0, 0x4d

    goto :goto_0

    .line 368
    :pswitch_29
    const/16 v0, 0xc5

    goto :goto_0

    .line 370
    :pswitch_2a
    const/16 v0, 0x4e

    goto :goto_0

    .line 372
    :pswitch_2b
    const/16 v0, 0x4f

    goto :goto_0

    .line 374
    :pswitch_2c
    const/16 v0, 0x50

    goto/16 :goto_0

    .line 376
    :pswitch_2d
    const/16 v0, 0x72

    goto/16 :goto_0

    .line 378
    :pswitch_2e
    const/16 v0, 0x53

    goto/16 :goto_0

    .line 380
    :pswitch_2f
    const/16 v0, 0x48

    goto/16 :goto_0

    .line 382
    :pswitch_30
    const/16 v0, 0x5d

    goto/16 :goto_0

    .line 384
    :pswitch_31
    const/16 v0, 0x5e

    goto/16 :goto_0

    .line 386
    :pswitch_32
    const/16 v0, 0x5f

    goto/16 :goto_0

    .line 388
    :pswitch_33
    new-instance v1, Ljava/lang/IllegalArgumentException;

    const-string v2, "No message ID for OTHER message type"

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 284
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_10
        :pswitch_11
        :pswitch_12
        :pswitch_13
        :pswitch_14
        :pswitch_15
        :pswitch_16
        :pswitch_17
        :pswitch_18
        :pswitch_19
        :pswitch_1a
        :pswitch_1b
        :pswitch_1c
        :pswitch_1d
        :pswitch_1e
        :pswitch_1f
        :pswitch_20
        :pswitch_21
        :pswitch_22
        :pswitch_23
        :pswitch_24
        :pswitch_25
        :pswitch_26
        :pswitch_27
        :pswitch_28
        :pswitch_29
        :pswitch_2a
        :pswitch_2b
        :pswitch_2c
        :pswitch_2d
        :pswitch_2e
        :pswitch_2f
        :pswitch_30
        :pswitch_31
        :pswitch_32
        :pswitch_33
    .end packed-switch
.end method
