.class public final Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "CapabilitiesMessage.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$AdvancedOptions3;,
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$AdvancedOptions2;,
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$AdvancedOptions;,
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$StandardOptions;,
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$MessageOffsets;,
        Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    }
.end annotation


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;


# instance fields
.field private mCapabilitiesArray:[Z

.field private mMaxDataChannels:I

.field private final mNumChannels:I

.field private final mNumNetworks:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CAPABILITIES:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 157
    sget-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;-><init>([B)V

    .line 158
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v1, 0x0

    .line 165
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 140
    iput v1, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMaxDataChannels:I

    .line 146
    invoke-static {}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapabilityArraySize()I

    move-result v0

    new-array v0, v0, [Z

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    .line 168
    invoke-static {p1, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumChannels:I

    .line 169
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumNetworks:I

    .line 172
    invoke-direct {p0}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->parseCapabilities()V

    .line 173
    return-void
.end method

.method private static getCapabilityArraySize()I
    .locals 1

    .prologue
    .line 377
    sget-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->NUMBER_OF_CAPABILITIES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    invoke-virtual {v0}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->ordinal()I

    move-result v0

    return v0
.end method

.method private parseCapabilities()V
    .locals 9

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x6

    const/4 v6, 0x3

    const/4 v5, 0x4

    const/4 v4, 0x2

    .line 261
    iget-object v1, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    array-length v0, v1

    .line 264
    .local v0, "messageLength":I
    if-lt v4, v0, :cond_1

    .line 375
    :cond_0
    :goto_0
    return-void

    .line 267
    :cond_1
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v4, v2, v8}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 271
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_CHANNELS:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v4, v2, v4}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 275
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_RECEIVE_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v4, v2, v5}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 279
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_TRANSMIT_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x8

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 283
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_ACKD_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x10

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 287
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NO_BURST_MESSAGES:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x20

    invoke-direct {p0, v1, v4, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 292
    if-ge v6, v0, :cond_0

    .line 295
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_NETWORK_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v6, v2, v4}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 299
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SERIAL_NUMBER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x8

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 303
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PER_CHANNEL_TX_POWER_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x10

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 307
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LOW_PRIORITY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x20

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 311
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCRIPT_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x40

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 315
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SEARCH_LIST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x80

    invoke-direct {p0, v1, v6, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 320
    if-ge v5, v0, :cond_0

    .line 323
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_LED_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v5, v2, v8}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 327
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_MESSAGE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v5, v2, v4}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 331
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SCAN_MODE_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v5, v2, v5}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 335
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_PROX_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x10

    invoke-direct {p0, v1, v5, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 339
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EXT_ASSIGN_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x20

    invoke-direct {p0, v1, v5, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 343
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_FS_ANTFS_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x40

    invoke-direct {p0, v1, v5, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 348
    const/4 v1, 0x5

    if-ge v1, v0, :cond_0

    .line 350
    iget-object v1, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/4 v2, 0x5

    invoke-static {v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v1

    iput v1, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMaxDataChannels:I

    .line 353
    if-ge v7, v0, :cond_0

    .line 356
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_ADVANCED_BURST_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v7, v2, v8}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 360
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_BUFFERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v7, v2, v4}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 364
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_EVENT_FILTERING_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    invoke-direct {p0, v1, v7, v2, v5}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 368
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_HIGH_DUTY_SEARCH_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x8

    invoke-direct {p0, v1, v7, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    .line 372
    sget-object v1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->CAPABILITIES_SELECTIVE_DATA_UPDATES_ENABLED:Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mMessageContent:[B

    const/16 v3, 0x40

    invoke-direct {p0, v1, v7, v2, v3}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V

    goto/16 :goto_0
.end method

.method private setCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;I[BI)V
    .locals 3
    .param p1, "option"    # Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    .param p2, "offset"    # I
    .param p3, "data"    # [B
    .param p4, "bitMask"    # I

    .prologue
    .line 257
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->ordinal()I

    move-result v1

    invoke-static {p4, p3, p2}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v2

    aput-boolean v2, v0, v1

    .line 258
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 7
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 201
    if-ne p0, p1, :cond_1

    .line 227
    :cond_0
    :goto_0
    return v3

    .line 204
    :cond_1
    if-nez p1, :cond_2

    move v3, v4

    .line 205
    goto :goto_0

    .line 207
    :cond_2
    instance-of v5, p1, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    if-nez v5, :cond_3

    move v3, v4

    .line 208
    goto :goto_0

    :cond_3
    move-object v2, p1

    .line 211
    check-cast v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;

    .line 213
    .local v2, "other":Lcom/dsi/ant/message/fromant/CapabilitiesMessage;
    iget v5, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumChannels:I

    iget v6, v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumChannels:I

    if-ne v5, v6, :cond_4

    iget v5, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumNetworks:I

    iget v6, v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumNetworks:I

    if-eq v5, v6, :cond_5

    :cond_4
    move v3, v4

    .line 215
    goto :goto_0

    .line 218
    :cond_5
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->hashCode()I

    move-result v5

    invoke-virtual {v2}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->hashCode()I

    move-result v6

    if-eq v5, v6, :cond_6

    move v3, v4

    .line 219
    goto :goto_0

    .line 221
    :cond_6
    invoke-static {}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapabilityArraySize()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 222
    .local v0, "arraySize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 223
    iget-object v5, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    aget-boolean v5, v5, v1

    iget-object v6, v2, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    aget-boolean v6, v6, v1

    if-eq v5, v6, :cond_7

    move v3, v4

    .line 224
    goto :goto_0

    .line 222
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method public getCapability(Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;)Z
    .locals 2
    .param p1, "option"    # Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    .prologue
    .line 182
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    invoke-virtual {p1}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->ordinal()I

    move-result v1

    aget-boolean v0, v0, v1

    return v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 395
    sget-object v0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public getNumberOfChannels()I
    .locals 1

    .prologue
    .line 384
    iget v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumChannels:I

    return v0
.end method

.method public getNumberOfNetworks()I
    .locals 1

    .prologue
    .line 391
    iget v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mNumNetworks:I

    return v0
.end method

.method public hashCode()I
    .locals 8

    .prologue
    .line 188
    const/16 v4, 0x1f

    .line 189
    .local v4, "prime":I
    const/4 v5, 0x7

    .line 191
    .local v5, "result":I
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    .local v0, "arr$":[Z
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-boolean v1, v0, v2

    .line 192
    .local v1, "capability":Z
    mul-int/lit8 v7, v5, 0x1f

    if-eqz v1, :cond_0

    const/4 v6, 0x1

    :goto_1
    add-int v5, v7, v6

    .line 191
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 192
    :cond_0
    const/4 v6, 0x0

    goto :goto_1

    .line 195
    .end local v1    # "capability":Z
    :cond_1
    return v5
.end method

.method public toString()Ljava/lang/String;
    .locals 9

    .prologue
    .line 233
    const/4 v2, 0x0

    .line 234
    .local v2, "counter":I
    invoke-static {}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->getCapabilityArraySize()I

    move-result v1

    .line 236
    .local v1, "arraySize":I
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 238
    .local v6, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v7, "\n  "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 239
    const-string v7, "Capabilities="

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 241
    invoke-static {}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->values()[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;

    move-result-object v0

    .local v0, "arr$":[Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_1

    aget-object v5, v0, v3

    .line 244
    .local v5, "option":Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    iget-object v7, p0, Lcom/dsi/ant/message/fromant/CapabilitiesMessage;->mCapabilitiesArray:[Z

    invoke-virtual {v5}, Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;->ordinal()I

    move-result v8

    aget-boolean v7, v7, v8

    if-eqz v7, :cond_0

    .line 245
    const-string v7, " -"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 247
    :cond_0
    add-int/lit8 v2, v2, 0x1

    .line 250
    if-ne v2, v1, :cond_2

    .line 253
    .end local v5    # "option":Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    :cond_1
    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    return-object v7

    .line 241
    .restart local v5    # "option":Lcom/dsi/ant/message/fromant/CapabilitiesMessage$Option;
    :cond_2
    add-int/lit8 v3, v3, 0x1

    goto :goto_0
.end method
