.class public Lcom/dsi/ant/message/Timestamp;
.super Ljava/lang/Object;
.source "Timestamp.java"


# static fields
.field public static final OFFSET_RX_TIMESTAMP:I = 0x0

.field public static final SIZE_RX_TIMESTAMP:I = 0x2

.field public static final SIZE_TIMESTAMP:B = 0x2t


# instance fields
.field private mRxTimestamp:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "rxTimestamp"    # I

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    return-void
.end method

.method public constructor <init>([BI)V
    .locals 2
    .param p1, "messageContent"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    add-int/lit8 v0, p2, 0x0

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 70
    if-ne p0, p1, :cond_1

    .line 88
    :cond_0
    :goto_0
    return v1

    .line 74
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    .line 75
    goto :goto_0

    .line 78
    :cond_2
    instance-of v3, p1, Lcom/dsi/ant/message/Timestamp;

    if-nez v3, :cond_3

    move v1, v2

    .line 79
    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 82
    check-cast v0, Lcom/dsi/ant/message/Timestamp;

    .line 84
    .local v0, "other":Lcom/dsi/ant/message/Timestamp;
    iget v3, v0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    iget v4, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    if-eq v3, v4, :cond_0

    move v1, v2

    .line 85
    goto :goto_0
.end method

.method public getRxTimestamp()I
    .locals 1

    .prologue
    .line 54
    iget v0, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 59
    const/16 v0, 0x1f

    .line 60
    .local v0, "prime":I
    const/4 v1, 0x7

    .line 62
    .local v1, "result":I
    iget v2, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    add-int/lit16 v1, v2, 0xd9

    .line 64
    return v1
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 94
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Timestamp:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 96
    .local v0, "infoStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, " Rx="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 97
    iget v1, p0, Lcom/dsi/ant/message/Timestamp;->mRxTimestamp:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
