.class public final Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "SerialNumberSetChannelIdMessage.java"


# static fields
.field public static final BITMASK_DEVICE_TYPE:I = 0x7f

.field public static final BITMASK_PAIR:I = 0x80

.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_DEVICE_TYPE:I = 0x1

.field public static final OFFSET_PAIRING_BIT:I = 0x1

.field public static final OFFSET_TRANSMISSION_TYPE:I = 0x2

.field public static final SIZE_DEVICE_TYPE:I = 0x1

.field public static final SIZE_TRANSMISSION_TYPE:I = 0x1


# instance fields
.field private final mDeviceType:I

.field private final mPair:Z

.field private final mTransmissionType:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->SERIAL_NUMBER_SET_CHANNEL_ID:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(ZII)V
    .locals 0
    .param p1, "pair"    # Z
    .param p2, "deviceType"    # I
    .param p3, "transmissionType"    # I

    .prologue
    .line 60
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 61
    iput-boolean p1, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mPair:Z

    .line 62
    iput p2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mDeviceType:I

    .line 63
    iput p3, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mTransmissionType:I

    .line 64
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 4
    .param p1, "messageContent"    # [B

    .prologue
    const/4 v3, 0x1

    .line 67
    const/16 v0, 0x80

    invoke-static {v0, p1, v3}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v0

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-static {p1, v3, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits([BIII)I

    move-result v1

    const/4 v2, 0x2

    invoke-static {p1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;-><init>(ZII)V

    .line 71
    return-void
.end method


# virtual methods
.method public getDeviceType()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mDeviceType:I

    return v0
.end method

.method public getMessageContent(II)[B
    .locals 7
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x1

    .line 100
    const/4 v2, 0x5

    new-array v0, v2, [B

    .line 102
    .local v0, "content":[B
    iget v4, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mDeviceType:I

    iget-boolean v2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mPair:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x80

    :goto_0
    add-int v1, v4, v2

    .line 104
    .local v1, "deviceTypeWithPiaringBit":I
    int-to-long v4, p1

    invoke-static {v4, v5, v0, v6, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 106
    int-to-long v2, v1

    invoke-static {v2, v3, v0, v6, v6}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 108
    iget v2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mTransmissionType:I

    int-to-long v2, v2

    const/4 v4, 0x2

    invoke-static {v2, v3, v0, v6, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 111
    return-object v0

    .end local v1    # "deviceTypeWithPiaringBit":I
    :cond_0
    move v2, v3

    .line 102
    goto :goto_0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 117
    sget-object v0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public getPair()Z
    .locals 1

    .prologue
    .line 80
    iget-boolean v0, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mPair:Z

    return v0
.end method

.method public getTransmissionType()I
    .locals 1

    .prologue
    .line 95
    iget v0, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mTransmissionType:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 122
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 124
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 125
    const-string v1, "Pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mPair:Z

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 126
    const-string v1, ", Device Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mDeviceType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 127
    const-string v1, ", Transmission Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/SerialNumberSetChannelIdMessage;->mTransmissionType:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 129
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
