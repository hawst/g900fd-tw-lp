.class public Lcom/dsi/ant/message/fromant/ChannelResponseMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "ChannelResponseMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final OFFSET_INITIATING_MESSAGE_ID:I = 0x1

.field public static final OFFSET_RESPONSE_CODE:I = 0x2

.field public static final SIZE_INITIATING_MESSAGE_ID:I = 0x1

.field public static final SIZE_RESPONSE_CODE:I = 0x1


# instance fields
.field private mInitiatingMessageId:I

.field private mRawResponseCode:I

.field private mResponseCode:Lcom/dsi/ant/message/ResponseCode;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->CHANNEL_RESPONSE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 57
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;-><init>([B)V

    .line 58
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 61
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 63
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->readFromMessageContent([B)V

    .line 64
    return-void
.end method

.method private readFromMessageContent([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 67
    iput-object p1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mMessageContent:[B

    .line 69
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    .line 71
    const/4 v0, 0x2

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    .line 72
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    invoke-static {v0}, Lcom/dsi/ant/message/ResponseCode;->create(I)Lcom/dsi/ant/message/ResponseCode;

    move-result-object v0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    .line 73
    return-void
.end method


# virtual methods
.method public getInitiatingMessageId()I
    .locals 1

    .prologue
    .line 81
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    return v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public getRawResponseCode()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    return v0
.end method

.method public getResponseCode()Lcom/dsi/ant/message/ResponseCode;
    .locals 1

    .prologue
    .line 96
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 105
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 107
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 108
    const-string v1, "Response to="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 109
    iget v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mInitiatingMessageId:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "\n  "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 110
    const-string v1, "code="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 111
    iget-object v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mResponseCode:Lcom/dsi/ant/message/ResponseCode;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 112
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 113
    iget v1, p0, Lcom/dsi/ant/message/fromant/ChannelResponseMessage;->mRawResponseCode:I

    invoke-static {v1}, Lcom/dsi/ant/message/MessageUtils;->getHexString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 114
    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 116
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
