.class public final Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "FrequencyAgilityMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_FREQUENCY_ONE:I = 0x1

.field public static final OFFSET_FREQUENCY_THREE:I = 0x3

.field public static final OFFSET_FREQUENCY_TWO:I = 0x2


# instance fields
.field private final mFrequencyOne:I

.field private final mFrequencyThree:I

.field private final mFrequencyTwo:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->FREQUENCY_AGILITY:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(III)V
    .locals 2
    .param p1, "frequencyOne"    # I
    .param p2, "frequencyTwo"    # I
    .param p3, "frequencyThree"    # I

    .prologue
    .line 46
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 47
    invoke-static {p1}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->inRange(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Frequency 1 out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 51
    :cond_0
    invoke-static {p2}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->inRange(I)Z

    move-result v0

    if-nez v0, :cond_1

    .line 52
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Frequency 2 out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 55
    :cond_1
    invoke-static {p3}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->inRange(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 56
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Frequency 3 out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 59
    :cond_2
    iput p1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyOne:I

    .line 60
    iput p2, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyTwo:I

    .line 61
    iput p3, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyThree:I

    .line 62
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 3
    .param p1, "messageContent"    # [B

    .prologue
    .line 65
    const/4 v0, 0x1

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    const/4 v1, 0x2

    invoke-static {p1, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v1

    const/4 v2, 0x3

    invoke-static {p1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v2

    invoke-direct {p0, v0, v1, v2}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;-><init>(III)V

    .line 68
    return-void
.end method

.method public static getRealRfFrequency(I)I
    .locals 1
    .param p0, "rfFrequencyOffset"    # I

    .prologue
    .line 80
    invoke-static {p0}, Lcom/dsi/ant/message/fromhost/ChannelRfFrequencyMessage;->getRealRfFrequency(I)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getFrequencyOne()I
    .locals 1

    .prologue
    .line 89
    iget v0, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyOne:I

    return v0
.end method

.method public getFrequencyThree()I
    .locals 1

    .prologue
    .line 105
    iget v0, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyThree:I

    return v0
.end method

.method public getFrequencyTwo()I
    .locals 1

    .prologue
    .line 97
    iget v0, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyTwo:I

    return v0
.end method

.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v4, 0x1

    .line 110
    const/4 v1, 0x4

    new-array v0, v1, [B

    .line 113
    .local v0, "content":[B
    int-to-long v1, p1

    const/4 v3, 0x0

    invoke-static {v1, v2, v0, v4, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 115
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyOne:I

    invoke-static {v1, v0, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 116
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyTwo:I

    const/4 v2, 0x2

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 117
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyThree:I

    const/4 v2, 0x3

    invoke-static {v1, v0, v2}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 119
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 125
    sget-object v0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 130
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 132
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 133
    const-string v1, "Frequency 1="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 134
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyOne:I

    invoke-static {v1}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getRealRfFrequency(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 135
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyOne:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 137
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 138
    const-string v1, "Frequency 2="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 139
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyTwo:I

    invoke-static {v1}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getRealRfFrequency(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyTwo:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 142
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    const-string v1, "Frequency 3="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 144
    iget v1, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyThree:I

    invoke-static {v1}, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->getRealRfFrequency(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "MHz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 145
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/FrequencyAgilityMessage;->mFrequencyThree:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 147
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
