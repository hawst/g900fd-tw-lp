.class public abstract Lcom/dsi/ant/message/fromant/DataMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "DataMessage.java"


# static fields
.field public static final LENGTH_STANDARD_PAYLOAD:I = 0x8

.field public static final OFFSET_PAYLOAD_START:I = 0x1


# instance fields
.field private mPayload:[B


# direct methods
.method protected constructor <init>([B)V
    .locals 1
    .param p1, "messageContent"    # [B

    .prologue
    .line 46
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    .line 48
    return-void
.end method

.method protected static getStandardPayload([B)[B
    .locals 4
    .param p0, "messageContent"    # [B

    .prologue
    const/16 v3, 0x8

    .line 38
    new-array v0, v3, [B

    .line 39
    .local v0, "payload":[B
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-static {p0, v1, v0, v2, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 40
    return-object v0
.end method


# virtual methods
.method public getExtendedData()Lcom/dsi/ant/message/ExtendedData;
    .locals 1

    .prologue
    .line 81
    new-instance v0, Lcom/dsi/ant/message/ExtendedData;

    invoke-direct {v0, p0}, Lcom/dsi/ant/message/ExtendedData;-><init>(Lcom/dsi/ant/message/fromant/DataMessage;)V

    return-object v0
.end method

.method public getPayload()[B
    .locals 5

    .prologue
    const/16 v4, 0x8

    .line 58
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    if-nez v0, :cond_0

    .line 59
    new-array v0, v4, [B

    iput-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    .line 60
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mMessageContent:[B

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    const/4 v3, 0x0

    invoke-static {v0, v1, v2, v3, v4}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_0
    iget-object v0, p0, Lcom/dsi/ant/message/fromant/DataMessage;->mPayload:[B

    return-object v0
.end method

.method public hasExtendedData()Z
    .locals 1

    .prologue
    .line 72
    invoke-static {p0}, Lcom/dsi/ant/message/ExtendedData;->hasExtendedData(Lcom/dsi/ant/message/fromant/AntMessageFromAnt;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    .line 86
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 88
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 89
    const-string v1, "Payload="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->getPayload()[B

    move-result-object v2

    invoke-static {v2}, Lcom/dsi/ant/message/MessageUtils;->getHexString([B)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 91
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->hasExtendedData()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 92
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 93
    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/DataMessage;->getExtendedData()Lcom/dsi/ant/message/ExtendedData;

    move-result-object v1

    invoke-virtual {v1}, Lcom/dsi/ant/message/ExtendedData;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 96
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
