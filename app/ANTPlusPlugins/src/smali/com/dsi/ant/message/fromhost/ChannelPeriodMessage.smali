.class public final Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "ChannelPeriodMessage.java"


# static fields
.field public static final CHANNEL_PERIOD_UNITS_FRACTION_OF_A_SECOND:D = 32768.0

.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

.field public static final OFFSET_CHANNEL_PERIOD:I = 0x1

.field public static final SIZE_CHANNEL_PERIOD:I = 0x2


# instance fields
.field private final mChannelPeriod:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CHANNEL_PERIOD:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "channelPeriod"    # I

    .prologue
    .line 53
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 54
    iput p1, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    .line 55
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    .line 57
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 58
    const/4 v0, 0x1

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    .line 60
    return-void
.end method

.method public static getChannelPeriod(D)I
    .locals 7
    .param p0, "messagesPerSecond"    # D

    .prologue
    const-wide/high16 v5, 0x40e0000000000000L    # 32768.0

    .line 73
    const-wide/16 v1, 0x0

    rem-double v3, v5, p0

    cmpl-double v1, v1, v3

    if-eqz v1, :cond_0

    .line 74
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Invalid message rate: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p0, p1}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 77
    :cond_0
    div-double v1, v5, p0

    double-to-int v0, v1

    .line 79
    .local v0, "channelPeriod":I
    return v0
.end method

.method public static getMessagesPerSecond(I)D
    .locals 6
    .param p0, "channelPeriod"    # I

    .prologue
    .line 101
    const-wide/high16 v2, 0x40e0000000000000L    # 32768.0

    int-to-double v4, p0

    div-double v0, v2, v4

    .line 103
    .local v0, "messagesPerSecond":D
    return-wide v0
.end method


# virtual methods
.method public getChannelPeriod()I
    .locals 1

    .prologue
    .line 88
    iget v0, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    return v0
.end method

.method public getMessageContent(II)[B
    .locals 5
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    .line 120
    const/4 v1, 0x3

    new-array v0, v1, [B

    .line 122
    .local v0, "content":[B
    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(I[BI)V

    .line 123
    iget v1, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    int-to-long v1, v1

    const/4 v3, 0x2

    const/4 v4, 0x1

    invoke-static {v1, v2, v0, v3, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 126
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 132
    sget-object v0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public getMessagesPerSecond()D
    .locals 2

    .prologue
    .line 114
    iget v0, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    invoke-static {v0}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->getMessagesPerSecond(I)D

    move-result-wide v0

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 137
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 139
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 140
    const-string v1, "Channel period="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->getMessagesPerSecond()D

    move-result-wide v2

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(D)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "Hz"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 141
    const-string v1, " ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/dsi/ant/message/fromhost/ChannelPeriodMessage;->mChannelPeriod:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 143
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
