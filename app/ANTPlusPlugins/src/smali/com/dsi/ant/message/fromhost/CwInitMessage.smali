.class public final Lcom/dsi/ant/message/fromhost/CwInitMessage;
.super Lcom/dsi/ant/message/fromhost/AntMessageFromHost;
.source "CwInitMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/dsi/ant/message/fromhost/MessageFromHostType;->CW_INIT:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    sput-object v0, Lcom/dsi/ant/message/fromhost/CwInitMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 28
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 0
    .param p1, "messageContent"    # [B

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/dsi/ant/message/fromhost/AntMessageFromHost;-><init>()V

    .line 37
    return-void
.end method


# virtual methods
.method public getMessageContent(II)[B
    .locals 3
    .param p1, "channelNumber"    # I
    .param p2, "networkNumber"    # I

    .prologue
    const/4 v2, 0x0

    .line 42
    const/4 v1, 0x1

    new-array v0, v1, [B

    aput-byte v2, v0, v2

    .line 43
    .local v0, "messageContent":[B
    return-object v0
.end method

.method public getMessageType()Lcom/dsi/ant/message/fromhost/MessageFromHostType;
    .locals 1

    .prologue
    .line 49
    sget-object v0, Lcom/dsi/ant/message/fromhost/CwInitMessage;->MY_TYPE:Lcom/dsi/ant/message/fromhost/MessageFromHostType;

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 54
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromhost/CwInitMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 56
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
