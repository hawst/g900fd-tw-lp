.class public final Lcom/dsi/ant/message/MessageId;
.super Ljava/lang/Object;
.source "MessageId.java"


# static fields
.field public static final ACKNOWLEDGED_DATA:I = 0x4f

.field public static final ACTIVE_SEARCH_SHARING:I = 0x81

.field public static final ADV_BURST_DATA:I = 0x72

.field public static final ALARM_VARIABLE_MODIFY_TEST:I = 0x99

.field public static final ANT_VERSION:I = 0x3e

.field public static final ASSIGN_CHANNEL:I = 0x42

.field public static final ATOD_EXTERNAL_ENABLE:I = 0x96

.field public static final ATOD_PIN_SETUP:I = 0x97

.field public static final ATOD_SETTINGS:I = 0x94

.field public static final AUTO_FREQ_CONFIG:I = 0x70

.field public static final BIST:I = 0xaa

.field public static final BROADCAST_DATA:I = 0x4e

.field public static final BURST_DATA:I = 0x50

.field public static final CAPABILITIES:I = 0x54

.field public static final CHANNEL_EVENT_INITIATING_ID:I = 0x1

.field public static final CHANNEL_EVENT_OR_RESPONSE:I = 0x40

.field public static final CHANNEL_ID:I = 0x51

.field public static final CHANNEL_MESG_PERIOD:I = 0x43

.field public static final CHANNEL_RADIO_FREQ:I = 0x45

.field public static final CHANNEL_RADIO_TX_POWER:I = 0x60

.field public static final CHANNEL_SEARCH_PRIORITY:I = 0x75

.field public static final CHANNEL_SEARCH_TIMEOUT:I = 0x44

.field public static final CHANNEL_STATUS:I = 0x52

.field public static final CLOSE_CHANNEL:I = 0x4c

.field public static final COEX_ADV_PRIORITY_CONFIG:I = 0x82

.field public static final COEX_PRIORITY_CONFIG:I = 0x73

.field public static final CONFIG_ADV_BURST:I = 0x78

.field public static final CUBE_CMD:I = 0x80

.field public static final ENABLE_LED_FLASH:I = 0x68

.field public static final ENCRYPT_ENABLE:I = 0x7d

.field public static final EVENT_BUFFERING_CONFIG:I = 0x74

.field public static final EVENT_FILTER_CONFIG:I = 0x79

.field public static final EXT_ACKNOWLEDGED_DATA:I = 0x5e

.field public static final EXT_BROADCAST_DATA:I = 0x5d

.field public static final EXT_BURST_DATA:I = 0x5f

.field public static final FIT1_SET_AGC:I = 0x8f

.field public static final FIT1_SET_EQUIP_STATE:I = 0x91

.field public static final GET_GRMN_ESN:I = 0xc6

.field public static final GET_PIN_DIODE_CONTROL:I = 0x8d

.field public static final GET_SERIAL_NUM:I = 0x61

.field public static final GET_TEMP_CAL:I = 0x62

.field public static final HCI_COMMAND_COMPLETE:I = 0xc8

.field public static final HIGH_DUTY_SEARCH_MODE:I = 0x77

.field public static final ID_LIST_ADD:I = 0x59

.field public static final ID_LIST_CONFIG:I = 0x5a

.field public static final INVALID:I = 0x0

.field public static final LIB_CONFIG:I = 0x6e

.field public static final NETWORK_128_KEY:I = 0x76

.field public static final NETWORK_KEY:I = 0x46

.field public static final NVM_ENCRYPT_KEY_OPS:I = 0x83

.field public static final OPEN_CHANNEL:I = 0x4b

.field public static final OPEN_RX_SCAN:I = 0x5b

.field public static final OVERWRITE_TEMP_CAL:I = 0x9b

.field public static final PARTIAL_RESET:I = 0x9a

.field public static final PIN_DIODE_CONTROL:I = 0x8e

.field public static final PORT_GET_IO_STATE:I = 0xb4

.field public static final PORT_SET_IO_STATE:I = 0xb5

.field public static final PROX_SEARCH_CONFIG:I = 0x71

.field public static final RADIO_CONFIG_ALWAYS:I = 0x67

.field public static final RADIO_CW_INIT:I = 0x53

.field public static final RADIO_CW_MODE:I = 0x48

.field public static final RADIO_TX_POWER:I = 0x47

.field public static final READ_PINS_FOR_SECT:I = 0x92

.field public static final REQUEST:I = 0x4d

.field public static final RFACTIVE_NOTIFICATION:I = 0x84

.field public static final RX_EXT_MESGS_ENABLE:I = 0x66

.field public static final SCRIPT_CMD:I = 0x57

.field public static final SCRIPT_DATA:I = 0x56

.field public static final SDU_CONFIG:I = 0x7a

.field public static final SDU_SET_MASK:I = 0x7b

.field public static final SERIAL_ERROR:I = 0xae

.field public static final SERIAL_NUM_SET_CHANNEL_ID:I = 0x65

.field public static final SERIAL_PASSTHRU_SETTINGS:I = 0x9c

.field public static final SETUP_ALARM:I = 0x98

.field public static final SET_CHANNEL_DATA_TYPE:I = 0x91

.field public static final SET_CHANNEL_INPUT_MASK:I = 0x90

.field public static final SET_ENCRYPT_INFO:I = 0x7f

.field public static final SET_ENCRYPT_KEY:I = 0x7e

.field public static final SET_LP_SEARCH_TIMEOUT:I = 0x63

.field public static final SET_SHARED_ADDRESS:I = 0x95

.field public static final SET_STRING:I = 0xaf

.field public static final SET_TX_SEARCH_ON_NEXT:I = 0x64

.field public static final SET_USB_INFO:I = 0xc7

.field public static final SLEEP:I = 0xc5

.field public static final STACKLIMIT:I = 0x55

.field public static final STARTUP_MESG:I = 0x6f

.field public static final SYSTEM_RESET:I = 0x4a

.field public static final TIMER_SELECT:I = 0x93

.field public static final UNASSIGN_CHANNEL:I = 0x41

.field public static final UNLOCK_INTERFACE:I = 0xad

.field public static final USER_CONFIG_PAGE:I = 0x7c

.field public static final XTAL_ENABLE:I = 0x6d


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
