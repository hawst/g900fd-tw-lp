.class public Lcom/dsi/ant/message/fromant/SerialNumberMessage;
.super Lcom/dsi/ant/message/fromant/AntMessageFromAnt;
.source "SerialNumberMessage.java"


# static fields
.field private static final MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

.field public static final OFFSET_SERIAL_NUMBER:I = 0x0

.field public static final SIZE_SERIAL_NUMBER:I = 0x4


# instance fields
.field private final mSerialNumber:J


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/dsi/ant/message/fromant/MessageFromAntType;->SERIAL_NUMBER:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    sput-object v0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-void
.end method

.method public constructor <init>(Lcom/dsi/ant/message/ipc/AntMessageParcel;)V
    .locals 1
    .param p1, "messageParcel"    # Lcom/dsi/ant/message/ipc/AntMessageParcel;

    .prologue
    .line 40
    sget-object v0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    invoke-static {v0, p1}, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->extractCorrectMessageContent(Lcom/dsi/ant/message/fromant/MessageFromAntType;Lcom/dsi/ant/message/ipc/AntMessageParcel;)[B

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/dsi/ant/message/fromant/SerialNumberMessage;-><init>([B)V

    .line 41
    return-void
.end method

.method protected constructor <init>([B)V
    .locals 2
    .param p1, "messageContent"    # [B

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/dsi/ant/message/fromant/AntMessageFromAnt;-><init>([B)V

    .line 46
    const/4 v0, 0x0

    const/4 v1, 0x4

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->mSerialNumber:J

    .line 47
    return-void
.end method


# virtual methods
.method public getMessageType()Lcom/dsi/ant/message/fromant/MessageFromAntType;
    .locals 1

    .prologue
    .line 58
    sget-object v0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->MY_TYPE:Lcom/dsi/ant/message/fromant/MessageFromAntType;

    return-object v0
.end method

.method public getSerialNumber()J
    .locals 2

    .prologue
    .line 54
    iget-wide v0, p0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->mSerialNumber:J

    return-wide v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 63
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-virtual {p0}, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->toStringHeader()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 65
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, "\n  "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 66
    const-string v1, "Serial number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-wide v2, p0, Lcom/dsi/ant/message/fromant/SerialNumberMessage;->mSerialNumber:J

    invoke-virtual {v1, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    .line 68
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
