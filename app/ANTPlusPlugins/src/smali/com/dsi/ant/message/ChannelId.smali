.class public Lcom/dsi/ant/message/ChannelId;
.super Ljava/lang/Object;
.source "ChannelId.java"


# static fields
.field public static final ANY_DEVICE_NUMBER:I = 0x0

.field public static final ANY_DEVICE_TYPE:I = 0x0

.field public static final ANY_TRANSMISSION_TYPE:I = 0x0

.field public static final BITMASK_DEVICE_TYPE:I = 0x7f

.field public static final BITMASK_PAIR:I = 0x80

.field public static final BITMASK_SHARED_ADDRESS_TYPE:I = 0x3

.field public static final MAX_DEVICE_NUMBER:I = 0xffff

.field public static final MAX_DEVICE_TYPE:I = 0x7f

.field public static final MAX_TRANSMISSION_TYPE:I = 0xff

.field public static final MIN_DEVICE_NUMBER:I = 0x0

.field public static final MIN_DEVICE_TYPE:I = 0x0

.field public static final MIN_TRANSMISSION_TYPE:I = 0x0

.field public static final OFFSET_DEVICE_NUMBER:I = 0x0

.field public static final OFFSET_DEVICE_TYPE:I = 0x2

.field public static final OFFSET_PAIRING_BIT:I = 0x2

.field public static final OFFSET_TRANSMISSION_TYPE:I = 0x3

.field private static final SHARED_ADDRESS_ONE_BYTE:I = 0x2

.field private static final SHARED_ADDRESS_TWO_BYTE:I = 0x3

.field public static final SIZE_CHANNEL_ID:I = 0x4

.field public static final SIZE_DEVICE_NUMBER:I = 0x2

.field public static final SIZE_DEVICE_TYPE:I = 0x1

.field public static final SIZE_TRANSMISSION_TYPE:I = 0x1


# instance fields
.field private final mDeviceNumber:I

.field private final mDeviceType:I

.field private final mPair:Z

.field private final mTransmissionType:I


# direct methods
.method public constructor <init>(III)V
    .locals 1
    .param p1, "deviceNumber"    # I
    .param p2, "deviceType"    # I
    .param p3, "transmissionType"    # I

    .prologue
    .line 124
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/dsi/ant/message/ChannelId;-><init>(IIIZ)V

    .line 125
    return-void
.end method

.method public constructor <init>(IIIZ)V
    .locals 2
    .param p1, "deviceNumber"    # I
    .param p2, "deviceType"    # I
    .param p3, "transmissionType"    # I
    .param p4, "pair"    # Z

    .prologue
    const/4 v1, 0x0

    .line 139
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 140
    const v0, 0xffff

    invoke-static {p1, v1, v0}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_0

    .line 141
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Device Number out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 144
    :cond_0
    const/16 v0, 0x7f

    invoke-static {p2, v1, v0}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_1

    .line 145
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Device Type out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 148
    :cond_1
    const/16 v0, 0xff

    invoke-static {p3, v1, v0}, Lcom/dsi/ant/message/MessageUtils;->inRange(III)Z

    move-result v0

    if-nez v0, :cond_2

    .line 149
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Transmission type out of range"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 152
    :cond_2
    iput p1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    .line 153
    iput p2, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    .line 154
    iput-boolean p4, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    .line 155
    iput p3, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    .line 156
    return-void
.end method

.method public constructor <init>([BI)V
    .locals 3
    .param p1, "messageContent"    # [B
    .param p2, "startOffset"    # I

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    add-int/lit8 v0, p2, 0x0

    const/4 v1, 0x2

    invoke-static {p1, v0, v1}, Lcom/dsi/ant/message/MessageUtils;->numberFromBytes([BII)J

    move-result-wide v0

    long-to-int v0, v0

    iput v0, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    .line 106
    add-int/lit8 v0, p2, 0x2

    const/16 v1, 0x7f

    const/4 v2, 0x0

    invoke-static {p1, v0, v1, v2}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits([BIII)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    .line 109
    const/16 v0, 0x80

    add-int/lit8 v1, p2, 0x2

    invoke-static {v0, p1, v1}, Lcom/dsi/ant/message/MessageUtils;->isFlagSet(I[BI)Z

    move-result v0

    iput-boolean v0, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    .line 112
    add-int/lit8 v0, p2, 0x3

    invoke-static {p1, v0}, Lcom/dsi/ant/message/MessageUtils;->numberFromByte([BI)I

    move-result v0

    iput v0, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    .line 114
    return-void
.end method


# virtual methods
.method public getDeviceNumber()I
    .locals 1

    .prologue
    .line 163
    iget v0, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    return v0
.end method

.method public getDeviceType()I
    .locals 1

    .prologue
    .line 180
    iget v0, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    return v0
.end method

.method public getMessageContent()[B
    .locals 8

    .prologue
    const/4 v7, 0x2

    const/4 v6, 0x1

    const/4 v3, 0x0

    .line 226
    const/4 v2, 0x4

    new-array v0, v2, [B

    .line 228
    .local v0, "content":[B
    iget v4, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    iget-boolean v2, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    if-eqz v2, :cond_0

    const/16 v2, 0x80

    :goto_0
    add-int v1, v4, v2

    .line 230
    .local v1, "deviceTypeWithPiaringBit":I
    iget v2, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    int-to-long v4, v2

    invoke-static {v4, v5, v0, v7, v3}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 232
    int-to-long v2, v1

    invoke-static {v2, v3, v0, v6, v7}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 234
    iget v2, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    int-to-long v2, v2

    const/4 v4, 0x3

    invoke-static {v2, v3, v0, v6, v4}, Lcom/dsi/ant/message/MessageUtils;->placeInArray(J[BII)V

    .line 237
    return-object v0

    .end local v1    # "deviceTypeWithPiaringBit":I
    :cond_0
    move v2, v3

    .line 228
    goto :goto_0
.end method

.method public getNumberofBytesForSharedAddress()I
    .locals 5

    .prologue
    .line 201
    iget v2, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    const/4 v3, 0x3

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Lcom/dsi/ant/message/MessageUtils;->numberFromBits(III)I

    move-result v0

    .line 204
    .local v0, "numberFromSharedAddressBits":I
    const/4 v1, 0x0

    .line 205
    .local v1, "sizeOfSharedAddress":I
    packed-switch v0, :pswitch_data_0

    .line 217
    :goto_0
    return v1

    .line 207
    :pswitch_0
    const/4 v1, 0x1

    .line 208
    goto :goto_0

    .line 210
    :pswitch_1
    const/4 v1, 0x2

    .line 211
    goto :goto_0

    .line 205
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public getPair()Z
    .locals 1

    .prologue
    .line 172
    iget-boolean v0, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    return v0
.end method

.method public getTransmissionType()I
    .locals 1

    .prologue
    .line 188
    iget v0, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 243
    new-instance v0, Ljava/lang/StringBuilder;

    const-string v1, "Channel ID:"

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 245
    .local v0, "toStringBuilder":Ljava/lang/StringBuilder;
    const-string v1, " Device number="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 246
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceNumber:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 247
    const-string v1, ", Pair="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 248
    iget-boolean v1, p0, Lcom/dsi/ant/message/ChannelId;->mPair:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    .line 249
    const-string v1, ", Device Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 250
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mDeviceType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 251
    const-string v1, ", Transmission Type="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 252
    iget v1, p0, Lcom/dsi/ant/message/ChannelId;->mTransmissionType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 254
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
