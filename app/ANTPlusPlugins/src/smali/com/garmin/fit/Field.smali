.class public Lcom/garmin/fit/Field;
.super Ljava/lang/Object;
.source "Field.java"


# instance fields
.field protected components:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/FieldComponent;",
            ">;"
        }
    .end annotation
.end field

.field protected isAccumulated:Z

.field protected name:Ljava/lang/String;

.field protected num:I

.field protected offset:D

.field protected scale:D

.field protected subFields:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/SubField;",
            ">;"
        }
    .end annotation
.end field

.field protected type:I

.field protected units:Ljava/lang/String;

.field protected values:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/garmin/fit/Field;)V
    .locals 6

    .prologue
    const/4 v2, 0x0

    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    if-nez p1, :cond_0

    .line 43
    const-string v0, "unknown"

    iput-object v0, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    .line 44
    const/16 v0, 0xff

    iput v0, p0, Lcom/garmin/fit/Field;->num:I

    .line 45
    iput v2, p0, Lcom/garmin/fit/Field;->type:I

    .line 46
    const-wide/high16 v0, 0x3ff0000000000000L    # 1.0

    iput-wide v0, p0, Lcom/garmin/fit/Field;->scale:D

    .line 47
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/garmin/fit/Field;->offset:D

    .line 48
    const-string v0, ""

    iput-object v0, p0, Lcom/garmin/fit/Field;->units:Ljava/lang/String;

    .line 49
    iput-boolean v2, p0, Lcom/garmin/fit/Field;->isAccumulated:Z

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    .line 51
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    .line 52
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    .line 83
    :goto_0
    return-void

    .line 56
    :cond_0
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    .line 57
    iget v0, p1, Lcom/garmin/fit/Field;->num:I

    iput v0, p0, Lcom/garmin/fit/Field;->num:I

    .line 58
    iget v0, p1, Lcom/garmin/fit/Field;->type:I

    iput v0, p0, Lcom/garmin/fit/Field;->type:I

    .line 59
    iget-wide v0, p1, Lcom/garmin/fit/Field;->scale:D

    iput-wide v0, p0, Lcom/garmin/fit/Field;->scale:D

    .line 60
    iget-wide v0, p1, Lcom/garmin/fit/Field;->offset:D

    iput-wide v0, p0, Lcom/garmin/fit/Field;->offset:D

    .line 61
    new-instance v0, Ljava/lang/String;

    iget-object v1, p1, Lcom/garmin/fit/Field;->units:Ljava/lang/String;

    invoke-direct {v0, v1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/garmin/fit/Field;->units:Ljava/lang/String;

    .line 62
    iget-boolean v0, p1, Lcom/garmin/fit/Field;->isAccumulated:Z

    iput-boolean v0, p0, Lcom/garmin/fit/Field;->isAccumulated:Z

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    .line 64
    iget-object v0, p1, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_1
    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_8

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    .line 65
    instance-of v2, v0, Ljava/lang/Byte;

    if-eqz v2, :cond_2

    .line 66
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Byte;

    check-cast v0, Ljava/lang/Byte;

    invoke-virtual {v0}, Ljava/lang/Byte;->byteValue()B

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/Byte;-><init>(B)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 67
    :cond_2
    instance-of v2, v0, Ljava/lang/Short;

    if-eqz v2, :cond_3

    .line 68
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Short;

    check-cast v0, Ljava/lang/Short;

    invoke-virtual {v0}, Ljava/lang/Short;->shortValue()S

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/Short;-><init>(S)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 69
    :cond_3
    instance-of v2, v0, Ljava/lang/Integer;

    if-eqz v2, :cond_4

    .line 70
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Integer;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 71
    :cond_4
    instance-of v2, v0, Ljava/lang/Long;

    if-eqz v2, :cond_5

    .line 72
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Long;

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Long;-><init>(J)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 73
    :cond_5
    instance-of v2, v0, Ljava/lang/Float;

    if-eqz v2, :cond_6

    .line 74
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Float;

    check-cast v0, Ljava/lang/Float;

    invoke-virtual {v0}, Ljava/lang/Float;->floatValue()F

    move-result v0

    invoke-direct {v3, v0}, Ljava/lang/Float;-><init>(F)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 75
    :cond_6
    instance-of v2, v0, Ljava/lang/Double;

    if-eqz v2, :cond_7

    .line 76
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/Double;

    check-cast v0, Ljava/lang/Double;

    invoke-virtual {v0}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v4

    invoke-direct {v3, v4, v5}, Ljava/lang/Double;-><init>(D)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 77
    :cond_7
    instance-of v2, v0, Ljava/lang/String;

    if-eqz v2, :cond_1

    .line 78
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/String;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v3, v0}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_1

    .line 81
    :cond_8
    iget-object v0, p1, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    .line 82
    iget-object v0, p1, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    iput-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    goto/16 :goto_0
.end method

.method protected constructor <init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V
    .locals 1

    .prologue
    .line 85
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 86
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p1}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    .line 87
    iput p2, p0, Lcom/garmin/fit/Field;->num:I

    .line 88
    iput p3, p0, Lcom/garmin/fit/Field;->type:I

    .line 89
    iput-wide p4, p0, Lcom/garmin/fit/Field;->scale:D

    .line 90
    iput-wide p6, p0, Lcom/garmin/fit/Field;->offset:D

    .line 91
    new-instance v0, Ljava/lang/String;

    invoke-direct {v0, p8}, Ljava/lang/String;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/garmin/fit/Field;->units:Ljava/lang/String;

    .line 92
    iput-boolean p9, p0, Lcom/garmin/fit/Field;->isAccumulated:Z

    .line 93
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    .line 94
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->components:Ljava/util/ArrayList;

    .line 95
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    .line 96
    return-void
.end method

.method private getByteValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 585
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 587
    if-nez v0, :cond_0

    .line 588
    const/4 v0, 0x0

    .line 590
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->byteValue()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    goto :goto_0
.end method

.method private getDoubleValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Double;
    .locals 4

    .prologue
    .line 710
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 712
    if-nez v0, :cond_0

    .line 713
    const/4 v0, 0x0

    .line 715
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/Double;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Double;-><init>(D)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getFloatValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Float;
    .locals 4

    .prologue
    .line 685
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 687
    if-nez v0, :cond_0

    .line 688
    const/4 v0, 0x0

    .line 690
    :goto_0
    return-object v0

    :cond_0
    new-instance v1, Ljava/lang/Float;

    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    invoke-direct {v1, v2, v3}, Ljava/lang/Float;-><init>(D)V

    move-object v0, v1

    goto :goto_0
.end method

.method private getIntegerValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 635
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 637
    if-nez v0, :cond_0

    .line 638
    const/4 v0, 0x0

    .line 640
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_0
.end method

.method private getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;
    .locals 2

    .prologue
    .line 660
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 662
    if-nez v0, :cond_0

    .line 663
    const/4 v0, 0x0

    .line 665
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_0
.end method

.method private getNameInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 111
    if-nez p1, :cond_0

    .line 112
    iget-object v0, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    .line 114
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/garmin/fit/SubField;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method private getRawValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 280
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 281
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    .line 283
    :cond_0
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method private getShortValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Short;
    .locals 1

    .prologue
    .line 610
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    .line 612
    if-nez v0, :cond_0

    .line 613
    const/4 v0, 0x0

    .line 615
    :goto_0
    return-object v0

    :cond_0
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    goto :goto_0
.end method

.method private getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;
    .locals 3

    .prologue
    const/4 v0, 0x0

    .line 735
    invoke-direct {p0, p1, p2}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v1

    .line 737
    if-nez v1, :cond_0

    .line 798
    :goto_0
    return-object v0

    .line 740
    :cond_0
    iget v2, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v2, :sswitch_data_0

    .line 798
    :cond_1
    invoke-virtual {v1}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 743
    :sswitch_0
    sget-object v2, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 747
    :sswitch_1
    sget-object v2, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 751
    :sswitch_2
    sget-object v2, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 755
    :sswitch_3
    sget-object v2, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 759
    :sswitch_4
    sget-object v2, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 763
    :sswitch_5
    sget-object v2, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 767
    :sswitch_6
    sget-object v2, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 771
    :sswitch_7
    sget-object v2, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 775
    :sswitch_8
    sget-object v2, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 779
    :sswitch_9
    sget-object v2, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 783
    :sswitch_a
    sget-object v2, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 787
    :sswitch_b
    sget-object v2, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 791
    :sswitch_c
    sget-object v2, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    invoke-virtual {v1, v2}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    goto :goto_0

    .line 740
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0xa -> :sswitch_3
        0xd -> :sswitch_c
        0x83 -> :sswitch_4
        0x84 -> :sswitch_5
        0x85 -> :sswitch_7
        0x86 -> :sswitch_8
        0x88 -> :sswitch_a
        0x89 -> :sswitch_b
        0x8b -> :sswitch_6
        0x8c -> :sswitch_9
    .end sparse-switch
.end method

.method private getTypeInternal(Lcom/garmin/fit/SubField;)I
    .locals 1

    .prologue
    .line 134
    if-nez p1, :cond_0

    .line 135
    iget v0, p0, Lcom/garmin/fit/Field;->type:I

    .line 137
    :goto_0
    return v0

    :cond_0
    iget v0, p1, Lcom/garmin/fit/SubField;->type:I

    goto :goto_0
.end method

.method private getUnitsInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 153
    if-nez p1, :cond_0

    .line 154
    iget-object v0, p0, Lcom/garmin/fit/Field;->units:Ljava/lang/String;

    .line 156
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/garmin/fit/SubField;->units:Ljava/lang/String;

    goto :goto_0
.end method

.method private getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;
    .locals 8

    .prologue
    .line 308
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 309
    const/4 v1, 0x0

    .line 385
    :cond_0
    :goto_0
    return-object v1

    .line 311
    :cond_1
    if-nez p2, :cond_4

    .line 312
    iget-wide v2, p0, Lcom/garmin/fit/Field;->scale:D

    .line 313
    iget-wide v0, p0, Lcom/garmin/fit/Field;->offset:D

    move-wide v4, v2

    move-wide v2, v0

    .line 320
    :goto_1
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    .line 322
    instance-of v0, v1, Ljava/lang/Number;

    if-eqz v0, :cond_0

    .line 323
    iget v0, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v0, :sswitch_data_0

    .line 380
    :cond_2
    const-wide/high16 v6, 0x3ff0000000000000L    # 1.0

    cmpl-double v0, v4, v6

    if-nez v0, :cond_3

    const-wide/16 v6, 0x0

    cmpl-double v0, v2, v6

    if-eqz v0, :cond_0

    .line 381
    :cond_3
    new-instance v0, Ljava/lang/Double;

    check-cast v1, Ljava/lang/Number;

    invoke-virtual {v1}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    div-double v4, v6, v4

    sub-double v1, v4, v2

    invoke-direct {v0, v1, v2}, Ljava/lang/Double;-><init>(D)V

    move-object v1, v0

    goto :goto_0

    .line 316
    :cond_4
    iget-wide v2, p2, Lcom/garmin/fit/SubField;->scale:D

    .line 317
    iget-wide v0, p2, Lcom/garmin/fit/SubField;->offset:D

    move-wide v4, v2

    move-wide v2, v0

    goto :goto_1

    :sswitch_0
    move-object v0, v1

    .line 325
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, v6}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 326
    sget-object v1, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    goto :goto_0

    :sswitch_1
    move-object v0, v1

    .line 329
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->byteValue()B

    move-result v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    invoke-virtual {v0, v6}, Ljava/lang/Byte;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 330
    sget-object v1, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    goto :goto_0

    :sswitch_2
    move-object v0, v1

    .line 333
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, v6}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 334
    sget-object v1, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    goto :goto_0

    :sswitch_3
    move-object v0, v1

    .line 337
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, v6}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 338
    sget-object v1, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    goto/16 :goto_0

    :sswitch_4
    move-object v0, v1

    .line 341
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, v6}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 342
    sget-object v1, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    goto/16 :goto_0

    :sswitch_5
    move-object v0, v1

    .line 345
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 346
    sget-object v1, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_6
    move-object v0, v1

    .line 349
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 350
    sget-object v1, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_7
    move-object v0, v1

    .line 353
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, v6}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 354
    sget-object v1, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    goto/16 :goto_0

    :sswitch_8
    move-object v0, v1

    .line 357
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    invoke-virtual {v0, v6}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 358
    sget-object v1, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_9
    move-object v0, v1

    .line 361
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    invoke-virtual {v0, v6}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 362
    sget-object v1, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    goto/16 :goto_0

    :sswitch_a
    move-object v0, v1

    .line 365
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    invoke-virtual {v0, v6}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 366
    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    goto/16 :goto_0

    :sswitch_b
    move-object v0, v1

    .line 369
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v6

    invoke-static {v6, v7}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    invoke-virtual {v0, v6}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 370
    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    goto/16 :goto_0

    :sswitch_c
    move-object v0, v1

    .line 373
    check-cast v0, Ljava/lang/Number;

    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    sget-object v6, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, v6}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 374
    sget-object v1, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    goto/16 :goto_0

    .line 323
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_2
        0xa -> :sswitch_3
        0xd -> :sswitch_c
        0x83 -> :sswitch_4
        0x84 -> :sswitch_5
        0x85 -> :sswitch_7
        0x86 -> :sswitch_8
        0x88 -> :sswitch_a
        0x89 -> :sswitch_b
        0x8b -> :sswitch_6
        0x8c -> :sswitch_9
    .end sparse-switch
.end method

.method private isSignedIntegerInternal(Lcom/garmin/fit/SubField;)Z
    .locals 1

    .prologue
    .line 228
    invoke-direct {p0, p1}, Lcom/garmin/fit/Field;->getTypeInternal(Lcom/garmin/fit/SubField;)I

    move-result v0

    sparse-switch v0, :sswitch_data_0

    .line 238
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 232
    :sswitch_0
    const/4 v0, 0x1

    goto :goto_0

    .line 228
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x83 -> :sswitch_0
        0x85 -> :sswitch_0
    .end sparse-switch
.end method

.method private setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V
    .locals 6

    .prologue
    .line 422
    :goto_0
    invoke-virtual {p0}, Lcom/garmin/fit/Field;->getNumValues()I

    move-result v0

    if-lt p1, v0, :cond_0

    .line 423
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    invoke-virtual {p0, v0}, Lcom/garmin/fit/Field;->addValue(Ljava/lang/Object;)V

    goto :goto_0

    .line 428
    :cond_0
    if-nez p3, :cond_1

    .line 429
    iget-wide v2, p0, Lcom/garmin/fit/Field;->scale:D

    .line 430
    iget-wide v0, p0, Lcom/garmin/fit/Field;->offset:D

    .line 437
    :goto_1
    if-nez p2, :cond_2

    .line 438
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    .line 526
    :goto_2
    return-void

    .line 433
    :cond_1
    iget-wide v2, p3, Lcom/garmin/fit/SubField;->scale:D

    .line 434
    iget-wide v0, p3, Lcom/garmin/fit/SubField;->offset:D

    goto :goto_1

    .line 440
    :cond_2
    instance-of v4, p2, Ljava/lang/Number;

    if-eqz v4, :cond_4

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    cmpl-double v4, v2, v4

    if-nez v4, :cond_3

    const-wide/16 v4, 0x0

    cmpl-double v4, v0, v4

    if-eqz v4, :cond_4

    .line 441
    :cond_3
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v4

    add-double/2addr v0, v4

    mul-double/2addr v0, v2

    .line 442
    iget v2, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_2

    .line 448
    :sswitch_0
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-short v0, v0

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 451
    :sswitch_1
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    int-to-byte v0, v0

    invoke-static {v0}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 456
    :sswitch_2
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    long-to-int v0, v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 460
    :sswitch_3
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Math;->round(D)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 463
    :sswitch_4
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    double-to-float v0, v0

    invoke-static {v0}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 466
    :sswitch_5
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_2

    .line 469
    :sswitch_6
    iget-object v2, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-static {v0, v1}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Double;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, p1, v0}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 475
    :cond_4
    instance-of v0, p2, Ljava/lang/String;

    if-eqz v0, :cond_5

    move-object v0, p2

    check-cast v0, Ljava/lang/String;

    const-string v1, ""

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 476
    iget v0, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v0, :sswitch_data_1

    goto/16 :goto_2

    .line 478
    :sswitch_7
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 481
    :sswitch_8
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 484
    :sswitch_9
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 487
    :sswitch_a
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 490
    :sswitch_b
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 493
    :sswitch_c
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 496
    :sswitch_d
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 499
    :sswitch_e
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 502
    :sswitch_f
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 505
    :sswitch_10
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 508
    :sswitch_11
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 511
    :sswitch_12
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 514
    :sswitch_13
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 517
    :sswitch_14
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    sget-object v1, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    invoke-virtual {v0, p1, v1}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 524
    :cond_5
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0, p1, p2}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto/16 :goto_2

    .line 442
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_1
        0x2 -> :sswitch_0
        0x7 -> :sswitch_6
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x83 -> :sswitch_0
        0x84 -> :sswitch_2
        0x85 -> :sswitch_2
        0x86 -> :sswitch_3
        0x88 -> :sswitch_4
        0x89 -> :sswitch_5
        0x8b -> :sswitch_2
        0x8c -> :sswitch_3
    .end sparse-switch

    .line 476
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_7
        0x1 -> :sswitch_8
        0x2 -> :sswitch_9
        0x7 -> :sswitch_11
        0xa -> :sswitch_a
        0xd -> :sswitch_14
        0x83 -> :sswitch_b
        0x84 -> :sswitch_c
        0x85 -> :sswitch_e
        0x86 -> :sswitch_f
        0x88 -> :sswitch_12
        0x89 -> :sswitch_13
        0x8b -> :sswitch_d
        0x8c -> :sswitch_10
    .end sparse-switch
.end method

.method private writeValue(Ljava/io/OutputStream;Ljava/lang/Object;)V
    .locals 6

    .prologue
    .line 1004
    :try_start_0
    new-instance v0, Ljava/io/DataOutputStream;

    invoke-direct {v0, p1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    .line 1006
    if-nez p2, :cond_0

    .line 1007
    iget v1, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v1, :sswitch_data_0

    .line 1104
    :goto_0
    return-void

    .line 1009
    :sswitch_0
    sget-object v1, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1102
    :catch_0
    move-exception v0

    goto :goto_0

    .line 1012
    :sswitch_1
    sget-object v1, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1015
    :sswitch_2
    sget-object v1, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1018
    :sswitch_3
    sget-object v1, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    invoke-virtual {v1}, Ljava/lang/Byte;->byteValue()B

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1021
    :sswitch_4
    sget-object v1, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1024
    :sswitch_5
    sget-object v1, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto :goto_0

    .line 1027
    :sswitch_6
    sget-object v1, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto :goto_0

    .line 1030
    :sswitch_7
    sget-object v1, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto :goto_0

    .line 1033
    :sswitch_8
    sget-object v1, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0

    .line 1036
    :sswitch_9
    sget-object v1, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0

    .line 1039
    :sswitch_a
    sget-object v1, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    invoke-virtual {v1}, Ljava/lang/Long;->longValue()J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto :goto_0

    .line 1042
    :sswitch_b
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto :goto_0

    .line 1045
    :sswitch_c
    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    invoke-virtual {v1}, Ljava/lang/Float;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto/16 :goto_0

    .line 1048
    :sswitch_d
    sget-object v1, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    invoke-virtual {v1}, Ljava/lang/Double;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/io/DataOutputStream;->writeDouble(D)V

    goto/16 :goto_0

    .line 1054
    :cond_0
    iget v1, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v1, :sswitch_data_1

    goto/16 :goto_0

    .line 1060
    :sswitch_e
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 1061
    sget-object v1, Ljava/lang/System;->err:Ljava/io/PrintStream;

    const-string v2, "Field.write(): Field %s value should not be string value %s\n"

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    const/4 v4, 0x1

    aput-object p2, v3, v4

    invoke-virtual {v1, v2, v3}, Ljava/io/PrintStream;->printf(Ljava/lang/String;[Ljava/lang/Object;)Ljava/io/PrintStream;

    .line 1062
    :cond_1
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeByte(I)V

    goto/16 :goto_0

    .line 1069
    :sswitch_f
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeShort(I)V

    goto/16 :goto_0

    .line 1076
    :sswitch_10
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    invoke-static {v1, v2}, Ljava/lang/Math;->round(D)J

    move-result-wide v1

    long-to-int v1, v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeInt(I)V

    goto/16 :goto_0

    .line 1081
    :sswitch_11
    new-instance v0, Ljava/io/OutputStreamWriter;

    const-string v1, "UTF-8"

    invoke-direct {v0, p1, v1}, Ljava/io/OutputStreamWriter;-><init>(Ljava/io/OutputStream;Ljava/lang/String;)V

    .line 1082
    invoke-virtual {p2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/io/OutputStreamWriter;->write(Ljava/lang/String;)V

    .line 1083
    invoke-virtual {v0}, Ljava/io/OutputStreamWriter;->flush()V

    .line 1084
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    goto/16 :goto_0

    .line 1089
    :sswitch_12
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->floatValue()F

    move-result v1

    invoke-virtual {v0, v1}, Ljava/io/DataOutputStream;->writeFloat(F)V

    goto/16 :goto_0

    .line 1094
    :sswitch_13
    check-cast p2, Ljava/lang/Number;

    invoke-virtual {p2}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v1

    invoke-virtual {v0, v1, v2}, Ljava/io/DataOutputStream;->writeDouble(D)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 1007
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_1
        0x7 -> :sswitch_b
        0xa -> :sswitch_2
        0xd -> :sswitch_4
        0x83 -> :sswitch_5
        0x84 -> :sswitch_6
        0x85 -> :sswitch_8
        0x86 -> :sswitch_9
        0x88 -> :sswitch_c
        0x89 -> :sswitch_d
        0x8b -> :sswitch_7
        0x8c -> :sswitch_a
    .end sparse-switch

    .line 1054
    :sswitch_data_1
    .sparse-switch
        0x0 -> :sswitch_e
        0x1 -> :sswitch_e
        0x2 -> :sswitch_e
        0x7 -> :sswitch_11
        0xa -> :sswitch_e
        0xd -> :sswitch_e
        0x83 -> :sswitch_f
        0x84 -> :sswitch_f
        0x85 -> :sswitch_10
        0x86 -> :sswitch_10
        0x88 -> :sswitch_12
        0x89 -> :sswitch_13
        0x8b -> :sswitch_f
        0x8c -> :sswitch_10
    .end sparse-switch
.end method


# virtual methods
.method public addValue(Ljava/lang/Object;)V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 242
    instance-of v0, p1, Ljava/lang/Number;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/garmin/fit/Field;->type:I

    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 243
    invoke-direct {p0, v2, v3}, Lcom/garmin/fit/Field;->getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    .line 244
    check-cast p1, Ljava/lang/Number;

    .line 246
    if-nez v0, :cond_0

    .line 247
    const-string v0, ""

    .line 249
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p1}, Ljava/lang/Number;->intValue()I

    move-result v1

    int-to-char v1, v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(C)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 250
    invoke-direct {p0, v2, v0, v3}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_1
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public getBitsValue(IIZ)Ljava/lang/Long;
    .locals 12

    .prologue
    .line 529
    const-wide/16 v2, 0x0

    .line 530
    const/4 v1, 0x0

    .line 532
    const/4 v0, 0x0

    move v4, v1

    move-wide v10, v2

    move-wide v1, v10

    .line 536
    :goto_0
    if-ge v4, p2, :cond_2

    .line 537
    add-int/lit8 v3, v0, 0x1

    const/4 v5, 0x0

    invoke-direct {p0, v0, v5}, Lcom/garmin/fit/Field;->getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;

    move-result-object v0

    .line 539
    if-nez v0, :cond_0

    .line 540
    const/4 v0, 0x0

    .line 565
    :goto_1
    return-object v0

    .line 542
    :cond_0
    invoke-virtual {v0}, Ljava/lang/Long;->longValue()J

    move-result-wide v5

    shr-long/2addr v5, p1

    invoke-static {v5, v6}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    .line 543
    sget-object v0, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v6, p0, Lcom/garmin/fit/Field;->type:I

    and-int/lit8 v6, v6, 0x1f

    aget v0, v0, v6

    mul-int/lit8 v0, v0, 0x8

    sub-int/2addr v0, p1

    .line 544
    sget-object v6, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v7, p0, Lcom/garmin/fit/Field;->type:I

    and-int/lit8 v7, v7, 0x1f

    aget v6, v6, v7

    mul-int/lit8 v6, v6, 0x8

    sub-int/2addr p1, v6

    .line 546
    if-lez v0, :cond_4

    .line 547
    const/4 p1, 0x0

    .line 549
    sub-int v6, p2, v4

    if-le v0, v6, :cond_1

    .line 550
    sub-int v0, p2, v4

    .line 552
    :cond_1
    const-wide/16 v6, 0x1

    shl-long/2addr v6, v0

    const-wide/16 v8, 0x1

    sub-long/2addr v6, v8

    .line 553
    invoke-virtual {v5}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    and-long v5, v8, v6

    shl-long/2addr v5, v4

    or-long/2addr v1, v5

    .line 554
    add-int/2addr v0, v4

    move v4, v0

    move v0, v3

    goto :goto_0

    .line 558
    :cond_2
    if-eqz p3, :cond_3

    .line 559
    const-wide/16 v3, 0x1

    add-int/lit8 v0, p2, -0x1

    shl-long/2addr v3, v0

    .line 561
    and-long v5, v1, v3

    const-wide/16 v7, 0x0

    cmp-long v0, v5, v7

    if-eqz v0, :cond_3

    .line 562
    neg-long v5, v3

    const-wide/16 v7, 0x1

    sub-long/2addr v3, v7

    and-long v0, v1, v3

    add-long/2addr v0, v5

    .line 565
    :goto_2
    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    goto :goto_1

    :cond_3
    move-wide v0, v1

    goto :goto_2

    :cond_4
    move v0, v3

    goto :goto_0
.end method

.method public getByteValue()Ljava/lang/Byte;
    .locals 2

    .prologue
    .line 569
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getByteValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getByteValue(I)Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 573
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getByteValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getByteValue(II)Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 577
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getByteValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getByteValue(ILjava/lang/String;)Ljava/lang/Byte;
    .locals 1

    .prologue
    .line 581
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getByteValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getDoubleValue()Ljava/lang/Double;
    .locals 2

    .prologue
    .line 694
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getDoubleValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getDoubleValue(I)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 698
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getDoubleValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getDoubleValue(II)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 702
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getDoubleValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getDoubleValue(ILjava/lang/String;)Ljava/lang/Double;
    .locals 1

    .prologue
    .line 706
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getDoubleValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Double;

    move-result-object v0

    return-object v0
.end method

.method public getFloatValue()Ljava/lang/Float;
    .locals 2

    .prologue
    .line 669
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getFloatValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getFloatValue(I)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 673
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getFloatValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getFloatValue(II)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 677
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getFloatValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getFloatValue(ILjava/lang/String;)Ljava/lang/Float;
    .locals 1

    .prologue
    .line 681
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getFloatValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getIntegerValue()Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 619
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getIntegerValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getIntegerValue(I)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 623
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getIntegerValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getIntegerValue(II)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 627
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getIntegerValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getIntegerValue(ILjava/lang/String;)Ljava/lang/Integer;
    .locals 1

    .prologue
    .line 631
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getIntegerValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getIsAccumulated()Z
    .locals 1

    .prologue
    .line 160
    iget-boolean v0, p0, Lcom/garmin/fit/Field;->isAccumulated:Z

    return v0
.end method

.method public getLongValue()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 644
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getLongValue(I)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 648
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getLongValue(II)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 652
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getLongValue(ILjava/lang/String;)Ljava/lang/Long;
    .locals 1

    .prologue
    .line 656
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getLongValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getNameInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getNameInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getName(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 107
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getNameInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNum()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/garmin/fit/Field;->num:I

    return v0
.end method

.method public getNumValues()I
    .locals 1

    .prologue
    .line 258
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method public getRawValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 262
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getRawValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getRawValue(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 266
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getRawValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getRawValue(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 270
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getRawValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getRawValue(ILjava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 274
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getRawValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getShortValue()Ljava/lang/Short;
    .locals 2

    .prologue
    .line 594
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getShortValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getShortValue(I)Ljava/lang/Short;
    .locals 1

    .prologue
    .line 598
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getShortValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getShortValue(II)Ljava/lang/Short;
    .locals 1

    .prologue
    .line 602
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getShortValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getShortValue(ILjava/lang/String;)Ljava/lang/Short;
    .locals 1

    .prologue
    .line 606
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getShortValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method protected getSize()I
    .locals 4

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 166
    iget v1, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v1, :sswitch_data_0

    .line 196
    :cond_0
    :goto_0
    return v0

    .line 180
    :sswitch_0
    invoke-virtual {p0}, Lcom/garmin/fit/Field;->getNumValues()I

    move-result v0

    sget-object v1, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v2, p0, Lcom/garmin/fit/Field;->type:I

    and-int/lit8 v2, v2, 0x1f

    aget v1, v1, v2

    mul-int/2addr v0, v1

    .line 181
    goto :goto_0

    .line 184
    :sswitch_1
    iget-object v1, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    .line 186
    :try_start_0
    invoke-virtual {v2}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v2

    const-string v3, "UTF-8"

    invoke-virtual {v2, v3}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v2

    array-length v2, v2
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0

    add-int/lit8 v2, v2, 0x1

    add-int/2addr v0, v2

    goto :goto_1

    .line 187
    :catch_0
    move-exception v2

    goto :goto_1

    .line 166
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_0
        0x2 -> :sswitch_0
        0x7 -> :sswitch_1
        0xa -> :sswitch_0
        0xd -> :sswitch_0
        0x83 -> :sswitch_0
        0x84 -> :sswitch_0
        0x85 -> :sswitch_0
        0x86 -> :sswitch_0
        0x88 -> :sswitch_0
        0x89 -> :sswitch_0
        0x8b -> :sswitch_0
        0x8c -> :sswitch_0
    .end sparse-switch
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 719
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringValue(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 723
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringValue(II)Ljava/lang/String;
    .locals 1

    .prologue
    .line 727
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getStringValue(ILjava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 731
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getStringValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getSubField(I)Lcom/garmin/fit/SubField;
    .locals 1

    .prologue
    .line 209
    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 210
    iget-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    .line 212
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;
    .locals 2

    .prologue
    .line 200
    const/4 v0, 0x0

    move v1, v0

    :goto_0
    iget-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_1

    .line 201
    iget-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    iget-object v0, v0, Lcom/garmin/fit/SubField;->name:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 202
    iget-object v0, p0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    .line 205
    :goto_1
    return-object v0

    .line 200
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 205
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 122
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getTypeInternal(Lcom/garmin/fit/SubField;)I

    move-result v0

    return v0
.end method

.method public getType(I)I
    .locals 1

    .prologue
    .line 126
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getTypeInternal(Lcom/garmin/fit/SubField;)I

    move-result v0

    return v0
.end method

.method public getType(Ljava/lang/String;)I
    .locals 1

    .prologue
    .line 130
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getTypeInternal(Lcom/garmin/fit/SubField;)I

    move-result v0

    return v0
.end method

.method public getUnits()Ljava/lang/String;
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getUnitsInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnits(I)Ljava/lang/String;
    .locals 1

    .prologue
    .line 145
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getUnitsInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getUnits(Ljava/lang/String;)Ljava/lang/String;
    .locals 1

    .prologue
    .line 149
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->getUnitsInternal(Lcom/garmin/fit/SubField;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getValue()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 288
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValue(I)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 292
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValue(II)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 296
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getValue(ILjava/lang/String;)Ljava/lang/Object;
    .locals 1

    .prologue
    .line 300
    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/garmin/fit/Field;->getValueInternal(ILcom/garmin/fit/SubField;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected isSignedInteger()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->isSignedIntegerInternal(Lcom/garmin/fit/SubField;)Z

    move-result v0

    return v0
.end method

.method protected isSignedInteger(I)Z
    .locals 1

    .prologue
    .line 220
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->isSignedIntegerInternal(Lcom/garmin/fit/SubField;)Z

    move-result v0

    return v0
.end method

.method protected isSignedInteger(Ljava/lang/String;)Z
    .locals 1

    .prologue
    .line 224
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Field;->isSignedIntegerInternal(Lcom/garmin/fit/SubField;)Z

    move-result v0

    return v0
.end method

.method protected read(Ljava/io/InputStream;I)Z
    .locals 9

    .prologue
    const/4 v1, 0x1

    const/16 v8, 0x8

    const/4 v2, 0x0

    .line 803
    :try_start_0
    new-instance v3, Ljava/io/DataInputStream;

    invoke-direct {v3, p1}, Ljava/io/DataInputStream;-><init>(Ljava/io/InputStream;)V

    .line 804
    iget v0, p0, Lcom/garmin/fit/Field;->type:I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1

    const/4 v4, 0x7

    if-ne v0, v4, :cond_6

    .line 806
    :try_start_1
    new-instance v4, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v4}, Ljava/io/ByteArrayOutputStream;-><init>()V

    move v3, v2

    move v0, v2

    .line 809
    :goto_0
    if-ge v3, p2, :cond_3

    .line 811
    invoke-virtual {p1}, Ljava/io/InputStream;->read()I

    move-result v5

    .line 812
    if-nez v5, :cond_2

    .line 813
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v5

    if-lez v5, :cond_1

    .line 814
    :goto_1
    if-lez v0, :cond_0

    .line 815
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/String;

    invoke-direct {v6}, Ljava/lang/String;-><init>()V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 816
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 819
    :cond_0
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v6, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v7

    const-string v8, "UTF-8"

    invoke-direct {v6, v7, v8}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v5, v6}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 824
    :goto_2
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 809
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 821
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 826
    :cond_2
    invoke-virtual {v4, v5}, Ljava/io/ByteArrayOutputStream;->write(I)V

    goto :goto_3

    .line 838
    :catch_0
    move-exception v0

    move v0, v1

    .line 981
    :goto_4
    return v0

    .line 830
    :cond_3
    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->size()I

    move-result v3

    if-lez v3, :cond_5

    .line 831
    :goto_5
    if-lez v0, :cond_4

    .line 832
    iget-object v3, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v5, Ljava/lang/String;

    invoke-direct {v5}, Ljava/lang/String;-><init>()V

    invoke-virtual {v3, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 833
    add-int/lit8 v0, v0, -0x1

    goto :goto_5

    .line 836
    :cond_4
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    new-instance v3, Ljava/lang/String;

    invoke-virtual {v4}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v4

    const-string v5, "UTF-8"

    invoke-direct {v3, v4, v5}, Ljava/lang/String;-><init>([BLjava/lang/String;)V

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/io/EOFException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1

    :cond_5
    :goto_6
    move v0, v1

    .line 981
    goto :goto_4

    :cond_6
    move v0, v1

    .line 845
    :goto_7
    if-lez p2, :cond_8

    .line 846
    :try_start_2
    iget v4, p0, Lcom/garmin/fit/Field;->type:I

    sparse-switch v4, :sswitch_data_0

    move v0, v2

    .line 968
    goto :goto_4

    .line 848
    :sswitch_0
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    .line 849
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 850
    sget-object v5, Lcom/garmin/fit/Fit;->ENUM_INVALID:Ljava/lang/Short;

    invoke-virtual {v4, v5}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 971
    :cond_7
    :goto_8
    sget-object v4, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v5, p0, Lcom/garmin/fit/Field;->type:I

    and-int/lit8 v5, v5, 0x1f

    aget v4, v4, v5

    sub-int/2addr p2, v4

    goto :goto_7

    .line 856
    :sswitch_1
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    .line 857
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 858
    sget-object v5, Lcom/garmin/fit/Fit;->UINT8_INVALID:Ljava/lang/Short;

    invoke-virtual {v4, v5}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 859
    goto :goto_8

    .line 864
    :sswitch_2
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    .line 865
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 866
    sget-object v5, Lcom/garmin/fit/Fit;->UINT8Z_INVALID:Ljava/lang/Short;

    invoke-virtual {v4, v5}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 867
    goto :goto_8

    .line 872
    :sswitch_3
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    invoke-static {v4}, Ljava/lang/Byte;->valueOf(B)Ljava/lang/Byte;

    move-result-object v4

    .line 873
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 874
    sget-object v5, Lcom/garmin/fit/Fit;->SINT8_INVALID:Ljava/lang/Byte;

    invoke-virtual {v4, v5}, Ljava/lang/Byte;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 875
    goto :goto_8

    .line 880
    :sswitch_4
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readShort()S

    move-result v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    .line 881
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 882
    sget-object v5, Lcom/garmin/fit/Fit;->SINT16_INVALID:Ljava/lang/Short;

    invoke-virtual {v4, v5}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 883
    goto :goto_8

    .line 888
    :sswitch_5
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 889
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 890
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 891
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 892
    sget-object v5, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 893
    goto/16 :goto_8

    .line 898
    :sswitch_6
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 899
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    shl-int/lit8 v4, v4, 0x8

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 900
    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v5

    and-int/lit16 v5, v5, 0xff

    or-int/2addr v4, v5

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 901
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 902
    sget-object v5, Lcom/garmin/fit/Fit;->UINT16Z_INVALID:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 903
    goto/16 :goto_8

    .line 908
    :sswitch_7
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readInt()I

    move-result v4

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    .line 909
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 910
    sget-object v5, Lcom/garmin/fit/Fit;->SINT32_INVALID:Ljava/lang/Integer;

    invoke-virtual {v4, v5}, Ljava/lang/Integer;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 911
    goto/16 :goto_8

    .line 916
    :sswitch_8
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 917
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 918
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 919
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 920
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 921
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 922
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 923
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 924
    sget-object v5, Lcom/garmin/fit/Fit;->UINT32_INVALID:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 925
    goto/16 :goto_8

    .line 930
    :sswitch_9
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-long v4, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 931
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 932
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 933
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 934
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 935
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    shl-long/2addr v4, v8

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 936
    invoke-virtual {v4}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v6

    and-int/lit16 v6, v6, 0xff

    int-to-long v6, v6

    or-long/2addr v4, v6

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    .line 937
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 938
    sget-object v5, Lcom/garmin/fit/Fit;->UINT32Z_INVALID:Ljava/lang/Long;

    invoke-virtual {v4, v5}, Ljava/lang/Long;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 939
    goto/16 :goto_8

    .line 944
    :sswitch_a
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readFloat()F

    move-result v4

    invoke-static {v4}, Ljava/lang/Float;->valueOf(F)Ljava/lang/Float;

    move-result-object v4

    .line 945
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 946
    sget-object v5, Lcom/garmin/fit/Fit;->FLOAT32_INVALID:Ljava/lang/Float;

    invoke-virtual {v4, v5}, Ljava/lang/Float;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 947
    goto/16 :goto_8

    .line 952
    :sswitch_b
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readDouble()D

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Double;->valueOf(D)Ljava/lang/Double;

    move-result-object v4

    .line 953
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 954
    sget-object v5, Lcom/garmin/fit/Fit;->FLOAT64_INVALID:Ljava/lang/Double;

    invoke-virtual {v4, v5}, Ljava/lang/Double;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 955
    goto/16 :goto_8

    .line 960
    :sswitch_c
    invoke-virtual {v3}, Ljava/io/DataInputStream;->readByte()B

    move-result v4

    and-int/lit16 v4, v4, 0xff

    int-to-short v4, v4

    invoke-static {v4}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v4

    .line 961
    iget-object v5, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v5, v4}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 962
    sget-object v5, Lcom/garmin/fit/Fit;->BYTE_INVALID:Ljava/lang/Short;

    invoke-virtual {v4, v5}, Ljava/lang/Short;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_7

    move v0, v2

    .line 963
    goto/16 :goto_8

    .line 974
    :cond_8
    if-eqz v0, :cond_5

    .line 975
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_6

    .line 977
    :catch_1
    move-exception v0

    move v0, v2

    .line 978
    goto/16 :goto_4

    .line 846
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_3
        0x2 -> :sswitch_1
        0xa -> :sswitch_2
        0xd -> :sswitch_c
        0x83 -> :sswitch_4
        0x84 -> :sswitch_5
        0x85 -> :sswitch_7
        0x86 -> :sswitch_8
        0x88 -> :sswitch_a
        0x89 -> :sswitch_b
        0x8b -> :sswitch_6
        0x8c -> :sswitch_9
    .end sparse-switch
.end method

.method public setValue(ILjava/lang/Object;)V
    .locals 1

    .prologue
    .line 401
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 402
    return-void
.end method

.method public setValue(ILjava/lang/Object;I)V
    .locals 3

    .prologue
    .line 405
    const/4 v0, 0x0

    .line 407
    const v1, 0xffff

    if-eq p3, v1, :cond_0

    .line 408
    invoke-virtual {p0, p3}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v0

    .line 410
    if-nez v0, :cond_0

    .line 411
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "com.garmin.fit.Field.setValue(): "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " is not a valid subfield index of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/garmin/fit/Field;->name:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 414
    :cond_0
    invoke-direct {p0, p1, p2, v0}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 415
    return-void
.end method

.method public setValue(ILjava/lang/Object;Ljava/lang/String;)V
    .locals 1

    .prologue
    .line 418
    invoke-virtual {p0, p3}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v0

    invoke-direct {p0, p1, p2, v0}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 419
    return-void
.end method

.method public setValue(Ljava/lang/Object;)V
    .locals 2

    .prologue
    .line 389
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, p1, v1}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 390
    return-void
.end method

.method public setValue(Ljava/lang/Object;I)V
    .locals 2

    .prologue
    .line 393
    const/4 v0, 0x0

    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(I)Lcom/garmin/fit/SubField;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 394
    return-void
.end method

.method public setValue(Ljava/lang/Object;Ljava/lang/String;)V
    .locals 2

    .prologue
    .line 397
    const/4 v0, 0x0

    invoke-virtual {p0, p2}, Lcom/garmin/fit/Field;->getSubField(Ljava/lang/String;)Lcom/garmin/fit/SubField;

    move-result-object v1

    invoke-direct {p0, v0, p1, v1}, Lcom/garmin/fit/Field;->setValueInternal(ILjava/lang/Object;Lcom/garmin/fit/SubField;)V

    .line 398
    return-void
.end method

.method protected write(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 997
    iget-object v0, p0, Lcom/garmin/fit/Field;->values:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    .line 998
    invoke-direct {p0, p1, v1}, Lcom/garmin/fit/Field;->writeValue(Ljava/io/OutputStream;Ljava/lang/Object;)V

    goto :goto_0

    .line 1000
    :cond_0
    return-void
.end method

.method protected write(Ljava/io/OutputStream;Lcom/garmin/fit/FieldDefinition;)V
    .locals 3

    .prologue
    .line 985
    iget v0, p2, Lcom/garmin/fit/FieldDefinition;->size:I

    invoke-virtual {p0}, Lcom/garmin/fit/Field;->getSize()I

    move-result v1

    sub-int/2addr v0, v1

    .line 987
    invoke-virtual {p0, p1}, Lcom/garmin/fit/Field;->write(Ljava/io/OutputStream;)V

    .line 990
    :goto_0
    if-lez v0, :cond_0

    .line 991
    const/4 v1, 0x0

    invoke-direct {p0, p1, v1}, Lcom/garmin/fit/Field;->writeValue(Ljava/io/OutputStream;Ljava/lang/Object;)V

    .line 992
    sget-object v1, Lcom/garmin/fit/Fit;->baseTypeSizes:[I

    iget v2, p0, Lcom/garmin/fit/Field;->type:I

    and-int/lit8 v2, v2, 0x1f

    aget v1, v1, v2

    sub-int/2addr v0, v1

    goto :goto_0

    .line 994
    :cond_0
    return-void
.end method
