.class public Lcom/garmin/fit/TotalsMesg;
.super Lcom/garmin/fit/Mesg;
.source "TotalsMesg.java"


# static fields
.field protected static final totalsMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "totals"

    const/16 v2, 0x21

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "timestamp"

    const/16 v2, 0xfd

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "timer_time"

    const/4 v2, 0x0

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "distance"

    const/4 v2, 0x1

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "calories"

    const/4 v2, 0x2

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "kcal"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sport"

    const/4 v2, 0x3

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "elapsed_time"

    const/4 v2, 0x4

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sessions"

    const/4 v2, 0x5

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/TotalsMesg;->totalsMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "active_time"

    const/4 v2, 0x6

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 48
    const/16 v0, 0x21

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 49
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 53
    return-void
.end method


# virtual methods
.method public getActiveTime()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 224
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getCalories()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 143
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getDistance()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 123
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getElapsedTime()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 62
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSessions()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 205
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSport()Lcom/garmin/fit/Sport;
    .locals 3

    .prologue
    .line 162
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 163
    if-nez v0, :cond_0

    .line 164
    const/4 v0, 0x0

    .line 165
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Sport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimerTime()Ljava/lang/Long;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 102
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 81
    const/16 v0, 0xfd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/TotalsMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/TotalsMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public setActiveTime(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 234
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 235
    return-void
.end method

.method public setCalories(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 153
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 154
    return-void
.end method

.method public setDistance(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 133
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 134
    return-void
.end method

.method public setElapsedTime(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 196
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 197
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 71
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 72
    return-void
.end method

.method public setSessions(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 214
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 215
    return-void
.end method

.method public setSport(Lcom/garmin/fit/Sport;)V
    .locals 4

    .prologue
    .line 174
    const/4 v0, 0x3

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Sport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 175
    return-void
.end method

.method public setTimerTime(Ljava/lang/Long;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 113
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, p1, v0}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 114
    return-void
.end method

.method public setTimestamp(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 91
    const/16 v0, 0xfd

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/TotalsMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 92
    return-void
.end method
