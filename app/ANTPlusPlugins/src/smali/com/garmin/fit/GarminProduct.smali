.class public Lcom/garmin/fit/GarminProduct;
.super Ljava/lang/Object;
.source "GarminProduct.java"


# static fields
.field public static final ALF04:I = 0x53d

.field public static final AMX:I = 0x5b5

.field public static final ANDROID_ANTPLUS_PLUGIN:I = 0xfffc

.field public static final AXB01:I = 0x3

.field public static final AXB02:I = 0x4

.field public static final AXH01:I = 0x2

.field public static final BCM:I = 0xa

.field public static final BSM:I = 0x9

.field public static final CHIRP:I = 0x4e5

.field public static final CONNECT:I = 0xfffe

.field public static final DSI_ALF01:I = 0x3f3

.field public static final DSI_ALF02:I = 0x6

.field public static final EDGE1000:I = 0x72c

.field public static final EDGE200:I = 0x52d

.field public static final EDGE200_TAIWAN:I = 0x613

.field public static final EDGE500:I = 0x40c

.field public static final EDGE500_CHINA:I = 0x56b

.field public static final EDGE500_JAPAN:I = 0x4bd

.field public static final EDGE500_KOREA:I = 0x58e

.field public static final EDGE500_TAIWAN:I = 0x4af

.field public static final EDGE510:I = 0x619

.field public static final EDGE510_ASIA:I = 0x71d

.field public static final EDGE510_JAPAN:I = 0x6ce

.field public static final EDGE510_KOREA:I = 0x77e

.field public static final EDGE800:I = 0x491

.field public static final EDGE800_CHINA:I = 0x56a

.field public static final EDGE800_JAPAN:I = 0x536

.field public static final EDGE800_KOREA:I = 0x5d9

.field public static final EDGE800_TAIWAN:I = 0x535

.field public static final EDGE810:I = 0x61f

.field public static final EDGE810_CHINA:I = 0x71e

.field public static final EDGE810_JAPAN:I = 0x6b9

.field public static final EDGE810_TAIWAN:I = 0x71f

.field public static final EDGE_REMOTE:I = 0x271e

.field public static final EDGE_TOURING:I = 0x6c8

.field public static final FENIX:I = 0x60f

.field public static final FR10:I = 0x5ca

.field public static final FR10_JAPAN:I = 0x698

.field public static final FR110:I = 0x464

.field public static final FR110_JAPAN:I = 0x4fa

.field public static final FR210_JAPAN:I = 0x550

.field public static final FR220:I = 0x660

.field public static final FR220_CHINA:I = 0x78b

.field public static final FR220_JAPAN:I = 0x78a

.field public static final FR301_CHINA:I = 0x1d9

.field public static final FR301_JAPAN:I = 0x1da

.field public static final FR301_KOREA:I = 0x1db

.field public static final FR301_TAIWAN:I = 0x1ee

.field public static final FR310XT:I = 0x3fa

.field public static final FR310XT_4T:I = 0x5a6

.field public static final FR405:I = 0x2cd

.field public static final FR405_JAPAN:I = 0x3db

.field public static final FR50:I = 0x30e

.field public static final FR60:I = 0x3dc

.field public static final FR610:I = 0x541

.field public static final FR610_JAPAN:I = 0x582

.field public static final FR620:I = 0x657

.field public static final FR620_CHINA:I = 0x789

.field public static final FR620_JAPAN:I = 0x788

.field public static final FR70:I = 0x59c

.field public static final FR910XT:I = 0x530

.field public static final FR910XT_CHINA:I = 0x601

.field public static final FR910XT_JAPAN:I = 0x640

.field public static final FR910XT_KOREA:I = 0x680

.field public static final HRM1:I = 0x1

.field public static final HRM2SS:I = 0x5

.field public static final HRM3SS:I = 0x7

.field public static final HRM_RUN:I = 0x6d8

.field public static final HRM_RUN_SINGLE_BYTE_PRODUCT_ID:I = 0x8

.field public static final INVALID:I

.field public static final SDM4:I = 0x2717

.field public static final SWIM:I = 0x5db

.field public static final TEMPE:I = 0x622

.field public static final TRAINING_CENTER:I = 0x4e97

.field public static final VECTOR_CP:I = 0x565

.field public static final VECTOR_SS:I = 0x564

.field public static final VIRB_ELITE:I = 0x6c7

.field public static final VIRB_REMOTE:I = 0x73d

.field public static final VIVO_FIT:I = 0x72d

.field public static final VIVO_KI:I = 0x75d


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 100
    sget-object v0, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/garmin/fit/GarminProduct;->INVALID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
