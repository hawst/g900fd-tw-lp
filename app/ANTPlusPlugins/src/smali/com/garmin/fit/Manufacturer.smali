.class public Lcom/garmin/fit/Manufacturer;
.super Ljava/lang/Object;
.source "Manufacturer.java"


# static fields
.field public static final ACE_SENSOR:I = 0x2b

.field public static final ACORN_PROJECTS_APS:I = 0x4f

.field public static final ACTIGRAPHCORP:I = 0x167f

.field public static final ALATECH_TECHNOLOGY_LTD:I = 0x3a

.field public static final ARCHINOETICS:I = 0x22

.field public static final A_AND_D:I = 0x15

.field public static final BEURER:I = 0x13

.field public static final BF1SYSTEMS:I = 0x2f

.field public static final BKOOL:I = 0x43

.field public static final BONTRAGER:I = 0x51

.field public static final BREAKAWAY:I = 0x39

.field public static final BRIM_BROTHERS:I = 0x2c

.field public static final CARDIOSPORT:I = 0x14

.field public static final CATEYE:I = 0x44

.field public static final CICLOSPORT:I = 0x4d

.field public static final CITIZEN_SYSTEMS:I = 0x24

.field public static final CLEAN_MOBILE:I = 0x1a

.field public static final CONCEPT2:I = 0x28

.field public static final DAYTON:I = 0x4

.field public static final DEVELOPMENT:I = 0xff

.field public static final DEXCOM:I = 0x1f

.field public static final DYNASTREAM:I = 0xf

.field public static final DYNASTREAM_OEM:I = 0xd

.field public static final ECHOWELL:I = 0xc

.field public static final ELITE:I = 0x56

.field public static final GARMIN:I = 0x1

.field public static final GARMIN_FR405_ANTFS:I = 0x2

.field public static final GEONAUTE:I = 0x3d

.field public static final GPULSE:I = 0x19

.field public static final HMM:I = 0x16

.field public static final HOLUX:I = 0x27

.field public static final IBIKE:I = 0x8

.field public static final IDT:I = 0x5

.field public static final ID_BIKE:I = 0x3e

.field public static final IFOR_POWELL:I = 0x36

.field public static final INVALID:I

.field public static final LEMOND_FITNESS:I = 0x1e

.field public static final LIFEBEAM:I = 0x50

.field public static final MAGELLAN:I = 0x25

.field public static final MAGURA:I = 0x54

.field public static final MAXWELL_GUIDER:I = 0x37

.field public static final METALOGICS:I = 0x32

.field public static final METRIGEAR:I = 0x11

.field public static final MIO_TECHNOLOGY_EUROPE:I = 0x3b

.field public static final MOXY:I = 0x4c

.field public static final NAUTILUS:I = 0xe

.field public static final NORTH_POLE_ENGINEERING:I = 0x42

.field public static final OCTANE_FITNESS:I = 0x21

.field public static final ONE_GIANT_LEAP:I = 0x2a

.field public static final OSYNCE:I = 0x26

.field public static final PEAKSWARE:I = 0x1c

.field public static final PEDAL_BRAIN:I = 0x1b

.field public static final PERCEPTION_DIGITAL:I = 0x2e

.field public static final PERIPEDAL:I = 0x48

.field public static final PHYSICAL_ENTERPRISES:I = 0x41

.field public static final PIONEER:I = 0x30

.field public static final POWERBAHN:I = 0x4e

.field public static final QUARQ:I = 0x7

.field public static final ROTOR:I = 0x3c

.field public static final SARIS:I = 0x9

.field public static final SAXONAR:I = 0x1d

.field public static final SCOSCHE:I = 0x53

.field public static final SEIKO_EPSON:I = 0x34

.field public static final SEIKO_EPSON_OEM:I = 0x35

.field public static final SIGMASPORT:I = 0x46

.field public static final SPANTEC:I = 0x31

.field public static final SPARK_HK:I = 0xa

.field public static final SPECIALIZED:I = 0x3f

.field public static final SRM:I = 0x6

.field public static final STAGES_CYCLING:I = 0x45

.field public static final STAR_TRAC:I = 0x38

.field public static final SUUNTO:I = 0x17

.field public static final TANITA:I = 0xb

.field public static final THE_HURT_BOX:I = 0x23

.field public static final THITA_ELEKTRONIK:I = 0x18

.field public static final TIMEX:I = 0x10

.field public static final TOMTOM:I = 0x47

.field public static final WAHOO_FITNESS:I = 0x20

.field public static final WATTBIKE:I = 0x49

.field public static final WELLGO:I = 0x52

.field public static final WOODWAY:I = 0x55

.field public static final WTEK:I = 0x40

.field public static final XELIC:I = 0x12

.field public static final XPLOVA:I = 0x2d

.field public static final ZEPHYR:I = 0x3

.field public static final _4IIIIS:I = 0x33


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 106
    sget-object v0, Lcom/garmin/fit/Fit;->UINT16_INVALID:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    sput v0, Lcom/garmin/fit/Manufacturer;->INVALID:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
