.class public Lcom/garmin/fit/GoalMesg;
.super Lcom/garmin/fit/Mesg;
.source "GoalMesg.java"


# static fields
.field protected static final goalMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "goal"

    const/16 v2, 0xf

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sport"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sub_sport"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "start_date"

    const/4 v2, 0x2

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "end_date"

    const/4 v2, 0x3

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "type"

    const/4 v2, 0x4

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "value"

    const/4 v2, 0x5

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "repeat"

    const/4 v2, 0x6

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "target_value"

    const/4 v2, 0x7

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "recurrence"

    const/16 v2, 0x8

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "recurrence_value"

    const/16 v2, 0x9

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    sget-object v10, Lcom/garmin/fit/GoalMesg;->goalMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "enabled"

    const/16 v2, 0xa

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 51
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 54
    const/16 v0, 0xf

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 55
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 59
    return-void
.end method


# virtual methods
.method public getEnabled()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 281
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 282
    if-nez v0, :cond_0

    .line 283
    const/4 v0, 0x0

    .line 284
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getEndDate()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 146
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/GoalMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 68
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getRecurrence()Lcom/garmin/fit/GoalRecurrence;
    .locals 3

    .prologue
    .line 242
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 243
    if-nez v0, :cond_0

    .line 244
    const/4 v0, 0x0

    .line 245
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/GoalRecurrence;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/GoalRecurrence;

    move-result-object v0

    goto :goto_0
.end method

.method public getRecurrenceValue()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 263
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getRepeat()Lcom/garmin/fit/Bool;
    .locals 3

    .prologue
    .line 203
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 204
    if-nez v0, :cond_0

    .line 205
    const/4 v0, 0x0

    .line 206
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Bool;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Bool;

    move-result-object v0

    goto :goto_0
.end method

.method public getSport()Lcom/garmin/fit/Sport;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 86
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 87
    if-nez v0, :cond_0

    .line 88
    const/4 v0, 0x0

    .line 89
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Sport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;

    move-result-object v0

    goto :goto_0
.end method

.method public getStartDate()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 128
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/GoalMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getSubSport()Lcom/garmin/fit/SubSport;
    .locals 3

    .prologue
    .line 107
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 108
    if-nez v0, :cond_0

    .line 109
    const/4 v0, 0x0

    .line 110
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SubSport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SubSport;

    move-result-object v0

    goto :goto_0
.end method

.method public getTargetValue()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 224
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getType()Lcom/garmin/fit/Goal;
    .locals 3

    .prologue
    .line 164
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 165
    if-nez v0, :cond_0

    .line 166
    const/4 v0, 0x0

    .line 167
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Goal;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Goal;

    move-result-object v0

    goto :goto_0
.end method

.method public getValue()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 185
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/GoalMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public setEnabled(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 293
    const/16 v0, 0xa

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 294
    return-void
.end method

.method public setEndDate(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 155
    const/4 v0, 0x3

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 156
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 77
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 78
    return-void
.end method

.method public setRecurrence(Lcom/garmin/fit/GoalRecurrence;)V
    .locals 4

    .prologue
    .line 254
    const/16 v0, 0x8

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/GoalRecurrence;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 255
    return-void
.end method

.method public setRecurrenceValue(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 272
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 273
    return-void
.end method

.method public setRepeat(Lcom/garmin/fit/Bool;)V
    .locals 4

    .prologue
    .line 215
    const/4 v0, 0x6

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Bool;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 216
    return-void
.end method

.method public setSport(Lcom/garmin/fit/Sport;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    iget-short v0, p1, Lcom/garmin/fit/Sport;->value:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    const v1, 0xffff

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 99
    return-void
.end method

.method public setStartDate(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 137
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 138
    return-void
.end method

.method public setSubSport(Lcom/garmin/fit/SubSport;)V
    .locals 4

    .prologue
    .line 119
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SubSport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 120
    return-void
.end method

.method public setTargetValue(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 233
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 234
    return-void
.end method

.method public setType(Lcom/garmin/fit/Goal;)V
    .locals 4

    .prologue
    .line 176
    const/4 v0, 0x4

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Goal;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 177
    return-void
.end method

.method public setValue(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 194
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/GoalMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 195
    return-void
.end method
