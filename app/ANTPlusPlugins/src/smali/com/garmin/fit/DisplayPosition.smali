.class public final enum Lcom/garmin/fit/DisplayPosition;
.super Ljava/lang/Enum;
.source "DisplayPosition.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/DisplayPosition;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/DisplayPosition;

.field public static final enum AUSTRIAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum BORNEO_RSO:Lcom/garmin/fit/DisplayPosition;

.field public static final enum BRITISH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum DEGREE:Lcom/garmin/fit/DisplayPosition;

.field public static final enum DEGREE_MINUTE:Lcom/garmin/fit/DisplayPosition;

.field public static final enum DEGREE_MINUTE_SECOND:Lcom/garmin/fit/DisplayPosition;

.field public static final enum DUTCH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum ESTONIAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum FINNISH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum GERMAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum HUNGARIAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum ICELANDIC_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_0:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IA:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IB:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IIA:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IIB:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IIIA:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IIIB:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IVA:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDIA_ZONE_IVB:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDONESIAN_EQUATORIAL:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDONESIAN_IRIAN:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INDONESIAN_SOUTHERN:Lcom/garmin/fit/DisplayPosition;

.field public static final enum INVALID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum IRISH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum IRISH_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

.field public static final enum LATVIAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum LORAN:Lcom/garmin/fit/DisplayPosition;

.field public static final enum MAIDENHEAD_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum MGRS_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum MODIFIED_SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum NEW_ZEALAND_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum NEW_ZEALAND_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

.field public static final enum QATAR_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum SOUTH_AFRICAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum SWEDISH_REF_99_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum SWISS_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum TAIWAN_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum UNITED_STATES_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum UTM_UPS_GRID:Lcom/garmin/fit/DisplayPosition;

.field public static final enum WEST_MALAYAN:Lcom/garmin/fit/DisplayPosition;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "DEGREE"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->DEGREE:Lcom/garmin/fit/DisplayPosition;

    .line 22
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "DEGREE_MINUTE"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->DEGREE_MINUTE:Lcom/garmin/fit/DisplayPosition;

    .line 23
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "DEGREE_MINUTE_SECOND"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->DEGREE_MINUTE_SECOND:Lcom/garmin/fit/DisplayPosition;

    .line 24
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "AUSTRIAN_GRID"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->AUSTRIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 25
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "BRITISH_GRID"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->BRITISH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 26
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "DUTCH_GRID"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->DUTCH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 27
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "HUNGARIAN_GRID"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->HUNGARIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 28
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "FINNISH_GRID"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->FINNISH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 29
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "GERMAN_GRID"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->GERMAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 30
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "ICELANDIC_GRID"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->ICELANDIC_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 31
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDONESIAN_EQUATORIAL"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_EQUATORIAL:Lcom/garmin/fit/DisplayPosition;

    .line 32
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDONESIAN_IRIAN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_IRIAN:Lcom/garmin/fit/DisplayPosition;

    .line 33
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDONESIAN_SOUTHERN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_SOUTHERN:Lcom/garmin/fit/DisplayPosition;

    .line 34
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_0"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_0:Lcom/garmin/fit/DisplayPosition;

    .line 35
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IA"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IA:Lcom/garmin/fit/DisplayPosition;

    .line 36
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IB"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IB:Lcom/garmin/fit/DisplayPosition;

    .line 37
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IIA"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIA:Lcom/garmin/fit/DisplayPosition;

    .line 38
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IIB"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIB:Lcom/garmin/fit/DisplayPosition;

    .line 39
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IIIA"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIIA:Lcom/garmin/fit/DisplayPosition;

    .line 40
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IIIB"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIIB:Lcom/garmin/fit/DisplayPosition;

    .line 41
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IVA"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IVA:Lcom/garmin/fit/DisplayPosition;

    .line 42
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INDIA_ZONE_IVB"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IVB:Lcom/garmin/fit/DisplayPosition;

    .line 43
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "IRISH_TRANSVERSE"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->IRISH_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

    .line 44
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "IRISH_GRID"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->IRISH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 45
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "LORAN"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->LORAN:Lcom/garmin/fit/DisplayPosition;

    .line 46
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "MAIDENHEAD_GRID"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->MAIDENHEAD_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 47
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "MGRS_GRID"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->MGRS_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 48
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "NEW_ZEALAND_GRID"

    const/16 v2, 0x1b

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->NEW_ZEALAND_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 49
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "NEW_ZEALAND_TRANSVERSE"

    const/16 v2, 0x1c

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->NEW_ZEALAND_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

    .line 50
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "QATAR_GRID"

    const/16 v2, 0x1d

    const/16 v3, 0x1d

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->QATAR_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 51
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "MODIFIED_SWEDISH_GRID"

    const/16 v2, 0x1e

    const/16 v3, 0x1e

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->MODIFIED_SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 52
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "SWEDISH_GRID"

    const/16 v2, 0x1f

    const/16 v3, 0x1f

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 53
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "SOUTH_AFRICAN_GRID"

    const/16 v2, 0x20

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->SOUTH_AFRICAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 54
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "SWISS_GRID"

    const/16 v2, 0x21

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->SWISS_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 55
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "TAIWAN_GRID"

    const/16 v2, 0x22

    const/16 v3, 0x22

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->TAIWAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 56
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "UNITED_STATES_GRID"

    const/16 v2, 0x23

    const/16 v3, 0x23

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->UNITED_STATES_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 57
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "UTM_UPS_GRID"

    const/16 v2, 0x24

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->UTM_UPS_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 58
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "WEST_MALAYAN"

    const/16 v2, 0x25

    const/16 v3, 0x25

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->WEST_MALAYAN:Lcom/garmin/fit/DisplayPosition;

    .line 59
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "BORNEO_RSO"

    const/16 v2, 0x26

    const/16 v3, 0x26

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->BORNEO_RSO:Lcom/garmin/fit/DisplayPosition;

    .line 60
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "ESTONIAN_GRID"

    const/16 v2, 0x27

    const/16 v3, 0x27

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->ESTONIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 61
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "LATVIAN_GRID"

    const/16 v2, 0x28

    const/16 v3, 0x28

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->LATVIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 62
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "SWEDISH_REF_99_GRID"

    const/16 v2, 0x29

    const/16 v3, 0x29

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->SWEDISH_REF_99_GRID:Lcom/garmin/fit/DisplayPosition;

    .line 63
    new-instance v0, Lcom/garmin/fit/DisplayPosition;

    const-string v1, "INVALID"

    const/16 v2, 0x2a

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/DisplayPosition;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->INVALID:Lcom/garmin/fit/DisplayPosition;

    .line 20
    const/16 v0, 0x2b

    new-array v0, v0, [Lcom/garmin/fit/DisplayPosition;

    sget-object v1, Lcom/garmin/fit/DisplayPosition;->DEGREE:Lcom/garmin/fit/DisplayPosition;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/DisplayPosition;->DEGREE_MINUTE:Lcom/garmin/fit/DisplayPosition;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/DisplayPosition;->DEGREE_MINUTE_SECOND:Lcom/garmin/fit/DisplayPosition;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/DisplayPosition;->AUSTRIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/DisplayPosition;->BRITISH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->DUTCH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->HUNGARIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->FINNISH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->GERMAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->ICELANDIC_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_EQUATORIAL:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_IRIAN:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDONESIAN_SOUTHERN:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_0:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IA:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IB:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIA:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIB:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIIA:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IIIB:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IVA:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INDIA_ZONE_IVB:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->IRISH_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->IRISH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->LORAN:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->MAIDENHEAD_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->MGRS_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->NEW_ZEALAND_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->NEW_ZEALAND_TRANSVERSE:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->QATAR_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->MODIFIED_SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->SWEDISH_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->SOUTH_AFRICAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x21

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->SWISS_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x22

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->TAIWAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x23

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->UNITED_STATES_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x24

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->UTM_UPS_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x25

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->WEST_MALAYAN:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x26

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->BORNEO_RSO:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x27

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->ESTONIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x28

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->LATVIAN_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x29

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->SWEDISH_REF_99_GRID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    const/16 v1, 0x2a

    sget-object v2, Lcom/garmin/fit/DisplayPosition;->INVALID:Lcom/garmin/fit/DisplayPosition;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/DisplayPosition;->$VALUES:[Lcom/garmin/fit/DisplayPosition;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 71
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 72
    iput-short p3, p0, Lcom/garmin/fit/DisplayPosition;->value:S

    .line 73
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayPosition;
    .locals 6

    .prologue
    .line 76
    invoke-static {}, Lcom/garmin/fit/DisplayPosition;->values()[Lcom/garmin/fit/DisplayPosition;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 77
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/DisplayPosition;->value:S

    if-ne v4, v5, :cond_0

    .line 81
    :goto_1
    return-object v0

    .line 76
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 81
    :cond_1
    sget-object v0, Lcom/garmin/fit/DisplayPosition;->INVALID:Lcom/garmin/fit/DisplayPosition;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/DisplayPosition;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/DisplayPosition;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/DisplayPosition;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/DisplayPosition;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/DisplayPosition;->$VALUES:[Lcom/garmin/fit/DisplayPosition;

    invoke-virtual {v0}, [Lcom/garmin/fit/DisplayPosition;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/DisplayPosition;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 85
    iget-short v0, p0, Lcom/garmin/fit/DisplayPosition;->value:S

    return v0
.end method
