.class public Lcom/garmin/fit/FileEncoder;
.super Ljava/lang/Object;
.source "FileEncoder.java"

# interfaces
.implements Lcom/garmin/fit/MesgDefinitionListener;
.implements Lcom/garmin/fit/MesgListener;


# instance fields
.field private file:Ljava/io/File;

.field private lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

.field private out:Ljava/io/FileOutputStream;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/garmin/fit/MesgDefinition;

    iput-object v0, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    .line 36
    return-void
.end method

.method public constructor <init>(Ljava/io/File;)V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/garmin/fit/MesgDefinition;

    iput-object v0, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    .line 39
    invoke-virtual {p0, p1}, Lcom/garmin/fit/FileEncoder;->open(Ljava/io/File;)V

    .line 40
    return-void
.end method

.method private writeFileHeader()V
    .locals 11

    .prologue
    const-wide/16 v0, 0x0

    const/16 v4, 0x10

    const/4 v5, 0x0

    const-wide/16 v9, 0xff

    .line 65
    iget-object v2, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    if-nez v2, :cond_0

    .line 66
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "File not open."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 69
    :cond_0
    :try_start_0
    new-instance v6, Ljava/io/RandomAccessFile;

    iget-object v2, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    const-string v3, "rw"

    invoke-direct {v6, v2, v3}, Ljava/io/RandomAccessFile;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 70
    iget-object v2, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    invoke-virtual {v2}, Ljava/io/File;->length()J

    move-result-wide v2

    const-wide/16 v7, 0xe

    sub-long/2addr v2, v7

    .line 74
    cmp-long v7, v2, v0

    if-gez v7, :cond_2

    :goto_0
    move v3, v5

    move v2, v5

    .line 77
    :goto_1
    const/16 v7, 0xc

    if-ge v3, v7, :cond_1

    .line 78
    packed-switch v3, :pswitch_data_0

    .line 94
    :goto_2
    invoke-virtual {v6, v2}, Ljava/io/RandomAccessFile;->write(I)V

    .line 95
    int-to-byte v7, v2

    invoke-static {v5, v7}, Lcom/garmin/fit/CRC;->get16(IB)I

    move-result v5

    .line 77
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 79
    :pswitch_0
    const/16 v2, 0xe

    goto :goto_2

    :pswitch_1
    move v2, v4

    .line 80
    goto :goto_2

    .line 81
    :pswitch_2
    const/16 v2, 0x4c

    goto :goto_2

    .line 82
    :pswitch_3
    const/4 v2, 0x4

    goto :goto_2

    .line 83
    :pswitch_4
    and-long v7, v0, v9

    long-to-int v2, v7

    goto :goto_2

    .line 84
    :pswitch_5
    const/16 v2, 0x8

    shr-long v7, v0, v2

    and-long/2addr v7, v9

    long-to-int v2, v7

    goto :goto_2

    .line 85
    :pswitch_6
    shr-long v7, v0, v4

    and-long/2addr v7, v9

    long-to-int v2, v7

    goto :goto_2

    .line 86
    :pswitch_7
    const/16 v2, 0x18

    shr-long v7, v0, v2

    and-long/2addr v7, v9

    long-to-int v2, v7

    goto :goto_2

    .line 87
    :pswitch_8
    const/16 v2, 0x2e

    goto :goto_2

    .line 88
    :pswitch_9
    const/16 v2, 0x46

    goto :goto_2

    .line 89
    :pswitch_a
    const/16 v2, 0x49

    goto :goto_2

    .line 90
    :pswitch_b
    const/16 v2, 0x54

    goto :goto_2

    .line 98
    :cond_1
    and-int/lit16 v0, v5, 0xff

    invoke-virtual {v6, v0}, Ljava/io/RandomAccessFile;->write(I)V

    .line 99
    shr-int/lit8 v0, v5, 0x8

    and-int/lit16 v0, v0, 0xff

    invoke-virtual {v6, v0}, Ljava/io/RandomAccessFile;->write(I)V

    .line 100
    invoke-virtual {v6}, Ljava/io/RandomAccessFile;->close()V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    return-void

    .line 101
    :catch_0
    move-exception v0

    .line 102
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1

    :cond_2
    move-wide v0, v2

    goto :goto_0

    .line 78
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
    .end packed-switch
.end method


# virtual methods
.method public close()V
    .locals 7

    .prologue
    const/4 v0, 0x0

    .line 168
    iget-object v1, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    if-nez v1, :cond_0

    .line 169
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "File not open."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 172
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    .line 175
    invoke-direct {p0}, Lcom/garmin/fit/FileEncoder;->writeFileHeader()V

    .line 178
    new-instance v2, Ljava/io/FileInputStream;

    iget-object v1, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    invoke-direct {v2, v1}, Ljava/io/FileInputStream;-><init>(Ljava/io/File;)V

    move v1, v0

    .line 180
    :goto_0
    int-to-long v3, v0

    iget-object v5, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    invoke-virtual {v5}, Ljava/io/File;->length()J

    move-result-wide v5

    cmp-long v3, v3, v5

    if-gez v3, :cond_1

    .line 181
    invoke-virtual {v2}, Ljava/io/FileInputStream;->read()I

    move-result v3

    int-to-byte v3, v3

    invoke-static {v1, v3}, Lcom/garmin/fit/CRC;->get16(IB)I

    move-result v1

    .line 180
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 183
    :cond_1
    invoke-virtual {v2}, Ljava/io/FileInputStream;->close()V

    .line 186
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v2, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    const/4 v3, 0x1

    invoke-direct {v0, v2, v3}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    .line 187
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    and-int/lit16 v2, v1, 0xff

    invoke-virtual {v0, v2}, Ljava/io/FileOutputStream;->write(I)V

    .line 188
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    invoke-virtual {v0, v1}, Ljava/io/FileOutputStream;->write(I)V

    .line 190
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    invoke-virtual {v0}, Ljava/io/FileOutputStream;->close()V

    .line 191
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 195
    return-void

    .line 192
    :catch_0
    move-exception v0

    .line 193
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public onMesg(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 110
    invoke-virtual {p0, p1}, Lcom/garmin/fit/FileEncoder;->write(Lcom/garmin/fit/Mesg;)V

    .line 111
    return-void
.end method

.method public onMesgDefinition(Lcom/garmin/fit/MesgDefinition;)V
    .locals 0

    .prologue
    .line 117
    invoke-virtual {p0, p1}, Lcom/garmin/fit/FileEncoder;->write(Lcom/garmin/fit/MesgDefinition;)V

    .line 118
    return-void
.end method

.method public open(Ljava/io/File;)V
    .locals 3

    .prologue
    .line 50
    invoke-virtual {p1}, Ljava/io/File;->delete()Z

    .line 51
    iput-object p1, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    .line 53
    invoke-direct {p0}, Lcom/garmin/fit/FileEncoder;->writeFileHeader()V

    .line 55
    :try_start_0
    new-instance v0, Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;Z)V

    iput-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 59
    return-void

    .line 56
    :catch_0
    move-exception v0

    .line 57
    new-instance v1, Lcom/garmin/fit/FitRuntimeException;

    invoke-direct {v1, v0}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/io/IOException;)V

    throw v1
.end method

.method public write(Lcom/garmin/fit/Mesg;)V
    .locals 3

    .prologue
    .line 142
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    if-nez v0, :cond_0

    .line 143
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "File not open."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 145
    :cond_0
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_1

    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/garmin/fit/MesgDefinition;->supports(Lcom/garmin/fit/Mesg;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 146
    :cond_1
    new-instance v0, Lcom/garmin/fit/MesgDefinition;

    invoke-direct {v0, p1}, Lcom/garmin/fit/MesgDefinition;-><init>(Lcom/garmin/fit/Mesg;)V

    invoke-virtual {p0, v0}, Lcom/garmin/fit/FileEncoder;->write(Lcom/garmin/fit/MesgDefinition;)V

    .line 149
    :cond_2
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    iget-object v1, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v1, v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/garmin/fit/Mesg;->write(Ljava/io/OutputStream;Lcom/garmin/fit/MesgDefinition;)V

    .line 150
    return-void
.end method

.method public write(Lcom/garmin/fit/MesgDefinition;)V
    .locals 2

    .prologue
    .line 127
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->file:Ljava/io/File;

    if-nez v0, :cond_0

    .line 128
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    const-string v1, "File not open."

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 130
    :cond_0
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->out:Ljava/io/FileOutputStream;

    invoke-virtual {p1, v0}, Lcom/garmin/fit/MesgDefinition;->write(Ljava/io/OutputStream;)V

    .line 131
    iget-object v0, p0, Lcom/garmin/fit/FileEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/MesgDefinition;->localNum:I

    aput-object p1, v0, v1

    .line 132
    return-void
.end method

.method public write(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/Mesg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 159
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Mesg;

    .line 160
    invoke-virtual {p0, v0}, Lcom/garmin/fit/FileEncoder;->write(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 162
    :cond_0
    return-void
.end method
