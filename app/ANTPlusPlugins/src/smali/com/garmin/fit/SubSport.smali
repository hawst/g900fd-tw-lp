.class public final enum Lcom/garmin/fit/SubSport;
.super Ljava/lang/Enum;
.source "SubSport.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/SubSport;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/SubSport;

.field public static final enum ALL:Lcom/garmin/fit/SubSport;

.field public static final enum CARDIO_TRAINING:Lcom/garmin/fit/SubSport;

.field public static final enum CHALLENGE:Lcom/garmin/fit/SubSport;

.field public static final enum CYCLOCROSS:Lcom/garmin/fit/SubSport;

.field public static final enum DOWNHILL:Lcom/garmin/fit/SubSport;

.field public static final enum ELLIPTICAL:Lcom/garmin/fit/SubSport;

.field public static final enum EXERCISE:Lcom/garmin/fit/SubSport;

.field public static final enum FLEXIBILITY_TRAINING:Lcom/garmin/fit/SubSport;

.field public static final enum GENERIC:Lcom/garmin/fit/SubSport;

.field public static final enum HAND_CYCLING:Lcom/garmin/fit/SubSport;

.field public static final enum INDOOR_CYCLING:Lcom/garmin/fit/SubSport;

.field public static final enum INDOOR_ROWING:Lcom/garmin/fit/SubSport;

.field public static final enum INDOOR_SKIING:Lcom/garmin/fit/SubSport;

.field public static final enum INVALID:Lcom/garmin/fit/SubSport;

.field public static final enum LAP_SWIMMING:Lcom/garmin/fit/SubSport;

.field public static final enum MATCH:Lcom/garmin/fit/SubSport;

.field public static final enum MOUNTAIN:Lcom/garmin/fit/SubSport;

.field public static final enum OPEN_WATER:Lcom/garmin/fit/SubSport;

.field public static final enum RECUMBENT:Lcom/garmin/fit/SubSport;

.field public static final enum ROAD:Lcom/garmin/fit/SubSport;

.field public static final enum SPIN:Lcom/garmin/fit/SubSport;

.field public static final enum STAIR_CLIMBING:Lcom/garmin/fit/SubSport;

.field public static final enum STREET:Lcom/garmin/fit/SubSport;

.field public static final enum STRENGTH_TRAINING:Lcom/garmin/fit/SubSport;

.field public static final enum TRACK:Lcom/garmin/fit/SubSport;

.field public static final enum TRACK_CYCLING:Lcom/garmin/fit/SubSport;

.field public static final enum TRAIL:Lcom/garmin/fit/SubSport;

.field public static final enum TREADMILL:Lcom/garmin/fit/SubSport;

.field public static final enum WARM_UP:Lcom/garmin/fit/SubSport;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->GENERIC:Lcom/garmin/fit/SubSport;

    .line 22
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "TREADMILL"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->TREADMILL:Lcom/garmin/fit/SubSport;

    .line 23
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "STREET"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->STREET:Lcom/garmin/fit/SubSport;

    .line 24
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "TRAIL"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->TRAIL:Lcom/garmin/fit/SubSport;

    .line 25
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "TRACK"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->TRACK:Lcom/garmin/fit/SubSport;

    .line 26
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "SPIN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->SPIN:Lcom/garmin/fit/SubSport;

    .line 27
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "INDOOR_CYCLING"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->INDOOR_CYCLING:Lcom/garmin/fit/SubSport;

    .line 28
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "ROAD"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->ROAD:Lcom/garmin/fit/SubSport;

    .line 29
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "MOUNTAIN"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->MOUNTAIN:Lcom/garmin/fit/SubSport;

    .line 30
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "DOWNHILL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->DOWNHILL:Lcom/garmin/fit/SubSport;

    .line 31
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "RECUMBENT"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->RECUMBENT:Lcom/garmin/fit/SubSport;

    .line 32
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "CYCLOCROSS"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->CYCLOCROSS:Lcom/garmin/fit/SubSport;

    .line 33
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "HAND_CYCLING"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->HAND_CYCLING:Lcom/garmin/fit/SubSport;

    .line 34
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "TRACK_CYCLING"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->TRACK_CYCLING:Lcom/garmin/fit/SubSport;

    .line 35
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "INDOOR_ROWING"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->INDOOR_ROWING:Lcom/garmin/fit/SubSport;

    .line 36
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "ELLIPTICAL"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->ELLIPTICAL:Lcom/garmin/fit/SubSport;

    .line 37
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "STAIR_CLIMBING"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->STAIR_CLIMBING:Lcom/garmin/fit/SubSport;

    .line 38
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "LAP_SWIMMING"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->LAP_SWIMMING:Lcom/garmin/fit/SubSport;

    .line 39
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "OPEN_WATER"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->OPEN_WATER:Lcom/garmin/fit/SubSport;

    .line 40
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "FLEXIBILITY_TRAINING"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->FLEXIBILITY_TRAINING:Lcom/garmin/fit/SubSport;

    .line 41
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "STRENGTH_TRAINING"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->STRENGTH_TRAINING:Lcom/garmin/fit/SubSport;

    .line 42
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "WARM_UP"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->WARM_UP:Lcom/garmin/fit/SubSport;

    .line 43
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "MATCH"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->MATCH:Lcom/garmin/fit/SubSport;

    .line 44
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "EXERCISE"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->EXERCISE:Lcom/garmin/fit/SubSport;

    .line 45
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "CHALLENGE"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->CHALLENGE:Lcom/garmin/fit/SubSport;

    .line 46
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "INDOOR_SKIING"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->INDOOR_SKIING:Lcom/garmin/fit/SubSport;

    .line 47
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "CARDIO_TRAINING"

    const/16 v2, 0x1a

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->CARDIO_TRAINING:Lcom/garmin/fit/SubSport;

    .line 48
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "ALL"

    const/16 v2, 0x1b

    const/16 v3, 0xfe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->ALL:Lcom/garmin/fit/SubSport;

    .line 49
    new-instance v0, Lcom/garmin/fit/SubSport;

    const-string v1, "INVALID"

    const/16 v2, 0x1c

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/SubSport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SubSport;->INVALID:Lcom/garmin/fit/SubSport;

    .line 20
    const/16 v0, 0x1d

    new-array v0, v0, [Lcom/garmin/fit/SubSport;

    sget-object v1, Lcom/garmin/fit/SubSport;->GENERIC:Lcom/garmin/fit/SubSport;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/SubSport;->TREADMILL:Lcom/garmin/fit/SubSport;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/SubSport;->STREET:Lcom/garmin/fit/SubSport;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/SubSport;->TRAIL:Lcom/garmin/fit/SubSport;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/SubSport;->TRACK:Lcom/garmin/fit/SubSport;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/SubSport;->SPIN:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/SubSport;->INDOOR_CYCLING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/SubSport;->ROAD:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/SubSport;->MOUNTAIN:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/SubSport;->DOWNHILL:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/SubSport;->RECUMBENT:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/SubSport;->CYCLOCROSS:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/SubSport;->HAND_CYCLING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/SubSport;->TRACK_CYCLING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/SubSport;->INDOOR_ROWING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/SubSport;->ELLIPTICAL:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/SubSport;->STAIR_CLIMBING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/SubSport;->LAP_SWIMMING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/SubSport;->OPEN_WATER:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/SubSport;->FLEXIBILITY_TRAINING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/SubSport;->STRENGTH_TRAINING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/SubSport;->WARM_UP:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/garmin/fit/SubSport;->MATCH:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/garmin/fit/SubSport;->EXERCISE:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/garmin/fit/SubSport;->CHALLENGE:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/garmin/fit/SubSport;->INDOOR_SKIING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/garmin/fit/SubSport;->CARDIO_TRAINING:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/garmin/fit/SubSport;->ALL:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/garmin/fit/SubSport;->INVALID:Lcom/garmin/fit/SubSport;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/SubSport;->$VALUES:[Lcom/garmin/fit/SubSport;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 57
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 58
    iput-short p3, p0, Lcom/garmin/fit/SubSport;->value:S

    .line 59
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SubSport;
    .locals 6

    .prologue
    .line 62
    invoke-static {}, Lcom/garmin/fit/SubSport;->values()[Lcom/garmin/fit/SubSport;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 63
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/SubSport;->value:S

    if-ne v4, v5, :cond_0

    .line 67
    :goto_1
    return-object v0

    .line 62
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 67
    :cond_1
    sget-object v0, Lcom/garmin/fit/SubSport;->INVALID:Lcom/garmin/fit/SubSport;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/SubSport;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/SubSport;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubSport;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/SubSport;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/SubSport;->$VALUES:[Lcom/garmin/fit/SubSport;

    invoke-virtual {v0}, [Lcom/garmin/fit/SubSport;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/SubSport;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 71
    iget-short v0, p0, Lcom/garmin/fit/SubSport;->value:S

    return v0
.end method
