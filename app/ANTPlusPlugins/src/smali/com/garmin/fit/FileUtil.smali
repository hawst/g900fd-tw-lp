.class public Lcom/garmin/fit/FileUtil;
.super Ljava/lang/Object;
.source "FileUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/garmin/fit/FileUtil$1;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    return-void
.end method

.method public static prepend(Ljava/util/Collection;Lcom/garmin/fit/File;Lcom/garmin/fit/File;)Ljava/util/Collection;
    .locals 13
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Collection",
            "<[B>;",
            "Lcom/garmin/fit/File;",
            "Lcom/garmin/fit/File;",
            ")",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 78
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 79
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 80
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 83
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :cond_0
    :goto_0
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 84
    new-instance v8, Lcom/garmin/fit/Decode;

    invoke-direct {v8}, Lcom/garmin/fit/Decode;-><init>()V

    .line 85
    sget-object v1, Lcom/garmin/fit/Decode$RETURN;->CONTINUE:Lcom/garmin/fit/Decode$RETURN;

    .line 88
    new-instance v9, Ljava/io/ByteArrayInputStream;

    invoke-direct {v9, v0}, Ljava/io/ByteArrayInputStream;-><init>([B)V

    move v1, v3

    .line 90
    :cond_1
    :goto_1
    if-nez v1, :cond_0

    .line 91
    invoke-virtual {v9}, Ljava/io/ByteArrayInputStream;->read()I

    move-result v10

    .line 93
    if-gez v10, :cond_2

    .line 94
    sget-object v0, Ljava/lang/System;->out:Ljava/io/PrintStream;

    const-string v1, "end of stream"

    invoke-virtual {v0, v1}, Ljava/io/PrintStream;->println(Ljava/lang/String;)V

    goto :goto_0

    .line 98
    :cond_2
    int-to-byte v10, v10

    invoke-virtual {v8, v10}, Lcom/garmin/fit/Decode;->read(B)Lcom/garmin/fit/Decode$RETURN;

    move-result-object v10

    .line 100
    sget-object v11, Lcom/garmin/fit/FileUtil$1;->$SwitchMap$com$garmin$fit$Decode$RETURN:[I

    invoke-virtual {v10}, Lcom/garmin/fit/Decode$RETURN;->ordinal()I

    move-result v10

    aget v10, v11, v10

    packed-switch v10, :pswitch_data_0

    goto :goto_1

    .line 102
    :pswitch_0
    invoke-virtual {v8}, Lcom/garmin/fit/Decode;->getMesg()Lcom/garmin/fit/Mesg;

    move-result-object v10

    .line 104
    iget-object v11, v10, Lcom/garmin/fit/Mesg;->name:Ljava/lang/String;

    const-string v12, "file_id"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-eqz v11, :cond_1

    .line 105
    new-instance v1, Lcom/garmin/fit/FileIdMesg;

    invoke-direct {v1, v10}, Lcom/garmin/fit/FileIdMesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 107
    invoke-virtual {v1}, Lcom/garmin/fit/FileIdMesg;->getType()Lcom/garmin/fit/File;

    move-result-object v10

    invoke-virtual {v10, p1}, Lcom/garmin/fit/File;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 108
    invoke-interface {v4, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 110
    :cond_3
    invoke-virtual {v1}, Lcom/garmin/fit/FileIdMesg;->getType()Lcom/garmin/fit/File;

    move-result-object v1

    invoke-virtual {v1, p2}, Lcom/garmin/fit/File;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 111
    invoke-interface {v5, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    :cond_4
    move v1, v2

    .line 114
    goto :goto_1

    :pswitch_1
    move v1, v2

    .line 119
    goto :goto_1

    .line 128
    :cond_5
    invoke-interface {p0}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v7

    :goto_2
    invoke-interface {v7}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_9

    invoke-interface {v7}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [B

    .line 129
    invoke-interface {v5, v0}, Ljava/util/Collection;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_8

    .line 131
    array-length v1, v0

    .line 134
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v8

    move v2, v1

    :goto_3
    invoke-interface {v8}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_6

    invoke-interface {v8}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 135
    array-length v1, v1

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_3

    .line 138
    :cond_6
    new-array v8, v2, [B

    .line 140
    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v9

    move v2, v3

    :goto_4
    invoke-interface {v9}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_7

    invoke-interface {v9}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, [B

    .line 141
    array-length v10, v1

    invoke-static {v1, v3, v8, v2, v10}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 142
    array-length v1, v1

    add-int/2addr v1, v2

    move v2, v1

    goto :goto_4

    .line 145
    :cond_7
    array-length v1, v0

    invoke-static {v0, v3, v8, v2, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 147
    invoke-interface {v6, v8}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 149
    :cond_8
    invoke-interface {v6, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 153
    :cond_9
    return-object v6

    .line 100
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public static split(Ljava/io/InputStream;)Ljava/util/Collection;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            ")",
            "Ljava/util/Collection",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 32
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 35
    :try_start_0
    invoke-virtual {p0}, Ljava/io/InputStream;->available()I

    move-result v0

    new-array v2, v0, [B

    .line 36
    const/4 v0, 0x0

    .line 41
    invoke-virtual {p0, v2}, Ljava/io/InputStream;->read([B)I

    .line 43
    :goto_0
    array-length v3, v2

    if-ge v0, v3, :cond_0

    .line 44
    aget-byte v3, v2, v0

    .line 45
    add-int/lit8 v4, v0, 0x4

    aget-byte v4, v2, v4

    and-int/lit16 v4, v4, 0xff

    .line 46
    add-int/lit8 v5, v0, 0x5

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x8

    or-int/2addr v4, v5

    .line 47
    add-int/lit8 v5, v0, 0x6

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x10

    or-int/2addr v4, v5

    .line 48
    add-int/lit8 v5, v0, 0x7

    aget-byte v5, v2, v5

    and-int/lit16 v5, v5, 0xff

    shl-int/lit8 v5, v5, 0x18

    or-int/2addr v4, v5

    .line 50
    add-int/lit8 v5, v0, 0x8

    aget-byte v5, v2, v5

    const/16 v6, 0x2e

    if-eq v5, v6, :cond_2

    .line 68
    :cond_0
    array-length v3, v2

    if-ge v0, v3, :cond_1

    .line 69
    array-length v3, v2

    invoke-static {v2, v0, v3}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v0

    invoke-interface {v1, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 73
    :cond_1
    :goto_1
    return-object v1

    .line 52
    :cond_2
    add-int/lit8 v5, v0, 0x9

    aget-byte v5, v2, v5

    const/16 v6, 0x46

    if-ne v5, v6, :cond_0

    .line 54
    add-int/lit8 v5, v0, 0xa

    aget-byte v5, v2, v5

    const/16 v6, 0x49

    if-ne v5, v6, :cond_0

    .line 56
    add-int/lit8 v5, v0, 0xb

    aget-byte v5, v2, v5

    const/16 v6, 0x54

    if-ne v5, v6, :cond_0

    .line 59
    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x2

    .line 61
    add-int v4, v0, v3

    array-length v5, v2

    if-gt v4, v5, :cond_0

    .line 64
    add-int v4, v0, v3

    invoke-static {v2, v0, v4}, Ljava/util/Arrays;->copyOfRange([BII)[B

    move-result-object v4

    invoke-interface {v1, v4}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 65
    add-int/2addr v0, v3

    goto :goto_0

    .line 70
    :catch_0
    move-exception v0

    goto :goto_1
.end method
