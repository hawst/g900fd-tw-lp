.class public final enum Lcom/garmin/fit/CoursePoint;
.super Ljava/lang/Enum;
.source "CoursePoint.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/CoursePoint;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/CoursePoint;

.field public static final enum DANGER:Lcom/garmin/fit/CoursePoint;

.field public static final enum FIRST_AID:Lcom/garmin/fit/CoursePoint;

.field public static final enum FIRST_CATEGORY:Lcom/garmin/fit/CoursePoint;

.field public static final enum FOOD:Lcom/garmin/fit/CoursePoint;

.field public static final enum FOURTH_CATEGORY:Lcom/garmin/fit/CoursePoint;

.field public static final enum GENERIC:Lcom/garmin/fit/CoursePoint;

.field public static final enum HORS_CATEGORY:Lcom/garmin/fit/CoursePoint;

.field public static final enum INVALID:Lcom/garmin/fit/CoursePoint;

.field public static final enum LEFT:Lcom/garmin/fit/CoursePoint;

.field public static final enum LEFT_FORK:Lcom/garmin/fit/CoursePoint;

.field public static final enum MIDDLE_FORK:Lcom/garmin/fit/CoursePoint;

.field public static final enum RIGHT:Lcom/garmin/fit/CoursePoint;

.field public static final enum RIGHT_FORK:Lcom/garmin/fit/CoursePoint;

.field public static final enum SECOND_CATEGORY:Lcom/garmin/fit/CoursePoint;

.field public static final enum SHARP_LEFT:Lcom/garmin/fit/CoursePoint;

.field public static final enum SHARP_RIGHT:Lcom/garmin/fit/CoursePoint;

.field public static final enum SLIGHT_LEFT:Lcom/garmin/fit/CoursePoint;

.field public static final enum SLIGHT_RIGHT:Lcom/garmin/fit/CoursePoint;

.field public static final enum SPRINT:Lcom/garmin/fit/CoursePoint;

.field public static final enum STRAIGHT:Lcom/garmin/fit/CoursePoint;

.field public static final enum SUMMIT:Lcom/garmin/fit/CoursePoint;

.field public static final enum THIRD_CATEGORY:Lcom/garmin/fit/CoursePoint;

.field public static final enum U_TURN:Lcom/garmin/fit/CoursePoint;

.field public static final enum VALLEY:Lcom/garmin/fit/CoursePoint;

.field public static final enum WATER:Lcom/garmin/fit/CoursePoint;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->GENERIC:Lcom/garmin/fit/CoursePoint;

    .line 22
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SUMMIT"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SUMMIT:Lcom/garmin/fit/CoursePoint;

    .line 23
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "VALLEY"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->VALLEY:Lcom/garmin/fit/CoursePoint;

    .line 24
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "WATER"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->WATER:Lcom/garmin/fit/CoursePoint;

    .line 25
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "FOOD"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->FOOD:Lcom/garmin/fit/CoursePoint;

    .line 26
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "DANGER"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->DANGER:Lcom/garmin/fit/CoursePoint;

    .line 27
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "LEFT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->LEFT:Lcom/garmin/fit/CoursePoint;

    .line 28
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "RIGHT"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->RIGHT:Lcom/garmin/fit/CoursePoint;

    .line 29
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "STRAIGHT"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->STRAIGHT:Lcom/garmin/fit/CoursePoint;

    .line 30
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "FIRST_AID"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->FIRST_AID:Lcom/garmin/fit/CoursePoint;

    .line 31
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "FOURTH_CATEGORY"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->FOURTH_CATEGORY:Lcom/garmin/fit/CoursePoint;

    .line 32
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "THIRD_CATEGORY"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->THIRD_CATEGORY:Lcom/garmin/fit/CoursePoint;

    .line 33
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SECOND_CATEGORY"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SECOND_CATEGORY:Lcom/garmin/fit/CoursePoint;

    .line 34
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "FIRST_CATEGORY"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->FIRST_CATEGORY:Lcom/garmin/fit/CoursePoint;

    .line 35
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "HORS_CATEGORY"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->HORS_CATEGORY:Lcom/garmin/fit/CoursePoint;

    .line 36
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SPRINT"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SPRINT:Lcom/garmin/fit/CoursePoint;

    .line 37
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "LEFT_FORK"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->LEFT_FORK:Lcom/garmin/fit/CoursePoint;

    .line 38
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "RIGHT_FORK"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->RIGHT_FORK:Lcom/garmin/fit/CoursePoint;

    .line 39
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "MIDDLE_FORK"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->MIDDLE_FORK:Lcom/garmin/fit/CoursePoint;

    .line 40
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SLIGHT_LEFT"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SLIGHT_LEFT:Lcom/garmin/fit/CoursePoint;

    .line 41
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SHARP_LEFT"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SHARP_LEFT:Lcom/garmin/fit/CoursePoint;

    .line 42
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SLIGHT_RIGHT"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SLIGHT_RIGHT:Lcom/garmin/fit/CoursePoint;

    .line 43
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "SHARP_RIGHT"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->SHARP_RIGHT:Lcom/garmin/fit/CoursePoint;

    .line 44
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "U_TURN"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->U_TURN:Lcom/garmin/fit/CoursePoint;

    .line 45
    new-instance v0, Lcom/garmin/fit/CoursePoint;

    const-string v1, "INVALID"

    const/16 v2, 0x18

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/CoursePoint;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/CoursePoint;->INVALID:Lcom/garmin/fit/CoursePoint;

    .line 20
    const/16 v0, 0x19

    new-array v0, v0, [Lcom/garmin/fit/CoursePoint;

    sget-object v1, Lcom/garmin/fit/CoursePoint;->GENERIC:Lcom/garmin/fit/CoursePoint;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/CoursePoint;->SUMMIT:Lcom/garmin/fit/CoursePoint;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/CoursePoint;->VALLEY:Lcom/garmin/fit/CoursePoint;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/CoursePoint;->WATER:Lcom/garmin/fit/CoursePoint;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/CoursePoint;->FOOD:Lcom/garmin/fit/CoursePoint;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/CoursePoint;->DANGER:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/CoursePoint;->LEFT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/CoursePoint;->RIGHT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/CoursePoint;->STRAIGHT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/CoursePoint;->FIRST_AID:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/CoursePoint;->FOURTH_CATEGORY:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/CoursePoint;->THIRD_CATEGORY:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SECOND_CATEGORY:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/CoursePoint;->FIRST_CATEGORY:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/CoursePoint;->HORS_CATEGORY:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SPRINT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/CoursePoint;->LEFT_FORK:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/CoursePoint;->RIGHT_FORK:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/CoursePoint;->MIDDLE_FORK:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SLIGHT_LEFT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SHARP_LEFT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SLIGHT_RIGHT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/garmin/fit/CoursePoint;->SHARP_RIGHT:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/garmin/fit/CoursePoint;->U_TURN:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/garmin/fit/CoursePoint;->INVALID:Lcom/garmin/fit/CoursePoint;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/CoursePoint;->$VALUES:[Lcom/garmin/fit/CoursePoint;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 54
    iput-short p3, p0, Lcom/garmin/fit/CoursePoint;->value:S

    .line 55
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/CoursePoint;
    .locals 6

    .prologue
    .line 58
    invoke-static {}, Lcom/garmin/fit/CoursePoint;->values()[Lcom/garmin/fit/CoursePoint;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 59
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/CoursePoint;->value:S

    if-ne v4, v5, :cond_0

    .line 63
    :goto_1
    return-object v0

    .line 58
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 63
    :cond_1
    sget-object v0, Lcom/garmin/fit/CoursePoint;->INVALID:Lcom/garmin/fit/CoursePoint;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/CoursePoint;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/CoursePoint;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/CoursePoint;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/CoursePoint;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/CoursePoint;->$VALUES:[Lcom/garmin/fit/CoursePoint;

    invoke-virtual {v0}, [Lcom/garmin/fit/CoursePoint;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/CoursePoint;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 67
    iget-short v0, p0, Lcom/garmin/fit/CoursePoint;->value:S

    return v0
.end method
