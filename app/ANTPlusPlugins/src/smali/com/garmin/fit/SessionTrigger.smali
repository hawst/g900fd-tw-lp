.class public final enum Lcom/garmin/fit/SessionTrigger;
.super Ljava/lang/Enum;
.source "SessionTrigger.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/SessionTrigger;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/SessionTrigger;

.field public static final enum ACTIVITY_END:Lcom/garmin/fit/SessionTrigger;

.field public static final enum AUTO_MULTI_SPORT:Lcom/garmin/fit/SessionTrigger;

.field public static final enum FITNESS_EQUIPMENT:Lcom/garmin/fit/SessionTrigger;

.field public static final enum INVALID:Lcom/garmin/fit/SessionTrigger;

.field public static final enum MANUAL:Lcom/garmin/fit/SessionTrigger;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/SessionTrigger;

    const-string v1, "ACTIVITY_END"

    invoke-direct {v0, v1, v3, v3}, Lcom/garmin/fit/SessionTrigger;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->ACTIVITY_END:Lcom/garmin/fit/SessionTrigger;

    .line 22
    new-instance v0, Lcom/garmin/fit/SessionTrigger;

    const-string v1, "MANUAL"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/SessionTrigger;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->MANUAL:Lcom/garmin/fit/SessionTrigger;

    .line 23
    new-instance v0, Lcom/garmin/fit/SessionTrigger;

    const-string v1, "AUTO_MULTI_SPORT"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/SessionTrigger;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->AUTO_MULTI_SPORT:Lcom/garmin/fit/SessionTrigger;

    .line 24
    new-instance v0, Lcom/garmin/fit/SessionTrigger;

    const-string v1, "FITNESS_EQUIPMENT"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/SessionTrigger;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->FITNESS_EQUIPMENT:Lcom/garmin/fit/SessionTrigger;

    .line 25
    new-instance v0, Lcom/garmin/fit/SessionTrigger;

    const-string v1, "INVALID"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v7, v2}, Lcom/garmin/fit/SessionTrigger;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->INVALID:Lcom/garmin/fit/SessionTrigger;

    .line 20
    const/4 v0, 0x5

    new-array v0, v0, [Lcom/garmin/fit/SessionTrigger;

    sget-object v1, Lcom/garmin/fit/SessionTrigger;->ACTIVITY_END:Lcom/garmin/fit/SessionTrigger;

    aput-object v1, v0, v3

    sget-object v1, Lcom/garmin/fit/SessionTrigger;->MANUAL:Lcom/garmin/fit/SessionTrigger;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/SessionTrigger;->AUTO_MULTI_SPORT:Lcom/garmin/fit/SessionTrigger;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/SessionTrigger;->FITNESS_EQUIPMENT:Lcom/garmin/fit/SessionTrigger;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/SessionTrigger;->INVALID:Lcom/garmin/fit/SessionTrigger;

    aput-object v1, v0, v7

    sput-object v0, Lcom/garmin/fit/SessionTrigger;->$VALUES:[Lcom/garmin/fit/SessionTrigger;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 33
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 34
    iput-short p3, p0, Lcom/garmin/fit/SessionTrigger;->value:S

    .line 35
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SessionTrigger;
    .locals 6

    .prologue
    .line 38
    invoke-static {}, Lcom/garmin/fit/SessionTrigger;->values()[Lcom/garmin/fit/SessionTrigger;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 39
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/SessionTrigger;->value:S

    if-ne v4, v5, :cond_0

    .line 43
    :goto_1
    return-object v0

    .line 38
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 43
    :cond_1
    sget-object v0, Lcom/garmin/fit/SessionTrigger;->INVALID:Lcom/garmin/fit/SessionTrigger;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/SessionTrigger;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/SessionTrigger;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SessionTrigger;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/SessionTrigger;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/SessionTrigger;->$VALUES:[Lcom/garmin/fit/SessionTrigger;

    invoke-virtual {v0}, [Lcom/garmin/fit/SessionTrigger;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/SessionTrigger;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 47
    iget-short v0, p0, Lcom/garmin/fit/SessionTrigger;->value:S

    return v0
.end method
