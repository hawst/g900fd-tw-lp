.class public Lcom/garmin/fit/BufferEncoder;
.super Ljava/lang/Object;
.source "BufferEncoder.java"

# interfaces
.implements Lcom/garmin/fit/MesgDefinitionListener;
.implements Lcom/garmin/fit/MesgListener;


# instance fields
.field private byteOutStream:Ljava/io/ByteArrayOutputStream;

.field private dataOutStream:Ljava/io/DataOutputStream;

.field private lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/garmin/fit/MesgDefinition;

    iput-object v0, p0, Lcom/garmin/fit/BufferEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    .line 33
    new-instance v0, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0}, Ljava/io/ByteArrayOutputStream;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    .line 34
    new-instance v0, Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-direct {v0, v1}, Ljava/io/DataOutputStream;-><init>(Ljava/io/OutputStream;)V

    iput-object v0, p0, Lcom/garmin/fit/BufferEncoder;->dataOutStream:Ljava/io/DataOutputStream;

    .line 35
    invoke-virtual {p0}, Lcom/garmin/fit/BufferEncoder;->open()V

    .line 36
    return-void
.end method

.method private writeFileHeader()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 50
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0xe

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 51
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x10

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 52
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x4c

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 53
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 54
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 55
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 56
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 57
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 58
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x2e

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 59
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x46

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 60
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x49

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 61
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    const/16 v1, 0x54

    invoke-virtual {v0, v1}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 62
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 63
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0, v2}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 64
    return-void
.end method


# virtual methods
.method public close()[B
    .locals 10

    .prologue
    const/16 v9, 0xc

    const-wide/16 v7, 0xff

    const/4 v0, 0x0

    .line 127
    iget-object v1, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 128
    iget-object v1, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1, v0}, Ljava/io/ByteArrayOutputStream;->write(I)V

    .line 131
    iget-object v1, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v1}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v3

    .line 132
    array-length v1, v3

    add-int/lit8 v1, v1, -0xe

    add-int/lit8 v1, v1, -0x2

    int-to-long v1, v1

    .line 135
    const/4 v4, 0x4

    and-long v5, v1, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 136
    const/4 v4, 0x5

    const/16 v5, 0x8

    shr-long v5, v1, v5

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 137
    const/4 v4, 0x6

    const/16 v5, 0x10

    shr-long v5, v1, v5

    and-long/2addr v5, v7

    long-to-int v5, v5

    int-to-byte v5, v5

    aput-byte v5, v3, v4

    .line 138
    const/4 v4, 0x7

    const/16 v5, 0x18

    shr-long/2addr v1, v5

    and-long/2addr v1, v7

    long-to-int v1, v1

    int-to-byte v1, v1

    aput-byte v1, v3, v4

    move v1, v0

    move v2, v0

    .line 142
    :goto_0
    if-ge v1, v9, :cond_0

    .line 143
    aget-byte v4, v3, v1

    invoke-static {v2, v4}, Lcom/garmin/fit/CRC;->get16(IB)I

    move-result v2

    .line 142
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 147
    :cond_0
    and-int/lit16 v1, v2, 0xff

    int-to-byte v1, v1

    aput-byte v1, v3, v9

    .line 148
    const/16 v1, 0xd

    shr-int/lit8 v2, v2, 0x8

    and-int/lit16 v2, v2, 0xff

    int-to-byte v2, v2

    aput-byte v2, v3, v1

    move v1, v0

    .line 152
    :goto_1
    array-length v2, v3

    add-int/lit8 v2, v2, -0x2

    if-ge v0, v2, :cond_1

    .line 153
    aget-byte v2, v3, v0

    invoke-static {v1, v2}, Lcom/garmin/fit/CRC;->get16(IB)I

    move-result v1

    .line 152
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 157
    :cond_1
    array-length v0, v3

    add-int/lit8 v0, v0, -0x2

    and-int/lit16 v2, v1, 0xff

    int-to-byte v2, v2

    aput-byte v2, v3, v0

    .line 158
    array-length v0, v3

    add-int/lit8 v0, v0, -0x1

    shr-int/lit8 v1, v1, 0x8

    and-int/lit16 v1, v1, 0xff

    int-to-byte v1, v1

    aput-byte v1, v3, v0

    .line 161
    invoke-virtual {p0}, Lcom/garmin/fit/BufferEncoder;->open()V

    .line 163
    return-object v3
.end method

.method public onMesg(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 70
    invoke-virtual {p0, p1}, Lcom/garmin/fit/BufferEncoder;->write(Lcom/garmin/fit/Mesg;)V

    .line 71
    return-void
.end method

.method public onMesgDefinition(Lcom/garmin/fit/MesgDefinition;)V
    .locals 0

    .prologue
    .line 77
    invoke-virtual {p0, p1}, Lcom/garmin/fit/BufferEncoder;->write(Lcom/garmin/fit/MesgDefinition;)V

    .line 78
    return-void
.end method

.method public open()V
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->byteOutStream:Ljava/io/ByteArrayOutputStream;

    invoke-virtual {v0}, Ljava/io/ByteArrayOutputStream;->reset()V

    .line 43
    invoke-direct {p0}, Lcom/garmin/fit/BufferEncoder;->writeFileHeader()V

    .line 44
    return-void
.end method

.method public write(Lcom/garmin/fit/Mesg;)V
    .locals 3

    .prologue
    .line 99
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v0, v0, v1

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v0, v0, v1

    invoke-virtual {v0, p1}, Lcom/garmin/fit/MesgDefinition;->supports(Lcom/garmin/fit/Mesg;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 100
    :cond_0
    new-instance v0, Lcom/garmin/fit/MesgDefinition;

    invoke-direct {v0, p1}, Lcom/garmin/fit/MesgDefinition;-><init>(Lcom/garmin/fit/Mesg;)V

    invoke-virtual {p0, v0}, Lcom/garmin/fit/BufferEncoder;->write(Lcom/garmin/fit/MesgDefinition;)V

    .line 103
    :cond_1
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->dataOutStream:Ljava/io/DataOutputStream;

    iget-object v1, p0, Lcom/garmin/fit/BufferEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v2, p1, Lcom/garmin/fit/Mesg;->localNum:I

    aget-object v1, v1, v2

    invoke-virtual {p1, v0, v1}, Lcom/garmin/fit/Mesg;->write(Ljava/io/OutputStream;Lcom/garmin/fit/MesgDefinition;)V

    .line 104
    return-void
.end method

.method public write(Lcom/garmin/fit/MesgDefinition;)V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->dataOutStream:Ljava/io/DataOutputStream;

    invoke-virtual {p1, v0}, Lcom/garmin/fit/MesgDefinition;->write(Ljava/io/OutputStream;)V

    .line 88
    iget-object v0, p0, Lcom/garmin/fit/BufferEncoder;->lastMesgDefinition:[Lcom/garmin/fit/MesgDefinition;

    iget v1, p1, Lcom/garmin/fit/MesgDefinition;->localNum:I

    aput-object p1, v0, v1

    .line 89
    return-void
.end method

.method public write(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/garmin/fit/Mesg;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 113
    invoke-interface {p1}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Mesg;

    .line 114
    invoke-virtual {p0, v0}, Lcom/garmin/fit/BufferEncoder;->write(Lcom/garmin/fit/Mesg;)V

    goto :goto_0

    .line 116
    :cond_0
    return-void
.end method
