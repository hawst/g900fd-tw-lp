.class public Lcom/garmin/fit/LapMesg;
.super Lcom/garmin/fit/Mesg;
.source "LapMesg.java"

# interfaces
.implements Lcom/garmin/fit/MesgWithEvent;


# static fields
.field protected static final lapMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    .line 28
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "lap"

    const/16 v2, 0x13

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    .line 29
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "timestamp"

    const/16 v2, 0xfd

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event"

    const/4 v2, 0x0

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event_type"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "start_time"

    const/4 v2, 0x2

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "start_position_lat"

    const/4 v2, 0x3

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "start_position_long"

    const/4 v2, 0x4

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "end_position_lat"

    const/4 v2, 0x5

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "end_position_long"

    const/4 v2, 0x6

    const/16 v3, 0x85

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "semicircles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_elapsed_time"

    const/4 v2, 0x7

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_timer_time"

    const/16 v2, 0x8

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 51
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_distance"

    const/16 v2, 0x9

    const/16 v3, 0x86

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 52
    const/16 v10, 0xc

    .line 53
    sget-object v11, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_cycles"

    const/16 v2, 0xa

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "cycles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 54
    const/4 v8, 0x0

    .line 55
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string v1, "total_strides"

    const/16 v2, 0x86

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string v7, "strides"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 56
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/16 v1, 0x19

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 59
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_calories"

    const/16 v2, 0xb

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "kcal"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 61
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_fat_calories"

    const/16 v2, 0xc

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "kcal"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 63
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_speed"

    const/16 v2, 0xd

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 65
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_speed"

    const/16 v2, 0xe

    const/16 v3, 0x84

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 67
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_heart_rate"

    const/16 v2, 0xf

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 69
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_heart_rate"

    const/16 v2, 0x10

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 70
    const/16 v10, 0x13

    .line 71
    sget-object v11, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_cadence"

    const/16 v2, 0x11

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 72
    const/4 v8, 0x0

    .line 73
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string v1, "avg_running_cadence"

    const/4 v2, 0x2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string v7, "strides/min"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 74
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/16 v1, 0x19

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 76
    const/16 v10, 0x14

    .line 77
    sget-object v11, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_cadence"

    const/16 v2, 0x12

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v11, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 78
    const/4 v8, 0x0

    .line 79
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v9, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    new-instance v0, Lcom/garmin/fit/SubField;

    const-string v1, "max_running_cadence"

    const/4 v2, 0x2

    const-wide/high16 v3, 0x3ff0000000000000L    # 1.0

    const-wide/16 v5, 0x0

    const-string v7, "strides/min"

    invoke-direct/range {v0 .. v7}, Lcom/garmin/fit/SubField;-><init>(Ljava/lang/String;IDDLjava/lang/String;)V

    invoke-virtual {v9, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 80
    sget-object v0, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    iget-object v0, v0, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    iget-object v0, v0, Lcom/garmin/fit/Field;->subFields:Ljava/util/ArrayList;

    invoke-virtual {v0, v8}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/SubField;

    const/16 v1, 0x19

    const-wide/16 v2, 0x1

    invoke-virtual {v0, v1, v2, v3}, Lcom/garmin/fit/SubField;->addMap(IJ)V

    .line 83
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_power"

    const/16 v2, 0x13

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 85
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_power"

    const/16 v2, 0x14

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 87
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_ascent"

    const/16 v2, 0x15

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 89
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_descent"

    const/16 v2, 0x16

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 91
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "intensity"

    const/16 v2, 0x17

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 93
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "lap_trigger"

    const/16 v2, 0x18

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 95
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sport"

    const/16 v2, 0x19

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 97
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "event_group"

    const/16 v2, 0x1a

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 99
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "num_lengths"

    const/16 v2, 0x20

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "lengths"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 101
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "normalized_power"

    const/16 v2, 0x21

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "watts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 103
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "left_right_balance"

    const/16 v2, 0x22

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 105
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "first_length_index"

    const/16 v2, 0x23

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 107
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stroke_distance"

    const/16 v2, 0x25

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 109
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "swim_stroke"

    const/16 v2, 0x26

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 111
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "sub_sport"

    const/16 v2, 0x27

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 113
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "num_active_lengths"

    const/16 v2, 0x28

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "lengths"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 115
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_work"

    const/16 v2, 0x29

    const/16 v3, 0x86

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "J"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 117
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_altitude"

    const/16 v2, 0x2a

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 119
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_altitude"

    const/16 v2, 0x2b

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 121
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "gps_accuracy"

    const/16 v2, 0x2c

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 123
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_grade"

    const/16 v2, 0x2d

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 125
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_pos_grade"

    const/16 v2, 0x2e

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 127
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_neg_grade"

    const/16 v2, 0x2f

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 129
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_pos_grade"

    const/16 v2, 0x30

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 131
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_neg_grade"

    const/16 v2, 0x31

    const/16 v3, 0x83

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 133
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_temperature"

    const/16 v2, 0x32

    const/4 v3, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "C"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 135
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_temperature"

    const/16 v2, 0x33

    const/4 v3, 0x1

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "C"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 137
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_moving_time"

    const/16 v2, 0x34

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 139
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_pos_vertical_speed"

    const/16 v2, 0x35

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 141
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_neg_vertical_speed"

    const/16 v2, 0x36

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 143
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_pos_vertical_speed"

    const/16 v2, 0x37

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 145
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_neg_vertical_speed"

    const/16 v2, 0x38

    const/16 v3, 0x83

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "m/s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 147
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "time_in_hr_zone"

    const/16 v2, 0x39

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 149
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "time_in_speed_zone"

    const/16 v2, 0x3a

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 151
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "time_in_cadence_zone"

    const/16 v2, 0x3b

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 153
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "time_in_power_zone"

    const/16 v2, 0x3c

    const/16 v3, 0x86

    const-wide v4, 0x408f400000000000L    # 1000.0

    const-wide/16 v6, 0x0

    const-string v8, "s"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 155
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "repetition_num"

    const/16 v2, 0x3d

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 157
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "min_altitude"

    const/16 v2, 0x3e

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4014000000000000L    # 5.0

    const-wide v6, 0x407f400000000000L    # 500.0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 159
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "min_heart_rate"

    const/16 v2, 0x3f

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 161
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "wkt_step_index"

    const/16 v2, 0x47

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 163
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "opponent_score"

    const/16 v2, 0x4a

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 165
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "stroke_count"

    const/16 v2, 0x4b

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "counts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 167
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "zone_count"

    const/16 v2, 0x4c

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "counts"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 169
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_vertical_oscillation"

    const/16 v2, 0x4d

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "mm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 171
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stance_time_percent"

    const/16 v2, 0x4e

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 173
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_stance_time"

    const/16 v2, 0x4f

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "ms"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 175
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_fractional_cadence"

    const/16 v2, 0x50

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 177
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_fractional_cadence"

    const/16 v2, 0x51

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string v8, "rpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 179
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "total_fractional_cycles"

    const/16 v2, 0x52

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4060000000000000L    # 128.0

    const-wide/16 v6, 0x0

    const-string v8, "cycles"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 181
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "player_score"

    const/16 v2, 0x53

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 183
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_total_hemoglobin_conc"

    const/16 v2, 0x54

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 185
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "min_total_hemoglobin_conc"

    const/16 v2, 0x55

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 187
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_total_hemoglobin_conc"

    const/16 v2, 0x56

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "g/dL"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 189
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_saturated_hemoglobin_percent"

    const/16 v2, 0x57

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 191
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "min_saturated_hemoglobin_percent"

    const/16 v2, 0x58

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 193
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_saturated_hemoglobin_percent"

    const/16 v2, 0x59

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "%"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 195
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_left_torque_effectiveness"

    const/16 v2, 0x5b

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 197
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_right_torque_effectiveness"

    const/16 v2, 0x5c

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 199
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_left_pedal_smoothness"

    const/16 v2, 0x5d

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 201
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_right_pedal_smoothness"

    const/16 v2, 0x5e

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 203
    sget-object v10, Lcom/garmin/fit/LapMesg;->lapMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "avg_combined_pedal_smoothness"

    const/16 v2, 0x5f

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4000000000000000L    # 2.0

    const-wide/16 v6, 0x0

    const-string v8, "percent"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 205
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 208
    const/16 v0, 0x13

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 209
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 212
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 213
    return-void
.end method


# virtual methods
.method public getAvgAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1056
    const/16 v0, 0x2a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgCadence()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 630
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgCombinedPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2038
    const/16 v0, 0x5f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgFractionalCadence()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1689
    const/16 v0, 0x50

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1116
    const/16 v0, 0x2d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 589
    const/16 v0, 0xf

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgLeftPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1998
    const/16 v0, 0x5d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgLeftTorqueEffectiveness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1958
    const/16 v0, 0x5b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgNegGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1156
    const/16 v0, 0x2f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgNegVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1296
    const/16 v0, 0x36

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPosGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1136
    const/16 v0, 0x2e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPosVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1276
    const/16 v0, 0x35

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 712
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRightPedalSmoothness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 2018
    const/16 v0, 0x5e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRightTorqueEffectiveness()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1978
    const/16 v0, 0x5c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgRunningCadence()Ljava/lang/Short;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 651
    const/16 v0, 0x11

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getAvgSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1874
    const/16 v0, 0x57

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 549
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStanceTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1668
    const/16 v0, 0x4f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStanceTimePercent()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1648
    const/16 v0, 0x4e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgStrokeDistance()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 952
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgTemperature()Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 1216
    const/16 v0, 0x32

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldByteValue(III)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getAvgTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1781
    const/16 v0, 0x54

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getAvgVerticalOscillation()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1628
    const/16 v0, 0x4d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getEndPositionLat()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 363
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getEndPositionLong()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 383
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getEvent()Lcom/garmin/fit/Event;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 262
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 263
    if-nez v0, :cond_0

    .line 264
    const/4 v0, 0x0

    .line 265
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Event;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Event;

    move-result-object v0

    goto :goto_0
.end method

.method public getEventGroup()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 855
    const/16 v0, 0x1a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getEventType()Lcom/garmin/fit/EventType;
    .locals 3

    .prologue
    .line 283
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 284
    if-nez v0, :cond_0

    .line 285
    const/4 v0, 0x0

    .line 286
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/EventType;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/EventType;

    move-result-object v0

    goto :goto_0
.end method

.method public getFirstLengthIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 933
    const/16 v0, 0x23

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getGpsAccuracy()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 1096
    const/16 v0, 0x2c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getIntensity()Lcom/garmin/fit/Intensity;
    .locals 3

    .prologue
    .line 792
    const/16 v0, 0x17

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 793
    if-nez v0, :cond_0

    .line 794
    const/4 v0, 0x0

    .line 795
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Intensity;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Intensity;

    move-result-object v0

    goto :goto_0
.end method

.method public getLapTrigger()Lcom/garmin/fit/LapTrigger;
    .locals 3

    .prologue
    .line 813
    const/16 v0, 0x18

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 814
    if-nez v0, :cond_0

    .line 815
    const/4 v0, 0x0

    .line 816
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/LapTrigger;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/LapTrigger;

    move-result-object v0

    goto :goto_0
.end method

.method public getLeftRightBalance()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 915
    const/16 v0, 0x22

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMaxAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1076
    const/16 v0, 0x2b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxCadence()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 671
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxFractionalCadence()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1711
    const/16 v0, 0x51

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 609
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxNegGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1196
    const/16 v0, 0x31

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxNegVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1336
    const/16 v0, 0x38

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPosGrade()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1176
    const/16 v0, 0x30

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPosVerticalSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1316
    const/16 v0, 0x37

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 733
    const/16 v0, 0x14

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMaxRunningCadence()Ljava/lang/Short;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 691
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1936
    const/16 v0, 0x59

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxSpeed()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 569
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMaxTemperature()Ljava/lang/Byte;
    .locals 3

    .prologue
    .line 1236
    const/16 v0, 0x33

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldByteValue(III)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getMaxTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1843
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 222
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMinAltitude()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1490
    const/16 v0, 0x3e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMinHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 1510
    const/16 v0, 0x3f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getMinSaturatedHemoglobinPercent(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1905
    const/16 v0, 0x58

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getMinTotalHemoglobinConc(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1812
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getNormalizedPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 896
    const/16 v0, 0x21

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumActiveLengths()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1015
    const/16 v0, 0x28

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumAvgSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 1862
    const/16 v0, 0x57

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumAvgTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 1769
    const/16 v0, 0x54

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumLengths()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 875
    const/16 v0, 0x20

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumMaxSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 1924
    const/16 v0, 0x59

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMaxTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 1831
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMinSaturatedHemoglobinPercent()I
    .locals 2

    .prologue
    .line 1893
    const/16 v0, 0x58

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumMinTotalHemoglobinConc()I
    .locals 2

    .prologue
    .line 1800
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumStrokeCount()I
    .locals 2

    .prologue
    .line 1563
    const/16 v0, 0x4b

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInCadenceZone()I
    .locals 2

    .prologue
    .line 1411
    const/16 v0, 0x3b

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInHrZone()I
    .locals 2

    .prologue
    .line 1353
    const/16 v0, 0x39

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInPowerZone()I
    .locals 2

    .prologue
    .line 1440
    const/16 v0, 0x3c

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumTimeInSpeedZone()I
    .locals 2

    .prologue
    .line 1382
    const/16 v0, 0x3a

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getNumZoneCount()I
    .locals 2

    .prologue
    .line 1594
    const/16 v0, 0x4c

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/LapMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getOpponentScore()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1547
    const/16 v0, 0x4a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getPlayerScore()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1753
    const/16 v0, 0x53

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getRepetitionNum()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1471
    const/16 v0, 0x3d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSport()Lcom/garmin/fit/Sport;
    .locals 3

    .prologue
    .line 834
    const/16 v0, 0x19

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 835
    if-nez v0, :cond_0

    .line 836
    const/4 v0, 0x0

    .line 837
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Sport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;

    move-result-object v0

    goto :goto_0
.end method

.method public getStartPositionLat()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 323
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getStartPositionLong()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 343
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getStartTime()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 304
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/LapMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getStrokeCount(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1575
    const/16 v0, 0x4b

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getSubSport()Lcom/garmin/fit/SubSport;
    .locals 3

    .prologue
    .line 992
    const/16 v0, 0x27

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 993
    if-nez v0, :cond_0

    .line 994
    const/4 v0, 0x0

    .line 995
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SubSport;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SubSport;

    move-result-object v0

    goto :goto_0
.end method

.method public getSwimStroke()Lcom/garmin/fit/SwimStroke;
    .locals 3

    .prologue
    .line 971
    const/16 v0, 0x26

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 972
    if-nez v0, :cond_0

    .line 973
    const/4 v0, 0x0

    .line 974
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/SwimStroke;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/SwimStroke;

    move-result-object v0

    goto :goto_0
.end method

.method public getTimeInCadenceZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1422
    const/16 v0, 0x3b

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInHrZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1364
    const/16 v0, 0x39

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInPowerZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1451
    const/16 v0, 0x3c

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimeInSpeedZone(I)Ljava/lang/Float;
    .locals 2

    .prologue
    .line 1393
    const/16 v0, 0x3a

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTimestamp()Lcom/garmin/fit/DateTime;
    .locals 3

    .prologue
    .line 242
    const/16 v0, 0xfd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/garmin/fit/LapMesg;->timestampToDateTime(Ljava/lang/Long;)Lcom/garmin/fit/DateTime;

    move-result-object v0

    return-object v0
.end method

.method public getTotalAscent()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 753
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalCalories()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 507
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalCycles()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 467
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTotalDescent()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 773
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalDistance()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 447
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalElapsedTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 404
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFatCalories()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 528
    const/16 v0, 0xc

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getTotalFractionalCycles()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1733
    const/16 v0, 0x52

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalMovingTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 1256
    const/16 v0, 0x34

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalStrides()Ljava/lang/Long;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 487
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v1, v1}, Lcom/garmin/fit/LapMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getTotalTimerTime()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 426
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getTotalWork()Ljava/lang/Long;
    .locals 3

    .prologue
    .line 1036
    const/16 v0, 0x29

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldLongValue(III)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public getWktStepIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 1529
    const/16 v0, 0x47

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getZoneCount(I)Ljava/lang/Integer;
    .locals 2

    .prologue
    .line 1606
    const/16 v0, 0x4c

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/LapMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public setAvgAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1066
    const/16 v0, 0x2a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1067
    return-void
.end method

.method public setAvgCadence(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 641
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 642
    return-void
.end method

.method public setAvgCombinedPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2048
    const/16 v0, 0x5f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2049
    return-void
.end method

.method public setAvgFractionalCadence(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1700
    const/16 v0, 0x50

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1701
    return-void
.end method

.method public setAvgGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1126
    const/16 v0, 0x2d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1127
    return-void
.end method

.method public setAvgHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 599
    const/16 v0, 0xf

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 600
    return-void
.end method

.method public setAvgLeftPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2008
    const/16 v0, 0x5d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2009
    return-void
.end method

.method public setAvgLeftTorqueEffectiveness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1968
    const/16 v0, 0x5b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1969
    return-void
.end method

.method public setAvgNegGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1166
    const/16 v0, 0x2f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1167
    return-void
.end method

.method public setAvgNegVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1306
    const/16 v0, 0x36

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1307
    return-void
.end method

.method public setAvgPosGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1146
    const/16 v0, 0x2e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1147
    return-void
.end method

.method public setAvgPosVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1286
    const/16 v0, 0x35

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1287
    return-void
.end method

.method public setAvgPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 723
    const/16 v0, 0x13

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 724
    return-void
.end method

.method public setAvgRightPedalSmoothness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 2028
    const/16 v0, 0x5e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 2029
    return-void
.end method

.method public setAvgRightTorqueEffectiveness(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1988
    const/16 v0, 0x5c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1989
    return-void
.end method

.method public setAvgRunningCadence(Ljava/lang/Short;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 661
    const/16 v0, 0x11

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 662
    return-void
.end method

.method public setAvgSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1886
    const/16 v0, 0x57

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1887
    return-void
.end method

.method public setAvgSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 559
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 560
    return-void
.end method

.method public setAvgStanceTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1678
    const/16 v0, 0x4f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1679
    return-void
.end method

.method public setAvgStanceTimePercent(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1658
    const/16 v0, 0x4e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1659
    return-void
.end method

.method public setAvgStrokeDistance(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 962
    const/16 v0, 0x25

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 963
    return-void
.end method

.method public setAvgTemperature(Ljava/lang/Byte;)V
    .locals 3

    .prologue
    .line 1226
    const/16 v0, 0x32

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1227
    return-void
.end method

.method public setAvgTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1793
    const/16 v0, 0x54

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1794
    return-void
.end method

.method public setAvgVerticalOscillation(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1638
    const/16 v0, 0x4d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1639
    return-void
.end method

.method public setEndPositionLat(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 373
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 374
    return-void
.end method

.method public setEndPositionLong(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 393
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 394
    return-void
.end method

.method public setEvent(Lcom/garmin/fit/Event;)V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 274
    iget-short v0, p1, Lcom/garmin/fit/Event;->value:S

    invoke-static {v0}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v0

    const v1, 0xffff

    invoke-virtual {p0, v2, v2, v0, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 275
    return-void
.end method

.method public setEventGroup(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 864
    const/16 v0, 0x1a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 865
    return-void
.end method

.method public setEventType(Lcom/garmin/fit/EventType;)V
    .locals 4

    .prologue
    .line 295
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/EventType;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 296
    return-void
.end method

.method public setFirstLengthIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 942
    const/16 v0, 0x23

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 943
    return-void
.end method

.method public setGpsAccuracy(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 1106
    const/16 v0, 0x2c

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1107
    return-void
.end method

.method public setIntensity(Lcom/garmin/fit/Intensity;)V
    .locals 4

    .prologue
    .line 804
    const/16 v0, 0x17

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Intensity;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 805
    return-void
.end method

.method public setLapTrigger(Lcom/garmin/fit/LapTrigger;)V
    .locals 4

    .prologue
    .line 825
    const/16 v0, 0x18

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/LapTrigger;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 826
    return-void
.end method

.method public setLeftRightBalance(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 924
    const/16 v0, 0x22

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 925
    return-void
.end method

.method public setMaxAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1086
    const/16 v0, 0x2b

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1087
    return-void
.end method

.method public setMaxCadence(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 681
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 682
    return-void
.end method

.method public setMaxFractionalCadence(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1722
    const/16 v0, 0x51

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1723
    return-void
.end method

.method public setMaxHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 619
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 620
    return-void
.end method

.method public setMaxNegGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1206
    const/16 v0, 0x31

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1207
    return-void
.end method

.method public setMaxNegVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1346
    const/16 v0, 0x38

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1347
    return-void
.end method

.method public setMaxPosGrade(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1186
    const/16 v0, 0x30

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1187
    return-void
.end method

.method public setMaxPosVerticalSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1326
    const/16 v0, 0x37

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1327
    return-void
.end method

.method public setMaxPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 743
    const/16 v0, 0x14

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 744
    return-void
.end method

.method public setMaxRunningCadence(Ljava/lang/Short;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 701
    const/16 v0, 0x12

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 702
    return-void
.end method

.method public setMaxSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1948
    const/16 v0, 0x59

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1949
    return-void
.end method

.method public setMaxSpeed(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 579
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 580
    return-void
.end method

.method public setMaxTemperature(Ljava/lang/Byte;)V
    .locals 3

    .prologue
    .line 1246
    const/16 v0, 0x33

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1247
    return-void
.end method

.method public setMaxTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1855
    const/16 v0, 0x56

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1856
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 231
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 232
    return-void
.end method

.method public setMinAltitude(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1500
    const/16 v0, 0x3e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1501
    return-void
.end method

.method public setMinHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 1520
    const/16 v0, 0x3f

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1521
    return-void
.end method

.method public setMinSaturatedHemoglobinPercent(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1917
    const/16 v0, 0x58

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1918
    return-void
.end method

.method public setMinTotalHemoglobinConc(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1824
    const/16 v0, 0x55

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1825
    return-void
.end method

.method public setNormalizedPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 906
    const/16 v0, 0x21

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 907
    return-void
.end method

.method public setNumActiveLengths(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1026
    const/16 v0, 0x28

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1027
    return-void
.end method

.method public setNumLengths(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 886
    const/16 v0, 0x20

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 887
    return-void
.end method

.method public setOpponentScore(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1556
    const/16 v0, 0x4a

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1557
    return-void
.end method

.method public setPlayerScore(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1762
    const/16 v0, 0x53

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1763
    return-void
.end method

.method public setRepetitionNum(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1480
    const/16 v0, 0x3d

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1481
    return-void
.end method

.method public setSport(Lcom/garmin/fit/Sport;)V
    .locals 4

    .prologue
    .line 846
    const/16 v0, 0x19

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Sport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 847
    return-void
.end method

.method public setStartPositionLat(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 333
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 334
    return-void
.end method

.method public setStartPositionLong(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 353
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 354
    return-void
.end method

.method public setStartTime(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 313
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 314
    return-void
.end method

.method public setStrokeCount(ILjava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1587
    const/16 v0, 0x4b

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1588
    return-void
.end method

.method public setSubSport(Lcom/garmin/fit/SubSport;)V
    .locals 4

    .prologue
    .line 1004
    const/16 v0, 0x27

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SubSport;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1005
    return-void
.end method

.method public setSwimStroke(Lcom/garmin/fit/SwimStroke;)V
    .locals 4

    .prologue
    .line 983
    const/16 v0, 0x26

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/SwimStroke;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 984
    return-void
.end method

.method public setTimeInCadenceZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1433
    const/16 v0, 0x3b

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1434
    return-void
.end method

.method public setTimeInHrZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1375
    const/16 v0, 0x39

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1376
    return-void
.end method

.method public setTimeInPowerZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1462
    const/16 v0, 0x3c

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1463
    return-void
.end method

.method public setTimeInSpeedZone(ILjava/lang/Float;)V
    .locals 2

    .prologue
    .line 1404
    const/16 v0, 0x3a

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1405
    return-void
.end method

.method public setTimestamp(Lcom/garmin/fit/DateTime;)V
    .locals 4

    .prologue
    .line 253
    const/16 v0, 0xfd

    const/4 v1, 0x0

    invoke-virtual {p1}, Lcom/garmin/fit/DateTime;->getTimestamp()Ljava/lang/Long;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 254
    return-void
.end method

.method public setTotalAscent(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 763
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 764
    return-void
.end method

.method public setTotalCalories(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 517
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 518
    return-void
.end method

.method public setTotalCycles(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 477
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 478
    return-void
.end method

.method public setTotalDescent(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 783
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 784
    return-void
.end method

.method public setTotalDistance(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 457
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 458
    return-void
.end method

.method public setTotalElapsedTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 415
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 416
    return-void
.end method

.method public setTotalFatCalories(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 539
    const/16 v0, 0xc

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 540
    return-void
.end method

.method public setTotalFractionalCycles(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1744
    const/16 v0, 0x52

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1745
    return-void
.end method

.method public setTotalMovingTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 1266
    const/16 v0, 0x34

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1267
    return-void
.end method

.method public setTotalStrides(Ljava/lang/Long;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 497
    const/16 v0, 0xa

    invoke-virtual {p0, v0, v1, p1, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 498
    return-void
.end method

.method public setTotalTimerTime(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 437
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 438
    return-void
.end method

.method public setTotalWork(Ljava/lang/Long;)V
    .locals 3

    .prologue
    .line 1046
    const/16 v0, 0x29

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1047
    return-void
.end method

.method public setWktStepIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 1538
    const/16 v0, 0x47

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1539
    return-void
.end method

.method public setZoneCount(ILjava/lang/Integer;)V
    .locals 2

    .prologue
    .line 1618
    const/16 v0, 0x4c

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/LapMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 1619
    return-void
.end method
