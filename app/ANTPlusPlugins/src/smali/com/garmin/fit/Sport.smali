.class public final enum Lcom/garmin/fit/Sport;
.super Ljava/lang/Enum;
.source "Sport.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/Sport;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/Sport;

.field public static final enum ALL:Lcom/garmin/fit/Sport;

.field public static final enum ALPINE_SKIING:Lcom/garmin/fit/Sport;

.field public static final enum AMERICAN_FOOTBALL:Lcom/garmin/fit/Sport;

.field public static final enum BASKETBALL:Lcom/garmin/fit/Sport;

.field public static final enum CROSS_COUNTRY_SKIING:Lcom/garmin/fit/Sport;

.field public static final enum CYCLING:Lcom/garmin/fit/Sport;

.field public static final enum FITNESS_EQUIPMENT:Lcom/garmin/fit/Sport;

.field public static final enum GENERIC:Lcom/garmin/fit/Sport;

.field public static final enum HIKING:Lcom/garmin/fit/Sport;

.field public static final enum INVALID:Lcom/garmin/fit/Sport;

.field public static final enum MOUNTAINEERING:Lcom/garmin/fit/Sport;

.field public static final enum MULTISPORT:Lcom/garmin/fit/Sport;

.field public static final enum PADDLING:Lcom/garmin/fit/Sport;

.field public static final enum ROWING:Lcom/garmin/fit/Sport;

.field public static final enum RUNNING:Lcom/garmin/fit/Sport;

.field public static final enum SNOWBOARDING:Lcom/garmin/fit/Sport;

.field public static final enum SOCCER:Lcom/garmin/fit/Sport;

.field public static final enum SWIMMING:Lcom/garmin/fit/Sport;

.field public static final enum TENNIS:Lcom/garmin/fit/Sport;

.field public static final enum TRAINING:Lcom/garmin/fit/Sport;

.field public static final enum TRANSITION:Lcom/garmin/fit/Sport;

.field public static final enum WALKING:Lcom/garmin/fit/Sport;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "GENERIC"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->GENERIC:Lcom/garmin/fit/Sport;

    .line 22
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "RUNNING"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->RUNNING:Lcom/garmin/fit/Sport;

    .line 23
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "CYCLING"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->CYCLING:Lcom/garmin/fit/Sport;

    .line 24
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "TRANSITION"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->TRANSITION:Lcom/garmin/fit/Sport;

    .line 25
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "FITNESS_EQUIPMENT"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->FITNESS_EQUIPMENT:Lcom/garmin/fit/Sport;

    .line 26
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "SWIMMING"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->SWIMMING:Lcom/garmin/fit/Sport;

    .line 27
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "BASKETBALL"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->BASKETBALL:Lcom/garmin/fit/Sport;

    .line 28
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "SOCCER"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->SOCCER:Lcom/garmin/fit/Sport;

    .line 29
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "TENNIS"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->TENNIS:Lcom/garmin/fit/Sport;

    .line 30
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "AMERICAN_FOOTBALL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->AMERICAN_FOOTBALL:Lcom/garmin/fit/Sport;

    .line 31
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "TRAINING"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->TRAINING:Lcom/garmin/fit/Sport;

    .line 32
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "WALKING"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->WALKING:Lcom/garmin/fit/Sport;

    .line 33
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "CROSS_COUNTRY_SKIING"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->CROSS_COUNTRY_SKIING:Lcom/garmin/fit/Sport;

    .line 34
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "ALPINE_SKIING"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->ALPINE_SKIING:Lcom/garmin/fit/Sport;

    .line 35
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "SNOWBOARDING"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->SNOWBOARDING:Lcom/garmin/fit/Sport;

    .line 36
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "ROWING"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->ROWING:Lcom/garmin/fit/Sport;

    .line 37
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "MOUNTAINEERING"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->MOUNTAINEERING:Lcom/garmin/fit/Sport;

    .line 38
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "HIKING"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->HIKING:Lcom/garmin/fit/Sport;

    .line 39
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "MULTISPORT"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->MULTISPORT:Lcom/garmin/fit/Sport;

    .line 40
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "PADDLING"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->PADDLING:Lcom/garmin/fit/Sport;

    .line 41
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "ALL"

    const/16 v2, 0x14

    const/16 v3, 0xfe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->ALL:Lcom/garmin/fit/Sport;

    .line 42
    new-instance v0, Lcom/garmin/fit/Sport;

    const-string v1, "INVALID"

    const/16 v2, 0x15

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Sport;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Sport;->INVALID:Lcom/garmin/fit/Sport;

    .line 20
    const/16 v0, 0x16

    new-array v0, v0, [Lcom/garmin/fit/Sport;

    sget-object v1, Lcom/garmin/fit/Sport;->GENERIC:Lcom/garmin/fit/Sport;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/Sport;->RUNNING:Lcom/garmin/fit/Sport;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/Sport;->CYCLING:Lcom/garmin/fit/Sport;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/Sport;->TRANSITION:Lcom/garmin/fit/Sport;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/Sport;->FITNESS_EQUIPMENT:Lcom/garmin/fit/Sport;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/Sport;->SWIMMING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/Sport;->BASKETBALL:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/Sport;->SOCCER:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/Sport;->TENNIS:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/Sport;->AMERICAN_FOOTBALL:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/Sport;->TRAINING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/Sport;->WALKING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/Sport;->CROSS_COUNTRY_SKIING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/Sport;->ALPINE_SKIING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/Sport;->SNOWBOARDING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/Sport;->ROWING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/Sport;->MOUNTAINEERING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/Sport;->HIKING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/Sport;->MULTISPORT:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/Sport;->PADDLING:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/Sport;->ALL:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/Sport;->INVALID:Lcom/garmin/fit/Sport;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/Sport;->$VALUES:[Lcom/garmin/fit/Sport;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 50
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 51
    iput-short p3, p0, Lcom/garmin/fit/Sport;->value:S

    .line 52
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Sport;
    .locals 6

    .prologue
    .line 55
    invoke-static {}, Lcom/garmin/fit/Sport;->values()[Lcom/garmin/fit/Sport;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 56
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/Sport;->value:S

    if-ne v4, v5, :cond_0

    .line 60
    :goto_1
    return-object v0

    .line 55
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 60
    :cond_1
    sget-object v0, Lcom/garmin/fit/Sport;->INVALID:Lcom/garmin/fit/Sport;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/Sport;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/Sport;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Sport;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/Sport;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/Sport;->$VALUES:[Lcom/garmin/fit/Sport;

    invoke-virtual {v0}, [Lcom/garmin/fit/Sport;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/Sport;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 64
    iget-short v0, p0, Lcom/garmin/fit/Sport;->value:S

    return v0
.end method
