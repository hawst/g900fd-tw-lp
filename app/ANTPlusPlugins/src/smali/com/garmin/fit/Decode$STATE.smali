.class final enum Lcom/garmin/fit/Decode$STATE;
.super Ljava/lang/Enum;
.source "Decode.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/garmin/fit/Decode;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x401a
    name = "STATE"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/Decode$STATE;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/Decode$STATE;

.field public static final enum ARCH:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FIELD_DATA:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FIELD_NUM:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FIELD_SIZE:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FIELD_TYPE:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FILE_CRC_HIGH:Lcom/garmin/fit/Decode$STATE;

.field public static final enum FILE_HDR:Lcom/garmin/fit/Decode$STATE;

.field public static final enum MESG_NUM_0:Lcom/garmin/fit/Decode$STATE;

.field public static final enum MESG_NUM_1:Lcom/garmin/fit/Decode$STATE;

.field public static final enum NUM_FIELDS:Lcom/garmin/fit/Decode$STATE;

.field public static final enum RECORD:Lcom/garmin/fit/Decode$STATE;

.field public static final enum RESERVED1:Lcom/garmin/fit/Decode$STATE;


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 36
    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FILE_HDR"

    invoke-direct {v0, v1, v3}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FILE_HDR:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "RECORD"

    invoke-direct {v0, v1, v4}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "RESERVED1"

    invoke-direct {v0, v1, v5}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->RESERVED1:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "ARCH"

    invoke-direct {v0, v1, v6}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->ARCH:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "MESG_NUM_0"

    invoke-direct {v0, v1, v7}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_0:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "MESG_NUM_1"

    const/4 v2, 0x5

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_1:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "NUM_FIELDS"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->NUM_FIELDS:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FIELD_NUM"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_NUM:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FIELD_SIZE"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_SIZE:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FIELD_TYPE"

    const/16 v2, 0x9

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_TYPE:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FIELD_DATA"

    const/16 v2, 0xa

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FIELD_DATA:Lcom/garmin/fit/Decode$STATE;

    new-instance v0, Lcom/garmin/fit/Decode$STATE;

    const-string v1, "FILE_CRC_HIGH"

    const/16 v2, 0xb

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Decode$STATE;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->FILE_CRC_HIGH:Lcom/garmin/fit/Decode$STATE;

    .line 35
    const/16 v0, 0xc

    new-array v0, v0, [Lcom/garmin/fit/Decode$STATE;

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->FILE_HDR:Lcom/garmin/fit/Decode$STATE;

    aput-object v1, v0, v3

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->RECORD:Lcom/garmin/fit/Decode$STATE;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->RESERVED1:Lcom/garmin/fit/Decode$STATE;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->ARCH:Lcom/garmin/fit/Decode$STATE;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_0:Lcom/garmin/fit/Decode$STATE;

    aput-object v1, v0, v7

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->MESG_NUM_1:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->NUM_FIELDS:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->FIELD_NUM:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->FIELD_SIZE:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->FIELD_TYPE:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->FIELD_DATA:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/Decode$STATE;->FILE_CRC_HIGH:Lcom/garmin/fit/Decode$STATE;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/Decode$STATE;->$VALUES:[Lcom/garmin/fit/Decode$STATE;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;I)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()V"
        }
    .end annotation

    .prologue
    .line 35
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/Decode$STATE;
    .locals 1

    .prologue
    .line 35
    const-class v0, Lcom/garmin/fit/Decode$STATE;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Decode$STATE;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/Decode$STATE;
    .locals 1

    .prologue
    .line 35
    sget-object v0, Lcom/garmin/fit/Decode$STATE;->$VALUES:[Lcom/garmin/fit/Decode$STATE;

    invoke-virtual {v0}, [Lcom/garmin/fit/Decode$STATE;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/Decode$STATE;

    return-object v0
.end method
