.class public final enum Lcom/garmin/fit/Gender;
.super Ljava/lang/Enum;
.source "Gender.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/Gender;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/Gender;

.field public static final enum FEMALE:Lcom/garmin/fit/Gender;

.field public static final enum INVALID:Lcom/garmin/fit/Gender;

.field public static final enum MALE:Lcom/garmin/fit/Gender;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/Gender;

    const-string v1, "FEMALE"

    invoke-direct {v0, v1, v3, v3}, Lcom/garmin/fit/Gender;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Gender;->FEMALE:Lcom/garmin/fit/Gender;

    .line 22
    new-instance v0, Lcom/garmin/fit/Gender;

    const-string v1, "MALE"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/Gender;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Gender;->MALE:Lcom/garmin/fit/Gender;

    .line 23
    new-instance v0, Lcom/garmin/fit/Gender;

    const-string v1, "INVALID"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v5, v2}, Lcom/garmin/fit/Gender;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Gender;->INVALID:Lcom/garmin/fit/Gender;

    .line 20
    const/4 v0, 0x3

    new-array v0, v0, [Lcom/garmin/fit/Gender;

    sget-object v1, Lcom/garmin/fit/Gender;->FEMALE:Lcom/garmin/fit/Gender;

    aput-object v1, v0, v3

    sget-object v1, Lcom/garmin/fit/Gender;->MALE:Lcom/garmin/fit/Gender;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/Gender;->INVALID:Lcom/garmin/fit/Gender;

    aput-object v1, v0, v5

    sput-object v0, Lcom/garmin/fit/Gender;->$VALUES:[Lcom/garmin/fit/Gender;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 31
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 32
    iput-short p3, p0, Lcom/garmin/fit/Gender;->value:S

    .line 33
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Gender;
    .locals 6

    .prologue
    .line 36
    invoke-static {}, Lcom/garmin/fit/Gender;->values()[Lcom/garmin/fit/Gender;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 37
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/Gender;->value:S

    if-ne v4, v5, :cond_0

    .line 41
    :goto_1
    return-object v0

    .line 36
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 41
    :cond_1
    sget-object v0, Lcom/garmin/fit/Gender;->INVALID:Lcom/garmin/fit/Gender;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/Gender;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/Gender;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Gender;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/Gender;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/Gender;->$VALUES:[Lcom/garmin/fit/Gender;

    invoke-virtual {v0}, [Lcom/garmin/fit/Gender;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/Gender;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 45
    iget-short v0, p0, Lcom/garmin/fit/Gender;->value:S

    return v0
.end method
