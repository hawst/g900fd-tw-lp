.class public Lcom/garmin/fit/ZonesTargetMesg;
.super Lcom/garmin/fit/Mesg;
.source "ZonesTargetMesg.java"


# static fields
.field protected static final zonesTargetMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 12

    .prologue
    const/4 v11, 0x7

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const/4 v9, 0x0

    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "zones_target"

    invoke-direct {v0, v1, v11}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "max_heart_rate"

    const/4 v2, 0x1

    const-string v8, ""

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "threshold_heart_rate"

    const-string v8, ""

    move v2, v3

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "functional_threshold_power"

    const/4 v2, 0x3

    const/16 v3, 0x84

    const-string v8, ""

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "hr_calc_type"

    const/4 v2, 0x5

    const-string v8, ""

    move v3, v9

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/ZonesTargetMesg;->zonesTargetMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "pwr_calc_type"

    const-string v8, ""

    move v2, v11

    move v3, v9

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x7

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 41
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 44
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 45
    return-void
.end method


# virtual methods
.method public getFunctionalThresholdPower()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 90
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getHrCalcType()Lcom/garmin/fit/HrZoneCalc;
    .locals 3

    .prologue
    .line 108
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 109
    if-nez v0, :cond_0

    .line 110
    const/4 v0, 0x0

    .line 111
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/HrZoneCalc;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/HrZoneCalc;

    move-result-object v0

    goto :goto_0
.end method

.method public getMaxHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 54
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getPwrCalcType()Lcom/garmin/fit/PwrZoneCalc;
    .locals 3

    .prologue
    .line 129
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 130
    if-nez v0, :cond_0

    .line 131
    const/4 v0, 0x0

    .line 132
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/PwrZoneCalc;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/PwrZoneCalc;

    move-result-object v0

    goto :goto_0
.end method

.method public getThresholdHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 72
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public setFunctionalThresholdPower(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 99
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 100
    return-void
.end method

.method public setHrCalcType(Lcom/garmin/fit/HrZoneCalc;)V
    .locals 4

    .prologue
    .line 120
    const/4 v0, 0x5

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/HrZoneCalc;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/ZonesTargetMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 121
    return-void
.end method

.method public setMaxHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 63
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 64
    return-void
.end method

.method public setPwrCalcType(Lcom/garmin/fit/PwrZoneCalc;)V
    .locals 4

    .prologue
    .line 141
    const/4 v0, 0x7

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/PwrZoneCalc;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/ZonesTargetMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 142
    return-void
.end method

.method public setThresholdHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 81
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/ZonesTargetMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 82
    return-void
.end method
