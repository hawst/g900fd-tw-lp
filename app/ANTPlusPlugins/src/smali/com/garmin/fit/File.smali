.class public final enum Lcom/garmin/fit/File;
.super Ljava/lang/Enum;
.source "File.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/File;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/File;

.field public static final enum ACTIVITY:Lcom/garmin/fit/File;

.field public static final enum ACTIVITY_SUMMARY:Lcom/garmin/fit/File;

.field public static final enum BLOOD_PRESSURE:Lcom/garmin/fit/File;

.field public static final enum COURSE:Lcom/garmin/fit/File;

.field public static final enum DEVICE:Lcom/garmin/fit/File;

.field public static final enum GOALS:Lcom/garmin/fit/File;

.field public static final enum INVALID:Lcom/garmin/fit/File;

.field public static final enum MONITORING_A:Lcom/garmin/fit/File;

.field public static final enum MONITORING_B:Lcom/garmin/fit/File;

.field public static final enum MONITORING_DAILY:Lcom/garmin/fit/File;

.field public static final enum SCHEDULES:Lcom/garmin/fit/File;

.field public static final enum SETTINGS:Lcom/garmin/fit/File;

.field public static final enum SPORT:Lcom/garmin/fit/File;

.field public static final enum TOTALS:Lcom/garmin/fit/File;

.field public static final enum WEIGHT:Lcom/garmin/fit/File;

.field public static final enum WORKOUT:Lcom/garmin/fit/File;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x5

    const/4 v7, 0x4

    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    .line 21
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "DEVICE"

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2, v4}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->DEVICE:Lcom/garmin/fit/File;

    .line 22
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "SETTINGS"

    invoke-direct {v0, v1, v4, v5}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->SETTINGS:Lcom/garmin/fit/File;

    .line 23
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "SPORT"

    invoke-direct {v0, v1, v5, v6}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->SPORT:Lcom/garmin/fit/File;

    .line 24
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "ACTIVITY"

    invoke-direct {v0, v1, v6, v7}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->ACTIVITY:Lcom/garmin/fit/File;

    .line 25
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "WORKOUT"

    invoke-direct {v0, v1, v7, v8}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->WORKOUT:Lcom/garmin/fit/File;

    .line 26
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "COURSE"

    const/4 v2, 0x6

    invoke-direct {v0, v1, v8, v2}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->COURSE:Lcom/garmin/fit/File;

    .line 27
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "SCHEDULES"

    const/4 v2, 0x6

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->SCHEDULES:Lcom/garmin/fit/File;

    .line 28
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "WEIGHT"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->WEIGHT:Lcom/garmin/fit/File;

    .line 29
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "TOTALS"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->TOTALS:Lcom/garmin/fit/File;

    .line 30
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "GOALS"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->GOALS:Lcom/garmin/fit/File;

    .line 31
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "BLOOD_PRESSURE"

    const/16 v2, 0xa

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->BLOOD_PRESSURE:Lcom/garmin/fit/File;

    .line 32
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "MONITORING_A"

    const/16 v2, 0xb

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->MONITORING_A:Lcom/garmin/fit/File;

    .line 33
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "ACTIVITY_SUMMARY"

    const/16 v2, 0xc

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->ACTIVITY_SUMMARY:Lcom/garmin/fit/File;

    .line 34
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "MONITORING_DAILY"

    const/16 v2, 0xd

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->MONITORING_DAILY:Lcom/garmin/fit/File;

    .line 35
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "MONITORING_B"

    const/16 v2, 0xe

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->MONITORING_B:Lcom/garmin/fit/File;

    .line 36
    new-instance v0, Lcom/garmin/fit/File;

    const-string v1, "INVALID"

    const/16 v2, 0xf

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/File;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/File;->INVALID:Lcom/garmin/fit/File;

    .line 20
    const/16 v0, 0x10

    new-array v0, v0, [Lcom/garmin/fit/File;

    const/4 v1, 0x0

    sget-object v2, Lcom/garmin/fit/File;->DEVICE:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    sget-object v1, Lcom/garmin/fit/File;->SETTINGS:Lcom/garmin/fit/File;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/File;->SPORT:Lcom/garmin/fit/File;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/File;->ACTIVITY:Lcom/garmin/fit/File;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/File;->WORKOUT:Lcom/garmin/fit/File;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/File;->COURSE:Lcom/garmin/fit/File;

    aput-object v1, v0, v8

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/File;->SCHEDULES:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/File;->WEIGHT:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/File;->TOTALS:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/File;->GOALS:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/File;->BLOOD_PRESSURE:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/File;->MONITORING_A:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/File;->ACTIVITY_SUMMARY:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/File;->MONITORING_DAILY:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/File;->MONITORING_B:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/File;->INVALID:Lcom/garmin/fit/File;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/File;->$VALUES:[Lcom/garmin/fit/File;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 44
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 45
    iput-short p3, p0, Lcom/garmin/fit/File;->value:S

    .line 46
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/File;
    .locals 6

    .prologue
    .line 49
    invoke-static {}, Lcom/garmin/fit/File;->values()[Lcom/garmin/fit/File;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 50
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/File;->value:S

    if-ne v4, v5, :cond_0

    .line 54
    :goto_1
    return-object v0

    .line 49
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 54
    :cond_1
    sget-object v0, Lcom/garmin/fit/File;->INVALID:Lcom/garmin/fit/File;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/File;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/File;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/File;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/File;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/File;->$VALUES:[Lcom/garmin/fit/File;

    invoke-virtual {v0}, [Lcom/garmin/fit/File;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/File;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 58
    iget-short v0, p0, Lcom/garmin/fit/File;->value:S

    return v0
.end method
