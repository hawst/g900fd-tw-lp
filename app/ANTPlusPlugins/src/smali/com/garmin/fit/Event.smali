.class public final enum Lcom/garmin/fit/Event;
.super Ljava/lang/Enum;
.source "Event.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/Event;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/Event;

.field public static final enum ACTIVITY:Lcom/garmin/fit/Event;

.field public static final enum BATTERY:Lcom/garmin/fit/Event;

.field public static final enum BATTERY_LOW:Lcom/garmin/fit/Event;

.field public static final enum CAD_HIGH_ALERT:Lcom/garmin/fit/Event;

.field public static final enum CAD_LOW_ALERT:Lcom/garmin/fit/Event;

.field public static final enum CALIBRATION:Lcom/garmin/fit/Event;

.field public static final enum CALORIE_DURATION_ALERT:Lcom/garmin/fit/Event;

.field public static final enum COURSE_POINT:Lcom/garmin/fit/Event;

.field public static final enum DISTANCE_DURATION_ALERT:Lcom/garmin/fit/Event;

.field public static final enum FITNESS_EQUIPMENT:Lcom/garmin/fit/Event;

.field public static final enum FRONT_GEAR_CHANGE:Lcom/garmin/fit/Event;

.field public static final enum HR_HIGH_ALERT:Lcom/garmin/fit/Event;

.field public static final enum HR_LOW_ALERT:Lcom/garmin/fit/Event;

.field public static final enum INVALID:Lcom/garmin/fit/Event;

.field public static final enum LAP:Lcom/garmin/fit/Event;

.field public static final enum LENGTH:Lcom/garmin/fit/Event;

.field public static final enum OFF_COURSE:Lcom/garmin/fit/Event;

.field public static final enum POWER_DOWN:Lcom/garmin/fit/Event;

.field public static final enum POWER_HIGH_ALERT:Lcom/garmin/fit/Event;

.field public static final enum POWER_LOW_ALERT:Lcom/garmin/fit/Event;

.field public static final enum POWER_UP:Lcom/garmin/fit/Event;

.field public static final enum REAR_GEAR_CHANGE:Lcom/garmin/fit/Event;

.field public static final enum RECOVERY_HR:Lcom/garmin/fit/Event;

.field public static final enum SESSION:Lcom/garmin/fit/Event;

.field public static final enum SPEED_HIGH_ALERT:Lcom/garmin/fit/Event;

.field public static final enum SPEED_LOW_ALERT:Lcom/garmin/fit/Event;

.field public static final enum SPORT_POINT:Lcom/garmin/fit/Event;

.field public static final enum TIMER:Lcom/garmin/fit/Event;

.field public static final enum TIME_DURATION_ALERT:Lcom/garmin/fit/Event;

.field public static final enum USER_MARKER:Lcom/garmin/fit/Event;

.field public static final enum VIRTUAL_PARTNER_PACE:Lcom/garmin/fit/Event;

.field public static final enum WORKOUT:Lcom/garmin/fit/Event;

.field public static final enum WORKOUT_STEP:Lcom/garmin/fit/Event;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x6

    const/4 v7, 0x5

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "TIMER"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->TIMER:Lcom/garmin/fit/Event;

    .line 22
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "WORKOUT"

    const/4 v2, 0x1

    invoke-direct {v0, v1, v2, v5}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->WORKOUT:Lcom/garmin/fit/Event;

    .line 23
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "WORKOUT_STEP"

    const/4 v2, 0x2

    invoke-direct {v0, v1, v2, v6}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->WORKOUT_STEP:Lcom/garmin/fit/Event;

    .line 24
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "POWER_DOWN"

    invoke-direct {v0, v1, v5, v7}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->POWER_DOWN:Lcom/garmin/fit/Event;

    .line 25
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "POWER_UP"

    invoke-direct {v0, v1, v6, v8}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->POWER_UP:Lcom/garmin/fit/Event;

    .line 26
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "OFF_COURSE"

    const/4 v2, 0x7

    invoke-direct {v0, v1, v7, v2}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->OFF_COURSE:Lcom/garmin/fit/Event;

    .line 27
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "SESSION"

    const/16 v2, 0x8

    invoke-direct {v0, v1, v8, v2}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->SESSION:Lcom/garmin/fit/Event;

    .line 28
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "LAP"

    const/4 v2, 0x7

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->LAP:Lcom/garmin/fit/Event;

    .line 29
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "COURSE_POINT"

    const/16 v2, 0x8

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->COURSE_POINT:Lcom/garmin/fit/Event;

    .line 30
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "BATTERY"

    const/16 v2, 0x9

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->BATTERY:Lcom/garmin/fit/Event;

    .line 31
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "VIRTUAL_PARTNER_PACE"

    const/16 v2, 0xa

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->VIRTUAL_PARTNER_PACE:Lcom/garmin/fit/Event;

    .line 32
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "HR_HIGH_ALERT"

    const/16 v2, 0xb

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->HR_HIGH_ALERT:Lcom/garmin/fit/Event;

    .line 33
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "HR_LOW_ALERT"

    const/16 v2, 0xc

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->HR_LOW_ALERT:Lcom/garmin/fit/Event;

    .line 34
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "SPEED_HIGH_ALERT"

    const/16 v2, 0xd

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->SPEED_HIGH_ALERT:Lcom/garmin/fit/Event;

    .line 35
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "SPEED_LOW_ALERT"

    const/16 v2, 0xe

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->SPEED_LOW_ALERT:Lcom/garmin/fit/Event;

    .line 36
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "CAD_HIGH_ALERT"

    const/16 v2, 0xf

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->CAD_HIGH_ALERT:Lcom/garmin/fit/Event;

    .line 37
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "CAD_LOW_ALERT"

    const/16 v2, 0x10

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->CAD_LOW_ALERT:Lcom/garmin/fit/Event;

    .line 38
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "POWER_HIGH_ALERT"

    const/16 v2, 0x11

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->POWER_HIGH_ALERT:Lcom/garmin/fit/Event;

    .line 39
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "POWER_LOW_ALERT"

    const/16 v2, 0x12

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->POWER_LOW_ALERT:Lcom/garmin/fit/Event;

    .line 40
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "RECOVERY_HR"

    const/16 v2, 0x13

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->RECOVERY_HR:Lcom/garmin/fit/Event;

    .line 41
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "BATTERY_LOW"

    const/16 v2, 0x14

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->BATTERY_LOW:Lcom/garmin/fit/Event;

    .line 42
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "TIME_DURATION_ALERT"

    const/16 v2, 0x15

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->TIME_DURATION_ALERT:Lcom/garmin/fit/Event;

    .line 43
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "DISTANCE_DURATION_ALERT"

    const/16 v2, 0x16

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->DISTANCE_DURATION_ALERT:Lcom/garmin/fit/Event;

    .line 44
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "CALORIE_DURATION_ALERT"

    const/16 v2, 0x17

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->CALORIE_DURATION_ALERT:Lcom/garmin/fit/Event;

    .line 45
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "ACTIVITY"

    const/16 v2, 0x18

    const/16 v3, 0x1a

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->ACTIVITY:Lcom/garmin/fit/Event;

    .line 46
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "FITNESS_EQUIPMENT"

    const/16 v2, 0x19

    const/16 v3, 0x1b

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->FITNESS_EQUIPMENT:Lcom/garmin/fit/Event;

    .line 47
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "LENGTH"

    const/16 v2, 0x1a

    const/16 v3, 0x1c

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->LENGTH:Lcom/garmin/fit/Event;

    .line 48
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "USER_MARKER"

    const/16 v2, 0x1b

    const/16 v3, 0x20

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->USER_MARKER:Lcom/garmin/fit/Event;

    .line 49
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "SPORT_POINT"

    const/16 v2, 0x1c

    const/16 v3, 0x21

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->SPORT_POINT:Lcom/garmin/fit/Event;

    .line 50
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "CALIBRATION"

    const/16 v2, 0x1d

    const/16 v3, 0x24

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->CALIBRATION:Lcom/garmin/fit/Event;

    .line 51
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "FRONT_GEAR_CHANGE"

    const/16 v2, 0x1e

    const/16 v3, 0x2a

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->FRONT_GEAR_CHANGE:Lcom/garmin/fit/Event;

    .line 52
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "REAR_GEAR_CHANGE"

    const/16 v2, 0x1f

    const/16 v3, 0x2b

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->REAR_GEAR_CHANGE:Lcom/garmin/fit/Event;

    .line 53
    new-instance v0, Lcom/garmin/fit/Event;

    const-string v1, "INVALID"

    const/16 v2, 0x20

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Event;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Event;->INVALID:Lcom/garmin/fit/Event;

    .line 20
    const/16 v0, 0x21

    new-array v0, v0, [Lcom/garmin/fit/Event;

    sget-object v1, Lcom/garmin/fit/Event;->TIMER:Lcom/garmin/fit/Event;

    aput-object v1, v0, v4

    const/4 v1, 0x1

    sget-object v2, Lcom/garmin/fit/Event;->WORKOUT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/4 v1, 0x2

    sget-object v2, Lcom/garmin/fit/Event;->WORKOUT_STEP:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    sget-object v1, Lcom/garmin/fit/Event;->POWER_DOWN:Lcom/garmin/fit/Event;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/Event;->POWER_UP:Lcom/garmin/fit/Event;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/Event;->OFF_COURSE:Lcom/garmin/fit/Event;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/Event;->SESSION:Lcom/garmin/fit/Event;

    aput-object v1, v0, v8

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/Event;->LAP:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/Event;->COURSE_POINT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/Event;->BATTERY:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/Event;->VIRTUAL_PARTNER_PACE:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/Event;->HR_HIGH_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/Event;->HR_LOW_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/Event;->SPEED_HIGH_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/Event;->SPEED_LOW_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/Event;->CAD_HIGH_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/Event;->CAD_LOW_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/Event;->POWER_HIGH_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/Event;->POWER_LOW_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/Event;->RECOVERY_HR:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/Event;->BATTERY_LOW:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/Event;->TIME_DURATION_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/garmin/fit/Event;->DISTANCE_DURATION_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/garmin/fit/Event;->CALORIE_DURATION_ALERT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/garmin/fit/Event;->ACTIVITY:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/garmin/fit/Event;->FITNESS_EQUIPMENT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/garmin/fit/Event;->LENGTH:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/garmin/fit/Event;->USER_MARKER:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1c

    sget-object v2, Lcom/garmin/fit/Event;->SPORT_POINT:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1d

    sget-object v2, Lcom/garmin/fit/Event;->CALIBRATION:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1e

    sget-object v2, Lcom/garmin/fit/Event;->FRONT_GEAR_CHANGE:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x1f

    sget-object v2, Lcom/garmin/fit/Event;->REAR_GEAR_CHANGE:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    const/16 v1, 0x20

    sget-object v2, Lcom/garmin/fit/Event;->INVALID:Lcom/garmin/fit/Event;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/Event;->$VALUES:[Lcom/garmin/fit/Event;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 61
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 62
    iput-short p3, p0, Lcom/garmin/fit/Event;->value:S

    .line 63
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Event;
    .locals 6

    .prologue
    .line 66
    invoke-static {}, Lcom/garmin/fit/Event;->values()[Lcom/garmin/fit/Event;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 67
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/Event;->value:S

    if-ne v4, v5, :cond_0

    .line 71
    :goto_1
    return-object v0

    .line 66
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 71
    :cond_1
    sget-object v0, Lcom/garmin/fit/Event;->INVALID:Lcom/garmin/fit/Event;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/Event;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/Event;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Event;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/Event;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/Event;->$VALUES:[Lcom/garmin/fit/Event;

    invoke-virtual {v0}, [Lcom/garmin/fit/Event;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/Event;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 75
    iget-short v0, p0, Lcom/garmin/fit/Event;->value:S

    return v0
.end method
