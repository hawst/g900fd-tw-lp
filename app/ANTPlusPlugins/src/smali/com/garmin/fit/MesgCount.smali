.class public final enum Lcom/garmin/fit/MesgCount;
.super Ljava/lang/Enum;
.source "MesgCount.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/MesgCount;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/MesgCount;

.field public static final enum INVALID:Lcom/garmin/fit/MesgCount;

.field public static final enum MAX_PER_FILE:Lcom/garmin/fit/MesgCount;

.field public static final enum MAX_PER_FILE_TYPE:Lcom/garmin/fit/MesgCount;

.field public static final enum NUM_PER_FILE:Lcom/garmin/fit/MesgCount;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 7

    .prologue
    const/4 v6, 0x3

    const/4 v5, 0x2

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/MesgCount;

    const-string v1, "NUM_PER_FILE"

    invoke-direct {v0, v1, v3, v3}, Lcom/garmin/fit/MesgCount;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/MesgCount;->NUM_PER_FILE:Lcom/garmin/fit/MesgCount;

    .line 22
    new-instance v0, Lcom/garmin/fit/MesgCount;

    const-string v1, "MAX_PER_FILE"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/MesgCount;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/MesgCount;->MAX_PER_FILE:Lcom/garmin/fit/MesgCount;

    .line 23
    new-instance v0, Lcom/garmin/fit/MesgCount;

    const-string v1, "MAX_PER_FILE_TYPE"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/MesgCount;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/MesgCount;->MAX_PER_FILE_TYPE:Lcom/garmin/fit/MesgCount;

    .line 24
    new-instance v0, Lcom/garmin/fit/MesgCount;

    const-string v1, "INVALID"

    const/16 v2, 0xff

    invoke-direct {v0, v1, v6, v2}, Lcom/garmin/fit/MesgCount;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/MesgCount;->INVALID:Lcom/garmin/fit/MesgCount;

    .line 20
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/garmin/fit/MesgCount;

    sget-object v1, Lcom/garmin/fit/MesgCount;->NUM_PER_FILE:Lcom/garmin/fit/MesgCount;

    aput-object v1, v0, v3

    sget-object v1, Lcom/garmin/fit/MesgCount;->MAX_PER_FILE:Lcom/garmin/fit/MesgCount;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/MesgCount;->MAX_PER_FILE_TYPE:Lcom/garmin/fit/MesgCount;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/MesgCount;->INVALID:Lcom/garmin/fit/MesgCount;

    aput-object v1, v0, v6

    sput-object v0, Lcom/garmin/fit/MesgCount;->$VALUES:[Lcom/garmin/fit/MesgCount;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 32
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 33
    iput-short p3, p0, Lcom/garmin/fit/MesgCount;->value:S

    .line 34
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/MesgCount;
    .locals 6

    .prologue
    .line 37
    invoke-static {}, Lcom/garmin/fit/MesgCount;->values()[Lcom/garmin/fit/MesgCount;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 38
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/MesgCount;->value:S

    if-ne v4, v5, :cond_0

    .line 42
    :goto_1
    return-object v0

    .line 37
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 42
    :cond_1
    sget-object v0, Lcom/garmin/fit/MesgCount;->INVALID:Lcom/garmin/fit/MesgCount;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/MesgCount;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/MesgCount;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgCount;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/MesgCount;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/MesgCount;->$VALUES:[Lcom/garmin/fit/MesgCount;

    invoke-virtual {v0}, [Lcom/garmin/fit/MesgCount;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/MesgCount;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 46
    iget-short v0, p0, Lcom/garmin/fit/MesgCount;->value:S

    return v0
.end method
