.class public final enum Lcom/garmin/fit/EventType;
.super Ljava/lang/Enum;
.source "EventType.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/EventType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/EventType;

.field public static final enum BEGIN_DEPRECIATED:Lcom/garmin/fit/EventType;

.field public static final enum CONSECUTIVE_DEPRECIATED:Lcom/garmin/fit/EventType;

.field public static final enum END_ALL_DEPRECIATED:Lcom/garmin/fit/EventType;

.field public static final enum END_DEPRECIATED:Lcom/garmin/fit/EventType;

.field public static final enum INVALID:Lcom/garmin/fit/EventType;

.field public static final enum MARKER:Lcom/garmin/fit/EventType;

.field public static final enum START:Lcom/garmin/fit/EventType;

.field public static final enum STOP:Lcom/garmin/fit/EventType;

.field public static final enum STOP_ALL:Lcom/garmin/fit/EventType;

.field public static final enum STOP_DISABLE:Lcom/garmin/fit/EventType;

.field public static final enum STOP_DISABLE_ALL:Lcom/garmin/fit/EventType;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "START"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->START:Lcom/garmin/fit/EventType;

    .line 22
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "STOP"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    .line 23
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "CONSECUTIVE_DEPRECIATED"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->CONSECUTIVE_DEPRECIATED:Lcom/garmin/fit/EventType;

    .line 24
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "MARKER"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->MARKER:Lcom/garmin/fit/EventType;

    .line 25
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "STOP_ALL"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->STOP_ALL:Lcom/garmin/fit/EventType;

    .line 26
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "BEGIN_DEPRECIATED"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->BEGIN_DEPRECIATED:Lcom/garmin/fit/EventType;

    .line 27
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "END_DEPRECIATED"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->END_DEPRECIATED:Lcom/garmin/fit/EventType;

    .line 28
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "END_ALL_DEPRECIATED"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->END_ALL_DEPRECIATED:Lcom/garmin/fit/EventType;

    .line 29
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "STOP_DISABLE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->STOP_DISABLE:Lcom/garmin/fit/EventType;

    .line 30
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "STOP_DISABLE_ALL"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->STOP_DISABLE_ALL:Lcom/garmin/fit/EventType;

    .line 31
    new-instance v0, Lcom/garmin/fit/EventType;

    const-string v1, "INVALID"

    const/16 v2, 0xa

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/EventType;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/EventType;->INVALID:Lcom/garmin/fit/EventType;

    .line 20
    const/16 v0, 0xb

    new-array v0, v0, [Lcom/garmin/fit/EventType;

    sget-object v1, Lcom/garmin/fit/EventType;->START:Lcom/garmin/fit/EventType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/EventType;->CONSECUTIVE_DEPRECIATED:Lcom/garmin/fit/EventType;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/EventType;->MARKER:Lcom/garmin/fit/EventType;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/EventType;->STOP_ALL:Lcom/garmin/fit/EventType;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/EventType;->BEGIN_DEPRECIATED:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/EventType;->END_DEPRECIATED:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/EventType;->END_ALL_DEPRECIATED:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/EventType;->STOP_DISABLE:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/EventType;->STOP_DISABLE_ALL:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/EventType;->INVALID:Lcom/garmin/fit/EventType;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/EventType;->$VALUES:[Lcom/garmin/fit/EventType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput-short p3, p0, Lcom/garmin/fit/EventType;->value:S

    .line 41
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/EventType;
    .locals 6

    .prologue
    .line 44
    invoke-static {}, Lcom/garmin/fit/EventType;->values()[Lcom/garmin/fit/EventType;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 45
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/EventType;->value:S

    if-ne v4, v5, :cond_0

    .line 49
    :goto_1
    return-object v0

    .line 44
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 49
    :cond_1
    sget-object v0, Lcom/garmin/fit/EventType;->INVALID:Lcom/garmin/fit/EventType;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/EventType;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/EventType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/EventType;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/EventType;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/EventType;->$VALUES:[Lcom/garmin/fit/EventType;

    invoke-virtual {v0}, [Lcom/garmin/fit/EventType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/EventType;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 53
    iget-short v0, p0, Lcom/garmin/fit/EventType;->value:S

    return v0
.end method
