.class public Lcom/garmin/fit/Profile$SubFields;
.super Ljava/lang/Object;
.source "Profile.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/garmin/fit/Profile;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "SubFields"
.end annotation


# static fields
.field public static final DEVICE_INFO_MESG_DEVICE_TYPE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final DEVICE_INFO_MESG_DEVICE_TYPE_FIELD_ANTPLUS_DEVICE_TYPE:I = 0x0

.field public static final DEVICE_INFO_MESG_DEVICE_TYPE_FIELD_ANT_DEVICE_TYPE:I = 0x1

.field public static final DEVICE_INFO_MESG_DEVICE_TYPE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final DEVICE_INFO_MESG_DEVICE_TYPE_FIELD_SUBFIELDS:I = 0x2

.field public static final EVENT_MESG_DATA_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final EVENT_MESG_DATA_FIELD_BATTERY_LEVEL:I = 0x2

.field public static final EVENT_MESG_DATA_FIELD_CAD_HIGH_ALERT:I = 0x8

.field public static final EVENT_MESG_DATA_FIELD_CAD_LOW_ALERT:I = 0x9

.field public static final EVENT_MESG_DATA_FIELD_CALORIE_DURATION_ALERT:I = 0xe

.field public static final EVENT_MESG_DATA_FIELD_COURSE_POINT_INDEX:I = 0x1

.field public static final EVENT_MESG_DATA_FIELD_DISTANCE_DURATION_ALERT:I = 0xd

.field public static final EVENT_MESG_DATA_FIELD_FITNESS_EQUIPMENT_STATE:I = 0xf

.field public static final EVENT_MESG_DATA_FIELD_GEAR_CHANGE_DATA:I = 0x11

.field public static final EVENT_MESG_DATA_FIELD_HR_HIGH_ALERT:I = 0x4

.field public static final EVENT_MESG_DATA_FIELD_HR_LOW_ALERT:I = 0x5

.field public static final EVENT_MESG_DATA_FIELD_MAIN_FIELD:I = 0xffff

.field public static final EVENT_MESG_DATA_FIELD_POWER_HIGH_ALERT:I = 0xa

.field public static final EVENT_MESG_DATA_FIELD_POWER_LOW_ALERT:I = 0xb

.field public static final EVENT_MESG_DATA_FIELD_SPEED_HIGH_ALERT:I = 0x6

.field public static final EVENT_MESG_DATA_FIELD_SPEED_LOW_ALERT:I = 0x7

.field public static final EVENT_MESG_DATA_FIELD_SPORT_POINT:I = 0x10

.field public static final EVENT_MESG_DATA_FIELD_SUBFIELDS:I = 0x12

.field public static final EVENT_MESG_DATA_FIELD_TIMER_TRIGGER:I = 0x0

.field public static final EVENT_MESG_DATA_FIELD_TIME_DURATION_ALERT:I = 0xc

.field public static final EVENT_MESG_DATA_FIELD_VIRTUAL_PARTNER_SPEED:I = 0x3

.field public static final FILE_ID_MESG_PRODUCT_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final FILE_ID_MESG_PRODUCT_FIELD_GARMIN_PRODUCT:I = 0x0

.field public static final FILE_ID_MESG_PRODUCT_FIELD_MAIN_FIELD:I = 0xffff

.field public static final FILE_ID_MESG_PRODUCT_FIELD_SUBFIELDS:I = 0x1

.field public static final LAP_MESG_AVG_CADENCE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final LAP_MESG_AVG_CADENCE_FIELD_AVG_RUNNING_CADENCE:I = 0x0

.field public static final LAP_MESG_AVG_CADENCE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final LAP_MESG_AVG_CADENCE_FIELD_SUBFIELDS:I = 0x1

.field public static final LAP_MESG_MAX_CADENCE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final LAP_MESG_MAX_CADENCE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final LAP_MESG_MAX_CADENCE_FIELD_MAX_RUNNING_CADENCE:I = 0x0

.field public static final LAP_MESG_MAX_CADENCE_FIELD_SUBFIELDS:I = 0x1

.field public static final LAP_MESG_TOTAL_CYCLES_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final LAP_MESG_TOTAL_CYCLES_FIELD_MAIN_FIELD:I = 0xffff

.field public static final LAP_MESG_TOTAL_CYCLES_FIELD_SUBFIELDS:I = 0x1

.field public static final LAP_MESG_TOTAL_CYCLES_FIELD_TOTAL_STRIDES:I = 0x0

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_MAIN_FIELD:I = 0xffff

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_MAX_PER_FILE:I = 0x1

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_MAX_PER_FILE_TYPE:I = 0x2

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_NUM_PER_FILE:I = 0x0

.field public static final MESG_CAPABILITIES_MESG_COUNT_FIELD_SUBFIELDS:I = 0x3

.field public static final MONITORING_MESG_CYCLES_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final MONITORING_MESG_CYCLES_FIELD_MAIN_FIELD:I = 0xffff

.field public static final MONITORING_MESG_CYCLES_FIELD_STEPS:I = 0x0

.field public static final MONITORING_MESG_CYCLES_FIELD_STROKES:I = 0x1

.field public static final MONITORING_MESG_CYCLES_FIELD_SUBFIELDS:I = 0x2

.field public static final SCHEDULE_MESG_PRODUCT_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final SCHEDULE_MESG_PRODUCT_FIELD_GARMIN_PRODUCT:I = 0x0

.field public static final SCHEDULE_MESG_PRODUCT_FIELD_MAIN_FIELD:I = 0xffff

.field public static final SCHEDULE_MESG_PRODUCT_FIELD_SUBFIELDS:I = 0x1

.field public static final SESSION_MESG_AVG_CADENCE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final SESSION_MESG_AVG_CADENCE_FIELD_AVG_RUNNING_CADENCE:I = 0x0

.field public static final SESSION_MESG_AVG_CADENCE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final SESSION_MESG_AVG_CADENCE_FIELD_SUBFIELDS:I = 0x1

.field public static final SESSION_MESG_MAX_CADENCE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final SESSION_MESG_MAX_CADENCE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final SESSION_MESG_MAX_CADENCE_FIELD_MAX_RUNNING_CADENCE:I = 0x0

.field public static final SESSION_MESG_MAX_CADENCE_FIELD_SUBFIELDS:I = 0x1

.field public static final SESSION_MESG_TOTAL_CYCLES_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final SESSION_MESG_TOTAL_CYCLES_FIELD_MAIN_FIELD:I = 0xffff

.field public static final SESSION_MESG_TOTAL_CYCLES_FIELD_SUBFIELDS:I = 0x1

.field public static final SESSION_MESG_TOTAL_CYCLES_FIELD_TOTAL_STRIDES:I = 0x0

.field public static final SLAVE_DEVICE_MESG_PRODUCT_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final SLAVE_DEVICE_MESG_PRODUCT_FIELD_GARMIN_PRODUCT:I = 0x0

.field public static final SLAVE_DEVICE_MESG_PRODUCT_FIELD_MAIN_FIELD:I = 0xffff

.field public static final SLAVE_DEVICE_MESG_PRODUCT_FIELD_SUBFIELDS:I = 0x1

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_CUSTOM_TARGET_CADENCE_HIGH:I = 0x2

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_CUSTOM_TARGET_HEART_RATE_HIGH:I = 0x1

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_CUSTOM_TARGET_POWER_HIGH:I = 0x3

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_CUSTOM_TARGET_SPEED_HIGH:I = 0x0

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_MAIN_FIELD:I = 0xffff

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_HIGH_FIELD_SUBFIELDS:I = 0x4

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_CUSTOM_TARGET_CADENCE_LOW:I = 0x2

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_CUSTOM_TARGET_HEART_RATE_LOW:I = 0x1

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_CUSTOM_TARGET_POWER_LOW:I = 0x3

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_CUSTOM_TARGET_SPEED_LOW:I = 0x0

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_MAIN_FIELD:I = 0xffff

.field public static final WORKOUT_STEP_MESG_CUSTOM_TARGET_VALUE_LOW_FIELD_SUBFIELDS:I = 0x4

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_CALORIES:I = 0x3

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_DISTANCE:I = 0x1

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_HR:I = 0x2

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_POWER:I = 0x5

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_STEP:I = 0x4

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_DURATION_TIME:I = 0x0

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final WORKOUT_STEP_MESG_DURATION_VALUE_FIELD_SUBFIELDS:I = 0x6

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_ACTIVE_SUBFIELD:I = 0xfffe

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_MAIN_FIELD:I = 0xffff

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_CALORIES:I = 0x5

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_DISTANCE:I = 0x4

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_HR:I = 0x6

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_POWER:I = 0x7

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_STEPS:I = 0x2

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_REPEAT_TIME:I = 0x3

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_SUBFIELDS:I = 0x8

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_TARGET_HR_ZONE:I = 0x0

.field public static final WORKOUT_STEP_MESG_TARGET_VALUE_FIELD_TARGET_POWER_ZONE:I = 0x1


# instance fields
.field final synthetic this$0:Lcom/garmin/fit/Profile;


# direct methods
.method public constructor <init>(Lcom/garmin/fit/Profile;)V
    .locals 0

    .prologue
    .line 21
    iput-object p1, p0, Lcom/garmin/fit/Profile$SubFields;->this$0:Lcom/garmin/fit/Profile;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
