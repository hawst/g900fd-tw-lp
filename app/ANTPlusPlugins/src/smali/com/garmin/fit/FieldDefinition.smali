.class public Lcom/garmin/fit/FieldDefinition;
.super Ljava/lang/Object;
.source "FieldDefinition.java"


# instance fields
.field protected num:I

.field protected size:I

.field protected type:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    const/16 v0, 0xff

    iput v0, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    .line 29
    const/4 v0, 0x0

    iput v0, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    .line 30
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Field;)V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    iget v0, p1, Lcom/garmin/fit/Field;->num:I

    iput v0, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    .line 34
    invoke-virtual {p1}, Lcom/garmin/fit/Field;->getSize()I

    move-result v0

    iput v0, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    .line 35
    iget v0, p1, Lcom/garmin/fit/Field;->type:I

    iput v0, p0, Lcom/garmin/fit/FieldDefinition;->type:I

    .line 36
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 64
    if-ne p0, p1, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    .line 67
    :cond_1
    instance-of v2, p1, Lcom/garmin/fit/FieldDefinition;

    if-nez v2, :cond_2

    move v0, v1

    .line 68
    goto :goto_0

    .line 70
    :cond_2
    check-cast p1, Lcom/garmin/fit/FieldDefinition;

    .line 72
    iget v2, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    iget v3, p1, Lcom/garmin/fit/FieldDefinition;->num:I

    if-eq v2, v3, :cond_3

    move v0, v1

    .line 73
    goto :goto_0

    .line 75
    :cond_3
    iget v2, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    iget v3, p1, Lcom/garmin/fit/FieldDefinition;->size:I

    if-eq v2, v3, :cond_4

    move v0, v1

    .line 76
    goto :goto_0

    .line 78
    :cond_4
    iget v2, p0, Lcom/garmin/fit/FieldDefinition;->type:I

    iget v3, p1, Lcom/garmin/fit/FieldDefinition;->type:I

    if-eq v2, v3, :cond_0

    move v0, v1

    .line 79
    goto :goto_0
.end method

.method public getNum()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    return v0
.end method

.method public getSize()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    return v0
.end method

.method public getType()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->type:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 85
    .line 87
    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x2f

    .line 88
    mul-int/lit8 v0, v0, 0x1f

    new-instance v1, Ljava/lang/Integer;

    iget v2, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 89
    mul-int/lit8 v0, v0, 0x13

    new-instance v1, Ljava/lang/Integer;

    iget v2, p0, Lcom/garmin/fit/FieldDefinition;->type:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 91
    return v0
.end method

.method public setSize(I)V
    .locals 0

    .prologue
    .line 52
    iput p1, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    .line 53
    return-void
.end method

.method protected write(Ljava/io/OutputStream;)V
    .locals 1

    .prologue
    .line 40
    :try_start_0
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->num:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 41
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->size:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 42
    iget v0, p0, Lcom/garmin/fit/FieldDefinition;->type:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 45
    :goto_0
    return-void

    .line 43
    :catch_0
    move-exception v0

    goto :goto_0
.end method
