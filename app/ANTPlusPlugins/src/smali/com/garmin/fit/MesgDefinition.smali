.class public Lcom/garmin/fit/MesgDefinition;
.super Ljava/lang/Object;
.source "MesgDefinition.java"


# instance fields
.field protected arch:I

.field protected fields:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/FieldDefinition;",
            ">;"
        }
    .end annotation
.end field

.field protected localNum:I

.field protected num:I


# direct methods
.method protected constructor <init>()V
    .locals 1

    .prologue
    .line 29
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    sget v0, Lcom/garmin/fit/MesgNum;->INVALID:I

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    .line 31
    const/4 v0, 0x0

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    .line 32
    const/4 v0, 0x1

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->arch:I

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    .line 34
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 4

    .prologue
    const/16 v3, 0x10

    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    iget v0, p1, Lcom/garmin/fit/Mesg;->num:I

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    .line 38
    iget v0, p1, Lcom/garmin/fit/Mesg;->localNum:I

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    .line 39
    const/4 v0, 0x1

    iput v0, p0, Lcom/garmin/fit/MesgDefinition;->arch:I

    .line 41
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    if-lt v0, v3, :cond_0

    .line 42
    new-instance v0, Lcom/garmin/fit/FitRuntimeException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid local message number "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".  Local message number must be < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/garmin/fit/FitRuntimeException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 44
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    .line 46
    iget-object v0, p1, Lcom/garmin/fit/Mesg;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Field;

    .line 47
    iget-object v2, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    new-instance v3, Lcom/garmin/fit/FieldDefinition;

    invoke-direct {v3, v0}, Lcom/garmin/fit/FieldDefinition;-><init>(Lcom/garmin/fit/Field;)V

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 49
    :cond_1
    return-void
.end method


# virtual methods
.method public addField(Lcom/garmin/fit/FieldDefinition;)V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 78
    return-void
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 126
    if-ne p0, p1, :cond_1

    move v2, v3

    .line 148
    :cond_0
    :goto_0
    return v2

    .line 129
    :cond_1
    instance-of v0, p1, Lcom/garmin/fit/MesgDefinition;

    if-eqz v0, :cond_0

    .line 132
    check-cast p1, Lcom/garmin/fit/MesgDefinition;

    .line 134
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    iget v1, p1, Lcom/garmin/fit/MesgDefinition;->num:I

    if-ne v0, v1, :cond_0

    .line 137
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    iget v1, p1, Lcom/garmin/fit/MesgDefinition;->localNum:I

    if-ne v0, v1, :cond_0

    .line 140
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    iget-object v1, p1, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ne v0, v1, :cond_0

    move v1, v2

    .line 143
    :goto_1
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge v1, v0, :cond_2

    .line 144
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    iget-object v4, p1, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/garmin/fit/FieldDefinition;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 143
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_1

    :cond_2
    move v2, v3

    .line 148
    goto :goto_0
.end method

.method public getArch()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->arch:I

    return v0
.end method

.method public getField(I)Lcom/garmin/fit/FieldDefinition;
    .locals 3

    .prologue
    .line 68
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    .line 69
    iget v2, v0, Lcom/garmin/fit/FieldDefinition;->num:I

    if-ne v2, p1, :cond_0

    .line 73
    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getFields()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/FieldDefinition;",
            ">;"
        }
    .end annotation

    .prologue
    .line 64
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getLocalNum()I
    .locals 1

    .prologue
    .line 56
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    return v0
.end method

.method public getNum()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 152
    .line 154
    new-instance v0, Ljava/lang/Integer;

    iget v1, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    invoke-direct {v0, v1}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v0}, Ljava/lang/Integer;->hashCode()I

    move-result v0

    add-int/lit8 v0, v0, 0x1f

    .line 155
    mul-int/lit8 v0, v0, 0x2f

    new-instance v1, Ljava/lang/Integer;

    iget v2, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    invoke-direct {v1, v2}, Ljava/lang/Integer;-><init>(I)V

    invoke-virtual {v1}, Ljava/lang/Integer;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 156
    mul-int/lit8 v0, v0, 0x13

    iget-object v1, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->hashCode()I

    move-result v1

    add-int/2addr v0, v1

    .line 158
    return v0
.end method

.method public supports(Lcom/garmin/fit/Mesg;)Z
    .locals 1

    .prologue
    .line 99
    new-instance v0, Lcom/garmin/fit/MesgDefinition;

    invoke-direct {v0, p1}, Lcom/garmin/fit/MesgDefinition;-><init>(Lcom/garmin/fit/Mesg;)V

    invoke-virtual {p0, v0}, Lcom/garmin/fit/MesgDefinition;->supports(Lcom/garmin/fit/MesgDefinition;)Z

    move-result v0

    return v0
.end method

.method public supports(Lcom/garmin/fit/MesgDefinition;)Z
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 103
    if-nez p1, :cond_0

    move v0, v1

    .line 122
    :goto_0
    return v0

    .line 106
    :cond_0
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    iget v2, p1, Lcom/garmin/fit/MesgDefinition;->num:I

    if-eq v0, v2, :cond_1

    move v0, v1

    .line 107
    goto :goto_0

    .line 109
    :cond_1
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    iget v2, p1, Lcom/garmin/fit/MesgDefinition;->localNum:I

    if-eq v0, v2, :cond_2

    move v0, v1

    .line 110
    goto :goto_0

    .line 112
    :cond_2
    iget-object v0, p1, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_3
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_5

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    .line 113
    iget v3, v0, Lcom/garmin/fit/FieldDefinition;->num:I

    invoke-virtual {p0, v3}, Lcom/garmin/fit/MesgDefinition;->getField(I)Lcom/garmin/fit/FieldDefinition;

    move-result-object v3

    .line 115
    if-nez v3, :cond_4

    move v0, v1

    .line 116
    goto :goto_0

    .line 118
    :cond_4
    iget v0, v0, Lcom/garmin/fit/FieldDefinition;->size:I

    iget v3, v3, Lcom/garmin/fit/FieldDefinition;->size:I

    if-le v0, v3, :cond_3

    move v0, v1

    .line 119
    goto :goto_0

    .line 122
    :cond_5
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public write(Ljava/io/OutputStream;)V
    .locals 2

    .prologue
    .line 83
    :try_start_0
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->localNum:I

    and-int/lit8 v0, v0, 0xf

    or-int/lit8 v0, v0, 0x40

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 84
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 85
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 86
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    shr-int/lit8 v0, v0, 0x8

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 87
    iget v0, p0, Lcom/garmin/fit/MesgDefinition;->num:I

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V

    .line 88
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    invoke-virtual {p1, v0}, Ljava/io/OutputStream;->write(I)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0

    .line 92
    :goto_0
    iget-object v0, p0, Lcom/garmin/fit/MesgDefinition;->fields:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/FieldDefinition;

    .line 93
    invoke-virtual {v0, p1}, Lcom/garmin/fit/FieldDefinition;->write(Ljava/io/OutputStream;)V

    goto :goto_1

    .line 96
    :cond_0
    return-void

    .line 89
    :catch_0
    move-exception v0

    goto :goto_0
.end method
