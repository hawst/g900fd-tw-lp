.class public Lcom/garmin/fit/CRC;
.super Ljava/lang/Object;
.source "CRC.java"


# static fields
.field private static final crc16_table:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const/16 v0, 0x10

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/garmin/fit/CRC;->crc16_table:[I

    return-void

    :array_0
    .array-data 4
        0x0
        0xcc01
        0xd801
        0x1400
        0xf001
        0x3c00
        0x2800
        0xe401
        0xa001
        0x6c00
        0x7800
        0xb401
        0x5000
        0x9c01
        0x8801
        0x4400
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static final get16(IB)I
    .locals 3

    .prologue
    .line 27
    sget-object v0, Lcom/garmin/fit/CRC;->crc16_table:[I

    and-int/lit8 v1, p0, 0xf

    aget v0, v0, v1

    .line 28
    shr-int/lit8 v1, p0, 0x4

    and-int/lit16 v1, v1, 0xfff

    .line 29
    xor-int/2addr v0, v1

    sget-object v1, Lcom/garmin/fit/CRC;->crc16_table:[I

    and-int/lit8 v2, p1, 0xf

    aget v1, v1, v2

    xor-int/2addr v0, v1

    .line 32
    sget-object v1, Lcom/garmin/fit/CRC;->crc16_table:[I

    and-int/lit8 v2, v0, 0xf

    aget v1, v1, v2

    .line 33
    shr-int/lit8 v0, v0, 0x4

    and-int/lit16 v0, v0, 0xfff

    .line 34
    xor-int/2addr v0, v1

    sget-object v1, Lcom/garmin/fit/CRC;->crc16_table:[I

    shr-int/lit8 v2, p1, 0x4

    and-int/lit8 v2, v2, 0xf

    aget v1, v1, v2

    xor-int/2addr v0, v1

    .line 36
    return v0
.end method
