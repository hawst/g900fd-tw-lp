.class public Lcom/garmin/fit/UserProfileMesg;
.super Lcom/garmin/fit/Mesg;
.source "UserProfileMesg.java"


# static fields
.field protected static final userProfileMesg:Lcom/garmin/fit/Mesg;


# direct methods
.method static constructor <clinit>()V
    .locals 11

    .prologue
    .line 26
    new-instance v0, Lcom/garmin/fit/Mesg;

    const-string v1, "user_profile"

    const/4 v2, 0x3

    invoke-direct {v0, v1, v2}, Lcom/garmin/fit/Mesg;-><init>(Ljava/lang/String;I)V

    sput-object v0, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    .line 27
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "message_index"

    const/16 v2, 0xfe

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 29
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "friendly_name"

    const/4 v2, 0x0

    const/4 v3, 0x7

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 31
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "gender"

    const/4 v2, 0x1

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 33
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "age"

    const/4 v2, 0x2

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "years"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 35
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "height"

    const/4 v2, 0x3

    const/4 v3, 0x2

    const-wide/high16 v4, 0x4059000000000000L    # 100.0

    const-wide/16 v6, 0x0

    const-string v8, "m"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 37
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "weight"

    const/4 v2, 0x4

    const/16 v3, 0x84

    const-wide/high16 v4, 0x4024000000000000L    # 10.0

    const-wide/16 v6, 0x0

    const-string v8, "kg"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 39
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "language"

    const/4 v2, 0x5

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 41
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "elev_setting"

    const/4 v2, 0x6

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 43
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "weight_setting"

    const/4 v2, 0x7

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 45
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "resting_heart_rate"

    const/16 v2, 0x8

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 47
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "default_max_running_heart_rate"

    const/16 v2, 0x9

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 49
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "default_max_biking_heart_rate"

    const/16 v2, 0xa

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 51
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "default_max_heart_rate"

    const/16 v2, 0xb

    const/4 v3, 0x2

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, "bpm"

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 53
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "hr_setting"

    const/16 v2, 0xc

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 55
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "speed_setting"

    const/16 v2, 0xd

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 57
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "dist_setting"

    const/16 v2, 0xe

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 59
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "power_setting"

    const/16 v2, 0x10

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 61
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "activity_class"

    const/16 v2, 0x11

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 63
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "position_setting"

    const/16 v2, 0x12

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 65
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "temperature_setting"

    const/16 v2, 0x15

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 67
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "local_id"

    const/16 v2, 0x16

    const/16 v3, 0x84

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 69
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "global_id"

    const/16 v2, 0x17

    const/16 v3, 0xd

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 71
    sget-object v10, Lcom/garmin/fit/UserProfileMesg;->userProfileMesg:Lcom/garmin/fit/Mesg;

    new-instance v0, Lcom/garmin/fit/Field;

    const-string v1, "height_setting"

    const/16 v2, 0x1e

    const/4 v3, 0x0

    const-wide/high16 v4, 0x3ff0000000000000L    # 1.0

    const-wide/16 v6, 0x0

    const-string v8, ""

    const/4 v9, 0x0

    invoke-direct/range {v0 .. v9}, Lcom/garmin/fit/Field;-><init>(Ljava/lang/String;IIDDLjava/lang/String;Z)V

    invoke-virtual {v10, v0}, Lcom/garmin/fit/Mesg;->addField(Lcom/garmin/fit/Field;)V

    .line 73
    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 76
    const/4 v0, 0x3

    invoke-static {v0}, Lcom/garmin/fit/Factory;->createMesg(I)Lcom/garmin/fit/Mesg;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 77
    return-void
.end method

.method public constructor <init>(Lcom/garmin/fit/Mesg;)V
    .locals 0

    .prologue
    .line 80
    invoke-direct {p0, p1}, Lcom/garmin/fit/Mesg;-><init>(Lcom/garmin/fit/Mesg;)V

    .line 81
    return-void
.end method


# virtual methods
.method public getActivityClass()Lcom/garmin/fit/ActivityClass;
    .locals 3

    .prologue
    .line 434
    const/16 v0, 0x11

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 435
    if-nez v0, :cond_0

    .line 436
    const/4 v0, 0x0

    .line 437
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/ActivityClass;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/ActivityClass;

    move-result-object v0

    goto :goto_0
.end method

.method public getAge()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 148
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultMaxBikingHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 311
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultMaxHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 331
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getDefaultMaxRunningHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 291
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getDistSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 392
    const/16 v0, 0xe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 393
    if-nez v0, :cond_0

    .line 394
    const/4 v0, 0x0

    .line 395
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getElevSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 228
    const/4 v0, 0x6

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 229
    if-nez v0, :cond_0

    .line 230
    const/4 v0, 0x0

    .line 231
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getFriendlyName()Ljava/lang/String;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 108
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, v0}, Lcom/garmin/fit/UserProfileMesg;->getFieldStringValue(III)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGender()Lcom/garmin/fit/Gender;
    .locals 3

    .prologue
    .line 126
    const/4 v0, 0x1

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 127
    if-nez v0, :cond_0

    .line 128
    const/4 v0, 0x0

    .line 129
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Gender;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Gender;

    move-result-object v0

    goto :goto_0
.end method

.method public getGlobalId(I)Ljava/lang/Byte;
    .locals 2

    .prologue
    .line 523
    const/16 v0, 0x17

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, v1}, Lcom/garmin/fit/UserProfileMesg;->getFieldByteValue(III)Ljava/lang/Byte;

    move-result-object v0

    return-object v0
.end method

.method public getHeight()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 168
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getHeightSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 542
    const/16 v0, 0x1e

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 543
    if-nez v0, :cond_0

    .line 544
    const/4 v0, 0x0

    .line 545
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getHrSetting()Lcom/garmin/fit/DisplayHeart;
    .locals 3

    .prologue
    .line 350
    const/16 v0, 0xc

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 351
    if-nez v0, :cond_0

    .line 352
    const/4 v0, 0x0

    .line 353
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayHeart;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayHeart;

    move-result-object v0

    goto :goto_0
.end method

.method public getLanguage()Lcom/garmin/fit/Language;
    .locals 3

    .prologue
    .line 207
    const/4 v0, 0x5

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 208
    if-nez v0, :cond_0

    .line 209
    const/4 v0, 0x0

    .line 210
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/Language;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Language;

    move-result-object v0

    goto :goto_0
.end method

.method public getLocalId()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 497
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getMessageIndex()Ljava/lang/Integer;
    .locals 3

    .prologue
    .line 90
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldIntegerValue(III)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public getNumGlobalId()I
    .locals 2

    .prologue
    .line 513
    const/16 v0, 0x17

    const v1, 0xffff

    invoke-virtual {p0, v0, v1}, Lcom/garmin/fit/UserProfileMesg;->getNumFieldValues(II)I

    move-result v0

    return v0
.end method

.method public getPositionSetting()Lcom/garmin/fit/DisplayPosition;
    .locals 3

    .prologue
    .line 455
    const/16 v0, 0x12

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 456
    if-nez v0, :cond_0

    .line 457
    const/4 v0, 0x0

    .line 458
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayPosition;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayPosition;

    move-result-object v0

    goto :goto_0
.end method

.method public getPowerSetting()Lcom/garmin/fit/DisplayPower;
    .locals 3

    .prologue
    .line 413
    const/16 v0, 0x10

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 414
    if-nez v0, :cond_0

    .line 415
    const/4 v0, 0x0

    .line 416
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayPower;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayPower;

    move-result-object v0

    goto :goto_0
.end method

.method public getRestingHeartRate()Ljava/lang/Short;
    .locals 3

    .prologue
    .line 271
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    return-object v0
.end method

.method public getSpeedSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 371
    const/16 v0, 0xd

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 372
    if-nez v0, :cond_0

    .line 373
    const/4 v0, 0x0

    .line 374
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getTemperatureSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 476
    const/16 v0, 0x15

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 477
    if-nez v0, :cond_0

    .line 478
    const/4 v0, 0x0

    .line 479
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public getWeight()Ljava/lang/Float;
    .locals 3

    .prologue
    .line 188
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldFloatValue(III)Ljava/lang/Float;

    move-result-object v0

    return-object v0
.end method

.method public getWeightSetting()Lcom/garmin/fit/DisplayMeasure;
    .locals 3

    .prologue
    .line 249
    const/4 v0, 0x7

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, v2}, Lcom/garmin/fit/UserProfileMesg;->getFieldShortValue(III)Ljava/lang/Short;

    move-result-object v0

    .line 250
    if-nez v0, :cond_0

    .line 251
    const/4 v0, 0x0

    .line 252
    :goto_0
    return-object v0

    :cond_0
    invoke-static {v0}, Lcom/garmin/fit/DisplayMeasure;->getByValue(Ljava/lang/Short;)Lcom/garmin/fit/DisplayMeasure;

    move-result-object v0

    goto :goto_0
.end method

.method public setActivityClass(Lcom/garmin/fit/ActivityClass;)V
    .locals 4

    .prologue
    .line 446
    const/16 v0, 0x11

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/ActivityClass;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 447
    return-void
.end method

.method public setAge(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 158
    const/4 v0, 0x2

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 159
    return-void
.end method

.method public setDefaultMaxBikingHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 321
    const/16 v0, 0xa

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 322
    return-void
.end method

.method public setDefaultMaxHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 341
    const/16 v0, 0xb

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 342
    return-void
.end method

.method public setDefaultMaxRunningHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 301
    const/16 v0, 0x9

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 302
    return-void
.end method

.method public setDistSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 404
    const/16 v0, 0xe

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 405
    return-void
.end method

.method public setElevSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 240
    const/4 v0, 0x6

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 241
    return-void
.end method

.method public setFriendlyName(Ljava/lang/String;)V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 117
    const v0, 0xffff

    invoke-virtual {p0, v1, v1, p1, v0}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 118
    return-void
.end method

.method public setGender(Lcom/garmin/fit/Gender;)V
    .locals 4

    .prologue
    .line 138
    const/4 v0, 0x1

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Gender;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 139
    return-void
.end method

.method public setGlobalId(ILjava/lang/Byte;)V
    .locals 2

    .prologue
    .line 533
    const/16 v0, 0x17

    const v1, 0xffff

    invoke-virtual {p0, v0, p1, p2, v1}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 534
    return-void
.end method

.method public setHeight(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 178
    const/4 v0, 0x3

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 179
    return-void
.end method

.method public setHeightSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 554
    const/16 v0, 0x1e

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 555
    return-void
.end method

.method public setHrSetting(Lcom/garmin/fit/DisplayHeart;)V
    .locals 4

    .prologue
    .line 362
    const/16 v0, 0xc

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayHeart;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 363
    return-void
.end method

.method public setLanguage(Lcom/garmin/fit/Language;)V
    .locals 4

    .prologue
    .line 219
    const/4 v0, 0x5

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/Language;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 220
    return-void
.end method

.method public setLocalId(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 506
    const/16 v0, 0x16

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 507
    return-void
.end method

.method public setMessageIndex(Ljava/lang/Integer;)V
    .locals 3

    .prologue
    .line 99
    const/16 v0, 0xfe

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 100
    return-void
.end method

.method public setPositionSetting(Lcom/garmin/fit/DisplayPosition;)V
    .locals 4

    .prologue
    .line 467
    const/16 v0, 0x12

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayPosition;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 468
    return-void
.end method

.method public setPowerSetting(Lcom/garmin/fit/DisplayPower;)V
    .locals 4

    .prologue
    .line 425
    const/16 v0, 0x10

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayPower;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 426
    return-void
.end method

.method public setRestingHeartRate(Ljava/lang/Short;)V
    .locals 3

    .prologue
    .line 281
    const/16 v0, 0x8

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 282
    return-void
.end method

.method public setSpeedSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 383
    const/16 v0, 0xd

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 384
    return-void
.end method

.method public setTemperatureSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 488
    const/16 v0, 0x15

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 489
    return-void
.end method

.method public setWeight(Ljava/lang/Float;)V
    .locals 3

    .prologue
    .line 198
    const/4 v0, 0x4

    const/4 v1, 0x0

    const v2, 0xffff

    invoke-virtual {p0, v0, v1, p1, v2}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 199
    return-void
.end method

.method public setWeightSetting(Lcom/garmin/fit/DisplayMeasure;)V
    .locals 4

    .prologue
    .line 261
    const/4 v0, 0x7

    const/4 v1, 0x0

    iget-short v2, p1, Lcom/garmin/fit/DisplayMeasure;->value:S

    invoke-static {v2}, Ljava/lang/Short;->valueOf(S)Ljava/lang/Short;

    move-result-object v2

    const v3, 0xffff

    invoke-virtual {p0, v0, v1, v2, v3}, Lcom/garmin/fit/UserProfileMesg;->setFieldValue(IILjava/lang/Object;I)V

    .line 262
    return-void
.end method
