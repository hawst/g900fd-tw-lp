.class public final enum Lcom/garmin/fit/Language;
.super Ljava/lang/Enum;
.source "Language.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/Language;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/Language;

.field public static final enum ARABIC:Lcom/garmin/fit/Language;

.field public static final enum BULGARIAN:Lcom/garmin/fit/Language;

.field public static final enum CROATIAN:Lcom/garmin/fit/Language;

.field public static final enum CUSTOM:Lcom/garmin/fit/Language;

.field public static final enum CZECH:Lcom/garmin/fit/Language;

.field public static final enum DANISH:Lcom/garmin/fit/Language;

.field public static final enum DUTCH:Lcom/garmin/fit/Language;

.field public static final enum ENGLISH:Lcom/garmin/fit/Language;

.field public static final enum FARSI:Lcom/garmin/fit/Language;

.field public static final enum FINNISH:Lcom/garmin/fit/Language;

.field public static final enum FRENCH:Lcom/garmin/fit/Language;

.field public static final enum GERMAN:Lcom/garmin/fit/Language;

.field public static final enum GREEK:Lcom/garmin/fit/Language;

.field public static final enum HUNGARIAN:Lcom/garmin/fit/Language;

.field public static final enum INVALID:Lcom/garmin/fit/Language;

.field public static final enum ITALIAN:Lcom/garmin/fit/Language;

.field public static final enum LATVIAN:Lcom/garmin/fit/Language;

.field public static final enum NORWEGIAN:Lcom/garmin/fit/Language;

.field public static final enum POLISH:Lcom/garmin/fit/Language;

.field public static final enum PORTUGUESE:Lcom/garmin/fit/Language;

.field public static final enum ROMANIAN:Lcom/garmin/fit/Language;

.field public static final enum RUSSIAN:Lcom/garmin/fit/Language;

.field public static final enum SLOVAKIAN:Lcom/garmin/fit/Language;

.field public static final enum SLOVENIAN:Lcom/garmin/fit/Language;

.field public static final enum SPANISH:Lcom/garmin/fit/Language;

.field public static final enum SWEDISH:Lcom/garmin/fit/Language;

.field public static final enum TURKISH:Lcom/garmin/fit/Language;

.field public static final enum UKRAINIAN:Lcom/garmin/fit/Language;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "ENGLISH"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->ENGLISH:Lcom/garmin/fit/Language;

    .line 22
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "FRENCH"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->FRENCH:Lcom/garmin/fit/Language;

    .line 23
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "ITALIAN"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->ITALIAN:Lcom/garmin/fit/Language;

    .line 24
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "GERMAN"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->GERMAN:Lcom/garmin/fit/Language;

    .line 25
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "SPANISH"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->SPANISH:Lcom/garmin/fit/Language;

    .line 26
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "CROATIAN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->CROATIAN:Lcom/garmin/fit/Language;

    .line 27
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "CZECH"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->CZECH:Lcom/garmin/fit/Language;

    .line 28
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "DANISH"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->DANISH:Lcom/garmin/fit/Language;

    .line 29
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "DUTCH"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->DUTCH:Lcom/garmin/fit/Language;

    .line 30
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "FINNISH"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->FINNISH:Lcom/garmin/fit/Language;

    .line 31
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "GREEK"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->GREEK:Lcom/garmin/fit/Language;

    .line 32
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "HUNGARIAN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->HUNGARIAN:Lcom/garmin/fit/Language;

    .line 33
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "NORWEGIAN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->NORWEGIAN:Lcom/garmin/fit/Language;

    .line 34
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "POLISH"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->POLISH:Lcom/garmin/fit/Language;

    .line 35
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "PORTUGUESE"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->PORTUGUESE:Lcom/garmin/fit/Language;

    .line 36
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "SLOVAKIAN"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->SLOVAKIAN:Lcom/garmin/fit/Language;

    .line 37
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "SLOVENIAN"

    const/16 v2, 0x10

    const/16 v3, 0x10

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->SLOVENIAN:Lcom/garmin/fit/Language;

    .line 38
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "SWEDISH"

    const/16 v2, 0x11

    const/16 v3, 0x11

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->SWEDISH:Lcom/garmin/fit/Language;

    .line 39
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "RUSSIAN"

    const/16 v2, 0x12

    const/16 v3, 0x12

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->RUSSIAN:Lcom/garmin/fit/Language;

    .line 40
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "TURKISH"

    const/16 v2, 0x13

    const/16 v3, 0x13

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->TURKISH:Lcom/garmin/fit/Language;

    .line 41
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "LATVIAN"

    const/16 v2, 0x14

    const/16 v3, 0x14

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->LATVIAN:Lcom/garmin/fit/Language;

    .line 42
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "UKRAINIAN"

    const/16 v2, 0x15

    const/16 v3, 0x15

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->UKRAINIAN:Lcom/garmin/fit/Language;

    .line 43
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "ARABIC"

    const/16 v2, 0x16

    const/16 v3, 0x16

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->ARABIC:Lcom/garmin/fit/Language;

    .line 44
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "FARSI"

    const/16 v2, 0x17

    const/16 v3, 0x17

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->FARSI:Lcom/garmin/fit/Language;

    .line 45
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "BULGARIAN"

    const/16 v2, 0x18

    const/16 v3, 0x18

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->BULGARIAN:Lcom/garmin/fit/Language;

    .line 46
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "ROMANIAN"

    const/16 v2, 0x19

    const/16 v3, 0x19

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->ROMANIAN:Lcom/garmin/fit/Language;

    .line 47
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "CUSTOM"

    const/16 v2, 0x1a

    const/16 v3, 0xfe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->CUSTOM:Lcom/garmin/fit/Language;

    .line 48
    new-instance v0, Lcom/garmin/fit/Language;

    const-string v1, "INVALID"

    const/16 v2, 0x1b

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/Language;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/Language;->INVALID:Lcom/garmin/fit/Language;

    .line 20
    const/16 v0, 0x1c

    new-array v0, v0, [Lcom/garmin/fit/Language;

    sget-object v1, Lcom/garmin/fit/Language;->ENGLISH:Lcom/garmin/fit/Language;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/Language;->FRENCH:Lcom/garmin/fit/Language;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/Language;->ITALIAN:Lcom/garmin/fit/Language;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/Language;->GERMAN:Lcom/garmin/fit/Language;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/Language;->SPANISH:Lcom/garmin/fit/Language;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/Language;->CROATIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/Language;->CZECH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/Language;->DANISH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/Language;->DUTCH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/Language;->FINNISH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/Language;->GREEK:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/Language;->HUNGARIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/Language;->NORWEGIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/Language;->POLISH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/Language;->PORTUGUESE:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/Language;->SLOVAKIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/Language;->SLOVENIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x11

    sget-object v2, Lcom/garmin/fit/Language;->SWEDISH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x12

    sget-object v2, Lcom/garmin/fit/Language;->RUSSIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x13

    sget-object v2, Lcom/garmin/fit/Language;->TURKISH:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x14

    sget-object v2, Lcom/garmin/fit/Language;->LATVIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x15

    sget-object v2, Lcom/garmin/fit/Language;->UKRAINIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x16

    sget-object v2, Lcom/garmin/fit/Language;->ARABIC:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x17

    sget-object v2, Lcom/garmin/fit/Language;->FARSI:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x18

    sget-object v2, Lcom/garmin/fit/Language;->BULGARIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x19

    sget-object v2, Lcom/garmin/fit/Language;->ROMANIAN:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x1a

    sget-object v2, Lcom/garmin/fit/Language;->CUSTOM:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    const/16 v1, 0x1b

    sget-object v2, Lcom/garmin/fit/Language;->INVALID:Lcom/garmin/fit/Language;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/Language;->$VALUES:[Lcom/garmin/fit/Language;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 56
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 57
    iput-short p3, p0, Lcom/garmin/fit/Language;->value:S

    .line 58
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/Language;
    .locals 6

    .prologue
    .line 61
    invoke-static {}, Lcom/garmin/fit/Language;->values()[Lcom/garmin/fit/Language;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 62
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/Language;->value:S

    if-ne v4, v5, :cond_0

    .line 66
    :goto_1
    return-object v0

    .line 61
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 66
    :cond_1
    sget-object v0, Lcom/garmin/fit/Language;->INVALID:Lcom/garmin/fit/Language;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/Language;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/Language;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/Language;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/Language;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/Language;->$VALUES:[Lcom/garmin/fit/Language;

    invoke-virtual {v0}, [Lcom/garmin/fit/Language;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/Language;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 70
    iget-short v0, p0, Lcom/garmin/fit/Language;->value:S

    return v0
.end method
