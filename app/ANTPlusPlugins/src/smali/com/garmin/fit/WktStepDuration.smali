.class public final enum Lcom/garmin/fit/WktStepDuration;
.super Ljava/lang/Enum;
.source "WktStepDuration.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/garmin/fit/WktStepDuration;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/garmin/fit/WktStepDuration;

.field public static final enum CALORIES:Lcom/garmin/fit/WktStepDuration;

.field public static final enum DISTANCE:Lcom/garmin/fit/WktStepDuration;

.field public static final enum HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum INVALID:Lcom/garmin/fit/WktStepDuration;

.field public static final enum OPEN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_CALORIES:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_DISTANCE:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_STEPS_CMPLT:Lcom/garmin/fit/WktStepDuration;

.field public static final enum REPEAT_UNTIL_TIME:Lcom/garmin/fit/WktStepDuration;

.field public static final enum TIME:Lcom/garmin/fit/WktStepDuration;


# instance fields
.field protected value:S


# direct methods
.method static constructor <clinit>()V
    .locals 9

    .prologue
    const/4 v8, 0x4

    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 21
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "TIME"

    invoke-direct {v0, v1, v4, v4}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->TIME:Lcom/garmin/fit/WktStepDuration;

    .line 22
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "DISTANCE"

    invoke-direct {v0, v1, v5, v5}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->DISTANCE:Lcom/garmin/fit/WktStepDuration;

    .line 23
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "HR_LESS_THAN"

    invoke-direct {v0, v1, v6, v6}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 24
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "HR_GREATER_THAN"

    invoke-direct {v0, v1, v7, v7}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 25
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "CALORIES"

    invoke-direct {v0, v1, v8, v8}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->CALORIES:Lcom/garmin/fit/WktStepDuration;

    .line 26
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "OPEN"

    const/4 v2, 0x5

    const/4 v3, 0x5

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->OPEN:Lcom/garmin/fit/WktStepDuration;

    .line 27
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_STEPS_CMPLT"

    const/4 v2, 0x6

    const/4 v3, 0x6

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_STEPS_CMPLT:Lcom/garmin/fit/WktStepDuration;

    .line 28
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_TIME"

    const/4 v2, 0x7

    const/4 v3, 0x7

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_TIME:Lcom/garmin/fit/WktStepDuration;

    .line 29
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_DISTANCE"

    const/16 v2, 0x8

    const/16 v3, 0x8

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_DISTANCE:Lcom/garmin/fit/WktStepDuration;

    .line 30
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_CALORIES"

    const/16 v2, 0x9

    const/16 v3, 0x9

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_CALORIES:Lcom/garmin/fit/WktStepDuration;

    .line 31
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_HR_LESS_THAN"

    const/16 v2, 0xa

    const/16 v3, 0xa

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 32
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_HR_GREATER_THAN"

    const/16 v2, 0xb

    const/16 v3, 0xb

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 33
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_POWER_LESS_THAN"

    const/16 v2, 0xc

    const/16 v3, 0xc

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 34
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "REPEAT_UNTIL_POWER_GREATER_THAN"

    const/16 v2, 0xd

    const/16 v3, 0xd

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 35
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "POWER_LESS_THAN"

    const/16 v2, 0xe

    const/16 v3, 0xe

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 36
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "POWER_GREATER_THAN"

    const/16 v2, 0xf

    const/16 v3, 0xf

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    .line 37
    new-instance v0, Lcom/garmin/fit/WktStepDuration;

    const-string v1, "INVALID"

    const/16 v2, 0x10

    const/16 v3, 0xff

    invoke-direct {v0, v1, v2, v3}, Lcom/garmin/fit/WktStepDuration;-><init>(Ljava/lang/String;IS)V

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->INVALID:Lcom/garmin/fit/WktStepDuration;

    .line 20
    const/16 v0, 0x11

    new-array v0, v0, [Lcom/garmin/fit/WktStepDuration;

    sget-object v1, Lcom/garmin/fit/WktStepDuration;->TIME:Lcom/garmin/fit/WktStepDuration;

    aput-object v1, v0, v4

    sget-object v1, Lcom/garmin/fit/WktStepDuration;->DISTANCE:Lcom/garmin/fit/WktStepDuration;

    aput-object v1, v0, v5

    sget-object v1, Lcom/garmin/fit/WktStepDuration;->HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v1, v0, v6

    sget-object v1, Lcom/garmin/fit/WktStepDuration;->HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v1, v0, v7

    sget-object v1, Lcom/garmin/fit/WktStepDuration;->CALORIES:Lcom/garmin/fit/WktStepDuration;

    aput-object v1, v0, v8

    const/4 v1, 0x5

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->OPEN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_STEPS_CMPLT:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/4 v1, 0x7

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_TIME:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_DISTANCE:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0x9

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_CALORIES:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xa

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_HR_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xb

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_HR_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xc

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xd

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->REPEAT_UNTIL_POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xe

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->POWER_LESS_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0xf

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->POWER_GREATER_THAN:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    const/16 v1, 0x10

    sget-object v2, Lcom/garmin/fit/WktStepDuration;->INVALID:Lcom/garmin/fit/WktStepDuration;

    aput-object v2, v0, v1

    sput-object v0, Lcom/garmin/fit/WktStepDuration;->$VALUES:[Lcom/garmin/fit/WktStepDuration;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IS)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(S)V"
        }
    .end annotation

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 46
    iput-short p3, p0, Lcom/garmin/fit/WktStepDuration;->value:S

    .line 47
    return-void
.end method

.method public static getByValue(Ljava/lang/Short;)Lcom/garmin/fit/WktStepDuration;
    .locals 6

    .prologue
    .line 50
    invoke-static {}, Lcom/garmin/fit/WktStepDuration;->values()[Lcom/garmin/fit/WktStepDuration;

    move-result-object v2

    array-length v3, v2

    const/4 v0, 0x0

    move v1, v0

    :goto_0
    if-ge v1, v3, :cond_1

    aget-object v0, v2, v1

    .line 51
    invoke-virtual {p0}, Ljava/lang/Short;->shortValue()S

    move-result v4

    iget-short v5, v0, Lcom/garmin/fit/WktStepDuration;->value:S

    if-ne v4, v5, :cond_0

    .line 55
    :goto_1
    return-object v0

    .line 50
    :cond_0
    add-int/lit8 v0, v1, 0x1

    move v1, v0

    goto :goto_0

    .line 55
    :cond_1
    sget-object v0, Lcom/garmin/fit/WktStepDuration;->INVALID:Lcom/garmin/fit/WktStepDuration;

    goto :goto_1
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/garmin/fit/WktStepDuration;
    .locals 1

    .prologue
    .line 20
    const-class v0, Lcom/garmin/fit/WktStepDuration;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/WktStepDuration;

    return-object v0
.end method

.method public static values()[Lcom/garmin/fit/WktStepDuration;
    .locals 1

    .prologue
    .line 20
    sget-object v0, Lcom/garmin/fit/WktStepDuration;->$VALUES:[Lcom/garmin/fit/WktStepDuration;

    invoke-virtual {v0}, [Lcom/garmin/fit/WktStepDuration;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/garmin/fit/WktStepDuration;

    return-object v0
.end method


# virtual methods
.method public getValue()S
    .locals 1

    .prologue
    .line 59
    iget-short v0, p0, Lcom/garmin/fit/WktStepDuration;->value:S

    return v0
.end method
