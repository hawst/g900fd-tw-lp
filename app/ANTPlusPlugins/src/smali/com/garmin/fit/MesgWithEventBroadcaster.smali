.class public Lcom/garmin/fit/MesgWithEventBroadcaster;
.super Ljava/lang/Object;
.source "MesgWithEventBroadcaster.java"

# interfaces
.implements Lcom/garmin/fit/MesgWithEventListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/garmin/fit/MesgWithEventBroadcaster$1;
    }
.end annotation


# instance fields
.field private BEGIN_END_GROUP:I

.field private DEFAULT_GROUP:I

.field private MAX_GROUPS:I

.field private listeners:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/garmin/fit/MesgWithEventListener;",
            ">;"
        }
    .end annotation
.end field

.field private startedEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/util/LinkedList",
            "<",
            "Lcom/garmin/fit/MesgWithEvent;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    const/16 v0, 0x100

    iput v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->MAX_GROUPS:I

    .line 25
    const/16 v0, 0xff

    iput v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->DEFAULT_GROUP:I

    .line 26
    const/16 v0, 0xfe

    iput v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->BEGIN_END_GROUP:I

    .line 32
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->listeners:Ljava/util/ArrayList;

    .line 33
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    .line 35
    const/4 v0, 0x0

    :goto_0
    iget v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->MAX_GROUPS:I

    if-ge v0, v1, :cond_0

    .line 36
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    new-instance v2, Ljava/util/LinkedList;

    invoke-direct {v2}, Ljava/util/LinkedList;-><init>()V

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 35
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 38
    :cond_0
    return-void
.end method

.method private broadcast(Lcom/garmin/fit/MesgWithEvent;)V
    .locals 2

    .prologue
    .line 147
    iget-object v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgWithEventListener;

    .line 148
    invoke-interface {v0, p1}, Lcom/garmin/fit/MesgWithEventListener;->onMesg(Lcom/garmin/fit/MesgWithEvent;)V

    goto :goto_0

    .line 150
    :cond_0
    return-void
.end method


# virtual methods
.method public addListener(Lcom/garmin/fit/MesgWithEventListener;)V
    .locals 1

    .prologue
    .line 41
    iget-object v0, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->listeners:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 42
    return-void
.end method

.method public onMesg(Lcom/garmin/fit/MesgWithEvent;)V
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 45
    check-cast p1, Lcom/garmin/fit/Mesg;

    invoke-static {p1}, Lcom/garmin/fit/Factory;->createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;

    move-result-object v0

    check-cast v0, Lcom/garmin/fit/MesgWithEvent;

    .line 46
    iget v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->DEFAULT_GROUP:I

    .line 48
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEventGroup()Ljava/lang/Short;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 49
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEventGroup()Ljava/lang/Short;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/Short;->shortValue()S

    move-result v1

    .line 52
    :cond_0
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEventType()Lcom/garmin/fit/EventType;

    move-result-object v2

    if-nez v2, :cond_1

    .line 144
    :goto_0
    return-void

    .line 56
    :cond_1
    sget-object v2, Lcom/garmin/fit/MesgWithEventBroadcaster$1;->$SwitchMap$com$garmin$fit$EventType:[I

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEventType()Lcom/garmin/fit/EventType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/garmin/fit/EventType;->ordinal()I

    move-result v4

    aget v2, v2, v4

    packed-switch v2, :pswitch_data_0

    move v2, v1

    .line 80
    :goto_1
    sget-object v1, Lcom/garmin/fit/MesgWithEventBroadcaster$1;->$SwitchMap$com$garmin$fit$EventType:[I

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEventType()Lcom/garmin/fit/EventType;

    move-result-object v4

    invoke-virtual {v4}, Lcom/garmin/fit/EventType;->ordinal()I

    move-result v4

    aget v1, v1, v4

    packed-switch v1, :pswitch_data_1

    .line 143
    :cond_2
    :goto_2
    invoke-direct {p0, v0}, Lcom/garmin/fit/MesgWithEventBroadcaster;->broadcast(Lcom/garmin/fit/MesgWithEvent;)V

    goto :goto_0

    .line 58
    :pswitch_0
    iget v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->BEGIN_END_GROUP:I

    .line 59
    sget-object v2, Lcom/garmin/fit/EventType;->START:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v2}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    move v2, v1

    .line 60
    goto :goto_1

    .line 63
    :pswitch_1
    iget v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->BEGIN_END_GROUP:I

    .line 64
    sget-object v2, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v2}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    move v2, v1

    .line 65
    goto :goto_1

    .line 68
    :pswitch_2
    sget-object v2, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v2}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    move v2, v1

    .line 69
    goto :goto_1

    .line 72
    :pswitch_3
    iget v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->BEGIN_END_GROUP:I

    .line 73
    sget-object v2, Lcom/garmin/fit/EventType;->STOP_ALL:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v2}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    move v2, v1

    .line 74
    goto :goto_1

    .line 82
    :goto_3
    :pswitch_4
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v3, v1, :cond_5

    .line 83
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    invoke-interface {v1}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v1

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v4

    if-ne v1, v4, :cond_4

    .line 84
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/Mesg;

    invoke-static {v1}, Lcom/garmin/fit/Factory;->createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    .line 85
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v4

    .line 86
    sget-object v5, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    invoke-interface {v1, v5}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    .line 87
    if-eqz v4, :cond_3

    .line 88
    invoke-interface {v1, v4}, Lcom/garmin/fit/MesgWithEvent;->setTimestamp(Lcom/garmin/fit/DateTime;)V

    .line 89
    :cond_3
    invoke-direct {p0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->broadcast(Lcom/garmin/fit/MesgWithEvent;)V

    .line 90
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 82
    :cond_4
    add-int/lit8 v1, v3, 0x1

    move v3, v1

    goto :goto_3

    .line 94
    :cond_5
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    move-object v2, v0

    check-cast v2, Lcom/garmin/fit/Mesg;

    invoke-static {v2}, Lcom/garmin/fit/Factory;->createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;

    move-result-object v2

    check-cast v2, Lcom/garmin/fit/MesgWithEvent;

    invoke-virtual {v1, v2}, Ljava/util/LinkedList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_2

    .line 99
    :goto_4
    :pswitch_5
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v3, v1, :cond_2

    .line 100
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    invoke-interface {v1}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v1

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v4

    if-ne v1, v4, :cond_6

    .line 101
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->remove(I)Ljava/lang/Object;

    .line 99
    :cond_6
    add-int/lit8 v3, v3, 0x1

    goto :goto_4

    .line 107
    :goto_5
    :pswitch_6
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v3, v1, :cond_9

    .line 108
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    invoke-interface {v1}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v1

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v4

    if-eq v1, v4, :cond_8

    .line 109
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/Mesg;

    invoke-static {v1}, Lcom/garmin/fit/Factory;->createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    .line 110
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v4

    .line 111
    sget-object v5, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    invoke-interface {v1, v5}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    .line 112
    if-eqz v4, :cond_7

    .line 113
    invoke-interface {v1, v4}, Lcom/garmin/fit/MesgWithEvent;->setTimestamp(Lcom/garmin/fit/DateTime;)V

    .line 114
    :cond_7
    invoke-direct {p0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->broadcast(Lcom/garmin/fit/MesgWithEvent;)V

    .line 107
    :cond_8
    add-int/lit8 v3, v3, 0x1

    goto :goto_5

    .line 118
    :cond_9
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 119
    sget-object v1, Lcom/garmin/fit/EventType;->STOP:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v1}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    goto/16 :goto_2

    .line 123
    :goto_6
    :pswitch_7
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->size()I

    move-result v1

    if-ge v3, v1, :cond_c

    .line 124
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    invoke-interface {v1}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v1

    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getEvent()Lcom/garmin/fit/Event;

    move-result-object v4

    if-eq v1, v4, :cond_b

    .line 125
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1, v3}, Ljava/util/LinkedList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/Mesg;

    invoke-static {v1}, Lcom/garmin/fit/Factory;->createMesg(Lcom/garmin/fit/Mesg;)Lcom/garmin/fit/Mesg;

    move-result-object v1

    check-cast v1, Lcom/garmin/fit/MesgWithEvent;

    .line 126
    invoke-interface {v0}, Lcom/garmin/fit/MesgWithEvent;->getTimestamp()Lcom/garmin/fit/DateTime;

    move-result-object v4

    .line 127
    sget-object v5, Lcom/garmin/fit/EventType;->STOP_DISABLE:Lcom/garmin/fit/EventType;

    invoke-interface {v1, v5}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    .line 128
    if-eqz v4, :cond_a

    .line 129
    invoke-interface {v1, v4}, Lcom/garmin/fit/MesgWithEvent;->setTimestamp(Lcom/garmin/fit/DateTime;)V

    .line 130
    :cond_a
    invoke-direct {p0, v1}, Lcom/garmin/fit/MesgWithEventBroadcaster;->broadcast(Lcom/garmin/fit/MesgWithEvent;)V

    .line 123
    :cond_b
    add-int/lit8 v3, v3, 0x1

    goto :goto_6

    .line 134
    :cond_c
    iget-object v1, p0, Lcom/garmin/fit/MesgWithEventBroadcaster;->startedEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/LinkedList;

    invoke-virtual {v1}, Ljava/util/LinkedList;->clear()V

    .line 135
    sget-object v1, Lcom/garmin/fit/EventType;->STOP_DISABLE:Lcom/garmin/fit/EventType;

    invoke-interface {v0, v1}, Lcom/garmin/fit/MesgWithEvent;->setEventType(Lcom/garmin/fit/EventType;)V

    goto/16 :goto_2

    .line 56
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch

    .line 80
    :pswitch_data_1
    .packed-switch 0x5
        :pswitch_4
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method
