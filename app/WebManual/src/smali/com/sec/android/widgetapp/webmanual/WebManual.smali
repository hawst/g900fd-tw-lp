.class public Lcom/sec/android/widgetapp/webmanual/WebManual;
.super Landroid/app/Activity;
.source "WebManual.java"


# static fields
.field private static final FIRST_SEND_PACKAGE:Landroid/content/ComponentName;

.field private static final SECOND_SEND_PACKAGE:Landroid/content/ComponentName;


# instance fields
.field adb:Landroid/app/AlertDialog$Builder;

.field public dontShowAgain:Landroid/widget/CheckBox;

.field private mParser:Lcom/sec/android/widgetapp/webmanual/CscParser;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 35
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.sec.android.app.sbrowser"

    const-string v2, "com.sec.android.app.sbrowser.SBrowserMainActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/widgetapp/webmanual/WebManual;->FIRST_SEND_PACKAGE:Landroid/content/ComponentName;

    .line 36
    new-instance v0, Landroid/content/ComponentName;

    const-string v1, "com.android.browser"

    const-string v2, "com.android.browser.BrowserActivity"

    invoke-direct {v0, v1, v2}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/widgetapp/webmanual/WebManual;->SECOND_SEND_PACKAGE:Landroid/content/ComponentName;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 37
    invoke-static {}, Lcom/sec/android/widgetapp/webmanual/CscParser;->getInstance()Lcom/sec/android/widgetapp/webmanual/CscParser;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->mParser:Lcom/sec/android/widgetapp/webmanual/CscParser;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/webmanual/WebManual;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/webmanual/WebManual;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->checkNetwork()I

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/webmanual/WebManual;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/webmanual/WebManual;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->showRetry()V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/webmanual/WebManual;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/webmanual/WebManual;
    .param p1, "x1"    # I

    .prologue
    .line 32
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V

    return-void
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/webmanual/WebManual;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/webmanual/WebManual;

    .prologue
    .line 32
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->launchBrowser()V

    return-void
.end method

.method private checkNetwork()I
    .locals 7

    .prologue
    .line 146
    const-string v4, "connectivity"

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    .line 147
    .local v0, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v0}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    .line 148
    .local v1, "info":Landroid/net/NetworkInfo;
    const/4 v2, 0x0

    .line 150
    .local v2, "netStatus":I
    if-eqz v1, :cond_0

    .line 151
    invoke-virtual {v1}, Landroid/net/NetworkInfo;->getTypeName()Ljava/lang/String;

    move-result-object v3

    .line 152
    .local v3, "typeName":Ljava/lang/String;
    const-string v4, "WIFI"

    invoke-virtual {v4, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 153
    const/4 v2, 0x2

    .line 158
    .end local v3    # "typeName":Ljava/lang/String;
    :cond_0
    :goto_0
    const-string v4, "WebManual"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "netStatus="

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 159
    return v2

    .line 155
    .restart local v3    # "typeName":Ljava/lang/String;
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private launchBrowser()V
    .locals 10

    .prologue
    const/4 v9, 0x0

    .line 90
    const/4 v4, 0x0

    .line 92
    .local v4, "manualURL":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->mParser:Lcom/sec/android/widgetapp/webmanual/CscParser;

    const-string v8, "Settings.Browser.eManual.URL"

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/webmanual/CscParser;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 96
    new-instance v0, Landroid/content/Intent;

    const-string v7, "android.intent.action.VIEW"

    invoke-direct {v0, v7}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 100
    .local v0, "callBrowserIntent":Landroid/content/Intent;
    if-eqz v4, :cond_0

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v7

    if-lez v7, :cond_0

    .line 102
    invoke-static {v4}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 110
    .local v6, "u":Landroid/net/Uri;
    :goto_0
    invoke-virtual {v0, v6}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 111
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v5

    .line 113
    .local v5, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    sget-object v7, Lcom/sec/android/widgetapp/webmanual/WebManual;->FIRST_SEND_PACKAGE:Landroid/content/ComponentName;

    const/16 v8, 0x80

    invoke-virtual {v5, v7, v8}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 116
    const/high16 v7, 0x10000000

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 117
    sget-object v7, Lcom/sec/android/widgetapp/webmanual/WebManual;->FIRST_SEND_PACKAGE:Landroid/content/ComponentName;

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 118
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 143
    :goto_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->finish()V

    .line 144
    return-void

    .line 106
    .end local v5    # "pm":Landroid/content/pm/PackageManager;
    .end local v6    # "u":Landroid/net/Uri;
    :cond_0
    const-string v7, "http://www.samsung.com/m-manual/common"

    invoke-static {v7}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .restart local v6    # "u":Landroid/net/Uri;
    goto :goto_0

    .line 121
    .restart local v5    # "pm":Landroid/content/pm/PackageManager;
    :catch_0
    move-exception v1

    .line 124
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_1
    sget-object v7, Lcom/sec/android/widgetapp/webmanual/WebManual;->SECOND_SEND_PACKAGE:Landroid/content/ComponentName;

    const/16 v8, 0x80

    invoke-virtual {v5, v7, v8}, Landroid/content/pm/PackageManager;->getActivityInfo(Landroid/content/ComponentName;I)Landroid/content/pm/ActivityInfo;

    .line 126
    const/high16 v7, 0x10000000

    invoke-virtual {v0, v7}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 127
    sget-object v7, Lcom/sec/android/widgetapp/webmanual/WebManual;->SECOND_SEND_PACKAGE:Landroid/content/ComponentName;

    invoke-virtual {v0, v7}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 128
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->startActivity(Landroid/content/Intent;)V
    :try_end_1
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    .line 130
    :catch_1
    move-exception v2

    .line 132
    .local v2, "error":Landroid/content/pm/PackageManager$NameNotFoundException;
    :try_start_2
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->startActivity(Landroid/content/Intent;)V
    :try_end_2
    .catch Landroid/content/ActivityNotFoundException; {:try_start_2 .. :try_end_2} :catch_2

    goto :goto_1

    .line 134
    :catch_2
    move-exception v3

    .line 135
    .local v3, "ex":Landroid/content/ActivityNotFoundException;
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v7

    if-eqz v7, :cond_1

    .line 136
    const v7, 0x7f040001

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1

    .line 138
    :cond_1
    const v7, 0x7f040002

    invoke-static {p0, v7, v9}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v7

    invoke-virtual {v7}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method private setupShortcut()V
    .locals 5

    .prologue
    .line 265
    new-instance v2, Landroid/content/Intent;

    const-string v3, "android.intent.action.MAIN"

    invoke-direct {v2, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 266
    .local v2, "shortcutIntent":Landroid/content/Intent;
    const-string v3, "android.intent.category.LAUNCHER"

    invoke-virtual {v2, v3}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 267
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, p0, v3}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 269
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 270
    .local v1, "intent":Landroid/content/Intent;
    const-string v3, "android.intent.extra.shortcut.INTENT"

    invoke-virtual {v1, v3, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 272
    const/high16 v3, 0x7f020000

    invoke-static {p0, v3}, Landroid/content/Intent$ShortcutIconResource;->fromContext(Landroid/content/Context;I)Landroid/content/Intent$ShortcutIconResource;

    move-result-object v0

    .line 274
    .local v0, "iconResource":Landroid/os/Parcelable;
    const-string v3, "android.intent.extra.shortcut.ICON_RESOURCE"

    invoke-virtual {v1, v3, v0}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 275
    const-string v3, "android.intent.extra.shortcut.NAME"

    const/high16 v4, 0x7f040000

    invoke-virtual {p0, v4}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 277
    const/4 v3, -0x1

    invoke-virtual {p0, v3, v1}, Lcom/sec/android/widgetapp/webmanual/WebManual;->setResult(ILandroid/content/Intent;)V

    .line 279
    return-void
.end method

.method private showRetry()V
    .locals 6

    .prologue
    .line 163
    const v3, 0x7f040009

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 164
    .local v2, "msgTitle":Ljava/lang/String;
    const v3, 0x7f04000a

    invoke-virtual {p0, v3}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 165
    .local v1, "msgText":Ljava/lang/String;
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f040008

    new-instance v5, Lcom/sec/android/widgetapp/webmanual/WebManual$3;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$3;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f04000c

    new-instance v5, Lcom/sec/android/widgetapp/webmanual/WebManual$2;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$2;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/widgetapp/webmanual/WebManual$1;

    invoke-direct {v4, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$1;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v0

    .line 202
    .local v0, "dialog":Landroid/app/AlertDialog;
    invoke-virtual {v0}, Landroid/app/AlertDialog;->show()V

    .line 203
    return-void
.end method

.method private show_checkbox_dialog(I)V
    .locals 10
    .param p1, "in_pos"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v9, 0x1

    .line 206
    move v5, p1

    .line 208
    .local v5, "pos":I
    const/4 v4, 0x0

    .line 209
    .local v4, "msgTitle":Ljava/lang/String;
    const/4 v3, 0x0

    .line 211
    .local v3, "msgText":Ljava/lang/String;
    const-string v7, "data_charging_Setting"

    invoke-virtual {p0, v7, v8}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v6

    .line 212
    .local v6, "pref":Landroid/content/SharedPreferences;
    const-string v7, "data_charging_checkbox"

    invoke-interface {v6, v7, v8}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    .line 213
    .local v1, "cb_info":Z
    if-ne v1, v9, :cond_0

    .line 214
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->launchBrowser()V

    .line 261
    :goto_0
    return-void

    .line 216
    :cond_0
    if-ne v5, v9, :cond_2

    .line 217
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040003

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 218
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040004

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 223
    :cond_1
    :goto_1
    new-instance v7, Landroid/app/AlertDialog$Builder;

    invoke-direct {v7, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    iput-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    .line 224
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7, v4}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 225
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7, v3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 226
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 227
    .local v0, "adbInflater":Landroid/view/LayoutInflater;
    const/high16 v7, 0x7f030000

    const/4 v8, 0x0

    invoke-virtual {v0, v7, v8}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 228
    .local v2, "eulaLayout":Landroid/view/View;
    const/high16 v7, 0x7f050000

    invoke-virtual {v2, v7}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    iput-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->dontShowAgain:Landroid/widget/CheckBox;

    .line 229
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->dontShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v7, v9}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 230
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    .line 231
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    const v8, 0x7f04000c

    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/sec/android/widgetapp/webmanual/WebManual$4;

    invoke-direct {v9, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$4;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setNegativeButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 239
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    const v8, 0x7f04000b

    invoke-virtual {p0, v8}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getString(I)Ljava/lang/String;

    move-result-object v8

    new-instance v9, Lcom/sec/android/widgetapp/webmanual/WebManual$5;

    invoke-direct {v9, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$5;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v7, v8, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(Ljava/lang/CharSequence;Landroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 252
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    new-instance v8, Lcom/sec/android/widgetapp/webmanual/WebManual$6;

    invoke-direct {v8, p0}, Lcom/sec/android/widgetapp/webmanual/WebManual$6;-><init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    invoke-virtual {v7, v8}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 259
    iget-object v7, p0, Lcom/sec/android/widgetapp/webmanual/WebManual;->adb:Landroid/app/AlertDialog$Builder;

    invoke-virtual {v7}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    goto :goto_0

    .line 219
    .end local v0    # "adbInflater":Landroid/view/LayoutInflater;
    .end local v2    # "eulaLayout":Landroid/view/View;
    :cond_2
    const/4 v7, 0x2

    if-ne v5, v7, :cond_1

    .line 220
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040005

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 221
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    const v8, 0x7f040006

    invoke-virtual {v7, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_1
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v7, 0x0

    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 52
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getIntent()Landroid/content/Intent;

    move-result-object v2

    .line 53
    .local v2, "intent":Landroid/content/Intent;
    invoke-virtual {v2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 55
    .local v0, "action":Ljava/lang/String;
    const-string v4, "android.intent.action.CREATE_SHORTCUT"

    invoke-virtual {v4, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 56
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->setupShortcut()V

    .line 57
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->finish()V

    .line 83
    :goto_0
    return-void

    .line 60
    :cond_0
    const-string v4, "ChinaNalSecurity"

    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_Common_ConfigLocalSecurityPolicy"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 61
    const-string v4, "data_charging_Setting"

    invoke-virtual {p0, v4, v7}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 62
    .local v3, "pref":Landroid/content/SharedPreferences;
    const-string v4, "data_charging_checkbox"

    invoke-interface {v3, v4, v7}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v4

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 63
    .local v1, "cb_info":Ljava/lang/Boolean;
    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-nez v4, :cond_1

    .line 64
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->checkNetwork()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 75
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->showRetry()V

    goto :goto_0

    .line 66
    :pswitch_0
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V

    goto :goto_0

    .line 69
    :pswitch_1
    const/4 v4, 0x2

    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V

    goto :goto_0

    .line 72
    :pswitch_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->showRetry()V

    goto :goto_0

    .line 78
    :cond_1
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->launchBrowser()V

    goto :goto_0

    .line 81
    .end local v1    # "cb_info":Ljava/lang/Boolean;
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    :cond_2
    invoke-direct {p0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->launchBrowser()V

    goto :goto_0

    .line 64
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method
