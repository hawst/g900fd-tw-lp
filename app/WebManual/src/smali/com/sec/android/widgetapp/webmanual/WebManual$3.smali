.class Lcom/sec/android/widgetapp/webmanual/WebManual$3;
.super Ljava/lang/Object;
.source "WebManual.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/webmanual/WebManual;->showRetry()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V
    .locals 0

    .prologue
    .line 169
    iput-object p1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$3;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 172
    iget-object v1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$3;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    # invokes: Lcom/sec/android/widgetapp/webmanual/WebManual;->checkNetwork()I
    invoke-static {v1}, Lcom/sec/android/widgetapp/webmanual/WebManual;->access$000(Lcom/sec/android/widgetapp/webmanual/WebManual;)I

    move-result v0

    .line 173
    .local v0, "netStatus":I
    if-nez v0, :cond_2

    .line 174
    if-eqz p1, :cond_0

    .line 175
    invoke-interface {p1}, Landroid/content/DialogInterface;->dismiss()V

    .line 176
    const/4 p1, 0x0

    .line 178
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$3;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    # invokes: Lcom/sec/android/widgetapp/webmanual/WebManual;->showRetry()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/webmanual/WebManual;->access$100(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    .line 190
    :cond_1
    :goto_0
    return-void

    .line 180
    :cond_2
    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 181
    iget-object v1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$3;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    # invokes: Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->access$200(Lcom/sec/android/widgetapp/webmanual/WebManual;I)V

    goto :goto_0

    .line 184
    :cond_3
    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 185
    const-string v1, "WebManual"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "netStatus wifi = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    iget-object v1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$3;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    # invokes: Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/webmanual/WebManual;->access$200(Lcom/sec/android/widgetapp/webmanual/WebManual;I)V

    goto :goto_0
.end method
