.class Lcom/sec/android/widgetapp/webmanual/WebManual$5;
.super Ljava/lang/Object;
.source "WebManual.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/webmanual/WebManual;->show_checkbox_dialog(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/webmanual/WebManual;)V
    .locals 0

    .prologue
    .line 240
    iput-object p1, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$5;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 5
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 242
    iget-object v2, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$5;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    iget-object v2, v2, Lcom/sec/android/widgetapp/webmanual/WebManual;->dontShowAgain:Landroid/widget/CheckBox;

    invoke-virtual {v2}, Landroid/widget/CheckBox;->isChecked()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 243
    iget-object v2, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$5;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    const-string v3, "data_charging_Setting"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Lcom/sec/android/widgetapp/webmanual/WebManual;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 244
    .local v1, "pref":Landroid/content/SharedPreferences;
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 245
    .local v0, "edit":Landroid/content/SharedPreferences$Editor;
    const-string v2, "data_charging_checkbox"

    const/4 v3, 0x1

    invoke-interface {v0, v2, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 246
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 248
    .end local v0    # "edit":Landroid/content/SharedPreferences$Editor;
    .end local v1    # "pref":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$5;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    # invokes: Lcom/sec/android/widgetapp/webmanual/WebManual;->launchBrowser()V
    invoke-static {v2}, Lcom/sec/android/widgetapp/webmanual/WebManual;->access$300(Lcom/sec/android/widgetapp/webmanual/WebManual;)V

    .line 249
    iget-object v2, p0, Lcom/sec/android/widgetapp/webmanual/WebManual$5;->this$0:Lcom/sec/android/widgetapp/webmanual/WebManual;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/webmanual/WebManual;->finish()V

    .line 250
    return-void
.end method
