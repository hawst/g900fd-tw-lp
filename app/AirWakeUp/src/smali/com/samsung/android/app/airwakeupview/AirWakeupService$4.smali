.class Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;
.super Ljava/lang/Object;
.source "AirWakeupService.java"

# interfaces
.implements Landroid/hardware/scontext/SContextListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/samsung/android/app/airwakeupview/AirWakeupService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0

    .prologue
    .line 207
    iput-object p1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onSContextChanged(Landroid/hardware/scontext/SContextEvent;)V
    .locals 3
    .param p1, "event"    # Landroid/hardware/scontext/SContextEvent;

    .prologue
    .line 210
    iget-object v1, p1, Landroid/hardware/scontext/SContextEvent;->scontext:Landroid/hardware/scontext/SContext;

    invoke-virtual {v1}, Landroid/hardware/scontext/SContext;->getType()I

    move-result v1

    sparse-switch v1, :sswitch_data_0

    .line 228
    :goto_0
    return-void

    .line 212
    :sswitch_0
    const-string v1, "AirWakeupService"

    const-string v2, "onSContextChanged() Approach"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 213
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    # invokes: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->launchAirWakeup()V
    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$500(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    goto :goto_0

    .line 216
    :sswitch_1
    invoke-virtual {p1}, Landroid/hardware/scontext/SContextEvent;->getFlatMotionContext()Landroid/hardware/scontext/SContextFlatMotion;

    move-result-object v0

    .line 217
    .local v0, "flatMotionContext":Landroid/hardware/scontext/SContextFlatMotion;
    invoke-virtual {v0}, Landroid/hardware/scontext/SContextFlatMotion;->getAction()I

    move-result v1

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 219
    :pswitch_0
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    # invokes: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->stopAirWakeup()V
    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$600(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    goto :goto_0

    .line 210
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x14 -> :sswitch_1
    .end sparse-switch

    .line 217
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
