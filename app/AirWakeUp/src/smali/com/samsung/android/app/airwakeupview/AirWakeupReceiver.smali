.class public Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;
.super Landroid/content/BroadcastReceiver;
.source "AirWakeupReceiver.java"


# static fields
.field static final TAG:Ljava/lang/String; = "AirWakeupReceiver"


# instance fields
.field private mIsSettingAirWakeUp:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 12
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 16
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->mIsSettingAirWakeUp:Z

    return-void
.end method

.method private startAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    const-class v0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 69
    invoke-virtual {p1, p2}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 70
    return-void
.end method

.method private stopAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 1
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 73
    const-class v0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-virtual {p2, p1, v0}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    .line 74
    invoke-virtual {p1, p2}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 75
    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 20
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 22
    .local v0, "action":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    const-string v2, "com.sec.feature.sensorhub"

    invoke-virtual {v1, v2}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 24
    invoke-static {p1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkSettingCondition(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->mIsSettingAirWakeUp:Z

    .line 26
    const-string v1, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 27
    const-string v1, "AirWakeupReceiver"

    const-string v2, "AirWakeupReceiver Intent.ACTION_BOOT_COMPLETED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 29
    iget-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->mIsSettingAirWakeUp:Z

    if-eqz v1, :cond_0

    .line 30
    const-string v1, "AirWakeupReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "start AirWakeup Service()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 31
    invoke-static {p1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 32
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->startAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 65
    :cond_0
    :goto_0
    return-void

    .line 40
    :cond_1
    sget-object v1, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->GLANCEVIEW_LED_ALARM:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 41
    const-string v1, "AirWakeupReceiver"

    const-string v2, "AirWakeupReceiver GLANCEVIEW_LED_ALARM"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 43
    const/4 v1, 0x0

    invoke-virtual {p0, p1, v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->setLEDNotification(Landroid/content/Context;Z)V

    .line 45
    invoke-static {p1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 46
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->startAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V

    .line 47
    const-string v1, "AirWakeupReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "start Airwakeup Service()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 49
    :cond_2
    sget-object v1, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->SETTINGS_AIR_WAKE_UP_CHANGED:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 50
    const-string v1, "AirWakeupReceiver"

    const-string v2, "AirWakeupReceiver AIR_WAKE_UP_SETTINGS_CHANGED"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 52
    iget-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->mIsSettingAirWakeUp:Z

    if-eqz v1, :cond_3

    .line 53
    const-string v1, "AirWakeupReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "start Airwakeup Service()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    invoke-static {p1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->startAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0

    .line 58
    :cond_3
    invoke-static {p1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 59
    const-string v1, "AirWakeupReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "stop Airwakeup Service()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 60
    invoke-direct {p0, p1, p2}, Lcom/samsung/android/app/airwakeupview/AirWakeupReceiver;->stopAirWakeupService(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method public setLEDNotification(Landroid/content/Context;Z)V
    .locals 10
    .param p1, "ctx"    # Landroid/content/Context;
    .param p2, "ledOn"    # Z

    .prologue
    const/16 v9, 0x207f

    const/16 v8, 0x3e8

    const/16 v7, 0xff

    const/4 v6, 0x0

    .line 78
    const-string v3, "AirWakeupReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setLEDNotification() LED ON : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 79
    const/16 v0, 0x207f

    .line 80
    .local v0, "LED_NOTIF_ID":I
    const-string v3, "notification"

    invoke-virtual {p1, v3}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 81
    .local v2, "notiMgr":Landroid/app/NotificationManager;
    if-eqz p2, :cond_0

    .line 82
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 83
    .local v1, "noti":Landroid/app/Notification;
    invoke-static {v7, v6, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    iput v3, v1, Landroid/app/Notification;->ledARGB:I

    .line 84
    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 85
    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 86
    iput v8, v1, Landroid/app/Notification;->ledOnMS:I

    .line 87
    iput v8, v1, Landroid/app/Notification;->ledOffMS:I

    .line 88
    invoke-virtual {v2, v9, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 92
    .end local v1    # "noti":Landroid/app/Notification;
    :goto_0
    return-void

    .line 90
    :cond_0
    invoke-virtual {v2, v9}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method
