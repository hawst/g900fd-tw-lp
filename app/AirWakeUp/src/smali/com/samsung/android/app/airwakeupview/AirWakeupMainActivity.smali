.class public Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;
.super Landroid/app/Activity;
.source "AirWakeupMainActivity.java"


# instance fields
.field public final DEBUG:Z

.field private final TAG:Ljava/lang/String;

.field private mComponentName:Landroid/content/ComponentName;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 19
    const-string v0, "AirWakeupMainActivity"

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->TAG:Ljava/lang/String;

    .line 21
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->DEBUG:Z

    .line 23
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->mComponentName:Landroid/content/ComponentName;

    .line 17
    return-void
.end method

.method private BtnUIEnd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 79
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "assistant_menu"

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 81
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 83
    const-string v0, "Now Service Stoped"

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 86
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->stopService(Landroid/content/Intent;)Z

    .line 87
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->mComponentName:Landroid/content/ComponentName;

    .line 92
    :goto_0
    return-void

    .line 89
    :cond_0
    const-string v0, "Already Service Stoped"

    invoke-static {p0, v0, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method private BtnUIStart()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 59
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "air_motion_wake_up"

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 61
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkServiceRunning(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 63
    const-string v0, "Now Service Started"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 66
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    move-result-object v0

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->mComponentName:Landroid/content/ComponentName;

    .line 72
    :goto_0
    return-void

    .line 69
    :cond_0
    const-string v0, "Already Service Started"

    invoke-static {p0, v0, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v0

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method static synthetic access$000(Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->BtnUIStart()V

    return-void
.end method

.method static synthetic access$100(Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;

    .prologue
    .line 13
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->BtnUIEnd()V

    return-void
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 28
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 30
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->setContentView(I)V

    .line 32
    const/high16 v2, 0x7f050000

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    .line 33
    .local v0, "sfMainStartservice":Landroid/widget/Button;
    new-instance v2, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity$1;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity$1;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;)V

    invoke-virtual {v0, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 40
    const v2, 0x7f050001

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/Button;

    .line 41
    .local v1, "sfMainStopservice":Landroid/widget/Button;
    new-instance v2, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity$2;

    invoke-direct {v2, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity$2;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupMainActivity;)V

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 47
    return-void
.end method

.method public onCreateOptionsMenu(Landroid/view/Menu;)Z
    .locals 1
    .param p1, "menu"    # Landroid/view/Menu;

    .prologue
    .line 51
    const/4 v0, 0x1

    return v0
.end method
