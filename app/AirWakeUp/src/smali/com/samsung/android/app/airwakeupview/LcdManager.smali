.class public Lcom/samsung/android/app/airwakeupview/LcdManager;
.super Ljava/lang/Object;
.source "LcdManager.java"


# annotations
.annotation build Landroid/annotation/TargetApi;
    value = 0x10
.end annotation


# static fields
.field protected static final AWAKE_INTERVAL_DEFAULT_MS:I = 0x2710

.field private static final TAG:Ljava/lang/String; = "AirWakeupLCDmanager"

.field private static final TIMEOUT:I = 0x1

.field private static mKeyguardManager:Landroid/app/KeyguardManager;

.field private static mPM:Landroid/os/PowerManager;

.field private static mWakeLock:Landroid/os/PowerManager$WakeLock;

.field private static mWakelockSequence:I

.field private static sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;


# instance fields
.field private mWakeLockHandler:Landroid/os/Handler;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    const/4 v0, 0x0

    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 60
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 93
    new-instance v0, Lcom/samsung/android/app/airwakeupview/LcdManager$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/airwakeupview/LcdManager$1;-><init>(Lcom/samsung/android/app/airwakeupview/LcdManager;)V

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    .line 61
    const-string v0, "power"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mPM:Landroid/os/PowerManager;

    .line 62
    const-string v0, "keyguard"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/KeyguardManager;

    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 63
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mPM:Landroid/os/PowerManager;

    const v1, 0x1000001a

    const-string v2, "AirWakeupService"

    invoke-virtual {v0, v1, v2}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v0

    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 65
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/PowerManager$WakeLock;->setReferenceCounted(Z)V

    .line 69
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/airwakeupview/LcdManager;I)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/LcdManager;
    .param p1, "x1"    # I

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/samsung/android/app/airwakeupview/LcdManager;->handleTimeout(I)V

    return-void
.end method

.method public static declared-synchronized getInstance(Landroid/content/Context;)Lcom/samsung/android/app/airwakeupview/LcdManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const-class v1, Lcom/samsung/android/app/airwakeupview/LcdManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    if-nez v0, :cond_0

    .line 49
    new-instance v0, Lcom/samsung/android/app/airwakeupview/LcdManager;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/airwakeupview/LcdManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    .line 51
    :cond_0
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 48
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method

.method private handleTimeout(I)V
    .locals 2
    .param p1, "seq"    # I

    .prologue
    .line 78
    monitor-enter p0

    .line 79
    :try_start_0
    const-string v0, "AirWakeupLCDmanager"

    const-string v1, "handleTimeout"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 81
    sget v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakelockSequence:I

    if-ne p1, v0, :cond_0

    .line 82
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v0}, Landroid/os/PowerManager$WakeLock;->release()V

    .line 84
    :cond_0
    monitor-exit p0

    .line 85
    return-void

    .line 84
    :catchall_0
    move-exception v0

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v0
.end method

.method public static isKeyguardLocked()Z
    .locals 1

    .prologue
    .line 190
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v0, :cond_0

    .line 191
    sget-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v0}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 192
    const/4 v0, 0x1

    .line 195
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSecureKeyguardLocked()Z
    .locals 2

    .prologue
    .line 180
    const/4 v0, 0x0

    .line 181
    .local v0, "bLockscreen":Z
    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_0

    .line 182
    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 183
    const/4 v0, 0x1

    .line 186
    :cond_0
    return v0
.end method

.method public static isUnsecureKeyguardLocked()Z
    .locals 2
    .annotation build Landroid/annotation/TargetApi;
        value = 0x10
    .end annotation

    .prologue
    .line 164
    const/4 v0, 0x0

    .line 165
    .local v0, "bLockscreen":Z
    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    if-eqz v1, :cond_0

    .line 166
    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardLocked()Z

    move-result v1

    if-eqz v1, :cond_0

    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    invoke-virtual {v1}, Landroid/app/KeyguardManager;->isKeyguardSecure()Z

    move-result v1

    if-nez v1, :cond_0

    .line 167
    const/4 v0, 0x1

    .line 170
    :cond_0
    return v0
.end method


# virtual methods
.method public finalize()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 149
    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mPM:Landroid/os/PowerManager;

    .line 150
    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mKeyguardManager:Landroid/app/KeyguardManager;

    .line 151
    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 153
    sput-object v0, Lcom/samsung/android/app/airwakeupview/LcdManager;->sLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    .line 154
    return-void
.end method

.method public pokeWakelock()V
    .locals 1

    .prologue
    .line 120
    const/16 v0, 0x2710

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/LcdManager;->pokeWakelock(I)V

    .line 121
    return-void
.end method

.method public pokeWakelock(I)V
    .locals 5
    .param p1, "holdMs"    # I

    .prologue
    .line 130
    monitor-enter p0

    .line 131
    :try_start_0
    const-string v1, "AirWakeupLCDmanager"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "[c] pokeWakelock("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ")"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    sget-object v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 134
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 135
    sget v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakelockSequence:I

    add-int/lit8 v1, v1, 0x1

    sput v1, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakelockSequence:I

    .line 136
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    const/4 v2, 0x1

    sget v3, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakelockSequence:I

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v0

    .line 137
    .local v0, "msg":Landroid/os/Message;
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/LcdManager;->mWakeLockHandler:Landroid/os/Handler;

    int-to-long v2, p1

    invoke-virtual {v1, v0, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 138
    monitor-exit p0

    .line 139
    return-void

    .line 138
    .end local v0    # "msg":Landroid/os/Message;
    :catchall_0
    move-exception v1

    monitor-exit p0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v1
.end method
