.class public Lcom/samsung/android/app/airwakeupview/AirWakeupService;
.super Landroid/app/Service;
.source "AirWakeupService.java"


# static fields
.field static final TAG:Ljava/lang/String; = "AirWakeupService"

.field private static mPendingIntent:Landroid/app/PendingIntent;


# instance fields
.field public mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

.field mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

.field private mIsSettingAirWakeUp:Z

.field private mLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

.field private mSContextListener:Landroid/hardware/scontext/SContextListener;

.field private mSContextManager:Landroid/hardware/scontext/SContextManager;

.field mScreenReceiver:Landroid/content/BroadcastReceiver;

.field private mledOn:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 33
    iput-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mledOn:Z

    .line 35
    iput-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z

    .line 37
    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 41
    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 42
    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    .line 96
    new-instance v0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$1;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService$1;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    .line 207
    new-instance v0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;

    invoke-direct {v0, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService$4;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    return-void
.end method

.method private InactivateLEDAlarm()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 243
    const-string v2, "alarm"

    invoke-virtual {p0, v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 244
    .local v0, "am":Landroid/app/AlarmManager;
    const-string v2, "AirWakeupService"

    const-string v3, "GlaceViewManager: InactivateLEDAlarm"

    invoke-static {v2, v3}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 245
    sget-object v2, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    if-eqz v2, :cond_0

    .line 246
    sget-object v2, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 252
    :goto_0
    return-void

    .line 248
    :cond_0
    new-instance v1, Landroid/content/Intent;

    sget-object v2, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->GLANCEVIEW_LED_ALARM:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 249
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v4, v1, v4}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v2

    sput-object v2, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 250
    sget-object v2, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v2}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    goto :goto_0
.end method

.method private UnregisterApproachListener()V
    .locals 3

    .prologue
    .line 265
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    iget-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-eqz v0, :cond_0

    .line 267
    iget-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->unregisterListener(Landroid/hardware/scontext/SContextListener;I)V

    .line 268
    const-string v0, "AirWakeupService"

    const-string v1, "AirWakeupService : UnregisterApproachListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 271
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mledOn:Z

    return v0
.end method

.method static synthetic access$100(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->InactivateLEDAlarm()V

    return-void
.end method

.method static synthetic access$200(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)Z
    .locals 1
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    iget-boolean v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z

    return v0
.end method

.method static synthetic access$202(Lcom/samsung/android/app/airwakeupview/AirWakeupService;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;
    .param p1, "x1"    # Z

    .prologue
    .line 30
    iput-boolean p1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z

    return p1
.end method

.method static synthetic access$300(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerApproachListener()V

    return-void
.end method

.method static synthetic access$400(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->UnregisterApproachListener()V

    return-void
.end method

.method static synthetic access$500(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->launchAirWakeup()V

    return-void
.end method

.method static synthetic access$600(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0
    .param p0, "x0"    # Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->stopAirWakeup()V

    return-void
.end method

.method private activateLEDAlarm()V
    .locals 8

    .prologue
    const/4 v5, 0x0

    .line 232
    new-instance v1, Landroid/content/Intent;

    sget-object v4, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->GLANCEVIEW_LED_ALARM:Ljava/lang/String;

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 234
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v5, v1, v5}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v4

    sput-object v4, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    .line 236
    const-string v4, "alarm"

    invoke-virtual {p0, v4}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 238
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    const-wide/16 v6, 0xbb8

    add-long v2, v4, v6

    .line 239
    .local v2, "triggerTime":J
    const/4 v4, 0x3

    sget-object v5, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mPendingIntent:Landroid/app/PendingIntent;

    invoke-virtual {v0, v4, v2, v3, v5}, Landroid/app/AlarmManager;->set(IJLandroid/app/PendingIntent;)V

    .line 240
    return-void
.end method

.method private addIntentBroadCast()V
    .locals 2

    .prologue
    .line 117
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 118
    .local v0, "filter":Landroid/content/IntentFilter;
    sget-object v1, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->SETTINGS_AIR_WAKE_UP_CHANGED:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 121
    new-instance v1, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;

    invoke-direct {v1, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 136
    return-void
.end method

.method private isSupportCover()Z
    .locals 4

    .prologue
    .line 139
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.cover.flip"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    .line 141
    .local v0, "sIsFilpCoverSystemFeatureEnabled":Z
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v2

    const-string v3, "com.sec.feature.cover.sview"

    invoke-virtual {v2, v3}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v1

    .line 144
    .local v1, "sIsSViewCoverSystemFeatureEnabled":Z
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    :cond_0
    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_1
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private launchAirWakeup()V
    .locals 9

    .prologue
    const/4 v6, 0x1

    .line 148
    const-string v5, "AirWakeupService"

    const-string v7, "launchAirWakeup()+"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string v5, "power"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/os/PowerManager;

    const-string v7, "AirWakeupService"

    invoke-virtual {v5, v6, v7}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;

    move-result-object v5

    iput-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    .line 151
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v5, :cond_0

    .line 152
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->acquire()V

    .line 153
    const-string v5, "AirWakeupService"

    const-string v7, "mAirWakeupWakeLock.acquire()"

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    :cond_0
    const-string v5, "phone"

    invoke-virtual {p0, v5}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/telephony/TelephonyManager;

    .line 157
    .local v4, "teleManager":Landroid/telephony/TelephonyManager;
    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    const/4 v7, 0x2

    if-eq v5, v7, :cond_1

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v5

    if-ne v5, v6, :cond_3

    .line 159
    :cond_1
    const-string v5, "AirWakeupService"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "CALL_STATE_OFFHOOK or CALL_STATE_RINGING : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v4}, Landroid/telephony/TelephonyManager;->getCallState()I

    move-result v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 200
    :cond_2
    :goto_0
    return-void

    .line 164
    :cond_3
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-virtual {v5}, Lcom/samsung/android/sdk/cover/ScoverManager;->getCoverState()Lcom/samsung/android/sdk/cover/ScoverState;

    move-result-object v3

    .line 166
    .local v3, "mScoverState":Lcom/samsung/android/sdk/cover/ScoverState;
    const/4 v0, 0x0

    .line 167
    .local v0, "bCoverOpen":Z
    if-eqz v3, :cond_4

    .line 168
    :try_start_0
    invoke-virtual {v3}, Lcom/samsung/android/sdk/cover/ScoverState;->getSwitchState()Z

    move-result v5

    if-ne v5, v6, :cond_7

    move v0, v6

    .line 171
    :cond_4
    :goto_1
    if-nez v0, :cond_5

    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->isSupportCover()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 172
    const-string v5, "AirWakeupService"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "..bCoverOpen.. = "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v0}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {v5, v7}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 175
    :catch_0
    move-exception v1

    .line 176
    .local v1, "e":Ljava/lang/Exception;
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    .line 179
    .end local v1    # "e":Ljava/lang/Exception;
    :cond_5
    invoke-virtual {p0, v6}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->setLEDNotification(Z)V

    .line 180
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->activateLEDAlarm()V

    .line 182
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    invoke-virtual {v5}, Lcom/samsung/android/app/airwakeupview/LcdManager;->pokeWakelock()V

    .line 183
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getApplicationContext()Landroid/content/Context;

    move-result-object v5

    const-string v6, "com.samsung.android.app.airwakeupview"

    const-string v7, "ACC3"

    invoke-static {v5, v6, v7}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->insertLog(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    .line 185
    iget-boolean v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mledOn:Z

    if-eqz v5, :cond_6

    .line 186
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    .line 187
    .local v2, "handler":Landroid/os/Handler;
    new-instance v5, Lcom/samsung/android/app/airwakeupview/AirWakeupService$3;

    invoke-direct {v5, p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService$3;-><init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    const-wide/16 v6, 0x3e8

    invoke-virtual {v2, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 197
    .end local v2    # "handler":Landroid/os/Handler;
    :cond_6
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    if-eqz v5, :cond_2

    .line 198
    iget-object v5, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mAirWakeupWakeLock:Landroid/os/PowerManager$WakeLock;

    invoke-virtual {v5}, Landroid/os/PowerManager$WakeLock;->release()V

    goto :goto_0

    .line 168
    :cond_7
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private registerApproachListener()V
    .locals 3

    .prologue
    .line 255
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    const-string v1, "com.sec.feature.sensorhub"

    invoke-virtual {v0, v1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 256
    iget-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    if-nez v0, :cond_0

    .line 257
    const-string v0, "scontext"

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/hardware/scontext/SContextManager;

    iput-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    .line 259
    :cond_0
    iget-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextManager:Landroid/hardware/scontext/SContextManager;

    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mSContextListener:Landroid/hardware/scontext/SContextListener;

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/hardware/scontext/SContextManager;->registerListener(Landroid/hardware/scontext/SContextListener;I)Z

    .line 260
    const-string v0, "AirWakeupService"

    const-string v1, "GlaceViewManager: registerApproachListener"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 262
    :cond_1
    return-void
.end method

.method private registerScontextListener()V
    .locals 2

    .prologue
    .line 87
    iget-boolean v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z

    if-eqz v0, :cond_0

    .line 88
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerApproachListener()V

    .line 89
    const-string v0, "AirWakeupService"

    const-string v1, "registerApproachListener()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 93
    :goto_0
    return-void

    .line 91
    :cond_0
    const-string v0, "AirWakeupService"

    const-string v1, "registerApproachListener() is not registered cause by mIsSettingAirWakeUp!"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method private stopAirWakeup()V
    .locals 2

    .prologue
    .line 203
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.sec.glanceview.STOP_BY_BOUNCE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 204
    .local v0, "stop_AirWakeup":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->sendBroadcast(Landroid/content/Intent;)V

    .line 205
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 284
    const/4 v0, 0x0

    return-object v0
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 275
    const-string v0, "AirWakeupService"

    const-string v1, "onDestroy()"

    invoke-static {v0, v1}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 276
    iget-object v0, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 277
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->UnregisterApproachListener()V

    .line 278
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 279
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    .line 48
    const-string v1, "AirWakeupService"

    const-string v2, "onStartCommand()+"

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 50
    invoke-static {p0}, Lcom/samsung/android/app/airwakeupview/LcdManager;->getInstance(Landroid/content/Context;)Lcom/samsung/android/app/airwakeupview/LcdManager;

    move-result-object v1

    iput-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mLcdManager:Lcom/samsung/android/app/airwakeupview/LcdManager;

    .line 52
    new-instance v1, Lcom/samsung/android/sdk/cover/ScoverManager;

    invoke-direct {v1, p0}, Lcom/samsung/android/sdk/cover/ScoverManager;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mCoverManager:Lcom/samsung/android/sdk/cover/ScoverManager;

    .line 54
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->addIntentBroadCast()V

    .line 56
    invoke-virtual {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkSettingCondition(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z

    .line 58
    invoke-direct {p0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerScontextListener()V

    .line 60
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.SCREEN_ON"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 61
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.SCREEN_OFF"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 62
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mScreenReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 64
    invoke-super {p0, p1, p2, p3}, Landroid/app/Service;->onStartCommand(Landroid/content/Intent;II)I

    move-result v1

    return v1
.end method

.method public setLEDNotification(Z)V
    .locals 9
    .param p1, "ledOn"    # Z

    .prologue
    const/16 v8, 0x207f

    const/16 v7, 0xff

    const/4 v6, 0x0

    .line 68
    const-string v3, "AirWakeupService"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "setLEDNotification() LED ON : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/secutil/Log;->secV(Ljava/lang/String;Ljava/lang/String;)I

    .line 69
    const/16 v0, 0x207f

    .line 70
    .local v0, "LED_NOTIF_ID":I
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 71
    .local v2, "notiMgr":Landroid/app/NotificationManager;
    iput-boolean p1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mledOn:Z

    .line 72
    if-eqz p1, :cond_0

    .line 73
    new-instance v1, Landroid/app/Notification;

    invoke-direct {v1}, Landroid/app/Notification;-><init>()V

    .line 74
    .local v1, "noti":Landroid/app/Notification;
    invoke-static {v7, v6, v6, v7}, Landroid/graphics/Color;->argb(IIII)I

    move-result v3

    iput v3, v1, Landroid/app/Notification;->ledARGB:I

    .line 75
    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit8 v3, v3, 0x1

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 76
    iget v3, v1, Landroid/app/Notification;->flags:I

    or-int/lit16 v3, v3, 0x400

    iput v3, v1, Landroid/app/Notification;->flags:I

    .line 77
    const/16 v3, 0x3e8

    iput v3, v1, Landroid/app/Notification;->ledOnMS:I

    .line 79
    iput v6, v1, Landroid/app/Notification;->ledOffMS:I

    .line 80
    invoke-virtual {v2, v8, v1}, Landroid/app/NotificationManager;->notify(ILandroid/app/Notification;)V

    .line 84
    .end local v1    # "noti":Landroid/app/Notification;
    :goto_0
    return-void

    .line 82
    :cond_0
    invoke-virtual {v2, v8}, Landroid/app/NotificationManager;->cancel(I)V

    goto :goto_0
.end method
