.class Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;
.super Landroid/content/BroadcastReceiver;
.source "AirWakeupService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/samsung/android/app/airwakeupview/AirWakeupService;->addIntentBroadCast()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;


# direct methods
.method constructor <init>(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V
    .locals 0

    .prologue
    .line 121
    iput-object p1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 123
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 124
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    iget-object v2, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    invoke-virtual {v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->checkSettingCondition(Landroid/content/Context;)Z

    move-result v2

    # setter for: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z
    invoke-static {v1, v2}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$202(Lcom/samsung/android/app/airwakeupview/AirWakeupService;Z)Z

    .line 125
    sget-object v1, Lcom/samsung/android/app/airwakeupview/GlobalConstant;->SETTINGS_AIR_WAKE_UP_CHANGED:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 126
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    # getter for: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->mIsSettingAirWakeUp:Z
    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$200(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 127
    const-string v1, "AirWakeupService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "registerApproachListener()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 128
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    # invokes: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->registerApproachListener()V
    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$300(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    .line 134
    :cond_0
    :goto_0
    return-void

    .line 130
    :cond_1
    const-string v1, "AirWakeupService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "action "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "UnregisterApproachListener()"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/secutil/Log;->secD(Ljava/lang/String;Ljava/lang/String;)I

    .line 131
    iget-object v1, p0, Lcom/samsung/android/app/airwakeupview/AirWakeupService$2;->this$0:Lcom/samsung/android/app/airwakeupview/AirWakeupService;

    # invokes: Lcom/samsung/android/app/airwakeupview/AirWakeupService;->UnregisterApproachListener()V
    invoke-static {v1}, Lcom/samsung/android/app/airwakeupview/AirWakeupService;->access$400(Lcom/samsung/android/app/airwakeupview/AirWakeupService;)V

    goto :goto_0
.end method
