.class Lcom/sec/android/app/snsimagecache/n;
.super Ljava/lang/Thread;


# instance fields
.field final synthetic a:Lcom/sec/android/app/snsimagecache/m;

.field private b:Ljava/lang/Throwable;


# direct methods
.method public constructor <init>(Lcom/sec/android/app/snsimagecache/m;Ljava/lang/String;)V
    .locals 1

    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/n;->a:Lcom/sec/android/app/snsimagecache/m;

    invoke-direct {p0, p2}, Ljava/lang/Thread;-><init>(Ljava/lang/String;)V

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->b:Ljava/lang/Throwable;

    return-void
.end method


# virtual methods
.method public a()Ljava/lang/Throwable;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->b:Ljava/lang/Throwable;

    return-object v0
.end method

.method public run()V
    .locals 3

    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->b:Ljava/lang/Throwable;

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->a:Lcom/sec/android/app/snsimagecache/m;

    invoke-static {v0}, Lcom/sec/android/app/snsimagecache/m;->a(Lcom/sec/android/app/snsimagecache/m;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/SecurityException; {:try_start_0 .. :try_end_0} :catch_1

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/io/IOException;->printStackTrace()V

    const-string v1, "SnsImageCacheHttpSendingThread"

    const-string v2, "SaveReceivedFile : FileSaving Failed : IO EXCEPTION"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->b:Ljava/lang/Throwable;

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/SecurityException;->printStackTrace()V

    const-string v1, "SnsImageCacheHttpSendingThread"

    const-string v2, "SaveReceivedFile : FileSaving Failed : SECURITY EXCEPTION"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/n;->b:Ljava/lang/Throwable;

    goto :goto_0
.end method
