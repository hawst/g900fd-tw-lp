.class public Lcom/sec/android/app/snsimagecache/i;
.super Ljava/lang/Object;


# static fields
.field private static a:I

.field private static b:I

.field private static c:F


# instance fields
.field private d:Landroid/content/Context;

.field private e:Ljava/util/concurrent/CopyOnWriteArrayList;

.field private f:Ljava/util/concurrent/ConcurrentHashMap;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, -0x1

    sput v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/snsimagecache/i;->b:I

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/snsimagecache/i;->c:F

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    return-void
.end method

.method private a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;
    .locals 18

    const/4 v8, 0x0

    const/4 v7, 0x0

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getFilesDir()Ljava/io/File;

    move-result-object v2

    invoke-virtual {v2}, Ljava/io/File;->getAbsolutePath()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "ReceivedFiles"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    sget-object v2, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    const-string v10, "content://com.sec.android.app.snsimagecache.provider/received_image/open/"

    const-string v1, "content://com.sec.android.app.snsimagecache.provider/received_image/"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/app/snsimagecache/i;->c()V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const/4 v3, 0x4

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "_id"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    const-string v5, "file_name"

    aput-object v5, v3, v4

    const/4 v4, 0x2

    const-string v5, "privilege"

    aput-object v5, v3, v4

    const/4 v4, 0x3

    const-string v5, "last_access_time"

    aput-object v5, v3, v4

    const/4 v4, 0x0

    const/4 v5, 0x0

    const-string v6, "last_access_time ASC"

    invoke-virtual/range {v1 .. v6}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    if-eqz v6, :cond_d

    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_9

    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v1

    const/16 v3, 0x3e8

    if-lt v1, v3, :cond_9

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v11

    const/4 v4, 0x0

    const/4 v1, 0x0

    :cond_0
    const-string v3, "_id"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v3, "file_name"

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {v6, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    const-string v13, "privilege"

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v13

    invoke-interface {v6, v13}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    const-string v14, "last_access_time"

    invoke-interface {v6, v14}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v14

    invoke-interface {v6, v14}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v14

    if-nez v4, :cond_1

    move-object v1, v3

    move-object v4, v5

    :cond_1
    if-eqz v13, :cond_2

    const-wide/32 v16, 0x337f9800

    add-long v13, v14, v16

    cmp-long v13, v13, v11

    if-gez v13, :cond_6

    :cond_2
    move-object v1, v5

    :goto_0
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "_id="

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v2, v1, v5}, Landroid/content/ContentResolver;->delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v1

    if-gtz v1, :cond_7

    new-instance v1, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v3, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    move-object/from16 v0, p2

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v1, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_3
    new-instance v1, Ljava/lang/Exception;

    const-string v3, "Received Photo Max Reached !!!"

    invoke-direct {v1, v3}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v1
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    :catch_0
    move-exception v1

    :try_start_1
    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    const/4 v1, 0x0

    const/4 v3, 0x1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :goto_1
    if-nez v3, :cond_5

    :try_start_2
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    const-string v1, "url"

    move-object/from16 v0, p1

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v1, "file_name"

    move-object/from16 v0, p2

    invoke-virtual {v3, v1, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    const-string v4, "privilege"

    if-eqz p3, :cond_a

    const/4 v1, 0x1

    :goto_2
    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    const-string v1, "last_access_time"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v3, v1, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentResolver;->insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;

    move-result-object v1

    if-nez v1, :cond_b

    new-instance v1, Ljava/io/File;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-eqz v2, :cond_4

    invoke-virtual {v1}, Ljava/io/File;->delete()Z
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    :cond_4
    const/4 v1, 0x0

    :cond_5
    :goto_3
    if-eqz v1, :cond_c

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    :goto_4
    return-object v1

    :cond_6
    :try_start_3
    invoke-interface {v6}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_0

    move-object v3, v1

    move-object v1, v4

    goto/16 :goto_0

    :cond_7
    new-instance v1, Ljava/io/File;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v1, v4}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v4

    if-eqz v4, :cond_8

    invoke-virtual {v1}, Ljava/io/File;->delete()Z

    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a()Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v1, v3}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Landroid/net/Uri;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    :cond_9
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    move-object v1, v7

    move v3, v8

    goto/16 :goto_1

    :catchall_0
    move-exception v1

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v1

    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_2

    :cond_b
    :try_start_4
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    move-object/from16 v0, p2

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_1

    move-result-object v1

    goto :goto_3

    :catch_1
    move-exception v1

    invoke-virtual {v1}, Ljava/lang/Exception;->printStackTrace()V

    const/4 v1, 0x0

    goto :goto_3

    :cond_c
    const/4 v1, 0x0

    goto :goto_4

    :cond_d
    move-object v1, v7

    move v3, v8

    goto/16 :goto_1
.end method

.method private a(I)Lcom/sec/android/app/snsimagecache/o;
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :cond_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/o;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v2

    if-ne v2, p1, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private a(Ljava/lang/String;)Ljava/util/Set;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    return-object v0
.end method

.method private b(Ljava/lang/String;)Landroid/net/Uri;
    .locals 9

    const/4 v7, 0x1

    const/4 v4, 0x0

    const/4 v8, 0x0

    const-string v6, "content://com.sec.android.app.snsimagecache.provider/received_image/open/"

    const-string v0, "content://com.sec.android.app.snsimagecache.provider/received_image/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, "file_name"

    aput-object v3, v2, v8

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "url="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {p1}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v2

    if-eqz v2, :cond_2

    :try_start_0
    invoke-interface {v2}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-lez v0, :cond_1

    invoke-interface {v2}, Landroid/database/Cursor;->moveToFirst()Z

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const/4 v1, 0x0

    invoke-interface {v2, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v0

    move v1, v7

    :goto_0
    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move-object v6, v0

    :goto_1
    if-eqz v1, :cond_0

    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    :cond_0
    return-object v4

    :cond_1
    move-object v0, v6

    move v1, v8

    goto :goto_0

    :catch_0
    move-exception v0

    :try_start_1
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    move v1, v8

    goto :goto_1

    :catchall_0
    move-exception v0

    invoke-interface {v2}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_2
    move v1, v7

    goto :goto_1
.end method

.method private b()Lcom/sec/android/app/snsimagecache/o;
    .locals 5

    const/4 v0, 0x0

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v0

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/o;

    add-int/lit8 v1, v1, 0x1

    const/16 v3, 0xa

    if-gt v1, v3, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->a()I

    move-result v3

    const/4 v4, 0x2

    if-gt v3, v4, :cond_0

    :goto_0
    return-object v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private b(I)Ljava/util/ArrayList;
    .locals 4

    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/o;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->d()I

    move-result v3

    if-ne v3, p1, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0

    :cond_1
    return-object v1
.end method

.method private b(Lcom/sec/android/app/snsimagecache/o;)Z
    .locals 3

    const/4 v1, 0x1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_0

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    if-lt v0, v1, :cond_0

    move v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private c(Lcom/sec/android/app/snsimagecache/o;)I
    .locals 3

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-nez v0, :cond_1

    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    :cond_0
    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v0

    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, -0x1

    goto :goto_0
.end method

.method private c()V
    .locals 8

    const-string v0, "content://com.sec.android.app.snsimagecache.provider/received_image/"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-static {}, Lcom/sec/android/app/snsimagecache/h;->a()Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->keySet()Ljava/util/Set;

    move-result-object v0

    :try_start_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v4

    :goto_0
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v5, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->length()I

    move-result v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    const-string v5, "file_name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    invoke-static {v0}, Landroid/database/DatabaseUtils;->sqlEscapeString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    new-instance v5, Landroid/content/ContentValues;

    invoke-direct {v5}, Landroid/content/ContentValues;-><init>()V

    const-string v6, "last_access_time"

    invoke-virtual {v2, v0}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Long;

    invoke-virtual {v5, v6, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-virtual {v0, v1, v5, v6, v7}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    :catch_0
    move-exception v0

    const-string v0, "SnsImageCacheFramework"

    const-string v1, "insertReceivedPhoto : Exception occurred"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    :goto_1
    return-void

    :cond_1
    :try_start_1
    invoke-virtual {v2}, Ljava/util/concurrent/ConcurrentHashMap;->clear()V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method


# virtual methods
.method public a()I
    .locals 1

    sget v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    sget v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    if-gez v0, :cond_0

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    :cond_0
    sget v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    return v0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/o;)I
    .locals 2

    invoke-virtual {p0}, Lcom/sec/android/app/snsimagecache/i;->a()I

    move-result v0

    invoke-virtual {p1, v0}, Lcom/sec/android/app/snsimagecache/o;->b(I)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v1, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    return v0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/p;)V
    .locals 13

    const/4 v1, 0x1

    const/4 v8, 0x0

    const-string v0, "SnsImageCacheFramework"

    const-string v2, "processResponse() : Enter Well~"

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->a()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->b()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    const/4 v4, 0x0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    move-object v7, v0

    check-cast v7, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v7}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a()Landroid/os/Handler;

    move-result-object v10

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/i;->a(I)Lcom/sec/android/app/snsimagecache/o;

    move-result-object v11

    if-eqz v11, :cond_2

    const/4 v0, 0x5

    invoke-virtual {v11, v0}, Lcom/sec/android/app/snsimagecache/o;->a(I)V

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->c()Ljava/lang/String;

    move-result-object v0

    if-eqz v0, :cond_4

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/16 v3, 0xc8

    if-ne v2, v3, :cond_4

    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->f()Z

    move-result v3

    invoke-direct {p0, v2, v0, v3}, Lcom/sec/android/app/snsimagecache/i;->a(Ljava/lang/String;Ljava/lang/String;Z)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_4

    move v3, v1

    :goto_0
    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/i;->a(Ljava/lang/String;)Ljava/util/Set;

    move-result-object v0

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v2

    if-lt v2, v1, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :goto_1
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v2

    new-instance v0, Lcom/sec/android/app/snsimagecache/g;

    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->d()I

    move-result v1

    invoke-virtual {v9}, Ljava/lang/Integer;->intValue()I

    move-result v5

    if-eqz v3, :cond_0

    const-string v6, "OK"

    :goto_2
    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/snsimagecache/g;-><init>(IIZLandroid/net/Uri;ILjava/lang/String;)V

    invoke-virtual {v7, v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Lcom/sec/android/app/snsimagecache/g;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v7, v0, v4}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1

    :cond_0
    const-string v6, "Network Error"

    goto :goto_2

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v11}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v11}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    :cond_2
    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/i;->b()Lcom/sec/android/app/snsimagecache/o;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v10, v8}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v10, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v0, "SnsImageCacheFramework"

    const-string v1, "processResponse() : sending a SnsThreadMessage.PROCESS_PHOTO_GET_BODY_REQUEST to MainThread..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    return-void

    :cond_4
    move v3, v8

    goto :goto_0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/k;)Z
    .locals 14

    const/16 v13, 0xa

    const/4 v3, 0x4

    const/4 v6, 0x0

    const/4 v9, 0x1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    check-cast v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    check-cast v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a()Landroid/os/Handler;

    move-result-object v11

    const-string v0, "SnsImageCacheFramework"

    const-string v1, "processRequest() Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    move v1, v6

    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/o;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->a()I

    move-result v0

    if-lt v0, v3, :cond_b

    add-int/lit8 v0, v1, 0x1

    :goto_1
    move v1, v0

    goto :goto_0

    :cond_0
    const-string v0, "SnsImageCacheFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processRequest() : QueueSize["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] ==> START POINT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processRequest() : nReqCount["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-lt v1, v13, :cond_a

    move v10, v9

    :goto_2
    if-nez v10, :cond_9

    move v7, v1

    move v8, v6

    move v10, v9

    :goto_3
    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/i;->b()Lcom/sec/android/app/snsimagecache/o;

    move-result-object v12

    if-eqz v12, :cond_2

    if-ge v7, v13, :cond_2

    :try_start_0
    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/i;->b(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v4

    if-eqz v4, :cond_1

    new-instance v0, Lcom/sec/android/app/snsimagecache/g;

    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->d()I

    move-result v1

    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v2

    const/4 v3, 0x1

    const/16 v5, 0xc8

    invoke-direct/range {v0 .. v5}, Lcom/sec/android/app/snsimagecache/g;-><init>(IIZLandroid/net/Uri;I)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Lcom/sec/android/app/snsimagecache/g;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    const/4 v0, 0x6

    invoke-virtual {v12, v0}, Lcom/sec/android/app/snsimagecache/o;->a(I)V

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v12}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z
    :try_end_0
    .catch Ljava/lang/NullPointerException; {:try_start_0 .. :try_end_0} :catch_0

    :try_start_1
    sget v0, Lcom/sec/android/app/snsimagecache/i;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/snsimagecache/i;->b:I
    :try_end_1
    .catch Ljava/lang/NullPointerException; {:try_start_1 .. :try_end_1} :catch_1

    move v8, v9

    goto :goto_3

    :cond_1
    :try_start_2
    invoke-direct {p0, v12}, Lcom/sec/android/app/snsimagecache/i;->b(Lcom/sec/android/app/snsimagecache/o;)Z

    move-result v0

    if-eqz v0, :cond_6

    invoke-direct {p0, v12}, Lcom/sec/android/app/snsimagecache/i;->c(Lcom/sec/android/app/snsimagecache/o;)I

    move-result v0

    if-le v0, v9, :cond_6

    const/4 v0, 0x3

    invoke-virtual {v12, v0}, Lcom/sec/android/app/snsimagecache/o;->a(I)V

    sget v0, Lcom/sec/android/app/snsimagecache/i;->b:I

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/app/snsimagecache/i;->b:I
    :try_end_2
    .catch Ljava/lang/NullPointerException; {:try_start_2 .. :try_end_2} :catch_0

    goto :goto_3

    :catch_0
    move-exception v0

    :goto_4
    move v10, v9

    :cond_2
    :goto_5
    sget v0, Lcom/sec/android/app/snsimagecache/i;->a:I

    if-gez v0, :cond_8

    const/4 v0, 0x0

    sput v0, Lcom/sec/android/app/snsimagecache/i;->c:F

    :goto_6
    invoke-static {}, Ljava/text/NumberFormat;->getPercentInstance()Ljava/text/NumberFormat;

    move-result-object v0

    const-string v1, "SnsImageCacheFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processRequest() : QueueSize["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v3}, Ljava/util/concurrent/CopyOnWriteArrayList;->size()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "] <== After Cache HIT"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheFramework"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "processRequest() : Cache Hit Ratio ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v3, Lcom/sec/android/app/snsimagecache/i;->c:F

    float-to-double v3, v3

    invoke-virtual {v0, v3, v4}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "]"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz v10, :cond_3

    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/i;->b()Lcom/sec/android/app/snsimagecache/o;

    move-result-object v0

    if-eqz v0, :cond_3

    invoke-virtual {v11, v6}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v11, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v0, "SnsImageCacheFramework"

    const-string v1, "processRequest() : sending a SnsThreadMessage.PROCESS_REQUEST to MainThread..."

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    :cond_3
    if-eqz v10, :cond_4

    if-eqz v8, :cond_5

    :cond_4
    move v6, v9

    :cond_5
    return v6

    :cond_6
    :try_start_3
    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->e()V

    invoke-direct {p0, v12}, Lcom/sec/android/app/snsimagecache/i;->c(Lcom/sec/android/app/snsimagecache/o;)I

    move-result v1

    if-lez v1, :cond_7

    invoke-virtual {v12}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v1

    invoke-virtual {p1, v1, v0}, Lcom/sec/android/app/snsimagecache/k;->a(ILjava/lang/String;)V

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/k;->b()Landroid/os/Handler;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Handler;->obtainMessage()Landroid/os/Message;

    move-result-object v0

    const/4 v1, 0x2

    iput v1, v0, Landroid/os/Message;->what:I

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/k;->b()Landroid/os/Handler;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const/4 v0, 0x4

    invoke-virtual {v12, v0}, Lcom/sec/android/app/snsimagecache/o;->a(I)V
    :try_end_3
    .catch Ljava/lang/NullPointerException; {:try_start_3 .. :try_end_3} :catch_0

    add-int/lit8 v1, v7, 0x1

    move v7, v1

    move v10, v6

    goto/16 :goto_3

    :cond_7
    move v10, v9

    goto/16 :goto_5

    :cond_8
    sget v0, Lcom/sec/android/app/snsimagecache/i;->b:I

    int-to-float v0, v0

    sget v1, Lcom/sec/android/app/snsimagecache/i;->a:I

    add-int/lit8 v1, v1, 0x1

    int-to-float v1, v1

    div-float/2addr v0, v1

    sput v0, Lcom/sec/android/app/snsimagecache/i;->c:F

    goto/16 :goto_6

    :catch_1
    move-exception v0

    move v8, v9

    goto/16 :goto_4

    :cond_9
    move v8, v6

    goto/16 :goto_5

    :cond_a
    move v10, v6

    goto/16 :goto_2

    :cond_b
    move v0, v1

    goto/16 :goto_1
.end method

.method public a(Lcom/sec/android/app/snsimagecache/k;I)Z
    .locals 2

    invoke-direct {p0, p2}, Lcom/sec/android/app/snsimagecache/i;->b(I)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_1
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p0, p1, p2, v0}, Lcom/sec/android/app/snsimagecache/i;->a(Lcom/sec/android/app/snsimagecache/k;II)Z

    goto :goto_1

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/k;II)Z
    .locals 7

    const/4 v3, 0x0

    invoke-direct {p0, p3}, Lcom/sec/android/app/snsimagecache/i;->a(I)Lcom/sec/android/app/snsimagecache/o;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/o;->d()I

    move-result v0

    if-ne v0, p2, :cond_0

    new-instance v0, Lcom/sec/android/app/snsimagecache/g;

    const/4 v4, 0x0

    const/16 v5, 0x3e9

    const-string v6, "Aborted By User"

    move v1, p2

    move v2, p3

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/app/snsimagecache/g;-><init>(IIZLandroid/net/Uri;ILjava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a()Landroid/os/Handler;

    move-result-object v2

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/i;->d:Landroid/content/Context;

    check-cast v1, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Lcom/sec/android/app/snsimagecache/g;)Ljava/lang/Runnable;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    invoke-virtual {p0, p1, p3}, Lcom/sec/android/app/snsimagecache/i;->b(Lcom/sec/android/app/snsimagecache/k;I)Z

    move-result v3

    :cond_0
    return v3
.end method

.method public b(Lcom/sec/android/app/snsimagecache/k;I)Z
    .locals 5

    const/4 v1, 0x1

    invoke-direct {p0, p2}, Lcom/sec/android/app/snsimagecache/i;->a(I)Lcom/sec/android/app/snsimagecache/o;

    move-result-object v2

    if-eqz v2, :cond_4

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->a()I

    move-result v0

    const/4 v3, 0x4

    if-lt v0, v3, :cond_3

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->size()I

    move-result v3

    if-le v3, v1, :cond_1

    invoke-interface {v0}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v3

    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/i;->a(I)Lcom/sec/android/app/snsimagecache/o;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v4, 0x2

    invoke-virtual {v0, v4}, Lcom/sec/android/app/snsimagecache/o;->a(I)V

    goto :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    invoke-virtual {p1, p2}, Lcom/sec/android/app/snsimagecache/k;->b(I)V

    :cond_2
    :goto_1
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->e:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, v2}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    move v0, v1

    :goto_2
    return v0

    :cond_3
    const/4 v3, 0x3

    if-ne v0, v3, :cond_2

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/util/concurrent/ConcurrentHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Set;

    if-eqz v0, :cond_2

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->b()I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v0, v3}, Ljava/util/Set;->remove(Ljava/lang/Object;)Z

    iget-object v3, p0, Lcom/sec/android/app/snsimagecache/i;->f:Ljava/util/concurrent/ConcurrentHashMap;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/o;->c()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    goto :goto_1

    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method
