.class public Lcom/sec/android/app/snsimagecache/o;
.super Ljava/lang/Object;


# instance fields
.field a:I

.field b:I

.field c:I

.field d:I

.field e:Z

.field f:Z

.field g:Z

.field h:Ljava/lang/String;

.field i:Z


# direct methods
.method public constructor <init>(ILjava/lang/String;)V
    .locals 1

    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/sec/android/app/snsimagecache/o;-><init>(ILjava/lang/String;Z)V

    return-void
.end method

.method public constructor <init>(ILjava/lang/String;Z)V
    .locals 2

    const/4 v0, -0x1

    const/4 v1, 0x0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput v0, p0, Lcom/sec/android/app/snsimagecache/o;->a:I

    iput v0, p0, Lcom/sec/android/app/snsimagecache/o;->b:I

    iput v1, p0, Lcom/sec/android/app/snsimagecache/o;->c:I

    iput v1, p0, Lcom/sec/android/app/snsimagecache/o;->d:I

    iput-boolean v1, p0, Lcom/sec/android/app/snsimagecache/o;->e:Z

    iput-boolean v1, p0, Lcom/sec/android/app/snsimagecache/o;->f:Z

    iput-boolean v1, p0, Lcom/sec/android/app/snsimagecache/o;->g:Z

    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/o;->h:Ljava/lang/String;

    iput-boolean v1, p0, Lcom/sec/android/app/snsimagecache/o;->i:Z

    iput p1, p0, Lcom/sec/android/app/snsimagecache/o;->a:I

    invoke-virtual {p0, p2}, Lcom/sec/android/app/snsimagecache/o;->a(Ljava/lang/String;)V

    iput-boolean p3, p0, Lcom/sec/android/app/snsimagecache/o;->i:Z

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/o;->c:I

    return v0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/i;Landroid/os/Handler;)I
    .locals 3

    invoke-virtual {p1, p0}, Lcom/sec/android/app/snsimagecache/i;->a(Lcom/sec/android/app/snsimagecache/o;)I

    move-result v0

    const/4 v1, 0x0

    invoke-virtual {p2, v1}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {p2, v1}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    const-string v1, "SnsImageCacheRequest"

    const-string v2, "SnsImageCacheRequest : send() : sendThreadMessage[PROCESS_REQUEST]"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return v0
.end method

.method public a(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/app/snsimagecache/o;->c:I

    return-void
.end method

.method public a(Ljava/lang/String;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/o;->h:Ljava/lang/String;

    return-void
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/o;->b:I

    return v0
.end method

.method public b(I)V
    .locals 0

    iput p1, p0, Lcom/sec/android/app/snsimagecache/o;->b:I

    return-void
.end method

.method public c()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/o;->h:Ljava/lang/String;

    return-object v0
.end method

.method public d()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/o;->a:I

    return v0
.end method

.method public e()V
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/o;->d:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/app/snsimagecache/o;->d:I

    return-void
.end method

.method public f()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/snsimagecache/o;->i:Z

    return v0
.end method
