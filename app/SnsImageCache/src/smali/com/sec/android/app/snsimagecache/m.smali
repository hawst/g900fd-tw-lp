.class Lcom/sec/android/app/snsimagecache/m;
.super Ljava/lang/Object;

# interfaces
.implements Ljava/lang/Runnable;


# static fields
.field static d:Ljava/lang/Integer;

.field static e:Lorg/apache/http/client/HttpClient;


# instance fields
.field a:Lcom/sec/android/app/snsimagecache/k;

.field b:Lorg/apache/http/HttpResponse;

.field c:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/snsimagecache/m;->e:Lorg/apache/http/client/HttpClient;

    return-void
.end method

.method constructor <init>(Lcom/sec/android/app/snsimagecache/k;)V
    .locals 0

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    return-void
.end method

.method private a(Ljava/lang/String;)Ljava/lang/String;
    .locals 10

    const/4 v1, 0x0

    const/16 v9, 0x2f

    const/16 v8, 0x2e

    const/4 v7, 0x0

    const/4 v6, -0x1

    invoke-static {}, Landroid/webkit/MimeTypeMap;->getSingleton()Landroid/webkit/MimeTypeMap;

    move-result-object v4

    const-string v0, "://"

    invoke-virtual {p1, v0}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    if-eq v0, v6, :cond_8

    const-string v2, "://"

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    add-int/2addr v0, v2

    invoke-virtual {p1, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v0

    :goto_0
    invoke-virtual {v0, v9}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-eq v2, v6, :cond_0

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_0
    const/16 v2, 0x3a

    invoke-virtual {v0, v2}, Ljava/lang/String;->indexOf(I)I

    move-result v2

    if-eq v2, v6, :cond_1

    invoke-virtual {v0, v7, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-virtual {p1, v9}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    if-eq v2, v6, :cond_9

    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    invoke-virtual {p1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    :goto_1
    if-eqz v3, :cond_2

    invoke-virtual {v3, v8}, Ljava/lang/String;->lastIndexOf(I)I

    move-result v2

    :cond_2
    if-eq v2, v6, :cond_3

    if-eqz v3, :cond_3

    add-int/lit8 v1, v2, 0x1

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v2

    invoke-virtual {v3, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    :cond_3
    if-eqz v1, :cond_4

    invoke-virtual {v4, v1}, Landroid/webkit/MimeTypeMap;->getMimeTypeFromExtension(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/m;->b:Lorg/apache/http/HttpResponse;

    const-string v3, "Content-Type"

    invoke-interface {v2, v3}, Lorg/apache/http/HttpResponse;->getHeaders(Ljava/lang/String;)[Lorg/apache/http/Header;

    move-result-object v2

    if-eqz v2, :cond_4

    array-length v3, v2

    if-lez v3, :cond_4

    aget-object v1, v2, v7

    invoke-interface {v1}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v4, v1}, Landroid/webkit/MimeTypeMap;->getExtensionFromMimeType(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :cond_4
    if-nez v1, :cond_5

    const-string v1, "file"

    :cond_5
    sget-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    sget-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    sget-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-gez v2, :cond_6

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    :cond_6
    sget-object v2, Lcom/sec/android/app/snsimagecache/m;->d:Ljava/lang/Integer;

    invoke-static {v2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v4, 0x5f

    invoke-virtual {v0, v8, v4}, Ljava/lang/String;->replace(CC)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v5}, Ljava/lang/Long;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v3, "_"

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "."

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/snsimagecache/u;->a()Z

    move-result v1

    if-eqz v1, :cond_7

    const-string v1, "SnsImageCacheHttpSendingThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SnsImageCacheHttpSendingThread : fileNameGenerator : Generated Name = ["

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "]"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_7
    return-object v0

    :cond_8
    move-object v0, p1

    goto/16 :goto_0

    :cond_9
    move-object v3, v1

    goto/16 :goto_1
.end method

.method private a(Lcom/sec/android/app/snsimagecache/j;)V
    .locals 5

    const/4 v0, 0x0

    invoke-static {}, Lcom/sec/android/app/snsimagecache/u;->a()Z

    move-result v1

    if-eqz v1, :cond_1

    :try_start_0
    const-string v1, "SnsImageCacheHttpSendingThread"

    const-string v2, "############################################################################################"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheHttpSendingThread"

    const-string v2, "# SNS IMAGE-CACHE HTTP REQUEST :                                                           #"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheHttpSendingThread"

    const-string v2, "############################################################################################"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheHttpSendingThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "# Req ID      = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/j;->a()I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheHttpSendingThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "# URI         = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/j;->getURI()Ljava/net/URI;

    move-result-object v3

    invoke-virtual {v3}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "SnsImageCacheHttpSendingThread"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "# RequestLine = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/j;->getRequestLine()Lorg/apache/http/RequestLine;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/j;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v1

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    :goto_0
    array-length v3, v1

    if-ge v0, v3, :cond_0

    const/4 v3, 0x0

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->length()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Ljava/lang/StringBuilder;->delete(II)Ljava/lang/StringBuilder;

    const-string v3, "# Headers     = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    const-string v3, ": "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    aget-object v3, v1, v0

    invoke-interface {v3}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_0
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "# Method      = HTTP-GET"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_1
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1

    :cond_1
    :try_start_1
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "# SNS IMAGE-CACHE HTTP REQUEST :                                                           #"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# Req ID      = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/j;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_1

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_1
.end method

.method static synthetic a(Lcom/sec/android/app/snsimagecache/m;)V
    .locals 0

    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/m;->c()V

    return-void
.end method

.method private a(Lcom/sec/android/app/snsimagecache/p;)V
    .locals 3

    invoke-static {}, Lcom/sec/android/app/snsimagecache/u;->a()Z

    move-result v0

    if-eqz v0, :cond_0

    :try_start_0
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "# SNS IMAGE-CACHE HTTP RESPONSE :                                                          #"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# Req ID       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# HttpStatus   = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# ImageCache Path = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->c()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    :cond_0
    :try_start_1
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "# SNS IMAGE-CACHE HTTP RESPONSE :                                                          #"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# Req ID       = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->a()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "# HttpStatus   = "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/p;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "############################################################################################"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_1

    goto :goto_0

    :catch_1
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0
.end method

.method private declared-synchronized b()Lcom/sec/android/app/snsimagecache/j;
    .locals 4

    monitor-enter p0

    const/4 v1, 0x0

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->g()Ljava/util/concurrent/Semaphore;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/concurrent/Semaphore;->acquireUninterruptibly()V

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->e()Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :cond_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/j;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/j;->b()Z

    move-result v3

    if-nez v3, :cond_0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/snsimagecache/j;->a(Z)V

    :goto_0
    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v1}, Lcom/sec/android/app/snsimagecache/k;->g()Ljava/util/concurrent/Semaphore;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/concurrent/Semaphore;->release()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit p0

    return-object v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    :cond_1
    move-object v0, v1

    goto :goto_0
.end method

.method private c()V
    .locals 5

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "SnsImageCacheHttpSendingThread : saveFile Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v0

    new-instance v1, Ljava/io/File;

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/k;->d()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v2

    if-nez v2, :cond_0

    invoke-virtual {v1}, Ljava/io/File;->mkdir()Z

    const-string v2, "SnsImageCacheHttpSendingThread"

    const-string v3, "SnsImageCacheHttpSendingThread :  make directory..."

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    :cond_0
    new-instance v2, Ljava/io/File;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1}, Ljava/io/File;->getPath()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Ljava/io/File;->separator:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/snsimagecache/m;->c:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v3

    if-nez v3, :cond_1

    invoke-virtual {v1}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-virtual {v2}, Ljava/io/File;->createNewFile()Z

    :cond_1
    invoke-virtual {v2}, Ljava/io/File;->exists()Z

    move-result v1

    if-eqz v1, :cond_2

    invoke-virtual {v2}, Ljava/io/File;->canWrite()Z

    move-result v1

    if-eqz v1, :cond_2

    new-instance v1, Ljava/io/FileOutputStream;

    invoke-direct {v1, v2}, Ljava/io/FileOutputStream;-><init>(Ljava/io/File;)V

    invoke-interface {v0, v1}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->getFD()Ljava/io/FileDescriptor;

    move-result-object v0

    invoke-virtual {v0}, Ljava/io/FileDescriptor;->sync()V

    invoke-virtual {v1}, Ljava/io/FileOutputStream;->close()V

    :cond_2
    return-void
.end method


# virtual methods
.method public a()V
    .locals 13

    const/16 v3, 0xc8

    const/4 v1, 0x0

    const/4 v11, 0x3

    const/4 v2, 0x1

    const/4 v5, 0x0

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v4, "SnsImageCacheHttpSendingThread : doHTTPSend Called !!!"

    invoke-static {v0, v4}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->e()Ljava/util/ArrayList;

    move-result-object v0

    iget-object v4, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v4}, Lcom/sec/android/app/snsimagecache/k;->c()Lorg/apache/http/client/HttpClient;

    move-result-object v4

    sput-object v4, Lcom/sec/android/app/snsimagecache/m;->e:Lorg/apache/http/client/HttpClient;

    sget-object v4, Lcom/sec/android/app/snsimagecache/m;->e:Lorg/apache/http/client/HttpClient;

    if-eqz v4, :cond_9

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_8

    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/m;->b()Lcom/sec/android/app/snsimagecache/j;

    move-result-object v6

    if-eqz v6, :cond_8

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->a()I

    move-result v7

    sget-object v0, Lcom/sec/android/app/snsimagecache/m;->e:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/conn/ClientConnectionManager;->getSchemeRegistry()Lorg/apache/http/conn/scheme/SchemeRegistry;

    move-result-object v0

    const-string v4, "http"

    invoke-virtual {v0, v4}, Lorg/apache/http/conn/scheme/SchemeRegistry;->getScheme(Ljava/lang/String;)Lorg/apache/http/conn/scheme/Scheme;

    move-result-object v0

    invoke-virtual {v0}, Lorg/apache/http/conn/scheme/Scheme;->getDefaultPort()I

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->f()Landroid/content/Context;

    move-result-object v0

    const-string v4, "power"

    invoke-virtual {v0, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/os/PowerManager;

    const/4 v4, 0x1

    const-string v8, "SnsImageCacheHttpSendingThread"

    invoke-virtual {v0, v4, v8}, Landroid/os/PowerManager;->newWakeLock(ILjava/lang/String;)Landroid/os/PowerManager$WakeLock;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_9
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v4

    :try_start_1
    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->acquire()V

    invoke-direct {p0, v6}, Lcom/sec/android/app/snsimagecache/m;->a(Lcom/sec/android/app/snsimagecache/j;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    :try_start_2
    sget-object v0, Lcom/sec/android/app/snsimagecache/m;->e:Lorg/apache/http/client/HttpClient;

    invoke-interface {v0, v6}, Lorg/apache/http/client/HttpClient;->execute(Lorg/apache/http/client/methods/HttpUriRequest;)Lorg/apache/http/HttpResponse;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->b:Lorg/apache/http/HttpResponse;

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : httpClient.execute(req) - CALLED~"

    invoke-static {v0, v8}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Lorg/apache/http/client/ClientProtocolException; {:try_start_2 .. :try_end_2} :catch_1
    .catch Ljava/io/InterruptedIOException; {:try_start_2 .. :try_end_2} :catch_2
    .catch Ljava/net/SocketException; {:try_start_2 .. :try_end_2} :catch_3
    .catch Ljava/net/UnknownHostException; {:try_start_2 .. :try_end_2} :catch_4
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_5
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_6
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    move v0, v5

    move v2, v5

    :goto_0
    :try_start_3
    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->isAborted()Z

    move-result v8

    if-nez v8, :cond_4

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->b:Lorg/apache/http/HttpResponse;

    if-nez v0, :cond_1

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v2, "SnsImageCacheHttpSendingThread : doHTTPSend() - mResponse is NULL !!!!!!! "

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    new-instance v0, Ljava/lang/Exception;

    const-string v2, "Response is Null."

    invoke-direct {v0, v2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;)V

    throw v0
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_0
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    :catch_0
    move-exception v0

    move-object v2, v1

    move-object v1, v4

    :goto_1
    const/16 v3, 0x3e9

    :try_start_4
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v4, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : EXCEPTION OCCUR !!!"

    invoke-static {v4, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_4

    new-instance v0, Lcom/sec/android/app/snsimagecache/p;

    invoke-direct {v0, v7, v3, v2}, Lcom/sec/android/app/snsimagecache/p;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/m;->a(Lcom/sec/android/app/snsimagecache/p;)V

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v2}, Lcom/sec/android/app/snsimagecache/k;->b()Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v3, v6}, Lcom/sec/android/app/snsimagecache/k;->a(Lcom/sec/android/app/snsimagecache/j;)V

    invoke-virtual {v2, v11, v5, v5, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v2, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_0
    :goto_2
    return-void

    :catch_1
    move-exception v0

    :try_start_5
    invoke-virtual {v0}, Lorg/apache/http/client/ClientProtocolException;->printStackTrace()V

    const-string v2, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : ClientProtocolException"

    invoke-static {v2, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v2, "SnsImageCacheHttpSendingThread : doHTTPSend() : Error in the HTTP protocol!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v5

    move v2, v5

    goto :goto_0

    :catch_2
    move-exception v0

    const-string v8, "SnsImageCacheHttpSendingThread"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "SnsImageCacheHttpSendingThread : doHTTPSend() : InterruptedIOException : "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v0}, Ljava/io/InterruptedIOException;->getMessage()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : The process reading/writing to a stream has been interrupted OR Socket timeout occurs before the request has completed!!"

    invoke-static {v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v2

    move v2, v5

    goto/16 :goto_0

    :catch_3
    move-exception v0

    invoke-virtual {v0}, Ljava/net/SocketException;->printStackTrace()V

    const-string v8, "Network unreachable"

    invoke-virtual {v0}, Ljava/net/SocketException;->getMessage()Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v8

    if-eqz v8, :cond_a

    :goto_3
    const-string v8, "SnsImageCacheHttpSendingThread"

    const-string v9, "SnsImageCacheHttpSendingThread : doHTTPSend() : SocketException"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : Error during socket creation or setting options!!"

    invoke-static {v0, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v5

    goto/16 :goto_0

    :catch_4
    move-exception v0

    invoke-virtual {v0}, Ljava/net/UnknownHostException;->printStackTrace()V

    const-string v8, "SnsImageCacheHttpSendingThread"

    const-string v9, "SnsImageCacheHttpSendingThread : doHTTPSend() : UnknownHostException"

    invoke-static {v8, v9, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v5

    goto/16 :goto_0

    :catch_5
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/IllegalArgumentException;->printStackTrace()V

    const-string v2, "SnsImageCacheHttpSendingThread"

    const-string v8, "SnsImageCacheHttpSendingThread : doHTTPSend() : IllegalArgumentException"

    invoke-static {v2, v8, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v2, "SnsImageCacheHttpSendingThread : doHTTPSend() : The method is invoked with an argument which it can not reasonably deal with!!"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v5

    move v2, v5

    goto/16 :goto_0

    :catch_6
    move-exception v0

    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v2, "SnsImageCacheHttpSendingThread : doHTTPSend() : Exception"

    invoke-static {v0, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->abort()V

    move v0, v5

    move v2, v5

    goto/16 :goto_0

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->b:Lorg/apache/http/HttpResponse;

    invoke-interface {v0}, Lorg/apache/http/HttpResponse;->getStatusLine()Lorg/apache/http/StatusLine;

    move-result-object v0

    invoke-interface {v0}, Lorg/apache/http/StatusLine;->getStatusCode()I
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_0
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    move-result v2

    if-ne v2, v3, :cond_2

    :try_start_6
    invoke-virtual {v6}, Lcom/sec/android/app/snsimagecache/j;->getURI()Ljava/net/URI;

    move-result-object v0

    invoke-virtual {v0}, Ljava/net/URI;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/m;->a(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->c:Ljava/lang/String;

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/m;->c:Ljava/lang/String;
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_0
    .catchall {:try_start_6 .. :try_end_6} :catchall_2

    :try_start_7
    new-instance v3, Lcom/sec/android/app/snsimagecache/n;

    iget-object v8, p0, Lcom/sec/android/app/snsimagecache/m;->c:Ljava/lang/String;

    invoke-direct {v3, p0, v8}, Lcom/sec/android/app/snsimagecache/n;-><init>(Lcom/sec/android/app/snsimagecache/m;Ljava/lang/String;)V

    invoke-virtual {v3}, Lcom/sec/android/app/snsimagecache/n;->start()V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_a
    .catchall {:try_start_7 .. :try_end_7} :catchall_3

    :try_start_8
    invoke-virtual {v3}, Lcom/sec/android/app/snsimagecache/n;->join()V

    invoke-virtual {v3}, Lcom/sec/android/app/snsimagecache/n;->a()Ljava/lang/Throwable;

    move-result-object v3

    if-eqz v3, :cond_3

    throw v3
    :try_end_8
    .catch Ljava/lang/InterruptedException; {:try_start_8 .. :try_end_8} :catch_7
    .catch Ljava/lang/Throwable; {:try_start_8 .. :try_end_8} :catch_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_a
    .catchall {:try_start_8 .. :try_end_8} :catchall_3

    :catch_7
    move-exception v1

    :try_start_9
    invoke-virtual {v1}, Ljava/lang/InterruptedException;->printStackTrace()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_a
    .catchall {:try_start_9 .. :try_end_9} :catchall_3

    move v1, v2

    :goto_4
    move v2, v1

    move-object v1, v0

    :cond_2
    :goto_5
    new-instance v0, Lcom/sec/android/app/snsimagecache/p;

    invoke-direct {v0, v7, v2, v1}, Lcom/sec/android/app/snsimagecache/p;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v0}, Lcom/sec/android/app/snsimagecache/m;->a(Lcom/sec/android/app/snsimagecache/p;)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v1}, Lcom/sec/android/app/snsimagecache/k;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/snsimagecache/k;->a(Lcom/sec/android/app/snsimagecache/j;)V

    invoke-virtual {v1, v11, v5, v5, v0}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v1, v0}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v4, :cond_0

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    goto/16 :goto_2

    :cond_3
    move v1, v2

    goto :goto_4

    :catch_8
    move-exception v3

    :try_start_a
    invoke-virtual {v3}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_a
    .catchall {:try_start_a .. :try_end_a} :catchall_3

    const/16 v0, 0x3ea

    move-object v12, v1

    move v1, v0

    move-object v0, v12

    goto :goto_4

    :cond_4
    if-eqz v2, :cond_5

    const/16 v2, 0x3f1

    goto :goto_5

    :cond_5
    if-eqz v0, :cond_6

    const/16 v2, 0x3f2

    goto :goto_5

    :cond_6
    const/16 v2, 0x3e9

    goto :goto_5

    :catchall_0
    move-exception v0

    move-object v4, v1

    move v2, v3

    :goto_6
    new-instance v3, Lcom/sec/android/app/snsimagecache/p;

    invoke-direct {v3, v7, v2, v1}, Lcom/sec/android/app/snsimagecache/p;-><init>(IILjava/lang/String;)V

    invoke-direct {p0, v3}, Lcom/sec/android/app/snsimagecache/m;->a(Lcom/sec/android/app/snsimagecache/p;)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v1}, Lcom/sec/android/app/snsimagecache/k;->b()Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/m;->a:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v2, v6}, Lcom/sec/android/app/snsimagecache/k;->a(Lcom/sec/android/app/snsimagecache/j;)V

    invoke-virtual {v1, v11, v5, v5, v3}, Landroid/os/Handler;->obtainMessage(IIILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    if-eqz v4, :cond_7

    invoke-virtual {v4}, Landroid/os/PowerManager$WakeLock;->release()V

    :cond_7
    throw v0

    :cond_8
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "SnsImageCacheHttpSendingThread : doHTTPSend() : NOTHING to SEND"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :cond_9
    const-string v0, "SnsImageCacheHttpSendingThread"

    const-string v1, "SnsImageCacheHttpSendingThread : doHTTPSend() - mHttpClient is NULL !!!!!!! "

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    :catchall_1
    move-exception v0

    move v2, v3

    goto :goto_6

    :catchall_2
    move-exception v0

    goto :goto_6

    :catchall_3
    move-exception v1

    move-object v12, v1

    move-object v1, v0

    move-object v0, v12

    goto :goto_6

    :catchall_4
    move-exception v0

    move-object v4, v1

    move-object v1, v2

    move v2, v3

    goto :goto_6

    :catch_9
    move-exception v0

    move-object v2, v1

    goto/16 :goto_1

    :catch_a
    move-exception v1

    move-object v2, v0

    move-object v0, v1

    move-object v1, v4

    goto/16 :goto_1

    :cond_a
    move v2, v5

    goto/16 :goto_3
.end method

.method public run()V
    .locals 0

    invoke-virtual {p0}, Lcom/sec/android/app/snsimagecache/m;->a()V

    return-void
.end method
