.class public final Lcom/sec/android/app/snsimagecache/g;
.super Ljava/lang/Object;


# instance fields
.field private a:I

.field private b:I

.field private c:Z

.field private d:Landroid/net/Uri;

.field private e:I

.field private f:Ljava/lang/String;


# direct methods
.method public constructor <init>(IIZLandroid/net/Uri;I)V
    .locals 1

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    iput p1, p0, Lcom/sec/android/app/snsimagecache/g;->a:I

    iput p2, p0, Lcom/sec/android/app/snsimagecache/g;->b:I

    iput-boolean p3, p0, Lcom/sec/android/app/snsimagecache/g;->c:Z

    iput-object p4, p0, Lcom/sec/android/app/snsimagecache/g;->d:Landroid/net/Uri;

    iput p5, p0, Lcom/sec/android/app/snsimagecache/g;->e:I

    const-string v0, "OK"

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/g;->f:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(IIZLandroid/net/Uri;ILjava/lang/String;)V
    .locals 0

    invoke-direct/range {p0 .. p5}, Lcom/sec/android/app/snsimagecache/g;-><init>(IIZLandroid/net/Uri;I)V

    iput-object p6, p0, Lcom/sec/android/app/snsimagecache/g;->f:Ljava/lang/String;

    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/g;->a:I

    return v0
.end method

.method public b()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/g;->b:I

    return v0
.end method

.method public c()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/snsimagecache/g;->c:Z

    return v0
.end method

.method public d()Landroid/net/Uri;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/g;->d:Landroid/net/Uri;

    return-object v0
.end method

.method public e()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/g;->e:I

    return v0
.end method

.method public f()Ljava/lang/String;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/g;->f:Ljava/lang/String;

    return-object v0
.end method
