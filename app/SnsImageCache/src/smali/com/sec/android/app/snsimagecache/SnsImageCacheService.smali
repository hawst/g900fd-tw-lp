.class public Lcom/sec/android/app/snsimagecache/SnsImageCacheService;
.super Landroid/app/Service;


# static fields
.field public static a:Z

.field private static e:I


# instance fields
.field private b:Landroid/os/Handler;

.field private c:Lcom/sec/android/app/snsimagecache/i;

.field private d:Lcom/sec/android/app/snsimagecache/k;

.field private final f:Landroid/os/RemoteCallbackList;

.field private final g:Lcom/sec/android/app/snsimagecache/e;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    const/4 v0, 0x1

    sput v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->e:I

    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    new-instance v0, Lcom/sec/android/app/snsimagecache/r;

    invoke-direct {v0, p0}, Lcom/sec/android/app/snsimagecache/r;-><init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->g:Lcom/sec/android/app/snsimagecache/e;

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/k;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d:Lcom/sec/android/app/snsimagecache/k;

    return-object v0
.end method

.method private a(ILjava/lang/String;)V
    .locals 3

    new-instance v0, Lcom/sec/android/app/snsimagecache/o;

    const/4 v1, 0x1

    invoke-direct {v0, p1, p2, v1}, Lcom/sec/android/app/snsimagecache/o;-><init>(ILjava/lang/String;Z)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c:Lcom/sec/android/app/snsimagecache/i;

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b:Landroid/os/Handler;

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/snsimagecache/o;->a(Lcom/sec/android/app/snsimagecache/i;Landroid/os/Handler;)I

    return-void
.end method

.method private a(ILjava/lang/String;Landroid/net/Uri;)V
    .locals 3

    if-eqz p3, :cond_0

    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    if-nez p1, :cond_1

    const-string v1, "SnsImageCacheService"

    const-string v2, "sendBroadcast() : IMAGE_DOWNLOADED"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.sec.android.app.snsimagecache.action.IMAGE_DOWNLOADED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    :goto_0
    const-string v1, "imageUrl"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "cacheUri"

    invoke-virtual {p3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    const-string v1, "com.sec.android.app.snsimagecache.permission.READ_SNSIMAGECACHE_DB"

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->sendBroadcast(Landroid/content/Intent;Ljava/lang/String;)V

    :cond_0
    return-void

    :cond_1
    const-string v1, "SnsImageCacheService"

    const-string v2, "sendBroadcast() : IMAGE_DELETED"

    invoke-static {v1, v2}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const-string v1, "com.sec.android.app.snsimagecache.action.IMAGE_DELETED"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_0
.end method

.method static synthetic a(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;ILjava/lang/String;Landroid/net/Uri;)V
    .locals 0

    invoke-direct {p0, p1, p2, p3}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(ILjava/lang/String;Landroid/net/Uri;)V

    return-void
.end method

.method static synthetic a(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;Lcom/sec/android/app/snsimagecache/g;)V
    .locals 0

    invoke-direct {p0, p1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b(Lcom/sec/android/app/snsimagecache/g;)V

    return-void
.end method

.method static synthetic b()I
    .locals 2

    sget v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->e:I

    add-int/lit8 v1, v0, 0x1

    sput v1, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->e:I

    return v0
.end method

.method static synthetic b(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/i;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c:Lcom/sec/android/app/snsimagecache/i;

    return-object v0
.end method

.method private b(Lcom/sec/android/app/snsimagecache/g;)V
    .locals 8

    const-string v0, "SnsImageCacheService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invokeCallback() : reqID = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->b()I

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "], result = ["

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->c()Z

    move-result v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "]"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v7

    const/4 v0, 0x0

    move v6, v0

    :goto_0
    if-ge v6, v7, :cond_1

    :try_start_0
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->getBroadcastCookie(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->a()I

    move-result v1

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0, v6}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v0

    check-cast v0, Lcom/sec/android/app/snsimagecache/a;

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->b()I

    move-result v1

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->c()Z

    move-result v2

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->d()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->e()I

    move-result v4

    invoke-virtual {p1}, Lcom/sec/android/app/snsimagecache/g;->f()Ljava/lang/String;

    move-result-object v5

    invoke-interface/range {v0 .. v5}, Lcom/sec/android/app/snsimagecache/a;->a(IZLandroid/net/Uri;ILjava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    :cond_0
    add-int/lit8 v0, v6, 0x1

    move v6, v0

    goto :goto_0

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Landroid/os/RemoteException;->printStackTrace()V

    const-string v1, "SnsImageCacheService"

    const-string v2, "invokeCallback() : FAILED !!!"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    return-void
.end method

.method static synthetic c()I
    .locals 1

    sget v0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->e:I

    return v0
.end method

.method static synthetic c(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Landroid/os/RemoteCallbackList;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->f:Landroid/os/RemoteCallbackList;

    return-object v0
.end method

.method static synthetic d(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b:Landroid/os/Handler;

    return-object v0
.end method

.method private d()V
    .locals 1

    new-instance v0, Lcom/sec/android/app/snsimagecache/q;

    invoke-direct {v0, p0}, Lcom/sec/android/app/snsimagecache/q;-><init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b:Landroid/os/Handler;

    return-void
.end method


# virtual methods
.method public a()Landroid/os/Handler;
    .locals 1

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b:Landroid/os/Handler;

    return-object v0
.end method

.method public a(Landroid/net/Uri;)Ljava/lang/Runnable;
    .locals 3

    new-instance v0, Lcom/sec/android/app/snsimagecache/t;

    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/sec/android/app/snsimagecache/t;-><init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;ILjava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/g;)Ljava/lang/Runnable;
    .locals 1

    new-instance v0, Lcom/sec/android/app/snsimagecache/s;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/app/snsimagecache/s;-><init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;Lcom/sec/android/app/snsimagecache/g;)V

    return-object v0
.end method

.method public a(Ljava/lang/String;Landroid/net/Uri;)Ljava/lang/Runnable;
    .locals 2

    new-instance v0, Lcom/sec/android/app/snsimagecache/t;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1, p1, p2}, Lcom/sec/android/app/snsimagecache/t;-><init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;ILjava/lang/String;Landroid/net/Uri;)V

    return-object v0
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 3

    const-string v0, "SnsImageCacheService"

    const-string v1, "onBind() Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    const-string v0, "com.sec.android.app.snsimagecache.ISnsImageCacheService"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->g:Lcom/sec/android/app/snsimagecache/e;

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "SnsImageCacheService"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "onBind() : invalid intent - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    const-string v0, "SnsImageCacheService"

    const-string v1, "onCreate() Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d()V

    new-instance v0, Lcom/sec/android/app/snsimagecache/i;

    invoke-direct {v0, p0}, Lcom/sec/android/app/snsimagecache/i;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c:Lcom/sec/android/app/snsimagecache/i;

    new-instance v0, Lcom/sec/android/app/snsimagecache/k;

    invoke-direct {v0, p0}, Lcom/sec/android/app/snsimagecache/k;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d:Lcom/sec/android/app/snsimagecache/k;

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->getState()Ljava/lang/Thread$State;

    move-result-object v0

    sget-object v1, Ljava/lang/Thread$State;->NEW:Ljava/lang/Thread$State;

    if-ne v0, v1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d:Lcom/sec/android/app/snsimagecache/k;

    invoke-virtual {v0}, Lcom/sec/android/app/snsimagecache/k;->a()V

    :cond_0
    return-void
.end method

.method public onDestroy()V
    .locals 2

    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    const-string v0, "SnsImageCacheService"

    const-string v1, "onDestroy() Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 3

    const-string v0, "SnsImageCacheService"

    const-string v1, "onStartCommand() Called !!!"

    invoke-static {v0, v1}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    if-eqz p1, :cond_0

    const-string v0, "imageUrlList"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {v0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    const/4 v2, 0x0

    invoke-direct {p0, v2, v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(ILjava/lang/String;)V

    goto :goto_0

    :cond_0
    const/4 v0, 0x3

    return v0
.end method
