.class public Lcom/sec/android/app/snsimagecache/j;
.super Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;

# interfaces
.implements Lorg/apache/http/client/methods/HttpUriRequest;


# instance fields
.field a:I

.field b:Z


# direct methods
.method constructor <init>(ILjava/lang/String;)V
    .locals 3

    invoke-direct {p0}, Lorg/apache/http/client/methods/HttpEntityEnclosingRequestBase;-><init>()V

    iput p1, p0, Lcom/sec/android/app/snsimagecache/j;->a:I

    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/snsimagecache/j;->b:Z

    :try_start_0
    new-instance v0, Ljava/net/URI;

    invoke-direct {v0, p2}, Ljava/net/URI;-><init>(Ljava/lang/String;)V

    invoke-virtual {p0, v0}, Lcom/sec/android/app/snsimagecache/j;->setURI(Ljava/net/URI;)V
    :try_end_0
    .catch Ljava/net/URISyntaxException; {:try_start_0 .. :try_end_0} :catch_0

    :goto_0
    return-void

    :catch_0
    move-exception v0

    invoke-virtual {v0}, Ljava/net/URISyntaxException;->printStackTrace()V

    const-string v1, "SnsImageCacheHttpGetRequest"

    const-string v2, "SnsImageCacheHttpGetRequest : CONSTRUCTOR : URISyntaxException occur"

    invoke-static {v1, v2, v0}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0
.end method

.method constructor <init>(ILjava/lang/String;Ljava/lang/String;)V
    .locals 1

    invoke-direct {p0, p1, p2}, Lcom/sec/android/app/snsimagecache/j;-><init>(ILjava/lang/String;)V

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/String;->length()I

    move-result v0

    if-lez v0, :cond_0

    const-string v0, "User-Agent"

    invoke-virtual {p0, v0, p3}, Lcom/sec/android/app/snsimagecache/j;->addHeader(Ljava/lang/String;Ljava/lang/String;)V

    :cond_0
    return-void
.end method


# virtual methods
.method public a()I
    .locals 1

    iget v0, p0, Lcom/sec/android/app/snsimagecache/j;->a:I

    return v0
.end method

.method public a(Z)V
    .locals 0

    iput-boolean p1, p0, Lcom/sec/android/app/snsimagecache/j;->b:Z

    return-void
.end method

.method public b()Z
    .locals 1

    iget-boolean v0, p0, Lcom/sec/android/app/snsimagecache/j;->b:Z

    return v0
.end method

.method public getMethod()Ljava/lang/String;
    .locals 1

    const-string v0, "GET"

    return-object v0
.end method
