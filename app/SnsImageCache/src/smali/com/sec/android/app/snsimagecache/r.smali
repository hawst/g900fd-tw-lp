.class Lcom/sec/android/app/snsimagecache/r;
.super Lcom/sec/android/app/snsimagecache/e;


# instance fields
.field final synthetic a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)V
    .locals 0

    iput-object p1, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-direct {p0}, Lcom/sec/android/app/snsimagecache/e;-><init>()V

    return-void
.end method


# virtual methods
.method public a(ILjava/lang/String;)I
    .locals 3

    if-nez p2, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    new-instance v0, Lcom/sec/android/app/snsimagecache/o;

    invoke-direct {v0, p1, p2}, Lcom/sec/android/app/snsimagecache/o;-><init>(ILjava/lang/String;)V

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/i;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v2}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->d(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Landroid/os/Handler;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/app/snsimagecache/o;->a(Lcom/sec/android/app/snsimagecache/i;Landroid/os/Handler;)I

    move-result v0

    goto :goto_0
.end method

.method public a(Lcom/sec/android/app/snsimagecache/a;)I
    .locals 2

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-static {}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b()I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;Ljava/lang/Object;)Z

    invoke-static {}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method public a(I)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/i;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/k;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Lcom/sec/android/app/snsimagecache/i;->a(Lcom/sec/android/app/snsimagecache/k;I)Z

    move-result v0

    return v0
.end method

.method public a(II)Z
    .locals 2

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->b(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/i;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v1}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->a(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Lcom/sec/android/app/snsimagecache/k;

    move-result-object v1

    invoke-virtual {v0, v1, p1, p2}, Lcom/sec/android/app/snsimagecache/i;->a(Lcom/sec/android/app/snsimagecache/k;II)Z

    move-result v0

    return v0
.end method

.method public b(Lcom/sec/android/app/snsimagecache/a;)V
    .locals 1

    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/sec/android/app/snsimagecache/r;->a:Lcom/sec/android/app/snsimagecache/SnsImageCacheService;

    invoke-static {v0}, Lcom/sec/android/app/snsimagecache/SnsImageCacheService;->c(Lcom/sec/android/app/snsimagecache/SnsImageCacheService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    :cond_0
    return-void
.end method
