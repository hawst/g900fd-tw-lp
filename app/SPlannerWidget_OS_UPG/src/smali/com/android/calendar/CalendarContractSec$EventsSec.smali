.class public Lcom/android/calendar/CalendarContractSec$EventsSec;
.super Ljava/lang/Object;
.source "CalendarContractSec.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/CalendarContractSec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventsSec"
.end annotation


# static fields
.field public static final AVAILABILITY_STATUS:Ljava/lang/String; = "availabilityStatus"

.field public static final CONTACT_ACCOUNT_TYPE:Ljava/lang/String; = "contact_account_type"

.field public static final CONTACT_DATA_ID:Ljava/lang/String; = "contact_data_id"

.field public static final CONTACT_EVENT_TYPE:Ljava/lang/String; = "contactEventType"

.field public static final CONTACT_ID:Ljava/lang/String; = "contact_id"

.field public static final FACEBOOK_EVENT_TYPE:Ljava/lang/String; = "facebook_event_type"

.field public static final FACEBOOK_HOSTNAME:Ljava/lang/String; = "facebook_hostname"

.field public static final FACEBOOK_MEM_COUNT:Ljava/lang/String; = "facebook_mem_count"

.field public static final FACEBOOK_OWNER:Ljava/lang/String; = "facebook_owner"

.field public static final FACEBOOK_PHOTO_URL:Ljava/lang/String; = "facebook_photo_url"

.field public static final FACEBOOK_POST_TIME:Ljava/lang/String; = "facebook_post_time"

.field public static final FACEBOOK_SCHEDULE_ID:Ljava/lang/String; = "facebook_schedule_id"

.field public static final FACEBOOK_SERVICE_PROVIDER:Ljava/lang/String; = "facebook_service_provider"

.field public static final LATITUDE:Ljava/lang/String; = "latitude"

.field public static final LONGITUDE:Ljava/lang/String; = "longitude"

.field public static final PHONE_NUMBER:Ljava/lang/String; = "phone_number"

.field public static final STICKER_NAME:Ljava/lang/String; = "sticker_ename"

.field public static final STICKER_TYPE:Ljava/lang/String; = "sticker_type"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
