.class public final Lcom/android/calendar/TaskContract$Tasks;
.super Ljava/lang/Object;
.source "TaskContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/TaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Tasks"
.end annotation


# static fields
.field public static final ACCOUNT_KEY:Ljava/lang/String; = "accountKey"

.field public static final ACCOUNT_NAME:Ljava/lang/String; = "accountName"

.field public static final BODY:Ljava/lang/String; = "body"

.field public static final BODY_SIZE:Ljava/lang/String; = "body_size"

.field public static final BODY_TRUNCATED:Ljava/lang/String; = "body_truncated"

.field public static final BODY_TYPE:Ljava/lang/String; = "bodyType"

.field public static final CATEGORY1:Ljava/lang/String; = "category1"

.field public static final CATEGORY2:Ljava/lang/String; = "category2"

.field public static final CATEGORY3:Ljava/lang/String; = "category3"

.field public static final CLIENT_ID:Ljava/lang/String; = "clientId"

.field public static final COMPLETE:Ljava/lang/String; = "complete"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DATE_COMPLETED:Ljava/lang/String; = "date_completed"

.field public static final DELETED:Ljava/lang/String; = "deleted"

.field public static final DISPLAY_NAME:Ljava/lang/String; = "displayName"

.field public static final DUE_DATE:Ljava/lang/String; = "due_date"

.field public static final GLANCE_DELETED:Ljava/lang/String; = "glance_deleted"

.field public static final GROUP_ID:Ljava/lang/String; = "groupId"

.field public static final IMPORTANCE:Ljava/lang/String; = "importance"

.field public static final INDENT:Ljava/lang/String; = "indent"

.field public static final MAILBOX_KEY:Ljava/lang/String; = "mailboxKey"

.field public static final PARENT_ID:Ljava/lang/String; = "parentId"

.field public static final PREVIOUS_ID:Ljava/lang/String; = "previousId"

.field public static final RECURRENCE_DAY_OF_MONTH:Ljava/lang/String; = "recurrence_day_of_month"

.field public static final RECURRENCE_DAY_OF_WEEK:Ljava/lang/String; = "recurrence_day_of_week"

.field public static final RECURRENCE_DEAD_OCCUR:Ljava/lang/String; = "recurrence_dead_occur"

.field public static final RECURRENCE_INTERVAL:Ljava/lang/String; = "recurrence_interval"

.field public static final RECURRENCE_MONTH_OF_YEAR:Ljava/lang/String; = "recurrence_month_of_year"

.field public static final RECURRENCE_OCCURRENCES:Ljava/lang/String; = "recurrence_occurrences"

.field public static final RECURRENCE_REGENERATE:Ljava/lang/String; = "recurrence_regenerate"

.field public static final RECURRENCE_START:Ljava/lang/String; = "recurrence_start"

.field public static final RECURRENCE_TYPE:Ljava/lang/String; = "recurrence_type"

.field public static final RECURRENCE_UNTIL:Ljava/lang/String; = "recurrence_until"

.field public static final RECURRENCE_WEEK_OF_MONTH:Ljava/lang/String; = "recurrence_week_of_month"

.field public static final REMINDER_SET:Ljava/lang/String; = "reminder_set"

.field public static final REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final SENSITIVITY:Ljava/lang/String; = "sensitivity"

.field public static final SOURCE_ID:Ljava/lang/String; = "sourceid"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final SYNCTIME:Ljava/lang/String; = "syncTime"

.field public static final SYNC_SERVER_ID:Ljava/lang/String; = "syncServerId"

.field public static final TABLE:Ljava/lang/String; = "Tasks"

.field public static final TASKORDER:Ljava/lang/String; = "task_order"

.field public static final UNINDENT:Ljava/lang/String; = "unindent"

.field public static final UTC_DUE_DATE:Ljava/lang/String; = "utc_due_date"

.field public static final UTC_START_DATE:Ljava/lang/String; = "utc_start_date"

.field public static final _SYNC_DIRTY:Ljava/lang/String; = "_sync_dirty"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 95
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/TaskContract$Tasks;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 91
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
