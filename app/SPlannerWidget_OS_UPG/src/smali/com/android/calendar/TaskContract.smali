.class public final Lcom/android/calendar/TaskContract;
.super Ljava/lang/Object;
.source "TaskContract.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/android/calendar/TaskContract$UpdatedTasks;,
        Lcom/android/calendar/TaskContract$DeletedTasks;,
        Lcom/android/calendar/TaskContract$Tasks;,
        Lcom/android/calendar/TaskContract$TasksReminders;,
        Lcom/android/calendar/TaskContract$TaskGroup;,
        Lcom/android/calendar/TaskContract$TasksAccounts;
    }
.end annotation


# static fields
.field public static final AUTHORITY:Ljava/lang/String; = "com.android.calendar"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "Calendar"

.field public static final TASK_REMINDER_ACTION:Ljava/lang/String; = "android.intent.action.TASK_REMINDER"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 31
    const-string v0, "content://com.android.calendar"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/TaskContract;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    return-void
.end method
