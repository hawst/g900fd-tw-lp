.class public Lcom/android/calendar/secfeature/DateToStringConvertor;
.super Ljava/lang/Object;
.source "DateToStringConvertor.java"


# static fields
.field protected static final DAY:I = 0x3

.field protected static final MONTH:I = 0x2

.field protected static final STRING_END:I = -0x2

.field protected static final TOKENS:[C

.field protected static final TOKENS_LEN:I

.field protected static final UNDEFINED:I = -0x1

.field protected static final WEEK_DAY:I = 0x0

.field protected static final YEAR:I = 0x1


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x4

    new-array v0, v0, [C

    fill-array-data v0, :array_0

    sput-object v0, Lcom/android/calendar/secfeature/DateToStringConvertor;->TOKENS:[C

    .line 36
    sget-object v0, Lcom/android/calendar/secfeature/DateToStringConvertor;->TOKENS:[C

    array-length v0, v0

    sput v0, Lcom/android/calendar/secfeature/DateToStringConvertor;->TOKENS_LEN:I

    return-void

    .line 35
    :array_0
    .array-data 2
        0x45s
        0x79s
        0x4ds
        0x64s
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method protected static compareChar(C)I
    .locals 3
    .param p0, "toCompare"    # C

    .prologue
    .line 211
    const/4 v0, -0x1

    .line 212
    .local v0, "res":I
    const/4 v1, 0x0

    .local v1, "t":I
    :goto_0
    sget v2, Lcom/android/calendar/secfeature/DateToStringConvertor;->TOKENS_LEN:I

    if-ge v1, v2, :cond_0

    .line 213
    sget-object v2, Lcom/android/calendar/secfeature/DateToStringConvertor;->TOKENS:[C

    aget-char v2, v2, v1

    if-ne v2, p0, :cond_1

    .line 214
    move v0, v1

    .line 218
    :cond_0
    return v0

    .line 212
    :cond_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0
.end method

.method public static convert(Ljava/lang/String;Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;JZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 18
    .param p0, "dateFromat"    # Ljava/lang/String;
    .param p1, "converter"    # Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .param p2, "timeMillis"    # J
    .param p4, "lunar"    # Z
    .param p5, "numericMonth"    # Z
    .param p6, "lunarAttr"    # Ljava/lang/String;
    .param p7, "leapAttr"    # Ljava/lang/String;

    .prologue
    .line 51
    const/4 v4, -0x1

    .line 52
    .local v4, "charClass":I
    const/4 v11, -0x1

    .line 55
    .local v11, "prevCharClass":I
    new-instance v3, Ljava/lang/StringBuffer;

    invoke-direct {v3}, Ljava/lang/StringBuffer;-><init>()V

    .line 56
    .local v3, "buffer":Ljava/lang/StringBuffer;
    new-instance v12, Ljava/lang/StringBuffer;

    invoke-direct {v12}, Ljava/lang/StringBuffer;-><init>()V

    .line 57
    .local v12, "result":Ljava/lang/StringBuffer;
    new-instance v13, Landroid/text/format/Time;

    invoke-direct {v13}, Landroid/text/format/Time;-><init>()V

    .line 58
    .local v13, "timeToConv":Landroid/text/format/Time;
    move-wide/from16 v0, p2

    invoke-virtual {v13, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 59
    iget v14, v13, Landroid/text/format/Time;->year:I

    .line 60
    .local v14, "year":I
    iget v10, v13, Landroid/text/format/Time;->month:I

    .line 61
    .local v10, "month":I
    iget v7, v13, Landroid/text/format/Time;->monthDay:I

    .line 62
    .local v7, "day":I
    const/4 v8, 0x0

    .line 64
    .local v8, "isLeapMonth":Z
    if-eqz p0, :cond_4

    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v15

    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v15, v0, :cond_4

    if-eqz p1, :cond_4

    .line 65
    const/4 v15, 0x1

    move/from16 v0, p4

    if-ne v0, v15, :cond_0

    .line 66
    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v10, v7}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->convertSolarToLunar(III)V

    .line 67
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getYear()I

    move-result v14

    .line 68
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getMonth()I

    move-result v10

    .line 69
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getDay()I

    move-result v7

    .line 71
    invoke-virtual/range {p1 .. p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth()Z

    move-result v8

    .line 74
    :cond_0
    invoke-virtual/range {p0 .. p0}, Ljava/lang/String;->length()I

    move-result v9

    .line 75
    .local v9, "len":I
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-static {v15}, Lcom/android/calendar/secfeature/DateToStringConvertor;->compareChar(C)I

    move-result v11

    .line 76
    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v15}, Ljava/lang/String;->charAt(I)C

    move-result v15

    invoke-virtual {v3, v15}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 78
    const/4 v6, 0x1

    .local v6, "currPos":I
    :goto_0
    if-gt v6, v9, :cond_3

    .line 79
    if-ge v6, v9, :cond_2

    .line 80
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Ljava/lang/String;->charAt(I)C

    move-result v5

    .line 81
    .local v5, "currChar":C
    invoke-static {v5}, Lcom/android/calendar/secfeature/DateToStringConvertor;->compareChar(C)I

    move-result v4

    .line 86
    :goto_1
    if-eq v4, v11, :cond_1

    .line 87
    packed-switch v11, :pswitch_data_0

    .line 104
    :goto_2
    const/4 v15, 0x0

    invoke-virtual {v3}, Ljava/lang/StringBuffer;->length()I

    move-result v16

    move/from16 v0, v16

    invoke-virtual {v3, v15, v0}, Ljava/lang/StringBuffer;->delete(II)Ljava/lang/StringBuffer;

    .line 105
    move v11, v4

    .line 107
    :cond_1
    invoke-virtual {v3, v5}, Ljava/lang/StringBuffer;->append(C)Ljava/lang/StringBuffer;

    .line 78
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 83
    .end local v5    # "currChar":C
    :cond_2
    const/4 v5, 0x0

    .line 84
    .restart local v5    # "currChar":C
    const/4 v4, -0x2

    goto :goto_1

    .line 89
    :pswitch_0
    invoke-virtual {v12, v3}, Ljava/lang/StringBuffer;->append(Ljava/lang/StringBuffer;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 92
    :pswitch_1
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move-wide/from16 v0, p2

    invoke-static {v15, v0, v1}, Lcom/android/calendar/secfeature/DateToStringConvertor;->getWeekDay(Ljava/lang/String;J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 95
    :pswitch_2
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v14}, Lcom/android/calendar/secfeature/DateToStringConvertor;->getYear(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 98
    :pswitch_3
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    move/from16 v0, p5

    invoke-static {v15, v10, v0}, Lcom/android/calendar/secfeature/DateToStringConvertor;->getMonth(Ljava/lang/String;IZ)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 101
    :pswitch_4
    invoke-virtual {v3}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    invoke-static {v15, v7}, Lcom/android/calendar/secfeature/DateToStringConvertor;->getDay(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    goto :goto_2

    .line 110
    .end local v5    # "currChar":C
    :cond_3
    move/from16 v0, p4

    move-object/from16 v1, p6

    move-object/from16 v2, p7

    invoke-static {v0, v8, v1, v2}, Lcom/android/calendar/secfeature/DateToStringConvertor;->getLunarAttr(ZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Ljava/lang/StringBuffer;->append(Ljava/lang/String;)Ljava/lang/StringBuffer;

    .line 113
    .end local v6    # "currPos":I
    .end local v9    # "len":I
    :cond_4
    invoke-virtual {v12}, Ljava/lang/StringBuffer;->toString()Ljava/lang/String;

    move-result-object v15

    return-object v15

    .line 87
    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method protected static getDay(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0, "dayFormat"    # Ljava/lang/String;
    .param p1, "day"    # I

    .prologue
    const/4 v3, 0x1

    .line 195
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 197
    .local v0, "result":Ljava/lang/String;
    invoke-static {}, Lcom/android/calendar/secfeature/DateToStringConvertor;->isChinese()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 198
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 199
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 207
    :cond_0
    :goto_0
    return-object v0

    .line 202
    :cond_1
    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 203
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "0"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static getLunarAttr(ZZLjava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "lunar"    # Z
    .param p1, "leap"    # Z
    .param p2, "lunarAttr"    # Ljava/lang/String;
    .param p3, "leapAttr"    # Ljava/lang/String;

    .prologue
    .line 117
    const-string v0, ""

    .line 119
    .local v0, "lunarDateAttribute":Ljava/lang/String;
    const/4 v1, 0x1

    if-ne p0, v1, :cond_0

    if-eqz p2, :cond_0

    if-eqz p3, :cond_0

    .line 120
    if-nez p1, :cond_1

    .line 121
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 127
    :cond_0
    :goto_0
    return-object v0

    .line 123
    :cond_1
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " ("

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ")"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method protected static getMonth(Ljava/lang/String;IZ)Ljava/lang/String;
    .locals 12
    .param p0, "monthFormat"    # Ljava/lang/String;
    .param p1, "month"    # I
    .param p2, "isNumeric"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x3

    const/4 v9, 0x1

    const/4 v8, 0x2

    .line 145
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    .line 146
    .local v0, "len":I
    if-ne v0, v8, :cond_0

    .line 147
    const/4 p2, 0x1

    .line 150
    :cond_0
    if-nez p2, :cond_5

    .line 151
    new-instance v6, Ljava/text/DateFormatSymbols;

    invoke-direct {v6}, Ljava/text/DateFormatSymbols;-><init>()V

    .line 152
    .local v6, "symbols":Ljava/text/DateFormatSymbols;
    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getShortMonths()[Ljava/lang/String;

    move-result-object v2

    .line 153
    .local v2, "monthNamesShr":[Ljava/lang/String;
    invoke-virtual {v6}, Ljava/text/DateFormatSymbols;->getMonths()[Ljava/lang/String;

    move-result-object v1

    .line 154
    .local v1, "monthNamesLng":[Ljava/lang/String;
    if-ne v0, v10, :cond_3

    .line 155
    aget-object v5, v2, p1

    .line 160
    .local v5, "result":Ljava/lang/String;
    :goto_0
    invoke-static {}, Lcom/android/calendar/secfeature/DateToStringConvertor;->isChinese()Z

    move-result v7

    if-eqz v7, :cond_1

    .line 161
    const-string v3, ""

    .line 162
    .local v3, "monthNumber":Ljava/lang/String;
    const-string v4, ""

    .line 163
    .local v4, "monthString":Ljava/lang/String;
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v8, :cond_4

    .line 164
    invoke-virtual {v5, v11, v9}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 165
    invoke-virtual {v5, v9, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 167
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 180
    .end local v1    # "monthNamesLng":[Ljava/lang/String;
    .end local v2    # "monthNamesShr":[Ljava/lang/String;
    .end local v3    # "monthNumber":Ljava/lang/String;
    .end local v4    # "monthString":Ljava/lang/String;
    .end local v6    # "symbols":Ljava/text/DateFormatSymbols;
    :cond_1
    :goto_1
    invoke-static {}, Lcom/android/calendar/secfeature/DateToStringConvertor;->isChinese()Z

    move-result v7

    if-eqz v7, :cond_6

    .line 181
    if-eqz p2, :cond_2

    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_2

    .line 182
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 190
    :cond_2
    :goto_2
    return-object v5

    .line 157
    .end local v5    # "result":Ljava/lang/String;
    .restart local v1    # "monthNamesLng":[Ljava/lang/String;
    .restart local v2    # "monthNamesShr":[Ljava/lang/String;
    .restart local v6    # "symbols":Ljava/text/DateFormatSymbols;
    :cond_3
    aget-object v5, v1, p1

    .restart local v5    # "result":Ljava/lang/String;
    goto :goto_0

    .line 169
    .restart local v3    # "monthNumber":Ljava/lang/String;
    .restart local v4    # "monthString":Ljava/lang/String;
    :cond_4
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v10, :cond_1

    .line 170
    invoke-virtual {v5, v11, v8}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v3

    .line 171
    invoke-virtual {v5, v8, v10}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    .line 173
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_1

    .line 177
    .end local v1    # "monthNamesLng":[Ljava/lang/String;
    .end local v2    # "monthNamesShr":[Ljava/lang/String;
    .end local v3    # "monthNumber":Ljava/lang/String;
    .end local v4    # "monthString":Ljava/lang/String;
    .end local v5    # "result":Ljava/lang/String;
    .end local v6    # "symbols":Ljava/text/DateFormatSymbols;
    :cond_5
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    add-int/lit8 v8, p1, 0x1

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .restart local v5    # "result":Ljava/lang/String;
    goto :goto_1

    .line 185
    :cond_6
    invoke-virtual {v5}, Ljava/lang/String;->length()I

    move-result v7

    if-ne v7, v9, :cond_2

    .line 186
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "0"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_2
.end method

.method protected static getWeekDay(Ljava/lang/String;J)Ljava/lang/String;
    .locals 1
    .param p0, "weekDayFormat"    # Ljava/lang/String;
    .param p1, "timeMillis"    # J

    .prologue
    .line 131
    invoke-static {p0, p1, p2}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected static getYear(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p0, "yearFormat"    # Ljava/lang/String;
    .param p1, "year"    # I

    .prologue
    const/4 v3, 0x4

    .line 136
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, ""

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 137
    .local v0, "result":Ljava/lang/String;
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    invoke-virtual {v0}, Ljava/lang/String;->length()I

    move-result v1

    if-ne v1, v3, :cond_0

    .line 138
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    .line 140
    :cond_0
    return-object v0
.end method

.method protected static isChinese()Z
    .locals 2

    .prologue
    .line 222
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method
