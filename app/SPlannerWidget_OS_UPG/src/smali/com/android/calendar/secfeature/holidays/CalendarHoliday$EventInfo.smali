.class public Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;
.super Ljava/lang/Object;
.source "CalendarHoliday.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "EventInfo"
.end annotation


# instance fields
.field public description:Ljava/lang/String;

.field public lunar:Z

.field public startTime:Landroid/text/format/Time;

.field public title:Ljava/lang/String;


# direct methods
.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "t"    # Ljava/lang/String;
    .param p2, "des"    # Ljava/lang/String;
    .param p3, "start"    # Ljava/lang/String;
    .param p4, "lun"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    invoke-static {p1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->title:Ljava/lang/String;

    .line 37
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->startTime:Landroid/text/format/Time;

    .line 38
    iget-object v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->startTime:Landroid/text/format/Time;

    invoke-virtual {v0, p3}, Landroid/text/format/Time;->parse3339(Ljava/lang/String;)Z

    .line 39
    iput-boolean p4, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->lunar:Z

    .line 40
    invoke-static {p2}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;->description:Ljava/lang/String;

    .line 41
    return-void
.end method
