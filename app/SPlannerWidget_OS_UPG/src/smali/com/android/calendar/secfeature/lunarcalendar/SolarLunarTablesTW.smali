.class public Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;
.super Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
.source "SolarLunarTablesTW.java"


# instance fields
.field mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;-><init>()V

    .line 23
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-direct {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    .line 27
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    iget-object v0, v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->lunarCHN:[B

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->lunar:[B

    .line 28
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    iget-object v0, v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->accumulatedLunarDaysCHN:[I

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->accumulatedLunarDays:[I

    .line 29
    return-void
.end method


# virtual methods
.method public isLunarHoliday(IIIZ)Z
    .locals 3
    .param p1, "lYear"    # I
    .param p2, "lMonth"    # I
    .param p3, "lDay"    # I
    .param p4, "isLeap"    # Z

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 32
    if-eqz p4, :cond_1

    .line 46
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    if-nez p2, :cond_2

    if-ne p3, v1, :cond_2

    move v0, v1

    .line 37
    goto :goto_0

    .line 39
    :cond_2
    const/4 v2, 0x4

    if-ne p2, v2, :cond_3

    const/4 v2, 0x5

    if-ne p3, v2, :cond_3

    move v0, v1

    .line 40
    goto :goto_0

    .line 42
    :cond_3
    const/4 v2, 0x7

    if-ne p2, v2, :cond_0

    const/16 v2, 0xf

    if-ne p3, v2, :cond_0

    move v0, v1

    .line 43
    goto :goto_0
.end method

.method public isOtherHoliday(Landroid/text/format/Time;)Z
    .locals 3
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v0, 0x1

    .line 50
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v0, :cond_1

    .line 63
    :cond_0
    :goto_0
    return v0

    .line 53
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v0, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x1c

    if-eq v1, v2, :cond_0

    .line 56
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xa

    if-eq v1, v2, :cond_0

    .line 59
    :cond_3
    iget-object v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesTW;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-virtual {v1, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->isTombSeeping(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 63
    const/4 v0, 0x0

    goto :goto_0
.end method
