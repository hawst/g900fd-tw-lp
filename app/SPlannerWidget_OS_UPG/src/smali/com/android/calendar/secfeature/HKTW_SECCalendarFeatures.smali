.class public Lcom/android/calendar/secfeature/HKTW_SECCalendarFeatures;
.super Lcom/android/calendar/secfeature/SECCalendarFeatures;
.source "HKTW_SECCalendarFeatures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;-><init>()V

    .line 31
    return-void
.end method


# virtual methods
.method public are24SoloarTermsSupported()Z
    .locals 1

    .prologue
    .line 68
    const/4 v0, 0x0

    return v0
.end method

.method public areNationalHolidaysSupported()Z
    .locals 1

    .prologue
    .line 64
    const/4 v0, 0x0

    return v0
.end method

.method public getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHolidayTitleColor()I
    .locals 1

    .prologue
    .line 72
    const/high16 v0, -0x1000000

    return v0
.end method

.method public getLocale()I
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x6

    return v0
.end method

.method public getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .locals 2

    .prologue
    .line 42
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    new-instance v1, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;-><init>(Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;)V

    .line 43
    .local v0, "converter":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    return-object v0
.end method

.method public getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    .locals 1

    .prologue
    .line 47
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-direct {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;-><init>()V

    .line 48
    .local v0, "tables":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    return-object v0
.end method

.method public isExpandMonthViewHeight()Z
    .locals 1

    .prologue
    .line 60
    const/4 v0, 0x1

    return v0
.end method

.method public isLunarCalendarSupported()Z
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x1

    return v0
.end method

.method public isLunarDateDisplayOnFirstDay()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x1

    return v0
.end method
