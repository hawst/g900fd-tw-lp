.class public Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;
.super Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
.source "SolarLunarTablesHK.java"


# instance fields
.field mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;-><init>()V

    .line 23
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-direct {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;-><init>()V

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    .line 27
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    iget-object v0, v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->lunarCHN:[B

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->lunar:[B

    .line 28
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    iget-object v0, v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->accumulatedLunarDaysCHN:[I

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->accumulatedLunarDays:[I

    .line 29
    return-void
.end method

.method private isEasterDays(Landroid/text/format/Time;)Z
    .locals 14
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 82
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 83
    .local v1, "stGoodFriday":Landroid/text/format/Time;
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 85
    .local v0, "stEasterMonday":Landroid/text/format/Time;
    iget v11, p1, Landroid/text/format/Time;->year:I

    iput v11, v1, Landroid/text/format/Time;->year:I

    .line 89
    iget v11, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v2, v11, 0x64

    .line 90
    .local v2, "temp1":I
    iget v11, v1, Landroid/text/format/Time;->year:I

    iget v12, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v12, v12, 0x13

    mul-int/lit8 v12, v12, 0x13

    sub-int v3, v11, v12

    .line 91
    .local v3, "temp2":I
    add-int/lit8 v11, v2, -0x11

    div-int/lit8 v4, v11, 0x19

    .line 92
    .local v4, "temp3":I
    div-int/lit8 v11, v2, 0x4

    sub-int v12, v2, v4

    div-int/lit8 v12, v12, 0x3

    add-int/2addr v11, v12

    sub-int v11, v2, v11

    mul-int/lit8 v12, v3, 0x13

    add-int/2addr v11, v12

    add-int/lit8 v5, v11, 0xf

    .line 93
    .local v5, "temp4":I
    div-int/lit8 v11, v5, 0x1e

    mul-int/lit8 v11, v11, 0x1e

    sub-int v6, v5, v11

    .line 94
    .local v6, "temp5":I
    div-int/lit8 v11, v6, 0x1c

    div-int/lit8 v12, v6, 0x1c

    rsub-int/lit8 v12, v12, 0x1

    mul-int/2addr v11, v12

    const/16 v12, 0x1d

    add-int/lit8 v13, v6, 0x1

    div-int/2addr v12, v13

    mul-int/2addr v11, v12

    rsub-int/lit8 v12, v3, 0x15

    div-int/lit8 v12, v12, 0xb

    mul-int/2addr v11, v12

    sub-int v7, v6, v11

    .line 95
    .local v7, "temp6":I
    iget v11, v1, Landroid/text/format/Time;->year:I

    iget v12, v1, Landroid/text/format/Time;->year:I

    div-int/lit8 v12, v12, 0x4

    add-int/2addr v11, v12

    add-int/2addr v11, v6

    add-int/lit8 v11, v11, 0x2

    sub-int/2addr v11, v2

    div-int/lit8 v12, v2, 0x4

    add-int v8, v11, v12

    .line 96
    .local v8, "temp7":I
    div-int/lit8 v11, v8, 0x7

    mul-int/lit8 v11, v11, 0x7

    sub-int v9, v8, v11

    .line 97
    .local v9, "temp8":I
    sub-int v10, v7, v9

    .line 99
    .local v10, "temp9":I
    add-int/lit8 v11, v10, 0x28

    div-int/lit8 v11, v11, 0x2c

    add-int/lit8 v11, v11, 0x3

    iput v11, v1, Landroid/text/format/Time;->month:I

    .line 100
    add-int/lit8 v11, v10, 0x1a

    iget v12, v1, Landroid/text/format/Time;->month:I

    div-int/lit8 v12, v12, 0x4

    mul-int/lit8 v12, v12, 0x1f

    sub-int/2addr v11, v12

    iput v11, v1, Landroid/text/format/Time;->monthDay:I

    .line 101
    iget v11, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v1, Landroid/text/format/Time;->month:I

    .line 103
    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 104
    iget v11, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v11, v11, 0x3

    iput v11, v0, Landroid/text/format/Time;->monthDay:I

    .line 106
    iget v11, v1, Landroid/text/format/Time;->monthDay:I

    if-gtz v11, :cond_0

    .line 107
    iget v11, v1, Landroid/text/format/Time;->month:I

    add-int/lit8 v11, v11, -0x1

    iput v11, v1, Landroid/text/format/Time;->month:I

    .line 108
    iget v11, v1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v11, v11, 0x1f

    iput v11, v1, Landroid/text/format/Time;->monthDay:I

    .line 111
    :cond_0
    const/4 v11, 0x1

    invoke-virtual {v1, v11}, Landroid/text/format/Time;->normalize(Z)J

    .line 112
    const/4 v11, 0x1

    invoke-virtual {v0, v11}, Landroid/text/format/Time;->normalize(Z)J

    .line 114
    iget v11, p1, Landroid/text/format/Time;->month:I

    iget v12, v1, Landroid/text/format/Time;->month:I

    if-ne v11, v12, :cond_1

    iget v11, p1, Landroid/text/format/Time;->monthDay:I

    iget v12, v1, Landroid/text/format/Time;->monthDay:I

    if-ne v11, v12, :cond_1

    .line 115
    const/4 v11, 0x1

    .line 120
    :goto_0
    return v11

    .line 116
    :cond_1
    iget v11, p1, Landroid/text/format/Time;->month:I

    iget v12, v0, Landroid/text/format/Time;->month:I

    if-ne v11, v12, :cond_2

    iget v11, p1, Landroid/text/format/Time;->monthDay:I

    iget v12, v0, Landroid/text/format/Time;->monthDay:I

    if-ne v11, v12, :cond_2

    .line 117
    const/4 v11, 0x1

    goto :goto_0

    .line 120
    :cond_2
    const/4 v11, 0x0

    goto :goto_0
.end method


# virtual methods
.method public isLunarHoliday(IIIZ)Z
    .locals 5
    .param p1, "lYear"    # I
    .param p2, "lMonth"    # I
    .param p3, "lDay"    # I
    .param p4, "isLeap"    # Z

    .prologue
    const/16 v4, 0x8

    const/4 v3, 0x3

    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 32
    if-eqz p4, :cond_1

    .line 52
    :cond_0
    :goto_0
    return v0

    .line 36
    :cond_1
    if-nez p2, :cond_3

    if-eq p3, v1, :cond_2

    const/4 v2, 0x2

    if-eq p3, v2, :cond_2

    if-ne p3, v3, :cond_3

    :cond_2
    move v0, v1

    .line 37
    goto :goto_0

    .line 39
    :cond_3
    if-ne p2, v3, :cond_4

    if-ne p3, v4, :cond_4

    move v0, v1

    .line 40
    goto :goto_0

    .line 42
    :cond_4
    const/4 v2, 0x4

    if-ne p2, v2, :cond_5

    const/4 v2, 0x5

    if-ne p3, v2, :cond_5

    move v0, v1

    .line 43
    goto :goto_0

    .line 45
    :cond_5
    const/4 v2, 0x7

    if-ne p2, v2, :cond_6

    const/16 v2, 0xf

    if-ne p3, v2, :cond_6

    move v0, v1

    .line 46
    goto :goto_0

    .line 48
    :cond_6
    if-ne p2, v4, :cond_0

    const/16 v2, 0x9

    if-ne p3, v2, :cond_0

    move v0, v1

    .line 49
    goto :goto_0
.end method

.method public isOtherHoliday(Landroid/text/format/Time;)Z
    .locals 3
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v0, 0x1

    .line 56
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v0, :cond_1

    .line 78
    :cond_0
    :goto_0
    return v0

    .line 59
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x4

    if-ne v1, v2, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 62
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 65
    :cond_3
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_4

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-eq v1, v0, :cond_0

    .line 68
    :cond_4
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_5

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x19

    if-eq v1, v2, :cond_0

    .line 71
    :cond_5
    iget-object v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->mTablesCHN:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;

    invoke-virtual {v1, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesCHN;->isTombSeeping(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 74
    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;->isEasterDays(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 78
    const/4 v0, 0x0

    goto :goto_0
.end method
