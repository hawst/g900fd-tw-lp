.class public Lcom/android/calendar/secfeature/SECCalendarFeatures;
.super Ljava/lang/Object;
.source "SECCalendarFeatures.java"


# static fields
.field public static final LOC_CHINA:I = 0x2

.field public static final LOC_DEFAULT:I = -0x1

.field public static final LOC_HKTW:I = 0x6

.field public static final LOC_HONGKONG:I = 0x5

.field public static final LOC_JAPAN:I = 0x3

.field public static final LOC_KOREA:I = 0x1

.field public static final LOC_TAIWAN:I = 0x4

.field public static final LOC_VI:I = 0x7

.field private static sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 41
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    return-void
.end method

.method public static getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;
    .locals 3

    .prologue
    .line 45
    sget-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    if-nez v1, :cond_0

    .line 46
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v1

    const-string v2, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-virtual {v1, v2}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 48
    .local v0, "CSC":Ljava/lang/String;
    const-string v1, "KOREA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 49
    new-instance v1, Lcom/android/calendar/secfeature/KOR_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/KOR_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 66
    :cond_0
    :goto_0
    sget-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    return-object v1

    .line 50
    :cond_1
    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 51
    new-instance v1, Lcom/android/calendar/secfeature/CHN_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/CHN_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 52
    :cond_2
    const-string v1, "HKTW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 53
    new-instance v1, Lcom/android/calendar/secfeature/HKTW_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/HKTW_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 54
    :cond_3
    const-string v1, "HONGKONG"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_4

    .line 55
    new-instance v1, Lcom/android/calendar/secfeature/HK_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/HK_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 56
    :cond_4
    const-string v1, "TAIWAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 57
    new-instance v1, Lcom/android/calendar/secfeature/TW_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/TW_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 58
    :cond_5
    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_6

    .line 59
    new-instance v1, Lcom/android/calendar/secfeature/JPN_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/JPN_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 60
    :cond_6
    const-string v1, "VI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_7

    .line 61
    new-instance v1, Lcom/android/calendar/secfeature/VI_SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/VI_SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0

    .line 63
    :cond_7
    new-instance v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/SECCalendarFeatures;-><init>()V

    sput-object v1, Lcom/android/calendar/secfeature/SECCalendarFeatures;->sInstance:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    goto :goto_0
.end method


# virtual methods
.method public are24SoloarTermsSupported()Z
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x0

    return v0
.end method

.method public areNationalHolidaysSupported()Z
    .locals 1

    .prologue
    .line 103
    const/4 v0, 0x0

    return v0
.end method

.method public areTaskGroupsSupported()Z
    .locals 1

    .prologue
    .line 111
    const/4 v0, 0x0

    return v0
.end method

.method public getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    const/4 v0, 0x0

    return-object v0
.end method

.method public getCalendarSubstituteHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 79
    const/4 v0, 0x0

    return-object v0
.end method

.method public getHolidayTitleColor()I
    .locals 1

    .prologue
    .line 115
    const/high16 v0, -0x1000000

    return v0
.end method

.method public getLocale()I
    .locals 1

    .prologue
    .line 70
    const/4 v0, -0x1

    return v0
.end method

.method public getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .locals 1

    .prologue
    .line 83
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    .locals 1

    .prologue
    .line 87
    const/4 v0, 0x0

    return-object v0
.end method

.method public isExpandMonthViewHeight()Z
    .locals 1

    .prologue
    .line 99
    const/4 v0, 0x0

    return v0
.end method

.method public isLunarCalendarSupported()Z
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public isLunarDateDisplayOnFirstDay()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x0

    return v0
.end method
