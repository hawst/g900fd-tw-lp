.class public Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
.super Ljava/lang/Object;
.source "SolarLunarConverter.java"


# static fields
.field private static final LUNAR_END_YEAR:I = 0x834

.field private static final LUNAR_START_YEAR:I = 0x759

.field private static sIndexOfYear:I


# instance fields
.field private final MAX_LUNAR_YEAR_OFFSET:I

.field private acmDaysInLeapYear_:[I

.field private acmDaysInYear_:[I

.field private day_:I

.field private isLeapMonth_:Z

.field private mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

.field private month_:I

.field private final totalDaysTo18810130:I

.field private year_:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 94
    const/4 v0, -0x1

    sput v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    return-void
.end method

.method public constructor <init>(Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;)V
    .locals 2
    .param p1, "tables"    # Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    .prologue
    const/16 v1, 0xd

    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->acmDaysInYear_:[I

    .line 35
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    iput-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->acmDaysInLeapYear_:[I

    .line 92
    const v0, 0xa7a5e

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->totalDaysTo18810130:I

    .line 93
    const/16 v0, 0xdd

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->MAX_LUNAR_YEAR_OFFSET:I

    .line 40
    iput-object p1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    .line 41
    return-void

    .line 31
    nop

    :array_0
    .array-data 4
        0x0
        0x1f
        0x3b
        0x5a
        0x78
        0x97
        0xb5
        0xd4
        0xf3
        0x111
        0x130
        0x14e
        0x16d
    .end array-data

    .line 35
    :array_1
    .array-data 4
        0x0
        0x1f
        0x3c
        0x5b
        0x79
        0x98
        0xb6
        0xd5
        0xf4
        0x112
        0x131
        0x14f
        0x16e
    .end array-data
.end method

.method private getAccumulatedDays(I)[I
    .locals 1
    .param p1, "year"    # I

    .prologue
    .line 86
    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapYear(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->acmDaysInLeapYear_:[I

    .line 89
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->acmDaysInYear_:[I

    goto :goto_0
.end method

.method private isLeapYear(I)Z
    .locals 2
    .param p1, "y"    # I

    .prologue
    const/4 v0, 0x1

    .line 79
    rem-int/lit8 v1, p1, 0x4

    if-gtz v1, :cond_0

    rem-int/lit8 v1, p1, 0x64

    if-ge v1, v0, :cond_1

    rem-int/lit16 v1, p1, 0x190

    if-lez v1, :cond_1

    .line 80
    :cond_0
    const/4 v0, 0x0

    .line 82
    :cond_1
    return v0
.end method


# virtual methods
.method public convertLunarToSolar(IIIZ)V
    .locals 12
    .param p1, "y"    # I
    .param p2, "m"    # I
    .param p3, "d"    # I
    .param p4, "isLeapMonth"    # Z

    .prologue
    .line 162
    const/16 v9, 0x759

    if-lt p1, v9, :cond_0

    const/16 v9, 0x834

    if-gt p1, v9, :cond_0

    if-ltz p2, :cond_0

    const/16 v9, 0xb

    if-gt p2, v9, :cond_0

    const/4 v9, 0x1

    if-lt p3, v9, :cond_0

    const/16 v9, 0x1e

    if-le p3, v9, :cond_1

    .line 163
    :cond_0
    new-instance v9, Ljava/lang/IllegalArgumentException;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "The date "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " is out of range."

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v9

    .line 167
    :cond_1
    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getAccumulatedDays(I)[I

    move-result-object v1

    .line 169
    .local v1, "days":[I
    add-int/lit16 v3, p1, -0x759

    .line 170
    .local v3, "indexOfYear":I
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v8, v3, 0xe

    .line 173
    .local v8, "startIndexOfYear":I
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v9, v3}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getAccumulatedLunarDays(I)I

    move-result v0

    .line 177
    .local v0, "countOfDays":I
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v10, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v10}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v10, v8, 0xd

    invoke-virtual {v9, v10}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v5

    .line 179
    .local v5, "leapMonth":I
    const/16 v9, 0x7f

    if-ne v5, v9, :cond_2

    const/16 v7, 0xc

    .line 181
    .local v7, "numOfMonth":I
    :goto_0
    const/16 v9, 0xc

    if-ne v7, v9, :cond_3

    .line 182
    const/4 v4, 0x0

    .local v4, "j":I
    :goto_1
    if-ge v4, p2, :cond_6

    .line 183
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int v10, v8, v4

    invoke-virtual {v9, v10}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v9

    add-int/2addr v0, v9

    .line 182
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 179
    .end local v4    # "j":I
    .end local v7    # "numOfMonth":I
    :cond_2
    const/16 v7, 0xd

    goto :goto_0

    .line 186
    .restart local v7    # "numOfMonth":I
    :cond_3
    if-eqz p4, :cond_4

    add-int/lit8 v9, p2, 0x1

    if-ne v9, v5, :cond_4

    .line 187
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_2
    if-ge v4, v5, :cond_6

    .line 188
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int v10, v8, v4

    invoke-virtual {v9, v10}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v9

    add-int/2addr v0, v9

    .line 187
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 192
    .end local v4    # "j":I
    :cond_4
    add-int/lit8 v9, p2, 0x1

    if-le v9, v5, :cond_5

    .line 193
    add-int/lit8 v6, p2, 0x1

    .line 198
    .local v6, "nM":I
    :goto_3
    const/4 v4, 0x0

    .restart local v4    # "j":I
    :goto_4
    if-ge v4, v6, :cond_6

    .line 199
    iget-object v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    add-int v10, v8, v4

    invoke-virtual {v9, v10}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v9

    add-int/2addr v0, v9

    .line 198
    add-int/lit8 v4, v4, 0x1

    goto :goto_4

    .line 195
    .end local v4    # "j":I
    .end local v6    # "nM":I
    :cond_5
    move v6, p2

    .restart local v6    # "nM":I
    goto :goto_3

    .line 205
    .end local v6    # "nM":I
    .restart local v4    # "j":I
    :cond_6
    add-int v9, v0, p3

    add-int/lit8 v0, v9, -0x1

    .line 208
    const/16 v9, 0x759

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    .line 209
    const/4 v9, 0x0

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    .line 210
    const/16 v9, 0x1e

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 212
    const/16 v9, 0x14f

    if-le v0, v9, :cond_a

    .line 214
    const/16 v9, 0x75a

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    .line 215
    const/4 v9, 0x0

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    .line 216
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 217
    add-int/lit16 v0, v0, -0x150

    .line 220
    const/16 v2, 0x16d

    .line 221
    .local v2, "daysOfYear":I
    :goto_5
    if-lt v0, v2, :cond_8

    .line 222
    sub-int/2addr v0, v2

    .line 224
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    .line 225
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    invoke-direct {p0, v9}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapYear(I)Z

    move-result v9

    if-eqz v9, :cond_7

    .line 226
    const/16 v2, 0x16e

    goto :goto_5

    .line 228
    :cond_7
    const/16 v2, 0x16d

    goto :goto_5

    .line 232
    :cond_8
    :goto_6
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v9, v9, 0x1

    aget v9, v1, v9

    if-lt v0, v9, :cond_9

    .line 233
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    goto :goto_6

    .line 235
    :cond_9
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    aget v9, v1, v9

    sub-int/2addr v0, v9

    .line 238
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    add-int/2addr v9, v0

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 256
    .end local v2    # "daysOfYear":I
    :goto_7
    return-void

    .line 240
    :cond_a
    const/4 v9, 0x1

    if-le v0, v9, :cond_c

    .line 241
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    .line 242
    const/4 v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 243
    add-int/lit8 v0, v0, -0x2

    .line 245
    :goto_8
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v9, v9, 0x1

    aget v9, v1, v9

    if-lt v0, v9, :cond_b

    .line 246
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v9, v9, 0x1

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    goto :goto_8

    .line 248
    :cond_b
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    aget v9, v1, v9

    sub-int/2addr v0, v9

    .line 251
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    add-int/2addr v9, v0

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    goto :goto_7

    .line 253
    :cond_c
    iget v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    add-int/2addr v9, v0

    iput v9, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    goto :goto_7
.end method

.method public convertSolarToLunar(III)V
    .locals 11
    .param p1, "y"    # I
    .param p2, "m"    # I
    .param p3, "d"    # I

    .prologue
    const/4 v10, 0x0

    const/4 v6, 0x1

    .line 96
    iput-boolean v10, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    .line 97
    const/16 v7, 0x759

    if-lt p1, v7, :cond_0

    const/16 v7, 0x834

    if-gt p1, v7, :cond_0

    if-ltz p2, :cond_0

    const/16 v7, 0xb

    if-gt p2, v7, :cond_0

    if-lt p3, v6, :cond_0

    const/16 v7, 0x1f

    if-le p3, v7, :cond_1

    .line 98
    :cond_0
    new-instance v6, Ljava/lang/IllegalArgumentException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "The date "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "/"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " is out of range."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 103
    :cond_1
    invoke-virtual {p0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getTotalDaysTo(I)I

    move-result v7

    add-int/2addr v7, p3

    const v8, 0xa7a5e

    sub-int/2addr v7, v8

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 104
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getAccumulatedDays(I)[I

    move-result-object v8

    aget v8, v8, p2

    add-int/2addr v7, v8

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 109
    sget v7, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    if-lez v7, :cond_3

    iget-object v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v7, v7, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    sget v8, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    add-int/lit8 v8, v8, -0x1

    aget v7, v7, v8

    iget v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    if-ge v7, v8, :cond_3

    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-object v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v8, v8, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    sget v9, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    aget v8, v8, v9

    if-gt v7, v8, :cond_3

    .line 112
    sget v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    .line 128
    .local v0, "indexOfYear":I
    :goto_0
    add-int/lit8 v0, v0, -0x1

    .line 130
    iget-object v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v7}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    mul-int/lit8 v5, v0, 0xe

    .line 131
    .local v5, "startIndexOfYear":I
    add-int/lit16 v7, v0, 0x759

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    .line 134
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-object v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v8, v8, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    aget v8, v8, v0

    sub-int/2addr v7, v8

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 137
    iget-object v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v7, v7, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->lunar:[B

    iget-object v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v8}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    add-int/lit8 v8, v5, 0xd

    aget-byte v2, v7, v8

    .line 139
    .local v2, "leapMonth":I
    const/16 v7, 0x7f

    if-ne v2, v7, :cond_7

    const/16 v4, 0xc

    .line 141
    .local v4, "numOfMonth":I
    :goto_1
    const/4 v7, -0x1

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    .line 143
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_2
    if-ge v1, v4, :cond_2

    .line 144
    iget-object v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v7, v7, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->lunar:[B

    add-int v8, v5, v1

    aget-byte v3, v7, v8

    .line 146
    .local v3, "m1":I
    if-ne v2, v1, :cond_8

    .line 147
    iput-boolean v6, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    .line 153
    :goto_3
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    if-gt v7, v3, :cond_9

    .line 159
    .end local v3    # "m1":I
    :cond_2
    return-void

    .line 116
    .end local v0    # "indexOfYear":I
    .end local v1    # "j":I
    .end local v2    # "leapMonth":I
    .end local v4    # "numOfMonth":I
    .end local v5    # "startIndexOfYear":I
    :cond_3
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-object v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v8, v8, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    const/16 v9, 0x6e

    aget v8, v8, v9

    if-gt v7, v8, :cond_5

    move v0, v6

    .line 118
    .restart local v0    # "indexOfYear":I
    :goto_4
    const/16 v7, 0xdd

    if-ge v0, v7, :cond_4

    .line 121
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-object v8, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget-object v8, v8, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    aget v8, v8, v0

    if-gt v7, v8, :cond_6

    .line 125
    :cond_4
    sput v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->sIndexOfYear:I

    goto :goto_0

    .line 116
    .end local v0    # "indexOfYear":I
    :cond_5
    const/16 v0, 0x6f

    goto :goto_4

    .line 118
    .restart local v0    # "indexOfYear":I
    :cond_6
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    .line 139
    .restart local v2    # "leapMonth":I
    .restart local v5    # "startIndexOfYear":I
    :cond_7
    const/16 v4, 0xd

    goto :goto_1

    .line 149
    .restart local v1    # "j":I
    .restart local v3    # "m1":I
    .restart local v4    # "numOfMonth":I
    :cond_8
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v7, v7, 0x1

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    .line 150
    iput-boolean v10, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    goto :goto_3

    .line 157
    :cond_9
    iget v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    sub-int/2addr v7, v3

    iput v7, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    .line 143
    add-int/lit8 v1, v1, 0x1

    goto :goto_2
.end method

.method public getDay()I
    .locals 1

    .prologue
    .line 52
    iget v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    return v0
.end method

.method public getDayLengthOf(IIZ)I
    .locals 1
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "isLeapMonth"    # Z

    .prologue
    .line 267
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v0, p1, p2, p3}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getDayLengthOf(IIZ)I

    move-result v0

    return v0
.end method

.method public getMonth()I
    .locals 1

    .prologue
    .line 48
    iget v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    return v0
.end method

.method public getStringValue()Ljava/lang/String;
    .locals 2

    .prologue
    .line 72
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    instance-of v0, v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesVI;

    if-eqz v0, :cond_0

    .line 73
    iget v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    :goto_0
    return-object v0

    :cond_0
    iget v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    add-int/lit8 v0, v0, 0x1

    invoke-static {v0}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "/"

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    invoke-static {v1}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->concat(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method public getTotalDaysTo(I)I
    .locals 3
    .param p1, "y"    # I

    .prologue
    .line 60
    add-int/lit8 v0, p1, -0x1

    .line 61
    .local v0, "year":I
    mul-int/lit16 v1, v0, 0x16d

    div-int/lit8 v2, v0, 0x4

    add-int/2addr v1, v2

    div-int/lit8 v2, v0, 0x64

    sub-int/2addr v1, v2

    div-int/lit16 v2, v0, 0x190

    add-int/2addr v1, v2

    return v1
.end method

.method public getWeekday(III)I
    .locals 2
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "day"    # I

    .prologue
    .line 259
    const/4 v0, 0x1

    if-gt p2, v0, :cond_0

    .line 260
    add-int/lit8 p2, p2, 0xc

    .line 261
    add-int/lit8 p1, p1, -0x1

    .line 263
    :cond_0
    mul-int/lit8 v0, p2, 0xd

    add-int/lit8 v0, v0, -0xe

    div-int/lit8 v0, v0, 0x5

    add-int/2addr v0, p3

    add-int/2addr v0, p1

    div-int/lit8 v1, p1, 0x4

    add-int/2addr v0, v1

    div-int/lit8 v1, p1, 0x64

    sub-int/2addr v0, v1

    div-int/lit16 v1, p1, 0x190

    add-int/2addr v0, v1

    rem-int/lit8 v0, v0, 0x7

    return v0
.end method

.method public getYear()I
    .locals 1

    .prologue
    .line 44
    iget v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    return v0
.end method

.method public isFirstLunarDay()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 65
    iget v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    if-ne v1, v0, :cond_0

    .line 68
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHoliday(Landroid/text/format/Time;)Z
    .locals 5
    .param p1, "solarTime"    # Landroid/text/format/Time;

    .prologue
    .line 272
    iget v0, p1, Landroid/text/format/Time;->year:I

    iget v1, p1, Landroid/text/format/Time;->month:I

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {p0, v0, v1, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->convertSolarToLunar(III)V

    .line 273
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->isOtherHoliday(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    iget v2, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    iget v3, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-boolean v4, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->isLunarHoliday(IIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isHolidayFst(Landroid/text/format/Time;)Z
    .locals 5
    .param p1, "solarTime"    # Landroid/text/format/Time;

    .prologue
    .line 279
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->isOtherHoliday(Landroid/text/format/Time;)Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    iget v1, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->year_:I

    iget v2, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->month_:I

    iget v3, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->day_:I

    iget-boolean v4, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    invoke-virtual {v0, v1, v2, v3, v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->isLunarHoliday(IIIZ)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isLeapMonth()Z
    .locals 1

    .prologue
    .line 56
    iget-boolean v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth_:Z

    return v0
.end method

.method public isSubstHoliday(Landroid/text/format/Time;)Z
    .locals 1
    .param p1, "solarTime"    # Landroid/text/format/Time;

    .prologue
    .line 283
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->mSolarLunarTables:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;

    invoke-virtual {v0, p1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->isSubstHoliday(Landroid/text/format/Time;)Z

    move-result v0

    return v0
.end method
