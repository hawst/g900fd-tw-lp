.class public Lcom/android/calendar/secfeature/holidays/VICalendarHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "VICalendarHoliday.java"


# static fields
.field private static final VI_HOLIDAYS_COUNT:I = 0x14


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 42
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-direct/range {p0 .. p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 30
    const/16 v35, 0x14

    move/from16 v0, v35

    move-object/from16 v1, p0

    iput v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolidayCount:I

    .line 31
    const/16 v35, 0x14

    move/from16 v0, v35

    new-array v0, v0, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    move-object/from16 v0, v35

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 33
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v27

    .line 34
    .local v27, "r":Landroid/content/res/Resources;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v35, v0

    invoke-virtual/range {v35 .. v35}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v26

    .line 36
    .local v26, "packageName":Ljava/lang/String;
    const-string v35, "vi_holiday_new_years_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v25

    .line 37
    .local v25, "newYearsID":I
    const-string v35, "vi_holiday_valentines_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v30

    .line 38
    .local v30, "valentinesID":I
    const-string v35, "vi_holiday_womens_day"

    const-string v36, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v34

    .line 39
    .local v34, "womensID":I
    const-string v35, "vi_holiday_reunification_day"

    const-string v36, "string"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mContext:Landroid/content/Context;

    move-object/from16 v37, v0

    invoke-virtual/range {v37 .. v37}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v37

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v37

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v28

    .line 40
    .local v28, "reunificationID":I
    const-string v35, "vi_holiday_labor_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v12

    .line 41
    .local v12, "laborID":I
    const-string v35, "vi_holiday_chilrens_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 42
    .local v5, "chilerensID":I
    const-string v35, "vi_holiday_august_revolution_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v4

    .line 43
    .local v4, "augRevolutionID":I
    const-string v35, "vi_holiday_national_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v24

    .line 44
    .local v24, "nationalID":I
    const-string v35, "vi_holiday_vietnamese_womens_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v33

    .line 45
    .local v33, "vietnameseWomensID":I
    const-string v35, "vi_holiday_tearchers_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v29

    .line 46
    .local v29, "tearchersID":I
    const-string v35, "vi_holiday_christmas"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v6

    .line 48
    .local v6, "christmasID":I
    const-string v35, "vi_holiday_vietnamese_new_years_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v32

    .line 49
    .local v32, "vietnameseNewYearsID":I
    const-string v35, "vi_holiday_lantern_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v13

    .line 50
    .local v13, "lanternID":I
    const-string v35, "vi_holiday_cold_food_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v7

    .line 51
    .local v7, "coldFoodID":I
    const-string v35, "vi_holiday_hung_kings_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v10

    .line 52
    .local v10, "hungKingsID":I
    const-string v35, "vi_holiday_vesak_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v31

    .line 53
    .local v31, "vesakID":I
    const-string v35, "vi_holiday_dragon_boat_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v8

    .line 54
    .local v8, "dragonBoatID":I
    const-string v35, "vi_holiday_ghost_day"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v9

    .line 55
    .local v9, "ghostID":I
    const-string v35, "vi_holiday_mid_autumn_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v23

    .line 56
    .local v23, "midAutumnID":I
    const-string v35, "vi_holiday_kitchen_god_festival"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v11

    .line 58
    .local v11, "kitchenGodID":I
    const-string v35, "vi_holiday_lunar_1_1"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v15

    .line 59
    .local v15, "lunar11ID":I
    const-string v35, "vi_holiday_lunar_1_15"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v14

    .line 60
    .local v14, "lunar115ID":I
    const-string v35, "vi_holiday_lunar_3_3"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v18

    .line 61
    .local v18, "lunar33ID":I
    const-string v35, "vi_holiday_lunar_3_10"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v17

    .line 62
    .local v17, "lunar310ID":I
    const-string v35, "vi_holiday_lunar_4_15"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v19

    .line 63
    .local v19, "lunar415ID":I
    const-string v35, "vi_holiday_lunar_5_5"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v20

    .line 64
    .local v20, "lunar55ID":I
    const-string v35, "vi_holiday_lunar_7_15"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v21

    .line 65
    .local v21, "lunar715ID":I
    const-string v35, "vi_holiday_lunar_8_15"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v22

    .line 66
    .local v22, "lunar815ID":I
    const-string v35, "vi_holiday_lunar_12_23"

    const-string v36, "string"

    move-object/from16 v0, v27

    move-object/from16 v1, v35

    move-object/from16 v2, v36

    move-object/from16 v3, v26

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v16

    .line 68
    .local v16, "lunar1223ID":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x0

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1902-01-01"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 69
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x1

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1902-02-14"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 70
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x2

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v34

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1911-03-08"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 71
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x3

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual/range {v27 .. v28}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1975-04-30"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 72
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x4

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1902-05-01"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 73
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x5

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1902-06-01"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 74
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x6

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1945-08-19"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 75
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x7

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1945-09-02"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 76
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x8

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1930-10-20"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 77
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x9

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1958-11-20"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 78
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xa

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    const-string v39, ""

    const-string v40, "1902-12-25"

    const/16 v41, 0x0

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 80
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xb

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    invoke-virtual {v0, v15}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-02-08"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 81
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xc

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    invoke-virtual {v0, v14}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-02-22"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 82
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xd

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v18

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-04-10"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 83
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xe

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-04-17"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 84
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0xf

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v31

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-05-22"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 85
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x10

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v8}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-06-10"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 86
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x11

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v21

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-08-18"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 87
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x12

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v22

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-09-16"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 88
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v35, v0

    const/16 v36, 0x13

    new-instance v37, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    move-object/from16 v0, v27

    invoke-virtual {v0, v11}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v38

    move-object/from16 v0, v27

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v39

    const-string v40, "1902-02-01"

    const/16 v41, 0x1

    invoke-direct/range {v37 .. v41}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v37, v35, v36

    .line 89
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 103
    const/16 v0, 0xff

    invoke-static {v0, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 92
    const-string v1, ""

    .line 93
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/VICalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 94
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/VICalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 96
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "vi_festival_calendar_label"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 97
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/VICalendarHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 99
    return-object v1
.end method
