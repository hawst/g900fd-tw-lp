.class public Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;
.super Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
.source "KORCalendarSubstituteHoliday.java"


# static fields
.field private static final KOR_HOLIDAYS_COUNT:I = 0x1a


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/16 v3, 0x1a

    const/4 v9, 0x0

    .line 30
    invoke-direct {p0, p1}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 32
    iput v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolidayCount:I

    .line 33
    new-array v3, v3, [Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    iput-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    .line 35
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 36
    .local v1, "r":Landroid/content/res/Resources;
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v3}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    .line 38
    .local v0, "packageName":Ljava/lang/String;
    const-string v3, "holiday_substitute_day"

    const-string v4, "string"

    invoke-virtual {v1, v3, v4, v0}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v2

    .line 41
    .local v2, "substHolID":I
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    new-instance v4, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    const-string v6, ""

    const-string v7, "2016-02-10"

    invoke-direct {v4, v5, v6, v7, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v4, v3, v9

    .line 42
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x1

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2017-01-30"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 43
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x2

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2020-01-27"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 44
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x3

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2023-01-24"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 45
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x4

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2024-02-12"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 46
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x5

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2027-02-09"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 47
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x6

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2030-02-05"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 48
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/4 v4, 0x7

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2033-02-02"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 49
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x8

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2034-02-21"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 50
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x9

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2036-01-30"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 52
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xa

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2018-05-07"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 53
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xb

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2019-05-06"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 54
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xc

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2024-05-06"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 55
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xd

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2029-05-07"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 56
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xe

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2030-05-06"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 57
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0xf

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2035-05-06"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 59
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x10

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2014-09-10"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 60
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x11

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2015-09-29"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 61
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x12

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2018-09-26"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 62
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x13

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2022-09-12"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 63
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x14

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2025-10-08"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 64
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x15

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2028-10-05"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 65
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x16

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2029-09-24"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 66
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x17

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2032-09-21"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 67
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x18

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2035-09-18"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 68
    iget-object v3, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mHolEvent:[Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    const/16 v4, 0x19

    new-instance v5, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    const-string v8, "2036-10-06"

    invoke-direct {v5, v6, v7, v8, v9}, Lcom/android/calendar/secfeature/holidays/CalendarHoliday$EventInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    aput-object v5, v3, v4

    .line 69
    return-void
.end method


# virtual methods
.method public getHolidayCalendarAccountColor()I
    .locals 3

    .prologue
    .line 83
    const/16 v0, 0xde

    const/16 v1, 0x54

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getHolidayCalendarName()Ljava/lang/String;
    .locals 6

    .prologue
    .line 72
    const-string v1, ""

    .line 73
    .local v1, "calendarName":Ljava/lang/String;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 74
    .local v3, "r":Landroid/content/res/Resources;
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    .line 76
    .local v2, "packageName":Ljava/lang/String;
    const-string v4, "holiday_substitute_day"

    const-string v5, "string"

    invoke-virtual {v3, v4, v5, v2}, Landroid/content/res/Resources;->getIdentifier(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 77
    .local v0, "calNameID":I
    iget-object v4, p0, Lcom/android/calendar/secfeature/holidays/KORCalendarSubstituteHoliday;->mContext:Landroid/content/Context;

    invoke-virtual {v4, v0}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 79
    return-object v1
.end method
