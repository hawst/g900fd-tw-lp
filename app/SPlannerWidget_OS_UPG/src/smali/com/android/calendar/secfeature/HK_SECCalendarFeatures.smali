.class public Lcom/android/calendar/secfeature/HK_SECCalendarFeatures;
.super Lcom/android/calendar/secfeature/SECCalendarFeatures;
.source "HK_SECCalendarFeatures.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 31
    invoke-direct {p0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;-><init>()V

    .line 32
    return-void
.end method


# virtual methods
.method public are24SoloarTermsSupported()Z
    .locals 1

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public areNationalHolidaysSupported()Z
    .locals 1

    .prologue
    .line 66
    const/4 v0, 0x1

    return v0
.end method

.method public getCalendarHoliday(Landroid/content/Context;)Lcom/android/calendar/secfeature/holidays/CalendarHoliday;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    new-instance v0, Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;

    invoke-direct {v0, p1}, Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;-><init>(Landroid/content/Context;)V

    .line 40
    .local v0, "calHol":Lcom/android/calendar/secfeature/holidays/HKCalendarHoliday;
    return-object v0
.end method

.method public getHolidayTitleColor()I
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 74
    const/16 v0, 0xff

    invoke-static {v0, v1, v1}, Landroid/graphics/Color;->rgb(III)I

    move-result v0

    return v0
.end method

.method public getLocale()I
    .locals 1

    .prologue
    .line 35
    const/4 v0, 0x5

    return v0
.end method

.method public getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    .locals 2

    .prologue
    .line 44
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    new-instance v1, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;

    invoke-direct {v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;-><init>()V

    invoke-direct {v0, v1}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;-><init>(Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;)V

    .line 45
    .local v0, "converter":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;
    return-object v0
.end method

.method public getSolarLunarTables()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    .locals 1

    .prologue
    .line 49
    new-instance v0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;

    invoke-direct {v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTablesHK;-><init>()V

    .line 50
    .local v0, "tables":Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
    return-object v0
.end method

.method public isExpandMonthViewHeight()Z
    .locals 1

    .prologue
    .line 62
    const/4 v0, 0x1

    return v0
.end method

.method public isLunarCalendarSupported()Z
    .locals 1

    .prologue
    .line 54
    const/4 v0, 0x1

    return v0
.end method

.method public isLunarDateDisplayOnFirstDay()Z
    .locals 1

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method
