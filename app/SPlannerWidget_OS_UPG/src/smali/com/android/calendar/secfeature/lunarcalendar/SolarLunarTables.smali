.class public abstract Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;
.super Ljava/lang/Object;
.source "SolarLunarTables.java"


# instance fields
.field public final END_OF_LUNAR_YEAR:I

.field public final INDEX_OF_LEAP_MONTH:I

.field public final START_OF_LUNAR_YEAR:I

.field public final WIDTH_PER_YEAR:I

.field protected accumulatedLunarDays:[I

.field protected lunar:[B


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/16 v0, 0x759

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->START_OF_LUNAR_YEAR:I

    .line 23
    const/16 v0, 0x834

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->END_OF_LUNAR_YEAR:I

    .line 24
    const/16 v0, 0xd

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->INDEX_OF_LEAP_MONTH:I

    .line 25
    const/16 v0, 0xe

    iput v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->WIDTH_PER_YEAR:I

    .line 32
    return-void
.end method


# virtual methods
.method public getAccumulatedLunarDays(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 66
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->accumulatedLunarDays:[I

    aget v0, v0, p1

    return v0
.end method

.method public getDayLengthOf(IIZ)I
    .locals 5
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "leap"    # Z

    .prologue
    .line 35
    const/16 v2, 0x759

    if-lt p1, v2, :cond_0

    const/16 v2, 0x834

    if-gt p1, v2, :cond_0

    if-ltz p2, :cond_0

    const/16 v2, 0xb

    if-le p2, v2, :cond_1

    .line 36
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The month "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is out of range."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 39
    :cond_1
    add-int/lit16 v2, p1, -0x759

    mul-int/lit8 v1, v2, 0xe

    .line 40
    .local v1, "startIndexOfYear":I
    add-int/lit8 v2, v1, 0xd

    invoke-virtual {p0, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v0

    .line 41
    .local v0, "leapMonth":I
    if-nez p3, :cond_2

    if-ge p2, v0, :cond_2

    .line 42
    add-int v2, v1, p2

    invoke-virtual {p0, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v2

    .line 44
    :goto_0
    return v2

    :cond_2
    add-int v2, v1, p2

    add-int/lit8 v2, v2, 0x1

    invoke-virtual {p0, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v2

    goto :goto_0
.end method

.method public getLunar(I)B
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 62
    iget-object v0, p0, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->lunar:[B

    aget-byte v0, v0, p1

    return v0
.end method

.method public isLeapMonth(II)Z
    .locals 5
    .param p1, "year"    # I
    .param p2, "month"    # I

    .prologue
    .line 49
    const/16 v2, 0x759

    if-lt p1, v2, :cond_0

    const/16 v2, 0x834

    if-gt p1, v2, :cond_0

    if-ltz p2, :cond_0

    const/16 v2, 0xc

    if-le p2, v2, :cond_1

    .line 50
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "The month "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "/"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " is out of range."

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 53
    :cond_1
    add-int/lit16 v2, p1, -0x759

    mul-int/lit8 v1, v2, 0xe

    .line 56
    .local v1, "startIndexOfYear":I
    add-int/lit8 v2, v1, 0xd

    invoke-virtual {p0, v2}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarTables;->getLunar(I)B

    move-result v0

    .line 58
    .local v0, "leapMonth":I
    add-int/lit8 v2, v0, -0x1

    if-ne v2, p2, :cond_2

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_2
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public isLunarHoliday(IIIZ)Z
    .locals 1
    .param p1, "lYear"    # I
    .param p2, "lMonth"    # I
    .param p3, "lDay"    # I
    .param p4, "isLeap"    # Z

    .prologue
    .line 70
    const/4 v0, 0x0

    return v0
.end method

.method public isOtherHoliday(Landroid/text/format/Time;)Z
    .locals 1
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 74
    const/4 v0, 0x0

    return v0
.end method

.method public isSubstHoliday(Landroid/text/format/Time;)Z
    .locals 1
    .param p1, "solarTime"    # Landroid/text/format/Time;

    .prologue
    .line 78
    const/4 v0, 0x0

    return v0
.end method
