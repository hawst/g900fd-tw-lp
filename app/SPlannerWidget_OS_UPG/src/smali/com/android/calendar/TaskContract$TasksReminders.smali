.class public final Lcom/android/calendar/TaskContract$TasksReminders;
.super Ljava/lang/Object;
.source "TaskContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/TaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TasksReminders"
.end annotation


# static fields
.field public static final ACCOUNT_KEY:Ljava/lang/String; = "accountkey"

.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final DUE_DATE:Ljava/lang/String; = "due_date"

.field public static final REMINDER_TIME:Ljava/lang/String; = "reminder_time"

.field public static final REMINDER_TYPE:Ljava/lang/String; = "reminder_type"

.field public static final START_DATE:Ljava/lang/String; = "start_date"

.field public static final STATE:Ljava/lang/String; = "state"

.field public static final SUBJECT:Ljava/lang/String; = "subject"

.field public static final TABLE:Ljava/lang/String; = "TasksReminders"

.field public static final TASK_ID:Ljava/lang/String; = "task_id"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 78
    const-string v0, "content://com.android.calendar/TasksReminders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/TaskContract$TasksReminders;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
