.class public final Lcom/android/calendar/CalendarContractSec$Notes;
.super Ljava/lang/Object;
.source "CalendarContractSec.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/CalendarContractSec;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Notes"
.end annotation


# static fields
.field public static final DATE:Ljava/lang/String; = "date"

.field public static final EVENT_ID:Ljava/lang/String; = "event_id"

.field public static final EVENT_TYPE:Ljava/lang/String; = "event_type"

.field public static final FILEPATH:Ljava/lang/String; = "filepath"

.field public static final IMAGEPATH:Ljava/lang/String; = "imagepath"

.field public static final LOCKED:Ljava/lang/String; = "locked"

.field public static final PAGE_NO:Ljava/lang/String; = "page_no"

.field public static final TABLE:Ljava/lang/String; = "Notes"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 71
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
