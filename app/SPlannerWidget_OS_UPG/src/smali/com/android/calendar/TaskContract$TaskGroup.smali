.class public final Lcom/android/calendar/TaskContract$TaskGroup;
.super Ljava/lang/Object;
.source "TaskContract.java"

# interfaces
.implements Landroid/provider/BaseColumns;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/android/calendar/TaskContract;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "TaskGroup"
.end annotation


# static fields
.field public static final CONTENT_URI:Landroid/net/Uri;

.field public static final GROUP_NAME:Ljava/lang/String; = "groupName"

.field public static final GROUP_ORDER:Ljava/lang/String; = "group_order"

.field public static final SELECTED:Ljava/lang/String; = "selected"

.field public static final TABLE:Ljava/lang/String; = "TaskGroup"

.field public static final _ACCOUNT_ID:Ljava/lang/String; = "_accountId"


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 65
    const-string v0, "content://com.android.calendar/TaskGroup"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/android/calendar/TaskContract$TaskGroup;->CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
