.class Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;
.super Ljava/lang/Object;
.source "SPlannerSurfaceWidget.java"

# interfaces
.implements Landroid/content/ServiceConnection;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 0

    .prologue
    .line 74
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onServiceConnected(Landroid/content/ComponentName;Landroid/os/IBinder;)V
    .locals 3
    .param p1, "className"    # Landroid/content/ComponentName;
    .param p2, "service"    # Landroid/os/IBinder;

    .prologue
    .line 80
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;->asInterface(Landroid/os/IBinder;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    move-result-object v2

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$002(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    .line 81
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    move-result-object v1

    if-nez v1, :cond_0

    .line 82
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$100()Ljava/lang/String;

    move-result-object v1

    const-string v2, "ISPlannerSService is null. So, service is not bounded."

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 92
    :goto_0
    return-void

    .line 87
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCallback:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;->registerCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V

    .line 88
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const/4 v2, 0x1

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$302(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Z)Z
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Landroid/os/RemoteException;
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$100()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onServiceDisconnected(Landroid/content/ComponentName;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/ComponentName;

    .prologue
    .line 96
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$002(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    .line 97
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const/4 v1, 0x0

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$302(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Z)Z

    .line 98
    return-void
.end method
