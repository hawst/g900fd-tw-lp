.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;
.super Ljava/lang/Object;
.source "SPlannerPreferenceManager.java"


# static fields
.field public static final FIXED_TIME_ZONE:I = 0x0

.field private static final KEY_HIDE_COMPLETED_TASK:Ljava/lang/String; = "preferences_hide_completed_task"

.field private static final KEY_HIDE_CONTACTS_EVENTS:Ljava/lang/String; = "preferences_hide_contacts_events"

.field private static final KEY_HIDE_DECLINED:Ljava/lang/String; = "preferences_hide_declined"

.field private static final KEY_HOME_TZ_ENABLED:Ljava/lang/String; = "preferences_home_tz_enabled"

.field private static final KEY_NOTIFICATION_MOMENT:Ljava/lang/String; = "preferences_notification_moment"

.field private static final KEY_TODAY_TIMEZONE:Ljava/lang/String; = "preferences_today_tz"

.field private static final KEY_WEATHER_ON:Ljava/lang/String; = "preferences_weather"

.field private static final KEY_WEEK_START_DAY:Ljava/lang/String; = "preferences_week_start_day"

.field public static final LOCAL_TIME_ZONE:I = 0x1

.field private static final PREFERENCE_TYPE_BOOLEAN:Ljava/lang/String; = "/PreferenceBoolean"

.field private static final PREFERENCE_TYPE_STRING:Ljava/lang/String; = "/PreferenceString"

.field private static final TAG:Ljava/lang/String;

.field public static final WEEK_START_DEFAULT:I = -0x1

.field private static sEnabledHomeTimeZone:Z

.field private static sFetchAllPreferenceTimeMillis:J

.field private static sHideCompletedTask:Z

.field private static sHideContactsEvents:Z

.field private static sHideDeclinedEvent:Z

.field private static sIsWeatherOn:Z

.field private static sTodayTimeZone:I

.field private static sWeekStartDay:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->TAG:Ljava/lang/String;

    .line 41
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sFetchAllPreferenceTimeMillis:J

    .line 47
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    .line 48
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    return-void
.end method

.method private static buildUri(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "dataType"    # Ljava/lang/String;

    .prologue
    .line 202
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "content://com.sec.android.calendar.preference"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static enabledHomeTimeZone()Z
    .locals 1

    .prologue
    .line 70
    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sEnabledHomeTimeZone:Z

    return v0
.end method

.method public static declared-synchronized fetchEntirePreference(Landroid/content/Context;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 94
    const-class v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;

    monitor-enter v3

    if-nez p0, :cond_0

    .line 112
    :goto_0
    monitor-exit v3

    return-void

    .line 98
    :cond_0
    :try_start_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 99
    .local v0, "currentTimeMillis":J
    sget-wide v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sFetchAllPreferenceTimeMillis:J

    cmp-long v2, v0, v4

    if-lez v2, :cond_1

    sget-wide v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sFetchAllPreferenceTimeMillis:J

    sub-long v4, v0, v4

    const-wide/16 v6, 0x3e8

    cmp-long v2, v4, v6

    if-gez v2, :cond_1

    .line 100
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->TAG:Ljava/lang/String;

    const-string v4, "already fetched all preferences"

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 94
    .end local v0    # "currentTimeMillis":J
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2

    .line 104
    .restart local v0    # "currentTimeMillis":J
    :cond_1
    :try_start_1
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchWeatherOnState(Landroid/content/Context;)Z

    .line 105
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchHideCompletedTasks(Landroid/content/Context;)Z

    .line 106
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchHideContactEvents(Landroid/content/Context;)Z

    .line 107
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchHideDeclinedEvents(Landroid/content/Context;)Z

    .line 108
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchTodayTimeZone(Landroid/content/Context;)I

    .line 109
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchWeekStartDay(Landroid/content/Context;)I

    .line 111
    sput-wide v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sFetchAllPreferenceTimeMillis:J
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0
.end method

.method public static fetchHideCompletedTasks(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 129
    const-string v2, "/PreferenceBoolean"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 130
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_hide_completed_task"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 131
    .local v0, "queryResult":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideCompletedTask:Z

    .line 132
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideCompletedTask:Z

    return v2

    .line 131
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchHideContactEvents(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 136
    const-string v2, "/PreferenceBoolean"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 137
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_hide_contacts_events"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 138
    .local v0, "queryResult":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideContactsEvents:Z

    .line 139
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideContactsEvents:Z

    return v2

    .line 138
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchHideDeclinedEvents(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 122
    const-string v2, "/PreferenceBoolean"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 123
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_hide_declined"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 124
    .local v0, "queryResult":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideDeclinedEvent:Z

    .line 125
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideDeclinedEvent:Z

    return v2

    .line 124
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchHomeTimeZoneState(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    const-string v2, "/PreferenceBoolean"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 144
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_home_tz_enabled"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 145
    .local v0, "queryResult":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sEnabledHomeTimeZone:Z

    .line 146
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sEnabledHomeTimeZone:Z

    return v2

    .line 145
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchTodayTimeZone(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 150
    const-string v2, "/PreferenceString"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 151
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_today_tz"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForStringType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 152
    .local v0, "result":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 153
    const/4 v2, 0x0

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    .line 154
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    .line 157
    :goto_0
    return v2

    .line 156
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    .line 157
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    goto :goto_0
.end method

.method public static fetchWeatherOnState(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 115
    const-string v2, "/PreferenceBoolean"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 116
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_weather"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    .line 117
    .local v0, "queryResult":I
    if-eqz v0, :cond_0

    const/4 v2, 0x1

    :goto_0
    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sIsWeatherOn:Z

    .line 118
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sIsWeatherOn:Z

    return v2

    .line 117
    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public static fetchWeekStartDay(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    const-string v2, "/PreferenceString"

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->buildUri(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 162
    .local v1, "uri":Ljava/lang/String;
    const-string v2, "preferences_week_start_day"

    invoke-static {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->queryPreferenceForStringType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 163
    .local v0, "result":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 164
    const/4 v2, -0x1

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    .line 165
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    .line 168
    :goto_0
    return v2

    .line 167
    :cond_0
    invoke-static {v0}, Ljava/lang/Integer;->valueOf(Ljava/lang/String;)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    .line 168
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    goto :goto_0
.end method

.method public static getTodayTimeZone()I
    .locals 1

    .prologue
    .line 78
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    return v0
.end method

.method public static getWeekStartDay()I
    .locals 1

    .prologue
    .line 74
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sWeekStartDay:I

    return v0
.end method

.method public static hideCompletedTask()Z
    .locals 1

    .prologue
    .line 62
    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideCompletedTask:Z

    return v0
.end method

.method public static hideContactsEvents()Z
    .locals 1

    .prologue
    .line 66
    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideContactsEvents:Z

    return v0
.end method

.method public static hideDeclinedEvent()Z
    .locals 1

    .prologue
    .line 58
    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sHideDeclinedEvent:Z

    return v0
.end method

.method private static declared-synchronized queryPreferenceForIntType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)I
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 187
    const-class v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v6

    .line 188
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 198
    :goto_0
    monitor-exit v8

    return v7

    .line 191
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 192
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 193
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->TAG:Ljava/lang/String;

    const-string v1, "cursor count is 0 in querying the int type"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 187
    .end local v6    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0

    .line 196
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :cond_2
    const/4 v0, 0x0

    :try_start_2
    invoke-interface {v6, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 197
    .local v7, "result":I
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_0
.end method

.method private static declared-synchronized queryPreferenceForStringType(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Ljava/lang/String;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 172
    const-class v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;

    monitor-enter v8

    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 173
    .local v6, "cursor":Landroid/database/Cursor;
    if-nez v6, :cond_0

    .line 174
    const-string v7, ""
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 183
    :goto_0
    monitor-exit v8

    return-object v7

    .line 176
    :cond_0
    :try_start_1
    invoke-interface {v6}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-eqz v0, :cond_1

    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-nez v0, :cond_2

    .line 177
    :cond_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 178
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->TAG:Ljava/lang/String;

    const-string v1, "cursor count is 0 in querying the string type"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 179
    const-string v7, ""

    goto :goto_0

    .line 181
    :cond_2
    const/4 v0, 0x0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 182
    .local v7, "result":Ljava/lang/String;
    invoke-interface {v6}, Landroid/database/Cursor;->close()V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 172
    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "result":Ljava/lang/String;
    :catchall_0
    move-exception v0

    monitor-exit v8

    throw v0
.end method

.method public static usedSystemTimezone()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 82
    sget v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sTodayTimeZone:I

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static weatherOn()Z
    .locals 1

    .prologue
    .line 54
    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->sIsWeatherOn:Z

    return v0
.end method
