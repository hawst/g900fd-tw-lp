.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;
.super Landroid/content/BroadcastReceiver;
.source "MagazineDayViewService42.java"

# interfaces
.implements Landroid/content/Loader$OnLoadCompleteListener;
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CalendarFactory"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/BroadcastReceiver;",
        "Landroid/widget/RemoteViewsService$RemoteViewsFactory;",
        "Landroid/content/Loader$OnLoadCompleteListener",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# static fields
.field private static final ACTION_SEC_TASK_ITEM_CHECK:Ljava/lang/String; = "ItemChecked"

.field private static final DEFAULT_PAINT_FLAG:I = 0x501

.field private static final EVENT_PROJECTION:[Ljava/lang/String;

.field private static final EVENT_SORT_ORDER:Ljava/lang/String;

.field private static final PRIORITY_LEVEL_HIGH:I = 0x2

.field private static final PRIORITY_LEVEL_LOW:I = 0x0

.field private static final PRIORITY_LEVEL_NORMAL:I = 0x1

.field private static mContext:Landroid/content/Context;

.field private static mCursor:Landroid/database/Cursor;

.field private static volatile mLock:Ljava/lang/Integer;


# instance fields
.field public final WIDGET_UPDATE_THROTTLE:J

.field private day_selected:Z

.field private mAppWidgetId:I

.field private mEventsArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field private final mHandler:Landroid/os/Handler;

.field private mLastLock:I

.field private mLoader:Landroid/content/CursorLoader;

.field private mResources:Landroid/content/res/Resources;

.field mTaskDimColor:I

.field private mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;

.field private mTasksArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempTasksArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation
.end field

.field private mTime:Landroid/text/format/Time;

.field mTitleColor:I

.field private pref:Landroid/content/SharedPreferences;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 78
    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->EVENT_PROJECTION:[Ljava/lang/String;

    .line 79
    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->EVENT_SORT_ORDER:Ljava/lang/String;

    .line 93
    const/4 v0, 0x4

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLock:Ljava/lang/Integer;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    .line 191
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 88
    const-wide/16 v0, 0x1f4

    iput-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->WIDGET_UPDATE_THROTTLE:J

    .line 96
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mHandler:Landroid/os/Handler;

    .line 107
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    .line 109
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    .line 111
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    .line 193
    return-void
.end method

.method protected constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 115
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 88
    const-wide/16 v2, 0x1f4

    iput-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->WIDGET_UPDATE_THROTTLE:J

    .line 96
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mHandler:Landroid/os/Handler;

    .line 107
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    .line 109
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    .line 111
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    .line 116
    sput-object p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    .line 117
    new-instance v1, Landroid/text/format/Time;

    const/4 v2, 0x0

    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    .line 118
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mResources:Landroid/content/res/Resources;

    .line 119
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 120
    .local v0, "resolver":Landroid/content/ContentResolver;
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    .line 121
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;

    invoke-direct {v1, p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;Landroid/content/ContentResolver;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;

    .line 122
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-direct {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;-><init>(Landroid/content/Context;)V

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->access$002(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 123
    const-string v1, "appWidgetId"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    .line 125
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mResources:Landroid/content/res/Resources;

    const v2, 0x106000b

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTitleColor:I

    .line 126
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mResources:Landroid/content/res/Resources;

    const v2, 0x7f070018

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTaskDimColor:I

    .line 127
    return-void
.end method

.method static synthetic access$100()Landroid/content/Context;
    .locals 1

    .prologue
    .line 75
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;)Landroid/text/format/Time;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;

    .prologue
    .line 75
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setCurrentDayEvents(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 75
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setTasks(Landroid/database/Cursor;)V

    return-void
.end method

.method private createLoaderUri()Landroid/net/Uri;
    .locals 18

    .prologue
    .line 150
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    .line 151
    .local v4, "currentMillis":J
    new-instance v7, Landroid/text/format/Time;

    sget-object v13, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    const/4 v14, 0x0

    invoke-static {v13, v14}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v13

    invoke-direct {v7, v13}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 152
    .local v7, "time":Landroid/text/format/Time;
    invoke-virtual {v7}, Landroid/text/format/Time;->setToNow()V

    .line 153
    const-string v13, "day_selected%d"

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    move/from16 v16, v0

    invoke-static/range {v16 .. v16}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v16

    aput-object v16, v14, v15

    invoke-static {v13, v14}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 154
    .local v6, "daySelectedkey":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v14, 0x0

    invoke-interface {v13, v6, v14}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    move-object/from16 v0, p0

    iput-boolean v13, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->day_selected:Z

    .line 155
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->day_selected:Z

    if-eqz v13, :cond_0

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getSelectedDay()Ljava/lang/String;

    move-result-object v13

    if-eqz v13, :cond_0

    .line 156
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getSelectedDay()Ljava/lang/String;

    move-result-object v13

    invoke-static {v13}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    iput v13, v7, Landroid/text/format/Time;->monthDay:I

    .line 158
    :cond_0
    sget-object v13, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v14, 0x2

    invoke-virtual {v13, v14}, Ljava/util/Calendar;->get(I)I

    move-result v13

    iput v13, v7, Landroid/text/format/Time;->month:I

    .line 159
    sget-object v13, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v14, 0x1

    invoke-virtual {v13, v14}, Ljava/util/Calendar;->get(I)I

    move-result v13

    iput v13, v7, Landroid/text/format/Time;->year:I

    .line 160
    const/4 v13, 0x0

    iput v13, v7, Landroid/text/format/Time;->hour:I

    .line 161
    const/4 v13, 0x0

    iput v13, v7, Landroid/text/format/Time;->minute:I

    .line 162
    const/4 v13, 0x0

    iput v13, v7, Landroid/text/format/Time;->second:I

    .line 164
    const/4 v13, 0x1

    invoke-virtual {v7, v13}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    .line 166
    .local v10, "now":J
    move-wide v2, v10

    .line 167
    .local v2, "begin":J
    const-wide/32 v14, 0x5265c00

    add-long/2addr v14, v10

    const-wide/16 v16, 0x1

    sub-long v8, v14, v16

    .line 169
    .local v8, "end":J
    sget-object v13, Landroid/provider/CalendarContract$Instances;->CONTENT_URI:Landroid/net/Uri;

    new-instance v14, Ljava/lang/StringBuilder;

    invoke-direct {v14}, Ljava/lang/StringBuilder;-><init>()V

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    const-string v15, "/"

    invoke-virtual {v14, v15}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v14

    invoke-virtual {v14}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    invoke-static {v13, v14}, Landroid/net/Uri;->withAppendedPath(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    .line 171
    .local v12, "uri":Landroid/net/Uri;
    return-object v12
.end method

.method private doDecoding(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 6
    .param p1, "path"    # Ljava/lang/String;

    .prologue
    .line 397
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->linkedHashMap_Bitmap:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 398
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->linkedHashMap_Bitmap:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p1}, Ljava/util/LinkedHashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/graphics/Bitmap;

    .line 419
    :goto_0
    return-object v4

    .line 402
    :cond_0
    const/4 v0, 0x0

    .line 404
    .local v0, "bMap":Landroid/graphics/Bitmap;
    :try_start_0
    new-instance v3, Ljava/io/FileInputStream;

    invoke-direct {v3, p1}, Ljava/io/FileInputStream;-><init>(Ljava/lang/String;)V

    .line 405
    .local v3, "in":Ljava/io/FileInputStream;
    new-instance v2, Ljava/io/BufferedInputStream;

    invoke-direct {v2, v3}, Ljava/io/BufferedInputStream;-><init>(Ljava/io/InputStream;)V

    .line 406
    .local v2, "buf":Ljava/io/BufferedInputStream;
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->available()I

    move-result v4

    new-array v1, v4, [B

    .line 407
    .local v1, "bMapArray":[B
    invoke-virtual {v2, v1}, Ljava/io/BufferedInputStream;->read([B)I

    .line 408
    const/4 v4, 0x0

    array-length v5, v1

    invoke-static {v1, v4, v5}, Landroid/graphics/BitmapFactory;->decodeByteArray([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 409
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->linkedHashMap_Bitmap:Ljava/util/LinkedHashMap;

    invoke-virtual {v4, p1, v0}, Ljava/util/LinkedHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 410
    if-eqz v3, :cond_1

    .line 411
    invoke-virtual {v3}, Ljava/io/FileInputStream;->close()V

    .line 413
    :cond_1
    if-eqz v2, :cond_2

    .line 414
    invoke-virtual {v2}, Ljava/io/BufferedInputStream;->close()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .end local v1    # "bMapArray":[B
    .end local v2    # "buf":Ljava/io/BufferedInputStream;
    .end local v3    # "in":Ljava/io/FileInputStream;
    :cond_2
    :goto_1
    move-object v4, v0

    .line 419
    goto :goto_0

    .line 416
    :catch_0
    move-exception v4

    goto :goto_1
.end method

.method private getEventCount()I
    .locals 1

    .prologue
    .line 694
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method private getEventPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 499
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->isTask(I)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 500
    const/4 v0, -0x1

    .line 502
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskCount()I

    move-result v0

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method private getPaintFlags()I
    .locals 1

    .prologue
    .line 425
    const/16 v0, 0x501

    return v0
.end method

.method private getSelectedDay()Ljava/lang/String;
    .locals 14

    .prologue
    .line 175
    const/4 v4, 0x0

    .line 176
    .local v4, "selectedDay":Ljava/lang/String;
    const/4 v6, 0x0

    .line 177
    .local v6, "selectedMonth":Ljava/lang/String;
    const/4 v7, 0x0

    .line 178
    .local v7, "selectedYear":Ljava/lang/String;
    sget-object v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v10}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v9

    .line 179
    .local v9, "widgetManager":Landroid/appwidget/AppWidgetManager;
    sget-object v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v8

    .line 181
    .local v8, "widgetIds":[I
    move-object v1, v8

    .local v1, "arr$":[I
    array-length v3, v1

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget v0, v1, v2

    .line 182
    .local v0, "appWidgetId":I
    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    if-lez v10, :cond_0

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    if-ne v0, v10, :cond_0

    .line 184
    const-string v10, "SelectedDay%d"

    const/4 v11, 0x1

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    aput-object v13, v11, v12

    invoke-static {v10, v11}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 185
    .local v5, "selectedDayKey":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v11, 0x0

    invoke-interface {v10, v5, v11}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 181
    .end local v5    # "selectedDayKey":Ljava/lang/String;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 188
    .end local v0    # "appWidgetId":I
    :cond_1
    return-object v4
.end method

.method private getSelectedMillis()J
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 646
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setSelectedTime()V

    .line 648
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 649
    .local v0, "time":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 650
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 651
    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 652
    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 653
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    return-wide v2
.end method

.method private getTaskCount()I
    .locals 1

    .prologue
    .line 698
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    return v0
.end method

.method static getTaskItemPendingIntentTemplate(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 451
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 452
    .local v0, "launchIntent":Landroid/content/Intent;
    const-string v1, "ItemChecked"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 453
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private getTaskPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 491
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->isTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 492
    const/4 v0, -0x1

    .line 494
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getEventCount()I

    move-result v0

    sub-int v0, p1, v0

    goto :goto_0
.end method

.method private getText(I)Ljava/lang/CharSequence;
    .locals 1
    .param p1, "stringId"    # I

    .prologue
    .line 429
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mResources:Landroid/content/res/Resources;

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getText(I)Ljava/lang/CharSequence;

    move-result-object v0

    return-object v0
.end method

.method private getTriggerTime(Landroid/content/Context;)J
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 542
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    .line 544
    .local v5, "tz":Ljava/lang/String;
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 545
    .local v4, "time":Landroid/text/format/Time;
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 546
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    .line 547
    iput v7, v4, Landroid/text/format/Time;->hour:I

    .line 548
    iput v7, v4, Landroid/text/format/Time;->minute:I

    .line 549
    iput v7, v4, Landroid/text/format/Time;->second:I

    .line 550
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 552
    .local v0, "midnightDeviceTz":J
    iput-object v5, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 553
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 554
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    .line 555
    iput v7, v4, Landroid/text/format/Time;->hour:I

    .line 556
    iput v7, v4, Landroid/text/format/Time;->minute:I

    .line 557
    iput v7, v4, Landroid/text/format/Time;->second:I

    .line 558
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 560
    .local v2, "midnightHomeTz":J
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    return-wide v6
.end method

.method private setCurrentDayEvents(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 657
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 658
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    .line 659
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setTaskData()V

    .line 660
    return-void
.end method

.method private setSelectedTime()V
    .locals 9

    .prologue
    .line 606
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    invoke-virtual {v5}, Landroid/text/format/Time;->setToNow()V

    .line 607
    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    .line 608
    .local v4, "targetWidgetId":I
    const-string v5, "day_selected%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 609
    .local v1, "selectedDayKey":Ljava/lang/String;
    const-string v5, "SelectedDay%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 610
    .local v0, "daySelectedKey":Ljava/lang/String;
    const-string v5, "SelectedMonth%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 611
    .local v2, "selectedMonthKey":Ljava/lang/String;
    const-string v5, "SelectedYear%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 612
    .local v3, "selectedYearKey":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v1, v6}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->day_selected:Z

    .line 613
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v0, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 614
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-interface {v6, v0, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->monthDay:I

    .line 616
    :cond_0
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->month:I

    .line 617
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v7, 0x1

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->year:I

    .line 618
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v2, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_1

    .line 619
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-interface {v6, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->month:I

    .line 621
    :cond_1
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v6, 0x0

    invoke-interface {v5, v3, v6}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_2

    .line 622
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v7, 0x0

    invoke-interface {v6, v3, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, v5, Landroid/text/format/Time;->year:I
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 626
    .end local v0    # "daySelectedKey":Ljava/lang/String;
    .end local v1    # "selectedDayKey":Ljava/lang/String;
    .end local v2    # "selectedMonthKey":Ljava/lang/String;
    .end local v3    # "selectedYearKey":Ljava/lang/String;
    .end local v4    # "targetWidgetId":I
    :cond_2
    :goto_0
    return-void

    .line 624
    :catch_0
    move-exception v5

    goto :goto_0
.end method

.method private setTasks(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 682
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 683
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 684
    .local v1, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v2, p1, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->constructTask(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/ArrayList;)V

    .line 685
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 687
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 688
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 687
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 691
    .end local v0    # "i":I
    .end local v1    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    :cond_0
    return-void
.end method

.method static updateTextView(Landroid/widget/RemoteViews;IILjava/lang/String;)V
    .locals 1
    .param p0, "views"    # Landroid/widget/RemoteViews;
    .param p1, "id"    # I
    .param p2, "visibility"    # I
    .param p3, "string"    # Ljava/lang/String;

    .prologue
    .line 513
    invoke-virtual {p0, p1, p2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 514
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Landroid/widget/RemoteViews;->setViewFingerHovered(IZ)V

    .line 515
    if-nez p2, :cond_0

    .line 516
    invoke-virtual {p0, p1, p3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 518
    :cond_0
    return-void
.end method


# virtual methods
.method public bindPriority(Landroid/widget/RemoteViews;I)Landroid/widget/RemoteViews;
    .locals 3
    .param p1, "views"    # Landroid/widget/RemoteViews;
    .param p2, "importance"    # I

    .prologue
    const/4 v0, 0x0

    const v2, 0x7f0c003c

    .line 433
    packed-switch p2, :pswitch_data_0

    .line 446
    const/4 p1, 0x0

    .end local p1    # "views":Landroid/widget/RemoteViews;
    :goto_0
    return-object p1

    .line 435
    .restart local p1    # "views":Landroid/widget/RemoteViews;
    :pswitch_0
    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 436
    const-string v0, "setBackgroundResource"

    const v1, 0x7f02001c

    invoke-virtual {p1, v2, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0

    .line 439
    :pswitch_1
    const/4 v0, 0x4

    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_0

    .line 442
    :pswitch_2
    invoke-virtual {p1, v2, v0}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 443
    const-string v0, "setBackgroundResource"

    const v1, 0x7f02001b

    invoke-virtual {p1, v2, v0, v1}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto :goto_0

    .line 433
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public getCount()I
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 465
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getEventCount()I

    move-result v1

    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskCount()I

    move-result v2

    add-int/2addr v1, v2

    if-ge v1, v0, :cond_0

    .line 468
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getEventCount()I

    move-result v0

    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskCount()I

    move-result v1

    add-int/2addr v0, v1

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 474
    if-ltz p1, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_1

    .line 476
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->isTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 477
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    .line 482
    :goto_0
    return-wide v0

    .line 479
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    goto :goto_0

    .line 482
    :cond_1
    const-wide/16 v0, 0x0

    goto :goto_0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 221
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLock:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ge v1, v2, :cond_0

    .line 222
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v1}, Landroid/content/CursorLoader;->forceLoad()V

    .line 228
    :goto_0
    new-instance v0, Landroid/widget/RemoteViews;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030001

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 230
    .local v0, "views":Landroid/widget/RemoteViews;
    return-object v0

    .line 224
    .end local v0    # "views":Landroid/widget/RemoteViews;
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v1}, Landroid/content/CursorLoader;->cancelLoad()Z

    .line 225
    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    .line 226
    sput-boolean v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    goto :goto_0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 31
    .param p1, "position"    # I

    .prologue
    .line 235
    if-ltz p1, :cond_0

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getCount()I

    move-result v2

    move/from16 v0, p1

    if-lt v0, v2, :cond_2

    .line 236
    :cond_0
    const/16 v27, 0x0

    .line 392
    :cond_1
    :goto_0
    return-object v27

    .line 238
    :cond_2
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getEventCount()I

    move-result v2

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskCount()I

    move-result v3

    add-int/2addr v2, v3

    const/4 v3, 0x1

    if-ge v2, v3, :cond_3

    .line 239
    new-instance v30, Landroid/widget/RemoteViews;

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030008

    move-object/from16 v0, v30

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 241
    .local v30, "views":Landroid/widget/RemoteViews;
    new-instance v26, Landroid/content/Intent;

    invoke-direct/range {v26 .. v26}, Landroid/content/Intent;-><init>()V

    .line 242
    .local v26, "intent":Landroid/content/Intent;
    const-string v2, "no_events"

    const/4 v3, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 244
    const/16 v27, 0x0

    goto :goto_0

    .line 246
    .end local v26    # "intent":Landroid/content/Intent;
    .end local v30    # "views":Landroid/widget/RemoteViews;
    :cond_3
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->isTask(I)Z

    move-result v2

    if-nez v2, :cond_6

    .line 249
    :try_start_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mEventsArray:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v20

    check-cast v20, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :try_end_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_0

    .line 253
    .local v20, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :goto_1
    new-instance v27, Landroid/widget/RemoteViews;

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030005

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 254
    .local v27, "remoteViews":Landroid/widget/RemoteViews;
    const v2, 0x7f0c003d

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 255
    if-eqz v20, :cond_1

    .line 257
    const v2, 0x7f0c0039

    const-string v3, "setPaintFlags"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getPaintFlags()I

    move-result v8

    and-int/lit8 v8, v8, -0x11

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 258
    const v2, 0x7f0c0039

    move-object/from16 v0, v20

    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 259
    const v2, 0x7f0c0039

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTitleColor:I

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 260
    const v2, 0x7f0c0038

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 261
    const v2, 0x7f0c0037

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 262
    const v2, 0x7f0c003c

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 281
    move-object/from16 v0, v20

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eqz v2, :cond_4

    .line 282
    const v2, 0x7f0c0038

    const v3, 0x7f0a001a

    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getText(I)Ljava/lang/CharSequence;

    move-result-object v3

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 315
    :goto_2
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    move-object/from16 v0, v20

    iget-wide v12, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    invoke-static {v2, v12, v13}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v23

    .line 317
    .local v23, "eventUri":Landroid/net/Uri;
    sget-object v11, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    move-object/from16 v0, v20

    iget-wide v12, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    move-object/from16 v0, v20

    iget-wide v14, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    move-object/from16 v0, v20

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    move-wide/from16 v16, v0

    invoke-static/range {v11 .. v17}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchEventFillInIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;

    move-result-object v24

    .line 319
    .local v24, "fillInIntent":Landroid/content/Intent;
    const-string v2, "EventItemClick"

    const/4 v3, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 320
    const v2, 0x7f0c003e

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 250
    .end local v20    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v23    # "eventUri":Landroid/net/Uri;
    .end local v24    # "fillInIntent":Landroid/content/Intent;
    .end local v27    # "remoteViews":Landroid/widget/RemoteViews;
    :catch_0
    move-exception v19

    .line 251
    .local v19, "e":Ljava/lang/IndexOutOfBoundsException;
    const/16 v20, 0x0

    .restart local v20    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    goto/16 :goto_1

    .line 284
    .end local v19    # "e":Ljava/lang/IndexOutOfBoundsException;
    .restart local v27    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_4
    move-object/from16 v0, v20

    iget-wide v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    .line 285
    .local v4, "start":J
    move-object/from16 v0, v20

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    .line 287
    .local v6, "end":J
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v22

    .line 288
    .local v22, "eventTimezone":Ljava/lang/String;
    invoke-static/range {v22 .. v22}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_5

    const/4 v9, 0x0

    .line 289
    .local v9, "timezone":Ljava/lang/String;
    :goto_3
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    const/4 v8, 0x0

    invoke-static/range {v2 .. v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateTimeString(Landroid/content/Context;ZJJZLjava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 291
    .local v21, "eventFormatTime":Ljava/lang/CharSequence;
    const v2, 0x7f0c0038

    move-object/from16 v0, v27

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto :goto_2

    .end local v9    # "timezone":Ljava/lang/String;
    .end local v21    # "eventFormatTime":Ljava/lang/CharSequence;
    :cond_5
    move-object/from16 v9, v22

    .line 288
    goto :goto_3

    .line 324
    .end local v4    # "start":J
    .end local v6    # "end":J
    .end local v20    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v22    # "eventTimezone":Ljava/lang/String;
    .end local v27    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_6
    invoke-direct/range {p0 .. p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTaskPosition(I)I

    move-result p1

    .line 325
    if-ltz p1, :cond_7

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    move/from16 v0, p1

    if-lt v0, v2, :cond_8

    .line 326
    :cond_7
    const/16 v27, 0x0

    goto/16 :goto_0

    .line 328
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    move/from16 v0, p1

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    .line 329
    .local v29, "task":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    new-instance v27, Landroid/widget/RemoteViews;

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f030005

    move-object/from16 v0, v27

    invoke-direct {v0, v2, v3}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 330
    .restart local v27    # "remoteViews":Landroid/widget/RemoteViews;
    if-eqz v29, :cond_1

    .line 331
    const v2, 0x7f0c0039

    move-object/from16 v0, v29

    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 332
    const v2, 0x7f0c003a

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 333
    const v2, 0x7f0c0038

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 334
    const v2, 0x7f0c003d

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 336
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v2

    if-eqz v2, :cond_c

    .line 337
    new-instance v2, Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDateFormatOrder()[C

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v2}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v10

    .line 338
    .local v10, "dateFormatOrder":Ljava/lang/String;
    const/16 v18, 0x0

    .line 339
    .local v18, "dateOrderStr":Ljava/lang/String;
    const-string v2, "YMD"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 340
    const-string v18, "yyyy/MM/dd, EEE"

    .line 346
    :goto_4
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 347
    new-instance v28, Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " HH:mm"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 358
    .end local v10    # "dateFormatOrder":Ljava/lang/String;
    .end local v18    # "dateOrderStr":Ljava/lang/String;
    .local v28, "sdf":Ljava/text/SimpleDateFormat;
    :goto_5
    move-object/from16 v0, v29

    iget-wide v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    .line 360
    .local v25, "formatSameDay":Ljava/lang/String;
    const v2, 0x7f0c0040

    const/16 v3, 0x8

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 363
    const v2, 0x7f0c0037

    const/4 v3, 0x0

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 364
    const v2, 0x7f0c0037

    const-string v3, "setBackgroundColor"

    const v8, 0x106000d

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 365
    move-object/from16 v0, v29

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    if-eqz v2, :cond_e

    .line 366
    const v2, 0x7f0c0037

    const-string v3, "setImageResource"

    const v8, 0x7f020016

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 367
    const v2, 0x7f0c0039

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTaskDimColor:I

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 368
    const v2, 0x7f0c0039

    const-string v3, "setPaintFlags"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getPaintFlags()I

    move-result v8

    or-int/lit8 v8, v8, 0x10

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 381
    :goto_6
    new-instance v26, Landroid/content/Intent;

    invoke-direct/range {v26 .. v26}, Landroid/content/Intent;-><init>()V

    .line 382
    .restart local v26    # "intent":Landroid/content/Intent;
    const-string v2, "TaskChecked"

    const/4 v3, 0x1

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 383
    const-string v2, "id"

    move-object/from16 v0, v29

    iget-wide v12, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v12, v13}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 384
    const-string v2, "isChecked"

    move-object/from16 v0, v29

    iget-boolean v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    move-object/from16 v0, v26

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 385
    const v2, 0x7f0c0037

    move-object/from16 v0, v27

    move-object/from16 v1, v26

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    .line 387
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    move-object/from16 v0, v29

    iget-wide v12, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    move-object/from16 v0, v29

    iget-wide v14, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    invoke-static {v2, v12, v13, v14, v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchTaskFillInIntent(Landroid/content/Context;JJ)Landroid/content/Intent;

    move-result-object v24

    .line 389
    .restart local v24    # "fillInIntent":Landroid/content/Intent;
    const-string v2, "TaskItemClick"

    const/4 v3, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 390
    const v2, 0x7f0c003e

    move-object/from16 v0, v27

    move-object/from16 v1, v24

    invoke-virtual {v0, v2, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 341
    .end local v24    # "fillInIntent":Landroid/content/Intent;
    .end local v25    # "formatSameDay":Ljava/lang/String;
    .end local v26    # "intent":Landroid/content/Intent;
    .end local v28    # "sdf":Ljava/text/SimpleDateFormat;
    .restart local v10    # "dateFormatOrder":Ljava/lang/String;
    .restart local v18    # "dateOrderStr":Ljava/lang/String;
    :cond_9
    const-string v2, "DMY"

    invoke-virtual {v10, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 342
    const-string v18, "dd/MM/yyyy, EEE"

    goto/16 :goto_4

    .line 344
    :cond_a
    const-string v18, "MM/dd/yyyy, EEE"

    goto/16 :goto_4

    .line 349
    :cond_b
    new-instance v28, Ljava/text/SimpleDateFormat;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " HH:mm aa"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v28    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_5

    .line 352
    .end local v10    # "dateFormatOrder":Ljava/lang/String;
    .end local v18    # "dateOrderStr":Ljava/lang/String;
    .end local v28    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_c
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_d

    .line 353
    new-instance v28, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, dd MMM yyyy HH:mm"

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v28    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_5

    .line 355
    .end local v28    # "sdf":Ljava/text/SimpleDateFormat;
    :cond_d
    new-instance v28, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE, dd MMM yyyy hh:mm aa"

    move-object/from16 v0, v28

    invoke-direct {v0, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .restart local v28    # "sdf":Ljava/text/SimpleDateFormat;
    goto/16 :goto_5

    .line 371
    .restart local v25    # "formatSameDay":Ljava/lang/String;
    :cond_e
    const v2, 0x7f0c0037

    const-string v3, "setImageResource"

    const v8, 0x7f020015

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 372
    const v2, 0x7f0c0039

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTitleColor:I

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 373
    const v2, 0x7f0c0039

    const-string v3, "setPaintFlags"

    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getPaintFlags()I

    move-result v8

    and-int/lit8 v8, v8, -0x11

    move-object/from16 v0, v27

    invoke-virtual {v0, v2, v3, v8}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    goto/16 :goto_6
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 460
    const/4 v0, 0x2

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 508
    const/4 v0, 0x1

    return v0
.end method

.method public initLoader()V
    .locals 8

    .prologue
    .line 132
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->createLoaderUri()Landroid/net/Uri;

    move-result-object v2

    .line 133
    .local v2, "uri":Landroid/net/Uri;
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLock:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    monitor-enter v1

    .line 134
    const/4 v0, 0x0

    :try_start_0
    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    .line 135
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 136
    const/4 v4, 0x0

    .line 137
    .local v4, "selection":Ljava/lang/String;
    new-instance v0, Landroid/content/CursorLoader;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v5, 0x0

    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->EVENT_SORT_ORDER:Ljava/lang/String;

    invoke-direct/range {v0 .. v6}, Landroid/content/CursorLoader;-><init>(Landroid/content/Context;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    .line 139
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    const-wide/16 v6, 0x1f4

    invoke-virtual {v0, v6, v7}, Landroid/content/CursorLoader;->setUpdateThrottle(J)V

    .line 140
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_0

    .line 141
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    invoke-virtual {v0, v1, p0}, Landroid/content/CursorLoader;->registerListener(ILandroid/content/Loader$OnLoadCompleteListener;)V

    .line 142
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->startLoading()V

    .line 145
    :cond_0
    return-void

    .line 135
    .end local v4    # "selection":Ljava/lang/String;
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public isTask(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 487
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getEventCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate()V
    .locals 0

    .prologue
    .line 197
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->initLoader()V

    .line 198
    return-void
.end method

.method public onDataSetChanged()V
    .locals 0

    .prologue
    .line 202
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->reloadCurrentDayEvents()V

    .line 203
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->reloadTasks()V

    .line 204
    return-void
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 208
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mCursor:Landroid/database/Cursor;

    if-eqz v0, :cond_0

    .line 209
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mCursor:Landroid/database/Cursor;

    invoke-interface {v0}, Landroid/database/Cursor;->close()V

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    if-eqz v0, :cond_1

    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLoader:Landroid/content/CursorLoader;

    invoke-virtual {v0}, Landroid/content/CursorLoader;->reset()V

    .line 214
    :cond_1
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->access$000()Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 215
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->access$000()Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->clear()V

    .line 217
    :cond_2
    return-void
.end method

.method public onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 5
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 703
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLock:Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    monitor-enter v3

    .line 704
    :try_start_0
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLock:Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    if-le v2, v4, :cond_0

    .line 705
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 706
    monitor-exit v3

    .line 718
    :goto_0
    return-void

    .line 708
    :cond_0
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v2}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v1

    .line 709
    .local v1, "widgetManager":Landroid/appwidget/AppWidgetManager;
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    const/4 v4, -0x1

    if-ne v2, v4, :cond_1

    .line 710
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v0

    .line 712
    .local v0, "ids":[I
    const v2, 0x7f0c0018

    invoke-virtual {v1, v0, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged([II)V

    .line 716
    .end local v0    # "ids":[I
    :goto_1
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mLastLock:I

    .line 717
    monitor-exit v3

    goto :goto_0

    .end local v1    # "widgetManager":Landroid/appwidget/AppWidgetManager;
    :catchall_0
    move-exception v2

    monitor-exit v3
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v2

    .line 714
    .restart local v1    # "widgetManager":Landroid/appwidget/AppWidgetManager;
    :cond_1
    :try_start_1
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mAppWidgetId:I

    const v4, 0x7f0c0018

    invoke-virtual {v1, v2, v4}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_1
.end method

.method public bridge synthetic onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 75
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 523
    sput-object p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    .line 524
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getTriggerTime(Landroid/content/Context;)J

    move-result-wide v2

    .line 526
    .local v2, "triggerTime":J
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    const-string v5, "alarm"

    invoke-virtual {v4, v5}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 528
    .local v0, "alertManager":Landroid/app/AlarmManager;
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getUpdateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 530
    .local v1, "pendingUpdate":Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 532
    const/4 v4, 0x1

    invoke-virtual {v0, v4, v2, v3, v1}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 533
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isKitKatOrLater()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 534
    const-string v4, "Splanner_MagazineDayViewService42"

    const-string v5, "kitkat"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 538
    :goto_0
    return-void

    .line 536
    :cond_0
    const-string v4, "Splanner_MagazineDayViewService42"

    const-string v5, "kitkat else part"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public reloadCurrentDayEvents()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 580
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->access$000()Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->startBackgroundThread()V

    .line 581
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setSelectedTime()V

    .line 582
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 583
    .local v6, "millis":J
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    .line 586
    .local v3, "mCurrentViewJulianDay":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 588
    .local v2, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;->access$000()Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    move-result-object v0

    new-instance v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;

    invoke-direct {v4, p0, v6, v7, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;JLjava/util/ArrayList;)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->loadEventsInBackground(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 602
    return-void
.end method

.method public reloadTasks()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 564
    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 566
    .local v3, "tasksUri":Landroid/net/Uri;
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 567
    .local v12, "time":Landroid/text/format/Time;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 568
    iput v1, v12, Landroid/text/format/Time;->hour:I

    .line 569
    iput v1, v12, Landroid/text/format/Time;->minute:I

    .line 570
    iput v1, v12, Landroid/text/format/Time;->second:I

    .line 572
    const/4 v0, 0x1

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 573
    .local v10, "startMillis":J
    const-wide/32 v6, 0x5265c00

    add-long v8, v10, v6

    .line 574
    .local v8, "endMillis":J
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;

    invoke-static {v0, v10, v11, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTaskSelection(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v5

    .line 575
    .local v5, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;

    const-string v7, " utc_due_date ASC, importance DESC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$TaskQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 577
    return-void
.end method

.method public setTaskData()V
    .locals 12

    .prologue
    .line 629
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->getSelectedMillis()J

    move-result-wide v8

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-wide v10, v10, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v8

    int-to-long v4, v8

    .line 630
    .local v4, "milis":J
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;

    iget-wide v0, v8, Landroid/text/format/Time;->gmtoff:J

    .line 631
    .local v0, "gmtoff":J
    const-wide/16 v6, 0x0

    .line 633
    .local v6, "taskDueDate":J
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 635
    .local v3, "taskArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 636
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    iget-wide v8, v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    invoke-static {v8, v9, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v8

    int-to-long v6, v8

    .line 637
    cmp-long v8, v4, v6

    if-nez v8, :cond_0

    .line 638
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 635
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 642
    :cond_1
    iput-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTasksArray:Ljava/util/ArrayList;

    .line 643
    return-void
.end method
