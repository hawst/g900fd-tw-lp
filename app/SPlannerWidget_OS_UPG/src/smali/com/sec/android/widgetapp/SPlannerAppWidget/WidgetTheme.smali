.class public interface abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
.super Ljava/lang/Object;
.source "WidgetTheme.java"


# virtual methods
.method public abstract initialize(Landroid/view/View;)V
.end method

.method public abstract onContentRequest()I
.end method

.method public abstract onFinishInflate(ILandroid/view/View;)V
.end method

.method public abstract onGoToToday()V
.end method

.method public abstract onPause(I)V
.end method

.method public abstract onRefresh()V
.end method

.method public abstract onResume(I)V
.end method

.method public abstract onThemeDestroy()V
.end method

.method public abstract onTouchEvent(ILandroid/view/MotionEvent;)Z
.end method

.method public abstract releaseOnClickListener()V
.end method
