.class Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;
.super Landroid/database/ContentObserver;
.source "SPlannerSurfaceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Landroid/os/Handler;)V
    .locals 0
    .param p2, "x0"    # Landroid/os/Handler;

    .prologue
    .line 168
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {p0, p2}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    return-void
.end method


# virtual methods
.method public deliverSelfNotifications()Z
    .locals 1

    .prologue
    .line 172
    const/4 v0, 0x1

    return v0
.end method

.method public onChange(Z)V
    .locals 9
    .param p1, "selfChange"    # Z

    .prologue
    const/4 v8, 0x0

    .line 178
    const/4 v2, 0x1

    .line 179
    .local v2, "numberOfWidgets":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    .line 180
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v2

    .line 183
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # operator++ for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$708(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I

    .line 185
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I

    move-result v3

    if-lt v3, v2, :cond_1

    .line 186
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I
    invoke-static {v3, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$702(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;I)I

    .line 191
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    .line 194
    .local v0, "now":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)J

    move-result-wide v4

    sub-long v4, v0, v4

    const-wide/16 v6, 0x384

    cmp-long v3, v4, v6

    if-lez v3, :cond_2

    .line 195
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I
    invoke-static {v3, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$902(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;I)I

    .line 200
    :goto_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J
    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$802(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;J)J

    .line 201
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshFromDBChangeRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$1000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/lang/Runnable;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 202
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mHandler:Landroid/os/Handler;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Landroid/os/Handler;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshFromDBChangeRunnable:Ljava/lang/Runnable;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$1000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/lang/Runnable;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I
    invoke-static {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$900(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I

    move-result v5

    int-to-long v6, v5

    invoke-virtual {v3, v4, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 203
    .end local v0    # "now":J
    :cond_1
    return-void

    .line 197
    .restart local v0    # "now":J
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const/16 v4, 0x384

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$902(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;I)I

    goto :goto_0
.end method
