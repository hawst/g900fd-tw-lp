.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;
.super Ljava/lang/Object;
.source "MonthTheme.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V
    .locals 0

    .prologue
    .line 750
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 754
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v2

    .line 755
    .local v2, "isUsedSystemTimezone":Z
    if-eqz v2, :cond_0

    .line 756
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 760
    .local v3, "midnight":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 762
    invoke-virtual {v3, v9}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    .line 764
    .local v0, "currentMillis":J
    iput v8, v3, Landroid/text/format/Time;->hour:I

    .line 765
    iput v8, v3, Landroid/text/format/Time;->minute:I

    .line 766
    iput v8, v3, Landroid/text/format/Time;->second:I

    .line 767
    iget v6, v3, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v3, Landroid/text/format/Time;->monthDay:I

    .line 768
    invoke-virtual {v3, v9}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    sub-long v4, v6, v0

    .line 770
    .local v4, "millisToMidnight":J
    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-object v6, v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-object v7, v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToToday:Ljava/lang/Runnable;

    invoke-virtual {v6, v7, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 771
    return-void

    .line 758
    .end local v0    # "currentMillis":J
    .end local v3    # "midnight":Landroid/text/format/Time;
    .end local v4    # "millisToMidnight":J
    :cond_0
    new-instance v3, Landroid/text/format/Time;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v3, v6}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v3    # "midnight":Landroid/text/format/Time;
    goto :goto_0
.end method
