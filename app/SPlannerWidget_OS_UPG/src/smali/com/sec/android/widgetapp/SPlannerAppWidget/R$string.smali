.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$string;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "string"
.end annotation


# static fields
.field public static final EEEEddmmyy:I = 0x7f0a0000

.field public static final EEEEmmddyy:I = 0x7f0a0001

.field public static final EEEEyymmdd:I = 0x7f0a0002

.field public static final EEEMMMdyyyy:I = 0x7f0a0003

.field public static final EEEMMMdyyyyhmmAA:I = 0x7f0a0004

.field public static final Edd:I = 0x7f0a0005

.field public static final Eddmmmyyyy:I = 0x7f0a0006

.field public static final Eddmmyy:I = 0x7f0a0007

.field public static final Emmddyy:I = 0x7f0a0008

.field public static final Emmmddyyyy:I = 0x7f0a0009

.field public static final Eyymmdd:I = 0x7f0a000a

.field public static final Eyyyymmmdd:I = 0x7f0a000b

.field public static final MMMdd:I = 0x7f0a000c

.field public static final SS_MONTH_VIEW_TTS:I = 0x7f0a000d

.field public static final TS_LIST_VIEW_BUTTON:I = 0x7f0a000e

.field public static final accessibility_create_event_button:I = 0x7f0a000f

.field public static final accessibility_create_task_button:I = 0x7f0a0073

.field public static final accessibility_double_tap_to_check_todays_schedule:I = 0x7f0a0010

.field public static final accessibility_double_tap_to_open:I = 0x7f0a0011

.field public static final accessibility_double_tap_to_open_splanner:I = 0x7f0a0012

.field public static final accessibility_next_day_button:I = 0x7f0a0013

.field public static final accessibility_next_month_button:I = 0x7f0a0014

.field public static final accessibility_previous_day_button:I = 0x7f0a0015

.field public static final accessibility_previous_month_button:I = 0x7f0a0016

.field public static final accessibility_refresh_button:I = 0x7f0a0074

.field public static final accessibility_today_button:I = 0x7f0a0017

.field public static final add_new:I = 0x7f0a0075

.field public static final agenda:I = 0x7f0a0076

.field public static final agenda_left_year:I = 0x7f0a0018

.field public static final agenda_right_year:I = 0x7f0a0019

.field public static final agenda_theme:I = 0x7f0a0077

.field public static final agenda_tomorrow:I = 0x7f0a0078

.field public static final all_day_event:I = 0x7f0a001a

.field public static final app_name:I = 0x7f0a001b

.field public static final application_not_found:I = 0x7f0a001c

.field public static final attendees_label:I = 0x7f0a0079

.field public static final calendar_day_theme:I = 0x7f0a007a

.field public static final calendar_default_name:I = 0x7f0a007b

.field public static final calendar_mini_day_theme:I = 0x7f0a001d

.field public static final calendar_month_theme:I = 0x7f0a001e

.field public static final calendar_name:I = 0x7f0a001f

.field public static final calendar_task_theme:I = 0x7f0a007c

.field public static final cancel:I = 0x7f0a007d

.field public static final createnew:I = 0x7f0a007e

.field public static final date_leap_mark_month_calendar:I = 0x7f0a0020

.field public static final day_format:I = 0x7f0a0021

.field public static final day_postfix:I = 0x7f0a0022

.field public static final day_theme:I = 0x7f0a007f

.field public static final days_theme:I = 0x7f0a0080

.field public static final ddMMM:I = 0x7f0a0023

.field public static final ddmmyy:I = 0x7f0a0024

.field public static final ddmmyyE:I = 0x7f0a0025

.field public static final dueMoreThanOneWeek:I = 0x7f0a0081

.field public static final dueWithinOneWeek:I = 0x7f0a0082

.field public static final due_date:I = 0x7f0a0083

.field public static final duetoday:I = 0x7f0a0084

.field public static final event_name:I = 0x7f0a0026

.field public static final event_name_pl:I = 0x7f0a0027

.field public static final gadget_no_events:I = 0x7f0a0085

.field public static final google_email_domain:I = 0x7f0a0028

.field public static final goto_today:I = 0x7f0a0029

.field public static final indentchilds_done:I = 0x7f0a0086

.field public static final indentchilds_undone:I = 0x7f0a0087

.field public static final indenttasks:I = 0x7f0a0088

.field public static final jp_calendar_name:I = 0x7f0a002a

.field public static final jp_calendar_sync_account:I = 0x7f0a002b

.field public static final jp_holiday_10_sports:I = 0x7f0a002c

.field public static final jp_holiday_11_culture:I = 0x7f0a002d

.field public static final jp_holiday_11_labor:I = 0x7f0a002e

.field public static final jp_holiday_12_emperor:I = 0x7f0a002f

.field public static final jp_holiday_13_mountain_day:I = 0x7f0a0030

.field public static final jp_holiday_1_age:I = 0x7f0a0031

.field public static final jp_holiday_1_newyear:I = 0x7f0a0032

.field public static final jp_holiday_2_foundation:I = 0x7f0a0033

.field public static final jp_holiday_3_spring:I = 0x7f0a0034

.field public static final jp_holiday_4_showa:I = 0x7f0a0035

.field public static final jp_holiday_5_children:I = 0x7f0a0036

.field public static final jp_holiday_5_constitution:I = 0x7f0a0037

.field public static final jp_holiday_5_green:I = 0x7f0a0038

.field public static final jp_holiday_7_marine:I = 0x7f0a0039

.field public static final jp_holiday_9_autumn:I = 0x7f0a003a

.field public static final jp_holiday_9_elders:I = 0x7f0a003b

.field public static final jp_holiday_9_people:I = 0x7f0a003c

.field public static final jp_holiday_substitute:I = 0x7f0a003d

.field public static final korean_after_noon:I = 0x7f0a003e

.field public static final korean_before_noon:I = 0x7f0a003f

.field public static final korean_dawn:I = 0x7f0a0040

.field public static final korean_day:I = 0x7f0a0041

.field public static final korean_evening:I = 0x7f0a0042

.field public static final korean_hour:I = 0x7f0a0043

.field public static final korean_minute:I = 0x7f0a0044

.field public static final korean_month:I = 0x7f0a0045

.field public static final korean_morning:I = 0x7f0a0046

.field public static final korean_night:I = 0x7f0a0047

.field public static final korean_tomorrow:I = 0x7f0a0048

.field public static final korean_year:I = 0x7f0a0049

.field public static final list_day_format:I = 0x7f0a004a

.field public static final loading:I = 0x7f0a004b

.field public static final location_label:I = 0x7f0a0089

.field public static final magazine:I = 0x7f0a004c

.field public static final menu_handwriting_mode_on:I = 0x7f0a004d

.field public static final mini_agenda_theme:I = 0x7f0a008a

.field public static final mini_month_theme:I = 0x7f0a004e

.field public static final mini_task_theme:I = 0x7f0a008b

.field public static final mini_theme:I = 0x7f0a008c

.field public static final mini_today_theme:I = 0x7f0a004f

.field public static final mmddyy:I = 0x7f0a0050

.field public static final mmddyyE:I = 0x7f0a0051

.field public static final mmmyyyy:I = 0x7f0a0052

.field public static final month_no_event:I = 0x7f0a008d

.field public static final month_postfix:I = 0x7f0a0053

.field public static final month_theme:I = 0x7f0a0054

.field public static final month_title_1:I = 0x7f0a0055

.field public static final month_title_2:I = 0x7f0a0056

.field public static final month_weeknum:I = 0x7f0a008e

.field public static final my_event_title_label:I = 0x7f0a0057

.field public static final my_task:I = 0x7f0a0058

.field public static final no_due_date:I = 0x7f0a008f

.field public static final no_event:I = 0x7f0a0059

.field public static final no_event_thanks_message:I = 0x7f0a005a

.field public static final no_event_thanks_message_calendar:I = 0x7f0a005b

.field public static final no_event_thanks_message_magazine:I = 0x7f0a005c

.field public static final no_tasks:I = 0x7f0a0090

.field public static final no_title_label:I = 0x7f0a0091

.field public static final ocr:I = 0x7f0a0092

.field public static final ok:I = 0x7f0a0093

.field public static final overDue:I = 0x7f0a0094

.field public static final priorityHigh:I = 0x7f0a0095

.field public static final priorityLow:I = 0x7f0a0096

.field public static final priorityMedium:I = 0x7f0a0097

.field public static final refr:I = 0x7f0a0098

.field public static final refresh:I = 0x7f0a005d

.field public static final slash:I = 0x7f0a005e

.field public static final splanner_day_theme:I = 0x7f0a0099

.field public static final splanner_magazine_name:I = 0x7f0a005f

.field public static final splanner_mini_day_theme:I = 0x7f0a0060

.field public static final splanner_month_theme:I = 0x7f0a0061

.field public static final splanner_name:I = 0x7f0a0062

.field public static final splanner_task_theme:I = 0x7f0a009a

.field public static final start_date:I = 0x7f0a009b

.field public static final stms_version:I = 0x7f0a0063

.field public static final tap_to_create_event:I = 0x7f0a0064

.field public static final tap_to_create_task:I = 0x7f0a009c

.field public static final task_name_pl:I = 0x7f0a0065

.field public static final task_theme:I = 0x7f0a0066

.field public static final today_theme:I = 0x7f0a0067

.field public static final tomorrow_label:I = 0x7f0a009d

.field public static final unDated:I = 0x7f0a009e

.field public static final vietnamese_labor_day:I = 0x7f0a0068

.field public static final week_title:I = 0x7f0a0069

.field public static final weekday_day:I = 0x7f0a006a

.field public static final weekday_format:I = 0x7f0a006b

.field public static final wrong_range_error:I = 0x7f0a006c

.field public static final year_format:I = 0x7f0a006d

.field public static final year_postfix:I = 0x7f0a006e

.field public static final year_title:I = 0x7f0a006f

.field public static final yesterday_label:I = 0x7f0a009f

.field public static final yymmdd:I = 0x7f0a0070

.field public static final yymmddE:I = 0x7f0a0071

.field public static final yyyymmm:I = 0x7f0a0072


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 597
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
