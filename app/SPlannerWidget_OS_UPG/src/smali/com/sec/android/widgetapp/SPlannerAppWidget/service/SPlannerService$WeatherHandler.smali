.class Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherHandler;
.super Landroid/os/Handler;
.source "SPlannerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeatherHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;


# direct methods
.method private constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V
    .locals 0

    .prologue
    .line 80
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 1
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 83
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 86
    :pswitch_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 87
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->updateWeatherInfo()V

    goto :goto_0

    .line 83
    :pswitch_data_0
    .packed-switch 0x12c0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method
