.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;
.super Ljava/lang/Object;
.source "MonthView.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

.field final synthetic val$bIsInCurrentMonth:Z

.field final synthetic val$events:Ljava/util/ArrayList;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;Z)V
    .locals 0

    .prologue
    .line 706
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iput-object p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->val$events:Ljava/util/ArrayList;

    iput-boolean p3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->val$bIsInCurrentMonth:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    .line 709
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    if-nez v0, :cond_0

    .line 728
    :goto_0
    return-void

    .line 713
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->val$events:Ljava/util/ArrayList;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1002(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 714
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 715
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->reloadCurrentDayEvents()V

    .line 717
    :cond_1
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->val$bIsInCurrentMonth:Z

    if-eqz v0, :cond_2

    .line 718
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->initEvents()V

    .line 721
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->calEventDay()Ljava/util/ArrayList;

    move-result-object v1

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;
    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1102(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;)Ljava/util/ArrayList;

    .line 722
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    check-cast v0, [[Ljava/lang/String;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$902(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[Ljava/lang/String;)[[Ljava/lang/String;

    .line 723
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    check-cast v0, [[I

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventColorArray:[[I
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1202(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[I)[[I

    .line 724
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Ljava/util/ArrayList;

    move-result-object v0

    const/4 v2, 0x2

    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    check-cast v0, [[I

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventDayGapArray:[[I
    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1302(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[I)[[I

    .line 726
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # setter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z
    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$702(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Z)Z

    .line 727
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->invalidate()V

    goto :goto_0
.end method
