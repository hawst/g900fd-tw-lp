.class Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;
.super Ljava/lang/Object;
.source "SPlannerSurfaceWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 0

    .prologue
    .line 214
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 218
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_2

    .line 219
    :cond_0
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$100()Ljava/lang/String;

    move-result-object v2

    const-string v3, "there are no widget to refresh"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 232
    :cond_1
    return-void

    .line 222
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchEntirePreference(Landroid/content/Context;)V

    .line 225
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_1

    .line 226
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v1, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    .line 227
    .local v1, "theme":Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
    if-nez v1, :cond_3

    .line 225
    :goto_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 230
    :cond_3
    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onRefresh()V

    goto :goto_1
.end method
