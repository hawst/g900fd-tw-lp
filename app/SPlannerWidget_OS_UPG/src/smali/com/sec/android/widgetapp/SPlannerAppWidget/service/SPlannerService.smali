.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
.super Landroid/app/Service;
.source "SPlannerService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherHandler;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;
    }
.end annotation


# static fields
.field private static final CHANGE_SHARE:Ljava/lang/String; = "com.sec.android.intent.CHANGE_SHARE"

.field private static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static final MSG_WEATHER_DATA_UPDATED:I = 0x12c1

.field private static final MSG_WEATHER_SETTING_CHANGED:I = 0x12c0

.field private static final SEND_MSG:I = 0x1

.field private static final TAG:Ljava/lang/String; = "SPlannerService"


# instance fields
.field private final mBinder:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;

.field private mBroadcastReceiver:Landroid/content/BroadcastReceiver;

.field private final mCallbacks:Landroid/os/RemoteCallbackList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/RemoteCallbackList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;",
            ">;"
        }
    .end annotation
.end field

.field private mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

.field private final mHandler:Landroid/os/Handler;

.field private final mReceiver:Landroid/content/BroadcastReceiver;

.field private mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 38
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 45
    new-instance v0, Landroid/os/RemoteCallbackList;

    invoke-direct {v0}, Landroid/os/RemoteCallbackList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    .line 109
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mReceiver:Landroid/content/BroadcastReceiver;

    .line 167
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBinder:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;

    .line 181
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    .prologue
    .line 38
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    return-object v0
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "arg0"    # Landroid/content/Intent;

    .prologue
    .line 212
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBinder:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;

    return-object v0
.end method

.method public onCreate()V
    .locals 6

    .prologue
    .line 130
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 132
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 134
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 135
    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 136
    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 137
    const-string v2, "clock.date_format_changed"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 138
    const-string v2, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v0, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 140
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 142
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 143
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 145
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->startCurrentLocationWeatherDataService()V

    .line 146
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->updateWeatherInfo()V

    .line 148
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->isReceiverEnabled:Z

    if-eqz v2, :cond_0

    .line 149
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$1;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    .line 151
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 152
    .local v1, "filter_weather":Landroid/content/IntentFilter;
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    iget-object v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 153
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    iget-object v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 154
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    iget-object v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 155
    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v1, v2}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 157
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v2, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 161
    .end local v1    # "filter_weather":Landroid/content/IntentFilter;
    :cond_0
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

    .line 162
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "content://settings/system/date_format"

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

    invoke-virtual {v2, v3, v4, v5}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 165
    return-void
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 218
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    if-eqz v0, :cond_0

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;

    invoke-virtual {v0}, Landroid/os/RemoteCallbackList;->kill()V

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 223
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_2

    .line 227
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 230
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

    if-eqz v0, :cond_3

    .line 231
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$DateFormatObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 234
    :cond_3
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 236
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-eqz v0, :cond_4

    .line 237
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->stopCurrentLocationWeatherDataService()V

    .line 240
    :cond_4
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_5

    sget-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->isReceiverEnabled:Z

    if-eqz v0, :cond_5

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mBroadcastReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 244
    :cond_5
    return-void
.end method
