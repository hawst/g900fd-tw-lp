.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;
.super Ljava/lang/Object;
.source "MonthTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V
    .locals 0

    .prologue
    .line 254
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 257
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 258
    .local v0, "other":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v1, :cond_0

    .line 259
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    const/4 v2, -0x1

    invoke-virtual {v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getPrevNextTime(II)Landroid/text/format/Time;

    move-result-object v0

    .line 263
    :goto_0
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isValidRange(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 264
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->launchOutOfBoundToast()V
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    .line 268
    :goto_1
    return-void

    .line 261
    :cond_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    goto :goto_0

    .line 266
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    const/4 v2, 0x1

    invoke-virtual {v1, v0, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->goTo(Landroid/text/format/Time;ZZ)V

    goto :goto_1
.end method
