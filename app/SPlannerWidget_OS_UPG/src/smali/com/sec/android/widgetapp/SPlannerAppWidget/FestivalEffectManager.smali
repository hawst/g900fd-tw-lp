.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;
.super Ljava/lang/Object;
.source "FestivalEffectManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$1;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$QueryHandler;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;
    }
.end annotation


# static fields
.field private static final CHINA_HOLIDAY_PROJECTION:[Ljava/lang/String;

.field private static final CHINA_HOLIDAY_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "FestivalEffectManager"

.field public static final TYPE_HOLIDAY:I = 0x1

.field public static final TYPE_INVALID:I = -0x1

.field public static final TYPE_WORKING_DAY:I = 0x2

.field private static sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

.field private static sPreUpdateHolidayListTime:J


# instance fields
.field private final mHolidayEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 34
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    .line 51
    const-wide/16 v0, 0x0

    sput-wide v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sPreUpdateHolidayListTime:J

    .line 53
    const/4 v0, 0x4

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "startDay"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->CHINA_HOLIDAY_PROJECTION:[Ljava/lang/String;

    .line 57
    const-string v0, "content://com.sec.android.chinaholiday/holidays_with_workingday"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->CHINA_HOLIDAY_URI:Landroid/net/Uri;

    return-void
.end method

.method private constructor <init>()V
    .locals 1

    .prologue
    .line 40
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 50
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    .line 41
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 30
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->buildFromCursor(Landroid/database/Cursor;)V

    return-void
.end method

.method private buildFromCursor(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 107
    if-nez p1, :cond_1

    .line 108
    const-string v1, "FestivalEffectManager"

    const-string v2, "china holiday cursor is null"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 133
    :cond_0
    :goto_0
    return-void

    .line 113
    :cond_1
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_3

    .line 114
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 119
    :cond_2
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$1;)V

    .line 120
    .local v0, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;
    const/4 v1, 0x0

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->id:I

    .line 121
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->title:Ljava/lang/String;

    .line 122
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->begin:I

    .line 123
    const/4 v1, 0x3

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->startDay:I

    .line 125
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 126
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-nez v1, :cond_2

    .line 129
    .end local v0    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;
    :cond_3
    if-eqz p1, :cond_0

    .line 130
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 129
    :catchall_0
    move-exception v1

    if-eqz p1, :cond_4

    .line 130
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    :cond_4
    throw v1
.end method

.method public static declared-synchronized getInstance()Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;
    .locals 2

    .prologue
    .line 44
    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    if-nez v0, :cond_0

    .line 45
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    .line 47
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    monitor-exit v1

    return-object v0

    .line 44
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0
.end method


# virtual methods
.method public clearHolidayList()V
    .locals 2

    .prologue
    .line 68
    const-string v0, "FestivalEffectManager"

    const-string v1, "clearHolidayList"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    .line 71
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 73
    :cond_0
    return-void
.end method

.method public isFestivalEffectDay(Landroid/content/ContentResolver;Landroid/text/format/Time;)I
    .locals 8
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v3, 0x1

    .line 136
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->updateHolidayList(Landroid/content/ContentResolver;)V

    .line 138
    invoke-virtual {p2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    iget-wide v6, p2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v2

    .line 140
    .local v2, "startDay":I
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_2

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;

    .line 141
    .local v1, "mHolidayEvent":Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;
    iget v4, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->startDay:I

    if-ne v2, v4, :cond_0

    .line 142
    const-string v4, "WorkingDay"

    iget-object v5, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;->title:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 143
    const/4 v3, 0x2

    .line 150
    .end local v1    # "mHolidayEvent":Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$EventInfo;
    :cond_1
    :goto_0
    return v3

    :cond_2
    const/4 v3, -0x1

    goto :goto_0
.end method

.method public updateHolidayList(Landroid/content/ContentResolver;)V
    .locals 10
    .param p1, "cr"    # Landroid/content/ContentResolver;

    .prologue
    const/4 v2, 0x0

    .line 76
    if-eqz p1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->mHolidayEvents:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v1

    if-nez v1, :cond_1

    .line 92
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 82
    .local v8, "currentTime":J
    const-string v1, "FestivalEffectManager"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "updateHolidayList "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-wide v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sPreUpdateHolidayListTime:J

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    sget-wide v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sPreUpdateHolidayListTime:J

    sub-long v4, v8, v4

    const-wide/16 v6, 0x3e8

    cmp-long v1, v4, v6

    if-gez v1, :cond_2

    .line 84
    const-string v1, "FestivalEffectManager"

    const-string v2, "updateHolidayList : too already update request"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 87
    :cond_2
    sput-wide v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->sPreUpdateHolidayListTime:J

    .line 89
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$QueryHandler;

    invoke-direct {v0, p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$QueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;Landroid/content/ContentResolver;)V

    .line 90
    .local v0, "queryHandler":Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$QueryHandler;
    const/4 v1, -0x1

    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->CHINA_HOLIDAY_URI:Landroid/net/Uri;

    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->CHINA_HOLIDAY_PROJECTION:[Ljava/lang/String;

    move-object v5, v2

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager$QueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
