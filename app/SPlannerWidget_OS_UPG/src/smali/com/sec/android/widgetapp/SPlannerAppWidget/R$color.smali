.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final appwidget_date:I = 0x7f070000

.field public static final appwidget_easy_bg:I = 0x7f070001

.field public static final appwidget_item_declined_color:I = 0x7f070002

.field public static final appwidget_item_standard_color:I = 0x7f070003

.field public static final appwidget_month:I = 0x7f070004

.field public static final appwidget_no_events:I = 0x7f070005

.field public static final appwidget_title:I = 0x7f070006

.field public static final appwidget_week:I = 0x7f070007

.field public static final appwidget_when:I = 0x7f070008

.field public static final appwidget_where:I = 0x7f070009

.field public static final background_color:I = 0x7f07000a

.field public static final black:I = 0x7f07000b

.field public static final black_dim:I = 0x7f07000c

.field public static final calendar_pressed:I = 0x7f07000d

.field public static final day_event_attendees_color:I = 0x7f07000e

.field public static final day_event_divider_horizontal:I = 0x7f07000f

.field public static final day_event_time_color:I = 0x7f070010

.field public static final day_event_title_color:I = 0x7f070011

.field public static final day_text_color_easy:I = 0x7f070012

.field public static final default_date_of_today:I = 0x7f070013

.field public static final divider:I = 0x7f070014

.field public static final event_title:I = 0x7f070015

.field public static final list_bg_color:I = 0x7f070016

.field public static final list_pressed:I = 0x7f070017

.field public static final mg_task_dim_color:I = 0x7f070018

.field public static final mg_today_text_color:I = 0x7f070019

.field public static final month_day_event_color:I = 0x7f07001a

.field public static final month_day_event_dim_color:I = 0x7f07001b

.field public static final month_day_number:I = 0x7f07001c

.field public static final month_day_number_dim:I = 0x7f07001d

.field public static final month_event_count_bg_color:I = 0x7f07001e

.field public static final month_event_count_text_color:I = 0x7f07001f

.field public static final month_event_date:I = 0x7f070020

.field public static final month_event_focused_color:I = 0x7f070021

.field public static final month_event_list_divider_color:I = 0x7f070022

.field public static final month_focus_allday_box_color:I = 0x7f070023

.field public static final month_focused_color:I = 0x7f070024

.field public static final month_focused_today_color:I = 0x7f070025

.field public static final month_line:I = 0x7f070026

.field public static final month_list_item_time_text_color:I = 0x7f070027

.field public static final month_list_item_title_text_color:I = 0x7f070028

.field public static final month_list_no_event_text_color:I = 0x7f070029

.field public static final month_local_holiday_saturday_number:I = 0x7f07002a

.field public static final month_local_holiday_saturday_number_dim:I = 0x7f07002b

.field public static final month_local_holiday_sunday_number:I = 0x7f07002c

.field public static final month_local_holiday_sunday_number_dim:I = 0x7f07002d

.field public static final month_lunar_text_color:I = 0x7f07002e

.field public static final month_memo_color:I = 0x7f07002f

.field public static final month_memo_stroke_color:I = 0x7f070030

.field public static final month_non_focus_bg_color:I = 0x7f070031

.field public static final month_overlay_add_color:I = 0x7f070032

.field public static final month_overlay_mul_color:I = 0x7f070033

.field public static final month_saturday_number:I = 0x7f070034

.field public static final month_saturday_number_dim:I = 0x7f070035

.field public static final month_sunday_number:I = 0x7f070036

.field public static final month_sunday_number_dim:I = 0x7f070037

.field public static final month_title_color:I = 0x7f070038

.field public static final month_title_color_easy:I = 0x7f070039

.field public static final month_today_marker:I = 0x7f07003a

.field public static final month_today_number:I = 0x7f07003b

.field public static final month_today_number_color:I = 0x7f07003c

.field public static final month_view_background_color:I = 0x7f07003d

.field public static final month_view_text_event_color:I = 0x7f07003e

.field public static final month_week_name:I = 0x7f07003f

.field public static final month_week_num_color:I = 0x7f070040

.field public static final month_week_w:I = 0x7f070041

.field public static final no_calendar_color:I = 0x7f070042

.field public static final no_event_text_color:I = 0x7f070043

.field public static final shadow_color:I = 0x7f070044

.field public static final task_account_color00:I = 0x7f070045

.field public static final task_account_color01:I = 0x7f070046

.field public static final task_account_color02:I = 0x7f070047

.field public static final task_account_color03:I = 0x7f070048

.field public static final task_account_color04:I = 0x7f070049

.field public static final task_account_color05:I = 0x7f07004a

.field public static final task_account_color06:I = 0x7f07004b

.field public static final task_account_color07:I = 0x7f07004c

.field public static final task_account_color08:I = 0x7f07004d

.field public static final task_account_color09:I = 0x7f07004e

.field public static final task_account_color10:I = 0x7f07004f

.field public static final task_account_color11:I = 0x7f070050

.field public static final task_account_color12:I = 0x7f070051

.field public static final task_account_color13:I = 0x7f070052

.field public static final task_account_color14:I = 0x7f070053

.field public static final task_account_color15:I = 0x7f070054

.field public static final task_account_color16:I = 0x7f070055

.field public static final task_account_color17:I = 0x7f070056

.field public static final task_account_color18:I = 0x7f070057

.field public static final task_dim_color:I = 0x7f070058

.field public static final task_list_divider_horizontal:I = 0x7f070059

.field public static final task_title_color:I = 0x7f07005a

.field public static final text_shadow_color:I = 0x7f07005b

.field public static final title_color_easy:I = 0x7f07005c

.field public static final title_dim_color:I = 0x7f07005d

.field public static final title_shadow_color_easy:I = 0x7f07005e

.field public static final today_divider_view_color:I = 0x7f07005f

.field public static final today_label_color:I = 0x7f070060

.field public static final top_menu_text_color:I = 0x7f07006e

.field public static final trans_month_focused_today_color:I = 0x7f070061

.field public static final trans_no_event_text_color:I = 0x7f070062

.field public static final wallpaper_sticker_color_40:I = 0x7f070063

.field public static final wallpaper_sticker_color_41:I = 0x7f070064

.field public static final wallpaper_sticker_color_42:I = 0x7f070065

.field public static final wallpaper_sticker_color_43:I = 0x7f070066

.field public static final weather_city_text_color:I = 0x7f070067

.field public static final weather_temp_text_color:I = 0x7f070068

.field public static final white:I = 0x7f070069

.field public static final widget_default_background_color:I = 0x7f07006a

.field public static final widget_default_background_color_minitoday:I = 0x7f07006b

.field public static final widget_label:I = 0x7f07006c

.field public static final widget_subtitle_horizontal_line_color:I = 0x7f07006d


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
