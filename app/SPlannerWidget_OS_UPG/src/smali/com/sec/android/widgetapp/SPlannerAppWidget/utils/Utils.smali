.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils$IAccountTypeConstants;
    }
.end annotation


# static fields
.field public static final APPWIDGET_DATA_TYPE:Ljava/lang/String; = "vnd.android.data/update"

.field public static AS_FLAG:Z = false

.field public static final CONTACT_ACCOUNT_TYPE:Ljava/lang/String; = "contact_account_type"

.field public static final CONTACT_DATA_ID:Ljava/lang/String; = "contact_data_id"

.field public static final CONTACT_RAW_ID:Ljava/lang/String; = "contact_id"

.field public static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field public static final EPOCH_JULIAN_DAY:I = 0x253d8c

.field public static final FACEBOOK_SCHEDULE_ID:Ljava/lang/String; = "facebook_schedule_id"

.field public static final HANDWRITING_MODE:Ljava/lang/String; = "handwriting_mode"

.field public static HORIZONTAL_SCROLL_THRESHOLD:I = 0x0

.field public static final HOUR_IN_MILLIS:J = 0x36ee80L

.field private static final INTENSITY_ADJUST:F = 0.8f

.field public static final INTENT_KEY_DETAIL_VIEW:Ljava/lang/String; = "DETAIL_VIEW"

.field public static final INTENT_KEY_VIEW_TYPE:Ljava/lang/String; = "VIEW"

.field public static final INTENT_VALUE_VIEW_TYPE_AGENDA:Ljava/lang/String; = "AGENDA"

.field public static final INTENT_VALUE_VIEW_TYPE_DAY:Ljava/lang/String; = "DAY"

.field public static final INTENT_VALUE_VIEW_TYPE_MONTH:Ljava/lang/String; = "MONTH"

.field public static final INTENT_VALUE_VIEW_TYPE_TASK:Ljava/lang/String; = "TASK"

.field public static final KEY_NOTIFICATION_MOMENT:Ljava/lang/String; = "preferences_notification_moment"

.field public static final MAX_JULIANDAY:I = 0x259d23

.field public static final MAX_YEAR:I = 0x7f4

.field public static final MINUTE_IN_MILLIS:J = 0xea60L

.field public static final MIN_JULIANDAY:I = 0x24dc87

.field public static final MIN_YEAR:I = 0x76e

.field private static final SATURATION_ADJUST:F = 1.3f

.field public static final SECOND_IN_MILLIS:J = 0x3e8L

.field private static final SHARED_PREFS_NAME:Ljava/lang/String; = "com.android.calendar_preferences"

.field public static final SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

.field private static final TAG:Ljava/lang/String; = "SPlannerAppWidget.Utils"

.field private static UNNEEDED_SPACES:Ljava/util/regex/Pattern;

.field private static final mTZUtils:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 68
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->AS_FLAG:Z

    .line 69
    const/16 v0, 0x32

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->HORIZONTAL_SCROLL_THRESHOLD:I

    .line 76
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 81
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;

    const-string v1, "com.android.calendar_preferences"

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->mTZUtils:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;

    .line 108
    const-string v0, "[\t\n]"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->UNNEEDED_SPACES:Ljava/util/regex/Pattern;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 110
    return-void
.end method

.method public static arrangeEventList(Ljava/util/ArrayList;I)V
    .locals 6
    .param p1, "currentDay"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 307
    .local p0, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    const/4 v1, 0x0

    .line 308
    .local v1, "dirty":Z
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 309
    .local v0, "alldayEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 311
    .local v4, "norEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 312
    .local v2, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    const/4 v5, -0x1

    if-eq p1, v5, :cond_1

    .line 313
    iget v5, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    if-gt v5, p1, :cond_0

    iget v5, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    if-lt v5, p1, :cond_0

    .line 317
    :cond_1
    iget-boolean v5, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eqz v5, :cond_2

    .line 318
    invoke-virtual {v0, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 319
    if-nez v1, :cond_0

    .line 320
    const/4 v1, 0x1

    goto :goto_0

    .line 322
    :cond_2
    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 323
    if-nez v1, :cond_0

    .line 324
    const/4 v1, 0x1

    goto :goto_0

    .line 328
    .end local v2    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :cond_3
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 329
    if-eqz v1, :cond_4

    .line 330
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 331
    invoke-virtual {p0, v4}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 334
    :cond_4
    return-void
.end method

.method public static displaySubstHolidaysCalendar(Landroid/content/Context;)Z
    .locals 10
    .param p0, "cont"    # Landroid/content/Context;

    .prologue
    const/4 v4, 0x0

    const/4 v9, 0x0

    const/4 v8, 0x1

    .line 734
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 735
    .local v0, "resolver":Landroid/content/ContentResolver;
    const/4 v6, 0x0

    .line 738
    .local v6, "bResult":Z
    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v9

    const-string v3, "visible"

    aput-object v3, v2, v8

    const-string v3, "name=\'legalSubstHoliday\'"

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 741
    .local v7, "calendarCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_1

    .line 743
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v1

    if-lez v1, :cond_0

    .line 744
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 745
    const/4 v1, 0x1

    invoke-interface {v7, v1}, Landroid/database/Cursor;->getInt(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    if-ne v1, v8, :cond_2

    move v6, v8

    .line 750
    :cond_0
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 754
    :cond_1
    return v6

    :cond_2
    move v6, v9

    .line 745
    goto :goto_0

    .line 750
    :catchall_0
    move-exception v1

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;
    .locals 9
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startMillis"    # J
    .param p3, "endMillis"    # J
    .param p5, "flags"    # I

    .prologue
    .line 291
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->mTZUtils:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;

    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-wide v4, p3

    move v6, p5

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;->formatDateRange(Landroid/content/Context;JJIZ)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static formatDateTimeString(Landroid/content/Context;ZJJZLjava/lang/String;)Ljava/lang/String;
    .locals 10
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "allday"    # Z
    .param p2, "startMillis"    # J
    .param p4, "endMillis"    # J
    .param p6, "displaySameDate"    # Z
    .param p7, "timezone"    # Ljava/lang/String;

    .prologue
    .line 252
    const/high16 v6, 0x10000

    move-object v1, p0

    move-wide v2, p2

    move-wide v4, p2

    invoke-static/range {v1 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v0

    .line 253
    .local v0, "fromDate":Ljava/lang/String;
    const/high16 v6, 0x10000

    move-object v1, p0

    move-wide v2, p4

    move-wide v4, p4

    invoke-static/range {v1 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v8

    .line 255
    .local v8, "toDate":Ljava/lang/String;
    const/16 v6, 0x201

    .line 256
    .local v6, "flags":I
    invoke-static {p0}, Landroid/text/format/DateFormat;->is24HourFormat(Landroid/content/Context;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 257
    or-int/lit16 v6, v6, 0x80

    :cond_0
    move-object v1, p0

    move-wide v2, p2

    move-wide v4, p2

    .line 260
    invoke-static/range {v1 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v7

    .local v7, "fromTime":Ljava/lang/String;
    move-object v1, p0

    move-wide v2, p4

    move-wide v4, p4

    .line 261
    invoke-static/range {v1 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v9

    .line 263
    .local v9, "toTime":Ljava/lang/String;
    cmp-long v1, p2, p4

    if-nez v1, :cond_1

    .line 272
    .end local v7    # "fromTime":Ljava/lang/String;
    :goto_0
    return-object v7

    .line 265
    .restart local v7    # "fromTime":Ljava/lang/String;
    :cond_1
    invoke-virtual {v0, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_3

    .line 266
    if-eqz p6, :cond_2

    .line 267
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 269
    :cond_2
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0

    .line 272
    :cond_3
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " - "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    goto :goto_0
.end method

.method public static getCalendarPreferenceNotificationMoment(Landroid/content/Context;)Z
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 295
    const/4 v0, 0x0

    .line 297
    .local v0, "cal":Landroid/content/Context;
    :try_start_0
    const-string v3, "com.android.calendar"

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->createPackageContext(Ljava/lang/String;I)Landroid/content/Context;
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v0

    .line 301
    :goto_0
    invoke-static {v0}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 303
    .local v2, "prefs":Landroid/content/SharedPreferences;
    const-string v3, "preferences_notification_moment"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    return v3

    .line 298
    .end local v2    # "prefs":Landroid/content/SharedPreferences;
    :catch_0
    move-exception v1

    .line 299
    .local v1, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    invoke-virtual {v1}, Landroid/content/pm/PackageManager$NameNotFoundException;->printStackTrace()V

    goto :goto_0
.end method

.method public static getConfigBool(Landroid/content/Context;I)Z
    .locals 1
    .param p0, "c"    # Landroid/content/Context;
    .param p1, "key"    # I

    .prologue
    .line 564
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    return v0
.end method

.method public static getDateFormatOrder()[C
    .locals 3

    .prologue
    .line 422
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    const-string v2, "yyyyMMMdd"

    invoke-static {v1, v2}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 423
    .local v0, "pattern":Ljava/lang/String;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDateFormatOrder(Ljava/lang/String;)[C

    move-result-object v1

    return-object v1
.end method

.method private static getDateFormatOrder(Ljava/lang/String;)[C
    .locals 14
    .param p0, "pattern"    # Ljava/lang/String;

    .prologue
    const/16 v13, 0x4c

    const/16 v12, 0x79

    const/16 v11, 0x64

    const/16 v10, 0x4d

    const/16 v9, 0x27

    .line 427
    const/4 v8, 0x3

    new-array v2, v8, [C

    .line 428
    .local v2, "result":[C
    const/4 v3, 0x0

    .line 429
    .local v3, "resultIndex":I
    const/4 v5, 0x0

    .line 430
    .local v5, "sawDay":Z
    const/4 v6, 0x0

    .line 431
    .local v6, "sawMonth":Z
    const/4 v7, 0x0

    .line 433
    .local v7, "sawYear":Z
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-ge v1, v8, :cond_b

    .line 434
    invoke-virtual {p0, v1}, Ljava/lang/String;->charAt(I)C

    move-result v0

    .line 435
    .local v0, "ch":C
    if-eq v0, v11, :cond_0

    if-eq v0, v13, :cond_0

    if-eq v0, v10, :cond_0

    if-ne v0, v12, :cond_5

    .line 436
    :cond_0
    if-ne v0, v11, :cond_2

    if-nez v5, :cond_2

    .line 437
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "resultIndex":I
    .local v4, "resultIndex":I
    aput-char v11, v2, v3

    .line 438
    const/4 v5, 0x1

    move v3, v4

    .line 433
    .end local v4    # "resultIndex":I
    .restart local v3    # "resultIndex":I
    :cond_1
    :goto_1
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 439
    :cond_2
    if-eq v0, v13, :cond_3

    if-ne v0, v10, :cond_4

    :cond_3
    if-nez v6, :cond_4

    .line 440
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "resultIndex":I
    .restart local v4    # "resultIndex":I
    aput-char v10, v2, v3

    .line 441
    const/4 v6, 0x1

    move v3, v4

    .end local v4    # "resultIndex":I
    .restart local v3    # "resultIndex":I
    goto :goto_1

    .line 442
    :cond_4
    if-ne v0, v12, :cond_1

    if-nez v7, :cond_1

    .line 443
    add-int/lit8 v4, v3, 0x1

    .end local v3    # "resultIndex":I
    .restart local v4    # "resultIndex":I
    aput-char v12, v2, v3

    .line 444
    const/4 v7, 0x1

    move v3, v4

    .end local v4    # "resultIndex":I
    .restart local v3    # "resultIndex":I
    goto :goto_1

    .line 446
    :cond_5
    const/16 v8, 0x61

    if-lt v0, v8, :cond_6

    const/16 v8, 0x7a

    if-le v0, v8, :cond_7

    :cond_6
    const/16 v8, 0x41

    if-lt v0, v8, :cond_8

    const/16 v8, 0x5a

    if-gt v0, v8, :cond_8

    .line 447
    :cond_7
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad pattern character \'"

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, "\' in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 449
    :cond_8
    if-ne v0, v9, :cond_1

    .line 450
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_9

    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p0, v8}, Ljava/lang/String;->charAt(I)C

    move-result v8

    if-ne v8, v9, :cond_9

    .line 451
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 453
    :cond_9
    add-int/lit8 v8, v1, 0x1

    invoke-virtual {p0, v9, v8}, Ljava/lang/String;->indexOf(II)I

    move-result v1

    .line 454
    const/4 v8, -0x1

    if-ne v1, v8, :cond_a

    .line 455
    new-instance v8, Ljava/lang/IllegalArgumentException;

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Bad quoting in "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-direct {v8, v9}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v8

    .line 457
    :cond_a
    add-int/lit8 v1, v1, 0x1

    goto/16 :goto_1

    .line 461
    .end local v0    # "ch":C
    :cond_b
    return-object v2
.end method

.method public static getDeclinedColorFromColor(I)I
    .locals 10
    .param p0, "color"    # I

    .prologue
    const/high16 v9, 0xff0000

    const v8, 0xff00

    const/high16 v7, -0x1000000

    .line 539
    const/4 v2, -0x1

    .line 540
    .local v2, "bg":I
    const/16 v0, 0x66

    .line 541
    .local v0, "a":I
    and-int v5, p0, v9

    mul-int/2addr v5, v0

    const/high16 v6, -0x67990000

    add-int/2addr v5, v6

    and-int v4, v5, v7

    .line 542
    .local v4, "r":I
    and-int v5, p0, v8

    mul-int/2addr v5, v0

    const v6, 0x986700

    add-int/2addr v5, v6

    and-int v3, v5, v9

    .line 543
    .local v3, "g":I
    and-int/lit16 v5, p0, 0xff

    mul-int/2addr v5, v0

    const v6, 0x9867

    add-int/2addr v5, v6

    and-int v1, v5, v8

    .line 544
    .local v1, "b":I
    or-int v5, v4, v3

    or-int/2addr v5, v1

    shr-int/lit8 v5, v5, 0x8

    or-int/2addr v5, v7

    return v5
.end method

.method public static getDefaultOrientation(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 676
    const/4 v0, 0x2

    return v0
.end method

.method public static getDisplayColorFromColor(I)I
    .locals 5
    .param p0, "color"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 482
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 483
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 484
    aget v1, v0, v3

    const v2, 0x3fa66666    # 1.3f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v3

    .line 485
    aget v1, v0, v4

    const v2, 0x3f4ccccd    # 0.8f

    mul-float/2addr v1, v2

    aput v1, v0, v4

    .line 486
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method

.method public static getFirstDayOfWeek(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 548
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->getWeekStartDay()I

    move-result v0

    .line 550
    .local v0, "firstDayOfWeek":I
    const/4 v1, -0x1

    if-ne v0, v1, :cond_0

    .line 551
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/Calendar;->getFirstDayOfWeek()I

    move-result v0

    .line 552
    const/4 v1, 0x7

    if-ne v0, v1, :cond_1

    .line 553
    const/4 v0, 0x7

    .line 560
    :cond_0
    :goto_0
    return v0

    .line 554
    :cond_1
    const/4 v1, 0x2

    if-ne v0, v1, :cond_2

    .line 555
    const/4 v0, 0x2

    goto :goto_0

    .line 557
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static getHideContactEventSelection(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "originalWhere"    # Ljava/lang/String;

    .prologue
    .line 465
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->hideContactsEvents()Z

    move-result v2

    if-nez v2, :cond_0

    .line 478
    .end local p1    # "originalWhere":Ljava/lang/String;
    :goto_0
    return-object p1

    .line 469
    .restart local p1    # "originalWhere":Ljava/lang/String;
    :cond_0
    const-string v0, "contact_id is null"

    .line 472
    .local v0, "hideWhere":Ljava/lang/String;
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 473
    const-string v1, "contact_id is null"

    .local v1, "result":Ljava/lang/String;
    :goto_1
    move-object p1, v1

    .line 478
    goto :goto_0

    .line 475
    .end local v1    # "result":Ljava/lang/String;
    :cond_1
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    .line 476
    .restart local v1    # "result":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "contact_id is null)"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_1
.end method

.method public static getJulianDay(JJ)I
    .locals 8
    .param p0, "millis"    # J
    .param p2, "gmtoff"    # J

    .prologue
    .line 226
    const-wide/16 v4, 0x0

    cmp-long v4, p0, v4

    if-gez v4, :cond_0

    .line 227
    const-wide/32 v4, 0x5265bff

    sub-long/2addr p0, v4

    .line 229
    :cond_0
    const-wide/16 v4, 0x3e8

    mul-long v2, p2, v4

    .line 230
    .local v2, "offsetMillis":J
    add-long v4, p0, v2

    const-wide/32 v6, 0x5265c00

    div-long v0, v4, v6

    .line 231
    .local v0, "julianDay":J
    long-to-int v4, v0

    const v5, 0x253d8c    # 3.419992E-39f

    add-int/2addr v4, v5

    return v4
.end method

.method public static getLightBoldTypeface()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 730
    const-string v0, "sec-roboto-light"

    const/4 v1, 0x1

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public static getLightTypeface()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    .line 721
    const-string v0, "sec-roboto-light"

    const/4 v1, 0x0

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    return-object v0
.end method

.method public static getOffsetMilliesFromSystemTZ(Ljava/lang/String;)J
    .locals 8
    .param p0, "calendarTimezoneId"    # Ljava/lang/String;

    .prologue
    .line 134
    if-nez p0, :cond_0

    .line 135
    const-string p0, "GMT"

    .line 138
    :cond_0
    invoke-static {}, Ljava/util/TimeZone;->getDefault()Ljava/util/TimeZone;

    move-result-object v1

    .line 139
    .local v1, "systemTimeZone":Ljava/util/TimeZone;
    invoke-static {p0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    .line 140
    .local v0, "calendarTimezone":Ljava/util/TimeZone;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-virtual {v1, v4, v5}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v4

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    invoke-virtual {v0, v6, v7}, Ljava/util/TimeZone;->getOffset(J)I

    move-result v5

    sub-int/2addr v4, v5

    int-to-long v2, v4

    .line 142
    .local v2, "offSet":J
    return-wide v2
.end method

.method public static getRegularTypeface()Landroid/graphics/Typeface;
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 725
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isKorean()Z

    move-result v0

    if-eqz v0, :cond_0

    const-string v0, "sec-roboto-regular"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    const-string v0, "sans-serif"

    invoke-static {v0, v1}, Landroid/graphics/Typeface;->create(Ljava/lang/String;I)Landroid/graphics/Typeface;

    move-result-object v0

    goto :goto_0
.end method

.method public static getTaskSelection(Landroid/content/Context;JJ)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "startMillis"    # J
    .param p3, "endMillis"    # J

    .prologue
    .line 406
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    .line 407
    .local v0, "builder":Ljava/lang/StringBuilder;
    const-string v1, " selected = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 408
    const-string v1, " AND groupSelected = 1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 409
    const-string v1, " AND deleted = 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 410
    const-string v1, " AND utc_due_date IS NOT NULL"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 411
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND utc_due_date >= "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1, p2}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 412
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, " AND utc_due_date < "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p3, p4}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 414
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->hideCompletedTask()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 415
    const-string v1, " AND complete = 0"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 418
    :cond_0
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 533
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->mTZUtils:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getUseLocationStatus(Landroid/content/Context;)Z
    .locals 11
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 758
    const-string v2, "content://com.sec.android.daemonapp.ap.accuweather.provider/settings"

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 759
    .local v1, "ACCU_SETTING_URI":Landroid/net/Uri;
    const/4 v8, 0x0

    .line 760
    .local v8, "result":I
    if-nez p0, :cond_0

    .line 784
    :goto_0
    return v10

    .line 764
    :cond_0
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isChinaFeature()Z

    move-result v2

    if-eqz v2, :cond_1

    move v10, v9

    .line 765
    goto :goto_0

    .line 768
    :cond_1
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 769
    .local v0, "cp":Landroid/content/ContentResolver;
    if-eqz v0, :cond_3

    .line 770
    new-array v2, v9, [Ljava/lang/String;

    const-string v4, "SHOW_USE_LOCATION_POPUP"

    aput-object v4, v2, v10

    move-object v4, v3

    move-object v5, v3

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 773
    .local v6, "cursor":Landroid/database/Cursor;
    if-eqz v6, :cond_3

    .line 774
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v2

    if-eqz v2, :cond_2

    .line 776
    const/4 v2, 0x0

    :try_start_0
    invoke-interface {v6, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 781
    :cond_2
    :goto_1
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 784
    .end local v6    # "cursor":Landroid/database/Cursor;
    :cond_3
    if-eqz v8, :cond_4

    move v2, v9

    :goto_2
    move v10, v2

    goto :goto_0

    .line 777
    .restart local v6    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v7

    .line 778
    .local v7, "e":Ljava/lang/NumberFormatException;
    invoke-virtual {v7}, Ljava/lang/NumberFormatException;->printStackTrace()V

    goto :goto_1

    .end local v6    # "cursor":Landroid/database/Cursor;
    .end local v7    # "e":Ljava/lang/NumberFormatException;
    :cond_4
    move v2, v10

    .line 784
    goto :goto_2
.end method

.method public static getWidgetScheduledUpdateAction(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 500
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".APPWIDGET_SCHEDULED_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 493
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {p0}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".APPWIDGET_UPDATE"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static final instancesQuery(Landroid/net/Uri$Builder;Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 11
    .param p0, "builder"    # Landroid/net/Uri$Builder;
    .param p1, "cr"    # Landroid/content/ContentResolver;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "orderBy"    # Ljava/lang/String;

    .prologue
    .line 683
    const/4 v9, 0x0

    .line 684
    .local v9, "cursor":Landroid/database/Cursor;
    const-string v8, "visible=?"

    .line 685
    .local v8, "WHERE_CALENDARS_SELECTED":Ljava/lang/String;
    const/4 v0, 0x1

    new-array v7, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    const-string v1, "1"

    aput-object v1, v7, v0

    .line 688
    .local v7, "WHERE_CALENDARS_ARGS":[Ljava/lang/String;
    const-string v6, "begin ASC"

    .line 690
    .local v6, "DEFAULT_SORT_ORDER":Ljava/lang/String;
    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 691
    move-object p3, v8

    .line 692
    move-object p4, v7

    .line 703
    :goto_0
    :try_start_0
    invoke-virtual {p0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    if-nez p5, :cond_2

    move-object v5, v6

    :goto_1
    move-object v0, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v9

    .line 705
    if-eqz v9, :cond_3

    move-object v0, v9

    .line 715
    :goto_2
    return-object v0

    .line 694
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 695
    if-eqz p4, :cond_1

    array-length v0, p4

    if-lez v0, :cond_1

    .line 696
    array-length v0, p4

    add-int/lit8 v0, v0, 0x1

    invoke-static {p4, v0}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p4

    .end local p4    # "selectionArgs":[Ljava/lang/String;
    check-cast p4, [Ljava/lang/String;

    .line 697
    .restart local p4    # "selectionArgs":[Ljava/lang/String;
    array-length v0, p4

    add-int/lit8 v0, v0, -0x1

    const/4 v1, 0x0

    aget-object v1, v7, v1

    aput-object v1, p4, v0

    goto :goto_0

    .line 699
    :cond_1
    move-object p4, v7

    goto :goto_0

    :cond_2
    move-object/from16 v5, p5

    .line 703
    goto :goto_1

    .line 708
    :catch_0
    move-exception v10

    .line 709
    .local v10, "e":Ljava/lang/Throwable;
    invoke-virtual {v10}, Ljava/lang/Throwable;->printStackTrace()V

    .line 710
    if-eqz v9, :cond_3

    .line 711
    invoke-interface {v9}, Landroid/database/Cursor;->close()V

    .line 712
    const/4 v0, 0x0

    goto :goto_2

    .end local v10    # "e":Ljava/lang/Throwable;
    :cond_3
    move-object v0, v9

    .line 715
    goto :goto_2
.end method

.method public static isChinese()Z
    .locals 2

    .prologue
    .line 616
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->CHINESE:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isFinnish()Z
    .locals 2

    .prologue
    .line 620
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    const-string v1, "fi"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isGerman()Z
    .locals 2

    .prologue
    .line 624
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->GERMAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isJapanese()Z
    .locals 2

    .prologue
    .line 628
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->JAPAN:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isKitKatOrLater()Z
    .locals 2

    .prologue
    .line 645
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isKorean()Z
    .locals 2

    .prologue
    .line 612
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v0

    sget-object v1, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v1}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isToday(Landroid/text/format/Time;)Z
    .locals 3
    .param p0, "EventStartTime"    # Landroid/text/format/Time;

    .prologue
    .line 568
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 569
    .local v0, "now":Landroid/text/format/Time;
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 571
    iget v1, v0, Landroid/text/format/Time;->year:I

    iget v2, p0, Landroid/text/format/Time;->year:I

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/text/format/Time;->month:I

    iget v2, p0, Landroid/text/format/Time;->month:I

    if-ne v1, v2, :cond_0

    iget v1, v0, Landroid/text/format/Time;->monthDay:I

    iget v2, p0, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v2, :cond_0

    .line 573
    const/4 v1, 0x1

    .line 575
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static isValidRange(Landroid/text/format/Time;)Z
    .locals 6
    .param p0, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v2, 0x1

    .line 216
    invoke-virtual {p0, v2}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 218
    .local v0, "millis":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    const v4, 0x24dc87

    if-lt v3, v4, :cond_0

    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    const v4, 0x259d23

    if-le v3, v4, :cond_1

    .line 221
    :cond_0
    const/4 v2, 0x0

    .line 222
    :cond_1
    return v2
.end method

.method public static launchEditEvent(Landroid/content/Context;Landroid/text/format/Time;)V
    .locals 18
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 146
    new-instance v12, Landroid/text/format/Time;

    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v14

    invoke-direct {v12, v14}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 147
    .local v12, "tempTime":Landroid/text/format/Time;
    move-object/from16 v0, p1

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 148
    const/4 v14, 0x1

    move-object/from16 v0, p1

    invoke-virtual {v0, v14}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    .line 149
    .local v8, "millis":J
    move-object/from16 v0, p1

    iget-wide v14, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v14, v15}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v7

    .line 150
    .local v7, "selectedDay":I
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p1

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v16, v0

    invoke-static/range {v14 .. v17}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v13

    .line 151
    .local v13, "todayJulian":I
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 152
    .local v2, "EventStartTime":Landroid/text/format/Time;
    if-ne v7, v13, :cond_0

    .line 153
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 154
    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v14

    const/16 v16, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v16 .. v16}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getOffsetMilliesFromSystemTZ(Ljava/lang/String;)J

    move-result-wide v16

    sub-long v14, v14, v16

    invoke-virtual {v2, v14, v15}, Landroid/text/format/Time;->set(J)V

    .line 155
    iget v14, v2, Landroid/text/format/Time;->hour:I

    add-int/lit8 v14, v14, 0x1

    iput v14, v2, Landroid/text/format/Time;->hour:I

    .line 160
    :goto_0
    const/4 v14, 0x0

    iput v14, v2, Landroid/text/format/Time;->minute:I

    .line 161
    const/4 v14, 0x0

    iput v14, v2, Landroid/text/format/Time;->second:I

    .line 162
    const/4 v14, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v14

    iput-object v14, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 164
    const/4 v14, 0x1

    invoke-virtual {v2, v14}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 165
    .local v10, "startMillis":J
    const-wide/32 v14, 0x36ee80

    add-long v4, v10, v14

    .line 167
    .local v4, "endMillis":J
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 168
    .local v6, "launchIntent":Landroid/content/Intent;
    const-string v14, "android.intent.action.VIEW"

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 169
    const-string v14, "com.android.calendar"

    const-string v15, "com.android.calendar.EditEventActivity"

    invoke-virtual {v6, v14, v15}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 171
    const v14, 0x14208000

    invoke-virtual {v6, v14}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 174
    const-string v14, "beginTime"

    invoke-virtual {v6, v14, v10, v11}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 175
    const-string v14, "endTime"

    invoke-virtual {v6, v14, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 177
    :try_start_0
    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 181
    :goto_1
    return-void

    .line 157
    .end local v4    # "endMillis":J
    .end local v6    # "launchIntent":Landroid/content/Intent;
    .end local v10    # "startMillis":J
    :cond_0
    invoke-virtual {v2, v12}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 158
    const/16 v14, 0x8

    iput v14, v2, Landroid/text/format/Time;->hour:I

    goto :goto_0

    .line 178
    .restart local v4    # "endMillis":J
    .restart local v6    # "launchIntent":Landroid/content/Intent;
    .restart local v10    # "startMillis":J
    :catch_0
    move-exception v3

    .line 179
    .local v3, "e":Landroid/content/ActivityNotFoundException;
    const v14, 0x7f0a001c

    const/4 v15, 0x0

    move-object/from16 v0, p0

    invoke-static {v0, v14, v15}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v14

    invoke-virtual {v14}, Landroid/widget/Toast;->show()V

    goto :goto_1
.end method

.method public static launchHandwritingMode(Landroid/content/Context;J)V
    .locals 5
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "millis"    # J

    .prologue
    .line 185
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 187
    .local v1, "launchIntent":Landroid/content/Intent;
    const-string v2, "com.android.calendar"

    const-string v3, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 189
    const/high16 v2, 0x14200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 193
    const-string v2, "beginTime"

    invoke-virtual {v1, v2, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 194
    const-string v2, "handwriting_mode"

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 195
    const-string v2, "VIEW"

    const-string v3, "MONTH"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 197
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 201
    :goto_0
    return-void

    .line 198
    :catch_0
    move-exception v0

    .line 199
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v2, 0x7f0a001c

    const/4 v3, 0x0

    invoke-static {p0, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v2

    invoke-virtual {v2}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method

.method public static recursiveRecycle(Landroid/view/View;)V
    .locals 5
    .param p0, "root"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    .line 581
    if-nez p0, :cond_0

    .line 603
    :goto_0
    return-void

    .line 584
    :cond_0
    invoke-virtual {p0, v4}, Landroid/view/View;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 586
    instance-of v3, p0, Landroid/view/ViewGroup;

    if-eqz v3, :cond_2

    move-object v1, p0

    .line 587
    check-cast v1, Landroid/view/ViewGroup;

    .line 588
    .local v1, "group":Landroid/view/ViewGroup;
    invoke-virtual {v1}, Landroid/view/ViewGroup;->getChildCount()I

    move-result v0

    .line 589
    .local v0, "count":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_1

    .line 590
    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->recursiveRecycle(Landroid/view/View;)V

    .line 589
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 593
    :cond_1
    instance-of v3, p0, Landroid/widget/AdapterView;

    if-nez v3, :cond_2

    .line 594
    invoke-virtual {v1}, Landroid/view/ViewGroup;->removeAllViews()V

    .line 598
    .end local v0    # "count":I
    .end local v1    # "group":Landroid/view/ViewGroup;
    .end local v2    # "i":I
    :cond_2
    instance-of v3, p0, Landroid/widget/ImageView;

    if-eqz v3, :cond_3

    .line 599
    check-cast p0, Landroid/widget/ImageView;

    .end local p0    # "root":Landroid/view/View;
    invoke-virtual {p0, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 602
    :cond_3
    const/4 p0, 0x0

    .line 603
    .restart local p0    # "root":Landroid/view/View;
    goto :goto_0
.end method

.method public static removeSpace(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p0, "str"    # Ljava/lang/String;

    .prologue
    .line 607
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->UNNEEDED_SPACES:Ljava/util/regex/Pattern;

    invoke-virtual {v1, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v0

    .line 608
    .local v0, "m":Ljava/util/regex/Matcher;
    invoke-virtual {v0}, Ljava/util/regex/Matcher;->matches()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/util/regex/Matcher;->replaceAll(Ljava/lang/String;)Ljava/lang/String;

    move-result-object p0

    .end local p0    # "str":Ljava/lang/String;
    :cond_0
    return-object p0
.end method

.method public static setAllDayEvents(Ljava/util/ArrayList;IILandroid/content/Context;)I
    .locals 12
    .param p1, "firstday"    # I
    .param p2, "lastday"    # I
    .param p3, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;II",
            "Landroid/content/Context;",
            ")I"
        }
    .end annotation

    .prologue
    .line 339
    .local p0, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    const/4 v2, 0x0

    .line 340
    .local v2, "dirty":Z
    new-instance v5, Ljava/util/ArrayList;

    invoke-direct {v5}, Ljava/util/ArrayList;-><init>()V

    .line 341
    .local v5, "longEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v7, Ljava/util/ArrayList;

    invoke-direct {v7}, Ljava/util/ArrayList;-><init>()V

    .line 342
    .local v7, "sameTimeEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 343
    .local v1, "annEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 344
    .local v0, "alldayEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 346
    .local v6, "norEvents":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    if-nez p0, :cond_0

    .line 347
    const/4 v8, 0x0

    .line 396
    :goto_0
    return v8

    .line 349
    :cond_0
    invoke-virtual {p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :cond_1
    :goto_1
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v8

    if-eqz v8, :cond_7

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 351
    .local v3, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    const/4 v8, -0x1

    if-eq p1, v8, :cond_3

    .line 352
    iget v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    if-gt v8, p2, :cond_2

    iget v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    if-ge v8, p1, :cond_3

    .line 353
    :cond_2
    const/4 v2, 0x1

    .line 354
    goto :goto_1

    .line 363
    :cond_3
    iget v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    iget v9, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    if-ge v8, v9, :cond_4

    iget-boolean v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-nez v8, :cond_4

    .line 364
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    .line 365
    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 366
    if-nez v2, :cond_1

    .line 367
    const/4 v2, 0x1

    goto :goto_1

    .line 368
    :cond_4
    iget-wide v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    iget-wide v10, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    cmp-long v8, v8, v10

    if-nez v8, :cond_5

    iget-boolean v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-nez v8, :cond_5

    .line 369
    const/4 v8, 0x1

    iput-boolean v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    .line 370
    invoke-virtual {v7, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 371
    if-nez v2, :cond_1

    .line 372
    const/4 v2, 0x1

    goto :goto_1

    .line 373
    :cond_5
    iget-boolean v8, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eqz v8, :cond_6

    .line 374
    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 375
    if-nez v2, :cond_1

    .line 376
    const/4 v2, 0x1

    goto :goto_1

    .line 378
    :cond_6
    invoke-virtual {v6, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 379
    if-nez v2, :cond_1

    .line 380
    const/4 v2, 0x1

    goto :goto_1

    .line 384
    .end local v3    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :cond_7
    if-eqz v2, :cond_8

    .line 385
    invoke-virtual {p0}, Ljava/util/ArrayList;->clear()V

    .line 387
    invoke-virtual {p0, v0}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 388
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 389
    invoke-virtual {p0, v5}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 390
    invoke-virtual {p0, v7}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 391
    invoke-virtual {p0, v6}, Ljava/util/ArrayList;->addAll(Ljava/util/Collection;)Z

    .line 396
    :cond_8
    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v8

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v8, v9

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v9

    add-int/2addr v8, v9

    goto/16 :goto_0
.end method

.method public static setJulianDay(Landroid/text/format/Time;I)J
    .locals 9
    .param p0, "time"    # Landroid/text/format/Time;
    .param p1, "julianDay"    # I

    .prologue
    const/4 v8, 0x0

    .line 235
    const v4, 0x253d8c    # 3.419992E-39f

    sub-int v4, p1, v4

    int-to-long v4, v4

    const-wide/32 v6, 0x5265c00

    mul-long v2, v4, v6

    .line 236
    .local v2, "millis":J
    invoke-virtual {p0, v2, v3}, Landroid/text/format/Time;->set(J)V

    .line 238
    iget-wide v4, p0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v0

    .line 239
    .local v0, "approximateDay":I
    sub-int v1, p1, v0

    .line 240
    .local v1, "diff":I
    iget v4, p0, Landroid/text/format/Time;->monthDay:I

    add-int/2addr v4, v1

    iput v4, p0, Landroid/text/format/Time;->monthDay:I

    .line 242
    iput v8, p0, Landroid/text/format/Time;->hour:I

    .line 243
    iput v8, p0, Landroid/text/format/Time;->minute:I

    .line 244
    iput v8, p0, Landroid/text/format/Time;->second:I

    .line 245
    const/4 v4, 0x1

    invoke-virtual {p0, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 246
    return-wide v2
.end method

.method public static setTimeToStartOfDay(Landroid/text/format/Time;)V
    .locals 1
    .param p0, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v0, 0x0

    .line 210
    iput v0, p0, Landroid/text/format/Time;->second:I

    .line 211
    iput v0, p0, Landroid/text/format/Time;->minute:I

    .line 212
    iput v0, p0, Landroid/text/format/Time;->hour:I

    .line 213
    return-void
.end method

.method public static setTimeZone(Landroid/content/Context;Ljava/lang/String;)V
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "timeZone"    # Ljava/lang/String;

    .prologue
    .line 513
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->mTZUtils:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;

    invoke-virtual {v0, p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils$TimeZoneUtils;->setTimeZone(Landroid/content/Context;Ljava/lang/String;)V

    .line 514
    return-void
.end method

.method public static startActivity(Landroid/content/Context;Ljava/lang/String;J)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "time"    # J

    .prologue
    .line 128
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 129
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p0, p1}, Landroid/content/Intent;->setClassName(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    .line 130
    const-string v1, "beginTime"

    invoke-virtual {v0, v1, p2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 131
    invoke-virtual {p0, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 132
    return-void
.end method

.method public static toCapitalize(Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "s"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 632
    invoke-static {p0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 637
    .end local p0    # "s":Ljava/lang/String;
    :goto_0
    return-object p0

    .line 634
    .restart local p0    # "s":Ljava/lang/String;
    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v0

    if-ne v0, v2, :cond_1

    .line 635
    invoke-virtual {p0}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object p0

    goto :goto_0

    .line 637
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const/4 v1, 0x0

    invoke-virtual {p0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {p0, v2}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p0

    goto :goto_0
.end method
