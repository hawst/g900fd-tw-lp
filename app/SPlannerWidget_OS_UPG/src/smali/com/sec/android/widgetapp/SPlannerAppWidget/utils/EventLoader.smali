.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
.super Ljava/lang/Object;
.source "EventLoader.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$1;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventsRequest;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventDaysRequest;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$ShutdownRequest;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadRequest;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/LinkedBlockingQueue",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadRequest;",
            ">;"
        }
    .end annotation
.end field

.field private mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

.field private mResolver:Landroid/content/ContentResolver;

.field private mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 212
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mHandler:Landroid/os/Handler;

    .line 38
    new-instance v0, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 213
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mContext:Landroid/content/Context;

    .line 214
    new-instance v0, Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0}, Ljava/util/concurrent/LinkedBlockingQueue;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    .line 215
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mResolver:Landroid/content/ContentResolver;

    .line 216
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Landroid/content/ContentResolver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mResolver:Landroid/content/ContentResolver;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Ljava/util/concurrent/atomic/AtomicInteger;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 219
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v0}, Ljava/util/concurrent/LinkedBlockingQueue;->clear()V

    .line 222
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->stopBackgroundThread()V

    .line 223
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    .line 224
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mContext:Landroid/content/Context;

    .line 225
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mResolver:Landroid/content/ContentResolver;

    .line 226
    return-void
.end method

.method loadEventDaysInBackground(II[ZLjava/lang/Runnable;)V
    .locals 4
    .param p1, "startDay"    # I
    .param p2, "numDays"    # I
    .param p3, "eventDays"    # [Z
    .param p4, "uiCallback"    # Ljava/lang/Runnable;

    .prologue
    .line 289
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventDaysRequest;

    invoke-direct {v1, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventDaysRequest;-><init>(II[ZLjava/lang/Runnable;)V

    .line 292
    .local v1, "request":Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventDaysRequest;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, v1}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 299
    :goto_0
    return-void

    .line 293
    :catch_0
    move-exception v0

    .line 297
    .local v0, "ex":Ljava/lang/InterruptedException;
    const-string v2, "Cal"

    const-string v3, "loadEventDaysInBackground() interrupted!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public loadEventsInBackground(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V
    .locals 8
    .param p1, "numDays"    # I
    .param p3, "startDay"    # I
    .param p4, "successCallback"    # Ljava/lang/Runnable;
    .param p5, "cancelCallback"    # Ljava/lang/Runnable;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;I",
            "Ljava/lang/Runnable;",
            "Ljava/lang/Runnable;",
            ")V"
        }
    .end annotation

    .prologue
    .line 260
    .local p2, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mSequenceNumber:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v2}, Ljava/util/concurrent/atomic/AtomicInteger;->incrementAndGet()I

    move-result v1

    .line 263
    .local v1, "id":I
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventsRequest;

    move v2, p3

    move v3, p1

    move-object v4, p2

    move-object v5, p4

    move-object v6, p5

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventsRequest;-><init>(IIILjava/util/ArrayList;Ljava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 267
    .local v0, "request":Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoadEventsRequest;
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-virtual {v2, v0}, Ljava/util/concurrent/LinkedBlockingQueue;->put(Ljava/lang/Object;)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    :goto_0
    return-void

    .line 268
    :catch_0
    move-exception v7

    .line 272
    .local v7, "ex":Ljava/lang/InterruptedException;
    const-string v2, "Cal"

    const-string v3, "loadEventsInBackground() interrupted!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public startBackgroundThread()V
    .locals 2

    .prologue
    .line 232
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderQueue:Ljava/util/concurrent/LinkedBlockingQueue;

    invoke-direct {v0, v1, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;-><init>(Ljava/util/concurrent/LinkedBlockingQueue;Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    .line 233
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;->start()V

    .line 234
    return-void
.end method

.method public stopBackgroundThread()V
    .locals 1

    .prologue
    .line 240
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    if-eqz v0, :cond_0

    .line 241
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->mLoaderThread:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader$LoaderThread;->shutdown()V

    .line 243
    :cond_0
    return-void
.end method
