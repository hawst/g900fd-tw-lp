.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;
.super Ljava/lang/Object;
.source "HolidayManager.java"


# static fields
.field public static final JP_COLOR_B:I = 0x16

.field public static final JP_COLOR_G:I = 0x3f

.field public static final JP_COLOR_R:I = 0xd4

.field public static final JP_HOLIDAY_KIND_DAY:Ljava/lang/String; = "day"

.field public static final JP_HOLIDAY_KIND_MONTH:Ljava/lang/String; = "month"

.field public static final JP_HOLIDAY_KIND_WEEK:Ljava/lang/String; = "week"

.field public static final JP_SYNC_ID:Ljava/lang/String; = "1"

.field private static final TAG:Ljava/lang/String; = "HolidayManager"


# instance fields
.field private final HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

.field private final PEOPLE_DAY:I

.field private final PEOPLE_DAY_YEAR:[I

.field private jp_calendar_account_name:Ljava/lang/String;

.field private jp_calendar_name:Ljava/lang/String;

.field private jp_holiday_10_sports:Ljava/lang/String;

.field private jp_holiday_11_culture:Ljava/lang/String;

.field private jp_holiday_11_labor:Ljava/lang/String;

.field private jp_holiday_12_emperor:Ljava/lang/String;

.field private jp_holiday_13_mountain_day:Ljava/lang/String;

.field private jp_holiday_1_age:Ljava/lang/String;

.field private jp_holiday_1_newyear:Ljava/lang/String;

.field private jp_holiday_2_foundation:Ljava/lang/String;

.field private jp_holiday_3_spring:Ljava/lang/String;

.field private jp_holiday_4_showa:Ljava/lang/String;

.field private jp_holiday_5_children:Ljava/lang/String;

.field private jp_holiday_5_constitution:Ljava/lang/String;

.field private jp_holiday_5_green:Ljava/lang/String;

.field private jp_holiday_7_marine:Ljava/lang/String;

.field private jp_holiday_9_autumn:Ljava/lang/String;

.field private jp_holiday_9_elders:Ljava/lang/String;

.field private mCalendarID:Ljava/lang/String;

.field private final mContext:Landroid/content/Context;

.field private final mResolver:Landroid/content/ContentResolver;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v7, 0x3

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 266
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 122
    const/16 v0, 0xc

    new-array v0, v0, [[Ljava/lang/String;

    new-array v1, v6, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    const-string v2, ""

    aput-object v2, v1, v5

    aput-object v1, v0, v4

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v5

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v6

    new-array v1, v5, [Ljava/lang/String;

    const-string v2, ""

    aput-object v2, v1, v4

    aput-object v1, v0, v7

    const/4 v1, 0x4

    new-array v2, v7, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    const-string v3, ""

    aput-object v3, v2, v6

    aput-object v2, v0, v1

    const/4 v1, 0x5

    new-array v2, v4, [Ljava/lang/String;

    aput-object v2, v0, v1

    const/4 v1, 0x6

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/4 v1, 0x7

    new-array v2, v4, [Ljava/lang/String;

    aput-object v2, v0, v1

    const/16 v1, 0x8

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0x9

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    const/16 v1, 0xa

    new-array v2, v6, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    const-string v3, ""

    aput-object v3, v2, v5

    aput-object v2, v0, v1

    const/16 v1, 0xb

    new-array v2, v5, [Ljava/lang/String;

    const-string v3, ""

    aput-object v3, v2, v4

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    .line 168
    new-array v0, v7, [I

    fill-array-data v0, :array_0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->PEOPLE_DAY_YEAR:[I

    .line 174
    const/16 v0, 0x16

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->PEOPLE_DAY:I

    .line 268
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mContext:Landroid/content/Context;

    .line 270
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mResolver:Landroid/content/ContentResolver;

    .line 272
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mContext:Landroid/content/Context;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->setHolidayName(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 273
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getCalendarInfo()Z

    .line 275
    :cond_0
    return-void

    .line 168
    :array_0
    .array-data 4
        0x7d9
        0x7df
        0x7ea
    .end array-data
.end method

.method private getAutumnDay(I)I
    .locals 1
    .param p1, "year"    # I

    .prologue
    .line 611
    rem-int/lit8 v0, p1, 0x4

    if-nez v0, :cond_0

    rem-int/lit8 v0, p1, 0x64

    if-nez v0, :cond_1

    :cond_0
    rem-int/lit16 v0, p1, 0x190

    if-nez v0, :cond_2

    .line 613
    :cond_1
    const/16 v0, 0x16

    .line 617
    :goto_0
    return v0

    :cond_2
    const/16 v0, 0x17

    goto :goto_0
.end method

.method private getCalendarInfo()Z
    .locals 10

    .prologue
    const/4 v4, 0x0

    const/4 v5, 0x0

    .line 400
    const/4 v6, 0x0

    .line 404
    .local v6, "bResult":Z
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mResolver:Landroid/content/ContentResolver;

    sget-object v1, Landroid/provider/CalendarContract$Calendars;->CONTENT_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const-string v3, "_id"

    aput-object v3, v2, v5

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "account_name=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_calendar_account_name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "account_type"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'LOCAL\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "_sync_id"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "1"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "name"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "=\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_calendar_name:Ljava/lang/String;

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "\'"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, " AND "

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "calendar_color"

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v5, "="

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const/16 v5, 0xd4

    const/16 v8, 0x3f

    const/16 v9, 0x16

    invoke-static {v5, v8, v9}, Landroid/graphics/Color;->rgb(III)I

    move-result v5

    invoke-virtual {v3, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    move-object v5, v4

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v7

    .line 463
    .local v7, "calendarCursor":Landroid/database/Cursor;
    if-eqz v7, :cond_2

    .line 467
    :try_start_0
    invoke-interface {v7}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-nez v0, :cond_0

    .line 469
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mCalendarID:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 488
    :goto_0
    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 497
    :goto_1
    return v6

    .line 473
    :cond_0
    :try_start_1
    invoke-interface {v7}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 475
    const/4 v0, 0x0

    invoke-interface {v7, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mCalendarID:Ljava/lang/String;

    .line 477
    const/4 v6, 0x1

    goto :goto_0

    .line 481
    :cond_1
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mCalendarID:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 488
    :catchall_0
    move-exception v0

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    throw v0

    .line 494
    :cond_2
    const-string v0, ""

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->mCalendarID:Ljava/lang/String;

    goto :goto_1
.end method

.method private getSiteiWeekDay(IIII)I
    .locals 6
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "num"    # I
    .param p4, "yobi"    # I

    .prologue
    const/4 v4, 0x1

    .line 503
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 505
    .local v3, "time":Landroid/text/format/Time;
    invoke-virtual {v3, v4, p2, p1}, Landroid/text/format/Time;->set(III)V

    .line 507
    invoke-virtual {v3, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 509
    iget v1, v3, Landroid/text/format/Time;->weekDay:I

    .line 511
    .local v1, "firstDayYobi":I
    const/4 v0, 0x0

    .line 513
    .local v0, "addWeekDays":I
    if-ge p4, v1, :cond_0

    .line 515
    const/4 v0, 0x7

    .line 519
    :cond_0
    add-int/lit8 v4, p3, -0x1

    mul-int/lit8 v4, v4, 0x7

    add-int v5, p4, v0

    sub-int/2addr v5, v1

    add-int/2addr v4, v5

    add-int/lit8 v2, v4, 0x1

    .line 521
    .local v2, "purPoseDay":I
    return v2
.end method

.method private getSpringDay(I)I
    .locals 6
    .param p1, "year"    # I

    .prologue
    const/4 v5, 0x3

    .line 527
    invoke-static {p1}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v3

    .line 529
    .local v3, "strYear":Ljava/lang/String;
    const/4 v4, 0x2

    invoke-virtual {v3, v4, v5}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 531
    .local v1, "num1":I
    const/4 v4, 0x4

    invoke-virtual {v3, v5, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    .line 533
    .local v2, "num2":I
    const/4 v0, 0x0

    .line 535
    .local v0, "day":I
    rem-int/lit8 v4, v1, 0x2

    if-nez v4, :cond_0

    .line 537
    packed-switch v2, :pswitch_data_0

    .line 605
    :goto_0
    return v0

    .line 551
    :pswitch_0
    const/16 v0, 0x14

    .line 553
    goto :goto_0

    .line 563
    :pswitch_1
    const/16 v0, 0x15

    goto :goto_0

    .line 571
    :cond_0
    packed-switch v2, :pswitch_data_1

    goto :goto_0

    .line 585
    :pswitch_2
    const/16 v0, 0x15

    .line 587
    goto :goto_0

    .line 597
    :pswitch_3
    const/16 v0, 0x14

    goto :goto_0

    .line 537
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_0
    .end packed-switch

    .line 571
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_2
        :pswitch_2
    .end packed-switch
.end method

.method private setHolidayName(Landroid/content/Context;)Z
    .locals 5
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 178
    const/4 v0, 0x0

    .line 180
    .local v0, "bSet":Z
    if-eqz p1, :cond_0

    .line 184
    const v2, 0x7f0a002a

    :try_start_0
    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_calendar_name:Ljava/lang/String;

    .line 186
    const v2, 0x7f0a002b

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_calendar_account_name:Ljava/lang/String;

    .line 188
    const v2, 0x7f0a0032

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_1_newyear:Ljava/lang/String;

    .line 190
    const v2, 0x7f0a0031

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_1_age:Ljava/lang/String;

    .line 192
    const v2, 0x7f0a0033

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_2_foundation:Ljava/lang/String;

    .line 194
    const v2, 0x7f0a0034

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_3_spring:Ljava/lang/String;

    .line 196
    const v2, 0x7f0a0035

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_4_showa:Ljava/lang/String;

    .line 198
    const v2, 0x7f0a0037

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_constitution:Ljava/lang/String;

    .line 200
    const v2, 0x7f0a0038

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_green:Ljava/lang/String;

    .line 202
    const v2, 0x7f0a0036

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_children:Ljava/lang/String;

    .line 204
    const v2, 0x7f0a0039

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_7_marine:Ljava/lang/String;

    .line 206
    const v2, 0x7f0a003b

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_9_elders:Ljava/lang/String;

    .line 208
    const v2, 0x7f0a003a

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_9_autumn:Ljava/lang/String;

    .line 210
    const v2, 0x7f0a002c

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_10_sports:Ljava/lang/String;

    .line 212
    const v2, 0x7f0a002d

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_11_culture:Ljava/lang/String;

    .line 214
    const v2, 0x7f0a002e

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_11_labor:Ljava/lang/String;

    .line 216
    const v2, 0x7f0a002f

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_12_emperor:Ljava/lang/String;

    .line 217
    const v2, 0x7f0a0030

    invoke-virtual {p1, v2}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_13_mountain_day:Ljava/lang/String;

    .line 218
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_1_newyear:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 220
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x0

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_1_age:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 222
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x1

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_2_foundation:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 224
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x2

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_3_spring:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 226
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x3

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_4_showa:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 228
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_constitution:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 230
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_green:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 232
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x4

    aget-object v2, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_5_children:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 234
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x6

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_7_marine:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 236
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_9_elders:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 238
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0x8

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_9_autumn:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 240
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0x9

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_10_sports:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 242
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_11_culture:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 244
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0xa

    aget-object v2, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_11_labor:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 246
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/16 v3, 0xb

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_12_emperor:Ljava/lang/String;

    aput-object v4, v2, v3

    .line 247
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->HOLIDAYS_ARRAY_NAME:[[Ljava/lang/String;

    const/4 v3, 0x7

    aget-object v2, v2, v3

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->jp_holiday_13_mountain_day:Ljava/lang/String;

    aput-object v4, v2, v3
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 248
    const/4 v0, 0x1

    .line 260
    :cond_0
    :goto_0
    return v0

    .line 250
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/lang/Exception;
    const-string v2, "HolidayManager"

    invoke-virtual {v1}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 254
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public checkAlternateJapaneseHoliday(Landroid/text/format/Time;)Z
    .locals 8
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/16 v7, 0xa

    const/4 v6, 0x4

    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 351
    const/4 v0, 0x0

    .line 353
    .local v0, "bResult":Z
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_0

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v4, :cond_0

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_0

    .line 354
    const/4 v0, 0x1

    .line 355
    :cond_0
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v3, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_1

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_1

    .line 356
    const/4 v0, 0x1

    .line 357
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v5, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x1e

    if-ne v1, v2, :cond_2

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_2

    .line 358
    const/4 v0, 0x1

    .line 366
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_4

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_4

    .line 367
    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-eq v1, v3, :cond_3

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-eq v1, v4, :cond_3

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v5, :cond_4

    .line 369
    :cond_3
    const/4 v0, 0x1

    .line 372
    :cond_4
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v7, :cond_5

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v6, :cond_5

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_5

    .line 373
    const/4 v0, 0x1

    .line 374
    :cond_5
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v7, :cond_6

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x18

    if-ne v1, v2, :cond_6

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_6

    .line 375
    const/4 v0, 0x1

    .line 376
    :cond_6
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_7

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x18

    if-ne v1, v2, :cond_7

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_7

    .line 377
    const/4 v0, 0x1

    .line 380
    :cond_7
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v4, :cond_8

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSpringDay(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v2, :cond_8

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_8

    .line 382
    const/4 v0, 0x1

    .line 386
    :cond_8
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x8

    if-ne v1, v2, :cond_9

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getAutumnDay(I)I

    move-result v1

    add-int/lit8 v1, v1, 0x1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v2, :cond_9

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_9

    .line 388
    const/4 v0, 0x1

    .line 392
    :cond_9
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_a

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xc

    if-ne v1, v2, :cond_a

    iget v1, p1, Landroid/text/format/Time;->weekDay:I

    if-ne v1, v3, :cond_a

    iget v1, p1, Landroid/text/format/Time;->year:I

    const/16 v2, 0x7e0

    if-lt v1, v2, :cond_a

    .line 393
    const/4 v0, 0x1

    .line 395
    :cond_a
    return v0
.end method

.method public checkJapaneseHoliday(Landroid/text/format/Time;)Z
    .locals 8
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v3, 0x4

    const/4 v7, 0x2

    const/16 v6, 0x8

    const/4 v5, 0x3

    const/4 v4, 0x1

    .line 278
    const/4 v0, 0x0

    .line 280
    .local v0, "bResult":Z
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_0

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v4, :cond_0

    .line 281
    const/4 v0, 0x1

    .line 282
    :cond_0
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v4, :cond_1

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_1

    .line 283
    const/4 v0, 0x1

    .line 284
    :cond_1
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v5, :cond_2

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x1d

    if-ne v1, v2, :cond_2

    .line 285
    const/4 v0, 0x1

    .line 286
    :cond_2
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v3, :cond_3

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v5, :cond_3

    .line 287
    const/4 v0, 0x1

    .line 288
    :cond_3
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v3, :cond_4

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v3, :cond_4

    .line 289
    const/4 v0, 0x1

    .line 290
    :cond_4
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v3, :cond_5

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/4 v2, 0x5

    if-ne v1, v2, :cond_5

    .line 291
    const/4 v0, 0x1

    .line 293
    :cond_5
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_6

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_6

    iget v1, p1, Landroid/text/format/Time;->year:I

    const/16 v2, 0x7df

    if-le v1, v2, :cond_6

    .line 294
    const/4 v0, 0x1

    .line 295
    :cond_6
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_7

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v5, :cond_7

    .line 296
    const/4 v0, 0x1

    .line 297
    :cond_7
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xa

    if-ne v1, v2, :cond_8

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x17

    if-ne v1, v2, :cond_8

    .line 298
    const/4 v0, 0x1

    .line 299
    :cond_8
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0xb

    if-ne v1, v2, :cond_9

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x17

    if-ne v1, v2, :cond_9

    .line 300
    const/4 v0, 0x1

    .line 303
    :cond_9
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v7, :cond_a

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSpringDay(I)I

    move-result v1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v2, :cond_a

    .line 304
    const/4 v0, 0x1

    .line 307
    :cond_a
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_b

    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getAutumnDay(I)I

    move-result v1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    if-ne v1, v2, :cond_b

    .line 308
    const/4 v0, 0x1

    .line 311
    :cond_b
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-nez v1, :cond_c

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    iget v2, p1, Landroid/text/format/Time;->year:I

    const/4 v3, 0x0

    invoke-direct {p0, v2, v3, v7, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSiteiWeekDay(IIII)I

    move-result v2

    if-ne v1, v2, :cond_c

    .line 312
    const/4 v0, 0x1

    .line 315
    :cond_c
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/16 v2, 0x9

    if-ne v1, v2, :cond_d

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    iget v2, p1, Landroid/text/format/Time;->year:I

    const/16 v3, 0x9

    invoke-direct {p0, v2, v3, v7, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSiteiWeekDay(IIII)I

    move-result v2

    if-ne v1, v2, :cond_d

    .line 316
    const/4 v0, 0x1

    .line 319
    :cond_d
    iget v1, p1, Landroid/text/format/Time;->month:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_e

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    iget v2, p1, Landroid/text/format/Time;->year:I

    const/4 v3, 0x6

    invoke-direct {p0, v2, v3, v5, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSiteiWeekDay(IIII)I

    move-result v2

    if-ne v1, v2, :cond_e

    .line 320
    const/4 v0, 0x1

    .line 323
    :cond_e
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_f

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2, v6, v5, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSiteiWeekDay(IIII)I

    move-result v2

    if-ne v1, v2, :cond_f

    .line 324
    const/4 v0, 0x1

    .line 327
    :cond_f
    iget v1, p1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->PEOPLE_DAY_YEAR:[I

    const/4 v3, 0x0

    aget v2, v2, v3

    if-eq v1, v2, :cond_10

    iget v1, p1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->PEOPLE_DAY_YEAR:[I

    aget v2, v2, v4

    if-eq v1, v2, :cond_10

    iget v1, p1, Landroid/text/format/Time;->year:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->PEOPLE_DAY_YEAR:[I

    aget v2, v2, v7

    if-ne v1, v2, :cond_11

    :cond_10
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_11

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    const/16 v2, 0x16

    if-ne v1, v2, :cond_11

    .line 329
    const/4 v0, 0x1

    .line 331
    :cond_11
    iget v1, p1, Landroid/text/format/Time;->month:I

    if-ne v1, v6, :cond_12

    .line 333
    iget v1, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getAutumnDay(I)I

    move-result v1

    iget v2, p1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v2, v2, 0x1

    if-ne v1, v2, :cond_12

    iget v1, p1, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p1, Landroid/text/format/Time;->year:I

    invoke-direct {p0, v2, v6, v5, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->getSiteiWeekDay(IIII)I

    move-result v2

    if-ne v1, v2, :cond_12

    .line 337
    const/4 v0, 0x1

    .line 341
    :cond_12
    return v0
.end method
