.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final alarm:I = 0x7f0c003b

.field public static final appwidget_loading:I = 0x7f0c000a

.field public static final bg:I = 0x7f0c0001

.field public static final body_container:I = 0x7f0c005a

.field public static final calendar_item:I = 0x7f0c0027

.field public static final calendar_no_items:I = 0x7f0c0043

.field public static final date:I = 0x7f0c0003

.field public static final date_container:I = 0x7f0c0000

.field public static final dateoftoday:I = 0x7f0c0059

.field public static final day0:I = 0x7f0c001f

.field public static final day1:I = 0x7f0c0020

.field public static final day2:I = 0x7f0c0021

.field public static final day3:I = 0x7f0c0022

.field public static final day4:I = 0x7f0c0023

.field public static final day5:I = 0x7f0c0024

.field public static final day6:I = 0x7f0c0025

.field public static final dayView:I = 0x7f0c002f

.field public static final dayView_2x4:I = 0x7f0c0028

.field public static final dayView_2x4_sep:I = 0x7f0c0029

.field public static final dayView_4x2:I = 0x7f0c0013

.field public static final dayView_4x2_sep:I = 0x7f0c0014

.field public static final dayView_layout_2x2:I = 0x7f0c0030

.field public static final day_event_row:I = 0x7f0c003e

.field public static final day_event_start_time:I = 0x7f0c0046

.field public static final day_event_sticker:I = 0x7f0c0049

.field public static final day_event_title:I = 0x7f0c0047

.field public static final day_events_list:I = 0x7f0c0032

.field public static final day_events_list_2x4:I = 0x7f0c002c

.field public static final day_events_list_4x2:I = 0x7f0c0018

.field public static final day_events_list_layout_2x4:I = 0x7f0c002a

.field public static final day_events_list_layout_4x2:I = 0x7f0c0016

.field public static final day_item_color:I = 0x7f0c0036

.field public static final day_item_icon:I = 0x7f0c003a

.field public static final day_item_time:I = 0x7f0c0038

.field public static final day_item_title:I = 0x7f0c0039

.field public static final day_names:I = 0x7f0c0053

.field public static final day_of_week:I = 0x7f0c0009

.field public static final day_of_week_slash:I = 0x7f0c0008

.field public static final day_task_priority:I = 0x7f0c0048

.field public static final event_item:I = 0x7f0c0035

.field public static final event_list_container:I = 0x7f0c005d

.field public static final firstDigit:I = 0x7f0c0004

.field public static final go_today:I = 0x7f0c0050

.field public static final handwriting:I = 0x7f0c0052

.field public static final handwriting_button_divider:I = 0x7f0c0051

.field public static final header:I = 0x7f0c0006

.field public static final icon_container:I = 0x7f0c003f

.field public static final loading:I = 0x7f0c000b

.field public static final magazine_no_events_2x2:I = 0x7f0c0033

.field public static final magazine_no_events_2x4:I = 0x7f0c002d

.field public static final magazine_no_events_4x2:I = 0x7f0c0019

.field public static final main:I = 0x7f0c0056

.field public static final main_container:I = 0x7f0c0057

.field public static final month:I = 0x7f0c0007

.field public static final month_day:I = 0x7f0c0042

.field public static final month_day_toggle_btn:I = 0x7f0c0011

.field public static final month_event_list:I = 0x7f0c005e

.field public static final month_header:I = 0x7f0c004e

.field public static final month_header_buttons:I = 0x7f0c004f

.field public static final month_switcher:I = 0x7f0c0055

.field public static final month_title:I = 0x7f0c000e

.field public static final month_view:I = 0x7f0c001c

.field public static final month_week_sep:I = 0x7f0c0026

.field public static final monthview:I = 0x7f0c0054

.field public static final new_event:I = 0x7f0c0012

.field public static final new_event_4x2:I = 0x7f0c0015

.field public static final next_btn:I = 0x7f0c000f

.field public static final next_month:I = 0x7f0c004d

.field public static final no_event:I = 0x7f0c005c

.field public static final no_event_container:I = 0x7f0c005b

.field public static final no_event_thanks_message:I = 0x7f0c0044

.field public static final no_event_thanks_message_2x2:I = 0x7f0c0034

.field public static final no_event_thanks_message_2x4:I = 0x7f0c002e

.field public static final no_event_thanks_message_4x2:I = 0x7f0c001b

.field public static final no_events:I = 0x7f0c001a

.field public static final num_events:I = 0x7f0c0031

.field public static final num_events_2x4:I = 0x7f0c002b

.field public static final num_events_4x2:I = 0x7f0c0017

.field public static final prev_btn:I = 0x7f0c000d

.field public static final prev_month:I = 0x7f0c004b

.field public static final priority:I = 0x7f0c003c

.field public static final refresh_layout:I = 0x7f0c0010

.field public static final reminder:I = 0x7f0c0041

.field public static final repeat:I = 0x7f0c0040

.field public static final secondDigit:I = 0x7f0c0005

.field public static final sticker:I = 0x7f0c003d

.field public static final task_check:I = 0x7f0c0037

.field public static final task_check_box:I = 0x7f0c0045

.field public static final title:I = 0x7f0c0002

.field public static final today_header:I = 0x7f0c0058

.field public static final week_days:I = 0x7f0c001e

.field public static final week_month_layout:I = 0x7f0c001d

.field public static final widget_container:I = 0x7f0c004a

.field public static final widget_main_id:I = 0x7f0c000c

.field public static final year:I = 0x7f0c004c


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 428
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
