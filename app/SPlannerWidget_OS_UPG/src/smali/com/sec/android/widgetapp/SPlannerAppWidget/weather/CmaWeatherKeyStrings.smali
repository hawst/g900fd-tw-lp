.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;
.source "CmaWeatherKeyStrings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;-><init>()V

    return-void
.end method


# virtual methods
.method public initKeyStrings()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "com.sec.android.widgetapp.cmaweatherdaemon.action.CHANGE_SETTING"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    .line 10
    const-string v0, "com.sec.android.widgetapp.ap.cmaweatherdaemon.action.CHANGE_WEATHER_DATA"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    .line 11
    const-string v0, "com.sec.android.widgetapp.cmaweatherdaemon.action.WEATHER_DATE_SYNC"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    .line 12
    const-string v0, "com.sec.android.widgetapp.cmaweatherdaemon.action.CURRENT_LOCATION_WEATHER_DATA"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    .line 13
    const-string v0, "com.sec.android.daemonapp.ap.cmaweather"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

    .line 14
    const-string v0, "com.sec.android.daemonapp.ap.cmaweather.CmaWeatherDaemonService"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

    .line 16
    const-string v0, "content://com.sec.android.daemonapp.cmaweather.provider/current_weatherinfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;->WEATHER_URI:Landroid/net/Uri;

    .line 17
    return-void
.end method
