.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
.super Ljava/lang/Object;
.source "Event.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field public allDay:Z

.field public color:I

.field public endDay:I

.field public endMillis:J

.field public endTime:I

.field public facebookScheudleId:Ljava/lang/CharSequence;

.field public guestsCanModify:Z

.field public hasAlarm:Z

.field public id:J

.field public instanceId:J

.field public isRepeating:Z

.field public location:Ljava/lang/CharSequence;

.field public mColumn:I

.field public mMaxColumns:I

.field public organizer:Ljava/lang/String;

.field public rdate:Ljava/lang/String;

.field public rrule:Ljava/lang/String;

.field public selfAttendeeStatus:I

.field public startDay:I

.field public startMillis:J

.field public startTime:I

.field public stickerGroup:I

.field public stickerPath:Ljava/lang/String;

.field public stickerType:J

.field public timezone:Ljava/lang/String;

.field public title:Ljava/lang/CharSequence;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 21
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 52
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 56
    if-ne p0, p1, :cond_1

    .line 93
    :cond_0
    :goto_0
    return v1

    .line 57
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 59
    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 61
    .local v0, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 62
    :cond_4
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 63
    :cond_5
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 64
    :cond_6
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 65
    :cond_7
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endTime:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endTime:I

    if-eq v3, v4, :cond_8

    move v1, v2

    goto :goto_0

    .line 66
    :cond_8
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->guestsCanModify:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->guestsCanModify:Z

    if-eq v3, v4, :cond_9

    move v1, v2

    goto :goto_0

    .line 67
    :cond_9
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    if-eq v3, v4, :cond_a

    move v1, v2

    goto :goto_0

    .line 68
    :cond_a
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_b

    move v1, v2

    goto :goto_0

    .line 69
    :cond_b
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->instanceId:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->instanceId:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_c

    move v1, v2

    goto :goto_0

    .line 70
    :cond_c
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    if-eq v3, v4, :cond_d

    move v1, v2

    goto :goto_0

    .line 71
    :cond_d
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    if-eq v3, v4, :cond_e

    move v1, v2

    goto :goto_0

    .line 72
    :cond_e
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    if-eq v3, v4, :cond_f

    move v1, v2

    goto :goto_0

    .line 73
    :cond_f
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->selfAttendeeStatus:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->selfAttendeeStatus:I

    if-eq v3, v4, :cond_10

    move v1, v2

    goto :goto_0

    .line 74
    :cond_10
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    if-eq v3, v4, :cond_11

    move v1, v2

    goto/16 :goto_0

    .line 75
    :cond_11
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_12

    move v1, v2

    goto/16 :goto_0

    .line 76
    :cond_12
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startTime:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startTime:I

    if-eq v3, v4, :cond_13

    move v1, v2

    goto/16 :goto_0

    .line 77
    :cond_13
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    if-eq v3, v4, :cond_14

    move v1, v2

    goto/16 :goto_0

    .line 78
    :cond_14
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_15

    move v1, v2

    goto/16 :goto_0

    .line 79
    :cond_15
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    if-eqz v3, :cond_17

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_18

    :cond_16
    move v1, v2

    .line 80
    goto/16 :goto_0

    .line 79
    :cond_17
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    if-nez v3, :cond_16

    .line 81
    :cond_18
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    if-eqz v3, :cond_1a

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1b

    :cond_19
    move v1, v2

    .line 82
    goto/16 :goto_0

    .line 81
    :cond_1a
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    if-nez v3, :cond_19

    .line 83
    :cond_1b
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    if-eqz v3, :cond_1d

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1e

    :cond_1c
    move v1, v2

    .line 84
    goto/16 :goto_0

    .line 83
    :cond_1d
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    if-nez v3, :cond_1c

    .line 85
    :cond_1e
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    if-eqz v3, :cond_20

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_21

    :cond_1f
    move v1, v2

    goto/16 :goto_0

    :cond_20
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    if-nez v3, :cond_1f

    .line 86
    :cond_21
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    if-eqz v3, :cond_23

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_24

    :cond_22
    move v1, v2

    goto/16 :goto_0

    :cond_23
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    if-nez v3, :cond_22

    .line 87
    :cond_24
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    if-eqz v3, :cond_26

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_27

    :cond_25
    move v1, v2

    .line 88
    goto/16 :goto_0

    .line 87
    :cond_26
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    if-nez v3, :cond_25

    .line 89
    :cond_27
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    if-eqz v3, :cond_29

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2a

    :cond_28
    move v1, v2

    .line 90
    goto/16 :goto_0

    .line 89
    :cond_29
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    if-nez v3, :cond_28

    .line 91
    :cond_2a
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    if-eqz v3, :cond_2b

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v3, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto/16 :goto_0

    :cond_2b
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/4 v3, 0x1

    const/16 v8, 0x20

    const/4 v1, 0x0

    .line 98
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v0

    .line 99
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_1
    add-int v0, v4, v2

    .line 100
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eqz v2, :cond_2

    move v2, v3

    :goto_2
    add-int v0, v4, v2

    .line 101
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    add-int v0, v2, v4

    .line 102
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_3
    add-int v0, v4, v2

    .line 103
    mul-int/lit8 v2, v0, 0x1f

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v0, v2, v4

    .line 104
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startTime:I

    add-int v0, v2, v4

    .line 105
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endTime:I

    add-int v0, v2, v4

    .line 106
    mul-int/lit8 v2, v0, 0x1f

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->instanceId:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->instanceId:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v0, v2, v4

    .line 107
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    add-int v0, v2, v4

    .line 108
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    add-int v0, v2, v4

    .line 109
    mul-int/lit8 v2, v0, 0x1f

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v0, v2, v4

    .line 110
    mul-int/lit8 v2, v0, 0x1f

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v0, v2, v4

    .line 111
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    if-eqz v2, :cond_4

    move v2, v3

    :goto_4
    add-int v0, v4, v2

    .line 112
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_5
    add-int v0, v4, v2

    .line 113
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    if-eqz v2, :cond_6

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_6
    add-int v0, v4, v2

    .line 114
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->selfAttendeeStatus:I

    add-int v0, v2, v4

    .line 115
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    if-eqz v2, :cond_7

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_7
    add-int v0, v4, v2

    .line 116
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->guestsCanModify:Z

    if-eqz v2, :cond_8

    move v2, v3

    :goto_8
    add-int v0, v4, v2

    .line 117
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    if-eqz v2, :cond_9

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    invoke-virtual {v2}, Ljava/lang/Object;->hashCode()I

    move-result v2

    :goto_9
    add-int v0, v4, v2

    .line 118
    mul-int/lit8 v2, v0, 0x1f

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    add-int v0, v2, v4

    .line 119
    mul-int/lit8 v4, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    if-eqz v2, :cond_a

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :goto_a
    add-int v0, v4, v2

    .line 120
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    if-eqz v4, :cond_b

    :goto_b
    add-int v0, v2, v3

    .line 121
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    ushr-long/2addr v4, v8

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 122
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    add-int v0, v1, v2

    .line 123
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    add-int v0, v1, v2

    .line 124
    return v0

    .end local v0    # "result":I
    :cond_0
    move v0, v1

    .line 98
    goto/16 :goto_0

    .restart local v0    # "result":I
    :cond_1
    move v2, v1

    .line 99
    goto/16 :goto_1

    :cond_2
    move v2, v1

    .line 100
    goto/16 :goto_2

    :cond_3
    move v2, v1

    .line 102
    goto/16 :goto_3

    :cond_4
    move v2, v1

    .line 111
    goto/16 :goto_4

    :cond_5
    move v2, v1

    .line 112
    goto :goto_5

    :cond_6
    move v2, v1

    .line 113
    goto :goto_6

    :cond_7
    move v2, v1

    .line 115
    goto :goto_7

    :cond_8
    move v2, v1

    .line 116
    goto :goto_8

    :cond_9
    move v2, v1

    .line 117
    goto :goto_9

    :cond_a
    move v2, v1

    .line 119
    goto :goto_a

    :cond_b
    move v3, v1

    .line 120
    goto :goto_b
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 129
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Event{title="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", location="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", allDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", timezone=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startTime:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endTime="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endTime:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", instanceId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->instanceId:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endDay="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", startMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", endMillis="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", hasAlarm="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rrule=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rrule:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", rdate=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->rdate:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", selfAttendeeStatus="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->selfAttendeeStatus:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", organizer=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", guestsCanModify="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->guestsCanModify:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", facebookScheudleId="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stickerGroup="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stickerPath=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isRepeating="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", stickerType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mColumn="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", mMaxColumns="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
