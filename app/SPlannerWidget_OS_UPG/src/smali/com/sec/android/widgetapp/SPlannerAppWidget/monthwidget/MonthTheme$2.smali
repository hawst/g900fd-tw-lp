.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;
.super Ljava/lang/Object;
.source "MonthTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V
    .locals 0

    .prologue
    .line 129
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 132
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-object v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 134
    .local v2, "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getSelectedMillis()J

    move-result-wide v0

    .line 135
    .local v0, "millis":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v3

    invoke-static {v3, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->launchHandwritingMode(Landroid/content/Context;J)V

    .line 136
    return-void
.end method
