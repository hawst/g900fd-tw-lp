.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;
.super Ljava/lang/Object;
.source "TaskAccount.java"


# static fields
.field public static final ACCOUNT_NAME_MYTASK:Ljava/lang/String; = "My task"


# instance fields
.field public Selected:I

.field public id:I

.field public key:I

.field public name:Ljava/lang/String;

.field public syncAccount:Ljava/lang/String;

.field public type:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 31
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    .line 24
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    .line 26
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    .line 29
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    .line 32
    return-void
.end method

.method public constructor <init>(ILjava/lang/String;ILjava/lang/String;I)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "syncAccount"    # Ljava/lang/String;
    .param p3, "key"    # I
    .param p4, "type"    # Ljava/lang/String;
    .param p5, "selected"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x0

    .line 34
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    .line 24
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    .line 25
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    .line 26
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    .line 27
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    .line 28
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    .line 29
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    .line 35
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    .line 36
    iput-object p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    .line 37
    iput p3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    .line 38
    iput-object p4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    .line 39
    iput p5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    .line 40
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 44
    if-ne p0, p1, :cond_1

    .line 58
    :cond_0
    :goto_0
    return v1

    .line 45
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 47
    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;

    .line 49
    .local v0, "that":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 50
    :cond_4
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 51
    :cond_5
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 52
    :cond_6
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    :cond_7
    move v1, v2

    goto :goto_0

    :cond_8
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 53
    :cond_9
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    if-eqz v3, :cond_b

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    :cond_a
    move v1, v2

    .line 54
    goto :goto_0

    .line 53
    :cond_b
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    if-nez v3, :cond_a

    .line 55
    :cond_c
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_f

    :cond_d
    move v1, v2

    goto :goto_0

    :cond_e
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 56
    :cond_f
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    if-eqz v3, :cond_10

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto :goto_0

    :cond_10
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 63
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    .line 64
    .local v0, "result":I
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_0
    add-int v0, v3, v1

    .line 65
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    add-int v0, v1, v3

    .line 66
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_1
    add-int v0, v3, v1

    .line 67
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v3, v1

    .line 68
    mul-int/lit8 v1, v0, 0x1f

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    add-int v0, v1, v3

    .line 69
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    if-eqz v3, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    :cond_0
    add-int v0, v1, v2

    .line 70
    return v0

    :cond_1
    move v1, v2

    .line 64
    goto :goto_0

    :cond_2
    move v1, v2

    .line 66
    goto :goto_1

    :cond_3
    move v1, v2

    .line 67
    goto :goto_2
.end method

.method public toString()Ljava/lang/String;
    .locals 3

    .prologue
    const/16 v2, 0x27

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "TaskAccount{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", syncAccount=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->syncAccount:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", key="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->key:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", type=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->type:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", url=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->url:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", Selected="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->Selected:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", name=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/TaskAccount;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
