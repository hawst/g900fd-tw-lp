.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;
.super Landroid/content/BroadcastReceiver;
.source "MagazineCalendarViewService.java"

# interfaces
.implements Landroid/content/Loader$OnLoadCompleteListener;
.implements Landroid/widget/RemoteViewsService$RemoteViewsFactory;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MagazineRemoteViewsFactory"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/BroadcastReceiver;",
        "Landroid/widget/RemoteViewsService$RemoteViewsFactory;",
        "Landroid/content/Loader$OnLoadCompleteListener",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field public current_month:Z

.field public day_selected:Z

.field df:Ljava/text/DateFormat;

.field inputMethod:Landroid/view/inputmethod/InputMethodManager;

.field private mAppWidgetId:I

.field mCalMaxPreMonth:I

.field private mContext:Landroid/content/Context;

.field mCurentDateString:Ljava/lang/String;

.field private mDayString:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field mFirstDay:I

.field mMaxPreMonth:I

.field mMaxWeekNumber:I

.field private mMonth:Ljava/util/Calendar;

.field mMonthLength:I

.field mOtherThanMonthColor:I

.field mOtherdayColor:I

.field public mPreMonth:Ljava/util/GregorianCalendar;

.field public mPreMonthMaxSet:Ljava/util/GregorianCalendar;

.field private mResources:Landroid/content/res/Resources;

.field private mSelectedDate:Ljava/util/GregorianCalendar;

.field private mSelectedMonth:I

.field private mSelectedYear:I

.field mTodayColor:I

.field public next_day:Z

.field public next_month:Z

.field pref:Landroid/content/SharedPreferences;

.field public prev_day:Z

.field public prev_month:Z

.field private selected_day:I

.field private selected_month:I

.field private selected_year:I

.field public today_selected:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 113
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 84
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_month:Z

    .line 85
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_month:Z

    .line 86
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_day:Z

    .line 87
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_day:Z

    .line 88
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->current_month:Z

    .line 89
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    .line 90
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->today_selected:Z

    .line 91
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    .line 92
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    .line 115
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v0, -0x1

    const/4 v1, 0x0

    .line 96
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 84
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_month:Z

    .line 85
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_month:Z

    .line 86
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_day:Z

    .line 87
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_day:Z

    .line 88
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->current_month:Z

    .line 89
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    .line 90
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->today_selected:Z

    .line 91
    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    .line 92
    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    .line 97
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    .line 98
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mResources:Landroid/content/res/Resources;

    .line 99
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/GregorianCalendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 100
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    .line 101
    const-string v0, "appWidgetId"

    invoke-virtual {p2, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    .line 104
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mResources:Landroid/content/res/Resources;

    const v1, 0x7f070019

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    .line 105
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mResources:Landroid/content/res/Resources;

    const v1, 0x106000b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mOtherdayColor:I

    .line 107
    const-string v0, "#92bd64"

    invoke-static {v0}, Landroid/graphics/Color;->parseColor(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mOtherThanMonthColor:I

    .line 109
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    const-string v1, "input_method"

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/inputmethod/InputMethodManager;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->inputMethod:Landroid/view/inputmethod/InputMethodManager;

    .line 111
    return-void
.end method

.method private getMaxP()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 184
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v2

    if-ne v1, v2, :cond_0

    .line 186
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    invoke-virtual {v1, v2, v3, v5}, Ljava/util/GregorianCalendar;->set(III)V

    .line 191
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/GregorianCalendar;->getActualMaximum(I)I

    move-result v0

    .line 193
    .local v0, "maxP":I
    return v0

    .line 189
    .end local v0    # "maxP":I
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    const/4 v2, -0x1

    invoke-virtual {v1, v4, v2}, Ljava/util/GregorianCalendar;->add(II)V

    goto :goto_0
.end method

.method private isCurrentMonth()Z
    .locals 3

    .prologue
    .line 222
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 223
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isToday(Ljava/lang/String;I)Z
    .locals 7
    .param p1, "gridvalue"    # Ljava/lang/String;
    .param p2, "position"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 309
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCurentDateString:Ljava/lang/String;

    const-string v5, "-"

    invoke-virtual {v4, v5}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    .line 310
    .local v0, "DATE":[Ljava/lang/String;
    const/4 v4, 0x2

    aget-object v4, v0, v4

    const-string v5, "^0*"

    const-string v6, ""

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 313
    .local v1, "day":Ljava/lang/String;
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    if-le v4, v2, :cond_0

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    if-lt p2, v4, :cond_1

    :cond_0
    invoke-static {p1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    const/4 v5, 0x7

    if-ge v4, v5, :cond_3

    const/16 v4, 0x1c

    if-le p2, v4, :cond_3

    :cond_1
    move v2, v3

    .line 317
    :cond_2
    :goto_0
    return v2

    :cond_3
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->isCurrentMonth()Z

    move-result v4

    if-eqz v4, :cond_4

    invoke-virtual {p1, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    :cond_4
    move v2, v3

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonthLength:I

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 212
    int-to-long v0, p1

    return-wide v0
.end method

.method public getLoadingView()Landroid/widget/RemoteViews;
    .locals 3

    .prologue
    .line 217
    new-instance v0, Landroid/widget/RemoteViews;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f030007

    invoke-direct {v0, v1, v2}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 218
    .local v0, "views":Landroid/widget/RemoteViews;
    return-object v0
.end method

.method public getViewAt(I)Landroid/widget/RemoteViews;
    .locals 12
    .param p1, "position"    # I

    .prologue
    .line 228
    if-ltz p1, :cond_0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->getCount()I

    move-result v7

    if-lt p1, v7, :cond_2

    .line 229
    :cond_0
    const/4 v5, 0x0

    .line 305
    :cond_1
    :goto_0
    return-object v5

    .line 231
    :cond_2
    new-instance v5, Landroid/widget/RemoteViews;

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v7

    const v8, 0x7f030006

    invoke-direct {v5, v7, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 234
    .local v5, "views":Landroid/widget/RemoteViews;
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    invoke-interface {v7}, Ljava/util/List;->size()I

    move-result v7

    if-le v7, p1, :cond_1

    .line 236
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    invoke-interface {v7, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    const-string v8, "-"

    invoke-virtual {v7, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 238
    .local v4, "separatedTime":[Ljava/lang/String;
    const/4 v7, 0x2

    aget-object v7, v4, v7

    const-string v8, "^0*"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 239
    .local v2, "gridvalue":Ljava/lang/String;
    const/4 v7, 0x1

    aget-object v7, v4, v7

    const-string v8, "^0*"

    const-string v9, ""

    invoke-virtual {v7, v8, v9}, Ljava/lang/String;->replaceFirst(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 240
    .local v3, "month":I
    const/4 v7, 0x0

    aget-object v7, v4, v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 242
    .local v6, "year":I
    invoke-direct {p0, v2, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->isToday(Ljava/lang/String;I)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCurentDateString:Ljava/lang/String;

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    invoke-interface {v8, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_8

    .line 243
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    if-eqz v7, :cond_6

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->today_selected:Z

    if-eqz v7, :cond_6

    .line 244
    sget-boolean v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    if-eqz v7, :cond_5

    .line 245
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    .line 246
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020003

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 247
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 276
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->inputMethod:Landroid/view/inputmethod/InputMethodManager;

    invoke-virtual {v7}, Landroid/view/inputmethod/InputMethodManager;->isAccessoryKeyboardState()I

    move-result v7

    if-eqz v7, :cond_3

    .line 277
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v7, "dd"

    invoke-direct {v0, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 278
    .local v0, "day":Ljava/text/SimpleDateFormat;
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->isCurrentMonth()Z

    move-result v7

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v7}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x2

    aget-object v8, v4, v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 279
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020002

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 280
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    .line 283
    .end local v0    # "day":Ljava/text/SimpleDateFormat;
    :cond_3
    const v8, 0x7f0c0042

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v7

    if-eqz v7, :cond_b

    move-object v7, v2

    :goto_2
    invoke-virtual {v5, v8, v7}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 285
    new-instance v1, Landroid/content/Intent;

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    const-class v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v1, v7, v8}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 287
    .local v1, "fillInIntent":Landroid/content/Intent;
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    if-lt v6, v7, :cond_4

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    if-ge v3, v7, :cond_c

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    if-ne v6, v7, :cond_c

    .line 288
    :cond_4
    const-string v7, "UpdatePreviousMonth"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    const-string v7, "SelectedDay"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 302
    :goto_3
    const v7, 0x7f0c0042

    invoke-virtual {v5, v7, v1}, Landroid/widget/RemoteViews;->setOnClickFillInIntent(ILandroid/content/Intent;)V

    goto/16 :goto_0

    .line 249
    .end local v1    # "fillInIntent":Landroid/content/Intent;
    :cond_5
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020003

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 250
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 253
    :cond_6
    sget-boolean v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    if-eqz v7, :cond_7

    .line 254
    const/4 v7, 0x0

    sput-boolean v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    .line 255
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020004

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 256
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 258
    :cond_7
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020004

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 259
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mTodayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 262
    :cond_8
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    if-eq v3, v7, :cond_9

    .line 265
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x106000d

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 266
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mOtherThanMonthColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 269
    :cond_9
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    if-eqz v7, :cond_a

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_a

    .line 270
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x7f020002

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 271
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mOtherdayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 273
    :cond_a
    const v7, 0x7f0c0042

    const-string v8, "setBackgroundResource"

    const v9, 0x106000d

    invoke-virtual {v5, v7, v8, v9}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 274
    const v7, 0x7f0c0042

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mOtherdayColor:I

    invoke-virtual {v5, v7, v8}, Landroid/widget/RemoteViews;->setTextColor(II)V

    goto/16 :goto_1

    .line 283
    :cond_b
    const-string v7, "%d"

    const/4 v9, 0x1

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v11

    invoke-static {v11}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    aput-object v11, v9, v10

    invoke-static {v7, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v7

    goto/16 :goto_2

    .line 291
    .restart local v1    # "fillInIntent":Landroid/content/Intent;
    :cond_c
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    if-gt v6, v7, :cond_d

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    if-le v3, v7, :cond_e

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    if-ne v6, v7, :cond_e

    .line 292
    :cond_d
    const-string v7, "UpdateNextMonth"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 293
    const-string v7, "SelectedDay"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    goto/16 :goto_3

    .line 296
    :cond_e
    const-string v7, "CalendarItemUpdate"

    invoke-virtual {v1, v7}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 297
    const-string v7, "SelectedDay"

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 298
    const-string v7, "SelectedMonth"

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    const/4 v9, 0x2

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 299
    const-string v7, "SelectedYear"

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    const/4 v9, 0x1

    invoke-virtual {v8, v9}, Ljava/util/Calendar;->get(I)I

    move-result v8

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 300
    const-string v7, "DaySelected"

    const/4 v8, 0x1

    invoke-virtual {v1, v7, v8}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    goto/16 :goto_3
.end method

.method public getViewTypeCount()I
    .locals 1

    .prologue
    .line 322
    const/4 v0, 0x1

    return v0
.end method

.method public hasStableIds()Z
    .locals 1

    .prologue
    .line 327
    const/4 v0, 0x1

    return v0
.end method

.method public onCreate()V
    .locals 3

    .prologue
    .line 198
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 199
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    .line 200
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->df:Ljava/text/DateFormat;

    .line 201
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->df:Ljava/text/DateFormat;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v1}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCurentDateString:Ljava/lang/String;

    .line 202
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->refreshDays()V

    .line 203
    return-void
.end method

.method public onDataSetChanged()V
    .locals 18

    .prologue
    .line 333
    const-string v1, "SelectedDay%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 334
    .local v14, "selectedDayKey":Ljava/lang/String;
    const-string v1, "SelectedMonth%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v15

    .line 335
    .local v15, "selectedMonthKey":Ljava/lang/String;
    const-string v1, "SelectedYear%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    .line 336
    .local v16, "selectedYearKey":Ljava/lang/String;
    const-string v1, "day_selected%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 337
    .local v9, "daySelectedkey":Ljava/lang/String;
    const-string v1, "today_selected%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v17

    .line 338
    .local v17, "todaySelectedkey":Ljava/lang/String;
    const-string v1, "next_month%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 339
    .local v11, "nextMonthSelection":Ljava/lang/String;
    const-string v1, "prev_month%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 340
    .local v13, "prevMonthSelection":Ljava/lang/String;
    const-string v1, "curr_month%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 341
    .local v8, "currentMonthSelection":Ljava/lang/String;
    const-string v1, "next_day%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 342
    .local v10, "nextDaySelection":Ljava/lang/String;
    const-string v1, "prev_day%d"

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v2, v3

    invoke-static {v1, v2}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 343
    .local v12, "prevDaySelection":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v9, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    .line 344
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    move-object/from16 v0, v17

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->today_selected:Z

    .line 345
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v11, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_month:Z

    .line 346
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v13, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_month:Z

    .line 347
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v10, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_day:Z

    .line 348
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v12, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_day:Z

    .line 349
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v8, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->current_month:Z

    .line 350
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mAppWidgetId:I

    if-lez v1, :cond_2

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    if-eqz v1, :cond_2

    .line 351
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v14, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 352
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v14, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    .line 354
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v15, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 355
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-interface {v1, v15, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_month:I

    .line 357
    :cond_1
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 358
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    move-object/from16 v0, v16

    invoke-interface {v1, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    move-object/from16 v0, p0

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_year:I

    .line 361
    :cond_2
    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_month:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_3

    move-object/from16 v0, p0

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_year:I

    const/4 v2, 0x1

    if-ge v1, v2, :cond_3

    .line 362
    const/4 v1, 0x1

    move-object/from16 v0, p0

    iput-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->current_month:Z

    .line 364
    :cond_3
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_month:Z

    if-eqz v1, :cond_5

    .line 365
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 366
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->refreshDays()V

    .line 367
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->fetchFromPref:Z

    .line 368
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-static {v1, v11, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 395
    :cond_4
    :goto_0
    return-void

    .line 369
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_month:Z

    if-eqz v1, :cond_6

    .line 370
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 371
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->refreshDays()V

    .line 372
    const/4 v1, 0x1

    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->fetchFromPref:Z

    .line 373
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-static {v1, v13, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    goto :goto_0

    .line 374
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->current_month:Z

    if-eqz v1, :cond_7

    .line 375
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 376
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/util/GregorianCalendar;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    .line 377
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "yyyy-MM-dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->df:Ljava/text/DateFormat;

    .line 378
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->df:Ljava/text/DateFormat;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedDate:Ljava/util/GregorianCalendar;

    invoke-virtual {v2}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCurentDateString:Ljava/lang/String;

    .line 379
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->refreshDays()V

    .line 380
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-static {v1, v8, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    goto :goto_0

    .line 381
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->day_selected:Z

    if-eqz v1, :cond_4

    .line 382
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->next_day:Z

    if-nez v1, :cond_8

    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->prev_day:Z

    if-eqz v1, :cond_9

    .line 384
    :cond_8
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-static {v1, v10, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 385
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    const/4 v2, 0x0

    invoke-static {v1, v12, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 388
    :cond_9
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v1

    check-cast v1, Ljava/util/GregorianCalendar;

    move-object/from16 v0, p0

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    .line 389
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_year:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_month:I

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-virtual/range {v1 .. v7}, Ljava/util/Calendar;->set(IIIIII)V

    .line 390
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_day:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v14, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 391
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_month:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v15, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 392
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->pref:Landroid/content/SharedPreferences;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->selected_year:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-static {v1, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 393
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->refreshDays()V

    goto/16 :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 399
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    .line 400
    return-void
.end method

.method public onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "cursor"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Loader",
            "<",
            "Landroid/database/Cursor;",
            ">;",
            "Landroid/database/Cursor;",
            ")V"
        }
    .end annotation

    .prologue
    .line 409
    .local p1, "loader":Landroid/content/Loader;, "Landroid/content/Loader<Landroid/database/Cursor;>;"
    return-void
.end method

.method public bridge synthetic onLoadComplete(Landroid/content/Loader;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Loader;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 51
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->onLoadComplete(Landroid/content/Loader;Landroid/database/Cursor;)V

    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 404
    return-void
.end method

.method public refreshDays()V
    .locals 10

    .prologue
    const/4 v9, 0x5

    const/4 v8, 0x2

    const/4 v7, 0x7

    const/4 v6, 0x1

    .line 118
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    if-eqz v3, :cond_0

    .line 119
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 121
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v3}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/GregorianCalendar;

    iput-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    .line 122
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    invoke-virtual {v3, v8}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedMonth:I

    .line 123
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    invoke-virtual {v3, v6}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mSelectedYear:I

    .line 130
    new-instance v3, Ljava/util/GregorianCalendar;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v4, v6}, Ljava/util/Calendar;->get(I)I

    move-result v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v5, v8}, Ljava/util/Calendar;->get(I)I

    move-result v5

    invoke-direct {v3, v4, v5, v6}, Ljava/util/GregorianCalendar;-><init>(III)V

    invoke-virtual {v3, v7}, Ljava/util/GregorianCalendar;->get(I)I

    move-result v3

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    .line 132
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mContext:Landroid/content/Context;

    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v2

    .line 134
    .local v2, "startDay":I
    if-ne v2, v7, :cond_3

    .line 135
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    if-lt v3, v6, :cond_2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    if-ge v3, v7, :cond_2

    .line 136
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    .line 149
    :cond_1
    :goto_0
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v4, v9}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v4

    add-int/2addr v3, v4

    add-int/lit8 v3, v3, 0x5

    div-int/lit8 v3, v3, 0x7

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMaxWeekNumber:I

    .line 151
    const/4 v3, 0x6

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMaxWeekNumber:I

    .line 153
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMaxWeekNumber:I

    mul-int/lit8 v3, v3, 0x7

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonthLength:I

    .line 154
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->getMaxP()I

    move-result v3

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMaxPreMonth:I

    .line 155
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMaxPreMonth:I

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    add-int/lit8 v4, v4, -0x1

    sub-int/2addr v3, v4

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCalMaxPreMonth:I

    .line 161
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonth:Ljava/util/GregorianCalendar;

    invoke-virtual {v3}, Ljava/util/GregorianCalendar;->clone()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/util/GregorianCalendar;

    iput-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonthMaxSet:Ljava/util/GregorianCalendar;

    .line 165
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonthMaxSet:Ljava/util/GregorianCalendar;

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mCalMaxPreMonth:I

    add-int/lit8 v4, v4, 0x1

    invoke-virtual {v3, v9, v4}, Ljava/util/GregorianCalendar;->set(II)V

    .line 170
    const/4 v0, 0x0

    .line 171
    .local v0, "itemvalue":Ljava/lang/String;
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    iput-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    .line 173
    const/4 v1, 0x0

    .local v1, "n":I
    :goto_1
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mMonthLength:I

    if-ge v1, v3, :cond_5

    .line 175
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->df:Ljava/text/DateFormat;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonthMaxSet:Ljava/util/GregorianCalendar;

    invoke-virtual {v4}, Ljava/util/GregorianCalendar;->getTime()Ljava/util/Date;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    .line 176
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mPreMonthMaxSet:Ljava/util/GregorianCalendar;

    invoke-virtual {v3, v9, v6}, Ljava/util/GregorianCalendar;->add(II)V

    .line 177
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mDayString:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 173
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 138
    .end local v0    # "itemvalue":Ljava/lang/String;
    .end local v1    # "n":I
    :cond_2
    iput v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    goto :goto_0

    .line 140
    :cond_3
    if-ne v2, v8, :cond_1

    .line 141
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    if-le v3, v6, :cond_4

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    if-gt v3, v7, :cond_4

    .line 142
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    goto :goto_0

    .line 144
    :cond_4
    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService$MagazineRemoteViewsFactory;->mFirstDay:I

    goto :goto_0

    .line 180
    .restart local v0    # "itemvalue":Ljava/lang/String;
    .restart local v1    # "n":I
    :cond_5
    return-void
.end method
