.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;
.super Ljava/lang/Object;
.source "TaskAccountHandler.java"


# static fields
.field public static final SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

.field public static final TASKSACCOUTS_CONTENT_URI:Landroid/net/Uri;

.field public static final TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

.field private static mTaskColors:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 12
    const-string v0, "content://com.android.calendar/syncTasks"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 15
    const-string v0, "content://com.android.calendar/TasksReminders"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->TASKS_REMINDERS_CONTENT_URI:Landroid/net/Uri;

    .line 18
    const-string v0, "content://com.android.calendar/TasksAccounts"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->TASKSACCOUTS_CONTENT_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    return-void
.end method

.method public static getTaskColor(Landroid/content/Context;I)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "key"    # I

    .prologue
    .line 54
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->initAccountColor(Landroid/content/res/Resources;)V

    .line 56
    if-gtz p1, :cond_0

    .line 57
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x0

    aget v0, v0, v1

    .line 59
    :goto_0
    return v0

    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    rem-int/lit8 v1, p1, 0x13

    aget v0, v0, v1

    goto :goto_0
.end method

.method public static initAccountColor(Landroid/content/res/Resources;)V
    .locals 3
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    .line 27
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    if-eqz v0, :cond_0

    .line 51
    :goto_0
    return-void

    .line 31
    :cond_0
    const/16 v0, 0x13

    new-array v0, v0, [I

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    .line 32
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x0

    const v2, 0x7f070045

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 33
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x1

    const v2, 0x7f070046

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 34
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x2

    const v2, 0x7f070047

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 35
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x3

    const v2, 0x7f070048

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 36
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x4

    const v2, 0x7f070049

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 37
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x5

    const v2, 0x7f07004a

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 38
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x6

    const v2, 0x7f07004b

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 39
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/4 v1, 0x7

    const v2, 0x7f07004c

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 40
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0x8

    const v2, 0x7f07004d

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 41
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0x9

    const v2, 0x7f07004e

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 42
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xa

    const v2, 0x7f07004f

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 43
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xb

    const v2, 0x7f070050

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 44
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xc

    const v2, 0x7f070051

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 45
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xd

    const v2, 0x7f070052

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 46
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xe

    const v2, 0x7f070053

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 47
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0xf

    const v2, 0x7f070054

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 48
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0x10

    const v2, 0x7f070055

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 49
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0x11

    const v2, 0x7f070056

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    .line 50
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->mTaskColors:[I

    const/16 v1, 0x12

    const v2, 0x7f070057

    invoke-virtual {p0, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    aput v2, v0, v1

    goto/16 :goto_0
.end method
