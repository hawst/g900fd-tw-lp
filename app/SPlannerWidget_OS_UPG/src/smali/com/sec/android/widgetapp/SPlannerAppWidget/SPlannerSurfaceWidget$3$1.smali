.class Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;
.super Ljava/lang/Object;
.source "SPlannerSurfaceWidget.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->handleMessage(Landroid/os/Message;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;)V
    .locals 0

    .prologue
    .line 135
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 2

    .prologue
    .line 138
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-nez v1, :cond_1

    .line 147
    :cond_0
    return-void

    .line 142
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 143
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    if-eqz v1, :cond_2

    .line 144
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;->this$1:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onRefresh()V

    .line 142
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_0
.end method
