.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;
    }
.end annotation


# static fields
.field public static final COLUMN_DATE:Ljava/lang/String; = "date"

.field public static final COLUMN_ID:Ljava/lang/String; = "_id"

.field public static final COLUMN_MEMO_ID:Ljava/lang/String; = "memo_id"

.field private static final EVENT_ARRAY_MAX_SIZE:I = 0xb

.field private static EVENT_BOX_STROKE_WIDTH:I = 0x0

.field private static final EVENT_MEMO_NUMBER:I = 0x2b

.field private static GRID_LINE_STROKE_WIDTH:I = 0x0

.field private static final INTENSITY_ADJUST:F = 1.5f

.field private static final MONTH_COLUMN_NUM:I = 0x8

.field private static final MONTH_COLUMN_WEEK:I = 0x0

.field private static MONTH_ROW_NUM:I = 0x0

.field public static final QSMEMOS_INDEX_DATE:I = 0x2

.field public static final QSMEMOS_INDEX_ID:I = 0x0

.field public static final QSMEMOS_INDEX_MEMO_ID:I = 0x1

.field public static final QSMEMO_CONTENT_URI:Ljava/lang/String; = "content://com.android.calendar/qsmemos"

.field private static final SATURATION_ADJUST:F = 2.3f

.field private static STICKER_GROUP_ID:I

.field private static STICKER_TYPE:I

.field private static final TAG:Ljava/lang/String;

.field private static TITLE_EVENT_NUMBER:I

.field private static mLunarLeapString:Ljava/lang/String;

.field private static sCurrentTime:Landroid/text/format/Time;


# instance fields
.field private final CALENDAR_STICKER_ENABLED:Z

.field public QSMEMOS_PROJECTION:[Ljava/lang/String;

.field private eventArrays:Ljava/util/ArrayList;

.field private isFontStyleDefault:Z

.field private isWeekFirstColumn:Z

.field private mAccountColorBarMarginRight:I

.field private mAccountColorBarWidth:I

.field private mAreNationalHolidaysSupported:Z

.field private mAttachMemo:Landroid/graphics/drawable/Drawable;

.field private mBGColor:I

.field private mBitmap:Landroid/graphics/Bitmap;

.field private final mBitmapRect:Landroid/graphics/Rect;

.field private final mCancelCallback:Ljava/lang/Runnable;

.field private mCanvas:Landroid/graphics/Canvas;

.field private mCellHeight:I

.field private mCellWidth:I

.field private mCheckBoxOff:Landroid/graphics/drawable/Drawable;

.field private mCheckBoxOn:Landroid/graphics/drawable/Drawable;

.field private mCheckboxMarginTopFromBoxTop:I

.field private mCheckboxWidth:I

.field private mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

.field private mDayEventCount:[I

.field private mDayNumPaint:Landroid/graphics/Paint;

.field private mDayTextPaddingLeft:I

.field private mDayTextPaddingTop:I

.field private mDayTextSize:I

.field private mDisplaySubstHolidays:Z

.field private mEventArray:[[Ljava/lang/String;

.field private mEventBoxHeight:I

.field private mEventBoxMarginBottom:I

.field private mEventBoxPaint:Landroid/graphics/Paint;

.field private mEventColorArray:[[I

.field private mEventCountBGHeight:I

.field private mEventCountBGOvalRadius:I

.field private mEventCountTextMarginRight:I

.field private mEventCountTextMarginTop:I

.field private mEventDayGapArray:[[I

.field public mEventMemos:[Ljava/lang/Integer;

.field private mEventNumOfMonthRow5:I

.field private mEventNumOfMonthRow6:I

.field private mEventTitleAvailAdjust:I

.field private mEventTitleMarginLeft:I

.field private mEventTitleMargineTop:I

.field private mEventTitleTextSize:I

.field private mEvents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field private mFestivalEffectBGDrawable:Landroid/graphics/drawable/Drawable;

.field private mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

.field private mFirstColumnCellWidth:I

.field private mGestureDetector:Landroid/view/GestureDetector;

.field private mHasStickerIcon:Z

.field private mHasWeatherIcon:Z

.field private mHolidayManager:Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

.field private mIconOccupiedEventLines:I

.field private mIntervalBetweenEventBox:I

.field private mIsChinaFeature:Z

.field private mIsHKTWFeature:Z

.field private mIsLocalHolidayColorDisplay:Z

.field private mIsLunarCalendarSupported:Z

.field private mIsLunarDateDisplaySupported:Z

.field private mIsUsaWeekNumber:Z

.field private mIsVietNameseFeature:Z

.field private mLastReloadMillis:J

.field private mLaunchDayView:Z

.field private mLunarDatePaint:Landroid/graphics/Paint;

.field private mLunarDateYOffset:I

.field private mLunarLeapStringWidth:I

.field private mLunarTextSize:I

.field private mMonthColumnExtraWidth:I

.field private mMonthDayCountTextSize:I

.field private mMonthDayNumberColor:I

.field private mMonthDayNumberDimColor:I

.field private mMonthEventCountBGColor:I

.field private mMonthEventCountTextColor:I

.field private mMonthEventCountTextDimColor:I

.field private mMonthFridayNumberColor:I

.field private mMonthFridayNumberDimColor:I

.field private mMonthHeight:I

.field private mMonthLineColor:I

.field private mMonthLineWidth:I

.field private mMonthLunarDateColor:I

.field private mMonthOverlayAddColor:I

.field private mMonthOverlayMulColor:I

.field private mMonthSaturdayNumberColor:I

.field private mMonthSaturdayNumberDimColor:I

.field private mMonthSundayNumberColor:I

.field private mMonthSundayNumberDimColor:I

.field private mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

.field private mMonthTodayFocusedNumberColor:I

.field private mMonthTodayNumberColor:I

.field private mMonthViewTextEventColor:I

.field private mMonthViewTextEventDimColor:I

.field private mMonthWeekNumColor:I

.field private mMonthWidth:I

.field private mMoreEventCountRect:Landroid/graphics/RectF;

.field private mNextFirstJulianDay:I

.field private mNumDays:I

.field private mPaint:Landroid/graphics/Paint;

.field private mPaintInside:Landroid/graphics/Paint;

.field private mPaintOutside:Landroid/graphics/Paint;

.field private mPressedCol:I

.field private mPressedRow:I

.field private mPrevNextDayTextSize:I

.field public mQSMemoEventArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;",
            ">;"
        }
    .end annotation
.end field

.field private mQSMemoHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

.field private final mRect:Landroid/graphics/Rect;

.field private mRedrawScreen:Z

.field private mSelectionStrokeWidth:I

.field protected mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

.field private mStartDay:I

.field private mStickerBitmapCache:Landroid/util/LruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/LruCache",
            "<",
            "Ljava/lang/Long;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private mStickerImageSize:I

.field private final mStickerInfo:[[J

.field private final mTaskComplete:[[Z

.field private mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

.field private mTaskTitleAvailAdjust:I

.field private mTaskTitleMarginLeft:I

.field private final mTasksArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation
.end field

.field private final mTempTime:Landroid/text/format/Time;

.field private mTextPaint:Landroid/text/TextPaint;

.field private mTitleEventNumber:I

.field private mToday:Landroid/text/format/Time;

.field private mTodayBackgroundMarginLeft:I

.field private mTodayBackgroundMarginTop:I

.field private mTodayMakerSize:I

.field private mVietnameseLaborDayString:Ljava/lang/String;

.field private final mWallpaperStickerInfo:[[J

.field private mWeatherImageMarginLeft:I

.field private mWeatherImageTopMargine:I

.field private mWeatherImageWidthSize:I


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 81
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    .line 84
    const/4 v0, 0x6

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 87
    const/4 v0, 0x2

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    .line 90
    const/4 v0, 0x0

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    .line 91
    sput v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    .line 92
    sput v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    .line 93
    sput v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->GRID_LINE_STROKE_WIDTH:I

    .line 202
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->sCurrentTime:Landroid/text/format/Time;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;I)V
    .locals 7
    .param p1, "monthTheme"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    .param p2, "mSurfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p3, "eventLoader"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    .param p4, "startDay"    # I

    .prologue
    const/4 v6, 0x2

    const/4 v5, 0x0

    const/16 v4, 0xb

    const/4 v3, 0x1

    const/16 v2, 0x2b

    .line 271
    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 94
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->CALENDAR_STICKER_ENABLED:Z

    .line 96
    const/4 v0, 0x6

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionStrokeWidth:I

    .line 162
    const/16 v0, 0x12

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarTextSize:I

    .line 166
    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDateYOffset:I

    .line 167
    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarLeapStringWidth:I

    .line 178
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedRow:I

    .line 179
    const/4 v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedCol:I

    .line 188
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;

    .line 189
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTasksArray:Ljava/util/ArrayList;

    .line 190
    filled-new-array {v2, v4}, [I

    move-result-object v0

    const-class v1, Ljava/lang/String;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    .line 191
    filled-new-array {v2, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventColorArray:[[I

    .line 192
    filled-new-array {v2, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[I

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventDayGapArray:[[I

    .line 193
    new-array v0, v2, [I

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    .line 194
    filled-new-array {v2, v4}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Boolean;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[Z

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskComplete:[[Z

    .line 195
    filled-new-array {v2, v6}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    .line 196
    filled-new-array {v2, v6}, [I

    move-result-object v0

    sget-object v1, Ljava/lang/Long;->TYPE:Ljava/lang/Class;

    invoke-static {v1, v0}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [[J

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    .line 200
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTempTime:Landroid/text/format/Time;

    .line 205
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isChinaFeature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    .line 206
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isHKTWFeature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsHKTWFeature:Z

    .line 207
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isVietNameseFeature()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsVietNameseFeature:Z

    .line 208
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isLocalHolidayColorDisplay()Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLocalHolidayColorDisplay:Z

    .line 213
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isFontStyleDefault:Z

    .line 227
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    .line 228
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    .line 229
    new-instance v0, Landroid/graphics/Paint;

    const/16 v1, 0x100

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    .line 230
    new-instance v0, Landroid/text/TextPaint;

    invoke-direct {v0}, Landroid/text/TextPaint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    .line 231
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    .line 233
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRect:Landroid/graphics/Rect;

    .line 234
    new-instance v0, Landroid/graphics/RectF;

    invoke-direct {v0}, Landroid/graphics/RectF;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    .line 237
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    .line 238
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    .line 239
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    .line 243
    new-array v0, v2, [Ljava/lang/Integer;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    .line 252
    const/4 v0, 0x3

    new-array v0, v0, [Ljava/lang/String;

    const-string v1, "_id"

    aput-object v1, v0, v5

    const-string v1, "memo_id"

    aput-object v1, v0, v3

    const-string v1, "date"

    aput-object v1, v0, v6

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->QSMEMOS_PROJECTION:[Ljava/lang/String;

    .line 642
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$4;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$4;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCancelCallback:Ljava/lang/Runnable;

    .line 272
    iput-object p3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 273
    iput p4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    .line 276
    const v0, 0x7f0a0068

    invoke-virtual {p2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mVietnameseLaborDayString:Ljava/lang/String;

    .line 277
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .line 278
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->init(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 279
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Z
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLaunchDayView:Z

    return v0
.end method

.method static synthetic access$1002(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$102(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLaunchDayView:Z

    return p1
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$1102(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;)Ljava/util/ArrayList;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->eventArrays:Ljava/util/ArrayList;

    return-object p1
.end method

.method static synthetic access$1202(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[I)[[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # [[I

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventColorArray:[[I

    return-object p1
.end method

.method static synthetic access$1302(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[I)[[I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # [[I

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventDayGapArray:[[I

    return-object p1
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setTasks(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setQSMemos(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getPressedRow(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getPressedColumn(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    return-object v0
.end method

.method static synthetic access$502(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedRow:I

    return p1
.end method

.method static synthetic access$602(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # I

    .prologue
    .line 78
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedCol:I

    return p1
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # Z

    .prologue
    .line 78
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    return p1
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Landroid/text/format/Time;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTempTime:Landroid/text/format/Time;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)[[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .prologue
    .line 78
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$902(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;[[Ljava/lang/String;)[[Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .param p1, "x1"    # [[Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    return-object p1
.end method

.method private clearStickerInfo()V
    .locals 6

    .prologue
    const-wide/16 v4, -0x1

    .line 2560
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 2561
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    aget-object v1, v1, v0

    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aput-wide v4, v1, v2

    .line 2562
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    aget-object v1, v1, v0

    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    aput-wide v4, v1, v2

    .line 2560
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2564
    :cond_0
    const/4 v0, 0x0

    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    array-length v1, v1

    if-ge v0, v1, :cond_1

    .line 2565
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    aget-object v1, v1, v0

    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aput-wide v4, v1, v2

    .line 2566
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    aget-object v1, v1, v0

    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    aput-wide v4, v1, v2

    .line 2564
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 2568
    :cond_1
    return-void
.end method

.method private cutTitleforLongTitle(Ljava/lang/CharSequence;IIFLandroid/text/TextPaint;)Ljava/util/ArrayList;
    .locals 10
    .param p1, "displayTitle"    # Ljava/lang/CharSequence;
    .param p2, "day"    # I
    .param p3, "i"    # I
    .param p4, "avail"    # F
    .param p5, "textPaint"    # Landroid/text/TextPaint;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/CharSequence;",
            "IIF",
            "Landroid/text/TextPaint;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    .line 2267
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 2268
    .local v3, "mEventCutTitles":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    invoke-virtual {v3}, Ljava/util/ArrayList;->clear()V

    .line 2269
    sget-object v7, Ljava/text/Normalizer$Form;->NFC:Ljava/text/Normalizer$Form;

    invoke-static {p1, v7}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v6

    .line 2270
    .local v6, "vietnamese_laborday":Ljava/lang/String;
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mVietnameseLaborDayString:Ljava/lang/String;

    invoke-virtual {v6, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 2271
    move-object p1, v6

    .line 2274
    :cond_0
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    sub-int v0, v7, p3

    .line 2277
    .local v0, "count":I
    add-int/lit8 v7, p3, 0x2

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-ge v7, v8, :cond_5

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v7, v7, p2

    add-int/lit8 v8, p3, 0x2

    aget-object v7, v7, v8

    if-eqz v7, :cond_5

    .line 2278
    add-int/lit8 v0, v0, -0x1

    .line 2285
    :cond_1
    :goto_0
    move-object v1, p1

    .line 2286
    .local v1, "cur":Ljava/lang/CharSequence;
    move-object v4, p1

    .line 2287
    .local v4, "rem":Ljava/lang/CharSequence;
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 2289
    .local v5, "remLen":I
    :goto_1
    const/4 v2, 0x0

    .line 2291
    .local v2, "iconWidth":F
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    sub-int/2addr v7, v0

    sget v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v8, v9

    if-lt v7, v8, :cond_3

    .line 2292
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    if-eqz v7, :cond_2

    .line 2293
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    int-to-float v7, v7

    add-float/2addr v2, v7

    .line 2296
    :cond_2
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasWeatherIcon:Z

    if-eqz v7, :cond_3

    .line 2297
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageWidthSize:I

    int-to-float v7, v7

    add-float/2addr v2, v7

    .line 2301
    :cond_3
    sub-float v7, p4, v2

    sget-object v8, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    invoke-static {v4, p5, v7, v8}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v1

    .line 2303
    const/4 v7, 0x1

    if-eq v0, v7, :cond_4

    invoke-virtual {v1, v4}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_6

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    if-ne v7, v5, :cond_6

    .line 2304
    :cond_4
    invoke-interface {v1}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2312
    return-object v3

    .line 2280
    .end local v1    # "cur":Ljava/lang/CharSequence;
    .end local v2    # "iconWidth":F
    .end local v4    # "rem":Ljava/lang/CharSequence;
    .end local v5    # "remLen":I
    :cond_5
    add-int/lit8 v7, p3, 0x3

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-ge v7, v8, :cond_1

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v7, v7, p2

    add-int/lit8 v8, p3, 0x3

    aget-object v7, v7, v8

    if-eqz v7, :cond_1

    .line 2281
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 2307
    .restart local v1    # "cur":Ljava/lang/CharSequence;
    .restart local v2    # "iconWidth":F
    .restart local v4    # "rem":Ljava/lang/CharSequence;
    .restart local v5    # "remLen":I
    :cond_6
    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v7

    add-int/lit8 v7, v7, -0x1

    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v8

    invoke-interface {v4, v7, v8}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v4

    .line 2308
    invoke-interface {v4}, Ljava/lang/CharSequence;->length()I

    move-result v5

    .line 2309
    const/4 v7, 0x0

    invoke-interface {v1}, Ljava/lang/CharSequence;->length()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    invoke-interface {v1, v7, v8}, Ljava/lang/CharSequence;->subSequence(II)Ljava/lang/CharSequence;

    move-result-object v7

    invoke-interface {v7}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2310
    add-int/lit8 v0, v0, -0x1

    .line 2311
    goto :goto_1
.end method

.method private doDraw(Landroid/graphics/Canvas;)V
    .locals 25
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 901
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRect:Landroid/graphics/Rect;

    move-object/from16 v17, v0

    .line 902
    .local v17, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v19

    .line 905
    .local v19, "columnDay1":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int v4, v4, v19

    add-int/lit8 v11, v4, 0x1

    .line 906
    .local v11, "day":I
    const/4 v12, 0x0

    .line 907
    .local v12, "weekNum":I
    const/16 v18, 0x0

    .line 908
    .local v18, "calendar":Ljava/util/Calendar;
    const/4 v4, 0x1

    move/from16 v0, v19

    if-ne v0, v4, :cond_2

    const/16 v22, 0x1

    .line 909
    .local v22, "noPrevMonth":Z
    :goto_0
    const/16 v24, 0x2

    .line 910
    .local v24, "weekColumn":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v20

    .line 911
    .local v20, "firstDayOfWeek":I
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v18

    .line 912
    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->setFirstDayOfWeek(I)V

    .line 915
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v21

    .line 916
    .local v21, "maxDay":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, v21

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v4

    add-int/lit8 v4, v4, 0x1

    sput v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 917
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v23

    .line 918
    .local v23, "res":Landroid/content/res/Resources;
    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    const/4 v5, 0x6

    if-eq v4, v5, :cond_3

    .line 919
    const/4 v4, 0x5

    sput v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 920
    const v4, 0x7f090013

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    .line 921
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow5:I

    sput v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    .line 927
    :goto_1
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsUsaWeekNumber:Z

    if-eqz v4, :cond_4

    .line 928
    const/4 v4, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    .line 939
    :cond_0
    :goto_2
    const/16 v24, 0x1

    .line 940
    new-instance v9, Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-direct {v9, v4}, Landroid/graphics/Paint;-><init>(Landroid/graphics/Paint;)V

    .line 941
    .local v9, "mPaintDrawLine":Landroid/graphics/Paint;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLineWidth:I

    int-to-float v4, v4

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 942
    const/4 v4, 0x0

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 943
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLineColor:I

    invoke-virtual {v9, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 945
    const/4 v13, 0x0

    .local v13, "row":I
    :goto_3
    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    if-ge v13, v4, :cond_9

    .line 946
    move-object/from16 v0, p0

    move/from16 v1, v24

    move/from16 v2, v22

    move-object/from16 v3, v18

    invoke-direct {v0, v13, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getWeekOfYear(IIZLjava/util/Calendar;)I

    move-result v12

    .line 948
    const/4 v14, 0x1

    .local v14, "column":I
    :goto_4
    const/16 v4, 0x8

    if-ge v14, v4, :cond_7

    .line 949
    if-nez v13, :cond_1

    if-eqz v14, :cond_1

    const/4 v4, 0x7

    if-eq v14, v4, :cond_1

    .line 951
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    mul-int/2addr v4, v14

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    add-int/2addr v4, v5

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int/2addr v4, v5

    int-to-float v5, v4

    const/4 v6, 0x0

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    mul-int/2addr v4, v14

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    add-int/2addr v4, v7

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int/2addr v4, v7

    int-to-float v7, v4

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 955
    :cond_1
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getTitleEventNumber(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    .line 957
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    move-object/from16 v16, v0

    move-object/from16 v10, p0

    move-object/from16 v15, p1

    invoke-direct/range {v10 .. v17}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawBox(IIIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;)V

    .line 959
    add-int/lit8 v11, v11, 0x1

    .line 948
    add-int/lit8 v14, v14, 0x1

    goto :goto_4

    .line 908
    .end local v9    # "mPaintDrawLine":Landroid/graphics/Paint;
    .end local v13    # "row":I
    .end local v14    # "column":I
    .end local v20    # "firstDayOfWeek":I
    .end local v21    # "maxDay":I
    .end local v22    # "noPrevMonth":Z
    .end local v23    # "res":Landroid/content/res/Resources;
    .end local v24    # "weekColumn":I
    :cond_2
    const/16 v22, 0x0

    goto/16 :goto_0

    .line 923
    .restart local v20    # "firstDayOfWeek":I
    .restart local v21    # "maxDay":I
    .restart local v22    # "noPrevMonth":Z
    .restart local v23    # "res":Landroid/content/res/Resources;
    .restart local v24    # "weekColumn":I
    :cond_3
    const v4, 0x7f090014

    move-object/from16 v0, v23

    invoke-virtual {v0, v4}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v4

    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    .line 924
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow6:I

    sput v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    goto/16 :goto_1

    .line 931
    :cond_4
    const/4 v4, 0x1

    move/from16 v0, v20

    if-ne v0, v4, :cond_5

    .line 932
    const/4 v4, 0x3

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    goto/16 :goto_2

    .line 933
    :cond_5
    const/4 v4, 0x2

    move/from16 v0, v20

    if-ne v0, v4, :cond_6

    .line 934
    const/4 v4, 0x4

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    goto/16 :goto_2

    .line 935
    :cond_6
    const/4 v4, 0x7

    move/from16 v0, v20

    if-ne v0, v4, :cond_0

    .line 936
    const/4 v4, 0x2

    move-object/from16 v0, v18

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->setMinimalDaysInFirstWeek(I)V

    goto/16 :goto_2

    .line 962
    .restart local v9    # "mPaintDrawLine":Landroid/graphics/Paint;
    .restart local v13    # "row":I
    .restart local v14    # "column":I
    :cond_7
    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    add-int/lit8 v4, v4, -0x1

    if-ge v13, v4, :cond_8

    .line 964
    const/4 v5, 0x0

    add-int/lit8 v4, v13, 0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int/2addr v4, v6

    int-to-float v6, v4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getWidth()I

    move-result v4

    int-to-float v7, v4

    add-int/lit8 v4, v13, 0x1

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int/2addr v4, v8

    int-to-float v8, v4

    move-object/from16 v4, p1

    invoke-virtual/range {v4 .. v9}, Landroid/graphics/Canvas;->drawLine(FFFFLandroid/graphics/Paint;)V

    .line 945
    :cond_8
    add-int/lit8 v13, v13, 0x1

    goto/16 :goto_3

    .line 967
    .end local v14    # "column":I
    :cond_9
    return-void
.end method

.method private doDrawText(Landroid/graphics/Canvas;)V
    .locals 11
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v0, 0x1

    .line 2057
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v10, 0x2

    if-ne v5, v10, :cond_0

    move v8, v0

    .line 2059
    .local v8, "isLandscape":Z
    :goto_0
    new-instance v6, Landroid/graphics/Paint;

    invoke-direct {v6}, Landroid/graphics/Paint;-><init>()V

    .line 2060
    .local v6, "p":Landroid/graphics/Paint;
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRect:Landroid/graphics/Rect;

    .line 2061
    .local v7, "r":Landroid/graphics/Rect;
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v5, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v9

    .line 2064
    .local v9, "columnDay1":I
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v0, v9

    add-int/lit8 v1, v0, 0x1

    .line 2066
    .local v1, "day":I
    const/4 v2, 0x0

    .line 2068
    .local v2, "weekNum":I
    const/4 v3, 0x0

    .local v3, "row":I
    :goto_1
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    if-ge v3, v0, :cond_2

    .line 2069
    const/4 v4, 0x1

    .local v4, "column":I
    :goto_2
    const/16 v0, 0x8

    if-ge v4, v0, :cond_1

    .line 2071
    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getTitleEventNumber(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    move-object v0, p0

    move-object v5, p1

    .line 2072
    invoke-direct/range {v0 .. v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawText(IIIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;Z)V

    .line 2074
    add-int/lit8 v1, v1, 0x1

    .line 2069
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 2057
    .end local v1    # "day":I
    .end local v2    # "weekNum":I
    .end local v3    # "row":I
    .end local v4    # "column":I
    .end local v6    # "p":Landroid/graphics/Paint;
    .end local v7    # "r":Landroid/graphics/Rect;
    .end local v8    # "isLandscape":Z
    .end local v9    # "columnDay1":I
    :cond_0
    const/4 v8, 0x0

    goto :goto_0

    .line 2068
    .restart local v1    # "day":I
    .restart local v2    # "weekNum":I
    .restart local v3    # "row":I
    .restart local v4    # "column":I
    .restart local v6    # "p":Landroid/graphics/Paint;
    .restart local v7    # "r":Landroid/graphics/Rect;
    .restart local v8    # "isLandscape":Z
    .restart local v9    # "columnDay1":I
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 2078
    .end local v4    # "column":I
    :cond_2
    return-void
.end method

.method private drawBox(IIIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;)V
    .locals 34
    .param p1, "day"    # I
    .param p2, "weekNum"    # I
    .param p3, "row"    # I
    .param p4, "column"    # I
    .param p5, "canvas"    # Landroid/graphics/Canvas;
    .param p6, "p"    # Landroid/graphics/Paint;
    .param p7, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1003
    const/16 v19, 0x0

    .line 1004
    .local v19, "drawSelection":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v8

    .line 1005
    .local v8, "withinCurrentMonth":Z
    const/16 v16, 0x0

    .line 1007
    .local v16, "isToday":Z
    const/16 v27, -0x1

    .line 1009
    .local v27, "idxFestivalEffect":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_0

    .line 1010
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isSelected(II)Z

    move-result v19

    .line 1013
    :cond_0
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->sCurrentTime:Landroid/text/format/Time;

    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 1014
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v4

    if-eqz v4, :cond_7

    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->sCurrentTime:Landroid/text/format/Time;

    iget-wide v0, v4, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v28, v0

    .line 1017
    .local v28, "gmtoff":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    move-wide/from16 v0, v28

    invoke-static {v4, v5, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v31

    .line 1018
    .local v31, "todayJulianDay":I
    move/from16 v0, v31

    move/from16 v1, p1

    if-ne v0, v1, :cond_1

    if-eqz p4, :cond_1

    .line 1019
    const/16 v16, 0x1

    .line 1022
    :cond_1
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int v4, v4, p3

    add-int/lit8 v33, v4, 0x1

    .line 1025
    .local v33, "y":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    add-int/lit8 v5, p4, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    mul-int/2addr v5, v6

    add-int v32, v4, v5

    .line 1026
    .local v32, "x":I
    const/4 v4, 0x1

    move/from16 v0, p4

    if-le v0, v4, :cond_2

    .line 1027
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int v32, v32, v4

    .line 1030
    :cond_2
    move/from16 v0, v32

    move-object/from16 v1, p7

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1031
    move/from16 v0, v33

    move-object/from16 v1, p7

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1032
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    add-int v4, v4, v32

    move-object/from16 v0, p7

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 1033
    const/4 v4, 0x1

    move/from16 v0, p4

    if-eq v0, v4, :cond_3

    const/4 v4, 0x7

    move/from16 v0, p4

    if-ne v0, v4, :cond_4

    .line 1034
    :cond_3
    move-object/from16 v0, p7

    iget v4, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int/2addr v4, v5

    move-object/from16 v0, p7

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 1037
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    add-int v4, v4, v33

    move-object/from16 v0, p7

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 1039
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedRow:I

    move/from16 v0, p3

    if-ne v4, v0, :cond_5

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedCol:I

    move/from16 v0, p4

    if-ne v4, v0, :cond_5

    if-nez v19, :cond_5

    .line 1042
    const/16 v19, 0x1

    :cond_5
    move-object/from16 v4, p0

    move/from16 v5, p1

    move/from16 v6, p4

    move/from16 v7, p3

    move-object/from16 v9, p7

    move-object/from16 v10, p5

    .line 1044
    invoke-direct/range {v4 .. v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawWallpaperSticker(IIIZLandroid/graphics/Rect;Landroid/graphics/Canvas;)V

    .line 1045
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-eqz v4, :cond_9

    .line 1046
    new-instance v30, Landroid/text/format/Time;

    invoke-direct/range {v30 .. v30}, Landroid/text/format/Time;-><init>()V

    .line 1047
    .local v30, "time":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v6

    move-object/from16 v0, v30

    invoke-virtual {v0, v4, v5, v6}, Landroid/text/format/Time;->set(III)V

    .line 1048
    move-object/from16 v0, p0

    move-object/from16 v1, v30

    move/from16 v2, p3

    invoke-direct {v0, v1, v8, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getCalculatedDate(Landroid/text/format/Time;ZI)V

    .line 1050
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->getInstance()Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v30

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->isFestivalEffectDay(Landroid/content/ContentResolver;Landroid/text/format/Time;)I

    move-result v10

    .line 1051
    .end local v27    # "idxFestivalEffect":I
    .local v10, "idxFestivalEffect":I
    const/4 v11, 0x1

    move-object/from16 v4, p0

    move-object/from16 v5, p5

    move/from16 v6, p4

    move/from16 v7, p3

    move-object/from16 v9, p7

    invoke-direct/range {v4 .. v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawFestivalEffectBorder(Landroid/graphics/Canvas;IIZLandroid/graphics/Rect;IZ)V

    .end local v30    # "time":Landroid/text/format/Time;
    :goto_1
    move-object/from16 v11, p0

    move/from16 v12, p1

    move-object/from16 v13, p5

    move-object/from16 v14, p7

    move-object/from16 v15, p6

    move/from16 v17, p3

    move/from16 v18, p4

    .line 1053
    invoke-direct/range {v11 .. v19}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawEvents(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;ZIIZ)V

    move-object/from16 v18, p0

    move/from16 v20, p4

    move/from16 v21, p3

    move/from16 v22, v8

    move-object/from16 v23, p7

    move-object/from16 v24, p5

    move/from16 v25, v16

    .line 1054
    invoke-direct/range {v18 .. v25}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawTodayMarker(ZIIZLandroid/graphics/Rect;Landroid/graphics/Canvas;Z)V

    .line 1055
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-eqz v4, :cond_6

    .line 1056
    const/4 v11, 0x0

    move-object/from16 v4, p0

    move-object/from16 v5, p5

    move/from16 v6, p4

    move/from16 v7, p3

    move-object/from16 v9, p7

    invoke-direct/range {v4 .. v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawFestivalEffectBorder(Landroid/graphics/Canvas;IIZLandroid/graphics/Rect;IZ)V

    .line 1058
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-eqz v4, :cond_8

    const/4 v4, 0x2

    if-ne v10, v4, :cond_8

    :goto_2
    move-object/from16 v20, p0

    move/from16 v21, p1

    move/from16 v22, p3

    move/from16 v23, p4

    move-object/from16 v24, p5

    move-object/from16 v25, p6

    move-object/from16 v26, p7

    .line 1063
    invoke-direct/range {v20 .. v26}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawWeather(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;)V

    .line 1064
    return-void

    .line 1014
    .end local v10    # "idxFestivalEffect":I
    .end local v28    # "gmtoff":J
    .end local v31    # "todayJulianDay":I
    .end local v32    # "x":I
    .end local v33    # "y":I
    .restart local v27    # "idxFestivalEffect":I
    :cond_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-wide v0, v4, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v28, v0

    goto/16 :goto_0

    .end local v27    # "idxFestivalEffect":I
    .restart local v10    # "idxFestivalEffect":I
    .restart local v28    # "gmtoff":J
    .restart local v31    # "todayJulianDay":I
    .restart local v32    # "x":I
    .restart local v33    # "y":I
    :cond_8
    move-object/from16 v20, p0

    move-object/from16 v21, p5

    move/from16 v22, p1

    move/from16 v23, p3

    move-object/from16 v24, p7

    move/from16 v25, v8

    .line 1061
    invoke-direct/range {v20 .. v25}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawStickers(Landroid/graphics/Canvas;IILandroid/graphics/Rect;Z)V

    goto :goto_2

    .end local v10    # "idxFestivalEffect":I
    .restart local v27    # "idxFestivalEffect":I
    :cond_9
    move/from16 v10, v27

    .end local v27    # "idxFestivalEffect":I
    .restart local v10    # "idxFestivalEffect":I
    goto :goto_1
.end method

.method private drawDayNumber(Landroid/graphics/Canvas;IIZZIZ)V
    .locals 26
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "row"    # I
    .param p3, "column"    # I
    .param p4, "withinCurrentMonth"    # Z
    .param p5, "drawSelection"    # Z
    .param p6, "day"    # I
    .param p7, "isToday"    # Z

    .prologue
    .line 1183
    const/4 v4, 0x1

    move/from16 v0, p3

    if-ne v0, v4, :cond_d

    const/4 v4, 0x1

    :goto_0
    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isWeekFirstColumn:Z

    .line 1185
    if-eqz p3, :cond_c

    .line 1186
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1187
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1188
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isFontStyleDefault:Z

    if-eqz v4, :cond_e

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfLight:Landroid/graphics/Typeface;

    if-eqz v4, :cond_e

    .line 1189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1193
    :goto_1
    if-eqz p4, :cond_f

    .line 1194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextSize:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1199
    :goto_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isValid(II)Z

    move-result v4

    if-nez v4, :cond_10

    .line 1200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWeekNumColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1237
    :cond_0
    :goto_3
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLocalHolidayColorDisplay:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAreNationalHolidaysSupported:Z

    if-eqz v4, :cond_5

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isValid(II)Z

    move-result v4

    if-eqz v4, :cond_5

    if-nez p7, :cond_5

    .line 1239
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v6

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v7

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v8

    move-object/from16 v4, p0

    move/from16 v5, p2

    move/from16 v9, p4

    invoke-direct/range {v4 .. v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getCurrentTime(IIIIZ)Landroid/text/format/Time;

    move-result-object v19

    .line 1242
    .local v19, "tempTime":Landroid/text/format/Time;
    const/4 v12, 0x0

    .line 1244
    .local v12, "isHoliday":Z
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    if-eqz v4, :cond_2

    .line 1245
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isHoliday(Landroid/text/format/Time;)Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDisplaySubstHolidays:Z

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isSubstHoliday(Landroid/text/format/Time;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_2

    .line 1247
    :cond_1
    const/4 v12, 0x1

    .line 1251
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHolidayManager:Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

    if-eqz v4, :cond_4

    .line 1252
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHolidayManager:Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->checkJapaneseHoliday(Landroid/text/format/Time;)Z

    move-result v4

    const/4 v5, 0x1

    if-eq v4, v5, :cond_3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHolidayManager:Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;->checkAlternateJapaneseHoliday(Landroid/text/format/Time;)Z

    move-result v4

    const/4 v5, 0x1

    if-ne v4, v5, :cond_4

    .line 1254
    :cond_3
    const/4 v12, 0x1

    .line 1258
    :cond_4
    const/4 v4, 0x1

    if-ne v12, v4, :cond_5

    .line 1259
    if-eqz p4, :cond_1a

    .line 1260
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1265
    :goto_4
    if-nez p4, :cond_5

    .line 1266
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getColor()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1271
    .end local v12    # "isHoliday":Z
    .end local v19    # "tempTime":Landroid/text/format/Time;
    :cond_5
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-eqz v4, :cond_6

    .line 1272
    new-instance v22, Landroid/text/format/Time;

    invoke-direct/range {v22 .. v22}, Landroid/text/format/Time;-><init>()V

    .line 1273
    .local v22, "time":Landroid/text/format/Time;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v6

    move-object/from16 v0, v22

    invoke-virtual {v0, v4, v5, v6}, Landroid/text/format/Time;->set(III)V

    .line 1274
    move-object/from16 v0, p0

    move-object/from16 v1, v22

    move/from16 v2, p4

    move/from16 v3, p2

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getCalculatedDate(Landroid/text/format/Time;ZI)V

    .line 1276
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->getInstance()Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-virtual {v5}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    move-object/from16 v0, v22

    invoke-virtual {v4, v5, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->isFestivalEffectDay(Landroid/content/ContentResolver;Landroid/text/format/Time;)I

    move-result v11

    .line 1277
    .local v11, "idxFestivalEffect":I
    if-eqz p7, :cond_1b

    .line 1278
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTodayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1286
    .end local v11    # "idxFestivalEffect":I
    .end local v22    # "time":Landroid/text/format/Time;
    :cond_6
    :goto_5
    new-instance v18, Landroid/graphics/Rect;

    invoke-direct/range {v18 .. v18}, Landroid/graphics/Rect;-><init>()V

    .line 1288
    .local v18, "r":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int v4, v4, p2

    add-int/lit8 v24, v4, 0x1

    .line 1291
    .local v24, "y":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    add-int/lit8 v5, p3, -0x1

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    mul-int/2addr v5, v6

    add-int v23, v4, v5

    .line 1292
    .local v23, "x":I
    const/4 v4, 0x1

    move/from16 v0, p3

    if-le v0, v4, :cond_7

    .line 1293
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int v23, v23, v4

    .line 1295
    :cond_7
    move/from16 v0, v23

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 1296
    move/from16 v0, v24

    move-object/from16 v1, v18

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 1297
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    add-int v4, v4, v23

    move-object/from16 v0, v18

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 1298
    const/4 v4, 0x1

    move/from16 v0, p3

    if-eq v0, v4, :cond_8

    const/4 v4, 0x7

    move/from16 v0, p3

    if-ne v0, v4, :cond_9

    .line 1299
    :cond_8
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int/2addr v4, v5

    move-object/from16 v0, v18

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 1302
    :cond_9
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    add-int v4, v4, v24

    move-object/from16 v0, v18

    iput v4, v0, Landroid/graphics/Rect;->bottom:I

    .line 1304
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    sget-object v5, Landroid/graphics/Paint$Align;->CENTER:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1305
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextPaddingLeft:I

    add-int v20, v4, v5

    .line 1306
    .local v20, "textX":I
    move-object/from16 v0, v18

    iget v4, v0, Landroid/graphics/Rect;->top:I

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextPaddingTop:I

    add-int v21, v4, v5

    .line 1307
    .local v21, "textY":I
    const-string v4, "ar"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v5

    invoke-virtual {v5}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1f

    .line 1308
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    const-string v5, "%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v8, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v4, v5, v6}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v20

    int-to-float v5, v0

    move/from16 v0, v21

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1315
    :goto_6
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLunarDateDisplaySupported:Z

    if-eqz v4, :cond_c

    if-nez p5, :cond_a

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isWeekFirstColumn:Z

    if-eqz v4, :cond_c

    .line 1316
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v25

    .line 1317
    .local v25, "year":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v17

    .line 1318
    .local v17, "month":I
    const/16 v13, 0x19

    .line 1320
    .local v13, "leapAttrOffset":I
    if-nez p4, :cond_b

    .line 1321
    if-nez p2, :cond_21

    .line 1322
    if-nez v17, :cond_20

    .line 1323
    add-int/lit8 v25, v25, -0x1

    .line 1324
    const/16 v17, 0xb

    .line 1338
    :cond_b
    :goto_7
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v5, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v5

    move/from16 v0, v25

    move/from16 v1, v17

    invoke-virtual {v4, v0, v1, v5}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->convertSolarToLunar(III)V

    .line 1341
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    invoke-virtual {v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->isLeapMonth()Z

    move-result v4

    if-eqz v4, :cond_23

    .line 1342
    new-instance v4, Ljava/lang/StringBuilder;

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarLeapString:Ljava/lang/String;

    invoke-direct {v4, v5}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    const-string v5, " "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    invoke-virtual {v5}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getStringValue()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v14

    .line 1348
    .local v14, "lunarDateString":Ljava/lang/String;
    :goto_8
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextPaddingLeft:I

    add-int v4, v4, v23

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    invoke-virtual {v5, v14}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v5

    float-to-int v5, v5

    div-int/lit8 v5, v5, 0x2

    sub-int v15, v4, v5

    .line 1349
    .local v15, "lunarDateX":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDateYOffset:I

    add-int/lit8 v16, v4, 0x32

    .line 1351
    .local v16, "lunarDateY":I
    int-to-float v4, v15

    move-object/from16 v0, v18

    iget v5, v0, Landroid/graphics/Rect;->top:I

    add-int v5, v5, v16

    int-to-float v5, v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v14, v4, v5, v6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 1354
    .end local v13    # "leapAttrOffset":I
    .end local v14    # "lunarDateString":Ljava/lang/String;
    .end local v15    # "lunarDateX":I
    .end local v16    # "lunarDateY":I
    .end local v17    # "month":I
    .end local v18    # "r":Landroid/graphics/Rect;
    .end local v20    # "textX":I
    .end local v21    # "textY":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    .end local v25    # "year":I
    :cond_c
    return-void

    .line 1183
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_0

    .line 1191
    :cond_e
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    const/4 v5, 0x0

    invoke-static {v5}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_1

    .line 1196
    :cond_f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPrevNextDayTextSize:I

    int-to-float v5, v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setTextSize(F)V

    goto/16 :goto_2

    .line 1202
    :cond_10
    if-eqz p5, :cond_11

    if-eqz p7, :cond_11

    .line 1203
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTodayFocusedNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_3

    .line 1205
    :cond_11
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    add-int v4, v4, p3

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v4, v4, -0x1

    rem-int/lit8 v4, v4, 0x7

    add-int/lit8 v10, v4, 0x1

    .line 1206
    .local v10, "dayOfWeek":I
    if-eqz p4, :cond_16

    .line 1207
    const/4 v4, 0x1

    if-ne v10, v4, :cond_13

    .line 1208
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1227
    :goto_9
    if-eqz p7, :cond_12

    .line 1228
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTodayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    .line 1231
    :cond_12
    if-nez p4, :cond_0

    .line 1232
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    invoke-virtual {v5}, Landroid/graphics/Paint;->getColor()I

    move-result v5

    move-object/from16 v0, p0

    invoke-direct {v0, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_3

    .line 1209
    :cond_13
    const/4 v4, 0x7

    if-ne v10, v4, :cond_14

    .line 1210
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1211
    :cond_14
    const/4 v4, 0x6

    if-ne v10, v4, :cond_15

    .line 1212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthFridayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1214
    :cond_15
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1217
    :cond_16
    const/4 v4, 0x1

    if-ne v10, v4, :cond_17

    .line 1218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1219
    :cond_17
    const/4 v4, 0x7

    if-ne v10, v4, :cond_18

    .line 1220
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberDimColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1221
    :cond_18
    const/4 v4, 0x6

    if-ne v10, v4, :cond_19

    .line 1222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthFridayNumberDimColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_9

    .line 1224
    :cond_19
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberDimColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_9

    .line 1262
    .end local v10    # "dayOfWeek":I
    .restart local v12    # "isHoliday":Z
    .restart local v19    # "tempTime":Landroid/text/format/Time;
    :cond_1a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    invoke-virtual {v4, v5}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_4

    .line 1279
    .end local v12    # "isHoliday":Z
    .end local v19    # "tempTime":Landroid/text/format/Time;
    .restart local v11    # "idxFestivalEffect":I
    .restart local v22    # "time":Landroid/text/format/Time;
    :cond_1b
    const/4 v4, 0x2

    if-ne v11, v4, :cond_1d

    .line 1280
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    if-eqz p4, :cond_1c

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberColor:I

    :goto_a
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_5

    :cond_1c
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberDimColor:I

    goto :goto_a

    .line 1281
    :cond_1d
    const/4 v4, 0x1

    if-ne v11, v4, :cond_6

    .line 1282
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    if-eqz p4, :cond_1e

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    :goto_b
    invoke-virtual {v5, v4}, Landroid/graphics/Paint;->setColor(I)V

    goto/16 :goto_5

    :cond_1e
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    goto :goto_b

    .line 1311
    .end local v11    # "idxFestivalEffect":I
    .end local v22    # "time":Landroid/text/format/Time;
    .restart local v18    # "r":Landroid/graphics/Rect;
    .restart local v20    # "textX":I
    .restart local v21    # "textY":I
    .restart local v23    # "x":I
    .restart local v24    # "y":I
    :cond_1f
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v4

    invoke-static {v4}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v4

    move/from16 v0, v20

    int-to-float v5, v0

    move/from16 v0, v21

    int-to-float v6, v0

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayNumPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p1

    invoke-virtual {v0, v4, v5, v6, v7}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_6

    .line 1326
    .restart local v13    # "leapAttrOffset":I
    .restart local v17    # "month":I
    .restart local v25    # "year":I
    :cond_20
    add-int/lit8 v17, v17, -0x1

    goto/16 :goto_7

    .line 1329
    :cond_21
    const/16 v4, 0xb

    move/from16 v0, v17

    if-ne v0, v4, :cond_22

    .line 1330
    add-int/lit8 v25, v25, 0x1

    .line 1331
    const/16 v17, 0x0

    goto/16 :goto_7

    .line 1333
    :cond_22
    add-int/lit8 v17, v17, 0x1

    goto/16 :goto_7

    .line 1345
    :cond_23
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    invoke-virtual {v4}, Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;->getStringValue()Ljava/lang/String;

    move-result-object v14

    .restart local v14    # "lunarDateString":Ljava/lang/String;
    goto/16 :goto_8
.end method

.method private drawEventCount(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;II)V
    .locals 9
    .param p1, "day"    # I
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "p"    # Landroid/graphics/Paint;
    .param p5, "row"    # I
    .param p6, "column"    # I

    .prologue
    .line 2381
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    aget v3, v3, p1

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-gt v3, v4, :cond_0

    .line 2424
    :goto_0
    return-void

    .line 2385
    :cond_0
    const/4 v3, 0x1

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2386
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayCountTextSize:I

    int-to-float v3, v3

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2388
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isFontStyleDefault:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfLight:Landroid/graphics/Typeface;

    if-eqz v3, :cond_1

    .line 2389
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2394
    :goto_1
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-nez v3, :cond_2

    const-string v3, ""

    :goto_2
    invoke-virtual {v4, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "%d"

    const/4 v5, 0x1

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    aget v7, v7, p1

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    sub-int/2addr v7, v8

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v4, v5}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    .line 2395
    .local v2, "moreCount":Ljava/lang/String;
    iget v3, p3, Landroid/graphics/Rect;->top:I

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountTextMarginTop:I

    add-int v1, v3, v4

    .line 2396
    .local v1, "cntY":I
    iget v3, p3, Landroid/graphics/Rect;->right:I

    invoke-virtual {p4, v2}, Landroid/graphics/Paint;->measureText(Ljava/lang/String;)F

    move-result v4

    float-to-int v4, v4

    sub-int/2addr v3, v4

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountTextMarginRight:I

    sub-int v0, v3, v4

    .line 2399
    .local v0, "cntX":I
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountBGColor:I

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2400
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3, p5, p6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v3

    if-nez v3, :cond_3

    .line 2401
    const/16 v3, 0x99

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2406
    :goto_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountTextMarginRight:I

    sub-int v4, v0, v4

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->left:F

    .line 2407
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    iget v4, p3, Landroid/graphics/Rect;->top:I

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->top:F

    .line 2408
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    iget v4, p3, Landroid/graphics/Rect;->right:I

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->right:F

    .line 2409
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    iget v4, p3, Landroid/graphics/Rect;->top:I

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountBGHeight:I

    add-int/2addr v4, v5

    int-to-float v4, v4

    iput v4, v3, Landroid/graphics/RectF;->bottom:F

    .line 2410
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL_AND_STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2411
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMoreEventCountRect:Landroid/graphics/RectF;

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountBGOvalRadius:I

    int-to-float v4, v4

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountBGOvalRadius:I

    int-to-float v5, v5

    invoke-virtual {p2, v3, v4, v5, p4}, Landroid/graphics/Canvas;->drawRoundRect(Landroid/graphics/RectF;FFLandroid/graphics/Paint;)V

    .line 2413
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    sget-object v4, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 2414
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3, p5, p6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v3

    if-nez v3, :cond_4

    .line 2415
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountTextColor:I

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v3

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2416
    const/16 v3, 0x99

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 2422
    :goto_4
    int-to-float v3, v0

    int-to-float v4, v1

    invoke-virtual {p2, v2, v3, v4, p4}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2423
    const/16 v3, 0xff

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    goto/16 :goto_0

    .line 2391
    .end local v0    # "cntX":I
    .end local v1    # "cntY":I
    .end local v2    # "moreCount":Ljava/lang/String;
    :cond_1
    const/4 v3, 0x0

    invoke-static {v3}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v3

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_1

    .line 2394
    :cond_2
    const-string v3, "+"

    goto/16 :goto_2

    .line 2403
    .restart local v0    # "cntX":I
    .restart local v1    # "cntY":I
    .restart local v2    # "moreCount":Ljava/lang/String;
    :cond_3
    const/16 v3, 0xff

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_3

    .line 2419
    :cond_4
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountTextColor:I

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 2420
    const/16 v3, 0xff

    invoke-virtual {p4, v3}, Landroid/graphics/Paint;->setAlpha(I)V

    goto :goto_4
.end method

.method private drawEventMemo(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V
    .locals 4
    .param p1, "day"    # I
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "p"    # Landroid/graphics/Paint;

    .prologue
    .line 2348
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    const/4 v3, -0x1

    if-ne v2, v3, :cond_1

    .line 2364
    :cond_0
    :goto_0
    return-void

    .line 2351
    :cond_1
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 2352
    .local v1, "r":Landroid/graphics/Rect;
    invoke-virtual {v1, p3}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 2354
    iget v2, v1, Landroid/graphics/Rect;->left:I

    add-int/lit8 v2, v2, 0x1

    iput v2, v1, Landroid/graphics/Rect;->left:I

    .line 2356
    iget v2, v1, Landroid/graphics/Rect;->top:I

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAttachMemo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 2357
    iget v2, v1, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAttachMemo:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v3}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 2359
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    aget-object v2, v2, p1

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    if-ltz v2, :cond_0

    .line 2360
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAttachMemo:Landroid/graphics/drawable/Drawable;

    .line 2361
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v0, v1}, Landroid/graphics/drawable/Drawable;->setBounds(Landroid/graphics/Rect;)V

    .line 2362
    invoke-virtual {v0, p2}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0
.end method

.method private drawEvents(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;ZIIZ)V
    .locals 23
    .param p1, "date"    # I
    .param p2, "canvas"    # Landroid/graphics/Canvas;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "p"    # Landroid/graphics/Paint;
    .param p5, "isToday"    # Z
    .param p6, "row"    # I
    .param p7, "column"    # I
    .param p8, "drawSelection"    # Z

    .prologue
    .line 1389
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    const/4 v6, 0x1

    invoke-virtual {v4, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v12

    .line 1390
    .local v12, "columnDay1":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v4, v12

    add-int/lit8 v18, v4, 0x1

    .line 1392
    .local v18, "monthStartDay":I
    const/4 v13, 0x0

    .line 1393
    .local v13, "eventAccountColor":I
    sub-int v5, p1, v18

    .local v5, "day":I
    move-object/from16 v4, p0

    move-object/from16 v6, p2

    move-object/from16 v7, p3

    move-object/from16 v8, p4

    move/from16 v9, p6

    move/from16 v10, p7

    .line 1395
    invoke-direct/range {v4 .. v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawEventCount(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;II)V

    .line 1396
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move-object/from16 v2, p3

    move-object/from16 v3, p4

    invoke-direct {v0, v5, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawEventMemo(ILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1398
    const/4 v15, 0x0

    .local v15, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-ge v15, v4, :cond_e

    .line 1401
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    if-lez v4, :cond_2

    .line 1402
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventColorArray:[[I

    aget-object v4, v4, v5

    aget v13, v4, v15

    .line 1405
    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v16, v4, 0x1

    .line 1406
    .local v16, "leftP":I
    move-object/from16 v0, p3

    iget v0, v0, Landroid/graphics/Rect;->right:I

    move/from16 v21, v0

    .line 1407
    .local v21, "rightP":I
    move-object/from16 v0, p3

    iget v4, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p3 .. p3}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v4, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIntervalBetweenEventBox:I

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    sub-int/2addr v7, v15

    mul-int/2addr v6, v7

    sub-int/2addr v4, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    sub-int v22, v4, v6

    .line 1410
    .local v22, "topP":I
    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    add-int v11, v22, v4

    .line 1412
    .local v11, "bottomP":I
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    if-eqz v4, :cond_b

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v6, 0x1

    if-le v4, v6, :cond_b

    .line 1413
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {v4, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v14

    .line 1415
    .local v14, "eventHeader":Ljava/lang/String;
    const-string v4, "|R|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    const-string v4, "|P|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_4

    .line 1416
    :cond_0
    new-instance v20, Landroid/graphics/Rect;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v4, v4, v16

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v6, v6, v22

    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    sub-int v7, v11, v7

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v4, v6, v1, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1420
    .local v20, "rectInside":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsTabletConfig:Z

    if-nez v4, :cond_3

    .line 1421
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1422
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDisplayColorFromColorForXhdpi(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1429
    :goto_1
    const-string v4, "|P|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1430
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1431
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1437
    :cond_1
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1398
    .end local v11    # "bottomP":I
    .end local v14    # "eventHeader":Ljava/lang/String;
    .end local v16    # "leftP":I
    .end local v20    # "rectInside":Landroid/graphics/Rect;
    .end local v21    # "rightP":I
    .end local v22    # "topP":I
    :cond_2
    :goto_2
    add-int/lit8 v15, v15, 0x1

    goto/16 :goto_0

    .line 1425
    .restart local v11    # "bottomP":I
    .restart local v14    # "eventHeader":Ljava/lang/String;
    .restart local v16    # "leftP":I
    .restart local v20    # "rectInside":Landroid/graphics/Rect;
    .restart local v21    # "rightP":I
    .restart local v22    # "topP":I
    :cond_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1426
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getFillColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_1

    .line 1440
    .end local v20    # "rectInside":Landroid/graphics/Rect;
    :cond_4
    const-string v4, "|A|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_7

    .line 1441
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    const/4 v6, 0x1

    invoke-static {v6}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 1442
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleTextSize:I

    int-to-float v6, v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 1443
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    sget-object v6, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 1444
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    const/4 v6, 0x0

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setStrikeThruText(Z)V

    .line 1445
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxPaint:Landroid/graphics/Paint;

    invoke-virtual {v4, v6}, Landroid/text/TextPaint;->set(Landroid/graphics/Paint;)V

    .line 1446
    new-instance v20, Landroid/graphics/Rect;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v4, v4, v16

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v6, v6, v22

    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    sub-int v7, v21, v7

    sget v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    sub-int v8, v11, v8

    move-object/from16 v0, v20

    invoke-direct {v0, v4, v6, v7, v8}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1450
    .restart local v20    # "rectInside":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsTabletConfig:Z

    if-nez v4, :cond_6

    .line 1451
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1452
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDisplayColorFromColorForXhdpi(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1459
    :goto_3
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v4

    if-nez v4, :cond_5

    .line 1460
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1461
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1467
    :cond_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1455
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1456
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getFillColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_3

    .line 1468
    .end local v20    # "rectInside":Landroid/graphics/Rect;
    :cond_7
    const-string v4, "|S|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_8

    const-string v4, "|T|"

    invoke-virtual {v4, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_2

    .line 1470
    :cond_8
    new-instance v19, Landroid/graphics/Paint;

    invoke-direct/range {v19 .. v19}, Landroid/graphics/Paint;-><init>()V

    .line 1472
    .local v19, "paint":Landroid/graphics/Paint;
    new-instance v17, Landroid/graphics/Rect;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v4, v4, v16

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v6, v6, v16

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAccountColorBarWidth:I

    add-int/2addr v6, v7

    move-object/from16 v0, v17

    move/from16 v1, v22

    invoke-direct {v0, v4, v1, v6, v11}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1475
    .local v17, "localRect":Landroid/graphics/Rect;
    move-object/from16 v0, v19

    invoke-virtual {v0, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1476
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v4

    if-nez v4, :cond_9

    .line 1477
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1479
    :cond_9
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1481
    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v4, v4, v16

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAccountColorBarWidth:I

    add-int/2addr v4, v6

    move-object/from16 v0, v17

    iput v4, v0, Landroid/graphics/Rect;->left:I

    .line 1482
    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    sub-int v4, v21, v4

    move-object/from16 v0, v17

    iput v4, v0, Landroid/graphics/Rect;->right:I

    .line 1483
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p6

    move/from16 v1, p7

    invoke-virtual {v4, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v4

    if-nez v4, :cond_a

    .line 1484
    invoke-virtual/range {v19 .. v19}, Landroid/graphics/Paint;->getColor()I

    move-result v4

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v4

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setColor(I)V

    .line 1486
    :cond_a
    const/16 v4, 0x33

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1487
    move-object/from16 v0, p2

    move-object/from16 v1, v17

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1490
    .end local v14    # "eventHeader":Ljava/lang/String;
    .end local v17    # "localRect":Landroid/graphics/Rect;
    .end local v19    # "paint":Landroid/graphics/Paint;
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    if-eqz v4, :cond_2

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    invoke-virtual {v4}, Ljava/lang/String;->length()I

    move-result v4

    const/4 v6, 0x1

    if-ne v4, v6, :cond_2

    .line 1492
    new-instance v20, Landroid/graphics/Rect;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->GRID_LINE_STROKE_WIDTH:I

    sub-int v4, v16, v4

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    add-int v6, v6, v22

    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->EVENT_BOX_STROKE_WIDTH:I

    sub-int v7, v11, v7

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-direct {v0, v4, v6, v1, v7}, Landroid/graphics/Rect;-><init>(IIII)V

    .line 1496
    .restart local v20    # "rectInside":Landroid/graphics/Rect;
    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsTabletConfig:Z

    if-nez v4, :cond_d

    .line 1497
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1498
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDisplayColorFromColorForXhdpi(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1504
    :goto_4
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v4, v4, v5

    aget-object v4, v4, v15

    const-string v6, "P"

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_c

    .line 1505
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1506
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-virtual {v6}, Landroid/graphics/Paint;->getColor()I

    move-result v6

    move-object/from16 v0, p0

    invoke-direct {v0, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    .line 1511
    :cond_c
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    move-object/from16 v0, p2

    move-object/from16 v1, v20

    invoke-virtual {v0, v1, v4}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    goto/16 :goto_2

    .line 1500
    :cond_d
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintOutside:Landroid/graphics/Paint;

    invoke-virtual {v4, v13}, Landroid/graphics/Paint;->setColor(I)V

    .line 1501
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaintInside:Landroid/graphics/Paint;

    invoke-static {v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getFillColor(I)I

    move-result v6

    invoke-virtual {v4, v6}, Landroid/graphics/Paint;->setColor(I)V

    goto :goto_4

    .line 1515
    .end local v11    # "bottomP":I
    .end local v16    # "leftP":I
    .end local v20    # "rectInside":Landroid/graphics/Rect;
    .end local v21    # "rightP":I
    .end local v22    # "topP":I
    :cond_e
    return-void
.end method

.method private drawEventsTitle(IIILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;ZZ)V
    .locals 26
    .param p1, "date"    # I
    .param p2, "row"    # I
    .param p3, "column"    # I
    .param p4, "canvas"    # Landroid/graphics/Canvas;
    .param p5, "rect"    # Landroid/graphics/Rect;
    .param p6, "p"    # Landroid/graphics/Paint;
    .param p7, "drawSelection"    # Z
    .param p8, "withinCurrentMonth"    # Z

    .prologue
    .line 2129
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    const/4 v6, 0x1

    invoke-virtual {v2, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v14

    .line 2130
    .local v14, "columnDay1":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v2, v14

    add-int/lit8 v23, v2, 0x1

    .line 2132
    .local v23, "monthStartDay":I
    sub-int v15, p1, v23

    .line 2135
    .local v15, "day":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v22

    .line 2136
    .local v22, "lastDayOfMonth":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleTextSize:I

    int-to-float v2, v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 2137
    sget-object v2, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 2138
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isFontStyleDefault:Z

    if-eqz v2, :cond_6

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfRegular:Landroid/graphics/Typeface;

    if-eqz v2, :cond_6

    .line 2139
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->tfRegular:Landroid/graphics/Typeface;

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 2143
    :goto_0
    const/16 v19, 0x0

    .local v19, "i":I
    :goto_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    move/from16 v0, v19

    if-ge v0, v2, :cond_11

    .line 2144
    const/4 v2, 0x0

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setStrikeThruText(Z)V

    .line 2146
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    if-eqz v2, :cond_5

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    invoke-virtual {v2}, Ljava/lang/String;->length()I

    move-result v2

    const/4 v6, 0x1

    if-le v2, v6, :cond_5

    .line 2148
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    const/4 v6, 0x0

    const/4 v7, 0x3

    invoke-virtual {v2, v6, v7}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v18

    .line 2150
    .local v18, "eventHeader":Ljava/lang/String;
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->top:I

    invoke-virtual/range {p5 .. p5}, Landroid/graphics/Rect;->height()I

    move-result v6

    add-int/2addr v2, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    move-object/from16 v0, p0

    iget v7, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIntervalBetweenEventBox:I

    add-int/2addr v6, v7

    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    sub-int v7, v7, v19

    mul-int/2addr v6, v7

    sub-int/2addr v2, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    sub-int v11, v2, v6

    .line 2154
    .local v11, "eventBoxYPosition":I
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->left:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMarginLeft:I

    add-int v4, v2, v6

    .line 2155
    .local v4, "textX":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMargineTop:I

    add-int v5, v11, v2

    .line 2157
    .local v5, "textY":I
    const-string v3, ""

    .line 2159
    .local v3, "msg":Ljava/lang/String;
    const/4 v2, 0x1

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 2160
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventColor:I

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2162
    if-nez p8, :cond_0

    .line 2163
    const-string v2, "|R|"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 2164
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventDimColor:I

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2168
    :cond_0
    const-string v2, "|A|"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    const-string v2, "|S|"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_7

    .line 2170
    :cond_1
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->right:I

    sub-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleAvailAdjust:I

    sub-int/2addr v2, v6

    int-to-float v12, v2

    .line 2171
    .local v12, "avail":F
    const/16 v20, 0x0

    .line 2173
    .local v20, "iconWidth":F
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v2, v6

    move/from16 v0, v19

    if-lt v0, v2, :cond_3

    .line 2174
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    if-eqz v2, :cond_2

    .line 2175
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    int-to-float v2, v2

    add-float v20, v20, v2

    .line 2178
    :cond_2
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasWeatherIcon:Z

    if-eqz v2, :cond_3

    .line 2179
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageWidthSize:I

    int-to-float v2, v2

    add-float v20, v20, v2

    .line 2183
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->set(Landroid/graphics/Paint;)V

    .line 2185
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    const/4 v6, 0x3

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->removeSpace(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 2186
    .local v17, "displayTitle":Ljava/lang/CharSequence;
    sget-object v2, Ljava/text/Normalizer$Form;->NFC:Ljava/text/Normalizer$Form;

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v25

    .line 2188
    .local v25, "vietnamese_laborday":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mVietnameseLaborDayString:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 2189
    move-object/from16 v17, v25

    .line 2191
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    sub-float v6, v12, v20

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v17

    invoke-static {v0, v2, v6, v7}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v24

    .line 2194
    .local v24, "text":Ljava/lang/CharSequence;
    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2254
    .end local v17    # "displayTitle":Ljava/lang/CharSequence;
    .end local v20    # "iconWidth":F
    .end local v25    # "vietnamese_laborday":Ljava/lang/String;
    :goto_2
    const-string v2, "|T|"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_10

    .line 2255
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskComplete:[[Z

    aget-object v2, v2, v15

    aget-boolean v9, v2, v19

    move-object/from16 v2, p0

    move-object/from16 v6, p4

    move/from16 v7, p7

    move-object/from16 v8, p6

    move/from16 v10, p8

    invoke-direct/range {v2 .. v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawTaskEventTitle(Ljava/lang/String;IILandroid/graphics/Canvas;ZLandroid/graphics/Paint;ZZI)V

    .line 2143
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "textX":I
    .end local v5    # "textY":I
    .end local v11    # "eventBoxYPosition":I
    .end local v12    # "avail":F
    .end local v18    # "eventHeader":Ljava/lang/String;
    .end local v24    # "text":Ljava/lang/CharSequence;
    :cond_5
    :goto_3
    add-int/lit8 v19, v19, 0x1

    goto/16 :goto_1

    .line 2141
    .end local v19    # "i":I
    :cond_6
    const/4 v2, 0x0

    invoke-static {v2}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v2

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    goto/16 :goto_0

    .line 2195
    .restart local v3    # "msg":Ljava/lang/String;
    .restart local v4    # "textX":I
    .restart local v5    # "textY":I
    .restart local v11    # "eventBoxYPosition":I
    .restart local v18    # "eventHeader":Ljava/lang/String;
    .restart local v19    # "i":I
    :cond_7
    const-string v2, "|T|"

    move-object/from16 v0, v18

    invoke-virtual {v2, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_a

    .line 2197
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAccountColorBarMarginRight:I

    add-int/2addr v4, v2

    .line 2199
    move-object/from16 v0, p5

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskTitleMarginLeft:I

    sub-int/2addr v2, v6

    sub-int/2addr v2, v4

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskTitleAvailAdjust:I

    sub-int/2addr v2, v6

    int-to-float v12, v2

    .line 2200
    .restart local v12    # "avail":F
    const/16 v20, 0x0

    .line 2202
    .restart local v20    # "iconWidth":F
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v2, v6

    move/from16 v0, v19

    if-lt v0, v2, :cond_8

    .line 2203
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    if-eqz v2, :cond_8

    .line 2204
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    int-to-float v2, v2

    add-float v20, v20, v2

    .line 2208
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->set(Landroid/graphics/Paint;)V

    .line 2210
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    const/4 v6, 0x3

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 2211
    .restart local v17    # "displayTitle":Ljava/lang/CharSequence;
    sget-object v2, Ljava/text/Normalizer$Form;->NFC:Ljava/text/Normalizer$Form;

    move-object/from16 v0, v17

    invoke-static {v0, v2}, Ljava/text/Normalizer;->normalize(Ljava/lang/CharSequence;Ljava/text/Normalizer$Form;)Ljava/lang/String;

    move-result-object v25

    .line 2213
    .restart local v25    # "vietnamese_laborday":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mVietnameseLaborDayString:Ljava/lang/String;

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 2214
    move-object/from16 v17, v25

    .line 2216
    :cond_9
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    sub-float v6, v12, v20

    sget-object v7, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v17

    invoke-static {v0, v2, v6, v7}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v24

    .line 2218
    .restart local v24    # "text":Ljava/lang/CharSequence;
    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    .line 2219
    goto/16 :goto_2

    .line 2220
    .end local v12    # "avail":F
    .end local v17    # "displayTitle":Ljava/lang/CharSequence;
    .end local v20    # "iconWidth":F
    .end local v24    # "text":Ljava/lang/CharSequence;
    .end local v25    # "vietnamese_laborday":Ljava/lang/String;
    :cond_a
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventDayGapArray:[[I

    aget-object v2, v2, v15

    aget v16, v2, v19

    .line 2221
    .local v16, "dayGap":I
    move/from16 v13, p3

    .line 2223
    .local v13, "columnCheck":I
    add-int/lit8 v2, p2, 0x1

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    if-ne v2, v6, :cond_b

    .line 2224
    add-int/lit8 v2, v15, 0x1

    sub-int v2, v22, v2

    rsub-int/lit8 v13, v2, 0x7

    .line 2226
    :cond_b
    add-int v2, v13, v16

    const/4 v6, 0x7

    if-le v2, v6, :cond_c

    add-int/lit8 v2, p2, 0x1

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    if-eq v2, v6, :cond_c

    .line 2227
    rsub-int/lit8 v16, v13, 0x7

    .line 2230
    :cond_c
    const/4 v12, 0x0

    .line 2231
    .restart local v12    # "avail":F
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v2, v6

    move/from16 v0, v19

    if-lt v0, v2, :cond_f

    .line 2232
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMarginLeft:I

    int-to-float v2, v2

    sub-float/2addr v12, v2

    .line 2233
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleAvailAdjust:I

    int-to-float v2, v2

    sub-float/2addr v12, v2

    .line 2235
    const/16 v21, 0x0

    .local v21, "j":I
    :goto_4
    move/from16 v0, v21

    move/from16 v1, v16

    if-gt v0, v1, :cond_d

    add-int v2, v21, p3

    const/16 v6, 0x8

    if-ge v2, v6, :cond_d

    .line 2236
    add-int v2, p1, v21

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasStickerIcon(I)Z

    move-result v2

    if-nez v2, :cond_d

    add-int v2, p1, v21

    move-object/from16 v0, p0

    invoke-direct {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasWeatherIcon(I)Z

    move-result v2

    if-eqz v2, :cond_e

    .line 2246
    .end local v21    # "j":I
    :cond_d
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    move-object/from16 v0, p6

    invoke-virtual {v2, v0}, Landroid/text/TextPaint;->set(Landroid/graphics/Paint;)V

    .line 2248
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v15

    aget-object v2, v2, v19

    const/4 v6, 0x3

    invoke-virtual {v2, v6}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v17

    .line 2249
    .local v17, "displayTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTextPaint:Landroid/text/TextPaint;

    sget-object v6, Landroid/text/TextUtils$TruncateAt;->END:Landroid/text/TextUtils$TruncateAt;

    move-object/from16 v0, v17

    invoke-static {v0, v2, v12, v6}, Landroid/text/TextUtils;->ellipsize(Ljava/lang/CharSequence;Landroid/text/TextPaint;FLandroid/text/TextUtils$TruncateAt;)Ljava/lang/CharSequence;

    move-result-object v24

    .line 2251
    .restart local v24    # "text":Ljava/lang/CharSequence;
    invoke-interface/range {v24 .. v24}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v3

    goto/16 :goto_2

    .line 2240
    .end local v17    # "displayTitle":Ljava/lang/String;
    .end local v24    # "text":Ljava/lang/CharSequence;
    .restart local v21    # "j":I
    :cond_e
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    int-to-float v2, v2

    add-float/2addr v12, v2

    .line 2235
    add-int/lit8 v21, v21, 0x1

    goto :goto_4

    .line 2243
    .end local v21    # "j":I
    :cond_f
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    add-int/lit8 v6, v16, 0x1

    mul-int/2addr v2, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMarginLeft:I

    sub-int/2addr v2, v6

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleAvailAdjust:I

    sub-int/2addr v2, v6

    int-to-float v12, v2

    goto :goto_5

    .line 2258
    .end local v13    # "columnCheck":I
    .end local v16    # "dayGap":I
    .restart local v24    # "text":Ljava/lang/CharSequence;
    :cond_10
    int-to-float v2, v4

    int-to-float v6, v5

    move-object/from16 v0, p4

    move-object/from16 v1, p6

    invoke-virtual {v0, v3, v2, v6, v1}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    goto/16 :goto_3

    .line 2262
    .end local v3    # "msg":Ljava/lang/String;
    .end local v4    # "textX":I
    .end local v5    # "textY":I
    .end local v11    # "eventBoxYPosition":I
    .end local v12    # "avail":F
    .end local v18    # "eventHeader":Ljava/lang/String;
    .end local v24    # "text":Ljava/lang/CharSequence;
    :cond_11
    return-void
.end method

.method private drawFestivalEffectBorder(Landroid/graphics/Canvas;IIZLandroid/graphics/Rect;IZ)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "column"    # I
    .param p3, "row"    # I
    .param p4, "withinCurrentMonth"    # Z
    .param p5, "r"    # Landroid/graphics/Rect;
    .param p6, "idxFestivalEffect"    # I
    .param p7, "isVacationDay"    # Z

    .prologue
    const/4 v2, 0x1

    .line 1067
    if-eqz p7, :cond_3

    if-ne p6, v2, :cond_3

    .line 1068
    if-eq p2, v2, :cond_1

    iget v2, p5, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v2, 0x1

    .line 1069
    .local v0, "holidayDBLeft":I
    :goto_0
    if-eqz p3, :cond_2

    iget v1, p5, Landroid/graphics/Rect;->top:I

    .line 1070
    .local v1, "holidayDBTop":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectBGDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 1071
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectBGDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, p5, Landroid/graphics/Rect;->right:I

    add-int/lit8 v3, v3, 0x1

    iget v4, p5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v0, v1, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1072
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectBGDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 1085
    .end local v0    # "holidayDBLeft":I
    .end local v1    # "holidayDBTop":I
    :cond_0
    :goto_2
    return-void

    .line 1068
    :cond_1
    iget v2, p5, Landroid/graphics/Rect;->left:I

    add-int/lit8 v0, v2, -0x2

    goto :goto_0

    .line 1069
    .restart local v0    # "holidayDBLeft":I
    :cond_2
    iget v2, p5, Landroid/graphics/Rect;->top:I

    add-int/lit8 v1, v2, -0x1

    goto :goto_1

    .line 1074
    .end local v0    # "holidayDBLeft":I
    :cond_3
    if-nez p7, :cond_0

    const/4 v2, 0x2

    if-ne p6, v2, :cond_0

    .line 1075
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v2, :cond_0

    .line 1076
    if-nez p4, :cond_4

    .line 1077
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    new-instance v3, Landroid/graphics/LightingColorFilter;

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    invoke-direct {v3, v4, v5}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 1081
    :goto_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    iget v3, p5, Landroid/graphics/Rect;->right:I

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    sub-int/2addr v3, v4

    iget v4, p5, Landroid/graphics/Rect;->bottom:I

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    sub-int/2addr v4, v5

    iget v5, p5, Landroid/graphics/Rect;->right:I

    iget v6, p5, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v2, v3, v4, v5, v6}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 1082
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v2, p1}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_2

    .line 1079
    :cond_4
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    goto :goto_3
.end method

.method private drawSticker(Landroid/graphics/Canvas;JIIZ)V
    .locals 8
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "stickerType"    # J
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "withinCurrentMonth"    # Z

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x0

    .line 1542
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerBitmapCache:Landroid/util/LruCache;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/graphics/Bitmap;

    .line 1543
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_1

    .line 1544
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v4, p2, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->getStickerImagePath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 1545
    .local v2, "stickerImagePath":Ljava/lang/String;
    invoke-static {v2, v6, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->decodeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 1547
    if-nez v0, :cond_0

    .line 1571
    .end local v2    # "stickerImagePath":Ljava/lang/String;
    :goto_0
    return-void

    .line 1550
    .restart local v2    # "stickerImagePath":Ljava/lang/String;
    :cond_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerBitmapCache:Landroid/util/LruCache;

    invoke-static {p2, p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v4, v5, v0}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1553
    .end local v2    # "stickerImagePath":Ljava/lang/String;
    :cond_1
    new-instance v3, Landroid/graphics/Paint;

    const/4 v4, 0x2

    invoke-direct {v3, v4}, Landroid/graphics/Paint;-><init>(I)V

    .line 1554
    .local v3, "stickerPaint":Landroid/graphics/Paint;
    if-nez p6, :cond_2

    .line 1555
    new-instance v4, Landroid/graphics/LightingColorFilter;

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    iget v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    invoke-direct {v4, v5, v6}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v3, v4}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1559
    :cond_2
    new-instance v1, Landroid/graphics/Rect;

    invoke-direct {v1}, Landroid/graphics/Rect;-><init>()V

    .line 1561
    .local v1, "sR":Landroid/graphics/Rect;
    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    sub-int v4, p4, v4

    iput v4, v1, Landroid/graphics/Rect;->left:I

    .line 1562
    iput p4, v1, Landroid/graphics/Rect;->right:I

    .line 1563
    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    sub-int v4, p5, v4

    iput v4, v1, Landroid/graphics/Rect;->top:I

    .line 1564
    iput p5, v1, Landroid/graphics/Rect;->bottom:I

    .line 1566
    if-eqz v0, :cond_3

    .line 1567
    invoke-virtual {p1, v0, v7, v1, v3}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 1570
    :cond_3
    invoke-virtual {v3, v7}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0
.end method

.method private drawStickers(Landroid/graphics/Canvas;IILandroid/graphics/Rect;Z)V
    .locals 14
    .param p1, "canvas"    # Landroid/graphics/Canvas;
    .param p2, "day"    # I
    .param p3, "row"    # I
    .param p4, "rect"    # Landroid/graphics/Rect;
    .param p5, "withinCurrentMonth"    # Z

    .prologue
    .line 1518
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-nez v2, :cond_1

    .line 1538
    :cond_0
    :goto_0
    return-void

    .line 1522
    :cond_1
    new-instance v11, Landroid/graphics/Rect;

    invoke-direct {v11}, Landroid/graphics/Rect;-><init>()V

    .line 1523
    .local v11, "stickerRect":Landroid/graphics/Rect;
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->left:I

    iput v2, v11, Landroid/graphics/Rect;->left:I

    .line 1524
    move-object/from16 v0, p4

    iget v2, v0, Landroid/graphics/Rect;->right:I

    iput v2, v11, Landroid/graphics/Rect;->right:I

    .line 1525
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int v2, v2, p3

    add-int/lit8 v2, v2, 0x1

    iput v2, v11, Landroid/graphics/Rect;->top:I

    .line 1526
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int v2, v2, p3

    add-int/lit8 v2, v2, 0x1

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    add-int/2addr v2, v3

    iput v2, v11, Landroid/graphics/Rect;->bottom:I

    .line 1528
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v9

    .line 1529
    .local v9, "columnDay1":I
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v2, v9

    add-int/lit8 v10, v2, 0x1

    .line 1530
    .local v10, "monthStartDay":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    sub-int v3, p2, v10

    aget-object v2, v2, v3

    sget v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v12, v2, v3

    .line 1532
    .local v12, "stickerGroupID":J
    invoke-virtual {p0, v12, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isBackgroundSticker(J)Z

    move-result v2

    if-nez v2, :cond_0

    const-wide/16 v2, -0x1

    cmp-long v2, v12, v2

    if-eqz v2, :cond_0

    .line 1533
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    sub-int v3, p2, v10

    aget-object v2, v2, v3

    sget v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    aget-wide v4, v2, v3

    iget v6, v11, Landroid/graphics/Rect;->right:I

    iget v7, v11, Landroid/graphics/Rect;->bottom:I

    move-object v2, p0

    move-object v3, p1

    move/from16 v8, p5

    invoke-direct/range {v2 .. v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawSticker(Landroid/graphics/Canvas;JIIZ)V

    goto :goto_0
.end method

.method private drawTaskEventTitle(Ljava/lang/String;IILandroid/graphics/Canvas;ZLandroid/graphics/Paint;ZZI)V
    .locals 5
    .param p1, "msg"    # Ljava/lang/String;
    .param p2, "textX"    # I
    .param p3, "textY"    # I
    .param p4, "canvas"    # Landroid/graphics/Canvas;
    .param p5, "drawSelection"    # Z
    .param p6, "p"    # Landroid/graphics/Paint;
    .param p7, "complete"    # Z
    .param p8, "withinCurrentMonth"    # Z
    .param p9, "eventBoxYPosition"    # I

    .prologue
    const/4 v3, 0x1

    .line 2319
    const/4 v1, 0x0

    .line 2321
    .local v1, "taskDrawable":Landroid/graphics/drawable/Drawable;
    if-ne p7, v3, :cond_1

    .line 2322
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckBoxOn:Landroid/graphics/drawable/Drawable;

    .line 2323
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventDimColor:I

    invoke-virtual {p6, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2324
    invoke-virtual {p6, v3}, Landroid/graphics/Paint;->setStrikeThruText(Z)V

    .line 2331
    :goto_0
    if-nez p8, :cond_0

    .line 2332
    new-instance v2, Landroid/graphics/LightingColorFilter;

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    invoke-direct {v2, v3, v4}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2334
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventDimColor:I

    invoke-virtual {p6, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2337
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckboxWidth:I

    .line 2339
    .local v0, "checkBoxWidth":I
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckboxMarginTopFromBoxTop:I

    add-int/2addr v2, p9

    add-int v3, p2, v0

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckboxMarginTopFromBoxTop:I

    add-int/2addr v4, p9

    add-int/2addr v4, v0

    invoke-virtual {v1, p2, v2, v3, v4}, Landroid/graphics/drawable/Drawable;->setBounds(IIII)V

    .line 2341
    invoke-virtual {v1, p4}, Landroid/graphics/drawable/Drawable;->draw(Landroid/graphics/Canvas;)V

    .line 2342
    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable;->setColorFilter(Landroid/graphics/ColorFilter;)V

    .line 2344
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskTitleMarginLeft:I

    add-int/2addr v2, p2

    int-to-float v2, v2

    int-to-float v3, p3

    invoke-virtual {p4, p1, v2, v3, p6}, Landroid/graphics/Canvas;->drawText(Ljava/lang/String;FFLandroid/graphics/Paint;)V

    .line 2345
    return-void

    .line 2326
    .end local v0    # "checkBoxWidth":I
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckBoxOff:Landroid/graphics/drawable/Drawable;

    .line 2327
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventColor:I

    invoke-virtual {p6, v2}, Landroid/graphics/Paint;->setColor(I)V

    .line 2328
    const/4 v2, 0x0

    invoke-virtual {p6, v2}, Landroid/graphics/Paint;->setStrikeThruText(Z)V

    goto :goto_0
.end method

.method private drawText(IIIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;Z)V
    .locals 25
    .param p1, "day"    # I
    .param p2, "weekNum"    # I
    .param p3, "row"    # I
    .param p4, "column"    # I
    .param p5, "canvas"    # Landroid/graphics/Canvas;
    .param p6, "p"    # Landroid/graphics/Paint;
    .param p7, "r"    # Landroid/graphics/Rect;
    .param p8, "isLandscape"    # Z

    .prologue
    .line 2084
    const/4 v9, 0x0

    .line 2085
    .local v9, "drawSelection":Z
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    if-eqz v2, :cond_0

    .line 2086
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isSelected(II)Z

    move-result v9

    .line 2089
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move/from16 v0, p3

    move/from16 v1, p4

    invoke-virtual {v2, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v10

    .line 2090
    .local v10, "withinCurrentMonth":Z
    const/16 v18, 0x0

    .line 2092
    .local v18, "isToday":Z
    new-instance v19, Landroid/text/format/Time;

    invoke-direct/range {v19 .. v19}, Landroid/text/format/Time;-><init>()V

    .line 2093
    .local v19, "mCurrentTime":Landroid/text/format/Time;
    invoke-virtual/range {v19 .. v19}, Landroid/text/format/Time;->setToNow()V

    .line 2094
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v2

    if-eqz v2, :cond_6

    move-object/from16 v0, v19

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v20, v0

    .line 2097
    .local v20, "gmtoff":J
    :goto_0
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    move-wide/from16 v0, v20

    invoke-static {v2, v3, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v22

    .line 2098
    .local v22, "todayJulianDay":I
    move/from16 v0, v22

    move/from16 v1, p1

    if-ne v0, v1, :cond_1

    if-eqz p4, :cond_1

    .line 2099
    const/16 v18, 0x1

    .line 2103
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int v24, p3, v2

    .line 2105
    .local v24, "y":I
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    add-int/lit8 v3, p4, -0x1

    move-object/from16 v0, p0

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    mul-int/2addr v3, v4

    add-int v23, v2, v3

    .line 2107
    .local v23, "x":I
    move/from16 v0, v23

    move-object/from16 v1, p7

    iput v0, v1, Landroid/graphics/Rect;->left:I

    .line 2108
    move/from16 v0, v24

    move-object/from16 v1, p7

    iput v0, v1, Landroid/graphics/Rect;->top:I

    .line 2109
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    add-int v2, v2, v23

    move-object/from16 v0, p7

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 2110
    const/4 v2, 0x1

    move/from16 v0, p4

    if-eq v0, v2, :cond_2

    const/4 v2, 0x7

    move/from16 v0, p4

    if-ne v0, v2, :cond_3

    .line 2111
    :cond_2
    move-object/from16 v0, p7

    iget v2, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p0

    iget v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    add-int/2addr v2, v3

    move-object/from16 v0, p7

    iput v2, v0, Landroid/graphics/Rect;->right:I

    .line 2113
    :cond_3
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    add-int v2, v2, v24

    move-object/from16 v0, p7

    iput v2, v0, Landroid/graphics/Rect;->bottom:I

    .line 2115
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedRow:I

    move/from16 v0, p3

    if-ne v2, v0, :cond_4

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPressedCol:I

    move/from16 v0, p4

    if-ne v2, v0, :cond_4

    if-nez v9, :cond_4

    .line 2117
    const/4 v9, 0x1

    .line 2120
    :cond_4
    if-eqz p4, :cond_5

    move-object/from16 v2, p0

    move/from16 v3, p1

    move/from16 v4, p3

    move/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p7

    move-object/from16 v8, p6

    .line 2121
    invoke-direct/range {v2 .. v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawEventsTitle(IIILandroid/graphics/Canvas;Landroid/graphics/Rect;Landroid/graphics/Paint;ZZ)V

    move-object/from16 v11, p0

    move-object/from16 v12, p5

    move/from16 v13, p3

    move/from16 v14, p4

    move v15, v10

    move/from16 v16, v9

    move/from16 v17, p1

    .line 2122
    invoke-direct/range {v11 .. v18}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawDayNumber(Landroid/graphics/Canvas;IIZZIZ)V

    .line 2124
    :cond_5
    return-void

    .line 2094
    .end local v20    # "gmtoff":J
    .end local v22    # "todayJulianDay":I
    .end local v23    # "x":I
    .end local v24    # "y":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-wide v0, v2, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v20, v0

    goto/16 :goto_0
.end method

.method private drawTextSanitizer(Ljava/lang/String;)Ljava/lang/String;
    .locals 8
    .param p1, "string"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x0

    .line 2032
    invoke-virtual {p1}, Ljava/lang/String;->length()I

    move-result v3

    .line 2033
    .local v3, "length":I
    new-array v5, v3, [C

    .line 2034
    .local v5, "oldChars":[C
    invoke-virtual {p1, v7, v3, v5, v7}, Ljava/lang/String;->getChars(II[CI)V

    .line 2035
    const/4 v4, 0x0

    .line 2036
    .local v4, "newLen":I
    const/4 v1, 0x0

    .line 2038
    .local v1, "changed":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_0
    if-ge v2, v3, :cond_1

    .line 2039
    aget-char v0, v5, v2

    .line 2041
    .local v0, "ch":C
    const/16 v6, 0xa

    if-eq v0, v6, :cond_0

    const/16 v6, 0x9

    if-eq v0, v6, :cond_0

    .line 2042
    aput-char v0, v5, v4

    .line 2047
    :goto_1
    add-int/lit8 v4, v4, 0x1

    .line 2038
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 2044
    :cond_0
    const/16 v6, 0x20

    aput-char v6, v5, v4

    .line 2045
    const/4 v1, 0x1

    goto :goto_1

    .line 2049
    .end local v0    # "ch":C
    :cond_1
    if-eqz v1, :cond_2

    .line 2050
    new-instance p1, Ljava/lang/String;

    .end local p1    # "string":Ljava/lang/String;
    invoke-direct {p1, v5, v7, v4}, Ljava/lang/String;-><init>([CII)V

    .line 2053
    .restart local p1    # "string":Ljava/lang/String;
    :cond_2
    return-object p1
.end method

.method private drawTodayMarker(ZIIZLandroid/graphics/Rect;Landroid/graphics/Canvas;Z)V
    .locals 8
    .param p1, "drawSelection"    # Z
    .param p2, "column"    # I
    .param p3, "row"    # I
    .param p4, "withinCurrentMonth"    # Z
    .param p5, "r"    # Landroid/graphics/Rect;
    .param p6, "canvas"    # Landroid/graphics/Canvas;
    .param p7, "isToday"    # Z

    .prologue
    const v7, 0x7f07003a

    const/4 v6, 0x1

    .line 1143
    if-eqz p1, :cond_2

    .line 1144
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    if-ne v2, v6, :cond_1

    .line 1145
    if-eqz p4, :cond_1

    .line 1146
    iget v2, p5, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginLeft:I

    add-int v0, v2, v3

    .line 1147
    .local v0, "todayLeft":I
    iget v2, p5, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginTop:I

    add-int v1, v2, v3

    .line 1149
    .local v1, "todayTop":I
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1150
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1152
    if-nez p7, :cond_0

    .line 1153
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->STROKE:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1155
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionStrokeWidth:I

    int-to-float v3, v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStrokeWidth(F)V

    .line 1156
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    add-int/2addr v3, v1

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p6, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1158
    if-nez p7, :cond_1

    .line 1159
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    sget-object v3, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 1179
    .end local v0    # "todayLeft":I
    .end local v1    # "todayTop":I
    :cond_1
    :goto_0
    return-void

    .line 1164
    :cond_2
    if-eqz p7, :cond_1

    .line 1166
    iget v2, p5, Landroid/graphics/Rect;->left:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginLeft:I

    add-int v0, v2, v3

    .line 1167
    .restart local v0    # "todayLeft":I
    iget v2, p5, Landroid/graphics/Rect;->top:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginTop:I

    add-int v1, v2, v3

    .line 1169
    .restart local v1    # "todayTop":I
    if-nez p4, :cond_3

    .line 1170
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    new-instance v3, Landroid/graphics/LightingColorFilter;

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    invoke-direct {v3, v4, v5}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1173
    :cond_3
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    invoke-virtual {v3, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 1174
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v2, v6}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 1175
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    add-int/2addr v2, v0

    int-to-float v2, v2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    add-int/2addr v3, v1

    int-to-float v3, v3

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    int-to-float v4, v4

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p6, v2, v3, v4, v5}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 1176
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0
.end method

.method private drawWallpaperSticker(IIIZLandroid/graphics/Rect;Landroid/graphics/Canvas;)V
    .locals 16
    .param p1, "day"    # I
    .param p2, "column"    # I
    .param p3, "row"    # I
    .param p4, "withinCurrentMonth"    # Z
    .param p5, "r"    # Landroid/graphics/Rect;
    .param p6, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 1109
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    const/4 v12, 0x1

    invoke-virtual {v11, v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v3

    .line 1110
    .local v3, "columnDay1":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v11, v3

    add-int/lit8 v4, v11, 0x1

    .line 1112
    .local v4, "monthStartDay":I
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    sub-int v12, p1, v4

    aget-object v11, v11, v12

    sget v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v12, v11, v12

    const-wide/16 v14, -0x1

    cmp-long v11, v12, v14

    if-eqz v11, :cond_0

    .line 1113
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    sub-int v12, p1, v4

    aget-object v11, v11, v12

    sget v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    aget-wide v6, v11, v12

    .line 1114
    .local v6, "stickerType":J
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerBitmapCache:Landroid/util/LruCache;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12}, Landroid/util/LruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/graphics/Bitmap;

    .line 1116
    .local v2, "bitmap":Landroid/graphics/Bitmap;
    if-nez v2, :cond_2

    .line 1117
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v11, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->getStickerImagePath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v5

    .line 1118
    .local v5, "stickerImagePath":Ljava/lang/String;
    const/4 v11, 0x0

    const/4 v12, 0x0

    invoke-static {v5, v11, v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->decodeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v2

    .line 1119
    if-nez v2, :cond_1

    .line 1139
    .end local v2    # "bitmap":Landroid/graphics/Bitmap;
    .end local v5    # "stickerImagePath":Ljava/lang/String;
    .end local v6    # "stickerType":J
    :cond_0
    :goto_0
    return-void

    .line 1122
    .restart local v2    # "bitmap":Landroid/graphics/Bitmap;
    .restart local v5    # "stickerImagePath":Ljava/lang/String;
    .restart local v6    # "stickerType":J
    :cond_1
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerBitmapCache:Landroid/util/LruCache;

    invoke-static {v6, v7}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    invoke-virtual {v11, v12, v2}, Landroid/util/LruCache;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 1125
    .end local v5    # "stickerImagePath":Ljava/lang/String;
    :cond_2
    new-instance v8, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v8}, Landroid/graphics/drawable/ColorDrawable;-><init>()V

    .line 1126
    .local v8, "wallPaperStickerDrawable":Landroid/graphics/drawable/ColorDrawable;
    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v11

    div-int/lit8 v11, v11, 0x2

    invoke-virtual {v2}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    invoke-virtual {v2, v11, v12}, Landroid/graphics/Bitmap;->getPixel(II)I

    move-result v11

    invoke-virtual {v8, v11}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1129
    const/4 v11, 0x1

    move/from16 v0, p2

    if-eq v0, v11, :cond_4

    move-object/from16 v0, p5

    iget v11, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v11, 0x1

    .line 1130
    .local v9, "wallpaperLeft":I
    :goto_1
    if-eqz p3, :cond_5

    move-object/from16 v0, p5

    iget v10, v0, Landroid/graphics/Rect;->top:I

    .line 1131
    .local v10, "wallpaperTop":I
    :goto_2
    if-nez p4, :cond_3

    .line 1132
    invoke-virtual {v8}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v11

    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getDimmedColor(I)I

    move-result v11

    invoke-virtual {v8, v11}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 1136
    :cond_3
    move-object/from16 v0, p5

    iget v11, v0, Landroid/graphics/Rect;->right:I

    move-object/from16 v0, p5

    iget v12, v0, Landroid/graphics/Rect;->bottom:I

    invoke-virtual {v8, v9, v10, v11, v12}, Landroid/graphics/drawable/ColorDrawable;->setBounds(IIII)V

    .line 1137
    move-object/from16 v0, p6

    invoke-virtual {v8, v0}, Landroid/graphics/drawable/ColorDrawable;->draw(Landroid/graphics/Canvas;)V

    goto :goto_0

    .line 1129
    .end local v9    # "wallpaperLeft":I
    .end local v10    # "wallpaperTop":I
    :cond_4
    move-object/from16 v0, p5

    iget v11, v0, Landroid/graphics/Rect;->left:I

    add-int/lit8 v9, v11, -0x2

    goto :goto_1

    .line 1130
    .restart local v9    # "wallpaperLeft":I
    :cond_5
    move-object/from16 v0, p5

    iget v11, v0, Landroid/graphics/Rect;->top:I

    add-int/lit8 v10, v11, -0x1

    goto :goto_2
.end method

.method private drawWeather(IIILandroid/graphics/Canvas;Landroid/graphics/Paint;Landroid/graphics/Rect;)V
    .locals 9
    .param p1, "day"    # I
    .param p2, "row"    # I
    .param p3, "column"    # I
    .param p4, "canvas"    # Landroid/graphics/Canvas;
    .param p5, "p"    # Landroid/graphics/Paint;
    .param p6, "r"    # Landroid/graphics/Rect;

    .prologue
    .line 1357
    iget v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-boolean v6, v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-nez v6, :cond_1

    .line 1384
    :cond_0
    :goto_0
    return-void

    .line 1361
    :cond_1
    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v2

    .line 1362
    .local v2, "weather":Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->makeWeatherKeyFromJulianDay(I)Ljava/lang/String;

    move-result-object v0

    .line 1363
    .local v0, "key":Ljava/lang/String;
    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->hasWeatherImage(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 1367
    invoke-virtual {v2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getWeatherImage(Ljava/lang/String;)Landroid/graphics/Bitmap;

    move-result-object v3

    .line 1369
    .local v3, "weatherIcon":Landroid/graphics/Bitmap;
    if-eqz v3, :cond_0

    .line 1370
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v5

    .line 1371
    .local v5, "weatherIconWidth":I
    invoke-virtual {v3}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v4

    .line 1373
    .local v4, "weatherIconHeight":I
    const/4 v6, 0x1

    invoke-static {v3, v5, v4, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v1

    .line 1376
    .local v1, "resizedweatherIcon":Landroid/graphics/Bitmap;
    const/16 v6, 0xff

    invoke-virtual {p5, v6}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 1377
    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v6, p2, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v6

    if-nez v6, :cond_2

    .line 1378
    new-instance v6, Landroid/graphics/LightingColorFilter;

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    invoke-direct {v6, v7, v8}, Landroid/graphics/LightingColorFilter;-><init>(II)V

    invoke-virtual {p5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    .line 1381
    :cond_2
    iget v6, p6, Landroid/graphics/Rect;->right:I

    sub-int/2addr v6, v5

    int-to-float v6, v6

    iget v7, p6, Landroid/graphics/Rect;->top:I

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageTopMargine:I

    add-int/2addr v7, v8

    int-to-float v7, v7

    invoke-virtual {p4, v1, v6, v7, p5}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;FFLandroid/graphics/Paint;)V

    .line 1382
    const/4 v6, 0x0

    invoke-virtual {p5, v6}, Landroid/graphics/Paint;->setColorFilter(Landroid/graphics/ColorFilter;)Landroid/graphics/ColorFilter;

    goto :goto_0
.end method

.method private drawingCalc(I)V
    .locals 5
    .param p1, "width"    # I

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 970
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->isRecycled()Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v1

    if-eq v1, p1, :cond_2

    :cond_0
    if-lez p1, :cond_2

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    if-lez v1, :cond_2

    .line 975
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    .line 976
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v1}, Landroid/graphics/Bitmap;->recycle()V

    .line 980
    :cond_1
    :try_start_0
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    sget-object v2, Landroid/graphics/Bitmap$Config;->ARGB_8888:Landroid/graphics/Bitmap$Config;

    invoke-static {p1, v1, v2}, Landroid/graphics/Bitmap;->createBitmap(IILandroid/graphics/Bitmap$Config;)Landroid/graphics/Bitmap;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;
    :try_end_0
    .catch Ljava/lang/OutOfMemoryError; {:try_start_0 .. :try_end_0} :catch_0

    .line 988
    new-instance v1, Landroid/graphics/Canvas;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-direct {v1, v2}, Landroid/graphics/Canvas;-><init>(Landroid/graphics/Bitmap;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    .line 990
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    if-nez v1, :cond_2

    .line 991
    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    .line 995
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->top:I

    .line 996
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    iput v2, v1, Landroid/graphics/Rect;->bottom:I

    .line 997
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    iput v3, v1, Landroid/graphics/Rect;->left:I

    .line 998
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    iput p1, v1, Landroid/graphics/Rect;->right:I

    .line 999
    :goto_0
    return-void

    .line 981
    :catch_0
    move-exception v0

    .line 983
    .local v0, "e":Ljava/lang/OutOfMemoryError;
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/OutOfMemoryError;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 984
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    .line 985
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    goto :goto_0
.end method

.method private getCalculatedDate(Landroid/text/format/Time;ZI)V
    .locals 2
    .param p1, "time"    # Landroid/text/format/Time;
    .param p2, "withinCurrentMonth"    # Z
    .param p3, "row"    # I

    .prologue
    const/16 v1, 0xb

    .line 1088
    if-nez p2, :cond_0

    .line 1089
    if-nez p3, :cond_2

    .line 1090
    iget v0, p1, Landroid/text/format/Time;->month:I

    if-nez v0, :cond_1

    .line 1091
    iget v0, p1, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Landroid/text/format/Time;->year:I

    .line 1092
    iput v1, p1, Landroid/text/format/Time;->month:I

    .line 1105
    :cond_0
    :goto_0
    return-void

    .line 1094
    :cond_1
    iget v0, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p1, Landroid/text/format/Time;->month:I

    goto :goto_0

    .line 1097
    :cond_2
    iget v0, p1, Landroid/text/format/Time;->month:I

    if-ne v0, v1, :cond_3

    .line 1098
    iget v0, p1, Landroid/text/format/Time;->year:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/text/format/Time;->year:I

    .line 1099
    const/4 v0, 0x0

    iput v0, p1, Landroid/text/format/Time;->month:I

    goto :goto_0

    .line 1101
    :cond_3
    iget v0, p1, Landroid/text/format/Time;->month:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p1, Landroid/text/format/Time;->month:I

    goto :goto_0
.end method

.method private getCurrentTime(IIIIZ)Landroid/text/format/Time;
    .locals 5
    .param p1, "row"    # I
    .param p2, "selectedYear"    # I
    .param p3, "selectedMonth"    # I
    .param p4, "selectedDay"    # I
    .param p5, "withinSelMonth"    # Z

    .prologue
    const/4 v4, 0x2

    .line 1640
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 1641
    .local v2, "result":Landroid/text/format/Time;
    const/4 v0, 0x0

    .line 1642
    .local v0, "increment":I
    move v1, p3

    .line 1643
    .local v1, "month":I
    move v3, p2

    .line 1647
    .local v3, "year":I
    if-nez p5, :cond_1

    .line 1648
    if-ge p1, v4, :cond_0

    .line 1649
    const/4 v0, -0x1

    .line 1651
    :cond_0
    if-le p1, v4, :cond_1

    .line 1652
    const/4 v0, 0x1

    .line 1655
    :cond_1
    add-int/2addr v1, v0

    .line 1656
    if-gez v1, :cond_2

    .line 1657
    const/16 v1, 0xb

    .line 1658
    add-int/lit8 v3, v3, -0x1

    .line 1660
    :cond_2
    const/16 v4, 0xb

    if-le v1, v4, :cond_3

    .line 1661
    const/4 v1, 0x0

    .line 1662
    add-int/lit8 v3, v3, 0x1

    .line 1664
    :cond_3
    invoke-virtual {v2, p4, v1, v3}, Landroid/text/format/Time;->set(III)V

    .line 1665
    const/4 v4, 0x0

    invoke-virtual {v2, v4}, Landroid/text/format/Time;->normalize(Z)J

    .line 1666
    return-object v2
.end method

.method private getDimmedColor(I)I
    .locals 4
    .param p1, "color"    # I

    .prologue
    const/4 v3, 0x1

    .line 2571
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 2572
    .local v0, "hsv":[F
    invoke-static {p1, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 2573
    aget v1, v0, v3

    const v2, 0x3f19999a    # 0.6f

    mul-float/2addr v1, v2

    aput v1, v0, v3

    .line 2576
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method

.method public static getDisplayColorFromColor(I)I
    .locals 5
    .param p0, "color"    # I

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 2508
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 2509
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 2510
    aget v1, v0, v3

    const v2, 0x40133333    # 2.3f

    mul-float/2addr v1, v2

    const/high16 v2, 0x3f800000    # 1.0f

    invoke-static {v1, v2}, Ljava/lang/Math;->min(FF)F

    move-result v1

    aput v1, v0, v3

    .line 2511
    aget v1, v0, v4

    const/high16 v2, 0x3fc00000    # 1.5f

    mul-float/2addr v1, v2

    aput v1, v0, v4

    .line 2512
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method

.method public static getDisplayColorFromColorForXhdpi(I)I
    .locals 6
    .param p0, "color"    # I

    .prologue
    .line 2516
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 2517
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 2518
    const/4 v1, 0x1

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3fd3333333333333L    # 0.3

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 2519
    const/4 v1, 0x2

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3ffccccccccccccdL    # 1.8

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 2520
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method

.method public static getFillColor(I)I
    .locals 6
    .param p0, "color"    # I

    .prologue
    .line 1585
    const/4 v1, 0x3

    new-array v0, v1, [F

    .line 1586
    .local v0, "hsv":[F
    invoke-static {p0, v0}, Landroid/graphics/Color;->colorToHSV(I[F)V

    .line 1587
    const/4 v1, 0x1

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x3fc3333333333333L    # 0.15

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 1588
    const/4 v1, 0x2

    aget v2, v0, v1

    float-to-double v2, v2

    const-wide v4, 0x4002666666666666L    # 2.3

    mul-double/2addr v2, v4

    double-to-float v2, v2

    aput v2, v0, v1

    .line 1589
    invoke-static {v0}, Landroid/graphics/Color;->HSVToColor([F)I

    move-result v1

    return v1
.end method

.method private getPressedColumn(I)I
    .locals 3
    .param p1, "x"    # I

    .prologue
    .line 618
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    if-gt p1, v1, :cond_1

    .line 619
    const/4 v0, 0x0

    .line 623
    .local v0, "col":I
    :goto_0
    const/4 v1, 0x7

    if-le v0, v1, :cond_0

    .line 624
    const/4 v0, 0x7

    .line 626
    :cond_0
    return v0

    .line 621
    .end local v0    # "col":I
    :cond_1
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    sub-int v1, p1, v1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    div-int/2addr v1, v2

    add-int/lit8 v0, v1, 0x1

    .restart local v0    # "col":I
    goto :goto_0
.end method

.method private getPressedRow(I)I
    .locals 2
    .param p1, "y"    # I

    .prologue
    .line 605
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    div-int v0, p1, v1

    .line 606
    .local v0, "row":I
    if-gez v0, :cond_0

    .line 607
    const/4 v0, 0x0

    .line 610
    :cond_0
    const/4 v1, 0x5

    if-le v0, v1, :cond_1

    .line 611
    const/4 v0, 0x5

    .line 613
    :cond_1
    return v0
.end method

.method private getTitleEventNumber(I)I
    .locals 4
    .param p1, "day"    # I

    .prologue
    const/4 v1, 0x0

    .line 2524
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    const/4 v0, 0x1

    .line 2526
    .local v0, "isLandscape":Z
    :goto_0
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasStickerIcon(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    .line 2527
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasWeatherIcon(I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasWeatherIcon:Z

    .line 2528
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    .line 2530
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsTabletConfig:Z

    if-eqz v2, :cond_3

    .line 2531
    if-nez v0, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    if-eqz v2, :cond_0

    .line 2532
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    .line 2539
    :cond_0
    :goto_1
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    if-gez v2, :cond_1

    .line 2540
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    .line 2542
    :cond_1
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    return v1

    .end local v0    # "isLandscape":Z
    :cond_2
    move v0, v1

    .line 2524
    goto :goto_0

    .line 2535
    .restart local v0    # "isLandscape":Z
    :cond_3
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHasStickerIcon:Z

    if-eqz v2, :cond_0

    .line 2536
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    sub-int/2addr v2, v3

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTitleEventNumber:I

    goto :goto_1
.end method

.method private getWeekOfYear(IIZLjava/util/Calendar;)I
    .locals 7
    .param p1, "row"    # I
    .param p2, "column"    # I
    .param p3, "isWithinCurrentMonth"    # Z
    .param p4, "calendar"    # Ljava/util/Calendar;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x2

    .line 1593
    const/4 v3, 0x5

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v4, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v4

    invoke-virtual {p4, v3, v4}, Ljava/util/Calendar;->set(II)V

    .line 1594
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    .line 1596
    .local v1, "within":Z
    if-eqz v1, :cond_0

    .line 1597
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v3

    invoke-virtual {p4, v5, v3}, Ljava/util/Calendar;->set(II)V

    .line 1598
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v3

    invoke-virtual {p4, v6, v3}, Ljava/util/Calendar;->set(II)V

    .line 1623
    :goto_0
    const/4 v3, 0x3

    invoke-virtual {p4, v3}, Ljava/util/Calendar;->get(I)I

    move-result v3

    return v3

    .line 1600
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v0

    .line 1601
    .local v0, "month":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v2

    .line 1603
    .local v2, "year":I
    if-ge p1, v5, :cond_2

    .line 1605
    if-nez v0, :cond_1

    .line 1606
    add-int/lit8 v2, v2, -0x1

    .line 1607
    const/16 v0, 0xb

    .line 1620
    :goto_1
    invoke-virtual {p4, v5, v0}, Ljava/util/Calendar;->set(II)V

    .line 1621
    invoke-virtual {p4, v6, v2}, Ljava/util/Calendar;->set(II)V

    goto :goto_0

    .line 1609
    :cond_1
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .line 1613
    :cond_2
    const/16 v3, 0xb

    if-ne v0, v3, :cond_3

    .line 1614
    add-int/lit8 v2, v2, 0x1

    .line 1615
    const/4 v0, 0x0

    goto :goto_1

    .line 1617
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private hasStickerIcon(I)Z
    .locals 8
    .param p1, "day"    # I

    .prologue
    const/4 v4, 0x1

    .line 2609
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v5, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v0

    .line 2610
    .local v0, "columnDay1":I
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    sub-int/2addr v5, v0

    add-int/lit8 v1, v5, 0x1

    .line 2611
    .local v1, "monthStartDay":I
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    sub-int v6, p1, v1

    aget-object v5, v5, v6

    sget v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v2, v5, v6

    .line 2613
    .local v2, "stickerGroup":J
    const-wide/16 v6, -0x1

    cmp-long v5, v2, v6

    if-eqz v5, :cond_0

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isBackgroundSticker(J)Z

    move-result v5

    if-nez v5, :cond_0

    .line 2617
    :goto_0
    return v4

    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method private hasWeatherIcon(I)Z
    .locals 10
    .param p1, "day"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 2588
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iget-boolean v5, v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v5, :cond_0

    .line 2589
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 2590
    .local v2, "today":Landroid/text/format/Time;
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 2591
    invoke-virtual {v2, v4}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v8, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v5

    sub-int v0, p1, v5

    .line 2594
    .local v0, "diff":I
    if-gez v0, :cond_1

    .line 2604
    .end local v0    # "diff":I
    .end local v2    # "today":Landroid/text/format/Time;
    :cond_0
    :goto_0
    return v3

    .line 2599
    .restart local v0    # "diff":I
    .restart local v2    # "today":Landroid/text/format/Time;
    :cond_1
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->makeWeatherKeyFromJulianDay(I)Ljava/lang/String;

    move-result-object v1

    .line 2600
    .local v1, "key":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->hasWeatherImage(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_0

    move v3, v4

    .line 2601
    goto :goto_0
.end method

.method public static isUSAWeekNumber()Ljava/lang/Boolean;
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2428
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 2430
    .local v1, "weekNumFeature":Ljava/lang/Boolean;
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v2

    const-string v3, "CscFeature_Calendar_EnableShiftedWeekNumber"

    invoke-virtual {v2, v3}, Lcom/sec/android/app/CscFeature;->getEnableStatus(Ljava/lang/String;)Z

    move-result v2

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 2435
    :goto_0
    return-object v1

    .line 2432
    :catch_0
    move-exception v0

    .line 2433
    .local v0, "e":Ljava/lang/Exception;
    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    goto :goto_0
.end method

.method private makeWeatherKeyFromJulianDay(I)Ljava/lang/String;
    .locals 2
    .param p1, "day"    # I

    .prologue
    .line 1578
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 1579
    .local v0, "time":Landroid/text/format/Time;
    invoke-virtual {v0, p1}, Landroid/text/format/Time;->setJulianDay(I)J

    .line 1581
    const-string v1, "%Y%m%d"

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static parseWeekdayColor(Ljava/lang/String;[I)[I
    .locals 10
    .param p0, "weekdayFeatureString"    # Ljava/lang/String;
    .param p1, "colorSet"    # [I

    .prologue
    const/4 v9, 0x3

    const/4 v8, 0x2

    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 2476
    const-string v2, "XXXXXXR"

    .line 2477
    .local v2, "defaultWeekdayColors":Ljava/lang/String;
    const/4 v0, 0x7

    .line 2478
    .local v0, "DEFAULT_NUM_DAYS":I
    const/4 v5, 0x7

    new-array v1, v5, [I

    .line 2480
    .local v1, "colorValues":[I
    if-eqz p0, :cond_0

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v5

    if-eq v5, v0, :cond_1

    .line 2481
    :cond_0
    move-object p0, v2

    .line 2484
    :cond_1
    if-eqz p1, :cond_2

    array-length v5, p1

    if-eq v5, v9, :cond_4

    .line 2485
    :cond_2
    if-nez p1, :cond_3

    .line 2486
    new-array p1, v9, [I

    .line 2488
    :cond_3
    const/high16 v5, -0x1000000

    aput v5, p1, v6

    .line 2489
    const v5, -0xffff01

    aput v5, p1, v7

    .line 2490
    const/high16 v5, -0x10000

    aput v5, p1, v8

    .line 2493
    :cond_4
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v0, :cond_7

    .line 2494
    invoke-virtual {p0, v3}, Ljava/lang/String;->charAt(I)C

    move-result v4

    .line 2495
    .local v4, "parsedColor":C
    const/16 v5, 0x52

    if-ne v4, v5, :cond_5

    .line 2496
    aget v5, p1, v8

    aput v5, v1, v3

    .line 2493
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 2497
    :cond_5
    const/16 v5, 0x42

    if-ne v4, v5, :cond_6

    .line 2498
    aget v5, p1, v7

    aput v5, v1, v3

    goto :goto_1

    .line 2500
    :cond_6
    aget v5, p1, v6

    aput v5, v1, v3

    goto :goto_1

    .line 2503
    .end local v4    # "parsedColor":C
    :cond_7
    return-object v1
.end method

.method private setNextFirstJulianDay()I
    .locals 4

    .prologue
    .line 2580
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 2581
    .local v1, "time":Landroid/text/format/Time;
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->setJulianDay(Landroid/text/format/Time;I)J

    .line 2583
    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->getActualMaximum(I)I

    move-result v0

    .line 2584
    .local v0, "lastDayOfThisMonth":I
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    add-int/2addr v2, v0

    return v2
.end method

.method private setQSMemos(Landroid/database/Cursor;)V
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 2366
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    const/4 v2, -0x1

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2367
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoEventArray:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->clear()V

    .line 2369
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 2371
    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;-><init>()V

    .line 2373
    .local v0, "item":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;
    const/4 v1, 0x1

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;->memoId:I

    .line 2374
    const/4 v1, 0x2

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    iput v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;->date:I

    .line 2375
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoEventArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2377
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-nez v1, :cond_0

    .line 2379
    return-void
.end method

.method private setTasks(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 774
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    if-eqz v2, :cond_0

    .line 775
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 776
    .local v1, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, p1, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->constructTask(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/ArrayList;)V

    .line 777
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 779
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 780
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 779
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 783
    .end local v0    # "i":I
    .end local v1    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    :cond_0
    return-void
.end method

.method private updateTodayButton()V
    .locals 2

    .prologue
    .line 859
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getSelectedDate()Landroid/text/format/Time;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->updateTodayButton(Landroid/text/format/Time;)V

    .line 860
    return-void
.end method


# virtual methods
.method protected calEventDay()Ljava/util/ArrayList;
    .locals 60
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1749
    new-instance v43, Ljava/util/ArrayList;

    invoke-direct/range {v43 .. v43}, Ljava/util/ArrayList;-><init>()V

    .line 1750
    .local v43, "result":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Object;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;

    move-object/from16 v23, v0

    .line 1751
    .local v23, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTasksArray:Ljava/util/ArrayList;

    move-object/from16 v22, v0

    .line 1752
    .local v22, "eventTasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNumDays:I

    move/from16 v37, v0

    .line 1753
    .local v37, "numDays":I
    invoke-virtual/range {v23 .. v23}, Ljava/util/ArrayList;->size()I

    move-result v38

    .line 1754
    .local v38, "numEvents":I
    invoke-virtual/range {v22 .. v22}, Ljava/util/ArrayList;->size()I

    move-result v39

    .line 1755
    .local v39, "numTaskEvents":I
    const/16 v56, 0x2b

    const/16 v57, 0xb

    filled-new-array/range {v56 .. v57}, [I

    move-result-object v56

    const-class v57, Ljava/lang/String;

    move-object/from16 v0, v57

    move-object/from16 v1, v56

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v13

    check-cast v13, [[Ljava/lang/String;

    .line 1756
    .local v13, "eventArray":[[Ljava/lang/String;
    const/16 v56, 0x2b

    const/16 v57, 0xb

    filled-new-array/range {v56 .. v57}, [I

    move-result-object v56

    sget-object v57, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v57

    move-object/from16 v1, v56

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, [[I

    .line 1757
    .local v15, "eventColorArray":[[I
    const/16 v56, 0x2b

    const/16 v57, 0xb

    filled-new-array/range {v56 .. v57}, [I

    move-result-object v56

    sget-object v57, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    move-object/from16 v0, v57

    move-object/from16 v1, v56

    invoke-static {v0, v1}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;[I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, [[I

    .line 1758
    .local v16, "eventDayGapArray":[[I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    move-object/from16 v56, v0

    invoke-static/range {v56 .. v56}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v56

    add-int/lit8 v25, v56, -0x1

    .line 1760
    .local v25, "firstDayOfWeek":I
    const-string v35, "|R|"

    .line 1761
    .local v35, "multiEventHeader":Ljava/lang/String;
    const-string v36, "R"

    .line 1763
    .local v36, "multiEventTail":Ljava/lang/String;
    const/16 v56, 0x2b

    move/from16 v0, v56

    new-array v0, v0, [I

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    .line 1764
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoEventArray:Ljava/util/ArrayList;

    move-object/from16 v41, v0

    .line 1765
    .local v41, "qsMemos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;>;"
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    move-object/from16 v56, v0

    const/16 v57, 0x1

    invoke-virtual/range {v56 .. v57}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v4

    .line 1766
    .local v4, "columnDay1":I
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    move/from16 v56, v0

    sub-int v56, v56, v4

    add-int/lit8 v34, v56, 0x1

    .line 1768
    .local v34, "monthStartDay":I
    const/16 v29, 0x0

    .local v29, "i":I
    :goto_0
    invoke-virtual/range {v41 .. v41}, Ljava/util/ArrayList;->size()I

    move-result v56

    move/from16 v0, v29

    move/from16 v1, v56

    if-ge v0, v1, :cond_1

    .line 1770
    move-object/from16 v0, v41

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v56

    check-cast v56, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;

    move-object/from16 v0, v56

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;->date:I

    move/from16 v33, v0

    .line 1771
    .local v33, "memodate":I
    sub-int v28, v33, v34

    .line 1773
    .local v28, "haveMemoDate":I
    if-ltz v28, :cond_0

    const/16 v56, 0x2b

    move/from16 v0, v28

    move/from16 v1, v56

    if-ge v0, v1, :cond_0

    .line 1774
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    move-object/from16 v57, v0

    move-object/from16 v0, v41

    move/from16 v1, v29

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v56

    check-cast v56, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;

    move-object/from16 v0, v56

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$QSMemoItem;->memoId:I

    move/from16 v56, v0

    invoke-static/range {v56 .. v56}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v56

    aput-object v56, v57, v28

    .line 1768
    :cond_0
    add-int/lit8 v29, v29, 0x1

    goto :goto_0

    .line 1778
    .end local v28    # "haveMemoDate":I
    .end local v33    # "memodate":I
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->clearStickerInfo()V

    .line 1781
    const/4 v6, 0x0

    .local v6, "day":I
    :goto_1
    move/from16 v0, v38

    if-ge v6, v0, :cond_7

    .line 1782
    move-object/from16 v0, v23

    invoke-virtual {v0, v6}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 1783
    .local v12, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    move/from16 v44, v0

    .line 1784
    .local v44, "startDay":I
    iget v11, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    .line 1785
    .local v11, "endDay":I
    sub-int v7, v11, v44

    .line 1786
    .local v7, "dayGap":I
    sub-int v20, v44, v34

    .line 1788
    .local v20, "eventStartDay":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    array-length v0, v0

    move/from16 v56, v0

    move/from16 v0, v20

    move/from16 v1, v56

    if-lt v0, v1, :cond_3

    .line 1781
    :cond_2
    add-int/lit8 v6, v6, 0x1

    goto :goto_1

    .line 1792
    :cond_3
    const/16 v56, -0x1

    move/from16 v0, v56

    move/from16 v1, v20

    if-ge v0, v1, :cond_4

    .line 1793
    invoke-static {v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->hasSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v56

    if-eqz v56, :cond_4

    .line 1794
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v56, v56, v57

    const-wide/16 v58, -0x1

    cmp-long v56, v56, v58

    if-nez v56, :cond_6

    invoke-static {v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->isBackgroundSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v56

    if-eqz v56, :cond_6

    .line 1796
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    move/from16 v58, v0

    move/from16 v0, v58

    int-to-long v0, v0

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    .line 1797
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    .line 1806
    :cond_4
    :goto_2
    if-lez v7, :cond_2

    .line 1807
    invoke-static {v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->hasSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v56

    if-eqz v56, :cond_2

    .line 1808
    const/4 v8, 0x1

    .local v8, "dd":I
    :goto_3
    if-gt v8, v7, :cond_2

    .line 1809
    const/16 v56, -0x1

    add-int v57, v20, v8

    move/from16 v0, v56

    move/from16 v1, v57

    if-ge v0, v1, :cond_5

    add-int v56, v20, v8

    move/from16 v0, v56

    move/from16 v1, v37

    if-ge v0, v1, :cond_5

    .line 1810
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    add-int v57, v20, v8

    aget-object v56, v56, v57

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v56, v56, v57

    const-wide/16 v58, 0x0

    cmp-long v56, v56, v58

    if-gez v56, :cond_5

    .line 1811
    invoke-static {v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->isBackgroundSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v56

    if-eqz v56, :cond_5

    .line 1812
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    add-int v57, v20, v8

    aget-object v56, v56, v57

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    move/from16 v58, v0

    move/from16 v0, v58

    int-to-long v0, v0

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    .line 1813
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWallpaperStickerInfo:[[J

    move-object/from16 v56, v0

    add-int v57, v20, v8

    aget-object v56, v56, v57

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    .line 1808
    :cond_5
    add-int/lit8 v8, v8, 0x1

    goto :goto_3

    .line 1798
    .end local v8    # "dd":I
    :cond_6
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v56, v56, v57

    const-wide/16 v58, -0x1

    cmp-long v56, v56, v58

    if-nez v56, :cond_4

    invoke-static {v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->isBackgroundSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v56

    if-nez v56, :cond_4

    .line 1800
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    move/from16 v58, v0

    move/from16 v0, v58

    int-to-long v0, v0

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    .line 1801
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_TYPE:I

    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    move-wide/from16 v58, v0

    aput-wide v58, v56, v57

    goto/16 :goto_2

    .line 1824
    .end local v7    # "dayGap":I
    .end local v11    # "endDay":I
    .end local v12    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v20    # "eventStartDay":I
    .end local v44    # "startDay":I
    :cond_7
    const/4 v5, 0x0

    .local v5, "d":I
    :goto_4
    move/from16 v0, v38

    if-ge v5, v0, :cond_22

    .line 1825
    move-object/from16 v0, v23

    invoke-virtual {v0, v5}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 1826
    .restart local v12    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    move/from16 v44, v0

    .line 1827
    .restart local v44    # "startDay":I
    iget v11, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    .line 1828
    .restart local v11    # "endDay":I
    sub-int v7, v11, v44

    .line 1829
    .restart local v7    # "dayGap":I
    const/16 v31, -0x1

    .line 1831
    .local v31, "location":I
    new-instance v45, Landroid/text/format/Time;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    move-object/from16 v56, v0

    const/16 v57, 0x0

    invoke-static/range {v56 .. v57}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, v45

    move-object/from16 v1, v56

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1832
    .local v45, "startTime":Landroid/text/format/Time;
    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    move-wide/from16 v46, v0

    .line 1833
    .local v46, "startMillis":J
    invoke-virtual/range {v45 .. v47}, Landroid/text/format/Time;->set(J)V

    .line 1834
    sub-int v56, v44, v34

    add-int v56, v56, v25

    rem-int/lit8 v48, v56, 0x7

    .line 1835
    .local v48, "startWeekDay":I
    sub-int v20, v44, v34

    .line 1836
    .restart local v20    # "eventStartDay":I
    if-gez v20, :cond_8

    .line 1837
    const/16 v20, 0x0

    .line 1839
    :cond_8
    if-lez v7, :cond_c

    .line 1840
    iget v14, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    .line 1841
    .local v14, "eventColor":I
    iget-object v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    move-object/from16 v56, v0

    invoke-interface/range {v56 .. v56}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawTextSanitizer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 1843
    .local v54, "title":Ljava/lang/String;
    sub-int v27, v44, v34

    .line 1845
    .local v27, "haveDay":I
    if-gez v27, :cond_b

    .line 1846
    new-instance v49, Landroid/text/format/Time;

    invoke-direct/range {v49 .. v49}, Landroid/text/format/Time;-><init>()V

    .line 1847
    .local v49, "time":Landroid/text/format/Time;
    move-object/from16 v0, v49

    move/from16 v1, v34

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->setJulianDay(Landroid/text/format/Time;I)J

    .line 1848
    move-object/from16 v0, v49

    iget v0, v0, Landroid/text/format/Time;->weekDay:I

    move/from16 v48, v0

    .line 1849
    add-int v7, v7, v27

    .line 1850
    const/16 v27, 0x0

    .line 1856
    .end local v49    # "time":Landroid/text/format/Time;
    :cond_9
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    move-object/from16 v56, v0

    aget v57, v56, v27

    add-int/lit8 v57, v57, 0x1

    aput v57, v56, v27

    .line 1857
    const/16 v26, 0x0

    .local v26, "g":I
    :goto_5
    move/from16 v0, v26

    if-ge v0, v7, :cond_d

    .line 1858
    add-int v56, v27, v26

    add-int/lit8 v56, v56, 0x1

    move/from16 v0, v56

    move/from16 v1, v37

    if-ge v0, v1, :cond_a

    .line 1859
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    move-object/from16 v56, v0

    add-int v57, v27, v26

    add-int/lit8 v57, v57, 0x1

    aget v58, v56, v57

    add-int/lit8 v58, v58, 0x1

    aput v58, v56, v57

    .line 1857
    :cond_a
    add-int/lit8 v26, v26, 0x1

    goto :goto_5

    .line 1851
    .end local v26    # "g":I
    :cond_b
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    move-object/from16 v56, v0

    move-object/from16 v0, v56

    array-length v0, v0

    move/from16 v56, v0

    move/from16 v0, v27

    move/from16 v1, v56

    if-lt v0, v1, :cond_9

    .line 1824
    .end local v14    # "eventColor":I
    .end local v27    # "haveDay":I
    .end local v54    # "title":Ljava/lang/String;
    :cond_c
    add-int/lit8 v5, v5, 0x1

    goto/16 :goto_4

    .line 1863
    .restart local v14    # "eventColor":I
    .restart local v26    # "g":I
    .restart local v27    # "haveDay":I
    .restart local v54    # "title":Ljava/lang/String;
    :cond_d
    sget v32, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    .line 1864
    .local v32, "mEventTitleLineNumOftheDay":I
    move/from16 v9, v44

    .line 1865
    .local v9, "drawEventStartDay":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v20

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v50, v56, v57

    .line 1866
    .local v50, "stickerGroup":J
    const-wide/16 v56, -0x1

    cmp-long v56, v50, v56

    if-eqz v56, :cond_e

    move-object/from16 v0, p0

    move-wide/from16 v1, v50

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isBackgroundSticker(J)Z

    move-result v56

    if-eqz v56, :cond_f

    :cond_e
    move-object/from16 v0, p0

    move/from16 v1, v44

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasWeatherIcon(I)Z

    move-result v56

    if-eqz v56, :cond_10

    .line 1868
    :cond_f
    sget v56, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    move/from16 v57, v0

    sub-int v32, v56, v57

    .line 1871
    :cond_10
    move/from16 v40, v27

    .line 1872
    .local v40, "originalHaveDay":I
    const/16 v29, 0x0

    :goto_6
    move/from16 v0, v29

    move/from16 v1, v32

    if-ge v0, v1, :cond_12

    .line 1873
    aget-object v56, v13, v27

    aget-object v56, v56, v29

    if-eqz v56, :cond_11

    aget-object v56, v13, v27

    aget-object v56, v56, v29

    invoke-virtual/range {v56 .. v56}, Ljava/lang/String;->length()I

    move-result v56

    if-nez v56, :cond_1c

    .line 1874
    :cond_11
    move/from16 v31, v29

    .line 1879
    :cond_12
    const/16 v56, -0x1

    move/from16 v0, v31

    move/from16 v1, v56

    if-ne v0, v1, :cond_18

    .line 1881
    sub-int v17, v11, v34

    .line 1882
    .local v17, "eventEndDay":I
    move/from16 v0, v17

    move/from16 v1, v37

    if-le v0, v1, :cond_13

    .line 1883
    move/from16 v17, v37

    .line 1885
    :cond_13
    add-int/lit8 v42, v27, 0x1

    .local v42, "recalHaveday":I
    :goto_7
    move/from16 v0, v42

    move/from16 v1, v17

    if-gt v0, v1, :cond_18

    .line 1887
    const/16 v55, 0x1

    .line 1888
    .local v55, "weatherDay":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerInfo:[[J

    move-object/from16 v56, v0

    aget-object v56, v56, v42

    sget v57, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->STICKER_GROUP_ID:I

    aget-wide v52, v56, v57

    .line 1889
    .local v52, "stickerGroupOfTheDay":J
    const-wide/16 v56, -0x1

    cmp-long v56, v52, v56

    if-eqz v56, :cond_14

    move-object/from16 v0, p0

    move-wide/from16 v1, v52

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isBackgroundSticker(J)Z

    move-result v56

    if-eqz v56, :cond_15

    :cond_14
    add-int v56, v44, v55

    move-object/from16 v0, p0

    move/from16 v1, v56

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->hasWeatherIcon(I)Z

    move-result v56

    if-eqz v56, :cond_1d

    .line 1892
    :cond_15
    sget v56, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    move/from16 v57, v0

    sub-int v32, v56, v57

    .line 1898
    :goto_8
    const/16 v29, 0x0

    :goto_9
    move/from16 v0, v29

    move/from16 v1, v32

    if-ge v0, v1, :cond_17

    .line 1899
    aget-object v56, v13, v42

    aget-object v56, v56, v29

    if-eqz v56, :cond_16

    aget-object v56, v13, v42

    aget-object v56, v56, v29

    invoke-virtual/range {v56 .. v56}, Ljava/lang/String;->length()I

    move-result v56

    if-nez v56, :cond_1e

    .line 1901
    :cond_16
    move/from16 v31, v29

    .line 1902
    move/from16 v27, v42

    .line 1903
    sub-int v56, v42, v40

    add-int v9, v44, v56

    .line 1904
    sub-int v56, v27, v40

    sub-int v7, v7, v56

    .line 1908
    :cond_17
    add-int/lit8 v55, v55, 0x1

    .line 1909
    const/16 v56, -0x1

    move/from16 v0, v31

    move/from16 v1, v56

    if-eq v0, v1, :cond_1f

    .line 1914
    .end local v17    # "eventEndDay":I
    .end local v42    # "recalHaveday":I
    .end local v52    # "stickerGroupOfTheDay":J
    .end local v55    # "weatherDay":I
    :cond_18
    const/16 v56, -0x1

    move/from16 v0, v31

    move/from16 v1, v56

    if-eq v0, v1, :cond_c

    .line 1916
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    move/from16 v56, v0

    move/from16 v0, v56

    if-ge v9, v0, :cond_19

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    move/from16 v56, v0

    move/from16 v0, v56

    if-lt v11, v0, :cond_1a

    :cond_19
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNextFirstJulianDay:I

    move/from16 v56, v0

    move/from16 v0, v56

    if-lt v9, v0, :cond_20

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNextFirstJulianDay:I

    move/from16 v56, v0

    move/from16 v0, v56

    if-lt v11, v0, :cond_20

    .line 1919
    :cond_1a
    const-string v35, "|P|"

    .line 1920
    const-string v36, "P"

    .line 1926
    :goto_a
    aget-object v56, v13, v27

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v57

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v56, v31

    .line 1927
    aget-object v56, v15, v27

    aput v14, v56, v31

    .line 1928
    aget-object v56, v16, v27

    aput v7, v56, v31

    .line 1930
    const/4 v8, 0x1

    .restart local v8    # "dd":I
    :goto_b
    if-gt v8, v7, :cond_c

    .line 1931
    add-int v56, v27, v8

    move/from16 v0, v56

    move/from16 v1, v37

    if-ge v0, v1, :cond_1b

    .line 1932
    sub-int v56, v27, v40

    add-int v56, v56, v48

    add-int v56, v56, v8

    rem-int/lit8 v56, v56, 0x7

    move/from16 v0, v56

    move/from16 v1, v25

    if-ne v0, v1, :cond_21

    .line 1933
    add-int v56, v27, v8

    aget-object v56, v13, v56

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v57

    move-object/from16 v1, v35

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v56, v31

    .line 1934
    add-int v56, v27, v8

    aget-object v56, v15, v56

    aput v14, v56, v31

    .line 1939
    :goto_c
    add-int v56, v27, v8

    aget-object v56, v16, v56

    sub-int v57, v7, v8

    aput v57, v56, v31

    .line 1930
    :cond_1b
    add-int/lit8 v8, v8, 0x1

    goto :goto_b

    .line 1872
    .end local v8    # "dd":I
    :cond_1c
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_6

    .line 1895
    .restart local v17    # "eventEndDay":I
    .restart local v42    # "recalHaveday":I
    .restart local v52    # "stickerGroupOfTheDay":J
    .restart local v55    # "weatherDay":I
    :cond_1d
    sget v32, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    goto/16 :goto_8

    .line 1898
    :cond_1e
    add-int/lit8 v29, v29, 0x1

    goto/16 :goto_9

    .line 1885
    :cond_1f
    add-int/lit8 v42, v42, 0x1

    goto/16 :goto_7

    .line 1922
    .end local v17    # "eventEndDay":I
    .end local v42    # "recalHaveday":I
    .end local v52    # "stickerGroupOfTheDay":J
    .end local v55    # "weatherDay":I
    :cond_20
    const-string v35, "|R|"

    .line 1923
    const-string v36, "R"

    goto/16 :goto_a

    .line 1936
    .restart local v8    # "dd":I
    :cond_21
    add-int v56, v27, v8

    aget-object v56, v13, v56

    aput-object v36, v56, v31

    .line 1937
    add-int v56, v27, v8

    aget-object v56, v15, v56

    aput v14, v56, v31

    goto :goto_c

    .line 1950
    .end local v7    # "dayGap":I
    .end local v8    # "dd":I
    .end local v9    # "drawEventStartDay":I
    .end local v11    # "endDay":I
    .end local v12    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v14    # "eventColor":I
    .end local v20    # "eventStartDay":I
    .end local v26    # "g":I
    .end local v27    # "haveDay":I
    .end local v31    # "location":I
    .end local v32    # "mEventTitleLineNumOftheDay":I
    .end local v40    # "originalHaveDay":I
    .end local v44    # "startDay":I
    .end local v45    # "startTime":Landroid/text/format/Time;
    .end local v46    # "startMillis":J
    .end local v48    # "startWeekDay":I
    .end local v50    # "stickerGroup":J
    .end local v54    # "title":Ljava/lang/String;
    :cond_22
    const/16 v24, 0x0

    .local v24, "ff":I
    :goto_d
    move/from16 v0, v24

    move/from16 v1, v38

    if-ge v0, v1, :cond_2a

    .line 1951
    invoke-virtual/range {v23 .. v24}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 1953
    .restart local v12    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    move/from16 v44, v0

    .line 1954
    .restart local v44    # "startDay":I
    iget v11, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    .line 1955
    .restart local v11    # "endDay":I
    sub-int v7, v11, v44

    .line 1956
    .restart local v7    # "dayGap":I
    const/16 v31, -0x1

    .line 1958
    .restart local v31    # "location":I
    if-nez v7, :cond_25

    .line 1960
    iget v14, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    .line 1961
    .restart local v14    # "eventColor":I
    iget-object v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    move-object/from16 v56, v0

    invoke-interface/range {v56 .. v56}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawTextSanitizer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 1963
    .restart local v54    # "title":Ljava/lang/String;
    sub-int v27, v44, v34

    .line 1964
    .restart local v27    # "haveDay":I
    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    move-wide/from16 v56, v0

    iget-wide v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    move-wide/from16 v58, v0

    sub-long v18, v56, v58

    .line 1966
    .local v18, "eventDuration":J
    if-ltz v27, :cond_25

    move/from16 v0, v27

    move/from16 v1, v37

    if-ge v0, v1, :cond_25

    .line 1967
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    move-object/from16 v56, v0

    aget v57, v56, v27

    add-int/lit8 v57, v57, 0x1

    aput v57, v56, v27

    .line 1969
    const/16 v29, 0x0

    :goto_e
    const/16 v56, 0xb

    move/from16 v0, v29

    move/from16 v1, v56

    if-ge v0, v1, :cond_24

    .line 1970
    aget-object v56, v13, v27

    aget-object v56, v56, v29

    if-eqz v56, :cond_23

    aget-object v56, v13, v27

    aget-object v56, v56, v29

    invoke-virtual/range {v56 .. v56}, Ljava/lang/String;->length()I

    move-result v56

    if-nez v56, :cond_26

    .line 1971
    :cond_23
    move/from16 v31, v29

    .line 1976
    :cond_24
    const/16 v56, -0x1

    move/from16 v0, v31

    move/from16 v1, v56

    if-ne v0, v1, :cond_27

    .line 1950
    .end local v14    # "eventColor":I
    .end local v18    # "eventDuration":J
    .end local v27    # "haveDay":I
    .end local v54    # "title":Ljava/lang/String;
    :cond_25
    :goto_f
    add-int/lit8 v24, v24, 0x1

    goto :goto_d

    .line 1969
    .restart local v14    # "eventColor":I
    .restart local v18    # "eventDuration":J
    .restart local v27    # "haveDay":I
    .restart local v54    # "title":Ljava/lang/String;
    :cond_26
    add-int/lit8 v29, v29, 0x1

    goto :goto_e

    .line 1979
    :cond_27
    iget-boolean v0, v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    move/from16 v56, v0

    if-nez v56, :cond_28

    const-wide/32 v56, 0x5265c00

    cmp-long v56, v18, v56

    if-ltz v56, :cond_29

    .line 1980
    :cond_28
    aget-object v56, v13, v27

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    const-string v58, "|A|"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v56, v31

    .line 1981
    aget-object v56, v15, v27

    aput v14, v56, v31

    goto :goto_f

    .line 1985
    :cond_29
    aget-object v56, v13, v27

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    const-string v58, "|S|"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v56, v31

    .line 1986
    aget-object v56, v15, v27

    aput v14, v56, v31

    goto :goto_f

    .line 1994
    .end local v7    # "dayGap":I
    .end local v11    # "endDay":I
    .end local v12    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v14    # "eventColor":I
    .end local v18    # "eventDuration":J
    .end local v27    # "haveDay":I
    .end local v31    # "location":I
    .end local v44    # "startDay":I
    .end local v54    # "title":Ljava/lang/String;
    :cond_2a
    const/16 v30, 0x0

    .local v30, "jj":I
    :goto_10
    move/from16 v0, v30

    move/from16 v1, v39

    if-ge v0, v1, :cond_30

    .line 1996
    move-object/from16 v0, v22

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v21

    check-cast v21, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    .line 1998
    .local v21, "eventTask":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    move-object/from16 v0, v21

    iget v14, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    .line 1999
    .restart local v14    # "eventColor":I
    move-object/from16 v0, v21

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    move-wide/from16 v56, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    move-object/from16 v58, v0

    move-object/from16 v0, v58

    iget-wide v0, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v58, v0

    invoke-static/range {v56 .. v59}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v10

    .line 2000
    .local v10, "dueDate":I
    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    move-object/from16 v56, v0

    invoke-virtual/range {v56 .. v56}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v56

    move-object/from16 v0, p0

    move-object/from16 v1, v56

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawTextSanitizer(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v54

    .line 2001
    .restart local v54    # "title":Ljava/lang/String;
    sub-int v27, v10, v34

    .line 2002
    .restart local v27    # "haveDay":I
    const/16 v31, -0x1

    .line 2004
    .restart local v31    # "location":I
    if-ltz v27, :cond_2d

    move/from16 v0, v27

    move/from16 v1, v37

    if-ge v0, v1, :cond_2d

    .line 2006
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    move-object/from16 v56, v0

    aget v57, v56, v27

    add-int/lit8 v57, v57, 0x1

    aput v57, v56, v27

    .line 2008
    const/16 v29, 0x0

    :goto_11
    const/16 v56, 0xb

    move/from16 v0, v29

    move/from16 v1, v56

    if-ge v0, v1, :cond_2c

    .line 2009
    aget-object v56, v13, v27

    aget-object v56, v56, v29

    if-eqz v56, :cond_2b

    aget-object v56, v13, v27

    aget-object v56, v56, v29

    invoke-virtual/range {v56 .. v56}, Ljava/lang/String;->length()I

    move-result v56

    if-nez v56, :cond_2e

    .line 2010
    :cond_2b
    move/from16 v31, v29

    .line 2015
    :cond_2c
    const/16 v56, -0x1

    move/from16 v0, v31

    move/from16 v1, v56

    if-ne v0, v1, :cond_2f

    .line 1994
    :cond_2d
    :goto_12
    add-int/lit8 v30, v30, 0x1

    goto :goto_10

    .line 2008
    :cond_2e
    add-int/lit8 v29, v29, 0x1

    goto :goto_11

    .line 2018
    :cond_2f
    aget-object v56, v13, v27

    new-instance v57, Ljava/lang/StringBuilder;

    invoke-direct/range {v57 .. v57}, Ljava/lang/StringBuilder;-><init>()V

    const-string v58, "|T|"

    invoke-virtual/range {v57 .. v58}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    move-object/from16 v0, v57

    move-object/from16 v1, v54

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v57

    invoke-virtual/range {v57 .. v57}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v57

    aput-object v57, v56, v31

    .line 2019
    aget-object v56, v15, v27

    aput v14, v56, v31

    .line 2020
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskComplete:[[Z

    move-object/from16 v56, v0

    aget-object v56, v56, v27

    move-object/from16 v0, v21

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    move/from16 v57, v0

    aput-boolean v57, v56, v31

    goto :goto_12

    .line 2024
    .end local v10    # "dueDate":I
    .end local v14    # "eventColor":I
    .end local v21    # "eventTask":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    .end local v27    # "haveDay":I
    .end local v31    # "location":I
    .end local v54    # "title":Ljava/lang/String;
    :cond_30
    move-object/from16 v0, v43

    invoke-virtual {v0, v13}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2025
    move-object/from16 v0, v43

    invoke-virtual {v0, v15}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2026
    move-object/from16 v0, v43

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 2027
    return-object v43
.end method

.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 827
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v0, :cond_0

    .line 828
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    invoke-virtual {v0}, Landroid/graphics/Bitmap;->recycle()V

    .line 829
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    .line 831
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    if-eqz v0, :cond_1

    .line 832
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->clear()V

    .line 834
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

    if-eqz v0, :cond_2

    .line 835
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->cancelOperation(I)V

    .line 836
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    .line 838
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    if-eqz v0, :cond_3

    .line 839
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    invoke-virtual {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->cancelOperation(I)V

    .line 840
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    .line 842
    :cond_3
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 843
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .line 844
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    .line 845
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPaint:Landroid/graphics/Paint;

    .line 846
    return-void
.end method

.method public clearCachedEvents()V
    .locals 2

    .prologue
    .line 639
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLastReloadMillis:J

    .line 640
    return-void
.end method

.method public getEventList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1721
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEvents:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getPrevNextTime(II)Landroid/text/format/Time;
    .locals 6
    .param p1, "monthOffset"    # I
    .param p2, "selectedDay"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v3, 0x0

    .line 1729
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-object v2, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 1730
    .local v1, "time":Landroid/text/format/Time;
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1731
    iget v2, v1, Landroid/text/format/Time;->month:I

    add-int/2addr v2, p1

    iput v2, v1, Landroid/text/format/Time;->month:I

    .line 1732
    iput v3, v1, Landroid/text/format/Time;->hour:I

    .line 1733
    iput v3, v1, Landroid/text/format/Time;->minute:I

    .line 1734
    iput v3, v1, Landroid/text/format/Time;->second:I

    .line 1735
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    iget v2, v1, Landroid/text/format/Time;->year:I

    iget v3, v1, Landroid/text/format/Time;->month:I

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    invoke-direct {v0, v2, v3, v5, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;-><init>(IIII)V

    .line 1737
    .local v0, "cursor":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;
    if-eqz p2, :cond_1

    .line 1738
    iput p2, v1, Landroid/text/format/Time;->monthDay:I

    .line 1742
    :goto_0
    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    if-gtz v2, :cond_0

    .line 1743
    iput v5, v1, Landroid/text/format/Time;->monthDay:I

    .line 1745
    :cond_0
    return-object v1

    .line 1740
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getSelectedDayOfMonth()I

    move-result v2

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->min(II)I

    move-result v2

    iput v2, v1, Landroid/text/format/Time;->monthDay:I

    goto :goto_0
.end method

.method public getSelectedDate()Landroid/text/format/Time;
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 1627
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTempTime:Landroid/text/format/Time;

    .line 1628
    .local v0, "time":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1629
    iget v1, v0, Landroid/text/format/Time;->month:I

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getSelectedMonthOffset()I

    move-result v2

    add-int/2addr v1, v2

    iput v1, v0, Landroid/text/format/Time;->month:I

    .line 1630
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getSelectedDayOfMonth()I

    move-result v1

    iput v1, v0, Landroid/text/format/Time;->monthDay:I

    .line 1631
    iput v3, v0, Landroid/text/format/Time;->hour:I

    .line 1632
    iput v3, v0, Landroid/text/format/Time;->minute:I

    .line 1633
    iput v3, v0, Landroid/text/format/Time;->second:I

    .line 1634
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    .line 1635
    return-object v0
.end method

.method public getSelectedMillis()J
    .locals 2

    .prologue
    .line 1670
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getSelectedDate()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    return-wide v0
.end method

.method public getSelectionMode()I
    .locals 1

    .prologue
    .line 1713
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    return v0
.end method

.method protected getStartDay()I
    .locals 1

    .prologue
    .line 1701
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    return v0
.end method

.method public getTaskList()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation

    .prologue
    .line 1725
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTasksArray:Ljava/util/ArrayList;

    return-object v0
.end method

.method public getTime()Landroid/text/format/Time;
    .locals 1

    .prologue
    .line 1709
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    return-object v0
.end method

.method public init(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 14
    .param p1, "mSurfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 282
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    .line 283
    .local v8, "res":Landroid/content/res/Resources;
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setFocusable(Z)V

    .line 284
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setFocusableInTouchMode(Z)V

    .line 285
    const/4 v9, 0x1

    invoke-virtual {p0, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setClickable(Z)V

    .line 287
    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-direct {v9, p0, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/content/ContentResolver;)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    .line 288
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9}, Ljava/util/ArrayList;-><init>()V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoEventArray:Ljava/util/ArrayList;

    .line 289
    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    invoke-direct {v9, p0, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/content/ContentResolver;)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

    .line 290
    new-instance v9, Landroid/text/format/Time;

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    const/4 v11, 0x0

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v10

    invoke-direct {v9, v10}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    .line 291
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 292
    .local v6, "now":J
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v9, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 293
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    const/4 v10, 0x1

    iput v10, v9, Landroid/text/format/Time;->monthDay:I

    .line 294
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    .line 295
    .local v4, "millis":J
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-wide v10, v9, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    .line 296
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setNextFirstJulianDay()I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNextFirstJulianDay:I

    .line 297
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v9, v6, v7}, Landroid/text/format/Time;->set(J)V

    .line 298
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mToday:Landroid/text/format/Time;

    .line 299
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mToday:Landroid/text/format/Time;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Landroid/text/format/Time;->set(J)V

    .line 300
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    if-nez v9, :cond_0

    .line 301
    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget v10, v10, Landroid/text/format/Time;->year:I

    iget-object v11, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget v11, v11, Landroid/text/format/Time;->month:I

    iget-object v12, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget v12, v12, Landroid/text/format/Time;->monthDay:I

    iget v13, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    invoke-direct {v9, v10, v11, v12, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;-><init>(IIII)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    .line 304
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    new-instance v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$1;

    invoke-direct {v10, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)V

    invoke-virtual {v9, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->setOnCursorMoveListener(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;)V

    .line 313
    :cond_0
    const v9, 0x7f02002e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckBoxOn:Landroid/graphics/drawable/Drawable;

    .line 314
    const v9, 0x7f02002d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckBoxOff:Landroid/graphics/drawable/Drawable;

    .line 315
    const v9, 0x7f020025

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAttachMemo:Landroid/graphics/drawable/Drawable;

    .line 316
    const v9, 0x7f07000d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFocusedBackground:Landroid/graphics/drawable/Drawable;

    .line 317
    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-eqz v9, :cond_1

    .line 318
    const v9, 0x7f020017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectBGDrawable:Landroid/graphics/drawable/Drawable;

    .line 319
    const v9, 0x7f020018

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFestivalEffectWorkingDrawable:Landroid/graphics/drawable/Drawable;

    .line 322
    :cond_1
    const v9, 0x7f07000a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBGColor:I

    .line 323
    const v9, 0x7f070040

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWeekNumColor:I

    .line 324
    const v9, 0x7f07001c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberColor:I

    .line 325
    const v9, 0x7f070025

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTodayFocusedNumberColor:I

    .line 326
    const v9, 0x7f07003c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTodayNumberColor:I

    .line 327
    const v9, 0x7f070036

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    .line 328
    const v9, 0x7f070034

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberColor:I

    .line 329
    const v9, 0x7f070026

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLineColor:I

    .line 330
    const v9, 0x7f07001f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountTextColor:I

    .line 331
    const v9, 0x7f07001b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountTextDimColor:I

    .line 332
    const v9, 0x7f07001e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthEventCountBGColor:I

    .line 333
    const v9, 0x7f07003e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventColor:I

    .line 334
    const v9, 0x7f07005d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthViewTextEventDimColor:I

    .line 335
    const v9, 0x7f07001d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberDimColor:I

    .line 336
    const v9, 0x7f070037

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    .line 337
    const v9, 0x7f070035

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberDimColor:I

    .line 338
    const v9, 0x7f070033

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayMulColor:I

    .line 339
    const v9, 0x7f070032

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthOverlayAddColor:I

    .line 341
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->updateColorSet()V

    .line 343
    const v9, 0x7f090024

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstColumnCellWidth:I

    .line 344
    const v9, 0x7f090016

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthColumnExtraWidth:I

    .line 345
    const v9, 0x7f090014

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    .line 346
    const v9, 0x7f08003f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellWidth:I

    .line 347
    const v9, 0x7f080073

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayMakerSize:I

    .line 348
    const v9, 0x7f08004f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountTextMarginRight:I

    .line 349
    const v9, 0x7f090023

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountTextMarginTop:I

    .line 350
    const v9, 0x7f08004d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountBGHeight:I

    .line 351
    const v9, 0x7f080077

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageTopMargine:I

    .line 352
    const v9, 0x7f08004e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventCountBGOvalRadius:I

    .line 353
    const v9, 0x7f09001b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMarginLeft:I

    .line 354
    const v9, 0x7f09002f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskTitleMarginLeft:I

    .line 355
    const v9, 0x7f09002e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskTitleAvailAdjust:I

    .line 356
    const v9, 0x7f09000d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleAvailAdjust:I

    .line 357
    const v9, 0x7f090018

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextSize:I

    .line 358
    const v9, 0x7f090018

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mPrevNextDayTextSize:I

    .line 359
    const v9, 0x7f090003

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextPaddingLeft:I

    .line 360
    const v9, 0x7f090004

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayTextPaddingTop:I

    .line 361
    const v9, 0x7f09000e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleTextSize:I

    .line 362
    const v9, 0x7f090017

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayCountTextSize:I

    .line 363
    const v9, 0x7f090033

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginLeft:I

    .line 364
    const v9, 0x7f090035

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTodayBackgroundMarginTop:I

    .line 365
    const v9, 0x7f080045

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    .line 366
    const v9, 0x7f080024

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    .line 367
    const v9, 0x7f080046

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIntervalBetweenEventBox:I

    .line 368
    const v9, 0x7f08004c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventTitleMargineTop:I

    .line 369
    const v9, 0x7f08003b

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAccountColorBarWidth:I

    .line 370
    const v9, 0x7f080039

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAccountColorBarMarginRight:I

    .line 371
    const v9, 0x7f080041

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckboxMarginTopFromBoxTop:I

    .line 372
    const v9, 0x7f080040

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCheckboxWidth:I

    .line 373
    const v9, 0x7f090009

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow5:I

    .line 374
    const v9, 0x7f09000a

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow6:I

    .line 375
    const v9, 0x7f080052

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLineWidth:I

    .line 376
    const v9, 0x7f08006c

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    .line 377
    const v9, 0x7f08009f

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageWidthSize:I

    .line 378
    const v9, 0x7f08009e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mWeatherImageMarginLeft:I

    .line 379
    const v9, 0x7f090029

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionStrokeWidth:I

    .line 380
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getSolarLunarConverter()Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    move-result-object v9

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSolarLunarConverter:Lcom/android/calendar/secfeature/lunarcalendar/SolarLunarConverter;

    .line 381
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->isLunarCalendarSupported()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLunarCalendarSupported:Z

    .line 382
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mAreNationalHolidaysSupported:Z

    .line 383
    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLunarCalendarSupported:Z

    if-eqz v9, :cond_8

    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsChinaFeature:Z

    if-nez v9, :cond_2

    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsHKTWFeature:Z

    if-nez v9, :cond_2

    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsVietNameseFeature:Z

    if-eqz v9, :cond_8

    :cond_2
    const/4 v9, 0x1

    :goto_0
    iput-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLunarDateDisplaySupported:Z

    .line 385
    iget-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsLunarDateDisplaySupported:Z

    if-eqz v9, :cond_3

    .line 386
    const v9, 0x7f080025

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    .line 387
    const v9, 0x7f080043

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayCountTextSize:I

    .line 388
    const v9, 0x7f07002e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLunarDateColor:I

    .line 389
    const v9, 0x7f08005d

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarTextSize:I

    .line 390
    const v9, 0x7f080032

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDateYOffset:I

    .line 391
    new-instance v9, Landroid/graphics/Paint;

    invoke-direct {v9}, Landroid/graphics/Paint;-><init>()V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    .line 392
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setFakeBoldText(Z)V

    .line 393
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    const/4 v10, 0x1

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setAntiAlias(Z)V

    .line 394
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarTextSize:I

    int-to-float v10, v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 395
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthLunarDateColor:I

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setColor(I)V

    .line 396
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Style;->FILL:Landroid/graphics/Paint$Style;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setStyle(Landroid/graphics/Paint$Style;)V

    .line 397
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    sget-object v10, Landroid/graphics/Paint$Align;->LEFT:Landroid/graphics/Paint$Align;

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTextAlign(Landroid/graphics/Paint$Align;)V

    .line 398
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarDatePaint:Landroid/graphics/Paint;

    const/4 v10, 0x0

    invoke-static {v10}, Landroid/graphics/Typeface;->defaultFromStyle(I)Landroid/graphics/Typeface;

    move-result-object v10

    invoke-virtual {v9, v10}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 399
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    const v10, 0x7f0a0020

    invoke-virtual {v9, v10}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v9

    sput-object v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLunarLeapString:Ljava/lang/String;

    .line 402
    :cond_3
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isUSAWeekNumber()Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v9

    iput-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIsUsaWeekNumber:Z

    .line 403
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v2

    .line 404
    .local v2, "maxDay":I
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v9, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v9

    add-int/lit8 v9, v9, 0x1

    sput v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 405
    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow6:I

    sput v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    .line 406
    sget v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    const/4 v10, 0x6

    if-eq v9, v10, :cond_4

    .line 407
    const/4 v9, 0x5

    sput v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 408
    const v9, 0x7f090013

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    .line 409
    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventNumOfMonthRow5:I

    sput v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TITLE_EVENT_NUMBER:I

    .line 412
    :cond_4
    const v9, 0x7f08007e

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v9

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWidth:I

    .line 413
    sget v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCellHeight:I

    mul-int/2addr v9, v10

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    .line 414
    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    iget v11, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIntervalBetweenEventBox:I

    add-int/2addr v10, v11

    div-int/2addr v9, v10

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    .line 417
    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerImageSize:I

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxMarginBottom:I

    sub-int/2addr v9, v10

    iget v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventBoxHeight:I

    iget v11, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIntervalBetweenEventBox:I

    add-int/2addr v10, v11

    if-le v9, v10, :cond_5

    .line 418
    const/4 v9, 0x1

    iput v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mIconOccupiedEventLines:I

    .line 421
    :cond_5
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v9

    const-string v10, "JAPAN"

    invoke-virtual {v9, v10}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v9

    if-eqz v9, :cond_6

    .line 422
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v9}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v9

    if-eqz v9, :cond_6

    .line 423
    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

    invoke-direct {v9, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;-><init>(Landroid/content/Context;)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mHolidayManager:Lcom/sec/android/widgetapp/SPlannerAppWidget/HolidayManager;

    .line 426
    :cond_6
    iget-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    const/4 v10, -0x1

    invoke-static {v10}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    invoke-static {v9, v10}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 427
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->clearStickerInfo()V

    .line 429
    const-string v9, "activity"

    invoke-virtual {p1, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/app/ActivityManager;

    invoke-virtual {v9}, Landroid/app/ActivityManager;->getMemoryClass()I

    move-result v3

    .line 432
    .local v3, "memClass":I
    const/high16 v9, 0x100000

    mul-int/2addr v9, v3

    div-int/lit8 v0, v9, 0x8

    .line 433
    .local v0, "cacheSize":I
    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$2;

    invoke-direct {v9, p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$2;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;I)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStickerBitmapCache:Landroid/util/LruCache;

    .line 443
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getSettingsFontStyle(Landroid/content/Context;)I

    move-result v1

    .line 444
    .local v1, "fontIndexDB":I
    if-eqz v1, :cond_7

    .line 445
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->isFontStyleDefault:Z

    .line 448
    :cond_7
    new-instance v9, Landroid/view/GestureDetector;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getContext()Landroid/content/Context;

    move-result-object v10

    new-instance v11, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$3;

    invoke-direct {v11, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$3;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)V

    invoke-direct {v9, v10, v11}, Landroid/view/GestureDetector;-><init>(Landroid/content/Context;Landroid/view/GestureDetector$OnGestureListener;)V

    iput-object v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mGestureDetector:Landroid/view/GestureDetector;

    .line 602
    return-void

    .line 383
    .end local v0    # "cacheSize":I
    .end local v1    # "fontIndexDB":I
    .end local v2    # "maxDay":I
    .end local v3    # "memClass":I
    :cond_8
    const/4 v9, 0x0

    goto/16 :goto_0
.end method

.method public initEvents()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 2546
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    const/16 v2, 0x2b

    if-ge v0, v2, :cond_1

    .line 2547
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_1
    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 2548
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventArray:[[Ljava/lang/String;

    aget-object v2, v2, v0

    const/4 v3, 0x0

    aput-object v3, v2, v1

    .line 2549
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventColorArray:[[I

    aget-object v2, v2, v0

    aput v4, v2, v1

    .line 2550
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventDayGapArray:[[I

    aget-object v2, v2, v0

    aput v4, v2, v1

    .line 2547
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 2546
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 2554
    .end local v1    # "j":I
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDayEventCount:[I

    invoke-static {v2, v4}, Ljava/util/Arrays;->fill([II)V

    .line 2555
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    const/4 v3, -0x1

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-static {v2, v3}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 2556
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->clearStickerInfo()V

    .line 2557
    return-void
.end method

.method public isBackgroundSticker(J)Z
    .locals 3
    .param p1, "stickerGroup"    # J

    .prologue
    .line 1574
    const-wide/16 v0, 0xa

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onAttachedToWindow()V
    .locals 0

    .prologue
    .line 817
    invoke-super {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->onAttachedToWindow()V

    .line 818
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 822
    invoke-super {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->onDetachedFromWindow()V

    .line 824
    return-void
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 5
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 864
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    if-eqz v1, :cond_5

    .line 865
    :cond_0
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->updateTodayButton()V

    .line 866
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    if-nez v1, :cond_3

    .line 867
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getWidth()I

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWidth:I

    if-eq v1, v2, :cond_2

    .line 868
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    const-string v2, "getWidth() is different from actual width"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 870
    :cond_2
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWidth:I

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->drawingCalc(I)V

    .line 873
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchTodayTimeZone(Landroid/content/Context;)I

    .line 875
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    if-eqz v1, :cond_4

    .line 877
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCanvas:Landroid/graphics/Canvas;

    .line 878
    .local v0, "bitmapCanvas":Landroid/graphics/Canvas;
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBGColor:I

    invoke-virtual {v0, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 880
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->doDraw(Landroid/graphics/Canvas;)V

    .line 881
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->doDrawText(Landroid/graphics/Canvas;)V

    .line 882
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    .line 889
    .end local v0    # "bitmapCanvas":Landroid/graphics/Canvas;
    :cond_4
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    if-eqz v1, :cond_6

    .line 890
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBitmapRect:Landroid/graphics/Rect;

    const/4 v4, 0x0

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Rect;Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 898
    :goto_1
    return-void

    .line 885
    :cond_5
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    const-string v2, "No need to redraw month view"

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 892
    :cond_6
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    const-string v2, "mBitmap is null. Can not draw month view "

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 893
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mBGColor:I

    invoke-virtual {p1, v1}, Landroid/graphics/Canvas;->drawColor(I)V

    .line 894
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->doDraw(Landroid/graphics/Canvas;)V

    .line 895
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->doDrawText(Landroid/graphics/Canvas;)V

    .line 896
    iput-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    goto :goto_1
.end method

.method protected onMeasure(II)V
    .locals 2
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 850
    invoke-super {p0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->onMeasure(II)V

    .line 851
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWidth:I

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthHeight:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setMeasuredDimension(II)V

    .line 853
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getMeasuredWidth()I

    move-result v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthWidth:I

    if-eq v0, v1, :cond_0

    .line 854
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->TAG:Ljava/lang/String;

    const-string v1, "getMeasuredWidth() is different from actual width"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 856
    :cond_0
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 631
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mGestureDetector:Landroid/view/GestureDetector;

    invoke-virtual {v0, p1}, Landroid/view/GestureDetector;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 632
    const/4 v0, 0x1

    .line 634
    :goto_0
    return v0

    :cond_0
    invoke-super {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method protected reloadEvents()V
    .locals 1

    .prologue
    .line 664
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents(Z)V

    .line 665
    return-void
.end method

.method protected reloadEvents(Z)V
    .locals 14
    .param p1, "bIsInCurrentMonth"    # Z

    .prologue
    const/4 v1, 0x0

    const/4 v13, 0x1

    .line 668
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->startBackgroundThread()V

    .line 669
    if-nez p1, :cond_0

    .line 670
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->initEvents()V

    .line 674
    :cond_0
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 675
    .local v12, "monthStart":Landroid/text/format/Time;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 676
    iput v13, v12, Landroid/text/format/Time;->monthDay:I

    .line 677
    iput v1, v12, Landroid/text/format/Time;->hour:I

    .line 678
    iput v1, v12, Landroid/text/format/Time;->minute:I

    .line 679
    iput v1, v12, Landroid/text/format/Time;->second:I

    .line 680
    invoke-virtual {v12, v13}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 683
    .local v10, "millis":J
    iget-wide v0, v12, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v10, v11, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    .line 684
    .local v3, "monthStartDay":I
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v0, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v6

    .line 685
    .local v6, "columnDay1":I
    sub-int v0, v3, v6

    add-int/lit8 v3, v0, 0x1

    .line 688
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v7

    .line 689
    .local v7, "maxDay":I
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    invoke-virtual {v0, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 690
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    const/4 v1, 0x6

    if-eq v0, v1, :cond_1

    .line 691
    const/4 v0, 0x5

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    .line 693
    :cond_1
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->MONTH_ROW_NUM:I

    mul-int/lit8 v0, v0, 0x7

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNumDays:I

    .line 696
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    .line 697
    .local v8, "current":J
    iget-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLastReloadMillis:J

    cmp-long v0, v8, v0

    if-nez v0, :cond_3

    .line 740
    :cond_2
    :goto_0
    return-void

    .line 700
    :cond_3
    iput-wide v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mLastReloadMillis:J

    .line 703
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 705
    .local v2, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNumDays:I

    new-instance v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;

    invoke-direct {v4, p0, v2, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$5;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Ljava/util/ArrayList;Z)V

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCancelCallback:Ljava/lang/Runnable;

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->loadEventsInBackground(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 732
    add-int/lit8 v0, v6, -0x1

    rsub-int/lit8 v0, v0, 0x1

    iput v0, v12, Landroid/text/format/Time;->monthDay:I

    .line 733
    invoke-virtual {v12, v13}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 735
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNumDays:I

    add-int/lit8 v1, v6, -0x1

    add-int/2addr v0, v1

    invoke-virtual {p0, v10, v11, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadTasks(JI)V

    .line 736
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNumDays:I

    add-int/lit8 v1, v6, -0x1

    add-int/2addr v0, v1

    invoke-virtual {p0, v12, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadMemo(Landroid/text/format/Time;I)V

    .line 737
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    invoke-virtual {v0}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->areNationalHolidaysSupported()Z

    move-result v0

    if-ne v0, v13, :cond_2

    .line 738
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->displaySubstHolidaysCalendar(Landroid/content/Context;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mDisplaySubstHolidays:Z

    goto :goto_0
.end method

.method public reloadMemo(Landroid/text/format/Time;I)V
    .locals 10
    .param p1, "monthStart"    # Landroid/text/format/Time;
    .param p2, "monthDay"    # I

    .prologue
    const/4 v2, 0x0

    .line 785
    const/4 v0, 0x1

    invoke-virtual {p1, v0}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v0

    iget-wide v6, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v9

    .line 786
    .local v9, "startDate":I
    add-int v8, v9, p2

    .line 787
    .local v8, "endDate":I
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "date >= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " AND date <= "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 789
    .local v5, "selection":Ljava/lang/String;
    const-string v0, "content://com.android.calendar/qsmemos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    .line 790
    .local v3, "qsMemoUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;

    const/4 v1, 0x0

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->QSMEMOS_PROJECTION:[Ljava/lang/String;

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 791
    return-void
.end method

.method public reloadTasks(JI)V
    .locals 11
    .param p1, "monthStart"    # J
    .param p3, "monthDay"    # I

    .prologue
    const/4 v2, 0x0

    .line 743
    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 744
    .local v3, "tasksUri":Landroid/net/Uri;
    const-wide/32 v0, 0x5265c00

    int-to-long v6, p3

    mul-long/2addr v0, v6

    add-long v8, p1, v0

    .line 745
    .local v8, "endMillis":J
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    invoke-static {v0, p1, p2, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTaskSelection(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v5

    .line 746
    .local v5, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;

    const/4 v1, 0x0

    const-string v7, " utc_due_date ASC, importance DESC"

    move-object v4, v2

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 748
    return-void
.end method

.method protected setEventLoader(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)V
    .locals 0
    .param p1, "eventLoader"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 1705
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 1706
    return-void
.end method

.method public setRedrawScreen(Z)V
    .locals 0
    .param p1, "reDraw"    # Z

    .prologue
    .line 2621
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    .line 2622
    return-void
.end method

.method public setSelectedTime(Landroid/text/format/Time;)V
    .locals 8
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v7, 0x1

    .line 1675
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v2, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1676
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iput v7, v2, Landroid/text/format/Time;->monthDay:I

    .line 1677
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v2, v7}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 1678
    .local v0, "millis":J
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-wide v2, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mFirstJulianDay:I

    .line 1679
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setNextFirstJulianDay()I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mNextFirstJulianDay:I

    .line 1680
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v2, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 1682
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    iget v3, p1, Landroid/text/format/Time;->year:I

    iget v4, p1, Landroid/text/format/Time;->month:I

    iget v5, p1, Landroid/text/format/Time;->monthDay:I

    iget v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    invoke-direct {v2, v3, v4, v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;-><init>(IIII)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    .line 1683
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    iget v3, p1, Landroid/text/format/Time;->monthDay:I

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->setSelectedDayOfMonth(I)V

    .line 1684
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mCursor:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$6;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$6;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)V

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->setOnCursorMoveListener(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;)V

    .line 1692
    iput-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mRedrawScreen:Z

    .line 1693
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->invalidate()V

    .line 1694
    return-void
.end method

.method public setSelectionMode(I)V
    .locals 0
    .param p1, "selectionMode"    # I

    .prologue
    .line 1717
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mSelectionMode:I

    .line 1718
    return-void
.end method

.method protected setStartDay(I)V
    .locals 0
    .param p1, "startDay"    # I

    .prologue
    .line 1697
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mStartDay:I

    .line 1698
    return-void
.end method

.method protected updateColorSet()V
    .locals 12

    .prologue
    const/4 v11, 0x4

    const/4 v7, 0x3

    const/4 v10, 0x2

    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 2441
    new-array v0, v7, [I

    .line 2442
    .local v0, "colorSet":[I
    new-array v2, v7, [I

    .line 2444
    .local v2, "dimmedColorSet":[I
    const-string v1, "XXXXXXR"

    .line 2446
    .local v1, "defaultWeekdayFeatureString":Ljava/lang/String;
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberColor:I

    aput v7, v0, v9

    .line 2447
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberColor:I

    aput v7, v0, v8

    .line 2448
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    aput v7, v0, v10

    .line 2450
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberDimColor:I

    aput v7, v2, v9

    .line 2451
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberDimColor:I

    aput v7, v2, v8

    .line 2452
    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    aput v7, v2, v10

    .line 2455
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v7

    const-string v8, "CscFeature_Calendar_SetColorOfDays"

    invoke-virtual {v7, v8}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 2461
    .local v6, "weekdayFeatureString":Ljava/lang/String;
    :goto_0
    invoke-static {v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->parseWeekdayColor(Ljava/lang/String;[I)[I

    move-result-object v4

    .line 2462
    .local v4, "mDayNumberColors":[I
    invoke-static {v6, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->parseWeekdayColor(Ljava/lang/String;[I)[I

    move-result-object v5

    .line 2464
    .local v5, "mDimmedDayNumberColors":[I
    aget v7, v4, v9

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberColor:I

    .line 2465
    aget v7, v4, v11

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthFridayNumberColor:I

    .line 2466
    const/4 v7, 0x5

    aget v7, v4, v7

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberColor:I

    .line 2467
    const/4 v7, 0x6

    aget v7, v4, v7

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberColor:I

    .line 2469
    aget v7, v5, v9

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthDayNumberDimColor:I

    .line 2470
    aget v7, v5, v11

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthFridayNumberDimColor:I

    .line 2471
    const/4 v7, 0x5

    aget v7, v5, v7

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSaturdayNumberDimColor:I

    .line 2472
    const/4 v7, 0x6

    aget v7, v5, v7

    iput v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthSundayNumberDimColor:I

    .line 2473
    return-void

    .line 2457
    .end local v4    # "mDayNumberColors":[I
    .end local v5    # "mDimmedDayNumberColors":[I
    .end local v6    # "weekdayFeatureString":Ljava/lang/String;
    :catch_0
    move-exception v3

    .line 2458
    .local v3, "e":Ljava/lang/Exception;
    move-object v6, v1

    .restart local v6    # "weekdayFeatureString":Ljava/lang/String;
    goto :goto_0
.end method

.method public updateTimezone()V
    .locals 4

    .prologue
    .line 650
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    if-eqz v2, :cond_0

    .line 651
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    iget-object v1, v2, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 652
    .local v1, "previousTimezone":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    .line 654
    .local v0, "currentTimezone":Ljava/lang/String;
    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 655
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    invoke-virtual {v2, v0}, Landroid/text/format/Time;->switchTimezone(Ljava/lang/String;)V

    .line 656
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mViewCalendar:Landroid/text/format/Time;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    .line 658
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents()V

    .line 661
    .end local v0    # "currentTimezone":Ljava/lang/String;
    .end local v1    # "previousTimezone":Ljava/lang/String;
    :cond_0
    return-void
.end method
