.class public interface abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils$IAccountTypeConstants;
.super Ljava/lang/Object;
.source "Utils.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "IAccountTypeConstants"
.end annotation


# static fields
.field public static final ACCOUNT_KNOX_NAME_ONE_MYTASK_KNOX:Ljava/lang/String; = "My task (KNOX)"

.field public static final ACCOUNT_NAME_MYCALENDAR:Ljava/lang/String; = "My calendar"

.field public static final ACCOUNT_NAME_MYCALENDAR_KNOX:Ljava/lang/String; = "My calendar (personal)"

.field public static final ACCOUNT_NAME_MYTASK:Ljava/lang/String; = "My Task"

.field public static final ACCOUNT_NAME_MYTASK_KNOX:Ljava/lang/String; = "My Task (personal)"

.field public static final ACCOUNT_TYPE_EXCHANGE:Ljava/lang/String; = "com.android.exchange"

.field public static final ACCOUNT_TYPE_FACEBOOK:Ljava/lang/String; = "com.sec.android.app.sns3.facebook"

.field public static final ACCOUNT_TYPE_GOOGLE:Ljava/lang/String; = "com.google"

.field public static final ACCOUNT_TYPE_MY_SINGLE:Ljava/lang/String; = "com.seven.Z7.work"

.field public static final ACCOUNT_TYPE_SAMSUNG:Ljava/lang/String; = "com.osp.app.signin"

.field public static final ACCOUNT_TYPE_SHAREPOINT:Ljava/lang/String; = "com.android.sharepoint"

.field public static final ICSUP_ACCOUNT_TYPE_FACEBOOK:Ljava/lang/String; = "com.sec.android.app.snsaccountfacebook.account_type"
