.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final all_day_event_title_width:I = 0x7f090000

.field public static final checkbox_position_bottom:I = 0x7f090001

.field public static final checkbox_position_top:I = 0x7f090002

.field public static final day_text_padding_left:I = 0x7f090003

.field public static final day_text_padding_top:I = 0x7f090004

.field public static final daynum_shadow_dx:I = 0x7f090005

.field public static final daynum_shadow_dy:I = 0x7f090006

.field public static final daynum_shadow_radius:I = 0x7f090007

.field public static final draw_text_position:I = 0x7f090008

.field public static final event_num_of_month_row_5:I = 0x7f090009

.field public static final event_num_of_month_row_6:I = 0x7f09000a

.field public static final event_num_of_month_row_6_full:I = 0x7f090037

.field public static final event_row_padding_land:I = 0x7f09000b

.field public static final event_row_padding_port:I = 0x7f09000c

.field public static final event_title_avail_adjust:I = 0x7f09000d

.field public static final event_title_text_size:I = 0x7f09000e

.field public static final highlight_sticker_margin_left:I = 0x7f09000f

.field public static final highlight_sticker_margin_top:I = 0x7f090010

.field public static final interval_between_checkboxs:I = 0x7f090011

.field public static final interval_between_event_titles:I = 0x7f090012

.field public static final month_cell_height_5:I = 0x7f090013

.field public static final month_cell_height_6:I = 0x7f090014

.field public static final month_cell_width:I = 0x7f090015

.field public static final month_column_extra_width:I = 0x7f090016

.field public static final month_count_size:I = 0x7f090017

.field public static final month_day_text_size:I = 0x7f090018

.field public static final month_event_height:I = 0x7f090019

.field public static final month_event_title_firstrow_position:I = 0x7f09001a

.field public static final month_event_title_margin_left:I = 0x7f09001b

.field public static final month_event_width:I = 0x7f09001c

.field public static final month_eventbox_margin:I = 0x7f09001d

.field public static final month_eventbox_position:I = 0x7f09001e

.field public static final month_eventcount_bg_margin_bottom:I = 0x7f09001f

.field public static final month_eventcount_bg_margin_left:I = 0x7f090020

.field public static final month_eventcount_bg_margin_left_full:I = 0x7f090036

.field public static final month_eventcount_bg_margin_right:I = 0x7f090021

.field public static final month_eventcount_bg_margin_top:I = 0x7f090022

.field public static final month_eventcount_position_top:I = 0x7f090023

.field public static final month_first_column_width:I = 0x7f090024

.field public static final month_focus_day_lunar_text_size:I = 0x7f090025

.field public static final month_focus_day_text_size:I = 0x7f090026

.field public static final month_prevnextday_text_size:I = 0x7f090027

.field public static final month_row_line_padding_left:I = 0x7f090028

.field public static final month_selection_stroke_width:I = 0x7f090029

.field public static final month_week_banner_height:I = 0x7f09002a

.field public static final month_week_text_size:I = 0x7f09002b

.field public static final month_widget_event_text_top_adjusment:I = 0x7f09002c

.field public static final single_event_title_width:I = 0x7f09002d

.field public static final task_title_avail_adjust:I = 0x7f09002e

.field public static final task_title_margin_left:I = 0x7f09002f

.field public static final text_Y_padding_1:I = 0x7f090030

.field public static final text_Y_padding_2:I = 0x7f090031

.field public static final today_background_margin_bottom:I = 0x7f090032

.field public static final today_background_margin_left:I = 0x7f090033

.field public static final today_background_margin_right:I = 0x7f090034

.field public static final today_background_margin_top:I = 0x7f090035


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 525
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
