.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService;
.super Landroid/widget/RemoteViewsService;
.source "MagazineDayViewService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService$CalendarFactory;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String; = "MagazineDayViewService"

.field private static final TASK_QUERY_TOKEN:I

.field private static mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 61
    invoke-direct {p0}, Landroid/widget/RemoteViewsService;-><init>()V

    .line 73
    return-void
.end method

.method static synthetic access$000()Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    .locals 1

    .prologue
    .line 61
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .prologue
    .line 61
    sput-object p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    return-object p0
.end method


# virtual methods
.method public onGetViewFactory(Landroid/content/Intent;)Landroid/widget/RemoteViewsService$RemoteViewsFactory;
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 70
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService$CalendarFactory;

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService$CalendarFactory;-><init>(Landroid/content/Context;Landroid/content/Intent;)V

    return-object v0
.end method
