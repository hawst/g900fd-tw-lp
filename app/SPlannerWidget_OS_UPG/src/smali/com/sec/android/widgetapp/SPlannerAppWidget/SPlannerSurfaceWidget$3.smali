.class Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;
.super Landroid/os/Handler;
.source "SPlannerSurfaceWidget.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 0

    .prologue
    .line 117
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 121
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 163
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 165
    :cond_0
    :goto_0
    return-void

    .line 123
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 124
    .local v2, "value":Ljava/lang/String;
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$100()Ljava/lang/String;

    move-result-object v3

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SPlannerSurfaceWidget.handleMessage] value: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 125
    const-string v3, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "android.intent.action.TIME_SET"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "clock.date_format_changed"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    const-string v3, "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 132
    :cond_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchEntirePreference(Landroid/content/Context;)V

    .line 134
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    .line 135
    .local v1, "refreshHandler":Landroid/os/Handler;
    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 150
    .end local v1    # "refreshHandler":Landroid/os/Handler;
    :cond_2
    const-string v3, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v3, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 151
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-eqz v3, :cond_0

    .line 155
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_0

    .line 156
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    if-eqz v3, :cond_3

    .line 157
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;

    move-result-object v3

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    invoke-interface {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onGoToToday()V

    .line 155
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 121
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
