.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;
.source "TodayTheme.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;
    }
.end annotation


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field protected mCheckTimeZone:Ljava/lang/Runnable;

.field private mDateContainer:Landroid/widget/LinearLayout;

.field private mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

.field private mEventList:Landroid/widget/ListView;

.field protected mGoToToday:Ljava/lang/Runnable;

.field private mGoToday:Landroid/widget/Button;

.field private mNewEvent:Landroid/widget/ImageButton;

.field private mNextDay:Landroid/widget/ImageButton;

.field private mPrevDay:Landroid/widget/ImageButton;

.field protected mTodayUpdater:Ljava/lang/Runnable;

.field private mTodayView:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 77
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 1
    .param p1, "surfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 92
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 541
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$9;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$9;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayUpdater:Ljava/lang/Runnable;

    .line 562
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCheckTimeZone:Ljava/lang/Runnable;

    .line 573
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$11;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$11;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToToday:Ljava/lang/Runnable;

    .line 93
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Ljava/util/ArrayList;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
    .param p1, "x1"    # Ljava/util/ArrayList;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->setCurrentDayEvents(Ljava/util/ArrayList;)V

    return-void
.end method

.method static synthetic access$1300(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/graphics/Typeface;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2500(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2600(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2700(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$2900(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$3000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;I)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
    .param p1, "x1"    # I

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->setSelectionPostion(I)V

    return-void
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Landroid/text/format/Time;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
    .param p1, "x1"    # Landroid/text/format/Time;

    .prologue
    .line 74
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V

    return-void
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/widget/ListView;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    return-object v0
.end method

.method private hasAlldayEvent(Ljava/util/ArrayList;)Z
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;)Z"
        }
    .end annotation

    .prologue
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    const/4 v0, 0x0

    .line 485
    if-eqz p1, :cond_0

    invoke-virtual {p1}, Ljava/util/ArrayList;->size()I

    move-result v1

    const/4 v2, 0x1

    if-ge v1, v2, :cond_1

    .line 488
    :cond_0
    :goto_0
    return v0

    :cond_1
    invoke-virtual {p1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    iget-boolean v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    goto :goto_0
.end method

.method private setCurrentDayEvents(Ljava/util/ArrayList;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    const/16 v9, 0x8

    const/4 v10, 0x0

    .line 422
    sget-object v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v8, "[TodayTheme] setcurrentDayEvents"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 423
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    if-eqz v7, :cond_0

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    if-nez v7, :cond_1

    .line 479
    :cond_0
    :goto_0
    return-void

    .line 427
    :cond_1
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    .line 428
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->setTaskData()V

    .line 429
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    invoke-virtual {v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->notifyDataSetChanged()V

    .line 431
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_3

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    if-eqz v7, :cond_2

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-nez v7, :cond_3

    .line 433
    :cond_2
    sget-object v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v8, "[TodayTheme] Today has no event"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 435
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v10}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 436
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mListContainerView:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v9}, Landroid/widget/FrameLayout;->setVisibility(I)V

    goto :goto_0

    .line 439
    :cond_3
    sget-object v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v8, "[TodayTheme] There are events today"

    invoke-static {v7, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 441
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    invoke-virtual {v7, v9}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 442
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mListContainerView:Landroid/widget/FrameLayout;

    invoke-virtual {v7, v10}, Landroid/widget/FrameLayout;->setVisibility(I)V

    .line 445
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    const/4 v8, 0x1

    invoke-virtual {v7, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 447
    .local v6, "tempTime":Ljava/lang/Long;
    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    invoke-virtual {p0, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->isToday(J)Z

    move-result v7

    if-eqz v7, :cond_8

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-direct {p0, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->hasAlldayEvent(Ljava/util/ArrayList;)Z

    move-result v7

    if-nez v7, :cond_8

    .line 448
    const/4 v1, 0x0

    .line 449
    .local v1, "itemId":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v0, v7, :cond_4

    .line 450
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v7, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    iget-wide v4, v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    .line 451
    .local v4, "start":J
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    .line 453
    .local v2, "now":J
    cmp-long v7, v4, v2

    if-lez v7, :cond_5

    .line 454
    move v1, v0

    .line 459
    .end local v2    # "now":J
    .end local v4    # "start":J
    :cond_4
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsRefreshFromCheckBoxTouch:Z

    if-eqz v7, :cond_6

    .line 460
    iput-boolean v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsRefreshFromCheckBoxTouch:Z

    .line 461
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSelectedPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setSelection(I)V

    .line 462
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mInstance:I

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->relayout(I)V

    goto/16 :goto_0

    .line 449
    .restart local v2    # "now":J
    .restart local v4    # "start":J
    :cond_5
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 466
    .end local v2    # "now":J
    .end local v4    # "start":J
    :cond_6
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    invoke-virtual {v7}, Landroid/widget/ListView;->getCount()I

    move-result v7

    if-ge v1, v7, :cond_7

    .line 467
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    invoke-virtual {v7, v1, v10}, Landroid/widget/ListView;->setSelectionFromTop(II)V

    .line 478
    .end local v0    # "i":I
    .end local v1    # "itemId":I
    :cond_7
    :goto_2
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mInstance:I

    invoke-virtual {v7, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->relayout(I)V

    goto/16 :goto_0

    .line 470
    :cond_8
    iget-boolean v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsRefreshFromCheckBoxTouch:Z

    if-eqz v7, :cond_9

    .line 471
    iput-boolean v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsRefreshFromCheckBoxTouch:Z

    .line 472
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    iget v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSelectedPosition:I

    invoke-virtual {v7, v8}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_2

    .line 474
    :cond_9
    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    invoke-virtual {v7, v10}, Landroid/widget/ListView;->setSelection(I)V

    goto :goto_2
.end method

.method private setSelectionPostion(I)V
    .locals 0
    .param p1, "mPosition"    # I

    .prologue
    .line 894
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSelectedPosition:I

    .line 895
    return-void
.end method

.method private updateDayTitle(Landroid/text/format/Time;)V
    .locals 8
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 265
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateTodayButton(Landroid/text/format/Time;)V

    .line 266
    new-instance v2, Landroid/text/SpannableStringBuilder;

    invoke-direct {v2}, Landroid/text/SpannableStringBuilder;-><init>()V

    .line 267
    .local v2, "spb":Landroid/text/SpannableStringBuilder;
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    if-eqz v4, :cond_0

    .line 268
    const/4 v4, 0x1

    invoke-virtual {p1, v4}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    .line 269
    .local v3, "tTime":Ljava/lang/Long;
    invoke-virtual {v3}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    invoke-virtual {p0, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->isToday(J)Z

    move-result v4

    if-eqz v4, :cond_3

    .line 270
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0a0029

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    const-string v5, " "

    invoke-virtual {v4, v5}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 271
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070060

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    .line 279
    .end local v3    # "tTime":Ljava/lang/Long;
    :cond_0
    :goto_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->getDateFormatForTitle(Landroid/text/format/Time;)Landroid/text/SpannableStringBuilder;

    move-result-object v4

    invoke-virtual {v2, v4}, Landroid/text/SpannableStringBuilder;->append(Ljava/lang/CharSequence;)Landroid/text/SpannableStringBuilder;

    move-result-object v2

    .line 280
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f080089

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    .line 281
    .local v1, "fontSize":I
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v4

    if-eqz v4, :cond_1

    .line 282
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsTabletConfig:Z

    if-eqz v4, :cond_1

    .line 283
    int-to-double v4, v1

    const-wide v6, 0x3feccccccccccccdL    # 0.9

    mul-double/2addr v4, v6

    double-to-int v1, v4

    .line 285
    :cond_1
    const/16 v0, 0x21

    .line 286
    .local v0, "flags":I
    new-instance v4, Landroid/text/style/AbsoluteSizeSpan;

    invoke-direct {v4, v1}, Landroid/text/style/AbsoluteSizeSpan;-><init>(I)V

    const/4 v5, 0x0

    invoke-virtual {v2}, Landroid/text/SpannableStringBuilder;->length()I

    move-result v6

    invoke-virtual {v2, v4, v5, v6, v0}, Landroid/text/SpannableStringBuilder;->setSpan(Ljava/lang/Object;III)V

    .line 288
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    if-eqz v4, :cond_2

    .line 289
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :cond_2
    return-void

    .line 274
    .end local v0    # "flags":I
    .end local v1    # "fontSize":I
    .restart local v3    # "tTime":Ljava/lang/Long;
    :cond_3
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070013

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_0
.end method

.method private updateTodayButton(Landroid/text/format/Time;)V
    .locals 4
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v1, 0x1

    .line 258
    invoke-virtual {p1, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->isToday(J)Z

    move-result v0

    .line 259
    .local v0, "isToday":Z
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    if-eqz v2, :cond_0

    .line 260
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    if-nez v0, :cond_1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 262
    :cond_0
    return-void

    .line 260
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method


# virtual methods
.method public clearViews()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 530
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mMain:Landroid/widget/LinearLayout;

    .line 531
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    .line 532
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mDateContainer:Landroid/widget/LinearLayout;

    .line 533
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mListContainerView:Landroid/widget/FrameLayout;

    .line 534
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mPrevDay:Landroid/widget/ImageButton;

    .line 535
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNextDay:Landroid/widget/ImageButton;

    .line 536
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNewEvent:Landroid/widget/ImageButton;

    .line 537
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    .line 538
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    .line 539
    return-void
.end method

.method public initialize(Landroid/view/View;)V
    .locals 7
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    const/4 v4, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->initialize(Landroid/view/View;)V

    .line 112
    const v2, 0x7f0c0056

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mMain:Landroid/widget/LinearLayout;

    .line 113
    const v2, 0x7f0c004b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mPrevDay:Landroid/widget/ImageButton;

    .line 114
    const v2, 0x7f0c004d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNextDay:Landroid/widget/ImageButton;

    .line 115
    const v2, 0x7f0c0012

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ImageButton;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNewEvent:Landroid/widget/ImageButton;

    .line 116
    const v2, 0x7f0c0050

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/Button;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    .line 117
    const v2, 0x7f0c0059

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    .line 118
    const/high16 v2, 0x7f0c0000

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mDateContainer:Landroid/widget/LinearLayout;

    .line 119
    const v2, 0x7f0c005e

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ListView;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    .line 120
    const v2, 0x7f0c005b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/LinearLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    .line 121
    const v2, 0x7f0c005d

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/FrameLayout;

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mListContainerView:Landroid/widget/FrameLayout;

    .line 122
    const v2, 0x7f0c0058

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 124
    .local v1, "title":Landroid/widget/TextView;
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->isFontStyleDefault:Z

    if-eqz v2, :cond_0

    .line 125
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 126
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 127
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayView:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 130
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    invoke-virtual {v2, v4}, Landroid/widget/ListView;->setCacheColorHint(I)V

    .line 131
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    const/16 v3, 0x10

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setFadingEdgeLength(I)V

    .line 132
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    invoke-virtual {v2, v6}, Landroid/widget/ListView;->setFocusable(Z)V

    .line 133
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTempTime:Landroid/text/format/Time;

    .line 134
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$1;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 141
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, "timeToday":Landroid/text/format/Time;
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 144
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V

    .line 145
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mDateContainer:Landroid/widget/LinearLayout;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$2;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$2;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 163
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mPrevDay:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 183
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNextDay:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$4;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$4;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 203
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNewEvent:Landroid/widget/ImageButton;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$5;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$5;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 210
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    new-instance v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;

    invoke-direct {v3, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    invoke-virtual {v2, v3}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 243
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v3, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    .line 244
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 246
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iput v4, v2, Landroid/text/format/Time;->hour:I

    .line 247
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iput v4, v2, Landroid/text/format/Time;->minute:I

    .line 248
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iput v4, v2, Landroid/text/format/Time;->second:I

    .line 249
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadCurrentDayEvents()V

    .line 250
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadTasks()V

    .line 252
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {v2, p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Landroid/content/Context;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    .line 253
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventList:Landroid/widget/ListView;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    invoke-virtual {v2, v3}, Landroid/widget/ListView;->setAdapter(Landroid/widget/ListAdapter;)V

    .line 254
    iput-boolean v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mInitialized:Z

    .line 255
    return-void
.end method

.method public onContentRequest()I
    .locals 2

    .prologue
    .line 104
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v1, "[TodayTheme] onContentRequest"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 105
    const v0, 0x7f03000b

    return v0
.end method

.method public onFinishInflate(ILandroid/view/View;)V
    .locals 2
    .param p1, "instance"    # I
    .param p2, "topView"    # Landroid/view/View;

    .prologue
    .line 97
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v1, "[TodayTheme] onFinishInflate"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 98
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mInstance:I

    .line 99
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->initialize(Landroid/view/View;)V

    .line 100
    return-void
.end method

.method public onGoToToday()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 342
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v3, "[TodayTheme] onGoToToday!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 345
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v0

    .line 346
    .local v0, "isUsedSystemTimezone":Z
    if-eqz v0, :cond_1

    .line 347
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 351
    .local v1, "nextDay":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 353
    if-eqz v0, :cond_0

    .line 354
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 355
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 357
    :cond_0
    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    .line 358
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V

    .line 359
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadTasks()V

    .line 360
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadCurrentDayEvents()V

    .line 361
    return-void

    .line 349
    .end local v1    # "nextDay":Landroid/text/format/Time;
    :cond_1
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v1    # "nextDay":Landroid/text/format/Time;
    goto :goto_0
.end method

.method public onPause(I)V
    .locals 5
    .param p1, "instance"    # I

    .prologue
    const/4 v4, 0x0

    .line 296
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToToday:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 297
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayUpdater:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 298
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mContext:Landroid/content/Context;

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 299
    .local v0, "deviceTime":Landroid/text/format/Time;
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 300
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCurrentDateOnPause:Ljava/lang/String;

    .line 301
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v1

    if-eqz v1, :cond_0

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v1

    :goto_0
    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->sCurrentTzOnPause:Ljava/lang/String;

    .line 303
    return-void

    .line 301
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public onRefresh()V
    .locals 1

    .prologue
    .line 331
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mInitialized:Z

    if-nez v0, :cond_0

    .line 338
    :goto_0
    return-void

    .line 335
    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadTasks()V

    .line 336
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadCurrentDayEvents()V

    .line 337
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V

    goto :goto_0
.end method

.method public onResume(I)V
    .locals 6
    .param p1, "instance"    # I

    .prologue
    .line 307
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->TAG:Ljava/lang/String;

    const-string v3, "[TodayTheme] onResume!!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 308
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 309
    .local v1, "systemTime":Landroid/text/format/Time;
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 310
    invoke-virtual {v1}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    const/16 v4, 0x8

    invoke-virtual {v2, v3, v4}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCurrentDateOnResume:Ljava/lang/String;

    .line 311
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCurrentDateOnResume:Ljava/lang/String;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCurrentDateOnPause:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 312
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->onGoToToday()V

    .line 314
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTodayUpdater:Ljava/lang/Runnable;

    invoke-interface {v2}, Ljava/lang/Runnable;->run()V

    .line 315
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mHandler:Landroid/os/Handler;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mCheckTimeZone:Ljava/lang/Runnable;

    const-wide/16 v4, 0x190

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 318
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    .line 319
    .local v0, "mRefreshListHandler":Landroid/os/Handler;
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$7;

    invoke-direct {v2, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$7;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V

    const-wide/16 v4, 0xc8

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 327
    return-void
.end method

.method public onThemeDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 365
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->releaseOnClickListener()V

    .line 366
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->mFirstTZRequest:Z

    .line 367
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->stopBackgroundThread()V

    .line 369
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->clear()V

    .line 371
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;->cancelOperation(I)V

    .line 373
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_1

    .line 374
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v2}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 375
    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    .line 378
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->clearViews()V

    .line 379
    return-void
.end method

.method public onTouchEvent(ILandroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "instance"    # I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 396
    const/4 v0, 0x0

    return v0
.end method

.method public releaseOnClickListener()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 383
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    if-eqz v0, :cond_0

    .line 384
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNoEventView:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 387
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mDateContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 388
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mPrevDay:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 389
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNextDay:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 390
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mNewEvent:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 391
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mGoToday:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 392
    return-void
.end method

.method public reloadCurrentDayEvents()V
    .locals 8

    .prologue
    const/4 v1, 0x1

    .line 401
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    .line 402
    .local v6, "millis":J
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iget-wide v4, v0, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    .line 405
    .local v3, "mCurrentViewJulianDay":I
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 406
    .local v2, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->startBackgroundThread()V

    .line 407
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    new-instance v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$8;

    invoke-direct {v4, p0, v6, v7, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$8;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;JLjava/util/ArrayList;)V

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->loadEventsInBackground(ILjava/util/ArrayList;ILjava/lang/Runnable;Ljava/lang/Runnable;)V

    .line 419
    return-void
.end method

.method public reloadTasks()V
    .locals 13

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x0

    .line 492
    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    .line 493
    .local v3, "tasksUri":Landroid/net/Uri;
    new-instance v12, Landroid/text/format/Time;

    invoke-direct {v12}, Landroid/text/format/Time;-><init>()V

    .line 494
    .local v12, "time":Landroid/text/format/Time;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    if-eqz v0, :cond_1

    .line 495
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 499
    :goto_0
    iput v1, v12, Landroid/text/format/Time;->hour:I

    .line 500
    iput v1, v12, Landroid/text/format/Time;->minute:I

    .line 501
    iput v1, v12, Landroid/text/format/Time;->second:I

    .line 503
    const/4 v0, 0x1

    invoke-virtual {v12, v0}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    .line 504
    .local v10, "startMillis":J
    const-wide/32 v6, 0x5265c00

    add-long v8, v10, v6

    .line 505
    .local v8, "endMillis":J
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v0, v10, v11, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTaskSelection(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v5

    .line 506
    .local v5, "selection":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

    if-eqz v0, :cond_0

    .line 507
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->TASK_PROJECTION:[Ljava/lang/String;

    const-string v7, " utc_due_date ASC, importance DESC"

    move-object v6, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 510
    :cond_0
    return-void

    .line 497
    .end local v5    # "selection":Ljava/lang/String;
    .end local v8    # "endMillis":J
    .end local v10    # "startMillis":J
    :cond_1
    invoke-virtual {v12}, Landroid/text/format/Time;->setToNow()V

    goto :goto_0
.end method

.method public setTaskData()V
    .locals 12

    .prologue
    .line 513
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->getSelectedMillis()Ljava/lang/Long;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/Long;->longValue()J

    move-result-wide v8

    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iget-wide v10, v10, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v8, v9, v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v8

    int-to-long v4, v8

    .line 514
    .local v4, "milis":J
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iget-wide v0, v8, Landroid/text/format/Time;->gmtoff:J

    .line 515
    .local v0, "gmtoff":J
    const-wide/16 v6, 0x0

    .line 517
    .local v6, "taskDueDate":J
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 518
    .local v3, "taskArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v8

    if-ge v2, v8, :cond_1

    .line 519
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    iget-wide v8, v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    invoke-static {v8, v9, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v8

    int-to-long v6, v8

    .line 520
    cmp-long v8, v4, v6

    if-nez v8, :cond_0

    .line 521
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v8

    invoke-virtual {v3, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 518
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 525
    :cond_1
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    invoke-virtual {v8, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->setTasksArray(Ljava/util/ArrayList;)V

    .line 526
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventAdapter:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;

    invoke-virtual {v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->notifyDataSetChanged()V

    .line 527
    return-void
.end method
