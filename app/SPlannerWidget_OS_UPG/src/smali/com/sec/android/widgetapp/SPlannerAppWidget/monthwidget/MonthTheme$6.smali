.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;
.super Ljava/lang/Object;
.source "MonthTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V
    .locals 0

    .prologue
    .line 233
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 237
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v1

    .line 238
    .local v1, "isUsedSystemTimezone":Z
    if-eqz v1, :cond_1

    .line 239
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 243
    .local v0, "Today":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 244
    if-eqz v1, :cond_0

    .line 245
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 246
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 248
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-virtual {v2, v0, v3, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->goTo(Landroid/text/format/Time;ZZ)V

    .line 249
    return-void

    .line 241
    .end local v0    # "Today":Landroid/text/format/Time;
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v0    # "Today":Landroid/text/format/Time;
    goto :goto_0
.end method
