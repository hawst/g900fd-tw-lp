.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MemoQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 794
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 795
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 796
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "token"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 800
    if-eqz p3, :cond_0

    invoke-interface {p3}, Landroid/database/Cursor;->getCount()I

    move-result v0

    if-gtz v0, :cond_2

    .line 802
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mEventMemos:[Ljava/lang/Integer;

    const/4 v1, -0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Ljava/util/Arrays;->fill([Ljava/lang/Object;Ljava/lang/Object;)V

    .line 803
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mQSMemoEventArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->clear()V

    .line 805
    if-eqz p3, :cond_1

    .line 806
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 813
    :cond_1
    :goto_0
    return-void

    .line 810
    :cond_2
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$MemoQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setQSMemos(Landroid/database/Cursor;)V
    invoke-static {v0, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1500(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/database/Cursor;)V

    .line 811
    if-eqz p3, :cond_1

    .line 812
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    goto :goto_0
.end method
