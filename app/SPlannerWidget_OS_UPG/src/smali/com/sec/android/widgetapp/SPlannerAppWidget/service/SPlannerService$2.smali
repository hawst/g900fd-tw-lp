.class Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;
.source "SPlannerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V
    .locals 0

    .prologue
    .line 167
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;-><init>()V

    return-void
.end method


# virtual methods
.method public registerCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    .prologue
    .line 170
    if-eqz p1, :cond_0

    .line 171
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->register(Landroid/os/IInterface;)Z

    .line 172
    :cond_0
    return-void
.end method

.method public unregisterCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V
    .locals 1
    .param p1, "cb"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    .prologue
    .line 176
    if-eqz p1, :cond_0

    .line 177
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$2;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/os/RemoteCallbackList;->unregister(Landroid/os/IInterface;)Z

    .line 178
    :cond_0
    return-void
.end method
