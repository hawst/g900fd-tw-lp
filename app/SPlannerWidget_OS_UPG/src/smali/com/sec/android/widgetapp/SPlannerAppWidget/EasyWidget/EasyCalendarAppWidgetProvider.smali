.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "EasyCalendarAppWidgetProvider.java"


# static fields
.field static final EXTRA_EVENT_IDS:Ljava/lang/String; = "com.android.calendar.EXTRA_EVENT_IDS"

.field public static final FONT_SIZE:Ljava/lang/String; = "font_size"

.field static final LOGD:Z = false

.field static final TAG:Ljava/lang/String; = "CalendarAppWidgetProvider"

.field static final date_num_drawables:[I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 59
    const/16 v0, 0xa

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->date_num_drawables:[I

    return-void

    :array_0
    .array-data 4
        0x7f02000a
        0x7f02000b
        0x7f02000c
        0x7f02000d
        0x7f02000e
        0x7f02000f
        0x7f020010
        0x7f020011
        0x7f020012
        0x7f020013
    .end array-data
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 50
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    return-void
.end method

.method static getComponentName(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 138
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method static getLaunchPendingIntentTemplate(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 235
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 236
    .local v0, "launchIntent":Landroid/content/Intent;
    const-string v1, "android.intent.action.VIEW"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 237
    const/high16 v1, 0x14200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 240
    const-string v1, "com.android.calendar"

    const-string v2, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 241
    const/4 v1, 0x0

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private getTriggerTime(Landroid/content/Context;)J
    .locals 9
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 247
    const/4 v6, 0x0

    invoke-static {p1, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    .line 249
    .local v5, "tz":Ljava/lang/String;
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 250
    .local v4, "time":Landroid/text/format/Time;
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 251
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    .line 252
    iput v7, v4, Landroid/text/format/Time;->hour:I

    .line 253
    iput v7, v4, Landroid/text/format/Time;->minute:I

    .line 254
    iput v7, v4, Landroid/text/format/Time;->second:I

    .line 255
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 257
    .local v0, "midnightDeviceTz":J
    iput-object v5, v4, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 258
    invoke-virtual {v4}, Landroid/text/format/Time;->setToNow()V

    .line 259
    iget v6, v4, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v6, v6, 0x1

    iput v6, v4, Landroid/text/format/Time;->monthDay:I

    .line 260
    iput v7, v4, Landroid/text/format/Time;->hour:I

    .line 261
    iput v7, v4, Landroid/text/format/Time;->minute:I

    .line 262
    iput v7, v4, Landroid/text/format/Time;->second:I

    .line 263
    invoke-virtual {v4, v8}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 265
    .local v2, "midnightHomeTz":J
    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v6

    return-wide v6
.end method

.method static getUpdateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 225
    new-instance v0, Landroid/content/Intent;

    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0, v2, v0, v2}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private performUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I[J)V
    .locals 25
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I
    .param p4, "changedEventIds"    # [J

    .prologue
    .line 157
    move-object/from16 v9, p3

    .local v9, "arr$":[I
    array-length v0, v9

    move/from16 v17, v0

    .local v17, "len$":I
    const/4 v14, 0x0

    .local v14, "i$":I
    :goto_0
    move/from16 v0, v17

    if-ge v14, v0, :cond_6

    aget v2, v9, v14

    .line 158
    .local v2, "appWidgetId":I
    new-instance v21, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v3

    const/high16 v6, 0x7f030000

    move-object/from16 v0, v21

    invoke-direct {v0, v3, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 160
    .local v21, "views":Landroid/widget/RemoteViews;
    new-instance v20, Landroid/text/format/Time;

    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, v20

    invoke-direct {v0, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 161
    .local v20, "time":Landroid/text/format/Time;
    invoke-virtual/range {v20 .. v20}, Landroid/text/format/Time;->setToNow()V

    .line 162
    const/4 v3, 0x1

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    .line 164
    .local v4, "millis":J
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    sget-object v6, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 166
    :cond_0
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v3, v3, 0x1

    const/4 v6, 0x3

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v10

    .line 172
    .local v10, "dayOfWeek":Ljava/lang/String;
    :goto_1
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v3, v3, 0x1

    const/4 v6, 0x3

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v11

    .line 175
    .local v11, "dayOfWeekAccessibility":Ljava/lang/String;
    const v8, 0x10028

    .line 177
    .local v8, "flags":I
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 178
    const v3, -0x10001

    and-int/2addr v8, v3

    :cond_1
    move-object/from16 v3, p1

    move-wide v6, v4

    .line 180
    invoke-static/range {v3 .. v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v18

    .line 182
    .local v18, "month":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const v6, 0x7f0a0012

    invoke-virtual {v3, v6}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 183
    .local v12, "doubleTapAccessibility":Ljava/lang/String;
    const v3, 0x7f0c0009

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v6, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, ", "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 184
    const v3, 0x7f0c0009

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v10}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 185
    const v3, 0x7f0c0007

    move-object/from16 v0, v21

    move-object/from16 v1, v18

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 186
    const v3, 0x7f0c0008

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a005e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 188
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isUsaOrCanada()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 189
    const v3, 0x7f0c0002

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a001b

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 194
    :goto_2
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    div-int/lit8 v13, v3, 0xa

    .line 195
    .local v13, "firstDigitNumber":I
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    rem-int/lit8 v19, v3, 0xa

    .line 197
    .local v19, "secondDigitNumber":I
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    const/16 v6, 0xa

    if-ge v3, v6, :cond_4

    .line 198
    const v3, 0x7f0c0004

    const/16 v6, 0x8

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 202
    :goto_3
    const v3, 0x7f0c0004

    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->date_num_drawables:[I

    aget v6, v6, v13

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 203
    const v3, 0x7f0c0005

    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->date_num_drawables:[I

    aget v6, v6, v19

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setImageViewResource(II)V

    .line 204
    const v3, 0x7f0c0002

    const-string v6, ","

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 205
    sget-object v3, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v6

    invoke-virtual {v6}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_5

    .line 206
    const v3, 0x7f0c0003

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a004a

    const/16 v22, 0x1

    move/from16 v0, v22

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v22, v0

    const/16 v23, 0x0

    move-object/from16 v0, v20

    iget v0, v0, Landroid/text/format/Time;->monthDay:I

    move/from16 v24, v0

    invoke-static/range {v24 .. v24}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v24

    aput-object v24, v22, v23

    move-object/from16 v0, v22

    invoke-virtual {v6, v7, v0}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 212
    :goto_4
    new-instance v15, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    invoke-direct {v15, v3}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 213
    .local v15, "launchCalendarIntent":Landroid/content/Intent;
    const-string v3, "com.android.calendar"

    const-string v6, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v15, v3, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 214
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "content://com.android.calendar/time/"

    invoke-virtual {v3, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v4, v5}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v15, v3}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 216
    const/4 v3, 0x0

    const/4 v6, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v15, v6}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v16

    .line 218
    .local v16, "launchCalendarPendingIntent":Landroid/app/PendingIntent;
    const/high16 v3, 0x7f0c0000

    move-object/from16 v0, v21

    move-object/from16 v1, v16

    invoke-virtual {v0, v3, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 220
    move-object/from16 v0, p2

    move-object/from16 v1, v21

    invoke-virtual {v0, v2, v1}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 157
    add-int/lit8 v14, v14, 0x1

    goto/16 :goto_0

    .line 169
    .end local v8    # "flags":I
    .end local v10    # "dayOfWeek":Ljava/lang/String;
    .end local v11    # "dayOfWeekAccessibility":Ljava/lang/String;
    .end local v12    # "doubleTapAccessibility":Ljava/lang/String;
    .end local v13    # "firstDigitNumber":I
    .end local v15    # "launchCalendarIntent":Landroid/content/Intent;
    .end local v16    # "launchCalendarPendingIntent":Landroid/app/PendingIntent;
    .end local v18    # "month":Ljava/lang/String;
    .end local v19    # "secondDigitNumber":I
    :cond_2
    move-object/from16 v0, v20

    iget v3, v0, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v3, v3, 0x1

    const/4 v6, 0x2

    invoke-static {v3, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v10

    .restart local v10    # "dayOfWeek":Ljava/lang/String;
    goto/16 :goto_1

    .line 191
    .restart local v8    # "flags":I
    .restart local v11    # "dayOfWeekAccessibility":Ljava/lang/String;
    .restart local v12    # "doubleTapAccessibility":Ljava/lang/String;
    .restart local v18    # "month":Ljava/lang/String;
    :cond_3
    const v3, 0x7f0c0002

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a005f

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    goto/16 :goto_2

    .line 200
    .restart local v13    # "firstDigitNumber":I
    .restart local v19    # "secondDigitNumber":I
    :cond_4
    const v3, 0x7f0c0004

    const/4 v6, 0x0

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 208
    :cond_5
    const v3, 0x7f0c0003

    move-object/from16 v0, v20

    iget v6, v0, Landroid/text/format/Time;->monthDay:I

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, v21

    invoke-virtual {v0, v3, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 222
    .end local v2    # "appWidgetId":I
    .end local v4    # "millis":J
    .end local v8    # "flags":I
    .end local v10    # "dayOfWeek":Ljava/lang/String;
    .end local v11    # "dayOfWeekAccessibility":Ljava/lang/String;
    .end local v12    # "doubleTapAccessibility":Ljava/lang/String;
    .end local v13    # "firstDigitNumber":I
    .end local v18    # "month":Ljava/lang/String;
    .end local v19    # "secondDigitNumber":I
    .end local v20    # "time":Landroid/text/format/Time;
    .end local v21    # "views":Landroid/widget/RemoteViews;
    :cond_6
    return-void
.end method


# virtual methods
.method public onDisabled(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 120
    const-string v2, "alarm"

    invoke-virtual {p1, v2}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/AlarmManager;

    .line 121
    .local v0, "am":Landroid/app/AlarmManager;
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->getUpdateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v1

    .line 122
    .local v1, "pendingUpdate":Landroid/app/PendingIntent;
    invoke-virtual {v0, v1}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 123
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 74
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 75
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 84
    if-nez p2, :cond_0

    .line 112
    :goto_0
    return-void

    .line 87
    :cond_0
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "action":Ljava/lang/String;
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isEasyMode(Landroid/content/Context;)Z

    move-result v6

    if-eqz v6, :cond_1

    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.TIME_SET"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    const-string v6, "android.intent.action.PROVIDER_CHANGED"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    const-string v6, "com.android.launcher.action.EASY_MODE_CHANGE"

    invoke-virtual {v6, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_3

    .line 97
    :cond_2
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->getTriggerTime(Landroid/content/Context;)J

    move-result-wide v4

    .line 99
    .local v4, "triggerTime":J
    const-string v6, "alarm"

    invoke-virtual {p1, v6}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/app/AlarmManager;

    .line 101
    .local v1, "alertManager":Landroid/app/AlarmManager;
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->getUpdateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;

    move-result-object v3

    .line 102
    .local v3, "pendingUpdate":Landroid/app/PendingIntent;
    invoke-virtual {v1, v3}, Landroid/app/AlarmManager;->cancel(Landroid/app/PendingIntent;)V

    .line 103
    const/4 v6, 0x1

    invoke-virtual {v1, v6, v4, v5, v3}, Landroid/app/AlarmManager;->setExact(IJLandroid/app/PendingIntent;)V

    .line 105
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 106
    .local v2, "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->getComponentName(Landroid/content/Context;)Landroid/content/ComponentName;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v6

    const/4 v7, 0x0

    invoke-direct {p0, p1, v2, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I[J)V

    goto :goto_0

    .line 110
    .end local v1    # "alertManager":Landroid/app/AlarmManager;
    .end local v2    # "appWidgetManager":Landroid/appwidget/AppWidgetManager;
    .end local v3    # "pendingUpdate":Landroid/app/PendingIntent;
    .end local v4    # "triggerTime":J
    :cond_3
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto :goto_0
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 130
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, p3, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/EasyWidget/EasyCalendarAppWidgetProvider;->performUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I[J)V

    .line 131
    return-void
.end method
