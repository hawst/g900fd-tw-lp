.class Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;
.super Landroid/os/Handler;
.source "SPlannerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V
    .locals 0

    .prologue
    .line 181
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    return-void
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 184
    iget v3, p1, Landroid/os/Message;->what:I

    packed-switch v3, :pswitch_data_0

    .line 205
    invoke-super {p0, p1}, Landroid/os/Handler;->handleMessage(Landroid/os/Message;)V

    .line 207
    :goto_0
    return-void

    .line 188
    :pswitch_0
    iget-object v2, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 190
    .local v2, "value":Ljava/lang/String;
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->beginBroadcast()I

    move-result v0

    .line 191
    .local v0, "N":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_0

    .line 193
    :try_start_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3, v1}, Landroid/os/RemoteCallbackList;->getBroadcastItem(I)Landroid/os/IInterface;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    invoke-interface {v3, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;->valueChanged(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 191
    :goto_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 200
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mCallbacks:Landroid/os/RemoteCallbackList;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/RemoteCallbackList;

    move-result-object v3

    invoke-virtual {v3}, Landroid/os/RemoteCallbackList;->finishBroadcast()V

    goto :goto_0

    .line 195
    :catch_0
    move-exception v3

    goto :goto_2

    .line 184
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
    .end packed-switch
.end method
