.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
.super Ljava/lang/Object;
.source "Task.java"


# instance fields
.field public accountKey:I

.field public accountName:Ljava/lang/String;

.field public color:I

.field public complete:Z

.field public dueDate:J

.field public groupName:Ljava/lang/String;

.field public id:J

.field public importance:I

.field public reminderSet:Z

.field public subject:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 8
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    if-ne p0, p1, :cond_1

    .line 55
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    if-eqz p1, :cond_2

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    :cond_2
    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 40
    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    .line 42
    .local v0, "task":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountKey:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountKey:I

    if-eq v3, v4, :cond_4

    move v1, v2

    goto :goto_0

    .line 43
    :cond_4
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    if-eq v3, v4, :cond_5

    move v1, v2

    goto :goto_0

    .line 44
    :cond_5
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    if-eq v3, v4, :cond_6

    move v1, v2

    goto :goto_0

    .line 45
    :cond_6
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_7

    move v1, v2

    goto :goto_0

    .line 46
    :cond_7
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    cmp-long v3, v4, v6

    if-eqz v3, :cond_8

    move v1, v2

    goto :goto_0

    .line 47
    :cond_8
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    iget v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    if-eq v3, v4, :cond_9

    move v1, v2

    goto :goto_0

    .line 48
    :cond_9
    iget-boolean v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    iget-boolean v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    if-eq v3, v4, :cond_a

    move v1, v2

    goto :goto_0

    .line 49
    :cond_a
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    if-eqz v3, :cond_c

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_d

    :cond_b
    move v1, v2

    .line 50
    goto :goto_0

    .line 49
    :cond_c
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    if-nez v3, :cond_b

    .line 51
    :cond_d
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    if-eqz v3, :cond_f

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    :cond_e
    move v1, v2

    .line 52
    goto :goto_0

    .line 51
    :cond_f
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    if-nez v3, :cond_e

    .line 53
    :cond_10
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    if-eqz v3, :cond_11

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    iget-object v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :goto_1
    move v1, v2

    goto/16 :goto_0

    :cond_11
    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    if-eqz v3, :cond_0

    goto :goto_1
.end method

.method public hashCode()I
    .locals 9

    .prologue
    const/16 v8, 0x20

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 60
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v0, v4

    .line 61
    .local v0, "result":I
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    iget-wide v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    ushr-long/2addr v6, v8

    xor-long/2addr v4, v6

    long-to-int v4, v4

    add-int v0, v1, v4

    .line 62
    mul-int/lit8 v4, v0, 0x1f

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    if-eqz v1, :cond_1

    move v1, v2

    :goto_0
    add-int v0, v4, v1

    .line 63
    mul-int/lit8 v1, v0, 0x1f

    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    if-eqz v4, :cond_2

    :goto_1
    add-int v0, v1, v2

    .line 64
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    add-int v0, v1, v2

    .line 65
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    if-eqz v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_2
    add-int v0, v2, v1

    .line 66
    mul-int/lit8 v2, v0, 0x1f

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    if-eqz v1, :cond_4

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    :goto_3
    add-int v0, v2, v1

    .line 67
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountKey:I

    add-int v0, v1, v2

    .line 68
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v3

    :cond_0
    add-int v0, v1, v3

    .line 69
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    add-int v0, v1, v2

    .line 70
    return v0

    :cond_1
    move v1, v3

    .line 62
    goto :goto_0

    :cond_2
    move v2, v3

    .line 63
    goto :goto_1

    :cond_3
    move v1, v3

    .line 65
    goto :goto_2

    :cond_4
    move v1, v3

    .line 66
    goto :goto_3
.end method

.method public toString()Ljava/lang/String;
    .locals 5

    .prologue
    const/16 v4, 0x27

    .line 75
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Task{id="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", dueDate="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    invoke-virtual {v0, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", reminderSet="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", complete="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", importance="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", subject=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", accountKey="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountKey:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", groupName=\'"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->groupName:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", color="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x7d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
