.class Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;
.super Ljava/lang/Object;
.source "TodayTheme.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V
    .locals 0

    .prologue
    .line 562
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 565
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v1

    if-eqz v1, :cond_1

    invoke-static {}, Landroid/text/format/Time;->getCurrentTimezone()Ljava/lang/String;

    move-result-object v0

    .line 567
    .local v0, "tz":Ljava/lang/String;
    :goto_0
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->sCurrentTzOnPause:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 568
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->onGoToToday()V

    .line 570
    :cond_0
    return-void

    .line 565
    .end local v0    # "tz":Ljava/lang/String;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$10;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$1400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
