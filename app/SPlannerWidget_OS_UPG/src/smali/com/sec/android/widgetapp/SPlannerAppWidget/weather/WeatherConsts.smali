.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherConsts;
.super Ljava/lang/Object;
.source "WeatherConsts.java"


# static fields
.field public static final CITYID_CURRENT_LOCATION:Ljava/lang/String; = "cityId:current"

.field public static final DAEMON_ACCUWEATHER:Ljava/lang/String; = "accuweather"

.field public static final DAEMON_CMAWEATHER:Ljava/lang/String; = "Cmaweather"

.field public static final DAEMON_JPWEATHER:Ljava/lang/String; = "weathernewsjp"

.field public static final DAEMON_KWEATHER:Ljava/lang/String; = "kweather"

.field public static final KEY_APP_SERVICE_STATUS:Ljava/lang/String; = "aw_daemon_service_key_app_service_status"

.field public static final KEY_CITY_NAME:Ljava/lang/String; = "aw_daemon_service_key_city_name"

.field public static final KEY_CURRENT_TEMP:Ljava/lang/String; = "aw_daemon_service_key_current_temp"

.field public static final KEY_DETAIL_INFO:Ljava/lang/String; = "aw_daemon_service_key_detail_info"

.field public static final KEY_ICON_NUM:Ljava/lang/String; = "aw_daemon_service_key_icon_num"

.field public static final KEY_TEMP_SCALE:Ljava/lang/String; = "aw_daemon_service_key_temp_scale"

.field public static final KEY_WEEKLY_INFO:Ljava/lang/String; = "aw_daemon_service_key_weekly_info"

.field public static final SERVICE_OFF:I = 0x0

.field public static final SERVICE_ON:I = 0x1


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 3
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
