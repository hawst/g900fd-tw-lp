.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;
.super Landroid/view/View;
.source "BaseMonthView.java"


# static fields
.field protected static final MEMO_SEARCH_QUERY_TOKEN:I = 0x0

.field protected static final SELECTION_HIDDEN:I = 0x0

.field protected static final SELECTION_PRESSED:I = 0x1

.field protected static final TASK_SEARCH_QUERY_TOKEN:I


# instance fields
.field protected final mContext:Landroid/content/Context;

.field protected mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

.field protected mFirstJulianDay:I

.field protected mFocusedBackground:Landroid/graphics/drawable/Drawable;

.field protected mIsTabletConfig:Z

.field protected mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

.field protected mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

.field protected mSelectionMode:I

.field protected mTodaySelectedBackground:Landroid/graphics/drawable/Drawable;

.field protected mViewCalendar:Landroid/text/format/Time;

.field protected tfLight:Landroid/graphics/Typeface;

.field protected tfRegular:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 1
    .param p1, "widget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    .line 47
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getLightTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->tfLight:Landroid/graphics/Typeface;

    .line 48
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getRegularTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->tfRegular:Landroid/graphics/Typeface;

    .line 49
    const/4 v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mSelectionMode:I

    .line 57
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mParentSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .line 58
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mContext:Landroid/content/Context;

    .line 59
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->init()V

    .line 60
    return-void
.end method

.method private init()V
    .locals 2

    .prologue
    .line 63
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mContext:Landroid/content/Context;

    const v1, 0x7f060002

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v0

    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mIsTabletConfig:Z

    .line 64
    invoke-static {}, Lcom/android/calendar/secfeature/SECCalendarFeatures;->getInstance()Lcom/android/calendar/secfeature/SECCalendarFeatures;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthView;->mSECFeatures:Lcom/android/calendar/secfeature/SECCalendarFeatures;

    .line 65
    return-void
.end method
