.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;
.super Ljava/lang/Object;
.source "SecDateUtils.java"


# static fields
.field public static final LENGTH_LONG:I = 0x3

.field public static final LENGTH_MEDIUM:I = 0x2

.field public static final LENGTH_SHORT:I = 0x1

.field public static final LENGTH_SHORTEST:I

.field private static sDateFormat:Ljava/text/SimpleDateFormat;

.field private static sLocale:Ljava/util/Locale;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    new-instance v0, Ljava/text/SimpleDateFormat;

    invoke-direct {v0}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    .line 23
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sLocale:Ljava/util/Locale;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getDayOfWeekString(II)Ljava/lang/String;
    .locals 4
    .param p0, "day"    # I
    .param p1, "length"    # I

    .prologue
    .line 83
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 84
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v2, 0x7

    invoke-virtual {v0, v2, p0}, Ljava/util/Calendar;->set(II)V

    .line 86
    if-nez p1, :cond_0

    .line 87
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEEEE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 95
    .local v1, "dayOfWeek":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 88
    .end local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    :cond_0
    const/4 v2, 0x1

    if-ne p1, v2, :cond_1

    .line 89
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 90
    .end local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    :cond_1
    const/4 v2, 0x2

    if-ne p1, v2, :cond_2

    .line 91
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 93
    .end local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    :cond_2
    new-instance v1, Ljava/text/SimpleDateFormat;

    const-string v2, "EEEE"

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v3

    invoke-direct {v1, v2, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v1    # "dayOfWeek":Ljava/text/SimpleDateFormat;
    goto :goto_0
.end method

.method public static getDayString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "day"    # I

    .prologue
    .line 98
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 99
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0}, Ljava/util/Calendar;->clear()V

    .line 100
    const/4 v1, 0x5

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 102
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sLocale:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 103
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sLocale:Ljava/util/Locale;

    .line 104
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    .line 106
    :cond_0
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a0021

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 107
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method public static getMonthString(IIZ)Ljava/lang/String;
    .locals 10
    .param p0, "month"    # I
    .param p1, "length"    # I
    .param p2, "standalone"    # Z

    .prologue
    const/4 v9, 0x2

    .line 36
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 37
    .local v0, "cal":Ljava/util/Calendar;
    invoke-virtual {v0, v9, p0}, Ljava/util/Calendar;->set(II)V

    .line 38
    const/4 v7, 0x5

    const/16 v8, 0xf

    invoke-virtual {v0, v7, v8}, Ljava/util/Calendar;->set(II)V

    .line 40
    const-string v6, "MMM"

    .line 41
    .local v6, "shortFormat":Ljava/lang/String;
    const-string v3, "MMM"

    .line 42
    .local v3, "mediumFormat":Ljava/lang/String;
    const-string v2, "MMMM"

    .line 44
    .local v2, "longFormat":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v7

    if-eqz v7, :cond_0

    .line 45
    const/4 p2, 0x0

    .line 48
    :cond_0
    if-eqz p2, :cond_1

    .line 49
    const-string v6, "LLL"

    .line 50
    const-string v3, "LLL"

    .line 51
    const-string v2, "LLLL"

    .line 54
    :cond_1
    const/4 v7, 0x1

    if-ne p1, v7, :cond_3

    .line 55
    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v4, v6, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 62
    .local v4, "monthDate":Ljava/text/SimpleDateFormat;
    :goto_0
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v4, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v5

    .line 63
    .local v5, "monthStr":Ljava/lang/String;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-virtual {v7}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v7

    sget-object v8, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v8}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 64
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v1, v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 65
    .local v1, "longDate":Ljava/text/SimpleDateFormat;
    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v7

    invoke-virtual {v1, v7}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v5, v7}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "."

    invoke-virtual {v5, v7}, Ljava/lang/String;->endsWith(Ljava/lang/String;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 66
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "."

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 70
    .end local v1    # "longDate":Ljava/text/SimpleDateFormat;
    :cond_2
    return-object v5

    .line 56
    .end local v4    # "monthDate":Ljava/text/SimpleDateFormat;
    .end local v5    # "monthStr":Ljava/lang/String;
    :cond_3
    if-ne p1, v9, :cond_4

    .line 57
    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v4, v3, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v4    # "monthDate":Ljava/text/SimpleDateFormat;
    goto :goto_0

    .line 59
    .end local v4    # "monthDate":Ljava/text/SimpleDateFormat;
    :cond_4
    new-instance v4, Ljava/text/SimpleDateFormat;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v7

    invoke-direct {v4, v2, v7}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .restart local v4    # "monthDate":Ljava/text/SimpleDateFormat;
    goto :goto_0
.end method

.method public static getYearString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "year"    # I

    .prologue
    .line 110
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    .line 111
    .local v0, "cal":Ljava/util/Calendar;
    const/4 v1, 0x1

    invoke-virtual {v0, v1, p1}, Ljava/util/Calendar;->set(II)V

    .line 112
    const/4 v1, 0x6

    const/16 v2, 0x96

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 114
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sLocale:Ljava/util/Locale;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/Locale;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 115
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sLocale:Ljava/util/Locale;

    .line 116
    new-instance v1, Ljava/text/SimpleDateFormat;

    invoke-direct {v1}, Ljava/text/SimpleDateFormat;-><init>()V

    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    .line 118
    :cond_0
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0a006d

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->applyPattern(Ljava/lang/String;)V

    .line 119
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->sDateFormat:Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
