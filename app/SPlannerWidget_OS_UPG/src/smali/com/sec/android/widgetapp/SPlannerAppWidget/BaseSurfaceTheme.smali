.class public abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;
.super Ljava/lang/Object;
.source "BaseSurfaceTheme.java"

# interfaces
.implements Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;


# instance fields
.field protected mContext:Landroid/content/Context;

.field protected mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

.field protected final mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

.field protected tfLight:Landroid/graphics/Typeface;

.field protected tfMedium:Landroid/graphics/Typeface;

.field protected tfRegular:Landroid/graphics/Typeface;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 1
    .param p1, "sPlannerSurfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 16
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getLightTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->tfLight:Landroid/graphics/Typeface;

    .line 17
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getRegularTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->tfRegular:Landroid/graphics/Typeface;

    .line 18
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getLightBoldTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->tfMedium:Landroid/graphics/Typeface;

    .line 21
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .line 22
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mContext:Landroid/content/Context;

    .line 23
    return-void
.end method


# virtual methods
.method public initialize(Landroid/view/View;)V
    .locals 2
    .param p1, "root"    # Landroid/view/View;

    .prologue
    .line 27
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 28
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->startBackgroundThread()V

    .line 29
    return-void
.end method
