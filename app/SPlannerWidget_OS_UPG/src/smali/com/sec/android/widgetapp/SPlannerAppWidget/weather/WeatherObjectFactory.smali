.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherObjectFactory;
.super Ljava/lang/Object;
.source "WeatherObjectFactory.java"


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 7
    return-void
.end method

.method public static createWeatherImageArchive(Ljava/lang/String;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;
    .locals 1
    .param p0, "CPName"    # Ljava/lang/String;

    .prologue
    .line 22
    const-string v0, "kweather"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 23
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherImageArchive;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherImageArchive;-><init>()V

    .line 29
    :goto_0
    return-object v0

    .line 24
    :cond_0
    const-string v0, "weathernewsjp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 25
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/JPWeatherImageArchive;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/JPWeatherImageArchive;-><init>()V

    goto :goto_0

    .line 26
    :cond_1
    const-string v0, "Cmaweather"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 27
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherImageArchive;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherImageArchive;-><init>()V

    goto :goto_0

    .line 29
    :cond_2
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AccuWeatherImageArchive;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AccuWeatherImageArchive;-><init>()V

    goto :goto_0
.end method

.method public static createWeatherKeyStrings(Ljava/lang/String;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;
    .locals 1
    .param p0, "CPName"    # Ljava/lang/String;

    .prologue
    .line 10
    const-string v0, "kweather"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 11
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;-><init>()V

    .line 17
    :goto_0
    return-object v0

    .line 12
    :cond_0
    const-string v0, "weathernewsjp"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 13
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/JPWeatherKeyStrings;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/JPWeatherKeyStrings;-><init>()V

    goto :goto_0

    .line 14
    :cond_1
    const-string v0, "Cmaweather"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 15
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherKeyStrings;-><init>()V

    goto :goto_0

    .line 17
    :cond_2
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AccuWeatherKeyStrings;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AccuWeatherKeyStrings;-><init>()V

    goto :goto_0
.end method
