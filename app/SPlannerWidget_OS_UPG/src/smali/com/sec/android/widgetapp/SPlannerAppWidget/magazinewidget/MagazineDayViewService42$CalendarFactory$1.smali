.class Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;
.super Ljava/lang/Object;
.source "MagazineDayViewService42.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->reloadCurrentDayEvents()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;

.field final synthetic val$events:Ljava/util/ArrayList;

.field final synthetic val$millis:J


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;JLjava/util/ArrayList;)V
    .locals 0

    .prologue
    .line 588
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;

    iput-wide p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->val$millis:J

    iput-object p4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->val$events:Ljava/util/ArrayList;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 591
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->access$100()Landroid/content/Context;

    move-result-object v1

    if-nez v1, :cond_0

    .line 599
    :goto_0
    return-void

    .line 595
    :cond_0
    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->val$millis:J

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->mTime:Landroid/text/format/Time;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;)Landroid/text/format/Time;

    move-result-object v1

    iget-wide v4, v1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v2, v3, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v0

    .line 596
    .local v0, "julianDay":I
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->val$events:Ljava/util/ArrayList;

    invoke-static {v1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->arrangeEventList(Ljava/util/ArrayList;I)V

    .line 597
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory$1;->val$events:Ljava/util/ArrayList;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->setCurrentDayEvents(Ljava/util/ArrayList;)V
    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42$CalendarFactory;Ljava/util/ArrayList;)V

    goto :goto_0
.end method
