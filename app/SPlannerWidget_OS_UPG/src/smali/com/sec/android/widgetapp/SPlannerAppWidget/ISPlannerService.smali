.class public interface abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
.super Ljava/lang/Object;
.source "ISPlannerService.java"

# interfaces
.implements Landroid/os/IInterface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService$Stub;
    }
.end annotation


# virtual methods
.method public abstract registerCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method

.method public abstract unregisterCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation
.end method
