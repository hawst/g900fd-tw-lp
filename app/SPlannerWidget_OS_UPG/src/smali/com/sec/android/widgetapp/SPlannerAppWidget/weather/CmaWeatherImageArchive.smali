.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/CmaWeatherImageArchive;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;
.source "CmaWeatherImageArchive.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;-><init>()V

    return-void
.end method


# virtual methods
.method public getImage(I)I
    .locals 4
    .param p1, "index"    # I

    .prologue
    const v1, 0x7f020039

    const v0, 0x7f020038

    const/4 v2, 0x0

    .line 8
    const/16 v3, 0x64

    if-eq p1, v3, :cond_0

    const/16 v3, 0x66

    if-ne p1, v3, :cond_2

    .line 13
    :cond_0
    :goto_0
    sparse-switch p1, :sswitch_data_0

    move v0, v2

    .line 78
    :cond_1
    :goto_1
    :sswitch_0
    return v0

    .line 11
    :cond_2
    rem-int/lit8 p1, p1, 0x64

    goto :goto_0

    .line 15
    :sswitch_1
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->isNight()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 16
    const v0, 0x7f020049

    goto :goto_1

    .line 23
    :sswitch_2
    const v0, 0x7f02003a

    goto :goto_1

    .line 25
    :sswitch_3
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->isNight()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 26
    const v0, 0x7f02004a

    goto :goto_1

    :cond_3
    move v0, v1

    .line 28
    goto :goto_1

    :sswitch_4
    move v0, v1

    .line 31
    goto :goto_1

    .line 35
    :sswitch_5
    const v0, 0x7f02003b

    goto :goto_1

    .line 38
    :sswitch_6
    const v0, 0x7f02003c

    goto :goto_1

    .line 44
    :sswitch_7
    const v0, 0x7f02003d

    goto :goto_1

    .line 46
    :sswitch_8
    const v0, 0x7f02003e

    goto :goto_1

    .line 48
    :sswitch_9
    const v0, 0x7f02003f

    goto :goto_1

    .line 52
    :sswitch_a
    const v0, 0x7f020041

    goto :goto_1

    .line 54
    :sswitch_b
    const v0, 0x7f020042

    goto :goto_1

    .line 58
    :sswitch_c
    const v0, 0x7f020043

    goto :goto_1

    .line 61
    :sswitch_d
    const v0, 0x7f020044

    goto :goto_1

    .line 63
    :sswitch_e
    const v0, 0x7f020045

    goto :goto_1

    .line 65
    :sswitch_f
    const v0, 0x7f02004d

    goto :goto_1

    .line 69
    :sswitch_10
    const v0, 0x7f02004c

    goto :goto_1

    .line 74
    :sswitch_11
    const v0, 0x7f02004e

    goto :goto_1

    :sswitch_12
    move v0, v2

    .line 76
    goto :goto_1

    .line 13
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0x1 -> :sswitch_2
        0x2 -> :sswitch_4
        0x3 -> :sswitch_8
        0x4 -> :sswitch_9
        0x5 -> :sswitch_f
        0x6 -> :sswitch_d
        0x7 -> :sswitch_5
        0x8 -> :sswitch_5
        0x9 -> :sswitch_10
        0xa -> :sswitch_7
        0xb -> :sswitch_7
        0xc -> :sswitch_7
        0xd -> :sswitch_b
        0xe -> :sswitch_a
        0xf -> :sswitch_a
        0x10 -> :sswitch_c
        0x11 -> :sswitch_e
        0x12 -> :sswitch_6
        0x13 -> :sswitch_d
        0x14 -> :sswitch_11
        0x15 -> :sswitch_5
        0x16 -> :sswitch_10
        0x17 -> :sswitch_10
        0x18 -> :sswitch_7
        0x19 -> :sswitch_7
        0x1a -> :sswitch_a
        0x1b -> :sswitch_c
        0x1c -> :sswitch_c
        0x1d -> :sswitch_11
        0x1e -> :sswitch_11
        0x1f -> :sswitch_11
        0x20 -> :sswitch_12
        0x35 -> :sswitch_6
        0x64 -> :sswitch_1
        0x66 -> :sswitch_3
    .end sparse-switch
.end method
