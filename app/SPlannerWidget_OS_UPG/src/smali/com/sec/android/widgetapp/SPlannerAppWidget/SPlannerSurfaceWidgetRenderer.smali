.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidgetRenderer;
.super Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
.source "SPlannerSurfaceWidgetRenderer.java"


# direct methods
.method protected constructor <init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "i"    # I
    .param p4, "handler"    # Landroid/os/Handler;

    .prologue
    .line 32
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V

    .line 33
    return-void
.end method

.method private getTodayDateString()Ljava/lang/String;
    .locals 8

    .prologue
    const/4 v6, 0x1

    const/4 v4, 0x0

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidgetRenderer;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 42
    .local v1, "context":Landroid/content/Context;
    if-nez v1, :cond_0

    .line 60
    :goto_0
    return-object v4

    .line 46
    :cond_0
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchTodayTimeZone(Landroid/content/Context;)I

    .line 47
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v7

    .line 49
    .local v7, "isUsedSystemTimezone":Z
    if-eqz v7, :cond_2

    .line 50
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 54
    .local v0, "Today":Landroid/text/format/Time;
    :goto_1
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 55
    if-eqz v7, :cond_1

    .line 56
    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 57
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    .line 59
    :cond_1
    invoke-virtual {v0, v6}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 60
    .local v2, "timeMillis":J
    const/16 v6, 0x10

    move-wide v4, v2

    invoke-static/range {v1 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateRange(Landroid/content/Context;JJI)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 52
    .end local v0    # "Today":Landroid/text/format/Time;
    .end local v2    # "timeMillis":J
    :cond_2
    new-instance v0, Landroid/text/format/Time;

    invoke-static {v1, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v0, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v0    # "Today":Landroid/text/format/Time;
    goto :goto_1
.end method


# virtual methods
.method protected onContentDescriptionUpdate()Ljava/lang/String;
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidgetRenderer;->getTodayDateString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
