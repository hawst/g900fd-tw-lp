.class Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;
.super Landroid/database/ContentObserver;
.source "MagazineAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DateFormatObserver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;)V
    .locals 1

    .prologue
    .line 1427
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    .line 1428
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    invoke-direct {p0, v0}, Landroid/database/ContentObserver;-><init>(Landroid/os/Handler;)V

    .line 1429
    return-void
.end method


# virtual methods
.method public onChange(Z)V
    .locals 3
    .param p1, "selfChange"    # Z

    .prologue
    .line 1433
    invoke-super {p0, p1}, Landroid/database/ContentObserver;->onChange(Z)V

    .line 1434
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->access$000()Landroid/content/Context;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1435
    new-instance v0, Landroid/content/Intent;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->access$000()Landroid/content/Context;

    move-result-object v1

    const-class v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1436
    .local v0, "serviceIntent":Landroid/content/Intent;
    const-string v1, "clock.date_format_changed"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1437
    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->access$000()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 1439
    .end local v0    # "serviceIntent":Landroid/content/Intent;
    :cond_0
    return-void
.end method
