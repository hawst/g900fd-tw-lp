.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;
.source "MonthTheme.java"


# static fields
.field private static final TAG:Ljava/lang/String;


# instance fields
.field private currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

.field protected mCheckTimeZone:Ljava/lang/Runnable;

.field protected mGoToToday:Ljava/lang/Runnable;

.field private mHandWriting:Landroid/widget/ImageButton;

.field protected mIsWeatherEnabled:Z

.field private mMonthFridayColor:I

.field private mMonthSaturdayColor:I

.field private mMonthSundayColor:I

.field private mNewEvent:Landroid/widget/ImageButton;

.field protected mTodayUpdater:Ljava/lang/Runnable;

.field public mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 57
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 1
    .param p1, "surfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 76
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 750
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$12;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTodayUpdater:Ljava/lang/Runnable;

    .line 774
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$13;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$13;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToToday:Ljava/lang/Runnable;

    .line 792
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$14;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$14;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCheckTimeZone:Ljava/lang/Runnable;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->launchOutOfBoundToast()V

    return-void
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    return-object v0
.end method

.method private launchOutOfBoundToast()V
    .locals 1

    .prologue
    .line 801
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->makeOutOfBoundToast(Landroid/content/Context;)V

    .line 802
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->sOutOfBoundToast:Landroid/widget/Toast;

    if-eqz v0, :cond_0

    .line 803
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->sOutOfBoundToast:Landroid/widget/Toast;

    invoke-virtual {v0}, Landroid/widget/Toast;->show()V

    .line 805
    :cond_0
    return-void
.end method

.method private makeOutOfBoundToast(Landroid/content/Context;)V
    .locals 14
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 808
    instance-of v11, p1, Landroid/app/Activity;

    if-eqz v11, :cond_0

    .line 809
    new-instance v7, Landroid/text/format/Time;

    const-string v11, "UTC"

    invoke-direct {v7, v11}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 810
    .local v7, "time":Landroid/text/format/Time;
    const v11, 0x24dc87

    invoke-static {v7, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->setJulianDay(Landroid/text/format/Time;I)J

    .line 811
    const/4 v11, 0x1

    invoke-virtual {v7, v11}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v8

    .line 812
    .local v8, "startMillis":J
    const v11, 0x259d23

    invoke-static {v7, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->setJulianDay(Landroid/text/format/Time;I)J

    .line 813
    const/4 v11, 0x1

    invoke-virtual {v7, v11}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v2

    .line 814
    .local v2, "endMillis":J
    invoke-static {p1}, Landroid/text/format/DateFormat;->getDateFormat(Landroid/content/Context;)Ljava/text/DateFormat;

    move-result-object v4

    .line 815
    .local v4, "format":Ljava/text/DateFormat;
    new-instance v5, Ljava/util/Date;

    invoke-direct {v5, v8, v9}, Ljava/util/Date;-><init>(J)V

    .line 816
    .local v5, "startDate":Ljava/util/Date;
    new-instance v0, Ljava/util/Date;

    invoke-direct {v0, v2, v3}, Ljava/util/Date;-><init>(J)V

    .line 817
    .local v0, "endDate":Ljava/util/Date;
    invoke-virtual {v4, v5}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    .line 818
    .local v6, "startDateStr":Ljava/lang/String;
    invoke-virtual {v4, v0}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    .line 819
    .local v1, "endDateStr":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a006c

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    const/4 v12, 0x2

    new-array v12, v12, [Ljava/lang/Object;

    const/4 v13, 0x0

    aput-object v6, v12, v13

    const/4 v13, 0x1

    aput-object v1, v12, v13

    invoke-static {v11, v12}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 822
    .local v10, "wrongRangeError":Ljava/lang/String;
    const/4 v11, 0x0

    invoke-static {p1, v10, v11}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v11

    sput-object v11, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->sOutOfBoundToast:Landroid/widget/Toast;

    .line 824
    .end local v0    # "endDate":Ljava/util/Date;
    .end local v1    # "endDateStr":Ljava/lang/String;
    .end local v2    # "endMillis":J
    .end local v4    # "format":Ljava/text/DateFormat;
    .end local v5    # "startDate":Ljava/util/Date;
    .end local v6    # "startDateStr":Ljava/lang/String;
    .end local v7    # "time":Landroid/text/format/Time;
    .end local v8    # "startMillis":J
    .end local v10    # "wrongRangeError":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method private setTitle(Landroid/text/format/Time;)V
    .locals 13
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v12, 0x0

    const/4 v10, 0x1

    .line 304
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 309
    .local v2, "tempTime":Landroid/text/format/Time;
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    iget v9, p1, Landroid/text/format/Time;->year:I

    invoke-static {v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getYearString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v7

    .line 310
    .local v7, "yearFormat":Ljava/lang/String;
    const/4 v1, 0x0

    .line 312
    .local v1, "systemDateFormat":Ljava/lang/String;
    new-instance v1, Ljava/lang/String;

    .end local v1    # "systemDateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDateFormatOrder()[C

    move-result-object v8

    invoke-direct {v1, v8}, Ljava/lang/String;-><init>([C)V

    .line 314
    .restart local v1    # "systemDateFormat":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 315
    const-string v1, "MDY"

    .line 318
    :cond_0
    invoke-virtual {v2, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 319
    const-string v8, "YMD"

    invoke-virtual {v1, v8}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v8

    if-eqz v8, :cond_6

    .line 320
    iget v8, p1, Landroid/text/format/Time;->month:I

    invoke-static {v8, v10, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getMonthString(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 321
    .local v0, "monthString":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_1

    .line 322
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->toCapitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 323
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    const-string v9, "MMMM"

    invoke-virtual {p1, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 325
    :cond_1
    if-eqz v7, :cond_2

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_2

    .line 326
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 328
    :cond_2
    const/4 v5, 0x0

    .line 329
    .local v5, "typefase_one":I
    const/4 v6, 0x1

    .line 330
    .local v6, "typefase_two":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    .line 331
    .local v3, "typefaceLeft":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    .line 347
    .end local v0    # "monthString":Ljava/lang/String;
    .local v4, "typefaceRight":Landroid/graphics/Typeface;
    :goto_0
    iget-boolean v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->isFontStyleDefault:Z

    if-eqz v8, :cond_9

    .line 348
    if-eqz v3, :cond_4

    if-eqz v4, :cond_4

    .line 349
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_3

    .line 350
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 351
    :cond_3
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_4

    .line 352
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v4}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 361
    :cond_4
    :goto_1
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDateContainer:Landroid/widget/LinearLayout;

    if-eqz v8, :cond_5

    .line 362
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDateContainer:Landroid/widget/LinearLayout;

    new-instance v9, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;

    invoke-direct {v9, p0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;Landroid/text/format/Time;)V

    invoke-virtual {v8, v9}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 389
    :cond_5
    return-void

    .line 333
    .end local v3    # "typefaceLeft":Landroid/graphics/Typeface;
    .end local v4    # "typefaceRight":Landroid/graphics/Typeface;
    .end local v5    # "typefase_one":I
    .end local v6    # "typefase_two":I
    :cond_6
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_7

    .line 334
    iget v8, p1, Landroid/text/format/Time;->month:I

    invoke-static {v8, v10, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getMonthString(IIZ)Ljava/lang/String;

    move-result-object v0

    .line 335
    .restart local v0    # "monthString":Ljava/lang/String;
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->toCapitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 336
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    const-string v9, "MMMM"

    invoke-virtual {p1, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v10

    invoke-static {v9, v10, v11}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;J)Ljava/lang/CharSequence;

    move-result-object v9

    invoke-virtual {v8, v9}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 338
    .end local v0    # "monthString":Ljava/lang/String;
    :cond_7
    if-eqz v7, :cond_8

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_8

    .line 339
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v7}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 341
    :cond_8
    const/4 v5, 0x1

    .line 342
    .restart local v5    # "typefase_one":I
    const/4 v6, 0x0

    .line 343
    .restart local v6    # "typefase_two":I
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    .line 344
    .restart local v3    # "typefaceLeft":Landroid/graphics/Typeface;
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    .restart local v4    # "typefaceRight":Landroid/graphics/Typeface;
    goto :goto_0

    .line 355
    :cond_9
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_a

    .line 356
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v12, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    .line 357
    :cond_a
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    if-eqz v8, :cond_4

    .line 358
    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    invoke-virtual {v8, v12, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;I)V

    goto :goto_1
.end method

.method private setWeekday(I)V
    .locals 4
    .param p1, "diff"    # I

    .prologue
    const/4 v3, 0x2

    .line 445
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    if-nez v1, :cond_1

    .line 498
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    add-int/lit8 v1, p1, 0x1

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 451
    .local v0, "dayString":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 453
    add-int/lit8 v1, p1, 0x2

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 455
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457
    add-int/lit8 v1, p1, 0x3

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 459
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 461
    add-int/lit8 v1, p1, 0x4

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 463
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 465
    add-int/lit8 v1, p1, 0x5

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 467
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 469
    add-int/lit8 v1, p1, 0x6

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 471
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 473
    add-int/lit8 v1, p1, 0x7

    rem-int/lit8 v1, v1, 0x7

    add-int/lit8 v1, v1, 0x1

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 475
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 477
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_2

    .line 478
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSaturdayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 479
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSundayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 480
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 481
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 482
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthFridayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 496
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 497
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto/16 :goto_0

    .line 483
    :cond_2
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    if-ne v1, v3, :cond_3

    .line 484
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 485
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 486
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthFridayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 487
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSaturdayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 488
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSundayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1

    .line 490
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSundayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 491
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 492
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 493
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthFridayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    .line 494
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSaturdayColor:I

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTextColor(I)V

    goto :goto_1
.end method

.method private startWeatherDaemon()V
    .locals 1

    .prologue
    .line 501
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v0, :cond_1

    .line 502
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-nez v0, :cond_0

    .line 503
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 505
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->startCurrentLocationWeatherDataService()V

    .line 507
    :cond_1
    return-void
.end method

.method private updateView()V
    .locals 3

    .prologue
    .line 672
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    .line 673
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    add-int/lit8 v2, v2, -0x1

    add-int/lit8 v0, v2, -0x1

    .line 674
    .local v0, "diff":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->updateColorSet()V

    .line 675
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setWeekday(I)V

    .line 676
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V

    .line 677
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 678
    .local v1, "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setStartDay(I)V

    .line 679
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectedTime(Landroid/text/format/Time;)V

    .line 680
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents()V

    .line 681
    return-void
.end method


# virtual methods
.method public clearView()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 732
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    .line 733
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    .line 734
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    .line 735
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    .line 736
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    .line 737
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    .line 738
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    .line 740
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mPrevMonth:Landroid/widget/ImageButton;

    .line 741
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNextMonth:Landroid/widget/ImageButton;

    .line 742
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNewEvent:Landroid/widget/ImageButton;

    .line 743
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    .line 745
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDateContainer:Landroid/widget/LinearLayout;

    .line 746
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    .line 747
    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    .line 748
    return-void
.end method

.method public goTo(Landroid/text/format/Time;ZZ)V
    .locals 12
    .param p1, "time"    # Landroid/text/format/Time;
    .param p2, "animate"    # Z
    .param p3, "doScroll"    # Z

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 684
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isValidRange(Landroid/text/format/Time;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 729
    :cond_0
    :goto_0
    return-void

    .line 687
    :cond_1
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    if-eqz v5, :cond_0

    .line 688
    invoke-virtual {p1, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-wide v8, p1, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v4

    .line 689
    .local v4, "selectedJulianDay":I
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v5, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v2

    .line 690
    .local v2, "currentViewTime":I
    if-eq v4, v2, :cond_0

    .line 694
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V

    .line 695
    const/4 v0, 0x0

    .line 696
    .local v0, "bIsInCurrentMonth":Z
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->year:I

    iget v6, p1, Landroid/text/format/Time;->year:I

    if-ne v5, v6, :cond_2

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    iget v5, v5, Landroid/text/format/Time;->month:I

    iget v6, p1, Landroid/text/format/Time;->month:I

    if-ne v5, v6, :cond_2

    .line 697
    const/4 v0, 0x1

    .line 700
    :cond_2
    const-string v5, "myTest"

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "[MonthTheme] goTo.time : "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {p1}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 701
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v5, :cond_0

    .line 702
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 703
    .local v1, "current":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    invoke-virtual {v1, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectionMode(I)V

    .line 704
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5, v11}, Landroid/widget/ViewSwitcher;->setInAnimation(Landroid/view/animation/Animation;)V

    .line 705
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5, v11}, Landroid/widget/ViewSwitcher;->setOutAnimation(Landroid/view/animation/Animation;)V

    .line 710
    if-eqz v0, :cond_4

    .line 711
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 716
    .local v3, "monthView":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    :goto_1
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getSelectionMode()I

    move-result v5

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectionMode(I)V

    .line 717
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-virtual {v3, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setStartDay(I)V

    .line 718
    invoke-virtual {v3, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectedTime(Landroid/text/format/Time;)V

    .line 720
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v5, p1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 721
    invoke-virtual {v3, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents(Z)V

    .line 723
    if-nez v0, :cond_3

    .line 724
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->showNext()V

    .line 726
    :cond_3
    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->requestFocus()Z

    goto/16 :goto_0

    .line 713
    .end local v3    # "monthView":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    :cond_4
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->getNextView()Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .restart local v3    # "monthView":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    goto :goto_1
.end method

.method public goToMonth(II)V
    .locals 3
    .param p1, "monthOffset"    # I
    .param p2, "selectedDay"    # I

    .prologue
    .line 845
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 847
    .local v0, "other":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v1, :cond_0

    .line 848
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v1}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v1, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getPrevNextTime(II)Landroid/text/format/Time;

    move-result-object v0

    .line 853
    :goto_0
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isValidRange(Landroid/text/format/Time;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 854
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->launchOutOfBoundToast()V

    .line 858
    :goto_1
    return-void

    .line 850
    :cond_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    goto :goto_0

    .line 856
    :cond_1
    const/4 v1, 0x1

    const/4 v2, 0x0

    invoke-virtual {p0, v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->goTo(Landroid/text/format/Time;ZZ)V

    goto :goto_1
.end method

.method public initialize(Landroid/view/View;)V
    .locals 12
    .param p1, "root"    # Landroid/view/View;

    .prologue
    const/4 v11, -0x1

    const/4 v10, -0x2

    const/4 v7, 0x0

    .line 91
    invoke-super {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->initialize(Landroid/view/View;)V

    .line 93
    const v5, 0x7f0c004e

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    .line 94
    .local v0, "Title":Landroid/widget/TextView;
    const v5, 0x7f0c004b

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mPrevMonth:Landroid/widget/ImageButton;

    .line 95
    const v5, 0x7f0c004d

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNextMonth:Landroid/widget/ImageButton;

    .line 96
    const v5, 0x7f0c0012

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNewEvent:Landroid/widget/ImageButton;

    .line 97
    const v5, 0x7f0c0050

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/Button;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    .line 98
    const/high16 v5, 0x7f0c0000

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDateContainer:Landroid/widget/LinearLayout;

    .line 99
    const v5, 0x7f0c0007

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthTitle:Landroid/widget/TextView;

    .line 100
    const v5, 0x7f0c004c

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mYearTitle:Landroid/widget/TextView;

    .line 101
    const v5, 0x7f0c001f

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    .line 102
    const v5, 0x7f0c0020

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    .line 103
    const v5, 0x7f0c0021

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    .line 104
    const v5, 0x7f0c0022

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    .line 105
    const v5, 0x7f0c0023

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    .line 106
    const v5, 0x7f0c0024

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    .line 107
    const v5, 0x7f0c0025

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    .line 108
    const v5, 0x7f0c0055

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ViewSwitcher;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    .line 110
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    .line 111
    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v5, :cond_0

    .line 112
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->startWeatherDaemon()V

    .line 115
    :cond_0
    new-instance v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$1;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 122
    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsSupportSPenUsp:Z

    if-eqz v5, :cond_1

    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsTabletConfig:Z

    if-eqz v5, :cond_1

    .line 123
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDefaultOrientation(Landroid/content/Context;)I

    move-result v2

    .line 124
    .local v2, "defaultOrientation":I
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v1, v5, Landroid/content/res/Configuration;->orientation:I

    .line 126
    .local v1, "currentOrientation":I
    const v5, 0x7f0c0052

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageButton;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandWriting:Landroid/widget/ImageButton;

    .line 127
    const v5, 0x7f0c0051

    invoke-virtual {p1, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 129
    .local v4, "handWritingDivider":Landroid/view/View;
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandWriting:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$2;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 139
    if-ne v2, v1, :cond_1

    .line 140
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandWriting:Landroid/widget/ImageButton;

    invoke-virtual {v5, v7}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 141
    invoke-virtual {v4, v7}, Landroid/view/View;->setVisibility(I)V

    .line 145
    .end local v1    # "currentOrientation":I
    .end local v2    # "defaultOrientation":I
    .end local v4    # "handWritingDivider":Landroid/view/View;
    :cond_1
    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->isFontStyleDefault:Z

    if-eqz v5, :cond_3

    .line 146
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    if-eqz v5, :cond_2

    .line 147
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {v0, v5}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 148
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfLight:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setTypeface(Landroid/graphics/Typeface;)V

    .line 150
    :cond_2
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    if-eqz v5, :cond_3

    .line 151
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 152
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay1:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 153
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay2:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 154
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay3:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 155
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay4:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 156
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay5:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 157
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->tfRegular:Landroid/graphics/Typeface;

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 161
    :cond_3
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07001c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    .line 162
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070036

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSundayColor:I

    .line 163
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f070034

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSaturdayColor:I

    .line 164
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    add-int/lit8 v5, v5, -0x1

    add-int/lit8 v3, v5, -0x1

    .line 165
    .local v3, "diff":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->updateColorSet()V

    .line 166
    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setWeekday(I)V

    .line 168
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V

    .line 169
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v5}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v5

    const/16 v6, 0x8

    invoke-virtual {v5, v7, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCurrentDateOnResume:Ljava/lang/String;

    .line 171
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->removeAllViews()V

    .line 172
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;I)V

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v11, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6, v7}, Landroid/widget/ViewSwitcher;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 175
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    iget-object v8, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    iget v9, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-direct {v6, p0, v7, v8, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;I)V

    new-instance v7, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v7, v11, v10}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {v5, v6, v7}, Landroid/widget/ViewSwitcher;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 179
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    .line 180
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iput-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 181
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectedTime(Landroid/text/format/Time;)V

    .line 182
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setStartDay(I)V

    .line 183
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setEventLoader(Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;)V

    .line 184
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->init(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 186
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->updateView()V

    .line 187
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v5}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v5

    check-cast v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents()V

    .line 189
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mPrevMonth:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$3;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$3;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 206
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNextMonth:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$4;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$4;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 226
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNewEvent:Landroid/widget/ImageButton;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$5;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$5;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 233
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$6;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 253
    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsTabletConfig:Z

    if-nez v5, :cond_4

    iget-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsXhdpiConfig:Z

    if-nez v5, :cond_4

    .line 254
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay0:Landroid/widget/TextView;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$7;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 271
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDay6:Landroid/widget/TextView;

    new-instance v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$8;

    invoke-direct {v6, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$8;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 291
    :cond_4
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isChinaFeature()Z

    move-result v5

    if-eqz v5, :cond_5

    .line 292
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->getInstance()Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    move-result-object v5

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->updateHolidayList(Landroid/content/ContentResolver;)V

    .line 294
    :cond_5
    const/4 v5, 0x1

    iput-boolean v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mInitialized:Z

    .line 295
    return-void
.end method

.method public isToday(J)Z
    .locals 5
    .param p1, "when"    # J

    .prologue
    const/4 v3, 0x0

    .line 402
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 403
    .local v0, "time":Landroid/text/format/Time;
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 406
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 407
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 412
    .local v1, "today":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 414
    iget v2, v1, Landroid/text/format/Time;->year:I

    iget v3, v0, Landroid/text/format/Time;->year:I

    if-ne v2, v3, :cond_1

    iget v2, v1, Landroid/text/format/Time;->month:I

    iget v3, v0, Landroid/text/format/Time;->month:I

    if-ne v2, v3, :cond_1

    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 409
    .end local v1    # "today":Landroid/text/format/Time;
    :cond_0
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v1    # "today":Landroid/text/format/Time;
    goto :goto_0

    .line 414
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method public onContentRequest()I
    .locals 1

    .prologue
    .line 86
    const v0, 0x7f03000a

    return v0
.end method

.method public onFinishInflate(ILandroid/view/View;)V
    .locals 0
    .param p1, "instance"    # I
    .param p2, "topView"    # Landroid/view/View;

    .prologue
    .line 81
    invoke-virtual {p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->initialize(Landroid/view/View;)V

    .line 82
    return-void
.end method

.method public onGoToToday()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 829
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v1

    .line 830
    .local v1, "isUsedSystemTimezone":Z
    if-eqz v1, :cond_1

    .line 831
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 835
    .local v0, "Today":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 836
    if-eqz v1, :cond_0

    .line 837
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 838
    const/4 v2, 0x1

    invoke-virtual {v0, v2}, Landroid/text/format/Time;->normalize(Z)J

    .line 840
    :cond_0
    invoke-virtual {p0, v0, v3, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->goTo(Landroid/text/format/Time;ZZ)V

    .line 841
    return-void

    .line 833
    .end local v0    # "Today":Landroid/text/format/Time;
    :cond_1
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    invoke-static {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v0    # "Today":Landroid/text/format/Time;
    goto :goto_0
.end method

.method public onPause(I)V
    .locals 4
    .param p1, "instance"    # I

    .prologue
    .line 562
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    if-nez v1, :cond_0

    .line 563
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-direct {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    .line 565
    :cond_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_1

    .line 566
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToToday:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 567
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTodayUpdater:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 570
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    if-eqz v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v1, :cond_3

    .line 571
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-nez v1, :cond_2

    .line 572
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 574
    :cond_2
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 577
    :cond_3
    new-instance v0, Landroid/text/format/Time;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    const/4 v2, 0x0

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 578
    .local v0, "deviceTime":Landroid/text/format/Time;
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 579
    invoke-virtual {v0}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    const/16 v3, 0x8

    invoke-virtual {v1, v2, v3}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCurrentDateOnPause:Ljava/lang/String;

    .line 580
    return-void
.end method

.method public onRefresh()V
    .locals 10

    .prologue
    const/4 v8, 0x1

    .line 584
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    .line 585
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v4, :cond_0

    .line 586
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->startWeatherDaemon()V

    .line 589
    :cond_0
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mInitialized:Z

    if-nez v4, :cond_2

    .line 634
    :cond_1
    :goto_0
    return-void

    .line 593
    :cond_2
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v4, :cond_4

    .line 594
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-nez v4, :cond_5

    .line 595
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 596
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->startCurrentLocationWeatherDataService()V

    .line 618
    :cond_3
    :goto_1
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->updateWeatherInfo()V

    .line 620
    :cond_4
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v4

    iput v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    .line 621
    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    add-int/lit8 v4, v4, -0x1

    add-int/lit8 v0, v4, -0x1

    .line 622
    .local v0, "diff":I
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->updateColorSet()V

    .line 623
    invoke-direct {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setWeekday(I)V

    .line 624
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V

    .line 626
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v4, :cond_1

    .line 627
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 628
    .local v2, "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    if-eqz v2, :cond_1

    .line 629
    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mStartDay:I

    invoke-virtual {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setStartDay(I)V

    .line 630
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setSelectedTime(Landroid/text/format/Time;)V

    .line 631
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->reloadEvents()V

    goto :goto_0

    .line 598
    .end local v0    # "diff":I
    .end local v2    # "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    :cond_5
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    const/4 v5, 0x0

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 599
    .local v3, "now":Landroid/text/format/Time;
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 601
    invoke-virtual {v3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    invoke-virtual {v5, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v5

    sub-int v1, v4, v5

    .line 606
    .local v1, "diff_day":I
    if-lez v1, :cond_3

    .line 607
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->stopCurrentLocationWeatherDataService()V

    .line 609
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$11;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$11;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_1
.end method

.method public onResume(I)V
    .locals 12
    .param p1, "instance"    # I

    .prologue
    const/4 v11, 0x0

    const/4 v10, 0x1

    .line 511
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v4

    iput-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    .line 512
    iget-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mIsWeatherEnabled:Z

    if-eqz v4, :cond_1

    .line 513
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-nez v4, :cond_3

    .line 514
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 515
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->startCurrentLocationWeatherDataService()V

    .line 537
    :cond_0
    :goto_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->updateWeatherInfo()V

    .line 540
    :cond_1
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    invoke-static {v4, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 541
    .local v3, "systemTime":Landroid/text/format/Time;
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 542
    invoke-virtual {v3}, Landroid/text/format/Time;->format2445()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/String;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v4

    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCurrentDateOnResume:Ljava/lang/String;

    .line 543
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCurrentDateOnResume:Ljava/lang/String;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCurrentDateOnPause:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 544
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->onGoToToday()V

    .line 546
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTodayUpdater:Ljava/lang/Runnable;

    invoke-interface {v4}, Ljava/lang/Runnable;->run()V

    .line 547
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mCheckTimeZone:Ljava/lang/Runnable;

    const-wide/16 v6, 0xc8

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 549
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v4}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 551
    .local v1, "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    if-eqz v1, :cond_4

    .line 552
    invoke-virtual {v1, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setRedrawScreen(Z)V

    .line 553
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->updateTimezone()V

    .line 554
    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->invalidate()V

    .line 558
    :goto_1
    return-void

    .line 517
    .end local v1    # "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .end local v3    # "systemTime":Landroid/text/format/Time;
    :cond_3
    new-instance v2, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mContext:Landroid/content/Context;

    invoke-static {v4, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 518
    .local v2, "now":Landroid/text/format/Time;
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 520
    invoke-virtual {v2, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v4

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    invoke-virtual {v5, v10}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v6

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    iget-wide v8, v5, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v6, v7, v8, v9}, Landroid/text/format/Time;->getJulianDay(JJ)I

    move-result v5

    sub-int v0, v4, v5

    .line 525
    .local v0, "diff_day":I
    if-lez v0, :cond_0

    .line 526
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->stopCurrentLocationWeatherDataService()V

    .line 528
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mHandler:Landroid/os/Handler;

    new-instance v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$10;

    invoke-direct {v5, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$10;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)V

    const-wide/16 v6, 0x32

    invoke-virtual {v4, v5, v6, v7}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto/16 :goto_0

    .line 556
    .end local v0    # "diff_day":I
    .end local v2    # "now":Landroid/text/format/Time;
    .restart local v1    # "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    .restart local v3    # "systemTime":Landroid/text/format/Time;
    :cond_4
    sget-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->TAG:Ljava/lang/String;

    const-string v5, "Cant get the currentView!! Fail to redraw month view"

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_1
.end method

.method public onThemeDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 638
    const-string v2, "SPlannerSurfaceWidget"

    const-string v3, "[MonthTheme] onDestroy"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 639
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->mFirstTZRequest:Z

    .line 640
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->releaseOnClickListener()V

    .line 641
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2}, Landroid/widget/ViewSwitcher;->getChildCount()I

    move-result v0

    .line 642
    .local v0, "cnt":I
    const/4 v1, 0x0

    .local v1, "t":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 643
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v2, v1}, Landroid/widget/ViewSwitcher;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->clear()V

    .line 642
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 645
    :cond_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mEventLoader:Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/EventLoader;->stopBackgroundThread()V

    .line 646
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->clear()V

    .line 647
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    .line 648
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSwitcher:Landroid/widget/ViewSwitcher;

    .line 649
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->currentView:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 651
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isChinaFeature()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 652
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->getInstance()Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/FestivalEffectManager;->clearHolidayList()V

    .line 654
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->clearView()V

    .line 655
    return-void
.end method

.method public onTouchEvent(ILandroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "instance"    # I
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 668
    const/4 v0, 0x0

    return v0
.end method

.method public releaseOnClickListener()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 659
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mPrevMonth:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 660
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNextMonth:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 661
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mNewEvent:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 662
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 663
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mDateContainer:Landroid/widget/LinearLayout;

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 664
    return-void
.end method

.method public reloadCurrentDayEvents()V
    .locals 4

    .prologue
    .line 392
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    if-eqz v3, :cond_0

    .line 393
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSwitcher:Landroid/widget/ViewSwitcher;

    invoke-virtual {v3}, Landroid/widget/ViewSwitcher;->getCurrentView()Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 394
    .local v2, "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->getSelectedMillis()J

    move-result-wide v0

    .line 395
    .local v0, "millis":J
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v3, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 396
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mTime:Landroid/text/format/Time;

    invoke-direct {p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V

    .line 398
    .end local v0    # "millis":J
    .end local v2    # "mv":Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
    :cond_0
    return-void
.end method

.method protected updateColorSet()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 420
    const/4 v5, 0x3

    new-array v0, v5, [I

    .line 422
    .local v0, "colorSet":[I
    const-string v1, "XXXXXXR"

    .line 424
    .local v1, "defaultWeekdayFeatureString":Ljava/lang/String;
    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f07001c

    invoke-virtual {v5, v6}, Landroid/content/res/Resources;->getColor(I)I

    move-result v5

    aput v5, v0, v8

    .line 425
    const/4 v5, 0x1

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070034

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    aput v6, v0, v5

    .line 426
    const/4 v5, 0x2

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f070036

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getColor(I)I

    move-result v6

    aput v6, v0, v5

    .line 429
    :try_start_0
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v5

    const-string v6, "CscFeature_Calendar_SetColorOfDays"

    invoke-virtual {v5, v6}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 435
    .local v4, "weekdayFeatureString":Ljava/lang/String;
    :goto_0
    invoke-static {v4, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->parseWeekdayColor(Ljava/lang/String;[I)[I

    move-result-object v3

    .line 437
    .local v3, "mDayNumberColors":[I
    aget v5, v3, v8

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthDayColor:I

    .line 438
    const/4 v5, 0x4

    aget v5, v3, v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthFridayColor:I

    .line 439
    const/4 v5, 0x5

    aget v5, v3, v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSaturdayColor:I

    .line 440
    const/4 v5, 0x6

    aget v5, v3, v5

    iput v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mMonthSundayColor:I

    .line 442
    return-void

    .line 431
    .end local v3    # "mDayNumberColors":[I
    .end local v4    # "weekdayFeatureString":Ljava/lang/String;
    :catch_0
    move-exception v2

    .line 432
    .local v2, "e":Ljava/lang/Exception;
    move-object v4, v1

    .restart local v4    # "weekdayFeatureString":Ljava/lang/String;
    goto :goto_0
.end method

.method public updateTodayButton(Landroid/text/format/Time;)V
    .locals 4
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v1, 0x1

    .line 298
    invoke-virtual {p1, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->isToday(J)Z

    move-result v0

    .line 300
    .local v0, "isToday":Z
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mGoToday:Landroid/widget/Button;

    if-nez v0, :cond_0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/widget/Button;->setEnabled(Z)V

    .line 301
    return-void

    .line 300
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
