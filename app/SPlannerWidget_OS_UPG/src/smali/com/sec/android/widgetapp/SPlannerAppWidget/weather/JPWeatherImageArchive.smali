.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/JPWeatherImageArchive;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;
.source "JPWeatherImageArchive.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;-><init>()V

    return-void
.end method


# virtual methods
.method public getImage(I)I
    .locals 3
    .param p1, "index"    # I

    .prologue
    const v2, 0x7f02003e

    const v1, 0x7f020039

    const v0, 0x7f020038

    .line 8
    sparse-switch p1, :sswitch_data_0

    .line 179
    :goto_0
    :sswitch_0
    return v0

    :sswitch_1
    move v0, v1

    .line 26
    goto :goto_0

    .line 30
    :sswitch_2
    const v0, 0x7f02003a

    goto :goto_0

    .line 57
    :sswitch_3
    const v0, 0x7f020044

    goto :goto_0

    :sswitch_4
    move v0, v2

    .line 84
    goto :goto_0

    .line 89
    :sswitch_5
    const v0, 0x7f020040

    goto :goto_0

    .line 102
    :sswitch_6
    const v0, 0x7f02003d

    goto :goto_0

    .line 129
    :sswitch_7
    const v0, 0x7f020043

    goto :goto_0

    .line 147
    :sswitch_8
    const v0, 0x7f020045

    goto :goto_0

    .line 160
    :sswitch_9
    const v0, 0x7f020042

    goto :goto_0

    .line 162
    :sswitch_a
    const v0, 0x7f020046

    goto :goto_0

    :sswitch_b
    move v0, v1

    .line 167
    goto :goto_0

    :sswitch_c
    move v0, v2

    .line 176
    goto :goto_0

    .line 8
    nop

    :sswitch_data_0
    .sparse-switch
        0x64 -> :sswitch_0
        0x65 -> :sswitch_1
        0x66 -> :sswitch_4
        0x67 -> :sswitch_4
        0x68 -> :sswitch_9
        0x69 -> :sswitch_9
        0x6a -> :sswitch_4
        0x6b -> :sswitch_4
        0x6c -> :sswitch_4
        0x6e -> :sswitch_1
        0x6f -> :sswitch_1
        0x70 -> :sswitch_4
        0x71 -> :sswitch_4
        0x72 -> :sswitch_4
        0x73 -> :sswitch_9
        0x74 -> :sswitch_9
        0x75 -> :sswitch_9
        0x76 -> :sswitch_4
        0x77 -> :sswitch_4
        0x78 -> :sswitch_4
        0x79 -> :sswitch_4
        0x7a -> :sswitch_4
        0x7b -> :sswitch_0
        0x7c -> :sswitch_0
        0x7d -> :sswitch_4
        0x7e -> :sswitch_4
        0x7f -> :sswitch_4
        0x80 -> :sswitch_4
        0x81 -> :sswitch_4
        0x82 -> :sswitch_0
        0x83 -> :sswitch_0
        0x84 -> :sswitch_1
        0x8c -> :sswitch_4
        0xa0 -> :sswitch_9
        0xaa -> :sswitch_9
        0xb5 -> :sswitch_9
        0xc8 -> :sswitch_2
        0xc9 -> :sswitch_1
        0xca -> :sswitch_3
        0xcb -> :sswitch_3
        0xcc -> :sswitch_7
        0xcd -> :sswitch_7
        0xce -> :sswitch_3
        0xcf -> :sswitch_3
        0xd0 -> :sswitch_3
        0xd1 -> :sswitch_2
        0xd2 -> :sswitch_1
        0xd3 -> :sswitch_1
        0xd4 -> :sswitch_3
        0xd5 -> :sswitch_3
        0xd6 -> :sswitch_3
        0xd7 -> :sswitch_7
        0xd8 -> :sswitch_7
        0xd9 -> :sswitch_7
        0xda -> :sswitch_3
        0xdb -> :sswitch_3
        0xdc -> :sswitch_3
        0xdd -> :sswitch_3
        0xde -> :sswitch_3
        0xdf -> :sswitch_1
        0xe0 -> :sswitch_3
        0xe1 -> :sswitch_3
        0xe2 -> :sswitch_3
        0xe3 -> :sswitch_3
        0xe4 -> :sswitch_7
        0xe5 -> :sswitch_7
        0xe6 -> :sswitch_7
        0xe7 -> :sswitch_2
        0xf0 -> :sswitch_3
        0xfa -> :sswitch_7
        0x104 -> :sswitch_7
        0x10e -> :sswitch_7
        0x119 -> :sswitch_7
        0x12c -> :sswitch_6
        0x12d -> :sswitch_4
        0x12e -> :sswitch_3
        0x12f -> :sswitch_8
        0x130 -> :sswitch_6
        0x132 -> :sswitch_6
        0x134 -> :sswitch_6
        0x135 -> :sswitch_8
        0x137 -> :sswitch_4
        0x139 -> :sswitch_3
        0x13a -> :sswitch_8
        0x13b -> :sswitch_8
        0x13c -> :sswitch_4
        0x13d -> :sswitch_3
        0x140 -> :sswitch_4
        0x141 -> :sswitch_3
        0x142 -> :sswitch_8
        0x143 -> :sswitch_4
        0x144 -> :sswitch_4
        0x145 -> :sswitch_4
        0x146 -> :sswitch_8
        0x147 -> :sswitch_8
        0x148 -> :sswitch_6
        0x149 -> :sswitch_6
        0x154 -> :sswitch_7
        0x15e -> :sswitch_6
        0x169 -> :sswitch_9
        0x173 -> :sswitch_7
        0x190 -> :sswitch_7
        0x191 -> :sswitch_9
        0x192 -> :sswitch_7
        0x193 -> :sswitch_8
        0x195 -> :sswitch_7
        0x196 -> :sswitch_7
        0x197 -> :sswitch_7
        0x199 -> :sswitch_8
        0x19b -> :sswitch_9
        0x19d -> :sswitch_7
        0x19e -> :sswitch_8
        0x1a4 -> :sswitch_9
        0x1a5 -> :sswitch_7
        0x1a6 -> :sswitch_8
        0x1a7 -> :sswitch_8
        0x1a8 -> :sswitch_8
        0x1a9 -> :sswitch_7
        0x1aa -> :sswitch_7
        0x1ab -> :sswitch_7
        0x1ae -> :sswitch_7
        0x1c2 -> :sswitch_7
        0x1f4 -> :sswitch_0
        0x226 -> :sswitch_a
        0x228 -> :sswitch_b
        0x229 -> :sswitch_c
        0x22e -> :sswitch_c
        0x232 -> :sswitch_b
        0x233 -> :sswitch_c
        0x238 -> :sswitch_c
        0x23c -> :sswitch_b
        0x23d -> :sswitch_c
        0x246 -> :sswitch_b
        0x247 -> :sswitch_c
        0x320 -> :sswitch_0
        0x352 -> :sswitch_6
        0x353 -> :sswitch_5
        0x354 -> :sswitch_3
        0x355 -> :sswitch_6
        0x356 -> :sswitch_8
        0x357 -> :sswitch_c
        0x35d -> :sswitch_5
        0x35e -> :sswitch_3
        0x35f -> :sswitch_6
        0x360 -> :sswitch_8
        0x361 -> :sswitch_c
        0x367 -> :sswitch_5
        0x368 -> :sswitch_3
        0x369 -> :sswitch_6
        0x36a -> :sswitch_8
        0x371 -> :sswitch_5
        0x372 -> :sswitch_3
        0x373 -> :sswitch_6
        0x374 -> :sswitch_8
    .end sparse-switch
.end method
