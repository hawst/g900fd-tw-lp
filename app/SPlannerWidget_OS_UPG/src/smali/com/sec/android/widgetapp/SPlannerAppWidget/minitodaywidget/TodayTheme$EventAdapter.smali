.class Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;
.super Landroid/widget/BaseAdapter;
.source "TodayTheme.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "EventAdapter"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    }
.end annotation


# instance fields
.field private mContext:Landroid/content/Context;

.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Landroid/content/Context;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 610
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {p0}, Landroid/widget/BaseAdapter;-><init>()V

    .line 611
    iput-object p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    .line 612
    return-void
.end method

.method static synthetic access$2200(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;I)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;
    .param p1, "x1"    # I

    .prologue
    .line 594
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getTaskPosition(I)I

    move-result v0

    return v0
.end method

.method private bindEventItem(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V
    .locals 17
    .param p1, "viewHolder"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 698
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    move/from16 v0, p2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 699
    .local v10, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    if-nez v10, :cond_0

    .line 750
    :goto_0
    return-void

    .line 703
    :cond_0
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAccountColor:Landroid/widget/ImageView;

    iget v3, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 704
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 705
    invoke-static {v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->isFacebook(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 706
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 710
    :goto_1
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v3}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v3

    and-int/lit8 v3, v3, -0x11

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 712
    iget-boolean v2, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-eqz v2, :cond_3

    .line 713
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v3, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 714
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    const v3, 0x7f0a001a

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(I)V

    .line 724
    :goto_2
    const/4 v13, 0x0

    .line 725
    .local v13, "stickerImage":Landroid/graphics/Bitmap;
    invoke-static {v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->hasSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 726
    iget-wide v14, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    .line 727
    .local v14, "stickerType":J
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    invoke-static {v2, v14, v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->getStickerImagePath(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v12

    .line 728
    .local v12, "path":Ljava/lang/String;
    const/4 v2, 0x0

    const/4 v3, 0x0

    invoke-static {v12, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/StickerUtils;->decodeBitmap(Ljava/lang/String;II)Landroid/graphics/Bitmap;

    move-result-object v13

    .line 731
    .end local v12    # "path":Ljava/lang/String;
    .end local v14    # "stickerType":J
    :cond_1
    if-eqz v13, :cond_5

    .line 732
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mStickerImage:Landroid/widget/ImageView;

    invoke-virtual {v2, v13}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 733
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mStickerImage:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 738
    :goto_3
    iget-boolean v2, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    if-eqz v2, :cond_6

    .line 739
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAlarmImage:Landroid/widget/ImageView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 744
    :goto_4
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 749
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 708
    .end local v13    # "stickerImage":Landroid/graphics/Bitmap;
    :cond_2
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 716
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    const/4 v3, 0x0

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v11

    .line 717
    .local v11, "eventTimezone":Ljava/lang/String;
    invoke-static {v11}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    const/4 v9, 0x0

    .line 718
    .local v9, "timezone":Ljava/lang/String;
    :goto_5
    move-object/from16 v0, p1

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$2000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v2

    const/4 v3, 0x0

    iget-wide v4, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    iget-wide v6, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    const/4 v8, 0x0

    invoke-static/range {v2 .. v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->formatDateTimeString(Landroid/content/Context;ZJJZLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v16

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 720
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v3, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 721
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_2

    .end local v9    # "timezone":Ljava/lang/String;
    :cond_4
    move-object v9, v11

    .line 717
    goto :goto_5

    .line 735
    .end local v11    # "eventTimezone":Ljava/lang/String;
    .restart local v13    # "stickerImage":Landroid/graphics/Bitmap;
    :cond_5
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mStickerImage:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_3

    .line 741
    :cond_6
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAlarmImage:Landroid/widget/ImageView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_4
.end method

.method private bindListener(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 815
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mItemLayout:Landroid/widget/LinearLayout;

    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$1;

    invoke-direct {v1, p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;I)V

    invoke-virtual {v0, v1}, Landroid/widget/LinearLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 861
    return-void
.end method

.method private bindPriority(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V
    .locals 2
    .param p1, "viewHolder"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    .param p2, "importance"    # I

    .prologue
    const/4 v1, 0x0

    .line 797
    packed-switch p2, :pswitch_data_0

    .line 812
    :goto_0
    return-void

    .line 799
    :pswitch_0
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 800
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    const v1, 0x7f02001c

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 803
    :pswitch_1
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_0

    .line 806
    :pswitch_2
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 807
    iget-object v0, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    const v1, 0x7f02001b

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setBackgroundResource(I)V

    goto :goto_0

    .line 797
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private bindTaskItem(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V
    .locals 9
    .param p1, "viewHolder"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    .param p2, "position"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x0

    const/16 v6, 0x8

    .line 753
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    invoke-direct {p0, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getTaskPosition(I)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    .line 754
    .local v2, "task":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    if-nez v2, :cond_0

    .line 794
    :goto_0
    return-void

    .line 758
    :cond_0
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAccountColor:Landroid/widget/ImageView;

    iget v4, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    invoke-virtual {v3, v4}, Landroid/widget/ImageView;->setBackgroundColor(I)V

    .line 759
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v4, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 760
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 761
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    invoke-virtual {v3, v6}, Landroid/widget/TextView;->setVisibility(I)V

    .line 762
    iget v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    invoke-direct {p0, p1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->bindPriority(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V

    .line 763
    iget-boolean v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    if-eqz v3, :cond_1

    .line 764
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAlarmImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 768
    :goto_1
    iget-boolean v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    if-eqz v3, :cond_2

    .line 769
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    const v4, 0x7f020028

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    .line 770
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020035

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 772
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v4

    or-int/lit8 v4, v4, 0x10

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setPaintFlags(I)V

    .line 781
    :goto_2
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020034

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 783
    iget-wide v0, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    .line 784
    .local v0, "id":J
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 785
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 786
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v7}, Landroid/widget/CheckBox;->setFocusable(Z)V

    .line 787
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    iget-boolean v4, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 788
    iget-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->canModify(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 789
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    new-instance v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {v4, v5, v0, v1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;JI)V

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 793
    :goto_3
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mStickerImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto/16 :goto_0

    .line 766
    .end local v0    # "id":J
    :cond_1
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAlarmImage:Landroid/widget/ImageView;

    invoke-virtual {v3, v6}, Landroid/widget/ImageView;->setVisibility(I)V

    goto :goto_1

    .line 775
    :cond_2
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    const v4, 0x7f020027

    invoke-virtual {v3, v4}, Landroid/widget/CheckBox;->setButtonDrawable(I)V

    .line 776
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    invoke-virtual {v4}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f020036

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getColorStateList(I)Landroid/content/res/ColorStateList;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setTextColor(Landroid/content/res/ColorStateList;)V

    .line 778
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v4, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    invoke-virtual {v4}, Landroid/widget/TextView;->getPaintFlags()I

    move-result v4

    and-int/lit8 v4, v4, -0x11

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setPaintFlags(I)V

    goto :goto_2

    .line 791
    .restart local v0    # "id":J
    :cond_3
    iget-object v3, p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    invoke-virtual {v3, v8}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto :goto_3
.end method

.method private getTaskPosition(I)I
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 654
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->isTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 655
    const/4 v0, -0x1

    .line 657
    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getEventCount()I

    move-result v0

    sub-int v0, p1, v0

    goto :goto_0
.end method


# virtual methods
.method public getCount()I
    .locals 2

    .prologue
    .line 620
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getEventCount()I

    move-result v0

    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getTaskCount()I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method public getEventCount()I
    .locals 1

    .prologue
    .line 624
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 633
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->isTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 634
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 636
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getTaskPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 642
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->isTask(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 643
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mEventsArray:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    .line 645
    :goto_0
    return-wide v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getTaskPosition(I)I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    iget-wide v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    goto :goto_0
.end method

.method public getTaskCount()I
    .locals 1

    .prologue
    .line 628
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    goto :goto_0
.end method

.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 665
    if-nez p2, :cond_1

    .line 666
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->mContext:Landroid/content/Context;

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f030004

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object p2

    .line 667
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$1;)V

    .line 668
    .local v0, "viewHolder":Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    const v1, 0x7f0c0036

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAccountColor:Landroid/widget/ImageView;

    .line 669
    const v1, 0x7f0c0038

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    .line 670
    const v1, 0x7f0c0039

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    .line 671
    const v1, 0x7f0c003a

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mIcon:Landroid/widget/ImageView;

    .line 672
    const v1, 0x7f0c0037

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/CheckBox;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mCheckBox:Landroid/widget/CheckBox;

    .line 673
    const v1, 0x7f0c003d

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mStickerImage:Landroid/widget/ImageView;

    .line 674
    const v1, 0x7f0c003c

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mPriorityImage:Landroid/widget/ImageView;

    .line 675
    const v1, 0x7f0c003b

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mAlarmImage:Landroid/widget/ImageView;

    .line 676
    const v1, 0x7f0c0035

    invoke-virtual {p2, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/LinearLayout;

    iput-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mItemLayout:Landroid/widget/LinearLayout;

    .line 677
    invoke-virtual {p2, v0}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 682
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-boolean v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->isFontStyleDefault:Z

    if-eqz v1, :cond_0

    .line 683
    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTime:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$1800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 684
    iget-object v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;->mTitle:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->tfLight:Landroid/graphics/Typeface;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$1900(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/graphics/Typeface;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setTypeface(Landroid/graphics/Typeface;)V

    .line 687
    :cond_0
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->isTask(I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 688
    invoke-direct {p0, v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->bindTaskItem(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V

    .line 692
    :goto_1
    invoke-direct {p0, v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->bindListener(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V

    .line 694
    return-object p2

    .line 679
    .end local v0    # "viewHolder":Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    :cond_1
    invoke-virtual {p2}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;

    .restart local v0    # "viewHolder":Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;
    goto :goto_0

    .line 690
    :cond_2
    invoke-direct {p0, v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->bindEventItem(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter$ViewHolder;I)V

    goto :goto_1
.end method

.method public isTask(I)Z
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 650
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->getEventCount()I

    move-result v0

    if-lt p1, v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setTasksArray(Ljava/util/ArrayList;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 615
    .local p1, "tasksArray":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$EventAdapter;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iput-object p1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTasksArray:Ljava/util/ArrayList;

    .line 616
    return-void
.end method
