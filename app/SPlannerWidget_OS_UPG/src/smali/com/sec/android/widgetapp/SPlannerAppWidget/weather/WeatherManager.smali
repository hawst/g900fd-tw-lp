.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
.super Ljava/lang/Object;
.source "WeatherManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherInfo;,
        Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;
    }
.end annotation


# static fields
.field private static final PRJ_INDEX_LOCATION_NAME:I = 0x19

.field private static final PRJ_INDEX_TEMP_SCALE:I = 0x9

.field private static final PRJ_INDEX_TODAY_DATE:I = 0x7

.field private static final PRJ_INDEX_TODAY_HIGH_TEMP:I = 0xb

.field private static final PRJ_INDEX_TODAY_ICON:I = 0x0

.field private static final PRJ_INDEX_TODAY_LOW_TEMP:I = 0x12

.field private static final PRJ_INDEX_TODAY_TEMP:I = 0x8

.field private static final PRJ_INDEX_TODAY_WEATHER_TEXT:I = 0xa

.field private static final TAG:Ljava/lang/String; = "WeatherManager"

.field public static isReceiverEnabled:Z

.field public static onPausedTime:Landroid/text/format/Time;

.field private static sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

.field private static sLock:Ljava/lang/Object;


# instance fields
.field public ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

.field public ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

.field public ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

.field public ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

.field public CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

.field public CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

.field private final WEATHER_PROJECTION:[Ljava/lang/String;

.field private mCPName:Ljava/lang/String;

.field private mContext:Landroid/content/Context;

.field private mHighLowTemperatureData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private mLocationText:Ljava/lang/String;

.field private mOps:Landroid/graphics/BitmapFactory$Options;

.field private mTemperature:Ljava/lang/String;

.field private mWeatherData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private mWeatherImageArchive:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;

.field private mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

.field private mWeatherQueryHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

.field private mWeatherText:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 24
    new-instance v0, Ljava/lang/Object;

    invoke-direct {v0}, Ljava/lang/Object;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sLock:Ljava/lang/Object;

    .line 41
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->isReceiverEnabled:Z

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    .line 100
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "accuweather"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    .line 49
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ONEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "TWODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "THREEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "FOURDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "FIVEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SIXDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TODAY_DATE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TODAY_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TODAY_WEATHER_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ONEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TWODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "THREEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "FOURDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "FIVEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SIXDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "TODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ONEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "TWODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "THREEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "FOURDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "FIVEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SIXDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "NAME"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->WEATHER_PROJECTION:[Ljava/lang/String;

    .line 101
    return-void
.end method

.method private constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 103
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const-string v0, "accuweather"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    .line 49
    const/16 v0, 0x1a

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "TODAY_ICON_NUM"

    aput-object v2, v0, v1

    const-string v1, "ONEDAY_ICON_NUM"

    aput-object v1, v0, v3

    const/4 v1, 0x2

    const-string v2, "TWODAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "THREEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "FOURDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "FIVEDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "SIXDAY_ICON_NUM"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "TODAY_DATE"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "TODAY_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "TEMP_SCALE"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "TODAY_WEATHER_TEXT"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "TODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ONEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "TWODAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "THREEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "FOURDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "FIVEDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "SIXDAY_HIGH_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "TODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "ONEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "TWODAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "THREEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "FOURDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "FIVEDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "SIXDAY_LOW_TEMP"

    aput-object v2, v0, v1

    const/16 v1, 0x19

    const-string v2, "NAME"

    aput-object v2, v0, v1

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->WEATHER_PROJECTION:[Ljava/lang/String;

    .line 104
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    .line 105
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mOps:Landroid/graphics/BitmapFactory$Options;

    .line 106
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mOps:Landroid/graphics/BitmapFactory$Options;

    iput v3, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 107
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->onPausedTime:Landroid/text/format/Time;

    .line 108
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->init()V

    .line 109
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->updateWeather(Landroid/database/Cursor;)V

    return-void
.end method

.method private createWeatherImage(I)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "index"    # I

    .prologue
    .line 336
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getWeatherImageResourceId(I)I

    move-result v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mOps:Landroid/graphics/BitmapFactory$Options;

    invoke-static {v0, v1, v2}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;ILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method

.method private getCMAWeatherConvertedIndex(Ljava/lang/String;I)I
    .locals 4
    .param p1, "key"    # Ljava/lang/String;
    .param p2, "index"    # I

    .prologue
    .line 308
    const/4 v3, -0x1

    if-ne p2, v3, :cond_0

    move v0, p2

    .line 318
    .end local p2    # "index":I
    .local v0, "index":I
    :goto_0
    return v0

    .line 311
    .end local v0    # "index":I
    .restart local p2    # "index":I
    :cond_0
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 312
    .local v1, "today":Landroid/text/format/Time;
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 313
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getKeyFromTime(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v2

    .line 315
    .local v2, "todayJulianDay":Ljava/lang/String;
    invoke-virtual {p1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 316
    add-int/lit8 p2, p2, 0x64

    :cond_1
    move v0, p2

    .line 318
    .end local p2    # "index":I
    .restart local v0    # "index":I
    goto :goto_0
.end method

.method public static getInstance(Landroid/content/Context;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 88
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sLock:Ljava/lang/Object;

    monitor-enter v1

    .line 89
    :try_start_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    if-nez v0, :cond_0

    .line 90
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    .line 92
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 93
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->sInstance:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    return-object v0

    .line 92
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method

.method public static getKeyFromTime(Landroid/text/format/Time;)Ljava/lang/String;
    .locals 1
    .param p0, "time"    # Landroid/text/format/Time;

    .prologue
    .line 244
    const-string v0, "%Y%m%d"

    invoke-virtual {p0, v0}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getWeatherImageResourceId(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 341
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherImageArchive:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;

    invoke-virtual {v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;->getImage(I)I

    move-result v0

    return v0
.end method

.method private init()V
    .locals 2

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getCPName()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    .line 113
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherObjectFactory;->createWeatherKeyStrings(Ljava/lang/String;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    .line 114
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherObjectFactory;->createWeatherImageArchive(Ljava/lang/String;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherImageArchive:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherImageArchive;

    .line 115
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    .line 116
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherQueryHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    .line 118
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mHighLowTemperatureData:Ljava/util/HashMap;

    .line 120
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    .line 121
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    .line 122
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    .line 123
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    .line 124
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

    .line 125
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

    .line 126
    return-void
.end method

.method public static isNight()Z
    .locals 3

    .prologue
    .line 347
    new-instance v1, Ljava/util/Date;

    invoke-direct {v1}, Ljava/util/Date;-><init>()V

    invoke-virtual {v1}, Ljava/util/Date;->getHours()I

    move-result v1

    mul-int/lit8 v1, v1, 0x64

    new-instance v2, Ljava/util/Date;

    invoke-direct {v2}, Ljava/util/Date;-><init>()V

    invoke-virtual {v2}, Ljava/util/Date;->getMinutes()I

    move-result v2

    add-int v0, v1, v2

    .line 348
    .local v0, "nCurTime":I
    const/16 v1, 0x23a

    if-lt v0, v1, :cond_0

    const/16 v1, 0x726

    if-gt v0, v1, :cond_0

    .line 349
    const/4 v1, 0x0

    .line 351
    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x1

    goto :goto_0
.end method

.method private updateWeather(Landroid/database/Cursor;)V
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v12, 0x7

    .line 188
    if-nez p1, :cond_0

    .line 241
    :goto_0
    return-void

    .line 193
    :cond_0
    :try_start_0
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v10

    if-lez v10, :cond_4

    .line 194
    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    .line 196
    new-instance v2, Landroid/text/format/Time;

    invoke-direct {v2}, Landroid/text/format/Time;-><init>()V

    .line 197
    .local v2, "dateKey":Landroid/text/format/Time;
    const/4 v10, 0x7

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    .line 199
    .local v0, "currentMillis":J
    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 200
    invoke-virtual {v2, v0, v1}, Landroid/text/format/Time;->set(J)V

    .line 201
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    invoke-virtual {v10}, Ljava/util/HashMap;->clear()V

    .line 203
    const/16 v10, 0x9

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    if-nez v10, :cond_1

    const-string v9, "\u2109"

    .line 204
    .local v9, "scale":Ljava/lang/String;
    :goto_1
    const/16 v10, 0x8

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    const-string v11, "999"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 205
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v11, 0xb

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const/16 v11, 0x12

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mTemperature:Ljava/lang/String;

    .line 212
    :goto_2
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_3
    if-ge v6, v12, :cond_3

    .line 214
    invoke-interface {p1, v6}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    .line 216
    .local v7, "iconNumber":I
    const-string v10, "%Y%m%d"

    invoke-virtual {v2, v10}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 218
    .local v3, "dateKeyString":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v11

    invoke-virtual {v10, v3, v11}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 221
    add-int/lit8 v5, v6, 0xb

    .line 222
    .local v5, "highTempIndex":I
    add-int/lit8 v8, v6, 0x12

    .line 224
    .local v8, "lowTempIndex":I
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-interface {p1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "/"

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface {p1, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    .line 227
    .local v4, "highLowTemperature":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mHighLowTemperatureData:Ljava/util/HashMap;

    invoke-virtual {v10, v3, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 229
    iget v10, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v10, v10, 0x1

    iput v10, v2, Landroid/text/format/Time;->monthDay:I

    .line 230
    const/4 v10, 0x1

    invoke-virtual {v2, v10}, Landroid/text/format/Time;->normalize(Z)J

    .line 212
    add-int/lit8 v6, v6, 0x1

    goto :goto_3

    .line 203
    .end local v3    # "dateKeyString":Ljava/lang/String;
    .end local v4    # "highLowTemperature":Ljava/lang/String;
    .end local v5    # "highTempIndex":I
    .end local v6    # "i":I
    .end local v7    # "iconNumber":I
    .end local v8    # "lowTempIndex":I
    .end local v9    # "scale":Ljava/lang/String;
    :cond_1
    const-string v9, "\u2103"

    goto/16 :goto_1

    .line 208
    .restart local v9    # "scale":Ljava/lang/String;
    :cond_2
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const/16 v11, 0x8

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mTemperature:Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_2

    .line 239
    .end local v0    # "currentMillis":J
    .end local v2    # "dateKey":Landroid/text/format/Time;
    .end local v9    # "scale":Ljava/lang/String;
    :catchall_0
    move-exception v10

    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    throw v10

    .line 233
    .restart local v0    # "currentMillis":J
    .restart local v2    # "dateKey":Landroid/text/format/Time;
    .restart local v6    # "i":I
    .restart local v9    # "scale":Ljava/lang/String;
    :cond_3
    const/16 v10, 0xa

    :try_start_1
    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherText:Ljava/lang/String;

    .line 235
    const/16 v10, 0x19

    invoke-interface {p1, v10}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    iput-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mLocationText:Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 239
    .end local v0    # "currentMillis":J
    .end local v2    # "dateKey":Landroid/text/format/Time;
    .end local v6    # "i":I
    .end local v9    # "scale":Ljava/lang/String;
    :cond_4
    invoke-interface {p1}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0
.end method


# virtual methods
.method public getCPName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 129
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getLiveWallPaperWeatherwallCpName()Ljava/lang/String;

    move-result-object v0

    .line 131
    .local v0, "cp":Ljava/lang/String;
    if-eqz v0, :cond_0

    const-string v1, ""

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 132
    :cond_0
    const-string v0, "accuweather"

    .line 135
    :cond_1
    return-object v0
.end method

.method public getHighLowTemperature(Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    .line 322
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mHighLowTemperatureData:Ljava/util/HashMap;

    if-nez v1, :cond_1

    .line 323
    const/4 v0, 0x0

    .line 332
    :cond_0
    :goto_0
    return-object v0

    .line 326
    :cond_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mHighLowTemperatureData:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 328
    .local v0, "temperatureText":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 329
    const-string v0, ""

    goto :goto_0
.end method

.method public getLocationText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 262
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mLocationText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    const-string v0, ""

    .line 265
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mLocationText:Ljava/lang/String;

    goto :goto_0
.end method

.method public getTemperature()Ljava/lang/String;
    .locals 1

    .prologue
    .line 248
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mTemperature:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 249
    const-string v0, ""

    .line 251
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mTemperature:Ljava/lang/String;

    goto :goto_0
.end method

.method public getWeatherImage(Ljava/lang/String;)Landroid/graphics/Bitmap;
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 292
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    if-nez v2, :cond_1

    .line 304
    :cond_0
    :goto_0
    return-object v1

    .line 296
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 297
    .local v0, "index":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 301
    const-string v1, "Cmaweather"

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 302
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, p1, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getCMAWeatherConvertedIndex(Ljava/lang/String;I)I

    move-result v1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    .line 304
    :cond_2
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->createWeatherImage(I)Landroid/graphics/Bitmap;

    move-result-object v1

    goto :goto_0
.end method

.method public getWeatherText()Ljava/lang/String;
    .locals 1

    .prologue
    .line 255
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 256
    const-string v0, ""

    .line 258
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherText:Ljava/lang/String;

    goto :goto_0
.end method

.method public hasWeatherImage(Landroid/text/format/Time;)Z
    .locals 1
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    .line 288
    invoke-static {p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->getKeyFromTime(Landroid/text/format/Time;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->hasWeatherImage(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public hasWeatherImage(Ljava/lang/String;)Z
    .locals 3
    .param p1, "key"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 275
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    if-nez v2, :cond_1

    .line 284
    :cond_0
    :goto_0
    return v1

    .line 279
    :cond_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherData:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 280
    .local v0, "index":Ljava/lang/Integer;
    if-eqz v0, :cond_0

    .line 284
    const/4 v1, 0x1

    goto :goto_0
.end method

.method public startCurrentLocationWeatherDataService()V
    .locals 3

    .prologue
    .line 139
    const-string v1, "WeatherManager"

    const-string v2, "startCurrentLocationWeatherDataService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 141
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 142
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "START"

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 143
    const-string v1, "PACKAGE"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 144
    const-string v1, "CP"

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 146
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 147
    return-void
.end method

.method public stopCurrentLocationWeatherDataService()V
    .locals 3

    .prologue
    .line 150
    const-string v1, "WeatherManager"

    const-string v2, "stopCurrentLocationWeatherDataService"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 151
    const-string v1, "WeatherManager"

    const-string v2, "stop weather data"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 153
    new-instance v0, Landroid/content/Intent;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 154
    .local v0, "intent":Landroid/content/Intent;
    const-string v1, "START"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 155
    const-string v1, "PACKAGE"

    const-string v2, "com.android.calendar"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 156
    const-string v1, "CP"

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mCPName:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 158
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1, v0}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 159
    return-void
.end method

.method public updateWeatherInfo()V
    .locals 8

    .prologue
    const/4 v2, 0x0

    .line 175
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherQueryHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    if-nez v0, :cond_1

    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    if-nez v0, :cond_0

    .line 185
    :goto_0
    return-void

    .line 179
    :cond_0
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;Landroid/content/ContentResolver;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherQueryHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    .line 183
    :cond_1
    const-string v0, "Location=\"%s\""

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v3, 0x0

    const-string v4, "cityId:current"

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 184
    .local v5, "location":Ljava/lang/String;
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherQueryHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;

    const/4 v1, -0x1

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->mWeatherKeys:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;

    iget-object v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->WEATHER_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->WEATHER_PROJECTION:[Ljava/lang/String;

    move-object v6, v2

    move-object v7, v2

    invoke-virtual/range {v0 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager$WeatherQueryHandler;->startQuery(ILjava/lang/Object;Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method
