.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
.super Lcom/samsung/surfacewidget/SurfaceWidgetClient;
.source "SPlannerSurfaceWidget.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;
    }
.end annotation


# static fields
.field private static final ACTION_CHANGE_SHARE:Ljava/lang/String; = "com.sec.android.intent.CHANGE_SHARE"

.field private static final DATE_FORMAT_CHANGED:Ljava/lang/String; = "clock.date_format_changed"

.field private static final HOME_THEME_CHANGED:Ljava/lang/String; = "com.sec.android.app.themechooser.HOME_THEME_CHANGED"

.field private static final SEND_MSG:I = 0x1

.field private static final TAG:Ljava/lang/String;


# instance fields
.field private final mCallback:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

.field private mCheckRefreshCount:I

.field private mEventChangeObserver:Landroid/database/ContentObserver;

.field private final mHandler:Landroid/os/Handler;

.field private mLastSyncFeedbackMillis:J

.field private mRefreshDelay:I

.field private mRefreshFromDBChangeRunnable:Ljava/lang/Runnable;

.field private mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

.field private mServiceBounded:Z

.field private final mServiceConnection:Landroid/content/ServiceConnection;

.field private mServiceHandler:Landroid/os/Handler;

.field private mThemeName:Ljava/lang/String;

.field private mTzObserver:Landroid/database/ContentObserver;

.field private final mWidgetThemeList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 48
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 46
    invoke-direct {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;-><init>()V

    .line 58
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mHandler:Landroid/os/Handler;

    .line 60
    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z

    .line 61
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    .line 74
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$1;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceConnection:Landroid/content/ServiceConnection;

    .line 101
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$2;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$2;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCallback:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    .line 117
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$3;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceHandler:Landroid/os/Handler;

    .line 168
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$4;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mEventChangeObserver:Landroid/database/ContentObserver;

    .line 210
    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I

    .line 211
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J

    .line 212
    const/16 v0, 0x64

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I

    .line 214
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$5;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshFromDBChangeRunnable:Ljava/lang/Runnable;

    .line 243
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$6;

    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$6;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Landroid/os/Handler;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mTzObserver:Landroid/database/ContentObserver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p1, "x1"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    .prologue
    .line 46
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    return-object p1
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 46
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshFromDBChangeRunnable:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCallback:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z

    return p1
.end method

.method static synthetic access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I

    return v0
.end method

.method static synthetic access$702(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I

    return p1
.end method

.method static synthetic access$708(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCheckRefreshCount:I

    return v0
.end method

.method static synthetic access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)J
    .locals 2
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J

    return-wide v0
.end method

.method static synthetic access$802(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;J)J
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p1, "x1"    # J

    .prologue
    .line 46
    iput-wide p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J

    return-wide p1
.end method

.method static synthetic access$900(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    .line 46
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I

    return v0
.end method

.method static synthetic access$902(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    .param p1, "x1"    # I

    .prologue
    .line 46
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mRefreshDelay:I

    return p1
.end method

.method private destroyWidgetTheme(I)V
    .locals 5
    .param p1, "instance"    # I

    .prologue
    .line 402
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    if-eqz v3, :cond_0

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 403
    :cond_0
    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    const-string v4, "widgetThemeList is null or size is 0!"

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 419
    :goto_0
    return-void

    .line 407
    :cond_1
    const/4 v1, 0x0

    .line 408
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-ge v0, v3, :cond_2

    .line 409
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v3, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget v3, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mInstanceId:I

    if-ne p1, v3, :cond_4

    .line 410
    move v1, v0

    .line 414
    :cond_2
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    .line 415
    .local v2, "widgetInfo":Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;
    if-eqz v2, :cond_3

    .line 416
    iget-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    invoke-interface {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onThemeDestroy()V

    .line 418
    :cond_3
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v3, v1}, Ljava/util/ArrayList;->remove(I)Ljava/lang/Object;

    goto :goto_0

    .line 408
    .end local v2    # "widgetInfo":Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;
    :cond_4
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
    .locals 4
    .param p1, "instance"    # I

    .prologue
    .line 387
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-nez v2, :cond_1

    .line 388
    :cond_0
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    const-string v3, "widgetthemeList is null or size is 0!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 389
    const/4 v2, 0x0

    .line 399
    :goto_0
    return-object v2

    .line 392
    :cond_1
    const/4 v1, 0x0

    .line 393
    .local v1, "index":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_2

    .line 394
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mInstanceId:I

    if-ne p1, v2, :cond_3

    .line 395
    move v1, v0

    .line 399
    :cond_2
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    iget-object v2, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;->mTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    goto :goto_0

    .line 393
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_1
.end method

.method private setWidgetTheme(I)I
    .locals 5
    .param p1, "instanceId"    # I

    .prologue
    .line 301
    const-string v2, "Today"

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mThemeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    const-string v2, "Today_mini"

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mThemeName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 302
    :cond_0
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 307
    .local v1, "theme":Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
    :goto_0
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;

    invoke-direct {v0, v1, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;I)V

    .line 308
    .local v0, "mWidgetTheme":Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mWidgetThemeList:Ljava/util/ArrayList;

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 309
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "[SPlannerSurface] setWidgetTheme.instance : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ", mThemeName: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mThemeName:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 311
    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onContentRequest()I

    move-result v2

    return v2

    .line 304
    .end local v0    # "mWidgetTheme":Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget$SurfaceWidgetInfo;
    .end local v1    # "theme":Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
    :cond_1
    new-instance v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    invoke-direct {v1, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .restart local v1    # "theme":Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;
    goto :goto_0
.end method


# virtual methods
.method public createRenderer(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)Lcom/samsung/surfacewidget/SurfaceWidgetRenderer;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "resources"    # Landroid/content/res/Resources;
    .param p3, "i"    # I
    .param p4, "handler"    # Landroid/os/Handler;

    .prologue
    .line 295
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidgetRenderer;

    invoke-direct {v0, p1, p2, p3, p4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidgetRenderer;-><init>(Landroid/content/Context;Landroid/content/res/Resources;ILandroid/os/Handler;)V

    return-object v0
.end method

.method public createUIHandler()Landroid/os/Handler;
    .locals 1

    .prologue
    .line 320
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->createUIHandler()Landroid/os/Handler;

    move-result-object v0

    return-object v0
.end method

.method public needImmediateRefreshInObserver()V
    .locals 2

    .prologue
    .line 238
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mLastSyncFeedbackMillis:J

    .line 239
    return-void
.end method

.method public onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 2
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 341
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 342
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    const-string v1, "[SPlannerSurface] onConfigurationChanged"

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    return-void
.end method

.method public onContentRequest(I)V
    .locals 7
    .param p1, "instance"    # I

    .prologue
    const/4 v6, 0x1

    .line 264
    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "[SPlannerSurface] onContentRequest.instance: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 265
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    new-instance v4, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v3, v4, v5, v6}, Landroid/content/Context;->bindService(Landroid/content/Intent;Landroid/content/ServiceConnection;I)Z

    .line 268
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 269
    .local v1, "cr":Landroid/content/ContentResolver;
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mEventChangeObserver:Landroid/database/ContentObserver;

    if-eqz v3, :cond_0

    .line 270
    sget-object v3, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mEventChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 274
    :cond_0
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mTzObserver:Landroid/database/ContentObserver;

    if-eqz v3, :cond_1

    .line 275
    sget-object v3, Landroid/provider/CalendarContract$CalendarCache;->URI:Landroid/net/Uri;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mTzObserver:Landroid/database/ContentObserver;

    invoke-virtual {v1, v3, v6, v4}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 277
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchEntirePreference(Landroid/content/Context;)V

    .line 279
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getProviderInfo(I)Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;

    move-result-object v2

    .line 280
    .local v2, "info":Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;
    invoke-virtual {v2}, Lcom/samsung/surfacewidget/SurfaceWidgetProviderInfo;->getThemeName()Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mThemeName:Ljava/lang/String;

    .line 281
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->setWidgetTheme(I)I

    move-result v0

    .line 283
    .local v0, "content":I
    invoke-super {p0, p1, v0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->setContentView(II)V

    .line 284
    return-void
.end method

.method public onDestroy()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 348
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    const-string v3, "[SPlannerSurface] all calendar widget instances are removed from idle "

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    .line 350
    .local v0, "cr":Landroid/content/ContentResolver;
    if-eqz v0, :cond_0

    .line 351
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mEventChangeObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 352
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mTzObserver:Landroid/database/ContentObserver;

    invoke-virtual {v0, v2}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 354
    :cond_0
    iget-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    if-eqz v2, :cond_2

    .line 356
    :try_start_0
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mService:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mCallback:Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;

    invoke-interface {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerService;->unregisterCallback(Lcom/sec/android/widgetapp/SPlannerAppWidget/ISPlannerServiceCallback;)V

    .line 357
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceHandler:Landroid/os/Handler;

    if-eqz v2, :cond_1

    .line 358
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceHandler:Landroid/os/Handler;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 359
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceHandler:Landroid/os/Handler;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    .line 364
    :cond_1
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceConnection:Landroid/content/ServiceConnection;

    invoke-virtual {v2, v3}, Landroid/content/Context;->unbindService(Landroid/content/ServiceConnection;)V

    .line 365
    const/4 v2, 0x0

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mServiceBounded:Z

    .line 367
    :cond_2
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mEventChangeObserver:Landroid/database/ContentObserver;

    .line 368
    iput-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->mTzObserver:Landroid/database/ContentObserver;

    .line 370
    invoke-super {p0}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy()V

    .line 371
    return-void

    .line 361
    :catch_0
    move-exception v1

    .line 362
    .local v1, "e":Landroid/os/RemoteException;
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    invoke-virtual {v1}, Landroid/os/RemoteException;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method public onDestroy(IZ)V
    .locals 3
    .param p1, "instance"    # I
    .param p2, "isRemovedFromIdle"    # Z

    .prologue
    .line 375
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->TAG:Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "[SPlannerSurface] onDestroy.instance: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", isRemovedFromIdle : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 377
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 378
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->destroyWidgetTheme(I)V

    .line 381
    :cond_0
    if-eqz p2, :cond_1

    .line 382
    invoke-super {p0, p1, p2}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onDestroy(IZ)V

    .line 384
    :cond_1
    return-void
.end method

.method protected onFinishInflate(ILandroid/view/View;)V
    .locals 1
    .param p1, "instanceId"    # I
    .param p2, "topView"    # Landroid/view/View;

    .prologue
    .line 288
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 289
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onFinishInflate(ILandroid/view/View;)V

    .line 291
    :cond_0
    return-void
.end method

.method protected onPause(I)V
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 333
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onPause(I)V

    .line 334
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 335
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onPause(I)V

    .line 337
    :cond_0
    return-void
.end method

.method protected onResume(I)V
    .locals 1
    .param p1, "instance"    # I

    .prologue
    .line 325
    invoke-super {p0, p1}, Lcom/samsung/surfacewidget/SurfaceWidgetClient;->onResume(I)V

    .line 326
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 327
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getWidgetTheme(I)Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;

    move-result-object v0

    invoke-interface {v0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/WidgetTheme;->onResume(I)V

    .line 329
    :cond_0
    return-void
.end method

.method public relayout(I)V
    .locals 0
    .param p1, "instance"    # I

    .prologue
    .line 315
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->handleLayoutViewsMessage(I)V

    .line 316
    return-void
.end method
