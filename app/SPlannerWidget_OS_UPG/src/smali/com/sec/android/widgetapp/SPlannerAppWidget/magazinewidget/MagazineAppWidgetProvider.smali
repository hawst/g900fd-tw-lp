.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;
.super Landroid/appwidget/AppWidgetProvider;
.source "MagazineAppWidgetProvider.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;
    }
.end annotation


# static fields
.field private static final ACTION_APPWIDGET_UPDATE:Ljava/lang/String; = "android.appwidget.action.APPWIDGET_UPDATE"

.field private static final ACTION_DATE_CHANGED:Ljava/lang/String; = "android.intent.action.DATE_CHANGED"

.field private static final ACTION_DATE_FORMAT_CHNAGED:Ljava/lang/String; = "clock.date_format_changed"

.field public static final ACTION_SEC_CALENDAR_ITEM_UPDATE:Ljava/lang/String; = "CalendarItemUpdate"

.field private static final ACTION_SEC_CHANGE_SHARE:Ljava/lang/String; = "com.sec.android.intent.CHANGE_SHARE"

.field private static final ACTION_SEC_EDITMODE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_EDITMODE"

.field public static final ACTION_SEC_LIST_ITEM_CLICK:Ljava/lang/String; = "ListItemClick"

.field private static final ACTION_SEC_NEW_EVENT:Ljava/lang/String; = "ActionNewEvent"

.field public static final ACTION_SEC_NEXT_MONTH_UPDATE:Ljava/lang/String; = "UpdateNextMonth"

.field public static final ACTION_SEC_PREV_MONTH_UPDATE:Ljava/lang/String; = "UpdatePreviousMonth"

.field private static final ACTION_SEC_TODAY_UPDATE:Ljava/lang/String; = "UpdateTodayMonth"

.field private static final ACTION_SEC_TOGGLE_UPDATE:Ljava/lang/String; = "TOGGLE_BUTTON_UPDATE"

.field private static final ACTION_SEC_WIDGET_RESIZE:Ljava/lang/String; = "com.sec.android.widgetapp.APPWIDGET_RESIZE"

.field private static final ACTION_TIMEZONE_CHANGED:Ljava/lang/String; = "android.intent.action.TIMEZONE_CHANGED"

.field private static final ACTION_TIME_SET_CHANGE:Ljava/lang/String; = "android.intent.action.TIME_SET"

.field private static final ALLDAY_WHERE:Ljava/lang/String; = "dispAllday=1"

.field private static final DISPLAY_AS_ALLDAY:Ljava/lang/String; = "dispAllday"

.field private static final EVENTS_WHERE:Ljava/lang/String; = "dispAllday=0"

.field private static final SORT_ALLDAY_BY:Ljava/lang/String; = "startDay ASC, endDay DESC, title ASC"

.field private static final SORT_EVENTS_BY:Ljava/lang/String; = "startDay,startMinute,title"

.field private static final TAG:Ljava/lang/String; = "MagazineAppWidgetProvider"

.field private static app_widget_enabled:Z

.field private static dayLimitForFebruary:I

.field public static fetchFromPref:Z

.field public static isDayClicked:Z

.field public static item_checked:Z

.field private static mContext:Landroid/content/Context;

.field public static mMonth:Ljava/util/Calendar;

.field private static mNoEventsTextMessage:Ljava/lang/CharSequence;

.field public static mRemoteViews:Landroid/widget/RemoteViews;

.field static mSelectedDate:Ljava/util/Calendar;

.field static prefsName:Ljava/lang/String;

.field public static resetAll:Z

.field public static tempTargetWidgetId:I

.field public static widgetResize:Z


# instance fields
.field public current_month:Z

.field public day_selected:Z

.field private df:Ljava/text/SimpleDateFormat;

.field private mContentResolver:Landroid/content/ContentResolver;

.field private mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

.field private mIsDayEventsListVisible:Z

.field private mLayout:I

.field private mTime:Landroid/text/format/Time;

.field public next_day:Z

.field public next_month:Z

.field public prev_day:Z

.field public prev_month:Z

.field private selected_day:I

.field private selected_month:I

.field private selected_year:I

.field public today_selected:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 74
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->item_checked:Z

    .line 75
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->fetchFromPref:Z

    .line 80
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    .line 81
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    .line 83
    const-string v0, "pref"

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    .line 95
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    .line 96
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 97
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->resetAll:Z

    .line 100
    sput-boolean v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    .line 101
    const/16 v0, 0x1c

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->dayLimitForFebruary:I

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 112
    invoke-direct {p0}, Landroid/appwidget/AppWidgetProvider;-><init>()V

    .line 67
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->next_month:Z

    .line 68
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prev_month:Z

    .line 69
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->next_day:Z

    .line 70
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prev_day:Z

    .line 71
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->current_month:Z

    .line 72
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->day_selected:Z

    .line 73
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->today_selected:Z

    .line 98
    iput-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mIsDayEventsListVisible:Z

    .line 114
    return-void
.end method

.method static synthetic access$000()Landroid/content/Context;
    .locals 1

    .prologue
    .line 48
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    return-object v0
.end method

.method private formatMonthYear(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 20
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetId"    # I
    .param p3, "action"    # Ljava/lang/String;

    .prologue
    .line 697
    const/4 v6, 0x0

    .line 698
    .local v6, "month":I
    const/4 v13, 0x0

    .line 699
    .local v13, "year":I
    if-nez p1, :cond_0

    .line 700
    const-string v7, ""

    .line 755
    :goto_0
    return-object v7

    .line 702
    :cond_0
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v15

    move-object/from16 v0, p3

    invoke-virtual {v15, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 703
    .local v1, "ACTION_DATE_AUTO_UPDATE":Ljava/lang/Boolean;
    sget-object v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 704
    .local v10, "pref":Landroid/content/SharedPreferences;
    if-nez v10, :cond_1

    .line 705
    const-string v7, ""

    goto :goto_0

    .line 707
    :cond_1
    const-string v15, "day_selected%d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 708
    .local v3, "daySelectedKey":Ljava/lang/String;
    const-string v15, "SelectedMonth%d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v11

    .line 709
    .local v11, "selectedMonthKey":Ljava/lang/String;
    const-string v15, "SelectedYear%d"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    invoke-static/range {p2 .. p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 710
    .local v12, "selectedYearKey":Ljava/lang/String;
    const/4 v15, 0x0

    invoke-interface {v10, v3, v15}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v15

    invoke-static {v15}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    .line 712
    .local v4, "isDaySelected":Ljava/lang/Boolean;
    if-lez p2, :cond_6

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-nez v15, :cond_6

    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_6

    sget-boolean v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->resetAll:Z

    if-nez v15, :cond_6

    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_6

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-eqz v15, :cond_6

    .line 714
    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_2

    .line 715
    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 717
    :cond_2
    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_3

    .line 718
    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    .line 734
    :cond_3
    :goto_1
    const/16 v15, 0x14

    invoke-static {v6, v15}, Landroid/text/format/DateUtils;->getMonthString(II)Ljava/lang/String;

    move-result-object v8

    .line 735
    .local v8, "month_string":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v15

    const-string v16, "date_format"

    invoke-static/range {v15 .. v16}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 736
    .local v2, "dateFormat":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v15

    if-nez v15, :cond_4

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isKorean()Z

    move-result v15

    if-eqz v15, :cond_a

    .line 737
    :cond_4
    new-instance v9, Landroid/text/format/Time;

    invoke-direct {v9}, Landroid/text/format/Time;-><init>()V

    .line 738
    .local v9, "myTime":Landroid/text/format/Time;
    const/4 v14, 0x0

    .line 739
    .local v14, "year_string":Ljava/lang/String;
    const v15, 0x7f0a006d

    move-object/from16 v0, p1

    invoke-virtual {v0, v15}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 740
    .local v5, "mYearFormat":Ljava/lang/String;
    const/4 v15, 0x1

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v9, v15, v0, v13}, Landroid/text/format/Time;->set(III)V

    .line 741
    invoke-virtual {v9, v5}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v14

    .line 742
    const-string v15, "MM-dd-yyyy"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_5

    const-string v15, "dd-MM-yyyy"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_9

    .line 743
    :cond_5
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .local v7, "monthTitle":Ljava/lang/String;
    goto/16 :goto_0

    .line 720
    .end local v2    # "dateFormat":Ljava/lang/String;
    .end local v5    # "mYearFormat":Ljava/lang/String;
    .end local v7    # "monthTitle":Ljava/lang/String;
    .end local v8    # "month_string":Ljava/lang/String;
    .end local v9    # "myTime":Landroid/text/format/Time;
    .end local v14    # "year_string":Ljava/lang/String;
    :cond_6
    if-lez p2, :cond_7

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v15

    if-nez v15, :cond_7

    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_7

    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    if-eqz v15, :cond_7

    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_7

    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v15

    if-eqz v15, :cond_7

    .line 722
    const/4 v15, 0x0

    invoke-interface {v10, v11, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    .line 723
    const/4 v15, 0x0

    invoke-interface {v10, v12, v15}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    invoke-static {v15}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v13

    goto/16 :goto_1

    .line 725
    :cond_7
    const-string v15, "MagazineAppWidgetProvider"

    new-instance v16, Ljava/lang/StringBuilder;

    invoke-direct/range {v16 .. v16}, Ljava/lang/StringBuilder;-><init>()V

    const-string v17, "formatMonthYear: selected_day - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, " selected_month - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    const-string v17, "selected_year - "

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v16

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move/from16 v17, v0

    invoke-virtual/range {v16 .. v17}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v16

    invoke-virtual/range {v16 .. v16}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v16

    invoke-static/range {v15 .. v16}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 726
    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    if-nez v15, :cond_8

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    if-nez v15, :cond_8

    .line 727
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setToday()V

    .line 729
    :cond_8
    sget-object v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/16 v16, 0x2

    invoke-virtual/range {v15 .. v16}, Ljava/util/Calendar;->get(I)I

    move-result v6

    .line 730
    sget-object v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/16 v16, 0x1

    invoke-virtual/range {v15 .. v16}, Ljava/util/Calendar;->get(I)I

    move-result v13

    goto/16 :goto_1

    .line 745
    .restart local v2    # "dateFormat":Ljava/lang/String;
    .restart local v5    # "mYearFormat":Ljava/lang/String;
    .restart local v8    # "month_string":Ljava/lang/String;
    .restart local v9    # "myTime":Landroid/text/format/Time;
    .restart local v14    # "year_string":Ljava/lang/String;
    :cond_9
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v14}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "monthTitle":Ljava/lang/String;
    goto/16 :goto_0

    .line 748
    .end local v5    # "mYearFormat":Ljava/lang/String;
    .end local v7    # "monthTitle":Ljava/lang/String;
    .end local v9    # "myTime":Landroid/text/format/Time;
    .end local v14    # "year_string":Ljava/lang/String;
    :cond_a
    const-string v15, "MM-dd-yyyy"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-nez v15, :cond_b

    const-string v15, "dd-MM-yyyy"

    invoke-virtual {v2, v15}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v15

    if-eqz v15, :cond_c

    .line 749
    :cond_b
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, "%d"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "monthTitle":Ljava/lang/String;
    goto/16 :goto_0

    .line 751
    .end local v7    # "monthTitle":Ljava/lang/String;
    :cond_c
    new-instance v15, Ljava/lang/StringBuilder;

    invoke-direct {v15}, Ljava/lang/StringBuilder;-><init>()V

    const-string v16, "%d"

    const/16 v17, 0x1

    move/from16 v0, v17

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    aput-object v19, v17, v18

    invoke-static/range {v16 .. v17}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v16

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    const-string v16, " "

    invoke-virtual/range {v15 .. v16}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v15

    invoke-virtual {v15}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    .restart local v7    # "monthTitle":Ljava/lang/String;
    goto/16 :goto_0
.end method

.method static getCalendarItemPendingIntentTemplate(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 1326
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1328
    .local v0, "launchIntent":Landroid/content/Intent;
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1329
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method public static getComponentName(Landroid/content/Context;)Landroid/content/ComponentName;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 1313
    new-instance v0, Landroid/content/ComponentName;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/ComponentName;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    return-object v0
.end method

.method private getDayEventCount(Landroid/content/Context;Landroid/text/format/Time;I)Ljava/lang/String;
    .locals 33
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # Landroid/text/format/Time;
    .param p3, "appWidgetId"    # I

    .prologue
    .line 1133
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 1134
    .local v7, "cr":Landroid/content/ContentResolver;
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    move/from16 v2, p3

    invoke-direct {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getSelectedMills(Landroid/text/format/Time;I)J

    move-result-wide v22

    .line 1135
    .local v22, "currentMillis":J
    const-wide/32 v10, 0x5265c00

    add-long v24, v22, v10

    .line 1136
    .local v24, "endMillis":J
    const/16 v26, 0x0

    .line 1137
    .local v26, "eventCursor":Landroid/database/Cursor;
    const/16 v20, 0x0

    .line 1138
    .local v20, "cAllday":Landroid/database/Cursor;
    const/16 v30, 0x0

    .line 1139
    .local v30, "numEvents":Ljava/lang/String;
    sget-object v8, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    invoke-virtual {v8}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v6

    .line 1140
    .local v6, "builder":Landroid/net/Uri$Builder;
    move-object/from16 v0, p2

    iget-wide v10, v0, Landroid/text/format/Time;->gmtoff:J

    move-wide/from16 v0, v22

    invoke-static {v0, v1, v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v29

    .line 1141
    .local v29, "julianDay":I
    move/from16 v0, v29

    int-to-long v10, v0

    invoke-static {v6, v10, v11}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 1142
    move/from16 v0, v29

    int-to-long v10, v0

    invoke-static {v6, v10, v11}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 1146
    :try_start_0
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->hideDeclinedEvent()Z

    move-result v27

    .line 1147
    .local v27, "hideDeclined":Z
    const-string v9, "dispAllday=0"

    .line 1148
    .local v9, "where":Ljava/lang/String;
    const-string v13, "dispAllday=1"

    .line 1149
    .local v13, "whereAllday":Ljava/lang/String;
    if-eqz v27, :cond_0

    .line 1150
    const-string v28, " AND selfAttendeeStatus!=2"

    .line 1152
    .local v28, "hideString":Ljava/lang/String;
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    .line 1153
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v8, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v28

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 1155
    .end local v28    # "hideString":Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getHideContactEventSelection(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 1156
    move-object/from16 v0, p1

    invoke-static {v0, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getHideContactEventSelection(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 1157
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v10, 0x0

    const-string v11, "startDay,startMinute,title"

    invoke-static/range {v6 .. v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->instancesQuery(Landroid/net/Uri$Builder;Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v26

    .line 1158
    sget-object v12, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "startDay ASC, endDay DESC, title ASC"

    move-object v10, v6

    move-object v11, v7

    invoke-static/range {v10 .. v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->instancesQuery(Landroid/net/Uri$Builder;Landroid/content/ContentResolver;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v20

    .line 1159
    if-eqz v26, :cond_6

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_6

    if-eqz v20, :cond_6

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_6

    .line 1160
    const-string v8, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->getCount()I

    move-result v12

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->getCount()I

    move-result v14

    add-int/2addr v12, v14

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v30

    .line 1178
    :cond_1
    :goto_0
    if-eqz v26, :cond_2

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_2

    .line 1179
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 1181
    :cond_2
    if-eqz v20, :cond_3

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1182
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    .line 1186
    .end local v9    # "where":Ljava/lang/String;
    .end local v13    # "whereAllday":Ljava/lang/String;
    .end local v27    # "hideDeclined":Z
    :cond_3
    :goto_1
    const/16 v32, 0x0

    .line 1187
    .local v32, "taskCursor":Landroid/database/Cursor;
    const/16 v31, 0x0

    .line 1189
    .local v31, "numTasks":Ljava/lang/String;
    move-object/from16 v0, p1

    move-wide/from16 v1, v22

    move-wide/from16 v3, v24

    invoke-static {v0, v1, v2, v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTaskSelection(Landroid/content/Context;JJ)Ljava/lang/String;

    move-result-object v17

    .line 1191
    .local v17, "selection":Ljava/lang/String;
    :try_start_1
    sget-object v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    const/16 v16, 0x0

    const/16 v18, 0x0

    const/16 v19, 0x0

    move-object v14, v7

    invoke-virtual/range {v14 .. v19}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v32

    .line 1192
    if-eqz v32, :cond_a

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_a

    if-eqz v17, :cond_a

    .line 1193
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->moveToFirst()Z

    .line 1194
    const-string v8, "%d"

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->getCount()I

    move-result v12

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v10, v11

    invoke-static {v8, v10}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_1
    .catch Ljava/lang/Throwable; {:try_start_1 .. :try_end_1} :catch_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    move-result-object v31

    .line 1209
    :cond_4
    :goto_2
    if-eqz v32, :cond_5

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1210
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    .line 1213
    :cond_5
    :goto_3
    if-eqz v30, :cond_c

    if-eqz v31, :cond_c

    .line 1214
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v30

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v10, ";"

    invoke-virtual {v8, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v31

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 1216
    :goto_4
    return-object v8

    .line 1165
    .end local v17    # "selection":Ljava/lang/String;
    .end local v31    # "numTasks":Ljava/lang/String;
    .end local v32    # "taskCursor":Landroid/database/Cursor;
    .restart local v9    # "where":Ljava/lang/String;
    .restart local v13    # "whereAllday":Ljava/lang/String;
    .restart local v27    # "hideDeclined":Z
    :cond_6
    if-eqz v26, :cond_1

    .line 1166
    :try_start_2
    const-string v8, "MagazineAppWidgetProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "eventCursor is wrong. isClose ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Throwable; {:try_start_2 .. :try_end_2} :catch_0
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto/16 :goto_0

    .line 1169
    .end local v9    # "where":Ljava/lang/String;
    .end local v13    # "whereAllday":Ljava/lang/String;
    .end local v27    # "hideDeclined":Z
    :catch_0
    move-exception v21

    .line 1170
    .local v21, "e":Ljava/lang/Throwable;
    :try_start_3
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    .line 1178
    if-eqz v26, :cond_7

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_7

    .line 1179
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 1181
    :cond_7
    if-eqz v20, :cond_3

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_3

    .line 1182
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    goto/16 :goto_1

    .line 1178
    .end local v21    # "e":Ljava/lang/Throwable;
    :catchall_0
    move-exception v8

    if-eqz v26, :cond_8

    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_8

    .line 1179
    invoke-interface/range {v26 .. v26}, Landroid/database/Cursor;->close()V

    .line 1181
    :cond_8
    if-eqz v20, :cond_9

    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_9

    .line 1182
    invoke-interface/range {v20 .. v20}, Landroid/database/Cursor;->close()V

    :cond_9
    throw v8

    .line 1199
    .restart local v17    # "selection":Ljava/lang/String;
    .restart local v31    # "numTasks":Ljava/lang/String;
    .restart local v32    # "taskCursor":Landroid/database/Cursor;
    :cond_a
    if-eqz v32, :cond_4

    .line 1200
    :try_start_4
    const-string v8, "MagazineAppWidgetProvider"

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "taskCursor is wrong. isClose ="

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->isClosed()Z

    move-result v11

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v8, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_4
    .catch Ljava/lang/Throwable; {:try_start_4 .. :try_end_4} :catch_1
    .catchall {:try_start_4 .. :try_end_4} :catchall_1

    goto/16 :goto_2

    .line 1203
    :catch_1
    move-exception v21

    .line 1204
    .restart local v21    # "e":Ljava/lang/Throwable;
    :try_start_5
    invoke-virtual/range {v21 .. v21}, Ljava/lang/Throwable;->printStackTrace()V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_1

    .line 1209
    if-eqz v32, :cond_5

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->isClosed()Z

    move-result v8

    if-nez v8, :cond_5

    .line 1210
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    goto/16 :goto_3

    .line 1209
    .end local v21    # "e":Ljava/lang/Throwable;
    :catchall_1
    move-exception v8

    if-eqz v32, :cond_b

    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->isClosed()Z

    move-result v10

    if-nez v10, :cond_b

    .line 1210
    invoke-interface/range {v32 .. v32}, Landroid/database/Cursor;->close()V

    :cond_b
    throw v8

    .line 1216
    :cond_c
    const-string v8, "0;0"

    goto/16 :goto_4
.end method

.method private getDayTitle(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;
    .locals 24
    .param p1, "dateFormat"    # Ljava/lang/String;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "appWidgetId"    # I

    .prologue
    .line 922
    const-string v5, ""

    .line 923
    .local v5, "dayTitle":Ljava/lang/String;
    new-instance v11, Ljava/text/SimpleDateFormat;

    const-string v19, "dd"

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 924
    .local v11, "sdf":Ljava/text/SimpleDateFormat;
    sget-object v19, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, v19

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v10

    .line 925
    .local v10, "pref":Landroid/content/SharedPreferences;
    const-string v19, "SelectedDay%d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v12

    .line 926
    .local v12, "selectedDayKey":Ljava/lang/String;
    const-string v19, "SelectedMonth%d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v13

    .line 927
    .local v13, "selectedMonthKey":Ljava/lang/String;
    const-string v19, "SelectedYear%d"

    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v22

    aput-object v22, v20, v21

    invoke-static/range {v19 .. v20}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v14

    .line 928
    .local v14, "selectedYearKey":Ljava/lang/String;
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v12, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_0

    .line 929
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v12, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 931
    :cond_0
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v13, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_1

    .line 932
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v13, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 934
    :cond_1
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v14, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    if-eqz v19, :cond_2

    .line 935
    const/16 v19, 0x0

    move-object/from16 v0, v19

    invoke-interface {v10, v14, v0}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v19

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 937
    :cond_2
    sget-object v19, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual/range {v19 .. v19}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Ljava/util/Calendar;

    .line 938
    .local v15, "tempMonth":Ljava/util/Calendar;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    move/from16 v21, v0

    move/from16 v0, v19

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v15, v0, v1, v2}, Ljava/util/Calendar;->set(III)V

    .line 939
    invoke-virtual {v15}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v11, v0}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v3

    .line 940
    .local v3, "day":Ljava/lang/String;
    invoke-virtual/range {p2 .. p2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v19

    const v20, 0x7f0a0022

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 941
    .local v4, "dayPostFix":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isGerman()Z

    move-result v19

    if-eqz v19, :cond_3

    .line 942
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 944
    :cond_3
    const/16 v19, 0x2

    const/16 v20, 0x1

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v21

    move/from16 v0, v19

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v15, v0, v1, v2}, Ljava/util/Calendar;->getDisplayName(IILjava/util/Locale;)Ljava/lang/String;

    move-result-object v8

    .line 945
    .local v8, "month":Ljava/lang/String;
    const/16 v19, 0x1

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v17

    .line 946
    .local v17, "year":I
    const/16 v19, 0x5

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Ljava/util/Calendar;->get(I)I

    move-result v9

    .line 947
    .local v9, "monthDay":I
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v19

    if-nez v19, :cond_4

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isKorean()Z

    move-result v19

    if-eqz v19, :cond_7

    .line 948
    :cond_4
    const v19, 0x7f0a006d

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 949
    .local v7, "mYearFormat":Ljava/lang/String;
    const v19, 0x7f0a0021

    move-object/from16 v0, p2

    move/from16 v1, v19

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 950
    .local v6, "mDayFormat":Ljava/lang/String;
    const/16 v18, 0x0

    .line 951
    .local v18, "year_string":Ljava/lang/String;
    new-instance v16, Landroid/text/format/Time;

    invoke-direct/range {v16 .. v16}, Landroid/text/format/Time;-><init>()V

    .line 952
    .local v16, "time":Landroid/text/format/Time;
    const/16 v19, 0x1

    move-object/from16 v0, v16

    move/from16 v1, v19

    move/from16 v2, v17

    invoke-virtual {v0, v9, v1, v2}, Landroid/text/format/Time;->set(III)V

    .line 953
    move-object/from16 v0, v16

    invoke-virtual {v0, v7}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 954
    move-object/from16 v0, v16

    invoke-virtual {v0, v6}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 955
    const-string v19, "MM-dd-yyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_5

    .line 956
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 971
    .end local v6    # "mDayFormat":Ljava/lang/String;
    .end local v7    # "mYearFormat":Ljava/lang/String;
    .end local v16    # "time":Landroid/text/format/Time;
    .end local v18    # "year_string":Ljava/lang/String;
    :goto_0
    new-instance v11, Ljava/text/SimpleDateFormat;

    .end local v11    # "sdf":Ljava/text/SimpleDateFormat;
    const-string v19, "dd-MM-yyyy"

    move-object/from16 v0, v19

    invoke-direct {v11, v0}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;)V

    .line 972
    .restart local v11    # "sdf":Ljava/text/SimpleDateFormat;
    return-object v5

    .line 957
    .restart local v6    # "mDayFormat":Ljava/lang/String;
    .restart local v7    # "mYearFormat":Ljava/lang/String;
    .restart local v16    # "time":Landroid/text/format/Time;
    .restart local v18    # "year_string":Ljava/lang/String;
    :cond_5
    const-string v19, "dd-MM-yyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_6

    .line 958
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 960
    :cond_6
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto :goto_0

    .line 963
    .end local v6    # "mDayFormat":Ljava/lang/String;
    .end local v7    # "mYearFormat":Ljava/lang/String;
    .end local v16    # "time":Landroid/text/format/Time;
    .end local v18    # "year_string":Ljava/lang/String;
    :cond_7
    const-string v19, "MM-dd-yyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_8

    .line 964
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 965
    :cond_8
    const-string v19, "dd-MM-yyyy"

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_9

    .line 966
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, "%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0

    .line 968
    :cond_9
    new-instance v19, Ljava/lang/StringBuilder;

    invoke-direct/range {v19 .. v19}, Ljava/lang/StringBuilder;-><init>()V

    const-string v20, "%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v20

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    const-string v20, " "

    invoke-virtual/range {v19 .. v20}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    move-object/from16 v0, v19

    invoke-virtual {v0, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    goto/16 :goto_0
.end method

.method private getEventItemFillInIntent(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 514
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 515
    .local v1, "fillInIntent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 516
    const/high16 v2, 0x14200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 519
    const-string v2, "com.android.calendar"

    const-string v3, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 521
    sget-object v2, Landroid/provider/CalendarContract$Events;->CONTENT_URI:Landroid/net/Uri;

    const-string v3, "id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 522
    .local v0, "eventUri":Landroid/net/Uri;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 523
    const-string v2, "beginTime"

    const-string v3, "start"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 524
    const-string v2, "endTime"

    const-string v3, "end"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 525
    const-string v2, "VIEW"

    const-string v3, "MONTH"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 526
    return-object v1
.end method

.method static getLaunchEventFillInIntent(Landroid/content/Context;JJJ)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "start"    # J
    .param p5, "end"    # J

    .prologue
    .line 1285
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1286
    .local v0, "fillInIntent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1287
    const-string v1, "start"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1288
    const-string v1, "end"

    invoke-virtual {v0, v1, p5, p6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1290
    return-object v0
.end method

.method static getLaunchPendingIntentTemplate(Landroid/content/Context;I)Landroid/app/PendingIntent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "appWidgetId"    # I

    .prologue
    .line 1318
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1319
    .local v0, "launchIntent":Landroid/content/Intent;
    const-string v1, "ListItemClick"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1320
    const-string v1, "appWidgetId"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1321
    const/high16 v1, 0x8000000

    invoke-static {p0, p1, v0, v1}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method static getLaunchTaskFillInIntent(Landroid/content/Context;JJ)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "id"    # J
    .param p3, "start"    # J

    .prologue
    .line 1296
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    .line 1297
    .local v0, "fillInIntent":Landroid/content/Intent;
    const-string v1, "id"

    invoke-virtual {v0, v1, p1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1298
    const-string v1, "start"

    invoke-virtual {v0, v1, p3, p4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 1300
    return-object v0
.end method

.method private getMaxP()I
    .locals 6

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x2

    .line 619
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/GregorianCalendar;

    .line 620
    .local v0, "mPreMonth":Ljava/util/GregorianCalendar;
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v3

    if-ne v2, v3, :cond_0

    .line 622
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    sget-object v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v3, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v3

    invoke-virtual {v0, v2, v3, v5}, Ljava/util/GregorianCalendar;->set(III)V

    .line 628
    :goto_0
    const/4 v2, 0x5

    invoke-virtual {v0, v2}, Ljava/util/GregorianCalendar;->getActualMaximum(I)I

    move-result v1

    .line 630
    .local v1, "maxP":I
    return v1

    .line 625
    .end local v1    # "maxP":I
    :cond_0
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v4}, Ljava/util/Calendar;->get(I)I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v0, v4, v2}, Ljava/util/GregorianCalendar;->set(II)V

    goto :goto_0
.end method

.method private getMessage()Ljava/lang/CharSequence;
    .locals 2

    .prologue
    .line 1125
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isUsaOrCanada()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1126
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a005b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 1128
    :goto_0
    return-object v0

    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a005c

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private getSelectedMills(Landroid/text/format/Time;I)J
    .locals 12
    .param p1, "time"    # Landroid/text/format/Time;
    .param p2, "appWidgetId"    # I

    .prologue
    const/4 v11, 0x1

    const/4 v10, 0x0

    const/4 v9, 0x0

    .line 1222
    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    sget-object v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 1223
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v6, "SelectedDay%d"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1224
    .local v0, "daySelectedKey":Ljava/lang/String;
    const-string v6, "SelectedMonth%d"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1225
    .local v4, "selectedMonthKey":Ljava/lang/String;
    const-string v6, "SelectedYear%d"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 1226
    .local v5, "selectedYearKey":Ljava/lang/String;
    const-string v6, "day_selected%d"

    new-array v7, v11, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 1227
    .local v3, "selectedDayKey":Ljava/lang/String;
    invoke-interface {v2, v3, v9}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v6

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    .line 1229
    .local v1, "isDaySelected":Ljava/lang/Boolean;
    if-lez p2, :cond_3

    invoke-interface {v2, v4, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-interface {v2, v5, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_3

    invoke-virtual {v1}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v6

    if-eqz v6, :cond_3

    .line 1231
    invoke-interface {v2, v4, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_0

    .line 1232
    invoke-interface {v2, v4, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->month:I

    .line 1234
    :cond_0
    invoke-interface {v2, v5, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_1

    .line 1235
    invoke-interface {v2, v5, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->year:I

    .line 1237
    :cond_1
    invoke-interface {v2, v0, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    if-eqz v6, :cond_2

    .line 1238
    invoke-interface {v2, v0, v10}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->monthDay:I

    .line 1245
    :cond_2
    :goto_0
    iput v9, p1, Landroid/text/format/Time;->hour:I

    .line 1246
    iput v9, p1, Landroid/text/format/Time;->minute:I

    .line 1247
    iput v9, p1, Landroid/text/format/Time;->second:I

    .line 1248
    invoke-virtual {p1, v11}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v6

    return-wide v6

    .line 1241
    :cond_3
    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    const/4 v7, 0x5

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->monthDay:I

    .line 1242
    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    const/4 v7, 0x2

    invoke-virtual {v6, v7}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->month:I

    .line 1243
    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v6, v11}, Ljava/util/Calendar;->get(I)I

    move-result v6

    iput v6, p1, Landroid/text/format/Time;->year:I

    goto :goto_0
.end method

.method private getTaskItemFillInIntent(Landroid/os/Bundle;)Landroid/content/Intent;
    .locals 6
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 530
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 531
    .local v1, "fillInIntent":Landroid/content/Intent;
    const-string v2, "android.intent.action.VIEW"

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 532
    const/high16 v2, 0x14200000

    invoke-virtual {v1, v2}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 535
    const-string v2, "com.android.calendar"

    const-string v3, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 537
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    const-string v3, "id"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 538
    .local v0, "eventUri":Landroid/net/Uri;
    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 539
    const-string v2, "beginTime"

    const-string v3, "start"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v4

    invoke-virtual {v1, v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 540
    const-string v2, "VIEW"

    const-string v3, "MONTH"

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 541
    return-object v1
.end method

.method public static getUpdateIntent(Landroid/content/Context;)Landroid/app/PendingIntent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x0

    .line 1304
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1305
    .local v0, "intent":Landroid/content/Intent;
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 1306
    sget-object v1, Landroid/provider/CalendarContract;->CONTENT_URI:Landroid/net/Uri;

    const-string v2, "vnd.android.data/update"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->setDataAndType(Landroid/net/Uri;Ljava/lang/String;)Landroid/content/Intent;

    .line 1308
    invoke-static {p0, v3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private isCurrentDate()Z
    .locals 3

    .prologue
    .line 605
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "yyyy-MM-dd"

    sget-object v2, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v2}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    .line 606
    .local v0, "sdf":Ljava/text/SimpleDateFormat;
    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v1

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    return v1
.end method

.method private isSameDayTapped(Landroid/content/SharedPreferences;IIII)Z
    .locals 11
    .param p1, "pref"    # Landroid/content/SharedPreferences;
    .param p2, "selectedDay"    # I
    .param p3, "selectedMonth"    # I
    .param p4, "selectedYear"    # I
    .param p5, "targetWidgetId"    # I

    .prologue
    .line 1343
    const/4 v0, 0x0

    .line 1345
    .local v0, "isSameDayTapped":Z
    const-string v7, "prevSelectedDay%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 1346
    .local v2, "prevSelectedDayKey":Ljava/lang/String;
    const-string v7, "prevSelectedMonth%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 1347
    .local v4, "prevSelectedMonthKey":Ljava/lang/String;
    const-string v7, "prevSelectedYear%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v10

    aput-object v10, v8, v9

    invoke-static {v7, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1349
    .local v6, "prevSelectedYearKey":Ljava/lang/String;
    const/4 v1, -0x1

    .local v1, "prevSelectedDay":I
    const/4 v3, -0x1

    .local v3, "prevSelectedMonth":I
    const/4 v5, -0x1

    .line 1351
    .local v5, "prevSelectedYear":I
    const/4 v7, 0x0

    invoke-interface {p1, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_0

    .line 1352
    const/4 v7, 0x0

    invoke-interface {p1, v2, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 1354
    :cond_0
    const/4 v7, 0x0

    invoke-interface {p1, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_1

    .line 1355
    const/4 v7, 0x0

    invoke-interface {p1, v4, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1357
    :cond_1
    const/4 v7, 0x0

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    if-eqz v7, :cond_2

    .line 1358
    const/4 v7, 0x0

    invoke-interface {p1, v6, v7}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    invoke-static {v7}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1361
    :cond_2
    if-ne p2, v1, :cond_3

    if-ne p3, v3, :cond_3

    if-ne p4, v5, :cond_3

    .line 1362
    const/4 v0, 0x1

    .line 1363
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v2, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1364
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v4, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1365
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1372
    :goto_0
    const/4 v2, 0x0

    .line 1373
    const/4 v4, 0x0

    .line 1374
    const/4 v6, 0x0

    .line 1375
    return v0

    .line 1367
    :cond_3
    const/4 v0, 0x0

    .line 1368
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v2, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1369
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v4, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1370
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-static {p1, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method private registerDateFormatObserver()V
    .locals 4

    .prologue
    .line 1412
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContentResolver:Landroid/content/ContentResolver;

    if-eqz v0, :cond_0

    .line 1413
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

    invoke-direct {v0, p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

    .line 1414
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContentResolver:Landroid/content/ContentResolver;

    const-string v1, "content://settings/system/date_format"

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

    invoke-virtual {v0, v1, v2, v3}, Landroid/content/ContentResolver;->registerContentObserver(Landroid/net/Uri;ZLandroid/database/ContentObserver;)V

    .line 1417
    :cond_0
    return-void
.end method

.method private static registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;
    .locals 4
    .param p0, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p1, "intentAction"    # Ljava/lang/String;
    .param p2, "id"    # I
    .param p3, "appWidgetId"    # I

    .prologue
    .line 761
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    if-eqz v2, :cond_0

    .line 763
    new-instance v0, Landroid/content/Intent;

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-direct {v0, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 764
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {v0, p1}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 766
    const-string v2, "appWidgetId"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 767
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    const/high16 v3, 0x8000000

    invoke-static {v2, p3, v0, v3}, Landroid/app/PendingIntent;->getBroadcast(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    .line 770
    .local v1, "pendingIntent":Landroid/app/PendingIntent;
    invoke-virtual {p0, p2, v1}, Landroid/widget/RemoteViews;->setOnClickPendingIntent(ILandroid/app/PendingIntent;)V

    .line 772
    .end local v0    # "intent":Landroid/content/Intent;
    .end local v1    # "pendingIntent":Landroid/app/PendingIntent;
    :cond_0
    return-object p0
.end method

.method private setDayViewRemoteService(Landroid/content/Context;Ljava/lang/Class;Landroid/widget/RemoteViews;Landroid/appwidget/AppWidgetManager;IIILandroid/text/format/Time;II)Landroid/widget/RemoteViews;
    .locals 24
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "remoteService"    # Ljava/lang/Class;
    .param p3, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p4, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p5, "appWidgetId"    # I
    .param p6, "textViewID"    # I
    .param p7, "listViewID"    # I
    .param p8, "time"    # Landroid/text/format/Time;
    .param p9, "prevSpanX"    # I
    .param p10, "prevSpanY"    # I

    .prologue
    .line 1007
    const-string v20, "isTogglePressed%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v10

    .line 1008
    .local v10, "isToggleButtonPressed":Ljava/lang/String;
    new-instance v18, Landroid/content/Intent;

    move-object/from16 v0, v18

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 1009
    .local v18, "updateIntent":Landroid/content/Intent;
    const-string v20, "appWidgetId"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    move/from16 v2, p5

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 1011
    const/16 v20, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v20

    invoke-static/range {v20 .. v20}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 1013
    const/4 v9, 0x0

    .line 1014
    .local v9, "events":I
    const/16 v16, 0x0

    .line 1015
    .local v16, "tasks":I
    const/4 v6, 0x0

    .line 1016
    .local v6, "dayview_header":Ljava/lang/String;
    const/4 v4, 0x0

    .line 1017
    .local v4, "SpanX":I
    const/4 v5, 0x0

    .line 1018
    .local v5, "SpanY":I
    sget-object v20, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v20

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v11

    .line 1019
    .local v11, "pref":Landroid/content/SharedPreferences;
    const-string v20, "widgetSize%d"

    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    invoke-static/range {p5 .. p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v23

    aput-object v23, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v19

    .line 1020
    .local v19, "widgetSize":Ljava/lang/String;
    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    if-eqz v20, :cond_0

    .line 1021
    const/16 v20, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-interface {v11, v0, v1}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v20

    const-string v21, ";"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v12

    .line 1022
    .local v12, "resizeValues":[Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v20, v12, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v4

    .line 1023
    const/16 v20, 0x1

    aget-object v20, v12, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v5

    .line 1025
    .end local v12    # "resizeValues":[Ljava/lang/String;
    :cond_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p8

    move/from16 v3, p5

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getDayEventCount(Landroid/content/Context;Landroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v20

    const-string v21, ";"

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v13

    .line 1026
    .local v13, "taskEvents":[Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v20, v13, v20

    if-eqz v20, :cond_9

    const/16 v20, 0x0

    aget-object v20, v13, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    if-nez v20, :cond_9

    const/16 v20, 0x1

    aget-object v20, v13, v20

    if-eqz v20, :cond_9

    const/16 v20, 0x1

    aget-object v20, v13, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v20

    if-nez v20, :cond_9

    .line 1027
    const v20, 0x7f0c0034

    sget-object v21, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1028
    const v20, 0x7f0c002e

    sget-object v21, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1029
    const v20, 0x7f0c001b

    sget-object v21, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, p3

    move/from16 v1, v20

    move-object/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1030
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-interface {v11, v10, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    if-nez v20, :cond_6

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v4, v0, :cond_6

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v5, v0, :cond_6

    .line 1032
    const v20, 0x7f0c0014

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1033
    const v20, 0x7f0c0029

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1034
    const v20, 0x7f0c0012

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1039
    :goto_0
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-interface {v11, v10, v0}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    if-eqz v20, :cond_1

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v4, v0, :cond_1

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v5, v0, :cond_1

    .line 1040
    const v20, 0x7f0c0012

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1042
    :cond_1
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v4, v0, :cond_7

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v5, v0, :cond_7

    .line 1043
    const v20, 0x7f0c0014

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1044
    const v20, 0x7f0c0029

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1045
    const v20, 0x7f0c0013

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1046
    const v20, 0x7f0c0028

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1047
    const v20, 0x7f0c0012

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1059
    :goto_1
    const v20, 0x7f0c0030

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1060
    const v20, 0x7f0c0033

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1061
    const v20, 0x7f0c002d

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1062
    const v20, 0x7f0c0019

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1063
    const/16 v20, 0x8

    move-object/from16 v0, p3

    move/from16 v1, p6

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1064
    const v20, 0x7f0c002a

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1065
    const v20, 0x7f0c0016

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1083
    :goto_2
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0026

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 1084
    .local v7, "eventName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0027

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 1085
    .local v8, "eventName_pl":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0066

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v14

    .line 1086
    .local v14, "taskName":Ljava/lang/String;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v20

    const v21, 0x7f0a0065

    invoke-virtual/range {v20 .. v21}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v15

    .line 1088
    .local v15, "taskName_pl":Ljava/lang/String;
    const/16 v20, 0x0

    aget-object v20, v13, v20

    if-eqz v20, :cond_2

    .line 1089
    const/16 v20, 0x0

    aget-object v20, v13, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 1091
    :cond_2
    const/16 v20, 0x1

    aget-object v20, v13, v20

    if-eqz v20, :cond_3

    .line 1092
    const/16 v20, 0x1

    aget-object v20, v13, v20

    invoke-static/range {v20 .. v20}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v16

    .line 1094
    :cond_3
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ge v9, v0, :cond_b

    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-le v0, v1, :cond_b

    .line 1095
    const-string v20, "%s %s,  %s %s"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v7, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x1

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    aput-object v15, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 1106
    :cond_4
    :goto_3
    move-object/from16 v0, p3

    move/from16 v1, p6

    invoke-virtual {v0, v1, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1108
    move-object/from16 v0, p3

    move/from16 v1, p5

    move/from16 v2, p7

    move-object/from16 v3, v18

    invoke-virtual {v0, v1, v2, v3}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    .line 1110
    move/from16 v0, p9

    if-eq v0, v4, :cond_5

    move/from16 v0, p10

    if-eq v0, v5, :cond_5

    .line 1111
    move-object/from16 v0, p4

    move/from16 v1, p5

    move/from16 v2, p7

    invoke-virtual {v0, v1, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 1116
    :cond_5
    move-object/from16 v0, p1

    move/from16 v1, p5

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchPendingIntentTemplate(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v17

    .line 1117
    .local v17, "updateEventIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p3

    move/from16 v1, p7

    move-object/from16 v2, v17

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 1120
    return-object p3

    .line 1037
    .end local v7    # "eventName":Ljava/lang/String;
    .end local v8    # "eventName_pl":Ljava/lang/String;
    .end local v14    # "taskName":Ljava/lang/String;
    .end local v15    # "taskName_pl":Ljava/lang/String;
    .end local v17    # "updateEventIntent":Landroid/app/PendingIntent;
    :cond_6
    const v20, 0x7f0c0012

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_0

    .line 1048
    :cond_7
    const/16 v20, 0x4

    move/from16 v0, v20

    if-ne v4, v0, :cond_8

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v5, v0, :cond_8

    .line 1049
    const v20, 0x7f0c0012

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1050
    const v20, 0x7f0c0014

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1051
    const v20, 0x7f0c0013

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1052
    const v20, 0x7f0c0015

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1

    .line 1054
    :cond_8
    const v20, 0x7f0c0012

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1055
    const v20, 0x7f0c0029

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1056
    const v20, 0x7f0c0028

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_1

    .line 1067
    :cond_9
    const/16 v20, 0x4

    move/from16 v0, v20

    if-ne v4, v0, :cond_a

    const/16 v20, 0x2

    move/from16 v0, v20

    if-ne v5, v0, :cond_a

    .line 1068
    const-string v20, " I am 4*2"

    const-string v21, " i am 4*2"

    invoke-static/range {v20 .. v21}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 1069
    const v20, 0x7f0c0015

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1070
    const v20, 0x7f0c0012

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1075
    :goto_4
    const v20, 0x7f0c0033

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1076
    const v20, 0x7f0c002d

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1077
    const v20, 0x7f0c0019

    const/16 v21, 0x8

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1078
    const v20, 0x7f0c0030

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1079
    const v20, 0x7f0c002a

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1080
    const v20, 0x7f0c0016

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 1081
    const/16 v20, 0x0

    move-object/from16 v0, p3

    move/from16 v1, p6

    move/from16 v2, v20

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_2

    .line 1072
    :cond_a
    const v20, 0x7f0c0012

    const/16 v21, 0x0

    move-object/from16 v0, p3

    move/from16 v1, v20

    move/from16 v2, v21

    invoke-virtual {v0, v1, v2}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto :goto_4

    .line 1096
    .restart local v7    # "eventName":Ljava/lang/String;
    .restart local v8    # "eventName_pl":Ljava/lang/String;
    .restart local v14    # "taskName":Ljava/lang/String;
    .restart local v15    # "taskName_pl":Ljava/lang/String;
    :cond_b
    if-nez v9, :cond_c

    if-nez v16, :cond_c

    .line 1097
    const-string v6, ""

    goto/16 :goto_3

    .line 1098
    :cond_c
    const/16 v20, 0x2

    move/from16 v0, v20

    if-ge v9, v0, :cond_d

    const/16 v20, 0x2

    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_d

    .line 1099
    const-string v20, "%s %s,  %s %s"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v7, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x1

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3

    .line 1100
    :cond_d
    const/16 v20, 0x1

    move/from16 v0, v20

    if-le v9, v0, :cond_e

    const/16 v20, 0x2

    move/from16 v0, v16

    move/from16 v1, v20

    if-ge v0, v1, :cond_e

    .line 1101
    const-string v20, "%s %s,  %s %s"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v8, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x1

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    aput-object v14, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3

    .line 1102
    :cond_e
    const/16 v20, 0x1

    move/from16 v0, v20

    if-le v9, v0, :cond_4

    const/16 v20, 0x1

    move/from16 v0, v16

    move/from16 v1, v20

    if-le v0, v1, :cond_4

    .line 1103
    const-string v20, "%s %s,  %s %s"

    const/16 v21, 0x4

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    const/16 v23, 0x0

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x1

    aput-object v8, v21, v22

    const/16 v22, 0x2

    const/16 v23, 0x1

    aget-object v23, v13, v23

    aput-object v23, v21, v22

    const/16 v22, 0x3

    aput-object v15, v21, v22

    invoke-static/range {v20 .. v21}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    goto/16 :goto_3
.end method

.method private setNextDay()V
    .locals 4

    .prologue
    const/4 v2, 0x5

    .line 634
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 635
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 636
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setNextMonth()V

    .line 641
    :goto_0
    return-void

    .line 638
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 639
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0
.end method

.method private setNextMonth()V
    .locals 5

    .prologue
    const/4 v4, 0x5

    const/4 v3, 0x2

    const/4 v2, 0x1

    .line 654
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 655
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 656
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 657
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 658
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v2, v2}, Ljava/util/Calendar;->add(II)V

    .line 659
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 661
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 672
    :goto_0
    return-void

    .line 663
    :cond_1
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 664
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 665
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3, v2}, Ljava/util/Calendar;->add(II)V

    .line 666
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 670
    :cond_2
    :goto_1
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0

    .line 667
    :cond_3
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->dayLimitForFebruary:I

    if-le v0, v1, :cond_2

    .line 668
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->dayLimitForFebruary:I

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    goto :goto_1
.end method

.method private setPrevDay()V
    .locals 4

    .prologue
    .line 644
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v2, 0x5

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 645
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getMaxP()I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 646
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setPreviousMonth()V

    .line 651
    :goto_0
    return-void

    .line 648
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 649
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    const/4 v3, 0x2

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->get(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0
.end method

.method private setPreviousMonth()V
    .locals 6

    .prologue
    const/4 v5, -0x1

    const/4 v4, 0x5

    const/4 v2, 0x1

    const/4 v3, 0x2

    .line 675
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v3}, Ljava/util/Calendar;->getActualMinimum(I)I

    move-result v1

    if-ne v0, v1, :cond_1

    .line 676
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v2}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 677
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 678
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_0

    .line 679
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v2, v5}, Ljava/util/Calendar;->add(II)V

    .line 680
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 682
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v3}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    .line 694
    :goto_0
    return-void

    .line 684
    :cond_1
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3}, Ljava/util/Calendar;->get(I)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 685
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v1

    if-ne v0, v1, :cond_3

    .line 686
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v3, v5}, Ljava/util/Calendar;->add(II)V

    .line 687
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v0, v4}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 691
    :cond_2
    :goto_1
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v1, v2}, Ljava/util/Calendar;->get(I)I

    move-result v1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v0, v1, v2, v3}, Ljava/util/Calendar;->set(III)V

    goto :goto_0

    .line 688
    :cond_3
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    if-ne v0, v2, :cond_2

    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    sget v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->dayLimitForFebruary:I

    if-le v0, v1, :cond_2

    .line 689
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->dayLimitForFebruary:I

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    goto :goto_1
.end method

.method private setToday()V
    .locals 2

    .prologue
    .line 610
    invoke-static {}, Ljava/util/Calendar;->getInstance()Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    .line 611
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Calendar;

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    .line 612
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->df:Ljava/text/SimpleDateFormat;

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v1}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 613
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 614
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 615
    return-void
.end method

.method private setTodayPref(Landroid/content/Context;I)V
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetId"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v9, 0x0

    .line 499
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setToday()V

    .line 500
    sget-object v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 501
    .local v2, "pref":Landroid/content/SharedPreferences;
    const-string v6, "SelectedDay%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 502
    .local v1, "daySelectedKey":Ljava/lang/String;
    const-string v6, "SelectedMonth%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    .line 503
    .local v4, "selectedMonthKey":Ljava/lang/String;
    const-string v6, "SelectedYear%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    .line 504
    .local v5, "selectedYearKey":Ljava/lang/String;
    const-string v6, "day_selected%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    .line 505
    .local v3, "selectedDayKey":Ljava/lang/String;
    const-string v6, "curr_month%d"

    new-array v7, v10, [Ljava/lang/Object;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v9

    invoke-static {v6, v7}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 506
    .local v0, "currentMonthSelection":Ljava/lang/String;
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v1, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 507
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v4, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 508
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, ""

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    iget v7, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v2, v5, v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 509
    invoke-static {v2, v0, v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 510
    invoke-static {v2, v3, v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 511
    return-void
.end method

.method public static setUpdateNameEventsView(Ljava/lang/String;I)V
    .locals 12
    .param p0, "numberOfEventAndTask"    # Ljava/lang/String;
    .param p1, "textViewID"    # I

    .prologue
    .line 1379
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    if-eqz v8, :cond_3

    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    if-eqz v8, :cond_3

    if-eqz p0, :cond_3

    invoke-virtual {p0}, Ljava/lang/String;->length()I

    move-result v8

    if-lez v8, :cond_3

    .line 1382
    const/4 v3, 0x0

    .line 1383
    .local v3, "events":I
    const/4 v7, 0x0

    .line 1384
    .local v7, "tasks":I
    const/4 v0, 0x0

    .line 1386
    .local v0, "dayview_header":Ljava/lang/String;
    const-string v8, ";"

    invoke-virtual {p0, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 1387
    .local v4, "taskEvents":[Ljava/lang/String;
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0026

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 1388
    .local v1, "eventName":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0027

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1389
    .local v2, "eventName_pl":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0066

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 1390
    .local v5, "taskName":Ljava/lang/String;
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    invoke-virtual {v8}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v8

    const v9, 0x7f0a0065

    invoke-virtual {v8, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 1392
    .local v6, "taskName_pl":Ljava/lang/String;
    const/4 v8, 0x0

    aget-object v8, v4, v8

    if-eqz v8, :cond_0

    .line 1393
    const/4 v8, 0x0

    aget-object v8, v4, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v3

    .line 1395
    :cond_0
    const/4 v8, 0x1

    aget-object v8, v4, v8

    if-eqz v8, :cond_1

    .line 1396
    const/4 v8, 0x1

    aget-object v8, v4, v8

    invoke-static {v8}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v7

    .line 1398
    :cond_1
    const/4 v8, 0x2

    if-ge v3, v8, :cond_4

    const/4 v8, 0x1

    if-le v7, v8, :cond_4

    .line 1399
    const-string v8, "%s %s,  %s %s"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v1, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x1

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    aput-object v6, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 1407
    :cond_2
    :goto_0
    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    invoke-virtual {v8, p1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1409
    .end local v0    # "dayview_header":Ljava/lang/String;
    .end local v1    # "eventName":Ljava/lang/String;
    .end local v2    # "eventName_pl":Ljava/lang/String;
    .end local v3    # "events":I
    .end local v4    # "taskEvents":[Ljava/lang/String;
    .end local v5    # "taskName":Ljava/lang/String;
    .end local v6    # "taskName_pl":Ljava/lang/String;
    .end local v7    # "tasks":I
    :cond_3
    return-void

    .line 1400
    .restart local v0    # "dayview_header":Ljava/lang/String;
    .restart local v1    # "eventName":Ljava/lang/String;
    .restart local v2    # "eventName_pl":Ljava/lang/String;
    .restart local v3    # "events":I
    .restart local v4    # "taskEvents":[Ljava/lang/String;
    .restart local v5    # "taskName":Ljava/lang/String;
    .restart local v6    # "taskName_pl":Ljava/lang/String;
    .restart local v7    # "tasks":I
    :cond_4
    const/4 v8, 0x2

    if-ge v3, v8, :cond_5

    const/4 v8, 0x2

    if-ge v7, v8, :cond_5

    .line 1401
    const-string v8, "%s %s,  %s %s"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v1, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x1

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1402
    :cond_5
    const/4 v8, 0x1

    if-le v3, v8, :cond_6

    const/4 v8, 0x2

    if-ge v7, v8, :cond_6

    .line 1403
    const-string v8, "%s %s,  %s %s"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x1

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    aput-object v5, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 1404
    :cond_6
    const/4 v8, 0x1

    if-le v3, v8, :cond_2

    const/4 v8, 0x1

    if-le v7, v8, :cond_2

    .line 1405
    const-string v8, "%s %s,  %s %s"

    const/4 v9, 0x4

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v10, 0x0

    const/4 v11, 0x0

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x1

    aput-object v2, v9, v10

    const/4 v10, 0x2

    const/4 v11, 0x1

    aget-object v11, v4, v11

    aput-object v11, v9, v10

    const/4 v10, 0x3

    aput-object v6, v9, v10

    invoke-static {v8, v9}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method

.method private setWeekDay(Landroid/widget/RemoteViews;I)V
    .locals 3
    .param p1, "remoteViews"    # Landroid/widget/RemoteViews;
    .param p2, "diff"    # I

    .prologue
    const/4 v2, 0x2

    .line 976
    add-int/lit8 v1, p2, 0x1

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 978
    .local v0, "dayString":Ljava/lang/String;
    const v1, 0x7f0c001f

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 980
    add-int/lit8 v1, p2, 0x2

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 982
    const v1, 0x7f0c0020

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 984
    add-int/lit8 v1, p2, 0x3

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 986
    const v1, 0x7f0c0021

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 988
    add-int/lit8 v1, p2, 0x4

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 990
    const v1, 0x7f0c0022

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 992
    add-int/lit8 v1, p2, 0x5

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 994
    const v1, 0x7f0c0023

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 996
    add-int/lit8 v1, p2, 0x6

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 998
    const v1, 0x7f0c0024

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1000
    add-int/lit8 v1, p2, 0x7

    rem-int/lit8 v1, v1, 0x7

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v0

    .line 1002
    const v1, 0x7f0c0025

    invoke-virtual {p1, v1, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 1003
    return-void
.end method

.method private unRegisterDateFormatObserver()V
    .locals 2

    .prologue
    .line 1420
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

    if-eqz v0, :cond_0

    .line 1421
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContentResolver:Landroid/content/ContentResolver;

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mDateFormatObserver:Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider$DateFormatObserver;

    invoke-virtual {v0, v1}, Landroid/content/ContentResolver;->unregisterContentObserver(Landroid/database/ContentObserver;)V

    .line 1423
    :cond_0
    return-void
.end method

.method private updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IIILjava/lang/String;)Landroid/widget/RemoteViews;
    .locals 26
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "SpanX"    # I
    .param p5, "SpanY"    # I
    .param p6, "action"    # Ljava/lang/String;

    .prologue
    .line 799
    const-string v5, "isTogglePressed%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v18

    .line 800
    .local v18, "isToggleButtonPressed":Ljava/lang/String;
    move/from16 v14, p4

    .line 801
    .local v14, "prevSpanX":I
    move/from16 v15, p5

    .line 802
    .local v15, "prevSpanY":I
    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v20

    .line 803
    .local v20, "pref":Landroid/content/SharedPreferences;
    const-string v5, "widgetSize%d"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    invoke-static/range {p3 .. p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-static {v5, v6}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v25

    .line 804
    .local v25, "widgetSize":Ljava/lang/String;
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    if-eqz v5, :cond_0

    .line 805
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v25

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    const-string v6, ";"

    invoke-virtual {v5, v6}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v21

    .line 806
    .local v21, "resizeValues":[Ljava/lang/String;
    const/4 v5, 0x0

    aget-object v5, v21, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p4

    .line 807
    const/4 v5, 0x1

    aget-object v5, v21, v5

    invoke-static {v5}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result p5

    .line 809
    .end local v21    # "resizeValues":[Ljava/lang/String;
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_5

    const/4 v5, 0x2

    move/from16 v0, p4

    if-gt v0, v5, :cond_1

    const/4 v5, 0x2

    move/from16 v0, p5

    if-le v0, v5, :cond_5

    .line 810
    :cond_1
    const v5, 0x7f030003

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    .line 816
    :goto_0
    new-instance v4, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    invoke-direct {v4, v5, v6}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 819
    .local v4, "remoteViews":Landroid/widget/RemoteViews;
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x2

    if-ne v5, v6, :cond_6

    .line 820
    const v5, 0x7f0c001d

    const/16 v6, 0x9

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    .line 824
    :cond_2
    :goto_1
    sput-object v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mRemoteViews:Landroid/widget/RemoteViews;

    .line 825
    const-string v5, "UpdateNextMonth"

    const v6, 0x7f0c000f

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 826
    const-string v5, "UpdatePreviousMonth"

    const v6, 0x7f0c000d

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 828
    const-string v5, "TOGGLE_BUTTON_UPDATE"

    const v6, 0x7f0c0011

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 829
    const-string v5, "ActionNewEvent"

    const v6, 0x7f0c0012

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 830
    const-string v5, "ActionNewEvent"

    const v6, 0x7f0c0015

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 831
    const-string v5, "ActionNewEvent"

    const v6, 0x7f0c0033

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 832
    const-string v5, "ActionNewEvent"

    const v6, 0x7f0c002d

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 833
    const-string v5, "ActionNewEvent"

    const v6, 0x7f0c0019

    move/from16 v0, p3

    invoke-static {v4, v5, v6, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerOnClickPendingIntentListener(Landroid/widget/RemoteViews;Ljava/lang/String;II)Landroid/widget/RemoteViews;

    .line 835
    new-instance v13, Landroid/text/format/Time;

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    invoke-direct {v13, v5}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 836
    .local v13, "time":Landroid/text/format/Time;
    invoke-virtual {v13}, Landroid/text/format/Time;->setToNow()V

    .line 839
    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v22

    .line 840
    .local v22, "startDay":I
    add-int/lit8 v17, v22, -0x1

    .line 841
    .local v17, "diff":I
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v4, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setWeekDay(Landroid/widget/RemoteViews;I)V

    .line 843
    const v5, 0x7f0c001c

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 845
    if-lez p4, :cond_3

    if-lez p5, :cond_3

    .line 846
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ge v0, v5, :cond_7

    const/4 v5, 0x4

    move/from16 v0, p5

    if-ge v0, v5, :cond_7

    .line 847
    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v0, v5, :cond_3

    const/4 v5, 0x2

    move/from16 v0, p5

    if-ne v0, v5, :cond_3

    .line 849
    const v5, 0x7f0c0014

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 850
    const v5, 0x7f0c0029

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 851
    const v5, 0x7f0c0012

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 853
    const v5, 0x7f0c0011

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 854
    const v5, 0x7f0c0011

    const-string v6, "setBackgroundResource"

    const v7, 0x7f020007

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 861
    :cond_3
    :goto_2
    const/4 v5, 0x2

    move/from16 v0, p4

    if-ne v0, v5, :cond_8

    const/4 v5, 0x2

    move/from16 v0, p5

    if-ne v0, v5, :cond_8

    .line 862
    const v5, 0x7f0c0014

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 863
    const v5, 0x7f0c0029

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 864
    const v5, 0x7f0c0012

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 865
    const v5, 0x7f0c0013

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 866
    const v5, 0x7f0c0028

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 877
    :goto_3
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-interface {v0, v1, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v5

    if-eqz v5, :cond_a

    const/4 v5, 0x4

    move/from16 v0, p4

    if-ge v0, v5, :cond_a

    const/4 v5, 0x4

    move/from16 v0, p5

    if-ge v0, v5, :cond_a

    .line 878
    const v5, 0x7f0c001c

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 879
    const v5, 0x7f0c002f

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 880
    const v5, 0x7f0c0011

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 881
    const v5, 0x7f0c0011

    const-string v6, "setBackgroundResource"

    const v7, 0x7f020009

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 882
    const v5, 0x7f0c0011

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a000d

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 883
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    const-string v6, "date_format"

    invoke-static {v5, v6}, Landroid/provider/Settings$System;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 885
    .local v16, "dateFormat":Ljava/lang/String;
    if-eqz p6, :cond_4

    const-string v5, "android.intent.action.PROVIDER_CHANGED"

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    const-string v5, "com.sec.android.widgetapp.APPWIDGET_EDITMODE"

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_4

    .line 887
    const v5, 0x7f0c000e

    move-object/from16 v0, p0

    move-object/from16 v1, v16

    move-object/from16 v2, p1

    move/from16 v3, p3

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getDayTitle(Ljava/lang/String;Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 889
    :cond_4
    const-class v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService;

    const v11, 0x7f0c0031

    const v12, 0x7f0c0032

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v8, v4

    move-object/from16 v9, p2

    move/from16 v10, p3

    invoke-direct/range {v5 .. v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setDayViewRemoteService(Landroid/content/Context;Ljava/lang/Class;Landroid/widget/RemoteViews;Landroid/appwidget/AppWidgetManager;IIILandroid/text/format/Time;II)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 918
    .end local v16    # "dateFormat":Ljava/lang/String;
    :goto_4
    return-object v4

    .line 813
    .end local v4    # "remoteViews":Landroid/widget/RemoteViews;
    .end local v13    # "time":Landroid/text/format/Time;
    .end local v17    # "diff":I
    .end local v22    # "startDay":I
    :cond_5
    const v5, 0x7f030002

    move-object/from16 v0, p0

    iput v5, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    goto/16 :goto_0

    .line 821
    .restart local v4    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_6
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    invoke-virtual {v5}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v5

    iget v5, v5, Landroid/content/res/Configuration;->orientation:I

    const/4 v6, 0x1

    if-ne v5, v6, :cond_2

    .line 822
    const v5, 0x7f0c001d

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    invoke-virtual/range {v4 .. v9}, Landroid/widget/RemoteViews;->setViewPadding(IIIII)V

    goto/16 :goto_1

    .line 857
    .restart local v13    # "time":Landroid/text/format/Time;
    .restart local v17    # "diff":I
    .restart local v22    # "startDay":I
    :cond_7
    const v5, 0x7f0c0011

    const/4 v6, 0x4

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 858
    const/4 v5, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v18

    invoke-static {v0, v1, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 867
    :cond_8
    const/4 v5, 0x4

    move/from16 v0, p4

    if-ne v0, v5, :cond_9

    const/4 v5, 0x2

    move/from16 v0, p5

    if-ne v0, v5, :cond_9

    .line 868
    const v5, 0x7f0c0012

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 869
    const v5, 0x7f0c0013

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 870
    const v5, 0x7f0c0014

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 874
    :cond_9
    const v5, 0x7f0c0028

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 875
    const v5, 0x7f0c0029

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_3

    .line 892
    :cond_a
    const v5, 0x7f0c002f

    const/16 v6, 0x8

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 893
    const v5, 0x7f0c001c

    const/4 v6, 0x0

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 894
    const v5, 0x7f0c0011

    const-string v6, "setBackgroundResource"

    const v7, 0x7f020007

    invoke-virtual {v4, v5, v6, v7}, Landroid/widget/RemoteViews;->setInt(ILjava/lang/String;I)V

    .line 895
    const v5, 0x7f0c0011

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f0a000e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v5, v6}, Landroid/widget/RemoteViews;->setContentDescription(ILjava/lang/CharSequence;)V

    .line 896
    new-instance v24, Landroid/content/Intent;

    const-class v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineCalendarViewService;

    move-object/from16 v0, v24

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v5}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 897
    .local v24, "updateIntent":Landroid/content/Intent;
    const-string v5, "appWidgetId"

    move-object/from16 v0, v24

    move/from16 v1, p3

    invoke-virtual {v0, v5, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 898
    const/4 v5, 0x1

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    move-object/from16 v0, v24

    invoke-virtual {v0, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 900
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, p3

    move-object/from16 v3, p6

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->formatMonthYear(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v19

    .line 901
    .local v19, "monthYear":Ljava/lang/String;
    const v5, 0x7f0c000e

    move-object/from16 v0, v19

    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 904
    const v5, 0x7f0c0027

    move/from16 v0, p3

    move-object/from16 v1, v24

    invoke-virtual {v4, v0, v5, v1}, Landroid/widget/RemoteViews;->setRemoteAdapter(IILandroid/content/Intent;)V

    .line 905
    if-eqz p6, :cond_b

    const-string v5, "com.sec.android.intent.CHANGE_SHARE"

    move-object/from16 v0, p6

    invoke-virtual {v0, v5}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_b

    .line 906
    const v5, 0x7f0c0027

    move-object/from16 v0, p2

    move/from16 v1, p3

    invoke-virtual {v0, v1, v5}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 908
    :cond_b
    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-static {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getCalendarItemPendingIntentTemplate(Landroid/content/Context;I)Landroid/app/PendingIntent;

    move-result-object v23

    .line 909
    .local v23, "updateEventIntent":Landroid/app/PendingIntent;
    const v5, 0x7f0c0027

    move-object/from16 v0, v23

    invoke-virtual {v4, v5, v0}, Landroid/widget/RemoteViews;->setPendingIntentTemplate(ILandroid/app/PendingIntent;)V

    .line 912
    const-class v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService42;

    const v11, 0x7f0c0017

    const v12, 0x7f0c0018

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v8, v4

    move-object/from16 v9, p2

    move/from16 v10, p3

    invoke-direct/range {v5 .. v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setDayViewRemoteService(Landroid/content/Context;Ljava/lang/Class;Landroid/widget/RemoteViews;Landroid/appwidget/AppWidgetManager;IIILandroid/text/format/Time;II)Landroid/widget/RemoteViews;

    move-result-object v4

    .line 914
    const-class v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineDayViewService24;

    const v11, 0x7f0c002b

    const v12, 0x7f0c002c

    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move-object v8, v4

    move-object/from16 v9, p2

    move/from16 v10, p3

    invoke-direct/range {v5 .. v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setDayViewRemoteService(Landroid/content/Context;Ljava/lang/Class;Landroid/widget/RemoteViews;Landroid/appwidget/AppWidgetManager;IIILandroid/text/format/Time;II)Landroid/widget/RemoteViews;

    move-result-object v4

    goto/16 :goto_4
.end method


# virtual methods
.method public getEditEventPendingIntent(Landroid/content/Context;Landroid/text/format/Time;)Landroid/app/PendingIntent;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # Landroid/text/format/Time;

    .prologue
    .line 1278
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchFillInIntent(Landroid/content/Context;Landroid/text/format/Time;)Landroid/content/Intent;

    move-result-object v0

    .line 1279
    .local v0, "launchIntent":Landroid/content/Intent;
    const/4 v1, 0x0

    const/high16 v2, 0x10000000

    invoke-static {p1, v1, v0, v2}, Landroid/app/PendingIntent;->getActivity(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method getLaunchFillInIntent(Landroid/content/Context;Landroid/text/format/Time;)Landroid/content/Intent;
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v6, 0x0

    .line 546
    new-instance v4, Landroid/text/format/Time;

    invoke-direct {v4}, Landroid/text/format/Time;-><init>()V

    .line 548
    .local v4, "tempTime":Landroid/text/format/Time;
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    iput v5, v4, Landroid/text/format/Time;->monthDay:I

    .line 549
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    iput v5, v4, Landroid/text/format/Time;->month:I

    .line 550
    iget v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    iput v5, v4, Landroid/text/format/Time;->year:I

    .line 551
    new-instance v0, Landroid/text/format/Time;

    invoke-direct {v0}, Landroid/text/format/Time;-><init>()V

    .line 552
    .local v0, "EventStartTime":Landroid/text/format/Time;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isToday(Landroid/text/format/Time;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 553
    invoke-virtual {v0}, Landroid/text/format/Time;->setToNow()V

    .line 554
    iget v5, v0, Landroid/text/format/Time;->hour:I

    add-int/lit8 v5, v5, 0x1

    iput v5, v0, Landroid/text/format/Time;->hour:I

    .line 559
    :goto_0
    iput v6, v0, Landroid/text/format/Time;->minute:I

    .line 560
    iput v6, v0, Landroid/text/format/Time;->second:I

    .line 561
    const/4 v5, 0x0

    invoke-static {p1, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, v0, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 562
    const/4 v5, 0x1

    invoke-virtual {v0, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 564
    .local v2, "startMillis":J
    new-instance v1, Landroid/content/Intent;

    invoke-direct {v1}, Landroid/content/Intent;-><init>()V

    .line 565
    .local v1, "launchIntent":Landroid/content/Intent;
    const-string v5, "android.intent.action.VIEW"

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    .line 566
    const-string v5, "com.android.calendar"

    const-string v6, "com.android.calendar.EditEventActivity"

    invoke-virtual {v1, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 568
    const/high16 v5, 0x14200000

    invoke-virtual {v1, v5}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 571
    const-string v5, "beginTime"

    invoke-virtual {v1, v5, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 572
    const-string v5, "endTime"

    const-wide/32 v6, 0x36ee80

    add-long/2addr v6, v2

    invoke-virtual {v1, v5, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 574
    return-object v1

    .line 556
    .end local v1    # "launchIntent":Landroid/content/Intent;
    .end local v2    # "startMillis":J
    :cond_0
    invoke-virtual {v0, v4}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 557
    const/16 v5, 0x8

    iput v5, v0, Landroid/text/format/Time;->hour:I

    goto :goto_0
.end method

.method public magazineWidgetRotation(Landroid/content/Context;III)Z
    .locals 11
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "widgetId"    # I
    .param p3, "spanX"    # I
    .param p4, "spanY"    # I

    .prologue
    const/4 v10, 0x1

    const/4 v4, 0x2

    .line 1253
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->df:Ljava/text/SimpleDateFormat;

    if-nez v0, :cond_0

    .line 1254
    new-instance v0, Ljava/text/SimpleDateFormat;

    const-string v1, "dd"

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v0, v1, v3}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->df:Ljava/text/SimpleDateFormat;

    .line 1257
    :cond_0
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    iget v0, v0, Landroid/content/res/Configuration;->orientation:I

    if-ne v0, v4, :cond_2

    if-gt p3, v4, :cond_1

    if-le p4, v4, :cond_2

    .line 1258
    :cond_1
    const v0, 0x7f030003

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    .line 1264
    :goto_0
    new-instance v8, Landroid/widget/RemoteViews;

    invoke-virtual {p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v0

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    invoke-direct {v8, v0, v1}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 1265
    .local v8, "r":Landroid/widget/RemoteViews;
    const v0, 0x7f0c000c

    invoke-virtual {v8, v0}, Landroid/widget/RemoteViews;->removeAllViews(I)V

    .line 1267
    invoke-static {p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v2

    .line 1268
    .local v2, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v7

    .line 1269
    .local v7, "pref":Landroid/content/SharedPreferences;
    const-string v0, "widgetSize%d"

    new-array v1, v10, [Ljava/lang/Object;

    const/4 v3, 0x0

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v1, v3

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v9

    .line 1270
    .local v9, "widgetSize":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ";"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v7, v9, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 1271
    const/4 v6, 0x0

    move-object v0, p0

    move-object v1, p1

    move v3, p2

    move v4, p3

    move v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IIILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v0

    invoke-virtual {v2, p2, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 1272
    return v10

    .line 1261
    .end local v2    # "widgetMgr":Landroid/appwidget/AppWidgetManager;
    .end local v7    # "pref":Landroid/content/SharedPreferences;
    .end local v8    # "r":Landroid/widget/RemoteViews;
    .end local v9    # "widgetSize":Ljava/lang/String;
    :cond_2
    const v0, 0x7f030002

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    goto :goto_0
.end method

.method public onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetId"    # I
    .param p4, "newOptions"    # Landroid/os/Bundle;

    .prologue
    .line 779
    invoke-super {p0, p1, p2, p3, p4}, Landroid/appwidget/AppWidgetProvider;->onAppWidgetOptionsChanged(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;ILandroid/os/Bundle;)V

    .line 782
    return-void
.end method

.method public onDeleted(Landroid/content/Context;[I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetIds"    # [I

    .prologue
    const/4 v5, 0x0

    .line 786
    invoke-super {p0, p1, p2}, Landroid/appwidget/AppWidgetProvider;->onDeleted(Landroid/content/Context;[I)V

    .line 787
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 788
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "widget%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 789
    .local v1, "widgetId":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 790
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    sput-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    .line 792
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->unRegisterDateFormatObserver()V

    .line 793
    sput-boolean v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    .line 794
    return-void
.end method

.method public onDisabled(Landroid/content/Context;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v5, 0x0

    .line 587
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onDisabled(Landroid/content/Context;)V

    .line 588
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    invoke-static {p1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 589
    .local v0, "pref":Landroid/content/SharedPreferences;
    const-string v2, "widget%d"

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    sget v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    aput-object v4, v3, v5

    invoke-static {v2, v3}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 590
    .local v1, "widgetId":Ljava/lang/String;
    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 591
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v2}, Ljava/util/Calendar;->clone()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/Calendar;

    sput-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    .line 592
    sput-boolean v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    .line 593
    return-void
.end method

.method public onEnabled(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 579
    invoke-super {p0, p1}, Landroid/appwidget/AppWidgetProvider;->onEnabled(Landroid/content/Context;)V

    .line 580
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setToday()V

    .line 581
    const/4 v0, 0x1

    sput-boolean v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 582
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    .line 583
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 50
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 118
    const-string v2, "MagazineAppWidgetProvider"

    const-string v8, "OnReceive : Intent Action - intent.getAction()"

    invoke-static {v2, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 119
    const/4 v12, 0x0

    .line 120
    .local v12, "spanX":I
    const/4 v13, 0x0

    .line 121
    .local v13, "spanY":I
    const/4 v7, 0x0

    .line 122
    .local v7, "targetWidgetId":I
    sput-object p1, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    .line 123
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getMessage()Ljava/lang/CharSequence;

    move-result-object v2

    sput-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    .line 124
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v14

    .line 125
    .local v14, "action":Ljava/lang/String;
    const-string v2, "appWidgetId"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 126
    const-string v2, "com.sec.android.widgetapp.APPWIDGET_RESIZE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 127
    const-string v2, "widgetspanx"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v12

    .line 128
    const-string v2, "widgetspany"

    const/4 v8, 0x0

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v13

    .line 129
    const-string v2, "widgetId"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    .line 130
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 131
    .local v3, "pref":Landroid/content/SharedPreferences;
    const-string v2, "widgetSize%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v48

    .line 132
    .local v48, "widgetSize":Ljava/lang/String;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v8, ";"

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v13}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v48

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 133
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    .line 135
    .end local v3    # "pref":Landroid/content/SharedPreferences;
    .end local v48    # "widgetSize":Ljava/lang/String;
    :cond_0
    new-instance v2, Landroid/text/format/Time;

    const/4 v8, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {v2, v8}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mTime:Landroid/text/format/Time;

    .line 136
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mTime:Landroid/text/format/Time;

    invoke-virtual {v2}, Landroid/text/format/Time;->setToNow()V

    .line 137
    new-instance v2, Ljava/text/SimpleDateFormat;

    const-string v8, "dd"

    sget-object v9, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-direct {v2, v8, v9}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->df:Ljava/text/SimpleDateFormat;

    .line 139
    invoke-static {}, Ljava/util/GregorianCalendar;->getInstance()Ljava/util/Calendar;

    move-result-object v2

    check-cast v2, Ljava/util/GregorianCalendar;

    sput-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    .line 140
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 141
    .restart local v3    # "pref":Landroid/content/SharedPreferences;
    const-string v2, "SelectedDay%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    .line 142
    .local v29, "daySelectedKey":Ljava/lang/String;
    const-string v2, "SelectedMonth%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    .line 143
    .local v40, "selectedMonthKey":Ljava/lang/String;
    const-string v2, "SelectedYear%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    .line 144
    .local v42, "selectedYearKey":Ljava/lang/String;
    const-string v2, "day_selected%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v39

    .line 145
    .local v39, "selectedDayKey":Ljava/lang/String;
    const-string v2, "today_selected%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v41

    .line 146
    .local v41, "selectedTodayKey":Ljava/lang/String;
    const-string v2, "next_month%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v34

    .line 147
    .local v34, "nextMonthSelection":Ljava/lang/String;
    const-string v2, "prev_month%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v36

    .line 148
    .local v36, "prevMonthSelection":Ljava/lang/String;
    const-string v2, "curr_month%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v27

    .line 149
    .local v27, "currentMonthSelection":Ljava/lang/String;
    const-string v2, "next_day%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v33

    .line 150
    .local v33, "nextDaySelection":Ljava/lang/String;
    const-string v2, "prev_day%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v35

    .line 151
    .local v35, "prevDaySelection":Ljava/lang/String;
    const-string v2, "isTogglePressed%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v31

    .line 152
    .local v31, "isToggleButtonPressed":Ljava/lang/String;
    const-string v2, "widget%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    sget v15, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v46

    .line 153
    .local v46, "widgetId":Ljava/lang/String;
    sput v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    .line 154
    const-string v2, "SelectedDay"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v28

    .line 155
    .local v28, "day":I
    const-string v2, "SelectedMonth"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v32

    .line 156
    .local v32, "month":I
    const-string v2, "SelectedYear"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v49

    .line 157
    .local v49, "year":I
    const/4 v2, 0x0

    move-object/from16 v0, v39

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->day_selected:Z

    .line 158
    const/4 v2, 0x0

    move-object/from16 v0, v41

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->today_selected:Z

    .line 159
    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_1

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 160
    :cond_1
    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    if-lez v2, :cond_12

    .line 161
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    sget v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v46

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 171
    :cond_2
    :goto_0
    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_3

    .line 172
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 174
    :cond_3
    invoke-static/range {p1 .. p1}, Landroid/appwidget/AppWidgetManager;->getInstance(Landroid/content/Context;)Landroid/appwidget/AppWidgetManager;

    move-result-object v10

    .line 175
    .local v10, "widgetMgr":Landroid/appwidget/AppWidgetManager;
    if-lez v7, :cond_14

    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->widgetResize:Z

    if-eqz v2, :cond_4

    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    if-eqz v2, :cond_5

    :cond_4
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->day_selected:Z

    if-nez v2, :cond_5

    if-gez v28, :cond_5

    if-gez v32, :cond_5

    if-ltz v49, :cond_14

    :cond_5
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_14

    .line 177
    const/4 v2, 0x0

    move-object/from16 v0, v29

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_6

    .line 178
    const/4 v2, 0x0

    move-object/from16 v0, v29

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 180
    :cond_6
    const/4 v2, 0x0

    move-object/from16 v0, v40

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_7

    .line 181
    const/4 v2, 0x0

    move-object/from16 v0, v40

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 183
    :cond_7
    const/4 v2, 0x0

    move-object/from16 v0, v42

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_8

    .line 184
    const/4 v2, 0x0

    move-object/from16 v0, v42

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 193
    :cond_8
    :goto_1
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8, v9, v15}, Ljava/util/Calendar;->set(III)V

    .line 195
    const-string v2, "TOGGLE_BUTTON_UPDATE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_9

    .line 196
    const/4 v12, 0x2

    .line 197
    const/4 v13, 0x2

    .line 198
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_19

    .line 199
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 204
    :cond_9
    :goto_2
    const-string v2, "UpdateNextMonth"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1b

    .line 205
    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 206
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 207
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1a

    .line 208
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setNextDay()V

    .line 209
    const/4 v2, 0x1

    move-object/from16 v0, v33

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 210
    const v2, 0x7f0c0032

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 219
    :cond_a
    :goto_3
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 220
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 221
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 222
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8, v9, v15}, Ljava/util/Calendar;->set(III)V

    .line 223
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 433
    :cond_b
    :goto_4
    const-string v2, "android.intent.action.PROVIDER_CHANGED"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v22

    .line 435
    .local v22, "PROVIDER_CHANGED":Z
    new-instance v26, Landroid/content/ComponentName;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    const-class v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;

    invoke-virtual {v8}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v8

    move-object/from16 v0, v26

    invoke-direct {v0, v2, v8}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 436
    .local v26, "cn":Landroid/content/ComponentName;
    move-object/from16 v0, v26

    invoke-virtual {v10, v0}, Landroid/appwidget/AppWidgetManager;->getAppWidgetIds(Landroid/content/ComponentName;)[I

    move-result-object v47

    .line 437
    .local v47, "widgetIds":[I
    const/4 v11, 0x0

    .line 438
    .local v11, "widgetIdSingle":I
    const-string v2, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    const-string v2, "clock.date_format_changed"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_c

    if-eqz v22, :cond_3c

    .line 441
    :cond_c
    const/16 v30, 0x0

    .local v30, "i":I
    :goto_5
    move-object/from16 v0, v47

    array-length v2, v0

    move/from16 v0, v30

    if-ge v0, v2, :cond_3a

    .line 442
    aget v11, v47, v30

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    .line 443
    invoke-direct/range {v8 .. v14}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IIILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 444
    if-nez v22, :cond_d

    .line 445
    const v2, 0x7f0c0027

    invoke-virtual {v10, v11, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 447
    :cond_d
    if-eqz v22, :cond_e

    .line 448
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->mFirstTZRequest:Z

    .line 449
    const v2, 0x7f0c0032

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 450
    const v2, 0x7f0c002c

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 451
    const v2, 0x7f0c0018

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 453
    :cond_e
    const-string v2, "day_selected%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v39

    .line 454
    const-string v2, "SelectedDay%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aget v15, v47, v30

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v29

    .line 455
    const-string v2, "SelectedMonth%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aget v15, v47, v30

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v40

    .line 456
    const-string v2, "SelectedYear%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    aget v15, v47, v30

    invoke-static {v15}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v42

    .line 457
    const/4 v2, 0x0

    move-object/from16 v0, v29

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    .line 458
    const/4 v2, 0x0

    move-object/from16 v0, v29

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 460
    :cond_f
    const/4 v2, 0x0

    move-object/from16 v0, v40

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_10

    .line 461
    const/4 v2, 0x0

    move-object/from16 v0, v40

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 463
    :cond_10
    const/4 v2, 0x0

    move-object/from16 v0, v42

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_11

    .line 464
    const/4 v2, 0x0

    move-object/from16 v0, v42

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    .line 466
    :cond_11
    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 467
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 468
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 469
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 441
    add-int/lit8 v30, v30, 0x1

    goto/16 :goto_5

    .line 162
    .end local v10    # "widgetMgr":Landroid/appwidget/AppWidgetManager;
    .end local v11    # "widgetIdSingle":I
    .end local v22    # "PROVIDER_CHANGED":Z
    .end local v26    # "cn":Landroid/content/ComponentName;
    .end local v30    # "i":I
    .end local v47    # "widgetIds":[I
    :cond_12
    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    if-nez v2, :cond_2

    sget v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    if-gez v2, :cond_2

    .line 163
    const/4 v2, 0x0

    move-object/from16 v0, v46

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_13

    .line 164
    const/4 v2, 0x0

    move-object/from16 v0, v46

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    .line 165
    sget v7, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    goto/16 :goto_0

    .line 167
    :cond_13
    const/4 v2, -0x1

    sput v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->tempTargetWidgetId:I

    goto/16 :goto_0

    .line 186
    .restart local v10    # "widgetMgr":Landroid/appwidget/AppWidgetManager;
    :cond_14
    const-string v2, "com.sec.android.intent.CHANGE_SHARE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_15

    sget-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    if-nez v2, :cond_17

    :cond_15
    if-gez v28, :cond_16

    if-gez v32, :cond_16

    if-ltz v49, :cond_17

    :cond_16
    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_18

    .line 187
    :cond_17
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setToday()V

    goto/16 :goto_1

    .line 189
    :cond_18
    const-string v2, "SelectedDay"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    .line 190
    const-string v2, "SelectedMonth"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    .line 191
    const-string v2, "SelectedYear"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    goto/16 :goto_1

    .line 201
    :cond_19
    const/4 v2, 0x1

    move-object/from16 v0, v31

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    goto/16 :goto_2

    .line 212
    :cond_1a
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setNextMonth()V

    .line 213
    const/4 v2, 0x1

    move-object/from16 v0, v34

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 214
    const-string v2, "SelectedDay"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 215
    .local v4, "select_day":I
    const/4 v2, -0x1

    if-eq v4, v2, :cond_a

    .line 216
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    goto/16 :goto_3

    .line 224
    .end local v4    # "select_day":I
    :cond_1b
    const-string v2, "UpdatePreviousMonth"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1e

    .line 225
    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 226
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 227
    const/4 v2, 0x0

    move-object/from16 v0, v31

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1d

    .line 228
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setPrevDay()V

    .line 229
    const/4 v2, 0x1

    move-object/from16 v0, v35

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 230
    const v2, 0x7f0c0032

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 239
    :cond_1c
    :goto_6
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 240
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 241
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 242
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8, v9, v15}, Ljava/util/Calendar;->set(III)V

    .line 243
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    goto/16 :goto_4

    .line 232
    :cond_1d
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setPreviousMonth()V

    .line 233
    const/4 v2, 0x1

    move-object/from16 v0, v36

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 234
    const-string v2, "SelectedDay"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 235
    .restart local v4    # "select_day":I
    const/4 v2, -0x1

    if-eq v4, v2, :cond_1c

    .line 236
    move-object/from16 v0, p0

    iput v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    goto/16 :goto_6

    .line 244
    .end local v4    # "select_day":I
    :cond_1e
    const-string v2, "UpdateTodayMonth"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1f

    .line 245
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 246
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isCurrentDate()Z

    move-result v2

    if-nez v2, :cond_b

    .line 247
    invoke-direct/range {p0 .. p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setToday()V

    .line 248
    const/4 v2, 0x1

    move-object/from16 v0, v27

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 249
    const/4 v2, 0x0

    move-object/from16 v0, v39

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 250
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    goto/16 :goto_4

    .line 252
    :cond_1f
    const-string v2, "ActionNewEvent"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_20

    .line 253
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mTime:Landroid/text/format/Time;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchFillInIntent(Landroid/content/Context;Landroid/text/format/Time;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 254
    :cond_20
    const-string v2, "CalendarItemUpdate"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_34

    .line 255
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->app_widget_enabled:Z

    .line 256
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isDayClicked:Z

    .line 257
    const/4 v2, 0x1

    move-object/from16 v0, v39

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 258
    const-string v2, "SelectedDay"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v4

    .line 259
    .restart local v4    # "select_day":I
    const-string v2, "SelectedMonth"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v5

    .line 260
    .local v5, "select_month":I
    const-string v2, "SelectedYear"

    const/4 v8, -0x1

    move-object/from16 v0, p2

    invoke-virtual {v0, v2, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v6

    .line 261
    .local v6, "select_year":I
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->df:Ljava/text/SimpleDateFormat;

    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mSelectedDate:Ljava/util/Calendar;

    invoke-virtual {v8}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v8

    invoke-virtual {v2, v8}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-ne v4, v2, :cond_25

    .line 262
    const/4 v2, 0x1

    move-object/from16 v0, v41

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    .line 266
    :goto_7
    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    if-eq v5, v2, :cond_26

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    const/4 v8, 0x1

    if-ge v2, v8, :cond_21

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    const/4 v8, 0x1

    if-ge v2, v8, :cond_21

    move-object/from16 v0, p0

    iget v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    const/4 v8, 0x1

    if-lt v2, v8, :cond_26

    .line 267
    :cond_21
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 268
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 269
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 270
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v2, v8, v9, v15}, Ljava/util/Calendar;->set(III)V

    .line 277
    :goto_8
    const/16 v23, 0x0

    .line 278
    .local v23, "SpanX":I
    const/16 v24, 0x0

    .line 279
    .local v24, "SpanY":I
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->prefsName:Ljava/lang/String;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->getSharedPreferences(Landroid/content/Context;Ljava/lang/String;)Landroid/content/SharedPreferences;

    move-result-object v3

    .line 280
    const-string v2, "widgetSize%d"

    const/4 v8, 0x1

    new-array v8, v8, [Ljava/lang/Object;

    const/4 v9, 0x0

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v15

    aput-object v15, v8, v9

    invoke-static {v2, v8}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v48

    .line 281
    .restart local v48    # "widgetSize":Ljava/lang/String;
    const/4 v2, 0x0

    move-object/from16 v0, v48

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_22

    .line 282
    const/4 v2, 0x0

    move-object/from16 v0, v48

    invoke-interface {v3, v0, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v8, ";"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v38

    .line 283
    .local v38, "resizeValues":[Ljava/lang/String;
    const/4 v2, 0x0

    aget-object v2, v38, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v23

    .line 284
    const/4 v2, 0x1

    aget-object v2, v38, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v24

    .line 286
    .end local v38    # "resizeValues":[Ljava/lang/String;
    :cond_22
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContext:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mTime:Landroid/text/format/Time;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v8, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getDayEventCount(Landroid/content/Context;Landroid/text/format/Time;I)Ljava/lang/String;

    move-result-object v2

    const-string v8, ";"

    invoke-virtual {v2, v8}, Ljava/lang/String;->split(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v43

    .line 287
    .local v43, "taskEvents":[Ljava/lang/String;
    const-string v2, "uNo of events"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "posiotp"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x0

    aget-object v9, v43, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const/4 v9, 0x1

    aget-object v9, v43, v9

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 288
    const/4 v2, 0x0

    aget-object v2, v43, v2

    if-eqz v2, :cond_23

    const/4 v2, 0x0

    aget-object v2, v43, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_23

    const/4 v2, 0x1

    aget-object v2, v43, v2

    if-eqz v2, :cond_23

    const/4 v2, 0x1

    aget-object v2, v43, v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v2

    if-nez v2, :cond_23

    .line 289
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mIsDayEventsListVisible:Z

    .line 291
    :cond_23
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mIsDayEventsListVisible:Z

    if-eqz v2, :cond_2a

    .line 292
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mIsDayEventsListVisible:Z

    .line 293
    const-string v2, "updating calendar item"

    const-string v8, "updating calendar item"

    invoke-static {v2, v8}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 294
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 295
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v2, v8, :cond_27

    const/4 v2, 0x2

    move/from16 v0, v23

    if-gt v0, v2, :cond_24

    const/4 v2, 0x2

    move/from16 v0, v24

    if-le v0, v2, :cond_27

    .line 296
    :cond_24
    const v2, 0x7f030003

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    .line 302
    :goto_9
    new-instance v37, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    move-object/from16 v0, v37

    invoke-direct {v0, v2, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 305
    .local v37, "remoteViews":Landroid/widget/RemoteViews;
    const/4 v2, 0x2

    move/from16 v0, v23

    if-ne v0, v2, :cond_28

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_28

    .line 306
    const v2, 0x7f0c0014

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 307
    const v2, 0x7f0c0029

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 308
    const v2, 0x7f0c0013

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 309
    const v2, 0x7f0c0028

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 310
    const v2, 0x7f0c0012

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 321
    :goto_a
    const v2, 0x7f0c0034

    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 322
    const v2, 0x7f0c002e

    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 323
    const v2, 0x7f0c001b

    sget-object v8, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mNoEventsTextMessage:Ljava/lang/CharSequence;

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setTextViewText(ILjava/lang/CharSequence;)V

    .line 324
    const v2, 0x7f0c0033

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 325
    const v2, 0x7f0c002d

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 326
    const v2, 0x7f0c0019

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 327
    const v2, 0x7f0c0031

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 328
    const v2, 0x7f0c002b

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 329
    const v2, 0x7f0c0017

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 330
    const v2, 0x7f0c002a

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 331
    const v2, 0x7f0c0016

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 332
    move-object/from16 v0, v37

    invoke-virtual {v10, v7, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 492
    .end local v4    # "select_day":I
    .end local v5    # "select_month":I
    .end local v6    # "select_year":I
    .end local v23    # "SpanX":I
    .end local v24    # "SpanY":I
    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    .end local v43    # "taskEvents":[Ljava/lang/String;
    .end local v48    # "widgetSize":Ljava/lang/String;
    :goto_b
    return-void

    .line 264
    .restart local v4    # "select_day":I
    .restart local v5    # "select_month":I
    .restart local v6    # "select_year":I
    :cond_25
    const/4 v2, 0x0

    move-object/from16 v0, v41

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Z)V

    goto/16 :goto_7

    .line 272
    :cond_26
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v29

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 273
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v40

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 274
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, ""

    invoke-virtual {v2, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, v42

    invoke-static {v3, v0, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/CalendarUtils;->setSharedPreference(Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    .line 275
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mMonth:Ljava/util/Calendar;

    invoke-virtual {v2, v6, v5, v4}, Ljava/util/Calendar;->set(III)V

    goto/16 :goto_8

    .line 299
    .restart local v23    # "SpanX":I
    .restart local v24    # "SpanY":I
    .restart local v43    # "taskEvents":[Ljava/lang/String;
    .restart local v48    # "widgetSize":Ljava/lang/String;
    :cond_27
    const v2, 0x7f030002

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    goto/16 :goto_9

    .line 311
    .restart local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_28
    const/4 v2, 0x4

    move/from16 v0, v23

    if-ne v0, v2, :cond_29

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_29

    .line 312
    const v2, 0x7f0c0012

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 313
    const v2, 0x7f0c0014

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 314
    const v2, 0x7f0c0013

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 315
    const v2, 0x7f0c0015

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_a

    .line 317
    :cond_29
    const v2, 0x7f0c0012

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 318
    const v2, 0x7f0c0029

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 319
    const v2, 0x7f0c0028

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_a

    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_2a
    move-object/from16 v2, p0

    .line 335
    invoke-direct/range {v2 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->isSameDayTapped(Landroid/content/SharedPreferences;IIII)Z

    move-result v2

    if-eqz v2, :cond_2f

    .line 337
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v2, v8, :cond_2c

    const/4 v2, 0x2

    move/from16 v0, v23

    if-gt v0, v2, :cond_2b

    const/4 v2, 0x2

    move/from16 v0, v24

    if-le v0, v2, :cond_2c

    .line 338
    :cond_2b
    const v2, 0x7f030003

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    .line 342
    :goto_c
    new-instance v37, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    move-object/from16 v0, v37

    invoke-direct {v0, v2, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 344
    .restart local v37    # "remoteViews":Landroid/widget/RemoteViews;
    const/4 v2, 0x2

    move/from16 v0, v23

    if-ne v0, v2, :cond_2d

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_2d

    .line 345
    const v2, 0x7f0c0014

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 346
    const v2, 0x7f0c0029

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 347
    const v2, 0x7f0c0013

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 348
    const v2, 0x7f0c0028

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 349
    const v2, 0x7f0c0012

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 362
    :goto_d
    const v2, 0x7f0c0033

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 363
    const v2, 0x7f0c002d

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 364
    const v2, 0x7f0c0019

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 365
    const v2, 0x7f0c0031

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 366
    const v2, 0x7f0c002b

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 367
    const v2, 0x7f0c0017

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 368
    const v2, 0x7f0c002a

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 369
    const v2, 0x7f0c0016

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 370
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 371
    const v2, 0x7f0c002c

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 372
    const v2, 0x7f0c0018

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 373
    move-object/from16 v0, v37

    invoke-virtual {v10, v7, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    goto/16 :goto_b

    .line 340
    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_2c
    const v2, 0x7f030002

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    goto/16 :goto_c

    .line 350
    .restart local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_2d
    const/4 v2, 0x4

    move/from16 v0, v23

    if-ne v0, v2, :cond_2e

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_2e

    .line 351
    const v2, 0x7f0c0012

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 352
    const v2, 0x7f0c0014

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 353
    const v2, 0x7f0c0013

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 354
    const v2, 0x7f0c0015

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_d

    .line 356
    :cond_2e
    const v2, 0x7f0c0012

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 357
    const v2, 0x7f0c0029

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 358
    const v2, 0x7f0c0028

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_d

    .line 376
    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_2f
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v2

    iget v2, v2, Landroid/content/res/Configuration;->orientation:I

    const/4 v8, 0x2

    if-ne v2, v8, :cond_31

    const/4 v2, 0x2

    move/from16 v0, v23

    if-gt v0, v2, :cond_30

    const/4 v2, 0x2

    move/from16 v0, v24

    if-le v0, v2, :cond_31

    .line 377
    :cond_30
    const v2, 0x7f030003

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    .line 383
    :goto_e
    new-instance v37, Landroid/widget/RemoteViews;

    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageName()Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    move-object/from16 v0, v37

    invoke-direct {v0, v2, v8}, Landroid/widget/RemoteViews;-><init>(Ljava/lang/String;I)V

    .line 385
    .restart local v37    # "remoteViews":Landroid/widget/RemoteViews;
    const/4 v2, 0x2

    move/from16 v0, v23

    if-ne v0, v2, :cond_32

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_32

    .line 386
    const v2, 0x7f0c0014

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 387
    const v2, 0x7f0c0029

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 388
    const v2, 0x7f0c0013

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 389
    const v2, 0x7f0c0028

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 390
    const v2, 0x7f0c0012

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 402
    :goto_f
    const v2, 0x7f0c0031

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 403
    const v2, 0x7f0c002b

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 404
    const v2, 0x7f0c0017

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 405
    const v2, 0x7f0c002a

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 406
    const v2, 0x7f0c0016

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 407
    move-object/from16 v0, v37

    invoke-virtual {v10, v7, v0}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 408
    const v2, 0x7f0c0027

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 409
    const v2, 0x7f0c0032

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 410
    const v2, 0x7f0c002c

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 411
    const v2, 0x7f0c0018

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    goto/16 :goto_4

    .line 380
    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_31
    const v2, 0x7f030002

    move-object/from16 v0, p0

    iput v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mLayout:I

    goto/16 :goto_e

    .line 391
    .restart local v37    # "remoteViews":Landroid/widget/RemoteViews;
    :cond_32
    const/4 v2, 0x4

    move/from16 v0, v23

    if-ne v0, v2, :cond_33

    const/4 v2, 0x2

    move/from16 v0, v24

    if-ne v0, v2, :cond_33

    .line 392
    const v2, 0x7f0c0012

    const/16 v8, 0x8

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 393
    const v2, 0x7f0c0014

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 394
    const v2, 0x7f0c0013

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 395
    const v2, 0x7f0c0015

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_f

    .line 397
    :cond_33
    const v2, 0x7f0c0012

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 398
    const v2, 0x7f0c0029

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    .line 399
    const v2, 0x7f0c0028

    const/4 v8, 0x0

    move-object/from16 v0, v37

    invoke-virtual {v0, v2, v8}, Landroid/widget/RemoteViews;->setViewVisibility(II)V

    goto/16 :goto_f

    .line 414
    .end local v4    # "select_day":I
    .end local v5    # "select_month":I
    .end local v6    # "select_year":I
    .end local v23    # "SpanX":I
    .end local v24    # "SpanY":I
    .end local v37    # "remoteViews":Landroid/widget/RemoteViews;
    .end local v43    # "taskEvents":[Ljava/lang/String;
    .end local v48    # "widgetSize":Ljava/lang/String;
    :cond_34
    const-string v2, "ListItemClick"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_b

    .line 415
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v25

    .line 416
    .local v25, "b":Landroid/os/Bundle;
    if-eqz v25, :cond_35

    .line 417
    const-string v2, "TaskChecked"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_37

    .line 418
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    const-string v8, "id"

    move-object/from16 v0, v25

    invoke-virtual {v0, v8}, Landroid/os/Bundle;->getLong(Ljava/lang/String;)J

    move-result-wide v8

    invoke-static {v2, v8, v9}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v44

    .line 419
    .local v44, "uri":Landroid/net/Uri;
    new-instance v45, Landroid/content/ContentValues;

    const/4 v2, 0x1

    move-object/from16 v0, v45

    invoke-direct {v0, v2}, Landroid/content/ContentValues;-><init>(I)V

    .line 420
    .local v45, "values":Landroid/content/ContentValues;
    const-string v8, "complete"

    const-string v2, "isChecked"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_36

    const/4 v2, 0x0

    :goto_10
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    move-object/from16 v0, v45

    invoke-virtual {v0, v8, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 421
    const-string v2, "date_completed"

    new-instance v8, Ljava/util/Date;

    invoke-direct {v8}, Ljava/util/Date;-><init>()V

    invoke-virtual {v8}, Ljava/util/Date;->getTime()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v8

    move-object/from16 v0, v45

    invoke-virtual {v0, v2, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 422
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const/4 v8, 0x0

    const/4 v9, 0x0

    move-object/from16 v0, v44

    move-object/from16 v1, v45

    invoke-virtual {v2, v0, v1, v8, v9}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 431
    .end local v44    # "uri":Landroid/net/Uri;
    .end local v45    # "values":Landroid/content/ContentValues;
    :cond_35
    :goto_11
    const v2, 0x7f0c0018

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    goto/16 :goto_4

    .line 420
    .restart local v44    # "uri":Landroid/net/Uri;
    .restart local v45    # "values":Landroid/content/ContentValues;
    :cond_36
    const/4 v2, 0x1

    goto :goto_10

    .line 423
    .end local v44    # "uri":Landroid/net/Uri;
    .end local v45    # "values":Landroid/content/ContentValues;
    :cond_37
    const-string v2, "TaskItemClick"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_38

    .line 424
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getTaskItemFillInIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_11

    .line 425
    :cond_38
    const-string v2, "EventItemClick"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_39

    .line 426
    move-object/from16 v0, p0

    move-object/from16 v1, v25

    invoke-direct {v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getEventItemFillInIntent(Landroid/os/Bundle;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_11

    .line 427
    :cond_39
    const-string v2, "no_events"

    move-object/from16 v0, v25

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v2

    if-eqz v2, :cond_35

    .line 428
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mTime:Landroid/text/format/Time;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->getLaunchFillInIntent(Landroid/content/Context;Landroid/text/format/Time;)Landroid/content/Intent;

    move-result-object v2

    move-object/from16 v0, p1

    invoke-virtual {v0, v2}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_11

    .line 471
    .end local v25    # "b":Landroid/os/Bundle;
    .restart local v11    # "widgetIdSingle":I
    .restart local v22    # "PROVIDER_CHANGED":Z
    .restart local v26    # "cn":Landroid/content/ComponentName;
    .restart local v30    # "i":I
    .restart local v47    # "widgetIds":[I
    :cond_3a
    const-string v2, "MagazineAppWidgetProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "selectedDayKey = "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v39

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " daySelectedKey"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v29

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " selectedMonthKey"

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v40

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " selectedYearKey ="

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, v42

    invoke-virtual {v8, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 473
    const-string v2, "MagazineAppWidgetProvider"

    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    const-string v9, "selected_day - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_day:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, " selected_month - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_month:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    const-string v9, "selected_year - "

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v8

    move-object/from16 v0, p0

    iget v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->selected_year:I

    invoke-virtual {v8, v9}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v8

    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    invoke-static {v2, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 491
    .end local v30    # "i":I
    :cond_3b
    :goto_12
    invoke-super/range {p0 .. p2}, Landroid/appwidget/AppWidgetProvider;->onReceive(Landroid/content/Context;Landroid/content/Intent;)V

    goto/16 :goto_b

    .line 475
    :cond_3c
    if-lez v7, :cond_3d

    const-string v2, "android.intent.action.TIME_SET"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    const-string v2, "android.intent.action.TIMEZONE_CHANGED"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    const-string v2, "android.appwidget.action.APPWIDGET_UPDATE"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    const-string v2, "android.intent.action.DATE_CHANGED"

    invoke-virtual {v14, v2}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_3d

    invoke-static/range {p1 .. p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getWidgetUpdateAction(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2, v14}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_3d

    move-object/from16 v15, p0

    move-object/from16 v16, p1

    move-object/from16 v17, v10

    move/from16 v18, v7

    move/from16 v19, v12

    move/from16 v20, v13

    move-object/from16 v21, v14

    .line 478
    invoke-direct/range {v15 .. v21}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IIILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v10, v7, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 479
    const/4 v2, 0x0

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->resetAll:Z

    goto :goto_12

    .line 482
    :cond_3d
    const/16 v30, 0x0

    .restart local v30    # "i":I
    :goto_13
    move-object/from16 v0, v47

    array-length v2, v0

    move/from16 v0, v30

    if-ge v0, v2, :cond_3b

    .line 483
    aget v11, v47, v30

    .line 484
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v0, v1, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->setTodayPref(Landroid/content/Context;I)V

    move-object/from16 v8, p0

    move-object/from16 v9, p1

    .line 485
    invoke-direct/range {v8 .. v14}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->updateViews(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;IIILjava/lang/String;)Landroid/widget/RemoteViews;

    move-result-object v2

    invoke-virtual {v10, v11, v2}, Landroid/appwidget/AppWidgetManager;->updateAppWidget(ILandroid/widget/RemoteViews;)V

    .line 486
    const v2, 0x7f0c0027

    invoke-virtual {v10, v11, v2}, Landroid/appwidget/AppWidgetManager;->notifyAppWidgetViewDataChanged(II)V

    .line 487
    const/4 v2, 0x1

    sput-boolean v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->resetAll:Z

    .line 482
    add-int/lit8 v30, v30, 0x1

    goto :goto_13
.end method

.method public onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "appWidgetManager"    # Landroid/appwidget/AppWidgetManager;
    .param p3, "appWidgetIds"    # [I

    .prologue
    .line 598
    invoke-super {p0, p1, p2, p3}, Landroid/appwidget/AppWidgetProvider;->onUpdate(Landroid/content/Context;Landroid/appwidget/AppWidgetManager;[I)V

    .line 600
    invoke-virtual {p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->mContentResolver:Landroid/content/ContentResolver;

    .line 601
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/magazinewidget/MagazineAppWidgetProvider;->registerDateFormatObserver()V

    .line 602
    return-void
.end method
