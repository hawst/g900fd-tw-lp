.class public abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;
.source "BaseMonthThemeImple.java"


# static fields
.field protected static final SELECTION_HIDDEN:I = 0x0

.field protected static final SELECTION_PRESSED:I = 0x1

.field protected static sCurrentTzOnPause:Ljava/lang/String;

.field protected static sOutOfBoundToast:Landroid/widget/Toast;


# instance fields
.field protected final CALENDAR_STICKER_ENABLED:Z

.field protected isFontStyleDefault:Z

.field protected mCurrentDateOnPause:Ljava/lang/String;

.field protected mCurrentDateOnResume:Ljava/lang/String;

.field protected mDateContainer:Landroid/widget/LinearLayout;

.field protected mDay0:Landroid/widget/TextView;

.field protected mDay1:Landroid/widget/TextView;

.field protected mDay2:Landroid/widget/TextView;

.field protected mDay3:Landroid/widget/TextView;

.field protected mDay4:Landroid/widget/TextView;

.field protected mDay5:Landroid/widget/TextView;

.field protected mDay6:Landroid/widget/TextView;

.field protected mGoToday:Landroid/widget/Button;

.field protected mHandler:Landroid/os/Handler;

.field protected mInitialized:Z

.field protected mIsSupportSPenUsp:Z

.field protected mIsTabletConfig:Z

.field protected mIsXhdpiConfig:Z

.field protected mMonthDayColor:I

.field protected mMonthSwitcher:Landroid/widget/ViewSwitcher;

.field protected mMonthTitle:Landroid/widget/TextView;

.field protected mNextMonth:Landroid/widget/ImageButton;

.field protected mPrevMonth:Landroid/widget/ImageButton;

.field protected mStartDay:I

.field protected mSwitcher:Landroid/widget/ViewSwitcher;

.field protected mTime:Landroid/text/format/Time;

.field protected mYearFormat:Ljava/lang/String;

.field protected mYearTitle:Landroid/widget/TextView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 39
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->sCurrentTzOnPause:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 4
    .param p1, "sPlannerSurfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    .line 79
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 63
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->CALENDAR_STICKER_ENABLED:Z

    .line 65
    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->isFontStyleDefault:Z

    .line 82
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mHandler:Landroid/os/Handler;

    .line 83
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v2, 0x7f060002

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mIsTabletConfig:Z

    .line 84
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v2, 0x7f060001

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mIsXhdpiConfig:Z

    .line 85
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v2, 0x7f0a006d

    invoke-virtual {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getString(I)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mYearFormat:Ljava/lang/String;

    .line 86
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mContext:Landroid/content/Context;

    invoke-static {v1, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v1

    sput-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->sCurrentTzOnPause:Ljava/lang/String;

    .line 87
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f07001c

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mMonthDayColor:I

    .line 88
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isSupportSPenUsp(Landroid/content/Context;)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mIsSupportSPenUsp:Z

    .line 89
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getFirstDayOfWeek(Landroid/content/Context;)I

    move-result v1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mStartDay:I

    .line 90
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mContext:Landroid/content/Context;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mTime:Landroid/text/format/Time;

    .line 91
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mTime:Landroid/text/format/Time;

    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 92
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getSettingsFontStyle(Landroid/content/Context;)I

    move-result v0

    .line 93
    .local v0, "fontIndexDB":I
    if-eqz v0, :cond_0

    .line 94
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/BaseMonthThemeImple;->isFontStyleDefault:Z

    .line 96
    :cond_0
    return-void
.end method
