.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$drawable;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "drawable"
.end annotation


# static fields
.field public static final app_icon:I = 0x7f020000

.field public static final app_icon_dcm:I = 0x7f020001

.field public static final cal_slected:I = 0x7f020002

.field public static final cal_slected_today:I = 0x7f020003

.field public static final cal_today:I = 0x7f020004

.field public static final calendar_list_icon_alarm:I = 0x7f020005

.field public static final calendar_list_icon_repeat:I = 0x7f020006

.field public static final calendar_view_selector:I = 0x7f020007

.field public static final calendar_widget_icon_facebook:I = 0x7f020008

.field public static final day_view_selector:I = 0x7f020009

.field public static final easy_date_0:I = 0x7f02000a

.field public static final easy_date_1:I = 0x7f02000b

.field public static final easy_date_2:I = 0x7f02000c

.field public static final easy_date_3:I = 0x7f02000d

.field public static final easy_date_4:I = 0x7f02000e

.field public static final easy_date_5:I = 0x7f02000f

.field public static final easy_date_6:I = 0x7f020010

.field public static final easy_date_7:I = 0x7f020011

.field public static final easy_date_8:I = 0x7f020012

.field public static final easy_date_9:I = 0x7f020013

.field public static final easy_widget_bg:I = 0x7f020014

.field public static final event_list_check_off:I = 0x7f020015

.field public static final event_list_check_on:I = 0x7f020016

.field public static final festival_effect_month_border:I = 0x7f020017

.field public static final festival_effect_working:I = 0x7f020018

.field public static final list_ic_clock:I = 0x7f020019

.field public static final list_ic_clock_month:I = 0x7f02001a

.field public static final list_icon_priority:I = 0x7f02001b

.field public static final list_icon_priority_low:I = 0x7f02001c

.field public static final magazine_bg:I = 0x7f020056

.field public static final magazine_list_divider:I = 0x7f020057

.field public static final magazine_week_divider:I = 0x7f020058

.field public static final mg_cal_flip_left:I = 0x7f02001d

.field public static final mg_cal_flip_right:I = 0x7f02001e

.field public static final mg_cal_noitem_bg:I = 0x7f02001f

.field public static final mg_calendar_w:I = 0x7f020020

.field public static final mg_calendar_w_press:I = 0x7f020021

.field public static final mg_today_w:I = 0x7f020022

.field public static final mg_today_w_press:I = 0x7f020023

.field public static final minitoday_preview:I = 0x7f020024

.field public static final month_memo_icon:I = 0x7f020025

.field public static final month_preview:I = 0x7f020026

.field public static final splanner_mini_list_check_normal:I = 0x7f020027

.field public static final splanner_mini_list_check_select:I = 0x7f020028

.field public static final splanner_mini_title_ic_next:I = 0x7f020029

.field public static final splanner_mini_title_ic_pre:I = 0x7f02002a

.field public static final splanner_month_list_check_normal:I = 0x7f02002b

.field public static final splanner_month_list_check_select:I = 0x7f02002c

.field public static final splanner_month_view_check_normal:I = 0x7f02002d

.field public static final splanner_month_view_check_select:I = 0x7f02002e

.field public static final splanner_title_divider:I = 0x7f02002f

.field public static final splanner_title_ic_add:I = 0x7f020030

.field public static final splanner_title_ic_next:I = 0x7f020031

.field public static final splanner_title_ic_pre:I = 0x7f020032

.field public static final splanner_title_pressed:I = 0x7f020033

.field public static final today_time_selector:I = 0x7f020034

.field public static final today_title_dim_selector:I = 0x7f020035

.field public static final today_title_selector:I = 0x7f020036

.field public static final tw_scrollbar_handle_holo_light:I = 0x7f020037

.field public static final weather_01:I = 0x7f020038

.field public static final weather_02:I = 0x7f020039

.field public static final weather_03:I = 0x7f02003a

.field public static final weather_04:I = 0x7f02003b

.field public static final weather_05:I = 0x7f02003c

.field public static final weather_06:I = 0x7f02003d

.field public static final weather_07:I = 0x7f02003e

.field public static final weather_08:I = 0x7f02003f

.field public static final weather_09:I = 0x7f020040

.field public static final weather_10:I = 0x7f020041

.field public static final weather_11:I = 0x7f020042

.field public static final weather_12:I = 0x7f020043

.field public static final weather_13:I = 0x7f020044

.field public static final weather_14:I = 0x7f020045

.field public static final weather_15:I = 0x7f020046

.field public static final weather_16:I = 0x7f020047

.field public static final weather_17:I = 0x7f020048

.field public static final weather_18:I = 0x7f020049

.field public static final weather_19:I = 0x7f02004a

.field public static final weather_20:I = 0x7f02004b

.field public static final weather_39:I = 0x7f02004c

.field public static final weather_40:I = 0x7f02004d

.field public static final weather_41:I = 0x7f02004e

.field public static final widget_btn_ripple:I = 0x7f02004f

.field public static final widget_innerline:I = 0x7f020050

.field public static final widget_list_bg_selector:I = 0x7f020051

.field public static final widget_planner_ic_handwriting:I = 0x7f020052

.field public static final widget_shadow:I = 0x7f020053

.field public static final widgetheader_add_btn_selector:I = 0x7f020054

.field public static final widgetheader_button_selector:I = 0x7f020055


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 337
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
