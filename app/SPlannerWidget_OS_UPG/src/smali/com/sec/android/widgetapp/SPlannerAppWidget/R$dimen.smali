.class public final Lcom/sec/android/widgetapp/SPlannerAppWidget/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final add_btn_margin_left:I = 0x7f080000

.field public static final add_btn_width:I = 0x7f080001

.field public static final add_button_margine:I = 0x7f080002

.field public static final agenda_date_text_size:I = 0x7f080003

.field public static final agenda_event_item_body_margin_left:I = 0x7f080004

.field public static final agenda_event_item_height:I = 0x7f080005

.field public static final agenda_event_item_title_text_size:I = 0x7f080006

.field public static final agenda_event_item_when_text_size:I = 0x7f080007

.field public static final agenda_location_margin_right:I = 0x7f080008

.field public static final agenda_noevent_text_size:I = 0x7f080009

.field public static final agenda_today_text_size:I = 0x7f08000a

.field public static final appwidget_easy_body_slash_margin:I = 0x7f08000b

.field public static final appwidget_easy_body_text_size:I = 0x7f08000c

.field public static final appwidget_easy_date_height:I = 0x7f08000d

.field public static final appwidget_easy_date_margin_bottom:I = 0x7f08000e

.field public static final appwidget_easy_date_margin_top:I = 0x7f08000f

.field public static final appwidget_easy_header_text_size:I = 0x7f080010

.field public static final appwidget_easy_height:I = 0x7f080011

.field public static final appwidget_easy_minHeight:I = 0x7f080012

.field public static final appwidget_easy_minWidth:I = 0x7f080013

.field public static final appwidget_easy_title_height:I = 0x7f080014

.field public static final appwidget_easy_title_text_size:I = 0x7f080015

.field public static final appwidget_easy_width:I = 0x7f080016

.field public static final checkbox_margin_left:I = 0x7f080017

.field public static final checkbox_margin_right:I = 0x7f080018

.field public static final checkbox_padding_all:I = 0x7f080019

.field public static final checkbox_size:I = 0x7f08001a

.field public static final color_bar_margin_right:I = 0x7f08001b

.field public static final date_container_width:I = 0x7f08001c

.field public static final date_margin_left:I = 0x7f08001d

.field public static final day_event_count_text_size:I = 0x7f08001e

.field public static final day_event_count_view_height:I = 0x7f08001f

.field public static final day_event_text_size:I = 0x7f080020

.field public static final day_header_height:I = 0x7f080021

.field public static final day_header_text_size:I = 0x7f080022

.field public static final day_time_text_size:I = 0x7f080023

.field public static final event_box_margin_bottom:I = 0x7f080024

.field public static final event_box_margin_bottom_with_lunar:I = 0x7f080025

.field public static final event_list_height:I = 0x7f080026

.field public static final event_list_width:I = 0x7f080027

.field public static final handwriting_btn_width:I = 0x7f080028

.field public static final header_btn_height:I = 0x7f080029

.field public static final header_btn_margin_top:I = 0x7f08002a

.field public static final header_shadow_height:I = 0x7f08002b

.field public static final header_shadow_height_month:I = 0x7f08002c

.field public static final header_today_height:I = 0x7f08002d

.field public static final header_today_width:I = 0x7f08002e

.field public static final left_btn_margin_left:I = 0x7f08002f

.field public static final list_item_height:I = 0x7f080030

.field public static final location_width:I = 0x7f080031

.field public static final lunar_date_y_offset:I = 0x7f080032

.field public static final mg_week_day_label_height:I = 0x7f080033

.field public static final mg_week_day_label_width:I = 0x7f080034

.field public static final mg_weekday_label_width:I = 0x7f0800c8

.field public static final mini_today_arrow_top_margine:I = 0x7f080035

.field public static final mini_today_label_left_margine:I = 0x7f080036

.field public static final minit_widget_label_text_size:I = 0x7f080037

.field public static final month_account_color_bar_margin_bottom:I = 0x7f080038

.field public static final month_account_color_bar_margin_right:I = 0x7f080039

.field public static final month_account_color_bar_margin_top:I = 0x7f08003a

.field public static final month_account_color_bar_width:I = 0x7f08003b

.field public static final month_body_margin_left:I = 0x7f08003c

.field public static final month_btn_margin_left:I = 0x7f08003d

.field public static final month_btn_top:I = 0x7f08003e

.field public static final month_cell_width:I = 0x7f08003f

.field public static final month_checkbox_height:I = 0x7f080040

.field public static final month_checkbox_margin_top:I = 0x7f080041

.field public static final month_checkbox_width:I = 0x7f080042

.field public static final month_count_size_with_lunar:I = 0x7f080043

.field public static final month_date_container_margin_top:I = 0x7f080044

.field public static final month_event_body_height:I = 0x7f0800bf

.field public static final month_event_box_height:I = 0x7f080045

.field public static final month_event_box_interval:I = 0x7f080046

.field public static final month_event_list_devider_width:I = 0x7f080047

.field public static final month_event_list_item_height:I = 0x7f080048

.field public static final month_event_list_margin_left:I = 0x7f080049

.field public static final month_event_margin_left:I = 0x7f08004a

.field public static final month_event_noevent_margin_left:I = 0x7f08004b

.field public static final month_event_title_height:I = 0x7f0800c0

.field public static final month_event_title_margin_top:I = 0x7f08004c

.field public static final month_event_title_text_size:I = 0x7f0800c1

.field public static final month_eventcount_bg_height:I = 0x7f08004d

.field public static final month_eventcount_bg_oval_radius:I = 0x7f08004e

.field public static final month_eventcount_position_right:I = 0x7f08004f

.field public static final month_first_column_cell_width:I = 0x7f080050

.field public static final month_icon_top:I = 0x7f080051

.field public static final month_line_width:I = 0x7f080052

.field public static final month_list_item_alarm_icon_size:I = 0x7f080053

.field public static final month_list_item_checkbox_padding_right:I = 0x7f080054

.field public static final month_list_item_event_sticker_size:I = 0x7f080055

.field public static final month_list_item_padding_Right:I = 0x7f080056

.field public static final month_list_item_padding_left:I = 0x7f080057

.field public static final month_list_item_repeat_icon_size:I = 0x7f080058

.field public static final month_list_item_task_pariority_size:I = 0x7f0800c2

.field public static final month_list_item_task_priority_size:I = 0x7f080059

.field public static final month_list_item_time_padding_right:I = 0x7f08005a

.field public static final month_list_item_time_text_size:I = 0x7f08005b

.field public static final month_list_item_title_text_size:I = 0x7f08005c

.field public static final month_lunar_date_text_size:I = 0x7f08005d

.field public static final month_memo_icon_height:I = 0x7f08005e

.field public static final month_memo_icon_width:I = 0x7f08005f

.field public static final month_move_btn_height:I = 0x7f0800c3

.field public static final month_move_btn_margin_left:I = 0x7f080060

.field public static final month_move_btn_margin_right:I = 0x7f080061

.field public static final month_move_btn_margin_top:I = 0x7f0800c4

.field public static final month_move_btn_text_size:I = 0x7f0800c5

.field public static final month_move_btn_width:I = 0x7f080062

.field public static final month_next_btn_margin_right:I = 0x7f080063

.field public static final month_next_button_left_margine:I = 0x7f080064

.field public static final month_no_event_text_margin_left:I = 0x7f080065

.field public static final month_no_event_text_margin_right:I = 0x7f080066

.field public static final month_no_event_text_size:I = 0x7f080067

.field public static final month_prev_btn_margin_left:I = 0x7f080068

.field public static final month_prev_button_left_margine:I = 0x7f080069

.field public static final month_prev_button_right_margine:I = 0x7f08006a

.field public static final month_prev_margine_left:I = 0x7f08006b

.field public static final month_sticker_size:I = 0x7f08006c

.field public static final month_theme_next_btn_height:I = 0x7f08006d

.field public static final month_theme_prev_btn_width:I = 0x7f08006e

.field public static final month_title_height:I = 0x7f08006f

.field public static final month_title_margine_left:I = 0x7f080070

.field public static final month_title_margine_right:I = 0x7f080071

.field public static final month_today_icon_width_adjust:I = 0x7f080072

.field public static final month_today_marker_size:I = 0x7f080073

.field public static final month_view_label_margin_top:I = 0x7f080074

.field public static final month_view_layout_margin_right:I = 0x7f080075

.field public static final month_view_widget_title_width:I = 0x7f080076

.field public static final month_weather_top_margine:I = 0x7f080077

.field public static final month_week_height:I = 0x7f080078

.field public static final month_week_label_padding_left:I = 0x7f080079

.field public static final month_week_label_width:I = 0x7f08007a

.field public static final month_week_margin_left:I = 0x7f08007b

.field public static final month_week_margin_top:I = 0x7f08007c

.field public static final month_week_text_size:I = 0x7f08007d

.field public static final month_width:I = 0x7f08007e

.field public static final no_event_container_height:I = 0x7f08007f

.field public static final no_event_icon_margin_left:I = 0x7f080080

.field public static final no_event_icon_margin_top:I = 0x7f080081

.field public static final no_event_margin_top:I = 0x7f080082

.field public static final no_event_text_size:I = 0x7f080083

.field public static final noevent_margin_left:I = 0x7f080084

.field public static final prev_btn_margin_left:I = 0x7f0800c6

.field public static final prev_btn_margin_right:I = 0x7f0800c7

.field public static final prev_btn_padding_left:I = 0x7f080085

.field public static final prev_btn_padding_right:I = 0x7f080086

.field public static final right_btn_margin_left:I = 0x7f080087

.field public static final sticker_image_size:I = 0x7f080088

.field public static final task_title_margin_left:I = 0x7f0800bd

.field public static final title_day_text:I = 0x7f080089

.field public static final title_height:I = 0x7f08008a

.field public static final title_margin_left:I = 0x7f08008b

.field public static final title_margin_top:I = 0x7f08008c

.field public static final title_text_size:I = 0x7f08008d

.field public static final title_width:I = 0x7f08008e

.field public static final today_body_top_margin:I = 0x7f08008f

.field public static final today_btn_margin_left:I = 0x7f080090

.field public static final today_button_default_minwidth:I = 0x7f080091

.field public static final today_button_default_padding:I = 0x7f080092

.field public static final today_date_container_height:I = 0x7f080093

.field public static final today_divider_margine_left:I = 0x7f080094

.field public static final today_list_height:I = 0x7f080095

.field public static final today_list_item_height:I = 0x7f080096

.field public static final today_move_btn_container_width:I = 0x7f080097

.field public static final today_no_event_text_margin:I = 0x7f0800be

.field public static final today_padding_bottom:I = 0x7f080098

.field public static final today_padding_top:I = 0x7f080099

.field public static final trans_title_day_text:I = 0x7f08009a

.field public static final weather_city_info_padding_left:I = 0x7f08009b

.field public static final weather_city_info_padding_right:I = 0x7f08009c

.field public static final weather_image_margin_left:I = 0x7f08009d

.field public static final weather_image_margin_left_monthview:I = 0x7f08009e

.field public static final weather_image_width_size:I = 0x7f08009f

.field public static final weather_info_container_height:I = 0x7f0800a0

.field public static final weather_temp_margin_right:I = 0x7f0800a1

.field public static final widget_day_num_top_padding:I = 0x7f0800a2

.field public static final widget_header_bottom_margin:I = 0x7f0800a3

.field public static final widget_header_height:I = 0x7f0800a4

.field public static final widget_header_line_height:I = 0x7f0800a5

.field public static final widget_header_line_margin_top:I = 0x7f0800a6

.field public static final widget_header_line_width:I = 0x7f0800a7

.field public static final widget_header_shadow_margin_left:I = 0x7f0800a8

.field public static final widget_header_shadow_margin_right:I = 0x7f0800a9

.field public static final widget_header_top_margin:I = 0x7f0800aa

.field public static final widget_height_default_minitoday:I = 0x7f0800ab

.field public static final widget_height_default_month:I = 0x7f0800ac

.field public static final widget_height_default_trans_month:I = 0x7f0800ad

.field public static final widget_height_max_minitoday:I = 0x7f0800ae

.field public static final widget_height_max_month:I = 0x7f0800af

.field public static final widget_height_min_minitoday:I = 0x7f0800b0

.field public static final widget_height_min_month:I = 0x7f0800b1

.field public static final widget_label_margin_left:I = 0x7f0800b2

.field public static final widget_label_padding_top:I = 0x7f0800b3

.field public static final widget_label_text_size:I = 0x7f0800b4

.field public static final widget_label_today_text_size:I = 0x7f0800b5

.field public static final widget_label_width:I = 0x7f0800b6

.field public static final widget_width_default_minitoday:I = 0x7f0800b7

.field public static final widget_width_default_month:I = 0x7f0800b8

.field public static final widgetheader_arrow_btn_height:I = 0x7f0800b9

.field public static final widgetheader_arrow_btn_margin_right:I = 0x7f0800ba

.field public static final widgetheader_arrow_btn_margin_top:I = 0x7f0800bb

.field public static final widgetheader_arrow_btn_width:I = 0x7f0800bc


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 134
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method
