.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;
.super Ljava/lang/Object;
.source "Feature.java"


# static fields
.field public static final ACTION_EASY_MODE_CHANGED:Ljava/lang/String; = "com.android.launcher.action.EASY_MODE_CHANGE"

.field public static final EASY_MODE_SWITCH:Ljava/lang/String; = "easy_mode_switch"

.field public static final FEATURE_SPEN_USP:Ljava/lang/String; = "com.sec.feature.spen_usp"

.field public static final FLIP_FONT_STYLE:Ljava/lang/String; = "flip_font_style"

.field public static final FLIP_FONT_STYLE_DEFAULT:I = 0x0

.field public static final STICKER_ENABLED:Z = true

.field public static final USE_LOCATION_URI:Landroid/net/Uri;

.field public static final WEATHER_ENABLED:Z = true

.field private static sHolidayCountry:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 41
    const-string v0, "content://com.sec.android.daemonapp.ap.accuweather.provider/settings"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->USE_LOCATION_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static getCscCountryCode()Ljava/lang/String;
    .locals 1

    .prologue
    .line 116
    const-string v0, "ro.csc.country_code"

    invoke-static {v0}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCscString(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p0, "featureName"    # Ljava/lang/String;

    .prologue
    .line 46
    invoke-static {}, Lcom/sec/android/app/CscFeature;->getInstance()Lcom/sec/android/app/CscFeature;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/sec/android/app/CscFeature;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getHolidayCountry()Ljava/lang/String;
    .locals 1

    .prologue
    .line 52
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->sHolidayCountry:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 53
    const-string v0, "CscFeature_Calendar_EnableLocalHolidayDisplay"

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getCscString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->sHolidayCountry:Ljava/lang/String;

    .line 55
    :cond_0
    sget-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->sHolidayCountry:Ljava/lang/String;

    return-object v0
.end method

.method public static getLiveWallPaperWeatherwallCpName()Ljava/lang/String;
    .locals 1

    .prologue
    .line 120
    const-string v0, "CscFeature_LiveWallpaper_WeatherWallCPName"

    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getCscString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public static getSettingsFontStyle(Landroid/content/Context;)I
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 124
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    const-string v1, "flip_font_style"

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Landroid/provider/Settings$Global;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v0

    return v0
.end method

.method private static hasFeature(Landroid/content/Context;Ljava/lang/String;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "feature"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/pm/PackageManager;->hasSystemFeature(Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isAccuweather2014(Landroid/content/Context;)Z
    .locals 10
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v8, 0x1

    const/4 v9, 0x0

    .line 63
    const/4 v6, 0x0

    .line 65
    .local v6, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    sget-object v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->USE_LOCATION_URI:Landroid/net/Uri;

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/String;

    const/4 v3, 0x0

    const-string v4, "SHOW_USE_LOCATION_POPUP"

    aput-object v4, v2, v3

    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v6

    .line 69
    if-eqz v6, :cond_0

    .line 70
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    :cond_0
    move v0, v8

    .line 72
    :goto_0
    return v0

    .line 66
    :catch_0
    move-exception v7

    .local v7, "exception":Ljava/lang/IllegalArgumentException;
    move v0, v9

    .line 67
    goto :goto_0
.end method

.method public static isChinaFeature()Z
    .locals 2

    .prologue
    .line 85
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isEasyMode(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    .line 128
    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "easy_mode_switch"

    invoke-static {v1, v2, v0}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    if-nez v1, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isHKTWFeature()Z
    .locals 2

    .prologue
    .line 89
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "HKTW"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isLocalHolidayColorDisplay()Z
    .locals 2

    .prologue
    .line 98
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "KOREA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "CHINA"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "JAPAN"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isSupportSPenUsp(Landroid/content/Context;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 103
    const-string v0, "com.sec.feature.spen_usp"

    invoke-static {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->hasFeature(Landroid/content/Context;Ljava/lang/String;)Z

    move-result v0

    return v0
.end method

.method public static isUsaOrCanada()Z
    .locals 2

    .prologue
    .line 111
    const-string v0, "USA"

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getCscCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "CAN"

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getCscCountryCode()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isVietNameseFeature()Z
    .locals 2

    .prologue
    .line 93
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getHolidayCountry()Ljava/lang/String;

    move-result-object v0

    const-string v1, "VI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    return v0
.end method

.method public static isWeatherEnabled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 76
    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isAccuweather2014(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 77
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherSupported()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getUseLocationStatus(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchWeatherOnState(Landroid/content/Context;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 81
    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    .line 77
    goto :goto_0

    .line 81
    :cond_2
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherSupported()Z

    move-result v2

    if-eqz v2, :cond_3

    invoke-static {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->fetchWeatherOnState(Landroid/content/Context;)Z

    move-result v2

    if-nez v2, :cond_0

    :cond_3
    move v0, v1

    goto :goto_0
.end method

.method public static isWeatherSupported()Z
    .locals 1

    .prologue
    .line 59
    const/4 v0, 0x1

    return v0
.end method
