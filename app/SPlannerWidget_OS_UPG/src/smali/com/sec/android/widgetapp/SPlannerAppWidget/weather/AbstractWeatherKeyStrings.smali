.class public abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;
.super Ljava/lang/Object;
.source "AbstractWeatherKeyStrings.java"


# instance fields
.field public ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

.field public ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

.field public ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

.field public ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

.field public CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

.field public CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

.field public WEATHER_URI:Landroid/net/Uri;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;->initKeyStrings()V

    .line 18
    return-void
.end method


# virtual methods
.method public abstract initKeyStrings()V
.end method
