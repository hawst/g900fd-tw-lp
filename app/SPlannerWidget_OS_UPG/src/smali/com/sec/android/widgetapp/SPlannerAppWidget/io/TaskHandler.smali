.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;
.super Ljava/lang/Object;
.source "TaskHandler.java"

# interfaces
.implements Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils$IAccountTypeConstants;


# static fields
.field public static final TASK_PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 32
    const/16 v0, 0xd

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "utc_due_date"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "reminder_set"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "complete"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "importance"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "subject"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "_sync_account"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "accountName"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "groupName"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "accountKey"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "body"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->TASK_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 48
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 49
    return-void
.end method

.method public static canModify(Ljava/lang/String;)Z
    .locals 1
    .param p0, "accountName"    # Ljava/lang/String;

    .prologue
    .line 80
    const-string v0, "My Task (personal)"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "My task (KNOX)"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 81
    :cond_0
    const/4 v0, 0x0

    .line 83
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public static constructTask(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/ArrayList;)V
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "c"    # Landroid/database/Cursor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/database/Cursor;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 52
    if-eqz p1, :cond_0

    invoke-interface {p1}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v3

    if-nez v3, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 55
    :cond_1
    invoke-virtual {p2}, Ljava/util/ArrayList;->clear()V

    .line 56
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    .line 59
    .local v1, "res":Landroid/content/res/Resources;
    :cond_2
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;

    invoke-direct {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;-><init>()V

    .line 60
    .local v2, "task":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;
    const-string v3, "_id"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->id:J

    .line 61
    const-string v3, "due_date"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    iput-wide v6, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->dueDate:J

    .line 62
    const-string v3, "reminder_set"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_5

    move v3, v4

    :goto_1
    iput-boolean v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->reminderSet:Z

    .line 63
    const-string v3, "complete"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    if-eqz v3, :cond_6

    move v3, v4

    :goto_2
    iput-boolean v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->complete:Z

    .line 64
    const-string v3, "importance"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->importance:I

    .line 65
    const-string v3, "subject"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    .line 66
    const-string v3, "accountName"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountName:Ljava/lang/String;

    .line 67
    const-string v3, "accountKey"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v3

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    iput v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->accountKey:I

    .line 69
    iget-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    if-eqz v3, :cond_3

    iget-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    invoke-virtual {v3}, Ljava/lang/String;->length()I

    move-result v3

    if-nez v3, :cond_4

    .line 70
    :cond_3
    const v3, 0x7f0a0058

    invoke-virtual {v1, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->subject:Ljava/lang/String;

    .line 73
    :cond_4
    const-string v3, "accountKey"

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    .line 74
    .local v0, "colIndex":I
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    invoke-static {p0, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskAccountHandler;->getTaskColor(Landroid/content/Context;I)I

    move-result v3

    iput v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;->color:I

    .line 75
    invoke-virtual {p2, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 76
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v3

    if-nez v3, :cond_2

    goto/16 :goto_0

    .end local v0    # "colIndex":I
    :cond_5
    move v3, v5

    .line 62
    goto :goto_1

    :cond_6
    move v3, v5

    .line 63
    goto :goto_2
.end method
