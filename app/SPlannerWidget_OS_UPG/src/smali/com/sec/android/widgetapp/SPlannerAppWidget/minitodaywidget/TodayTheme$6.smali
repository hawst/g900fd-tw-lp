.class Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;
.super Ljava/lang/Object;
.source "TodayTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V
    .locals 0

    .prologue
    .line 210
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 9
    .param p1, "v"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 215
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v1

    .line 216
    .local v1, "isUsedSystemTimezone":Z
    if-eqz v1, :cond_1

    .line 217
    new-instance v3, Landroid/text/format/Time;

    invoke-direct {v3}, Landroid/text/format/Time;-><init>()V

    .line 221
    .local v3, "todayTime":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v3}, Landroid/text/format/Time;->setToNow()V

    .line 223
    if-eqz v1, :cond_0

    .line 224
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    iput-object v4, v3, Landroid/text/format/Time;->timezone:Ljava/lang/String;

    .line 225
    invoke-virtual {v3, v8}, Landroid/text/format/Time;->normalize(Z)J

    .line 228
    :cond_0
    invoke-virtual {v3, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-wide v6, v3, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v2

    .line 229
    .local v2, "todayJulianDay":I
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v4, v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    invoke-virtual {v4, v8}, Landroid/text/format/Time;->toMillis(Z)J

    move-result-wide v4

    iget-object v6, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v6, v6, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    iget-wide v6, v6, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v4, v5, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v0

    .line 231
    .local v0, "currentViewTime":I
    if-ne v2, v0, :cond_2

    .line 240
    :goto_1
    return-void

    .line 219
    .end local v0    # "currentViewTime":I
    .end local v2    # "todayJulianDay":I
    .end local v3    # "todayTime":Landroid/text/format/Time;
    :cond_1
    new-instance v3, Landroid/text/format/Time;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v3    # "todayTime":Landroid/text/format/Time;
    goto :goto_0

    .line 235
    .restart local v0    # "currentViewTime":I
    .restart local v2    # "todayJulianDay":I
    :cond_2
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iput-object v3, v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    .line 236
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v5, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v5, v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V
    invoke-static {v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Landroid/text/format/Time;)V

    .line 237
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadTasks()V

    .line 238
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$6;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadCurrentDayEvents()V

    goto :goto_1
.end method
