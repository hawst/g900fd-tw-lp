.class Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;
.super Ljava/lang/Object;
.source "TodayTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->initialize(Landroid/view/View;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)V
    .locals 0

    .prologue
    .line 163
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 166
    new-instance v2, Landroid/text/format/Time;

    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$300(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v3

    const/4 v4, 0x0

    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 167
    .local v2, "prevDay":Landroid/text/format/Time;
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v2

    .line 169
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    .line 170
    .local v0, "millis":J
    const-wide/16 v4, -0x1

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    iget-wide v4, v2, Landroid/text/format/Time;->gmtoff:J

    invoke-static {v0, v1, v4, v5}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getJulianDay(JJ)I

    move-result v3

    const v4, 0x24dc87

    if-gt v3, v4, :cond_1

    .line 180
    :cond_0
    :goto_0
    return-void

    .line 175
    :cond_1
    iget v3, v2, Landroid/text/format/Time;->monthDay:I

    add-int/lit8 v3, v3, -0x1

    iput v3, v2, Landroid/text/format/Time;->monthDay:I

    .line 176
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iput-object v2, v3, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    .line 177
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget-object v4, v4, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mTime:Landroid/text/format/Time;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->updateDayTitle(Landroid/text/format/Time;)V
    invoke-static {v3, v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$400(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;Landroid/text/format/Time;)V

    .line 178
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadCurrentDayEvents()V

    .line 179
    iget-object v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$3;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-virtual {v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->reloadTasks()V

    goto :goto_0
.end method
