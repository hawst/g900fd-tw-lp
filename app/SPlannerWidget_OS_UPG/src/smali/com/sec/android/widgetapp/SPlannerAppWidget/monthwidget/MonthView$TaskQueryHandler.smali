.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;
.super Landroid/content/AsyncQueryHandler;
.source "MonthView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "TaskQueryHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/content/ContentResolver;)V
    .locals 0
    .param p2, "cr"    # Landroid/content/ContentResolver;

    .prologue
    .line 751
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    .line 752
    invoke-direct {p0, p2}, Landroid/content/AsyncQueryHandler;-><init>(Landroid/content/ContentResolver;)V

    .line 753
    return-void
.end method


# virtual methods
.method protected onQueryComplete(ILjava/lang/Object;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "queryIndex"    # I
    .param p2, "cookie"    # Ljava/lang/Object;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 757
    if-nez p3, :cond_1

    .line 770
    :cond_0
    :goto_0
    return-void

    .line 762
    :cond_1
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->setTasks(Landroid/database/Cursor;)V
    invoke-static {v0, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$1400(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;Landroid/database/Cursor;)V

    .line 763
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 764
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView$TaskQueryHandler;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->mMonthTheme:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;
    invoke-static {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthView;)Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    move-result-object v0

    invoke-virtual {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->reloadCurrentDayEvents()V

    .line 766
    :cond_2
    if-eqz p3, :cond_0

    .line 767
    invoke-interface {p3}, Landroid/database/Cursor;->close()V

    .line 768
    const/4 p3, 0x0

    goto :goto_0
.end method
