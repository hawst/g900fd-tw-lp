.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;
.source "KWeatherKeyStrings.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 5
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/AbstractWeatherKeyStrings;-><init>()V

    return-void
.end method


# virtual methods
.method public initKeyStrings()V
    .locals 1

    .prologue
    .line 9
    const-string v0, "com.sec.android.widgetapp.ap.kweatherdaemon.action.CHANGE_SETTING"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    .line 10
    const-string v0, "com.sec.android.widgetapp.ap.kweatherdaemon.action.CHANGE_WEATHER_DATA"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    .line 12
    const-string v0, "com.sec.android.widgetapp.ap.kweatherdaemon.action.WEATHER_DATE_SYNC"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    .line 13
    const-string v0, "com.sec.android.widgetapp.ap.kweatherdaemon.action.CURRENT_LOCATION_WEATHER_DATA"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->ACTION_CURRENT_LOCATION_WEATHER_DATA:Ljava/lang/String;

    .line 14
    const-string v0, "com.sec.android.daemonapp.ap.kweather"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->CURRENT_WEATHER_PACKAGE_NAME:Ljava/lang/String;

    .line 15
    const-string v0, "com.sec.android.daemonapp.ap.kweather.KWeatherDaemonService"

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->CURRENT_WEATHER_CLASS_NAME:Ljava/lang/String;

    .line 17
    const-string v0, "content://com.sec.android.daemonapp.ap.kweather.provider/current_weatherinfo"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/KWeatherKeyStrings;->WEATHER_URI:Landroid/net/Uri;

    .line 18
    return-void
.end method
