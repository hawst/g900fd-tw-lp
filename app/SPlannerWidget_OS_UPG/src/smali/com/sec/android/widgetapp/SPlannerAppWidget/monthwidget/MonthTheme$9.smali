.class Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;
.super Ljava/lang/Object;
.source "MonthTheme.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->setTitle(Landroid/text/format/Time;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

.field final synthetic val$tempTime:Landroid/text/format/Time;


# direct methods
.method constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;Landroid/text/format/Time;)V
    .locals 0

    .prologue
    .line 362
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    iput-object p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->val$tempTime:Landroid/text/format/Time;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 7
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 366
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->val$tempTime:Landroid/text/format/Time;

    const/4 v5, 0x1

    invoke-virtual {v4, v5}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v2

    .line 368
    .local v2, "millis":J
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 369
    .local v1, "launchIntent":Landroid/content/Intent;
    const-string v4, "com.android.calendar"

    const-string v5, "com.android.calendar.AllInOneActivity"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 371
    const v4, 0x14208000

    invoke-virtual {v1, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 374
    const-string v4, "beginTime"

    invoke-virtual {v1, v4, v2, v3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;J)Landroid/content/Intent;

    .line 375
    const-string v4, "VIEW"

    const-string v5, "MONTH"

    invoke-virtual {v1, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 378
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$500(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 380
    :try_start_0
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$600(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    invoke-virtual {v4, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 386
    :cond_0
    :goto_0
    return-void

    .line 381
    :catch_0
    move-exception v0

    .line 382
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme$9;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;->access$700(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/MonthTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    const v5, 0x7f0a001c

    const/4 v6, 0x0

    invoke-static {v4, v5, v6}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v4

    invoke-virtual {v4}, Landroid/widget/Toast;->show()V

    goto :goto_0
.end method
