.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;
.super Ljava/lang/Object;
.source "EventHandler.java"


# static fields
.field private static final ALLDAY_WHERE:Ljava/lang/String; = "dispAllday=1"

.field private static final DISPLAY_AS_ALLDAY:Ljava/lang/String; = "dispAllday"

.field private static final EVENTS_WHERE:Ljava/lang/String; = "dispAllday=0"

.field public static final EVENT_PROJECTION:[Ljava/lang/String;

.field private static final PROJECTION_ALL_DAY_INDEX:I = 0x2

.field private static final PROJECTION_BEGIN_INDEX:I = 0x6

.field private static final PROJECTION_COLOR_INDEX:I = 0x3

.field private static final PROJECTION_DISPLAY_AS_ALLDAY:I = 0x13

.field private static final PROJECTION_END_DAY_INDEX:I = 0xa

.field private static final PROJECTION_END_INDEX:I = 0x7

.field private static final PROJECTION_END_MINUTE_INDEX:I = 0xc

.field private static final PROJECTION_EVENT_ID_INDEX:I = 0x5

.field private static final PROJECTION_FACEBOOK_SCHEDULE_ID_INDEX:I = 0x14

.field private static final PROJECTION_GUESTS_CAN_INVITE_OTHERS_INDEX:I = 0x12

.field private static final PROJECTION_HAS_ALARM_INDEX:I = 0xd

.field private static final PROJECTION_INDEX_EVENT_COLOR:I = 0x18

.field private static final PROJECTION_INSTANCEID_INDEX:I = 0x8

.field private static final PROJECTION_LOCATION_INDEX:I = 0x1

.field private static final PROJECTION_ORGANIZER_INDEX:I = 0x11

.field private static final PROJECTION_RDATE_INDEX:I = 0xf

.field private static final PROJECTION_RRULE_INDEX:I = 0xe

.field private static final PROJECTION_SELF_ATTENDEE_STATUS_INDEX:I = 0x10

.field private static final PROJECTION_START_DAY_INDEX:I = 0x9

.field private static final PROJECTION_START_MINUTE_INDEX:I = 0xb

.field private static final PROJECTION_STICKERGROUP:I = 0x16

.field private static final PROJECTION_STICKER_FILEPATH:I = 0x17

.field private static final PROJECTION_STICKER_TYPE:I = 0x15

.field private static final PROJECTION_TIMEZONE_INDEX:I = 0x4

.field private static final PROJECTION_TITLE_INDEX:I = 0x0

.field private static final SORT_ALLDAY_BY:Ljava/lang/String; = "startDay ASC, endDay DESC, title ASC"

.field private static final SORT_EVENTS_BY:Ljava/lang/String; = "startDay,startMinute,title"

.field private static final TAG:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 27
    const-class v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->TAG:Ljava/lang/String;

    .line 46
    const/16 v0, 0x19

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "eventLocation"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "allDay"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "calendar_color"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "eventTimezone"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "event_id"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "begin"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "end"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "startDay"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "endDay"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "startMinute"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "endMinute"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "hasAlarm"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "rrule"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "rdate"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "selfAttendeeStatus"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "organizer"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "guestsCanModify"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "allDay=1 OR (end-begin)>=86400000 AS dispAllday"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "facebook_schedule_id"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "sticker_type"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "sticker_group"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "filepath"

    aput-object v2, v0, v1

    const/16 v1, 0x18

    const-string v2, "eventColor"

    aput-object v2, v0, v1

    sput-object v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 102
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 103
    return-void
.end method

.method private static buildEventFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .locals 12
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cEvents"    # Landroid/database/Cursor;

    .prologue
    .line 260
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    .line 261
    .local v7, "res":Landroid/content/res/Resources;
    new-instance v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    invoke-direct {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;-><init>()V

    .line 263
    .local v0, "e":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    const/4 v9, 0x5

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->id:J

    .line 264
    const/4 v9, 0x0

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    .line 265
    const/4 v9, 0x1

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->location:Ljava/lang/CharSequence;

    .line 266
    const/4 v9, 0x2

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_3

    const/4 v9, 0x1

    :goto_0
    iput-boolean v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    .line 267
    const/16 v9, 0x11

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->organizer:Ljava/lang/String;

    .line 268
    const/16 v9, 0x12

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_4

    const/4 v9, 0x1

    :goto_1
    iput-boolean v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->guestsCanModify:Z

    .line 269
    const/4 v9, 0x4

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->timezone:Ljava/lang/String;

    .line 271
    iget-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    if-eqz v9, :cond_0

    iget-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    invoke-interface {v9}, Ljava/lang/CharSequence;->length()I

    move-result v9

    if-nez v9, :cond_1

    .line 272
    :cond_0
    const v9, 0x7f0a0057

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->title:Ljava/lang/CharSequence;

    .line 275
    :cond_1
    const/16 v9, 0x18

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    .line 276
    .local v1, "eventColor":I
    if-nez v1, :cond_6

    .line 277
    const/4 v9, 0x3

    invoke-interface {p1, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-nez v9, :cond_5

    .line 279
    const/4 v9, 0x3

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    invoke-static {v9}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDisplayColorFromColor(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    .line 287
    :goto_2
    const/4 v9, 0x6

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 288
    .local v4, "eStart":J
    const/4 v9, 0x7

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    .line 290
    .local v2, "eEnd":J
    iput-wide v4, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    .line 291
    const/16 v9, 0xb

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startTime:I

    .line 292
    const/16 v9, 0x9

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    .line 294
    iput-wide v2, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    .line 295
    const/16 v9, 0xc

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endTime:I

    .line 296
    const/16 v9, 0xa

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    .line 298
    const/16 v9, 0xd

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    if-eqz v9, :cond_7

    const/4 v9, 0x1

    :goto_3
    iput-boolean v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->hasAlarm:Z

    .line 301
    const/16 v9, 0xe

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 302
    .local v8, "rrule":Ljava/lang/String;
    const/16 v9, 0xf

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 303
    .local v6, "rdate":Ljava/lang/String;
    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-eqz v9, :cond_2

    invoke-static {v6}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v9

    if-nez v9, :cond_8

    .line 304
    :cond_2
    const/4 v9, 0x1

    iput-boolean v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    .line 309
    :goto_4
    const/16 v9, 0x10

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->selfAttendeeStatus:I

    .line 310
    const/16 v9, 0x14

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    .line 312
    const/16 v9, 0x15

    invoke-interface {p1, v9}, Landroid/database/Cursor;->isNull(I)Z

    move-result v9

    if-eqz v9, :cond_9

    .line 313
    const-wide/16 v10, 0x0

    iput-wide v10, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    .line 318
    :goto_5
    const/16 v9, 0x16

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    .line 319
    const/16 v9, 0x17

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    iput-object v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerPath:Ljava/lang/String;

    .line 321
    return-object v0

    .line 266
    .end local v1    # "eventColor":I
    .end local v2    # "eEnd":J
    .end local v4    # "eStart":J
    .end local v6    # "rdate":Ljava/lang/String;
    .end local v8    # "rrule":Ljava/lang/String;
    :cond_3
    const/4 v9, 0x0

    goto/16 :goto_0

    .line 268
    :cond_4
    const/4 v9, 0x0

    goto/16 :goto_1

    .line 281
    .restart local v1    # "eventColor":I
    :cond_5
    const v9, 0x7f070042

    invoke-virtual {v7, v9}, Landroid/content/res/Resources;->getColor(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    goto/16 :goto_2

    .line 284
    :cond_6
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDisplayColorFromColor(I)I

    move-result v9

    iput v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->color:I

    goto/16 :goto_2

    .line 298
    .restart local v2    # "eEnd":J
    .restart local v4    # "eStart":J
    :cond_7
    const/4 v9, 0x0

    goto :goto_3

    .line 306
    .restart local v6    # "rdate":Ljava/lang/String;
    .restart local v8    # "rrule":Ljava/lang/String;
    :cond_8
    const/4 v9, 0x0

    iput-boolean v9, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->isRepeating:Z

    goto :goto_4

    .line 315
    :cond_9
    const/16 v9, 0x15

    invoke-interface {p1, v9}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    iput-wide v10, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    goto :goto_5
.end method

.method public static buildEventsFromCursor(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;II)V
    .locals 4
    .param p1, "cEvents"    # Landroid/database/Cursor;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "startDay"    # I
    .param p4, "endDay"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;",
            "Landroid/database/Cursor;",
            "Landroid/content/Context;",
            "II)V"
        }
    .end annotation

    .prologue
    .line 232
    .local p0, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    if-eqz p1, :cond_0

    if-nez p0, :cond_2

    .line 233
    :cond_0
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->TAG:Ljava/lang/String;

    const-string v3, "buildEventsFromCursor: null cursor or null events list!"

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    :cond_1
    return-void

    .line 237
    :cond_2
    invoke-interface {p1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 239
    .local v0, "count":I
    if-eqz v0, :cond_1

    .line 245
    :cond_3
    :goto_0
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 246
    invoke-static {p2, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->buildEventFromCursor(Landroid/content/Context;Landroid/database/Cursor;)Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    move-result-object v1

    .line 247
    .local v1, "e":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget v2, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    if-gt v2, p4, :cond_3

    iget v2, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    if-lt v2, p3, :cond_3

    .line 250
    invoke-virtual {p0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto :goto_0
.end method

.method public static computePositions(Ljava/util/ArrayList;J)V
    .locals 1
    .param p1, "minimumDurationMillis"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;J)V"
        }
    .end annotation

    .prologue
    .line 340
    .local p0, "eventsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    if-nez p0, :cond_0

    .line 347
    :goto_0
    return-void

    .line 345
    :cond_0
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->doComputePositions(Ljava/util/ArrayList;JZ)V

    .line 346
    const/4 v0, 0x1

    invoke-static {p0, p1, p2, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->doComputePositions(Ljava/util/ArrayList;JZ)V

    goto :goto_0
.end method

.method private static doComputePositions(Ljava/util/ArrayList;JZ)V
    .locals 17
    .param p1, "minimumDurationMillis"    # J
    .param p3, "doAlldayEvents"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;JZ)V"
        }
    .end annotation

    .prologue
    .line 351
    .local p0, "eventsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v8, Ljava/util/ArrayList;

    invoke-direct {v8}, Ljava/util/ArrayList;-><init>()V

    .line 352
    .local v8, "activeList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    new-instance v11, Ljava/util/ArrayList;

    invoke-direct {v11}, Ljava/util/ArrayList;-><init>()V

    .line 354
    .local v11, "groupList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    const-wide/16 v4, 0x0

    cmp-long v3, p1, v4

    if-gez v3, :cond_0

    .line 355
    const-wide/16 p1, 0x0

    .line 358
    :cond_0
    const-wide/16 v6, 0x0

    .line 359
    .local v6, "colMask":J
    const/4 v15, 0x0

    .line 360
    .local v15, "maxCols":I
    invoke-virtual/range {p0 .. p0}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    :cond_1
    :goto_0
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_6

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 362
    .local v2, "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->drawAsAllday(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z

    move-result v3

    move/from16 v0, p3

    if-ne v3, v0, :cond_1

    .line 365
    if-nez p3, :cond_2

    .line 366
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    move-wide/from16 v4, p1

    invoke-static/range {v2 .. v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->removeNonAlldayActiveEvents(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;Ljava/util/Iterator;JJ)J

    move-result-wide v6

    .line 374
    :goto_1
    invoke-virtual {v8}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 375
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v13

    .local v13, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v13}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_3

    invoke-interface {v13}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 376
    .local v10, "ev":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iput v15, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    goto :goto_2

    .line 369
    .end local v10    # "ev":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_2
    invoke-virtual {v8}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v3

    invoke-static {v2, v3, v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->removeAlldayActiveEvents(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;Ljava/util/Iterator;J)J

    move-result-wide v6

    goto :goto_1

    .line 378
    .restart local v13    # "i$":Ljava/util/Iterator;
    :cond_3
    const/4 v15, 0x0

    .line 379
    const-wide/16 v6, 0x0

    .line 380
    invoke-virtual {v11}, Ljava/util/ArrayList;->clear()V

    .line 385
    .end local v13    # "i$":Ljava/util/Iterator;
    :cond_4
    invoke-static {v6, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->findFirstZeroBit(J)I

    move-result v9

    .line 386
    .local v9, "col":I
    const/16 v3, 0x40

    if-ne v9, v3, :cond_5

    .line 387
    const/16 v9, 0x3f

    .line 388
    :cond_5
    const-wide/16 v4, 0x1

    shl-long/2addr v4, v9

    or-long/2addr v6, v4

    .line 389
    iput v9, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    .line 390
    invoke-virtual {v8, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 391
    invoke-virtual {v11, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 392
    invoke-virtual {v8}, Ljava/util/ArrayList;->size()I

    move-result v14

    .line 393
    .local v14, "len":I
    if-ge v15, v14, :cond_1

    .line 394
    move v15, v14

    goto :goto_0

    .line 396
    .end local v2    # "event":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v9    # "col":I
    .end local v14    # "len":I
    :cond_6
    invoke-virtual {v11}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v12

    .local v12, "i$":Ljava/util/Iterator;
    :goto_3
    invoke-interface {v12}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_7

    invoke-interface {v12}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 397
    .restart local v10    # "ev":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iput v15, v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mMaxColumns:I

    goto :goto_3

    .line 399
    .end local v10    # "ev":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :cond_7
    return-void
.end method

.method public static drawAsAllday(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z
    .locals 4
    .param p0, "e"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .prologue
    .line 456
    iget-boolean v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->allDay:Z

    if-nez v0, :cond_0

    iget-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    iget-wide v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    sub-long/2addr v0, v2

    const-wide/32 v2, 0x5265c00

    cmp-long v0, v0, v2

    if-ltz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static findFirstZeroBit(J)I
    .locals 6
    .param p0, "val"    # J

    .prologue
    const/16 v1, 0x40

    .line 435
    const/4 v0, 0x0

    .local v0, "ii":I
    :goto_0
    if-ge v0, v1, :cond_1

    .line 436
    const-wide/16 v2, 0x1

    shl-long/2addr v2, v0

    and-long/2addr v2, p0

    const-wide/16 v4, 0x0

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 439
    .end local v0    # "ii":I
    :goto_1
    return v0

    .line 435
    .restart local v0    # "ii":I
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    :cond_1
    move v0, v1

    .line 439
    goto :goto_1
.end method

.method public static hasSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z
    .locals 4
    .param p0, "e"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .prologue
    .line 447
    iget-wide v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerType:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static final instancesQuery(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 14
    .param p0, "cr"    # Landroid/content/ContentResolver;
    .param p1, "projection"    # [Ljava/lang/String;
    .param p2, "startDay"    # I
    .param p3, "endDay"    # I
    .param p4, "selection"    # Ljava/lang/String;
    .param p5, "selectionArgs"    # [Ljava/lang/String;
    .param p6, "orderBy"    # Ljava/lang/String;

    .prologue
    .line 188
    const/4 v12, 0x0

    .line 189
    .local v12, "cursor":Landroid/database/Cursor;
    const-string v10, "visible=?"

    .line 190
    .local v10, "WHERE_CALENDARS_SELECTED":Ljava/lang/String;
    const/4 v2, 0x1

    new-array v9, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "1"

    aput-object v3, v9, v2

    .line 191
    .local v9, "WHERE_CALENDARS_ARGS":[Ljava/lang/String;
    const-string v8, "begin ASC"

    .line 193
    .local v8, "DEFAULT_SORT_ORDER":Ljava/lang/String;
    sget-object v2, Landroid/provider/CalendarContract$Instances;->CONTENT_BY_DAY_URI:Landroid/net/Uri;

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v11

    .line 194
    .local v11, "builder":Landroid/net/Uri$Builder;
    move/from16 v0, p2

    int-to-long v2, v0

    invoke-static {v11, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 195
    move/from16 v0, p3

    int-to-long v2, v0

    invoke-static {v11, v2, v3}, Landroid/content/ContentUris;->appendId(Landroid/net/Uri$Builder;J)Landroid/net/Uri$Builder;

    .line 196
    invoke-static/range {p4 .. p4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 197
    move-object/from16 p4, v10

    .line 198
    move-object/from16 p5, v9

    .line 209
    :goto_0
    :try_start_0
    invoke-virtual {v11}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    if-nez p6, :cond_2

    move-object v7, v8

    :goto_1
    move-object v2, p0

    move-object v4, p1

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    invoke-virtual/range {v2 .. v7}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v12

    .line 211
    if-eqz v12, :cond_3

    move-object v2, v12

    .line 221
    :goto_2
    return-object v2

    .line 200
    :cond_0
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "("

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    move-object/from16 v0, p4

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ") AND "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p4

    .line 201
    if-eqz p5, :cond_1

    move-object/from16 v0, p5

    array-length v2, v0

    if-lez v2, :cond_1

    .line 202
    move-object/from16 v0, p5

    array-length v2, v0

    add-int/lit8 v2, v2, 0x1

    move-object/from16 v0, p5

    invoke-static {v0, v2}, Ljava/util/Arrays;->copyOf([Ljava/lang/Object;I)[Ljava/lang/Object;

    move-result-object p5

    .end local p5    # "selectionArgs":[Ljava/lang/String;
    check-cast p5, [Ljava/lang/String;

    .line 203
    .restart local p5    # "selectionArgs":[Ljava/lang/String;
    move-object/from16 v0, p5

    array-length v2, v0

    add-int/lit8 v2, v2, -0x1

    const/4 v3, 0x0

    aget-object v3, v9, v3

    aput-object v3, p5, v2

    goto :goto_0

    .line 205
    :cond_1
    move-object/from16 p5, v9

    goto :goto_0

    :cond_2
    move-object/from16 v7, p6

    .line 209
    goto :goto_1

    .line 214
    :catch_0
    move-exception v13

    .line 215
    .local v13, "e":Ljava/lang/Throwable;
    invoke-virtual {v13}, Ljava/lang/Throwable;->printStackTrace()V

    .line 216
    if-eqz v12, :cond_3

    .line 217
    invoke-interface {v12}, Landroid/database/Cursor;->close()V

    .line 218
    const/4 v2, 0x0

    goto :goto_2

    .end local v13    # "e":Ljava/lang/Throwable;
    :cond_3
    move-object v2, v12

    .line 221
    goto :goto_2
.end method

.method public static isBackgroundSticker(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z
    .locals 2
    .param p0, "e"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .prologue
    .line 451
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->stickerGroup:I

    const/16 v1, 0xa

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isFacebook(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;)Z
    .locals 1
    .param p0, "e"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .prologue
    .line 443
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->facebookScheudleId:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static loadEvents(Landroid/content/Context;Ljava/util/ArrayList;IIILjava/util/concurrent/atomic/AtomicInteger;)V
    .locals 20
    .param p0, "context"    # Landroid/content/Context;
    .param p2, "startDay"    # I
    .param p3, "days"    # I
    .param p4, "requestId"    # I
    .param p5, "sequenceNumber"    # Ljava/util/concurrent/atomic/AtomicInteger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;III",
            "Ljava/util/concurrent/atomic/AtomicInteger;",
            ")V"
        }
    .end annotation

    .prologue
    .line 110
    .local p1, "events":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    if-nez p0, :cond_1

    .line 164
    :cond_0
    :goto_0
    return-void

    .line 114
    :cond_1
    const/16 v17, 0x0

    .line 115
    .local v17, "cEvents":Landroid/database/Cursor;
    const/16 v16, 0x0

    .line 116
    .local v16, "cAllday":Landroid/database/Cursor;
    invoke-virtual/range {p1 .. p1}, Ljava/util/ArrayList;->clear()V

    .line 118
    add-int v4, p2, p3

    add-int/lit8 v7, v4, -0x1

    .line 131
    .local v7, "endDay":I
    :try_start_0
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->hideDeclinedEvent()Z

    move-result v18

    .line 132
    .local v18, "hideDeclined":Z
    const-string v8, "dispAllday=0"

    .line 133
    .local v8, "where":Ljava/lang/String;
    const-string v13, "dispAllday=1"

    .line 134
    .local v13, "whereAllday":Ljava/lang/String;
    if-eqz v18, :cond_2

    .line 135
    const-string v19, " AND selfAttendeeStatus!=2"

    .line 137
    .local v19, "hideString":Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v8

    .line 138
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v4, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    move-object/from16 v0, v19

    invoke-virtual {v4, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v13

    .line 140
    .end local v19    # "hideString":Ljava/lang/String;
    :cond_2
    move-object/from16 v0, p0

    invoke-static {v0, v8}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getHideContactEventSelection(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 141
    move-object/from16 v0, p0

    invoke-static {v0, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getHideContactEventSelection(Landroid/content/Context;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v13

    .line 143
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    sget-object v5, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v9, 0x0

    const-string v10, "startDay,startMinute,title"

    move/from16 v6, p2

    invoke-static/range {v4 .. v10}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->instancesQuery(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v17

    .line 145
    invoke-virtual/range {p0 .. p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    sget-object v10, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->EVENT_PROJECTION:[Ljava/lang/String;

    const/4 v14, 0x0

    const-string v15, "startDay ASC, endDay DESC, title ASC"

    move/from16 v11, p2

    move v12, v7

    invoke-static/range {v9 .. v15}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->instancesQuery(Landroid/content/ContentResolver;[Ljava/lang/String;IILjava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v16

    .line 150
    invoke-virtual/range {p5 .. p5}, Ljava/util/concurrent/atomic/AtomicInteger;->get()I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v4

    move/from16 v0, p4

    if-eq v0, v4, :cond_4

    .line 157
    if-eqz v17, :cond_3

    .line 158
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 160
    :cond_3
    if-eqz v16, :cond_0

    .line 161
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 154
    :cond_4
    :try_start_1
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    move-object/from16 v2, p0

    move/from16 v3, p2

    invoke-static {v0, v1, v2, v3, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->buildEventsFromCursor(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;II)V

    .line 155
    move-object/from16 v0, p1

    move-object/from16 v1, v17

    move-object/from16 v2, p0

    move/from16 v3, p2

    invoke-static {v0, v1, v2, v3, v7}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/EventHandler;->buildEventsFromCursor(Ljava/util/ArrayList;Landroid/database/Cursor;Landroid/content/Context;II)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 157
    if-eqz v17, :cond_5

    .line 158
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 160
    :cond_5
    if-eqz v16, :cond_0

    .line 161
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    goto/16 :goto_0

    .line 157
    .end local v8    # "where":Ljava/lang/String;
    .end local v13    # "whereAllday":Ljava/lang/String;
    .end local v18    # "hideDeclined":Z
    :catchall_0
    move-exception v4

    if-eqz v17, :cond_6

    .line 158
    invoke-interface/range {v17 .. v17}, Landroid/database/Cursor;->close()V

    .line 160
    :cond_6
    if-eqz v16, :cond_7

    .line 161
    invoke-interface/range {v16 .. v16}, Landroid/database/Cursor;->close()V

    :cond_7
    throw v4
.end method

.method private static removeAlldayActiveEvents(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;Ljava/util/Iterator;J)J
    .locals 6
    .param p0, "event"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .param p2, "colMask"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;J)J"
        }
    .end annotation

    .prologue
    .line 405
    .local p1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 406
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 407
    .local v0, "active":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endDay:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startDay:I

    if-ge v1, v2, :cond_0

    .line 408
    const-wide/16 v2, 0x1

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    shl-long/2addr v2, v1

    const-wide/16 v4, -0x1

    xor-long/2addr v2, v4

    and-long/2addr p2, v2

    .line 409
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 412
    .end local v0    # "active":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    :cond_1
    return-wide p2
.end method

.method private static removeNonAlldayActiveEvents(Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;Ljava/util/Iterator;JJ)J
    .locals 10
    .param p0, "event"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .param p2, "minDurationMillis"    # J
    .param p4, "colMask"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            "Ljava/util/Iterator",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;JJ)J"
        }
    .end annotation

    .prologue
    .line 417
    .local p1, "iter":Ljava/util/Iterator;, "Ljava/util/Iterator<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;>;"
    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    .line 421
    .local v4, "start":J
    :cond_0
    :goto_0
    invoke-interface {p1}, Ljava/util/Iterator;->hasNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 422
    invoke-interface {p1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;

    .line 424
    .local v0, "active":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->endMillis:J

    iget-wide v8, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    sub-long/2addr v6, v8

    invoke-static {v6, v7, p2, p3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 426
    .local v2, "duration":J
    iget-wide v6, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->startMillis:J

    add-long/2addr v6, v2

    cmp-long v1, v6, v4

    if-gtz v1, :cond_0

    .line 427
    const-wide/16 v6, 0x1

    iget v1, v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;->mColumn:I

    shl-long/2addr v6, v1

    const-wide/16 v8, -0x1

    xor-long/2addr v6, v8

    and-long/2addr p4, v6

    .line 428
    invoke-interface {p1}, Ljava/util/Iterator;->remove()V

    goto :goto_0

    .line 431
    .end local v0    # "active":Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;
    .end local v2    # "duration":J
    :cond_1
    return-wide p4
.end method
