.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;
.super Ljava/lang/Object;
.source "TodayTheme.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1
    name = "CompleteCheckChangedListener"
.end annotation


# instance fields
.field private final mPosition:I

.field private final mTaskId:J

.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;JI)V
    .locals 0
    .param p2, "taskId"    # J
    .param p4, "position"    # I

    .prologue
    .line 869
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 870
    iput-wide p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->mTaskId:J

    .line 871
    iput p4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->mPosition:I

    .line 872
    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 7
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v6, 0x0

    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 876
    invoke-virtual {p1, v2}, Landroid/widget/CompoundButton;->playSoundEffect(I)V

    .line 878
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$2700(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 879
    iget-object v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$2800(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    move-result-object v4

    invoke-virtual {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->needImmediateRefreshInObserver()V

    .line 882
    :cond_0
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1, v3}, Landroid/content/ContentValues;-><init>(I)V

    .line 883
    .local v1, "values":Landroid/content/ContentValues;
    const-string v4, "complete"

    if-eqz p2, :cond_1

    move v2, v3

    :cond_1
    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 884
    const-string v2, "date_completed"

    new-instance v4, Ljava/util/Date;

    invoke-direct {v4}, Ljava/util/Date;-><init>()V

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v1, v2, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 885
    sget-object v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->SYNCHED_TASKS_CONTENT_URI:Landroid/net/Uri;

    iget-wide v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->mTaskId:J

    invoke-static {v2, v4, v5}, Landroid/content/ContentUris;->withAppendedId(Landroid/net/Uri;J)Landroid/net/Uri;

    move-result-object v0

    .line 886
    .local v0, "uri":Landroid/net/Uri;
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mContext:Landroid/content/Context;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$2900(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;)Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    invoke-virtual {v2, v0, v1, v6, v6}, Landroid/content/ContentResolver;->update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 888
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iput-boolean v3, v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->mIsRefreshFromCheckBoxTouch:Z

    .line 889
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme$CompleteCheckChangedListener;->mPosition:I

    # invokes: Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->setSelectionPostion(I)V
    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;->access$3000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/TodayTheme;I)V

    .line 890
    return-void
.end method
