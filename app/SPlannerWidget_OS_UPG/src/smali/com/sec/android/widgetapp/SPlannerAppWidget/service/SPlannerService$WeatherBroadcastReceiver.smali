.class Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;
.super Landroid/content/BroadcastReceiver;
.source "SPlannerService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "WeatherBroadcastReceiver"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;


# direct methods
.method private constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V
    .locals 0

    .prologue
    .line 53
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;
    .param p2, "x1"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$1;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 5
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    if-nez p2, :cond_1

    .line 77
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const-string v1, "SPlannerService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onReceive:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 63
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    invoke-virtual {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->isWeatherEnabled(Landroid/content/Context;)Z

    move-result v1

    if-nez v1, :cond_2

    .line 64
    const-string v1, "SPlannerService"

    const-string v2, "No Weather Service."

    invoke-static {v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 68
    :cond_2
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    .line 70
    .local v0, "action":Ljava/lang/String;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_SETTING_CHANGED:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATA_UPDATED:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_3

    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mWeather:Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;

    move-result-object v1

    iget-object v1, v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/weather/WeatherManager;->ACTION_WEATHER_DATE_SYNC:Ljava/lang/String;

    invoke-virtual {v1, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 75
    :cond_3
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService$WeatherBroadcastReceiver;->this$0:Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;

    # getter for: Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;->access$100(Lcom/sec/android/widgetapp/SPlannerAppWidget/service/SPlannerService;)Landroid/os/Handler;

    move-result-object v2

    const/4 v3, 0x1

    const-string v4, "android.intent.action.TIME_SET"

    invoke-virtual {v2, v3, v4}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->sendMessage(Landroid/os/Message;)Z

    goto :goto_0
.end method
