.class public Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;
.super Ljava/lang/Object;
.source "TwDayOfMonthCursor.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;
    }
.end annotation


# static fields
.field private static mPrevColumn:I


# instance fields
.field private final mCalendar:Ljava/util/Calendar;

.field private mColumn:I

.field private mNumDaysInMonth:I

.field private mNumDaysInPrevMonth:I

.field private mOffset:I

.field private mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

.field private mRow:I

.field private final mWeekStartDay:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 40
    const/4 v0, -0x1

    sput v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mPrevColumn:I

    return-void
.end method

.method public constructor <init>(IIII)V
    .locals 4
    .param p1, "year"    # I
    .param p2, "month"    # I
    .param p3, "dayOfMonth"    # I
    .param p4, "weekStartDay"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 42
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    .line 56
    if-lt p4, v3, :cond_0

    const/4 v0, 0x7

    if-le p4, v0, :cond_1

    .line 57
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 60
    :cond_1
    iput p4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mWeekStartDay:I

    .line 61
    const-string v0, "GMT"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    iput-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    .line 62
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0, v3, p1}, Ljava/util/Calendar;->set(II)V

    .line 63
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1, p2}, Ljava/util/Calendar;->set(II)V

    .line 64
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x5

    invoke-virtual {v0, v1, v3}, Ljava/util/Calendar;->set(II)V

    .line 65
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xb

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 66
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xc

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 67
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/16 v1, 0xd

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->set(II)V

    .line 68
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTimeInMillis()J

    .line 70
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->recalculate()V

    .line 73
    invoke-virtual {p0, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 74
    invoke-virtual {p0, p3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 75
    return-void
.end method

.method private recalculate()V
    .locals 6

    .prologue
    const/4 v5, 0x5

    const/4 v4, 0x2

    .line 203
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInMonth:I

    .line 204
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v3, -0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    .line 205
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    invoke-virtual {v2, v5}, Ljava/util/Calendar;->getActualMaximum(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInPrevMonth:I

    .line 206
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v3, 0x1

    invoke-virtual {v2, v4, v3}, Ljava/util/Calendar;->add(II)V

    .line 208
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getFirstDayOfMonth()I

    move-result v0

    .line 209
    .local v0, "firstDayOfMonth":I
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mWeekStartDay:I

    sub-int v1, v0, v2

    .line 211
    .local v1, "offset":I
    if-gez v1, :cond_0

    .line 212
    add-int/lit8 v1, v1, 0x7

    .line 214
    :cond_0
    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    .line 215
    return-void
.end method


# virtual methods
.method public down()Z
    .locals 3

    .prologue
    .line 311
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, 0x1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 313
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 314
    const/4 v0, 0x0

    .line 324
    .local v0, "returnValue":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v1, :cond_0

    .line 325
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 327
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    sput v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mPrevColumn:I

    .line 328
    return v0

    .line 317
    .end local v0    # "returnValue":Z
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->nextMonth()V

    .line 318
    const/4 v1, 0x0

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 319
    :goto_1
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-nez v1, :cond_2

    .line 320
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    goto :goto_1

    .line 322
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "returnValue":Z
    goto :goto_0
.end method

.method public getColumnOf(I)I
    .locals 1
    .param p1, "day"    # I

    .prologue
    .line 161
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    rem-int/lit8 v0, v0, 0x7

    add-int/lit8 v0, v0, 0x1

    return v0
.end method

.method public getDayAt(II)I
    .locals 4
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 139
    if-nez p2, :cond_1

    const/4 v0, 0x0

    .line 140
    .local v0, "c":I
    :goto_0
    if-nez p1, :cond_2

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    if-ge v0, v2, :cond_2

    .line 141
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInPrevMonth:I

    add-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    sub-int/2addr v2, v3

    add-int/lit8 v1, v2, 0x1

    .line 146
    :cond_0
    :goto_1
    return v1

    .line 139
    .end local v0    # "c":I
    :cond_1
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 144
    .restart local v0    # "c":I
    :cond_2
    mul-int/lit8 v2, p1, 0x7

    add-int/2addr v2, v0

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    sub-int/2addr v2, v3

    add-int/lit8 v1, v2, 0x1

    .line 146
    .local v1, "day":I
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInMonth:I

    if-le v1, v2, :cond_0

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInMonth:I

    sub-int/2addr v1, v2

    goto :goto_1
.end method

.method public getDigitsForRow(I)[I
    .locals 5
    .param p1, "row"    # I

    .prologue
    const/4 v3, 0x7

    .line 120
    if-ltz p1, :cond_0

    const/4 v2, 0x5

    if-le p1, v2, :cond_1

    .line 121
    :cond_0
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "row "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " out of range (0-5)"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 124
    :cond_1
    new-array v1, v3, [I

    .line 125
    .local v1, "result":[I
    const/4 v0, 0x0

    .local v0, "column":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 126
    invoke-virtual {p0, p1, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v2

    aput v2, v1, v0

    .line 125
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 128
    :cond_2
    return-object v1
.end method

.method public getFirstDayOfMonth()I
    .locals 2

    .prologue
    .line 91
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x7

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getMonth()I
    .locals 2

    .prologue
    .line 83
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public getNumberOfDaysInMonth()I
    .locals 1

    .prologue
    .line 98
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInMonth:I

    return v0
.end method

.method public getOffset()I
    .locals 1

    .prologue
    .line 108
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    return v0
.end method

.method public getRowOf(I)I
    .locals 1
    .param p1, "day"    # I

    .prologue
    .line 154
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    add-int/2addr v0, p1

    add-int/lit8 v0, v0, -0x1

    div-int/lit8 v0, v0, 0x7

    return v0
.end method

.method public getSelectedColumn()I
    .locals 1

    .prologue
    .line 223
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    return v0
.end method

.method public getSelectedDayOfMonth()I
    .locals 2

    .prologue
    .line 236
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getDayAt(II)I

    move-result v0

    return v0
.end method

.method public getSelectedMonthOffset()I
    .locals 2

    .prologue
    .line 246
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 247
    const/4 v0, 0x0

    .line 254
    :goto_0
    return v0

    .line 250
    :cond_0
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    if-nez v0, :cond_1

    .line 251
    const/4 v0, -0x1

    goto :goto_0

    .line 254
    :cond_1
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public getSelectedRow()I
    .locals 1

    .prologue
    .line 219
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    return v0
.end method

.method public getYear()I
    .locals 2

    .prologue
    .line 79
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I

    move-result v0

    return v0
.end method

.method public isSelected(II)Z
    .locals 1
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 271
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    if-ne v0, p2, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public isValid(II)Z
    .locals 3
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    .line 407
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getMonth()I

    move-result v0

    .line 408
    .local v0, "month":I
    invoke-virtual {p0, p1, p2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v2

    if-nez v2, :cond_0

    .line 409
    if-nez p1, :cond_3

    .line 410
    add-int/lit8 v0, v0, -0x1

    .line 414
    :cond_0
    :goto_0
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getYear()I

    move-result v1

    .line 416
    .local v1, "year":I
    if-gez v0, :cond_1

    const/16 v2, 0x76e

    if-eq v1, v2, :cond_2

    :cond_1
    const/16 v2, 0xb

    if-le v0, v2, :cond_4

    const/16 v2, 0x7f4

    if-ne v1, v2, :cond_4

    .line 418
    :cond_2
    const/4 v2, 0x0

    .line 420
    :goto_1
    return v2

    .line 412
    .end local v1    # "year":I
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 420
    .restart local v1    # "year":I
    :cond_4
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public isWithinCurrentMonth(II)Z
    .locals 5
    .param p1, "row"    # I
    .param p2, "column"    # I

    .prologue
    const/4 v2, 0x0

    .line 184
    if-nez p2, :cond_1

    move v0, v2

    .line 185
    .local v0, "c":I
    :goto_0
    if-ltz p1, :cond_0

    if-ltz v0, :cond_0

    const/4 v3, 0x5

    if-gt p1, v3, :cond_0

    const/4 v3, 0x6

    if-le v0, v3, :cond_2

    .line 198
    :cond_0
    :goto_1
    return v2

    .line 184
    .end local v0    # "c":I
    :cond_1
    add-int/lit8 v0, p2, -0x1

    goto :goto_0

    .line 189
    .restart local v0    # "c":I
    :cond_2
    if-nez p1, :cond_3

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    if-lt v0, v3, :cond_0

    .line 193
    :cond_3
    mul-int/lit8 v3, p1, 0x7

    add-int/2addr v3, v0

    iget v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOffset:I

    sub-int/2addr v3, v4

    add-int/lit8 v1, v3, 0x1

    .line 195
    .local v1, "day":I
    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mNumDaysInMonth:I

    if-gt v1, v3, :cond_0

    .line 198
    const/4 v2, 0x1

    goto :goto_1
.end method

.method public left()Z
    .locals 4

    .prologue
    .line 340
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    if-nez v2, :cond_1

    .line 341
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 342
    const/4 v2, 0x7

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 347
    :goto_0
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 348
    const/4 v1, 0x0

    .line 357
    .local v1, "returnValue":Z
    :goto_1
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v2, :cond_0

    .line 358
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 360
    :cond_0
    return v1

    .line 344
    .end local v1    # "returnValue":Z
    :cond_1
    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    goto :goto_0

    .line 351
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->previousMonth()V

    .line 352
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getNumberOfDaysInMonth()I

    move-result v0

    .line 353
    .local v0, "lastDay":I
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 354
    invoke-virtual {p0, v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 355
    const/4 v1, 0x1

    .restart local v1    # "returnValue":Z
    goto :goto_1
.end method

.method public nextMonth()V
    .locals 3

    .prologue
    .line 176
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x2

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 177
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->recalculate()V

    .line 178
    return-void
.end method

.method public previousMonth()V
    .locals 3

    .prologue
    .line 168
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mCalendar:Ljava/util/Calendar;

    const/4 v1, 0x2

    const/4 v2, -0x1

    invoke-virtual {v0, v1, v2}, Ljava/util/Calendar;->add(II)V

    .line 169
    invoke-direct {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->recalculate()V

    .line 170
    return-void
.end method

.method public right()Z
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 372
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    const/4 v2, 0x7

    if-ne v1, v2, :cond_1

    .line 373
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 374
    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 379
    :goto_0
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 380
    const/4 v0, 0x0

    .line 392
    .local v0, "returnValue":Z
    :goto_1
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v1, :cond_0

    .line 393
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 395
    :cond_0
    return v0

    .line 376
    .end local v0    # "returnValue":Z
    :cond_1
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    goto :goto_0

    .line 383
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->nextMonth()V

    .line 384
    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 385
    iput v3, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 386
    :goto_2
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-nez v1, :cond_3

    .line 387
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    goto :goto_2

    .line 389
    :cond_3
    const/4 v0, 0x1

    .restart local v0    # "returnValue":Z
    goto :goto_1
.end method

.method public setOnCursorMoveListener(Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    .prologue
    .line 399
    iput-object p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    .line 400
    return-void
.end method

.method public setSelectedDayOfMonth(I)V
    .locals 1
    .param p1, "dayOfMonth"    # I

    .prologue
    .line 258
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getRowOf(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 259
    invoke-virtual {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->getColumnOf(I)I

    move-result v0

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 261
    sget v0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mPrevColumn:I

    if-nez v0, :cond_0

    .line 262
    iget v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 265
    :cond_0
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v0, :cond_1

    .line 266
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 268
    :cond_1
    return-void
.end method

.method public setSelectedRowColumn(II)V
    .locals 1
    .param p1, "row"    # I
    .param p2, "col"    # I

    .prologue
    .line 227
    iput p1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 228
    iput p2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    .line 230
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 233
    :cond_0
    return-void
.end method

.method public up()Z
    .locals 3

    .prologue
    .line 283
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, -0x1

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 285
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 286
    const/4 v0, 0x0

    .line 296
    .local v0, "returnValue":Z
    :goto_0
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    if-eqz v1, :cond_0

    .line 297
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mOnCursorMoveListener:Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;

    invoke-interface {v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor$OnCursorMoveListener;->onCusorMoved()V

    .line 299
    :cond_0
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    sput v1, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mPrevColumn:I

    .line 300
    return v0

    .line 289
    .end local v0    # "returnValue":Z
    :cond_1
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->previousMonth()V

    .line 290
    const/4 v1, 0x5

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    .line 291
    :goto_1
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    iget v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mColumn:I

    invoke-virtual {p0, v1, v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->isWithinCurrentMonth(II)Z

    move-result v1

    if-nez v1, :cond_2

    .line 292
    iget v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    add-int/lit8 v1, v1, -0x1

    iput v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/monthwidget/TwDayOfMonthCursor;->mRow:I

    goto :goto_1

    .line 294
    :cond_2
    const/4 v0, 0x1

    .restart local v0    # "returnValue":Z
    goto :goto_0
.end method
