.class public abstract Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;
.super Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;
.source "BaseMiniTodayThemeImple.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;
    }
.end annotation


# static fields
.field public static final PRIORITY_LEVEL_HIGH:I = 0x2

.field public static final PRIORITY_LEVEL_LOW:I = 0x0

.field public static final PRIORITY_LEVEL_NORMAL:I = 0x1

.field protected static final TASK_QUERY_TOKEN:I

.field protected static sCurrentTzOnPause:Ljava/lang/String;


# instance fields
.field protected isFontStyleDefault:Z

.field protected mCurrentDateOnPause:Ljava/lang/String;

.field protected mCurrentDateOnResume:Ljava/lang/String;

.field protected mDayFormat:Ljava/lang/String;

.field protected mEventsArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Event;",
            ">;"
        }
    .end annotation
.end field

.field protected mHandler:Landroid/os/Handler;

.field protected mInitialized:Z

.field protected mInstance:I

.field protected mIsRefreshFromCheckBoxTouch:Z

.field protected final mIsTabletConfig:Z

.field protected mListContainerView:Landroid/widget/FrameLayout;

.field protected mMain:Landroid/widget/LinearLayout;

.field protected mNoEventView:Landroid/widget/LinearLayout;

.field protected mSelectedPosition:I

.field protected mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

.field protected mTasksArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation
.end field

.field protected final mTempTasksArray:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;",
            ">;"
        }
    .end annotation
.end field

.field protected mTempTime:Landroid/text/format/Time;

.field protected mTime:Landroid/text/format/Time;

.field protected mYearFormat:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V
    .locals 5
    .param p1, "sPlannerSurfaceWidget"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    .prologue
    const/4 v4, 0x0

    .line 82
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/BaseSurfaceTheme;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;)V

    .line 59
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->isFontStyleDefault:Z

    .line 62
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mEventsArray:Ljava/util/ArrayList;

    .line 63
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTasksArray:Ljava/util/ArrayList;

    .line 64
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTempTasksArray:Ljava/util/ArrayList;

    .line 78
    iput v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSelectedPosition:I

    .line 84
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v3, 0x7f060002

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getConfigBool(Landroid/content/Context;I)Z

    move-result v2

    iput-boolean v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mIsTabletConfig:Z

    .line 85
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v3, 0x7f0a0021

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mDayFormat:Ljava/lang/String;

    .line 86
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v3, 0x7f0a006d

    invoke-virtual {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getString(I)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mYearFormat:Ljava/lang/String;

    .line 87
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-virtual {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    .line 88
    .local v1, "resolver":Landroid/content/ContentResolver;
    new-instance v2, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

    invoke-direct {v2, p0, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;-><init>(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;Landroid/content/ContentResolver;)V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTaskHandler:Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple$TaskQueryHandler;

    .line 89
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mHandler:Landroid/os/Handler;

    .line 90
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2}, Lcom/sec/android/widgetapp/SPlannerAppWidget/Feature;->getSettingsFontStyle(Landroid/content/Context;)I

    move-result v0

    .line 91
    .local v0, "fontIndexDB":I
    if-eqz v0, :cond_0

    .line 92
    iput-boolean v4, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->isFontStyleDefault:Z

    .line 94
    :cond_0
    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 42
    invoke-direct {p0, p1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->setTasks(Landroid/database/Cursor;)V

    return-void
.end method

.method private setTasks(Landroid/database/Cursor;)V
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 97
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    if-eqz v2, :cond_0

    .line 98
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 99
    .local v1, "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, p1, v1}, Lcom/sec/android/widgetapp/SPlannerAppWidget/io/TaskHandler;->constructTask(Landroid/content/Context;Landroid/database/Cursor;Ljava/util/ArrayList;)V

    .line 100
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->clear()V

    .line 102
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v2

    if-ge v0, v2, :cond_0

    .line 103
    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTempTasksArray:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 106
    .end local v0    # "i":I
    .end local v1    # "tasks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/sec/android/widgetapp/SPlannerAppWidget/io/model/Task;>;"
    :cond_0
    return-void
.end method


# virtual methods
.method protected getDateFormatForTitle(Landroid/text/format/Time;)Landroid/text/SpannableStringBuilder;
    .locals 14
    .param p1, "time"    # Landroid/text/format/Time;

    .prologue
    const/4 v13, 0x2

    const/4 v12, 0x1

    .line 111
    iget v10, p1, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v10, v10, 0x1

    invoke-static {v10, v13}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v8

    .line 112
    .local v8, "weekday":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mContext:Landroid/content/Context;

    iget v11, p1, Landroid/text/format/Time;->monthDay:I

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v5

    .line 113
    .local v5, "monthday":Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v10

    if-eqz v10, :cond_0

    .line 114
    invoke-virtual {v5}, Ljava/lang/String;->trim()Ljava/lang/String;

    move-result-object v5

    .line 116
    :cond_0
    iget v10, p1, Landroid/text/format/Time;->month:I

    invoke-static {v10, v13, v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getMonthString(IIZ)Ljava/lang/String;

    move-result-object v4

    .line 118
    .local v4, "monthString":Ljava/lang/String;
    invoke-static {v4}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->toCapitalize(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 119
    .local v3, "month":Ljava/lang/String;
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mContext:Landroid/content/Context;

    iget v11, p1, Landroid/text/format/Time;->year:I

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getYearString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v9

    .line 120
    .local v9, "year":Ljava/lang/String;
    new-instance v10, Ljava/lang/String;

    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getDateFormatOrder()[C

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/String;-><init>([C)V

    invoke-virtual {v10}, Ljava/lang/String;->toUpperCase()Ljava/lang/String;

    move-result-object v2

    .line 121
    .local v2, "dateFormatOrder":Ljava/lang/String;
    const/4 v1, 0x0

    .line 122
    .local v1, "dateFormat":Ljava/lang/String;
    const-string v10, "YMD"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_5

    .line 123
    const-string v1, "yyyy-MM-dd"

    .line 130
    :goto_0
    new-instance v7, Landroid/text/SpannableStringBuilder;

    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 131
    .local v7, "title":Landroid/text/SpannableStringBuilder;
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isFinnish()Z

    move-result v10

    if-eqz v10, :cond_1

    .line 132
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ". "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 134
    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    :cond_1
    if-eqz v1, :cond_4

    .line 135
    const-string v10, "MM-dd-yyyy"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_8

    .line 136
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-virtual {v11}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_7

    .line 137
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 145
    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    :cond_2
    :goto_1
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->isChinese()Z

    move-result v10

    if-eqz v10, :cond_b

    .line 146
    const-string v0, ", "

    .line 148
    .local v0, "beforeWeekday":Ljava/lang/String;
    invoke-virtual {p1, v12}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v10

    invoke-static {v10, v11}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    .line 149
    .local v6, "tTime":Ljava/lang/Long;
    iget-boolean v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mIsTabletConfig:Z

    if-eqz v10, :cond_3

    invoke-virtual {v6}, Ljava/lang/Long;->longValue()J

    move-result-wide v10

    invoke-virtual {p0, v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->isToday(J)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 150
    const-string v0, ", \n"

    .line 153
    :cond_3
    iget v10, p1, Landroid/text/format/Time;->month:I

    invoke-static {v10, v12, v12}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getMonthString(IIZ)Ljava/lang/String;

    move-result-object v3

    .line 155
    const-string v10, "MM-dd-yyyy"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_9

    .line 156
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .line 200
    .end local v0    # "beforeWeekday":Ljava/lang/String;
    .end local v6    # "tTime":Ljava/lang/Long;
    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    :cond_4
    :goto_2
    return-object v7

    .line 124
    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    :cond_5
    const-string v10, "DMY"

    invoke-virtual {v2, v10}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v10

    if-eqz v10, :cond_6

    .line 125
    const-string v1, "dd-MM-yyyy"

    goto/16 :goto_0

    .line 127
    :cond_6
    const-string v1, "MM-dd-yyyy"

    goto/16 :goto_0

    .line 139
    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    :cond_7
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_1

    .line 141
    :cond_8
    const-string v10, "yyyy-MM-dd"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 142
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, ", "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_1

    .line 158
    .restart local v0    # "beforeWeekday":Ljava/lang/String;
    .restart local v6    # "tTime":Ljava/lang/Long;
    :cond_9
    const-string v10, "yyyy-MM-dd"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_a

    .line 159
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 162
    :cond_a
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 165
    .end local v0    # "beforeWeekday":Ljava/lang/String;
    .end local v6    # "tTime":Ljava/lang/Long;
    :cond_b
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->KOREA:Ljava/util/Locale;

    invoke-virtual {v11}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-nez v10, :cond_c

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->KOREAN:Ljava/util/Locale;

    invoke-virtual {v11}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_f

    .line 167
    :cond_c
    iget v10, p1, Landroid/text/format/Time;->weekDay:I

    add-int/lit8 v10, v10, 0x1

    const/4 v11, 0x3

    invoke-static {v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/SecDateUtils;->getDayOfWeekString(II)Ljava/lang/String;

    move-result-object v8

    .line 168
    const-string v10, "MM-dd-yyyy"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_d

    .line 169
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 171
    :cond_d
    const-string v10, "yyyy-MM-dd"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_e

    .line 172
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 175
    :cond_e
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 178
    :cond_f
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    sget-object v11, Ljava/util/Locale;->JAPANESE:Ljava/util/Locale;

    invoke-virtual {v11}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_12

    .line 179
    iget-object v10, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    const v11, 0x7f0a006b

    invoke-virtual {v10, v11}, Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;->getString(I)Ljava/lang/String;

    move-result-object v10

    invoke-virtual {p1, v10}, Landroid/text/format/Time;->format(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    .line 180
    const-string v10, "MM-dd-yyyy"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_10

    .line 181
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 182
    :cond_10
    const-string v10, "yyyy-MM-dd"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_11

    .line 183
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 185
    :cond_11
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 187
    :cond_12
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v10

    invoke-virtual {v10}, Ljava/util/Locale;->getLanguage()Ljava/lang/String;

    move-result-object v10

    const-string v11, "ar"

    invoke-virtual {v10, v11}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 188
    const-string v10, "MM-dd-yyyy"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_13

    .line 189
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\u060c "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 191
    :cond_13
    const-string v10, "yyyy-MM-dd"

    invoke-virtual {v10, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_14

    .line 192
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\u060c "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2

    .line 195
    :cond_14
    new-instance v7, Landroid/text/SpannableStringBuilder;

    .end local v7    # "title":Landroid/text/SpannableStringBuilder;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v10, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, "\u060c "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-direct {v7, v10}, Landroid/text/SpannableStringBuilder;-><init>(Ljava/lang/CharSequence;)V

    .restart local v7    # "title":Landroid/text/SpannableStringBuilder;
    goto/16 :goto_2
.end method

.method protected getSelectedMillis()Ljava/lang/Long;
    .locals 2

    .prologue
    .line 230
    invoke-virtual {p0}, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->getSelectedTime()Landroid/text/format/Time;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->normalize(Z)J

    move-result-wide v0

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method protected getSelectedTime()Landroid/text/format/Time;
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 221
    iget-object v0, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTempTime:Landroid/text/format/Time;

    .line 222
    .local v0, "time":Landroid/text/format/Time;
    iget-object v1, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mTime:Landroid/text/format/Time;

    invoke-virtual {v0, v1}, Landroid/text/format/Time;->set(Landroid/text/format/Time;)V

    .line 223
    iput v2, v0, Landroid/text/format/Time;->hour:I

    .line 224
    iput v2, v0, Landroid/text/format/Time;->minute:I

    .line 225
    iput v2, v0, Landroid/text/format/Time;->second:I

    .line 226
    return-object v0
.end method

.method protected isToday(J)Z
    .locals 5
    .param p1, "when"    # J

    .prologue
    const/4 v3, 0x0

    .line 204
    new-instance v0, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v0, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .line 205
    .local v0, "time":Landroid/text/format/Time;
    invoke-virtual {v0, p1, p2}, Landroid/text/format/Time;->set(J)V

    .line 208
    invoke-static {}, Lcom/sec/android/widgetapp/SPlannerAppWidget/preference/SPlannerPreferenceManager;->usedSystemTimezone()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 209
    new-instance v1, Landroid/text/format/Time;

    invoke-direct {v1}, Landroid/text/format/Time;-><init>()V

    .line 214
    .local v1, "today":Landroid/text/format/Time;
    :goto_0
    invoke-virtual {v1}, Landroid/text/format/Time;->setToNow()V

    .line 216
    iget v2, v1, Landroid/text/format/Time;->year:I

    iget v3, v0, Landroid/text/format/Time;->year:I

    if-ne v2, v3, :cond_1

    iget v2, v1, Landroid/text/format/Time;->month:I

    iget v3, v0, Landroid/text/format/Time;->month:I

    if-ne v2, v3, :cond_1

    iget v2, v1, Landroid/text/format/Time;->monthDay:I

    iget v3, v0, Landroid/text/format/Time;->monthDay:I

    if-ne v2, v3, :cond_1

    const/4 v2, 0x1

    :goto_1
    return v2

    .line 211
    .end local v1    # "today":Landroid/text/format/Time;
    :cond_0
    new-instance v1, Landroid/text/format/Time;

    iget-object v2, p0, Lcom/sec/android/widgetapp/SPlannerAppWidget/minitodaywidget/BaseMiniTodayThemeImple;->mSurfaceWidget:Lcom/sec/android/widgetapp/SPlannerAppWidget/SPlannerSurfaceWidget;

    invoke-static {v2, v3}, Lcom/sec/android/widgetapp/SPlannerAppWidget/utils/Utils;->getTimeZone(Landroid/content/Context;Ljava/lang/Runnable;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/text/format/Time;-><init>(Ljava/lang/String;)V

    .restart local v1    # "today":Landroid/text/format/Time;
    goto :goto_0

    .line 216
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method
