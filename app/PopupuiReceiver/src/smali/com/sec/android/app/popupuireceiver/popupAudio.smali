.class public Lcom/sec/android/app/popupuireceiver/popupAudio;
.super Landroid/app/Activity;
.source "popupAudio.java"


# instance fields
.field private mAudioManager:Landroid/media/AudioManager;

.field mSaveZoneBR:Landroid/content/BroadcastReceiver;

.field private nAudioFormat:I

.field private nAudioSampleRate:I

.field private nAudiochannels:I

.field private nAudiostate:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 39
    new-instance v0, Lcom/sec/android/app/popupuireceiver/popupAudio$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/popupAudio$1;-><init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->mSaveZoneBR:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupuireceiver/popupAudio;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I

    return v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/popupuireceiver/popupAudio;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I

    return p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I

    return v0
.end method

.method static synthetic access$102(Lcom/sec/android/app/popupuireceiver/popupAudio;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I

    return p1
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/popupAudio;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I

    return v0
.end method

.method static synthetic access$202(Lcom/sec/android/app/popupuireceiver/popupAudio;I)I
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;
    .param p1, "x1"    # I

    .prologue
    .line 13
    iput p1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I

    return p1
.end method

.method static synthetic access$300(Lcom/sec/android/app/popupuireceiver/popupAudio;)I
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;

    .prologue
    .line 13
    iget v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioSampleRate:I

    return v0
.end method

.method static synthetic access$400(Lcom/sec/android/app/popupuireceiver/popupAudio;)Landroid/media/AudioManager;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupAudio;

    .prologue
    .line 13
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->mAudioManager:Landroid/media/AudioManager;

    return-object v0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, -0x1

    .line 56
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 57
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->requestWindowFeature(I)Z

    .line 58
    const/high16 v2, 0x7f030000

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->setContentView(I)V

    .line 60
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hdmi_audio_output"

    invoke-static {v2, v3, v5}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v1

    .line 62
    .local v1, "returnValue":I
    const-string v2, "audio"

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/media/AudioManager;

    iput-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->mAudioManager:Landroid/media/AudioManager;

    .line 64
    const-string v2, "PopupAudio"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "System.getInt(hdmi_audio_output) = "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 65
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 66
    .local v0, "intentParent":Landroid/content/Intent;
    const-string v2, "AudioFormat"

    const/16 v3, 0x10

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I

    .line 67
    const-string v2, "AudioSampleRate"

    const v3, 0xbb80

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    iput v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioSampleRate:I

    .line 68
    const-string v2, "PopupAudio"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onCreate  state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  channels : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " nAudioFormat:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " nAudioSampleRate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioSampleRate:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 70
    if-eq v1, v5, :cond_0

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->finish()V

    .line 74
    :cond_0
    const/4 v2, 0x2

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->showDialog(I)V

    .line 76
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 5
    .param p1, "id"    # I

    .prologue
    const/4 v3, 0x1

    .line 114
    iput v3, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I

    .line 115
    const/4 v1, 0x2

    iput v1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I

    .line 117
    const-string v1, "layout_inflater"

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/LayoutInflater;

    .line 119
    .local v0, "inflater":Landroid/view/LayoutInflater;
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-direct {v1, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f06000c

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const/high16 v2, 0x7f070000

    const/4 v3, 0x0

    new-instance v4, Lcom/sec/android/app/popupuireceiver/popupAudio$5;

    invoke-direct {v4, p0}, Lcom/sec/android/app/popupuireceiver/popupAudio$5;-><init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V

    invoke-virtual {v1, v2, v3, v4}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(IILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    new-instance v2, Lcom/sec/android/app/popupuireceiver/popupAudio$4;

    invoke-direct {v2, p0}, Lcom/sec/android/app/popupuireceiver/popupAudio$4;-><init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060007

    new-instance v3, Lcom/sec/android/app/popupuireceiver/popupAudio$3;

    invoke-direct {v3, p0}, Lcom/sec/android/app/popupuireceiver/popupAudio$3;-><init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f060009

    new-instance v3, Lcom/sec/android/app/popupuireceiver/popupAudio$2;

    invoke-direct {v3, p0}, Lcom/sec/android/app/popupuireceiver/popupAudio$2;-><init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V

    invoke-virtual {v1, v2, v3}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 81
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 82
    const/4 v0, 0x2

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->removeDialog(I)V

    .line 83
    return-void
.end method

.method public onPause()V
    .locals 2

    .prologue
    .line 96
    const-string v0, "PopupAudio"

    const-string v1, "onPause()"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 97
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 107
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->mSaveZoneBR:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 109
    return-void
.end method

.method public onResume()V
    .locals 3

    .prologue
    .line 87
    const-string v1, "PopupAudio"

    const-string v2, "onResume()"

    invoke-static {v1, v2}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 89
    new-instance v0, Landroid/content/IntentFilter;

    invoke-direct {v0}, Landroid/content/IntentFilter;-><init>()V

    .line 90
    .local v0, "filter":Landroid/content/IntentFilter;
    const-string v1, "android.intent.action.ACTION_HDMI_AUDIO_CH_POPUP"

    invoke-virtual {v0, v1}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio;->mSaveZoneBR:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 92
    return-void
.end method
