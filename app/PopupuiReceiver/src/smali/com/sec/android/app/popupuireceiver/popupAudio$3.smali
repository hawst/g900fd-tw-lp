.class Lcom/sec/android/app/popupuireceiver/popupAudio$3;
.super Ljava/lang/Object;
.source "popupAudio.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/popupAudio;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V
    .locals 0

    .prologue
    .line 146
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 6
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v5, 0x2

    .line 150
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v2

    if-ne v2, v5, :cond_0

    .line 151
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-virtual {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hdmi_audio_output"

    const/4 v4, 0x0

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 153
    const-string v2, "PopupAudio"

    const-string v3, "System.putInt(hdmi_audio_output) = 0"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 155
    :cond_0
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v2

    const/16 v3, 0x20

    if-ne v2, v3, :cond_1

    .line 156
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-virtual {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "hdmi_audio_output"

    const/4 v4, 0x1

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$System;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 158
    const-string v2, "PopupAudio"

    const-string v3, "System.putInt(hdmi_audio_output) = 1"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 162
    :cond_1
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v2

    if-eq v2, v5, :cond_2

    .line 163
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    const/16 v3, 0x10

    # setter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I
    invoke-static {v2, v3}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$202(Lcom/sec/android/app/popupuireceiver/popupAudio;I)I

    .line 165
    :cond_2
    const/4 v1, 0x0

    .line 167
    .local v1, "format24":I
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$200(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v2

    const/16 v3, 0x18

    if-eq v2, v3, :cond_3

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$200(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v2

    const/16 v3, 0x28

    if-ne v2, v3, :cond_4

    .line 168
    :cond_3
    const/4 v1, 0x1

    .line 171
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I
    invoke-static {v3}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":48000:48000:0:0:"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioSampleRate:I
    invoke-static {v3}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$300(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ":"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I
    invoke-static {v3}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$200(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 172
    .local v0, "chPlusedName":Ljava/lang/String;
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->mAudioManager:Landroid/media/AudioManager;
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$400(Lcom/sec/android/app/popupuireceiver/popupAudio;)Landroid/media/AudioManager;

    move-result-object v2

    const/16 v3, 0x400

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I
    invoke-static {v4}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$000(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v4

    invoke-virtual {v2, v3, v4, v0}, Landroid/media/AudioManager;->setWiredDeviceConnectionState(IILjava/lang/String;)V

    .line 174
    const-string v2, "PopupAudio"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "sendBroadcast : ACTION_HDMI_AUDIO  state : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I
    invoke-static {v4}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$000(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, "  channels : "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiochannels:I
    invoke-static {v4}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$100(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " nAudioFormat:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioFormat:I
    invoke-static {v4}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$200(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " nAudioSampleRate:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudioSampleRate:I
    invoke-static {v4}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$300(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 177
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-virtual {v2}, Lcom/sec/android/app/popupuireceiver/popupAudio;->finish()V

    .line 178
    return-void
.end method
