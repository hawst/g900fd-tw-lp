.class public Lcom/sec/android/app/popupuireceiver/SviewCover;
.super Landroid/app/Activity;
.source "SviewCover.java"


# instance fields
.field private isNAOperator:Z

.field private mDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;

.field private mShowSviewCoverPopup:Z

.field private mSviewCoverTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 37
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 43
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    .line 45
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mShowSviewCoverPopup:Z

    .line 46
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    .line 103
    new-instance v0, Lcom/sec/android/app/popupuireceiver/SviewCover$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/SviewCover$1;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCover;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/popupuireceiver/SviewCover;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCover;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 37
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupuireceiver/SviewCover;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCover;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/SviewCover;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCover;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method private showPopupSviewCover()V
    .locals 24

    .prologue
    .line 110
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    if-eqz v20, :cond_0

    .line 249
    :goto_0
    return-void

    .line 112
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->isChinaModel()Z

    move-result v20

    if-eqz v20, :cond_1

    .line 113
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    const-string v21, "SVIEW_COVER_POPUP"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v20

    const-string v21, "originalSviewCover"

    const/16 v22, 0x1

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v11

    .line 114
    .local v11, "isOriginalSviewCover":Z
    const-string v20, "PopupuiReceiver"

    new-instance v21, Ljava/lang/StringBuilder;

    invoke-direct/range {v21 .. v21}, Ljava/lang/StringBuilder;-><init>()V

    const-string v22, "showPopupSviewCover()+ isOriginalSviewCover : "

    invoke-virtual/range {v21 .. v22}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v21

    move-object/from16 v0, v21

    invoke-virtual {v0, v11}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v21

    invoke-virtual/range {v21 .. v21}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 115
    if-nez v11, :cond_1

    .line 116
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->finish()V

    goto :goto_0

    .line 121
    .end local v11    # "isOriginalSviewCover":Z
    :cond_1
    const-string v20, "VZW"

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "KVZW"

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "SPR"

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-nez v20, :cond_2

    const-string v20, "KSPORTSPR"

    const-string v21, ""

    invoke-virtual/range {v20 .. v21}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_3

    .line 125
    :cond_2
    const/16 v20, 0x1

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    .line 126
    const-string v20, "PopupuiReceiver"

    const-string v21, "SPR, VZW Enable+"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 129
    :cond_3
    const/4 v12, 0x0

    .line 130
    .local v12, "isTalkbackOn":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getBaseContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    .line 131
    .local v9, "cr":Landroid/content/ContentResolver;
    const-string v20, "enabled_accessibility_services"

    move-object/from16 v0, v20

    invoke-static {v9, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v18

    .line 132
    .local v18, "talkbackString":Ljava/lang/String;
    const-string v20, "accessibility_enabled"

    const/16 v21, 0x0

    move-object/from16 v0, v20

    move/from16 v1, v21

    invoke-static {v9, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v20

    const/16 v21, 0x1

    move/from16 v0, v20

    move/from16 v1, v21

    if-ne v0, v1, :cond_4

    if-eqz v18, :cond_4

    const-string v20, "(?i).*talkback.*"

    move-object/from16 v0, v18

    move-object/from16 v1, v20

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v20

    if-eqz v20, :cond_4

    .line 134
    const/4 v12, 0x1

    .line 137
    :cond_4
    const-string v20, "PopupuiReceiver"

    const-string v21, "showPopupSviewCover()+"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 139
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 140
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getBaseContext()Landroid/content/Context;

    move-result-object v20

    const v21, 0x7f030002

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 152
    .local v14, "popupView":Landroid/view/View;
    :goto_1
    new-instance v20, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v20 .. v20}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v20

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v14}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 155
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v20

    new-instance v21, Landroid/graphics/drawable/ColorDrawable;

    const/16 v22, 0x0

    invoke-direct/range {v21 .. v22}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual/range {v20 .. v21}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 156
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    new-instance v21, Lcom/sec/android/app/popupuireceiver/SviewCover$3;

    move-object/from16 v0, v21

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCover$3;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCover;)V

    invoke-virtual/range {v20 .. v21}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 165
    const v20, 0x7f090001

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 166
    .local v10, "imgView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    move/from16 v20, v0

    if-nez v20, :cond_8

    .line 167
    if-nez v12, :cond_5

    .line 168
    new-instance v20, Landroid/os/Handler;

    invoke-direct/range {v20 .. v20}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    .line 169
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    move-object/from16 v21, v0

    const-wide/16 v22, 0xfa0

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 171
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    const/16 v21, 0x1

    invoke-virtual/range {v20 .. v21}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 172
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/AlertDialog;->show()V

    .line 173
    if-eqz v10, :cond_6

    .line 174
    const v20, 0x7f02001f

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 175
    invoke-virtual {v10}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    .line 176
    .local v4, "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 178
    .end local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_6
    const v20, 0x7f090002

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 179
    .local v17, "sviewCoverText":Landroid/widget/TextView;
    const v20, 0x7f06003a

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 180
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getBaseContext()Landroid/content/Context;

    move-result-object v20

    invoke-virtual/range {v20 .. v20}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 181
    .local v15, "res":Landroid/content/res/Resources;
    const v20, 0x7f050005

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v19

    .line 182
    .local v19, "topPadding":I
    const v20, 0x7f050006

    move/from16 v0, v20

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v6

    .line 183
    .local v6, "bottomPadding":I
    const/16 v20, 0x0

    const/16 v21, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v20

    move/from16 v2, v19

    move/from16 v3, v21

    invoke-virtual {v0, v1, v2, v3, v6}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 142
    .end local v6    # "bottomPadding":I
    .end local v10    # "imgView":Landroid/widget/ImageView;
    .end local v14    # "popupView":Landroid/view/View;
    .end local v15    # "res":Landroid/content/res/Resources;
    .end local v17    # "sviewCoverText":Landroid/widget/TextView;
    .end local v19    # "topPadding":I
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getBaseContext()Landroid/content/Context;

    move-result-object v20

    const v21, 0x7f030001

    const/16 v22, 0x0

    invoke-static/range {v20 .. v22}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 143
    .restart local v14    # "popupView":Landroid/view/View;
    new-instance v20, Lcom/sec/android/app/popupuireceiver/SviewCover$2;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCover$2;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCover;)V

    move-object/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 186
    .restart local v10    # "imgView":Landroid/widget/ImageView;
    :cond_8
    const-string v20, "SviewCover"

    const-string v21, "NA WaterProofPopup Concept"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 187
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    const-string v21, "SVIEW_COVER_POPUP"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v20

    const-string v21, "show_sviewcover_popup"

    const/16 v22, 0x1

    invoke-interface/range {v20 .. v22}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v20

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->mShowSviewCoverPopup:Z

    .line 188
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mShowSviewCoverPopup:Z

    move/from16 v20, v0

    if-eqz v20, :cond_e

    .line 190
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Landroid/app/AlertDialog;->show()V

    .line 191
    if-eqz v10, :cond_9

    .line 192
    const v20, 0x7f020020

    move/from16 v0, v20

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 193
    invoke-virtual {v10}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    .line 194
    .restart local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 196
    .end local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_9
    const-string v20, "SviewCover"

    const-string v21, "NA Show WaterProofPopup"

    invoke-static/range {v20 .. v21}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 197
    const v20, 0x7f090002

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v17

    check-cast v17, Landroid/widget/TextView;

    .line 198
    .restart local v17    # "sviewCoverText":Landroid/widget/TextView;
    const v20, 0x7f06003a

    move-object/from16 v0, v17

    move/from16 v1, v20

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(I)V

    .line 199
    const v20, 0x7f090003

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/LinearLayout;

    .line 200
    .local v8, "checkboxlayout":Landroid/widget/LinearLayout;
    const v20, 0x7f090004

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/CheckBox;

    .line 201
    .local v7, "checkBox":Landroid/widget/CheckBox;
    const v20, 0x7f090005

    move/from16 v0, v20

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    .line 202
    .local v13, "okBtn":Landroid/widget/Button;
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 203
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 204
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v20

    const-string v21, "SVIEW_COVER_POPUP"

    const/16 v22, 0x0

    invoke-virtual/range {v20 .. v22}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 205
    .local v16, "sp":Landroid/content/SharedPreferences;
    const-string v20, "show_sviewcover_count"

    const/16 v21, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v20

    move/from16 v2, v21

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 206
    .local v5, "bootCount":I
    const/16 v20, 0x1

    move/from16 v0, v20

    if-ne v5, v0, :cond_a

    .line 207
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 208
    const v20, 0x7f060007

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    .line 225
    :goto_2
    new-instance v20, Lcom/sec/android/app/popupuireceiver/SviewCover$4;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCover$4;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCover;)V

    move-object/from16 v0, v20

    invoke-virtual {v7, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 209
    :cond_a
    const/16 v20, 0x4

    move/from16 v0, v20

    if-ge v5, v0, :cond_c

    .line 210
    const/16 v20, 0x8

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 211
    if-nez v12, :cond_b

    .line 212
    new-instance v20, Landroid/os/Handler;

    invoke-direct/range {v20 .. v20}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    .line 213
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x1770

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 215
    :cond_b
    const v20, 0x7f060008

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 217
    :cond_c
    const/16 v20, 0x0

    move/from16 v0, v20

    invoke-virtual {v8, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 218
    if-nez v12, :cond_d

    .line 219
    new-instance v20, Landroid/os/Handler;

    invoke-direct/range {v20 .. v20}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    move-object/from16 v21, v0

    const-wide/16 v22, 0x1770

    invoke-virtual/range {v20 .. v23}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 222
    :cond_d
    const v20, 0x7f060008

    move/from16 v0, v20

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 246
    .end local v5    # "bootCount":I
    .end local v7    # "checkBox":Landroid/widget/CheckBox;
    .end local v8    # "checkboxlayout":Landroid/widget/LinearLayout;
    .end local v13    # "okBtn":Landroid/widget/Button;
    .end local v16    # "sp":Landroid/content/SharedPreferences;
    .end local v17    # "sviewCoverText":Landroid/widget/TextView;
    :cond_e
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->finish()V

    goto/16 :goto_0
.end method


# virtual methods
.method public closeBatterycoverPopup(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 252
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->finish()V

    .line 253
    return-void
.end method

.method isChinaModel()Z
    .locals 2

    .prologue
    .line 280
    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 281
    const/4 v0, 0x1

    .line 284
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 52
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 53
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->setContentView(I)V

    .line 55
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCover;->setupIntentValue(Landroid/content/Intent;Z)V

    .line 56
    return-void
.end method

.method protected onDestroy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 258
    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mShowSviewCoverPopup:Z

    if-eqz v3, :cond_0

    .line 259
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "SVIEW_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 260
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v3, "show_sviewcover_count"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 261
    .local v0, "bootCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "SVIEW_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 262
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "show_sviewcover_count"

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 263
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 266
    .end local v0    # "bootCount":I
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 267
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 268
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_2

    .line 269
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 270
    iput-object v7, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mDialog:Landroid/app/AlertDialog;

    .line 272
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mShowSviewCoverPopup:Z

    .line 273
    iput-boolean v5, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->isNAOperator:Z

    .line 274
    iput-object v7, p0, Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;

    .line 275
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 277
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 61
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 63
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->setupIntentValue(Landroid/content/Intent;Z)V

    .line 64
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 3
    .param p1, "hasFocus"    # Z

    .prologue
    .line 82
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v0

    .line 84
    .local v0, "userId":I
    const-string v1, "FINISH"

    const-string v2, "persist.sys.setupwizard"

    invoke-static {v2}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_1

    if-nez v0, :cond_1

    .line 86
    const-string v1, "PopupuiReceiver"

    const-string v2, "onWindowFocusChanged()+ in FINISH so return"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 88
    if-eqz p1, :cond_0

    .line 90
    const-string v1, "PopupuiReceiver"

    const-string v2, "onWindowFocusChanged()+ in to show"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    invoke-direct {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->showPopupSviewCover()V

    .line 100
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 101
    :goto_0
    return-void

    .line 95
    :cond_1
    const-string v1, "PopupuiReceiver"

    const-string v2, "onWindowFocusChanged()+ in Invoke finish()"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->finish()V

    goto :goto_0
.end method

.method setupIntentValue(Landroid/content/Intent;Z)V
    .locals 5
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "newIntent"    # Z

    .prologue
    .line 67
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->isChinaModel()Z

    move-result v2

    if-eqz v2, :cond_0

    if-nez p1, :cond_1

    .line 76
    :cond_0
    :goto_0
    return-void

    .line 71
    :cond_1
    const-string v2, "verified"

    const/4 v3, 0x1

    invoke-virtual {p1, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    .line 73
    .local v1, "originalSviewCover":Z
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "SVIEW_COVER_POPUP"

    const/4 v4, 0x0

    invoke-virtual {v2, v3, v4}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 74
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v2, "originalSviewCover"

    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 75
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
