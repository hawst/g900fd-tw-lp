.class Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;
.super Ljava/lang/Object;
.source "popupPowerSharing.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/popupPowerSharing;)V
    .locals 0

    .prologue
    .line 71
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v6, 0x1

    .line 75
    :try_start_0
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    iget-object v5, v5, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->checkboxAgain:Landroid/widget/CheckBox;

    invoke-virtual {v5}, Landroid/widget/CheckBox;->isChecked()Z
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_1

    move-result v5

    if-ne v5, v6, :cond_0

    .line 79
    :try_start_1
    const-string v5, "PopupuiReceiver"

    const-string v6, "Check true save to file"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 80
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    const-string v6, "CheckShow"

    const/4 v7, 0x1

    invoke-virtual {v5, v6, v7}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 81
    .local v0, "FirstBoot":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 82
    .local v1, "FirstBootEdit":Landroid/content/SharedPreferences$Editor;
    const-string v5, "CheckShow"

    const/4 v6, 0x1

    invoke-interface {v1, v5, v6}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 83
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 87
    const-string v5, "PopupuiReceiver"

    const-string v6, "Save to check boolean  : CheckShow"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_1 .. :try_end_1} :catch_1

    .line 94
    .end local v0    # "FirstBoot":Landroid/content/SharedPreferences;
    .end local v1    # "FirstBootEdit":Landroid/content/SharedPreferences$Editor;
    :cond_0
    :goto_0
    :try_start_2
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    const/high16 v6, 0x7f060000

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_package_name:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->access$002(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    const v6, 0x7f060001

    invoke-virtual {v5, v6}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->getText(I)Ljava/lang/CharSequence;

    move-result-object v5

    check-cast v5, Ljava/lang/String;

    # setter for: Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_class_name:Ljava/lang/String;
    invoke-static {v5}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->access$102(Ljava/lang/String;)Ljava/lang/String;

    .line 97
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    .line 98
    .local v2, "call_intent":Landroid/content/Intent;
    # getter for: Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_package_name:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->access$000()Ljava/lang/String;

    move-result-object v5

    # getter for: Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_class_name:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->access$100()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v5, v6}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 99
    const/high16 v5, 0x10000000

    invoke-virtual {v2, v5}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 101
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    invoke-virtual {v5, v2}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->startActivity(Landroid/content/Intent;)V

    .line 102
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    invoke-virtual {v5}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->finish()V

    .line 108
    .end local v2    # "call_intent":Landroid/content/Intent;
    :goto_1
    return-void

    .line 88
    :catch_0
    move-exception v4

    .line 89
    .local v4, "s2":Ljava/lang/Exception;
    const-string v5, "PopupuiReceiver"

    const-string v6, "Check box preferenced failed!"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/IllegalArgumentException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_0

    .line 104
    .end local v4    # "s2":Ljava/lang/Exception;
    :catch_1
    move-exception v3

    .line 105
    .local v3, "e":Ljava/lang/IllegalArgumentException;
    const-string v5, "PopupuiReceiver"

    const-string v6, "unknown package"

    invoke-static {v5, v6}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;->this$0:Lcom/sec/android/app/popupuireceiver/popupPowerSharing;

    invoke-virtual {v5}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->finish()V

    goto :goto_1
.end method
