.class public Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;
.super Landroid/app/Activity;
.source "popupCustomizeKey.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 15
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 21
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 22
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;->requestWindowFeature(I)Z

    .line 23
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;->setContentView(I)V

    .line 25
    const/4 v0, 0x3

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;->showDialog(I)V

    .line 27
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 6
    .param p1, "id"    # I

    .prologue
    .line 31
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 32
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v3, 0x7f030007

    const/4 v4, 0x0

    invoke-virtual {v0, v3, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v2

    .line 35
    .local v2, "popupEntryView":Landroid/view/View;
    const v3, 0x7f090008

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 36
    .local v1, "msg":Landroid/widget/TextView;
    const v3, 0x7f060011

    invoke-virtual {v1, v3}, Landroid/widget/TextView;->setText(I)V

    .line 38
    new-instance v3, Landroid/app/AlertDialog$Builder;

    invoke-direct {v3, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v4, 0x7f060010

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060012

    new-instance v5, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$3;

    invoke-direct {v5, p0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$3;-><init>(Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    new-instance v4, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$2;

    invoke-direct {v4, p0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$2;-><init>(Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;)V

    invoke-virtual {v3, v4}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    const v4, 0x7f060009

    new-instance v5, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$1;

    invoke-direct {v5, p0}, Lcom/sec/android/app/popupuireceiver/popupCustomizeKey$1;-><init>(Lcom/sec/android/app/popupuireceiver/popupCustomizeKey;)V

    invoke-virtual {v3, v4, v5}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    return-object v3
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 67
    return-void
.end method
