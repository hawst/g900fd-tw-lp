.class Lcom/sec/android/app/popupuireceiver/BatteryCover$4;
.super Ljava/lang/Object;
.source "BatteryCover.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/BatteryCover;->showPopupBatteryCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/BatteryCover;)V
    .locals 0

    .prologue
    .line 192
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v3, 0x0

    .line 195
    if-eqz p2, :cond_1

    .line 196
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->access$100(Lcom/sec/android/app/popupuireceiver/BatteryCover;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 197
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->access$100(Lcom/sec/android/app/popupuireceiver/BatteryCover;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->access$200(Lcom/sec/android/app/popupuireceiver/BatteryCover;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 199
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "BATTERY_COVER_POPUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 200
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "show_batterycover_popup"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 201
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 208
    :goto_0
    return-void

    .line 204
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/BatteryCover;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "BATTERY_COVER_POPUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 205
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "show_batterycover_popup"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 206
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
