.class public Lcom/sec/android/app/popupuireceiver/popupPowerSharing;
.super Landroid/app/Activity;
.source "popupPowerSharing.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static SLinks_class_name:Ljava/lang/String;

.field private static SLinks_package_name:Ljava/lang/String;


# instance fields
.field checkboxAgain:Landroid/widget/CheckBox;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_package_name:Ljava/lang/String;

    .line 25
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_class_name:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 19
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_package_name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$002(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 19
    sput-object p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_package_name:Ljava/lang/String;

    return-object p0
.end method

.method static synthetic access$100()Ljava/lang/String;
    .locals 1

    .prologue
    .line 19
    sget-object v0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_class_name:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$102(Ljava/lang/String;)Ljava/lang/String;
    .locals 0
    .param p0, "x0"    # Ljava/lang/String;

    .prologue
    .line 19
    sput-object p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->SLinks_class_name:Ljava/lang/String;

    return-object p0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 31
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 32
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->requestWindowFeature(I)Z

    .line 33
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->setContentView(I)V

    .line 36
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->showDialog(I)V

    .line 38
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 11
    .param p1, "id"    # I

    .prologue
    .line 42
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v5

    .line 43
    .local v5, "factory":Landroid/view/LayoutInflater;
    const v9, 0x7f030005

    const/4 v10, 0x0

    invoke-virtual {v5, v9, v10}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v7

    .line 49
    .local v7, "popupEntryView":Landroid/view/View;
    const-string v8, "Power Sharing"

    .line 51
    .local v8, "title":Ljava/lang/String;
    const v1, 0x7f060007

    .line 52
    .local v1, "btnPositiveText":I
    const v0, 0x7f060009

    .line 55
    .local v0, "btnNegativeText":I
    const v9, 0x7f090009

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/CheckBox;

    iput-object v9, p0, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->checkboxAgain:Landroid/widget/CheckBox;

    .line 58
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f06003f

    invoke-virtual {v9, v10}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 59
    .local v3, "description":Ljava/lang/String;
    const v9, 0x7f090008

    invoke-virtual {v7, v9}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/TextView;

    .line 62
    .local v6, "msg":Landroid/widget/TextView;
    invoke-virtual {v6, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    new-instance v9, Landroid/app/AlertDialog$Builder;

    invoke-direct {v9, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v8}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    invoke-virtual {v9, v7}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    new-instance v10, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$1;

    invoke-direct {v10, p0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$1;-><init>(Lcom/sec/android/app/popupuireceiver/popupPowerSharing;)V

    invoke-virtual {v9, v0, v10}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    .line 71
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v9, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;

    invoke-direct {v9, p0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$2;-><init>(Lcom/sec/android/app/popupuireceiver/popupPowerSharing;)V

    invoke-virtual {v2, v1, v9}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 110
    new-instance v9, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$3;

    invoke-direct {v9, p0}, Lcom/sec/android/app/popupuireceiver/popupPowerSharing$3;-><init>(Lcom/sec/android/app/popupuireceiver/popupPowerSharing;)V

    invoke-virtual {v2, v9}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 118
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    .line 119
    .local v4, "dlg":Landroid/app/AlertDialog;
    return-object v4
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 127
    return-void
.end method
