.class public Lcom/sec/android/app/popupuireceiver/popupNetworkError;
.super Landroid/app/Activity;
.source "popupNetworkError.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static isChecked:Z

.field private static mobileDataNetworkOnly:Z

.field private static popupType:I

.field private static supportVoLTE:Z


# instance fields
.field private dlg:Landroid/app/AlertDialog;

.field private mImsInterface:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;

.field private mTemplate:Landroid/net/NetworkTemplate;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 44
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 83
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mImsInterface:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;

    return-void
.end method

.method static synthetic access$000()Z
    .locals 1

    .prologue
    .line 44
    sget-boolean v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z

    return v0
.end method

.method static synthetic access$002(Z)Z
    .locals 0
    .param p0, "x0"    # Z

    .prologue
    .line 44
    sput-boolean p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z

    return p0
.end method

.method static synthetic access$100()I
    .locals 1

    .prologue
    .line 44
    sget v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    return v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/popupNetworkError;)V
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    .prologue
    .line 44
    invoke-direct {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->setCdmaGsmDataRoamingEnabled()V

    return-void
.end method

.method private setCdmaGsmDataRoamingEnabled()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 119
    invoke-static {}, Lcom/android/internal/telephony/PhoneFactory;->getDefaultPhone()Lcom/android/internal/telephony/Phone;

    move-result-object v0

    .line 120
    .local v0, "phone":Lcom/android/internal/telephony/Phone;
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getPhoneType()I

    move-result v1

    .line 122
    .local v1, "phoneType":I
    const/4 v2, 0x2

    if-ne v1, v2, :cond_2

    .line 123
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->IsDomesticRoaming()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 124
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "roam_setting_data_domestic"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 137
    :cond_0
    :goto_0
    return-void

    .line 125
    :cond_1
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->IsInternationalRoaming()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 126
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v2

    const-string v3, "roam_setting_data_international"

    invoke-static {v2, v3, v4}, Landroid/provider/Settings$Secure;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    goto :goto_0

    .line 127
    :cond_2
    if-eq v1, v4, :cond_3

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 129
    :cond_3
    invoke-interface {v0}, Lcom/android/internal/telephony/Phone;->getServiceState()Landroid/telephony/ServiceState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/telephony/ServiceState;->getRoaming()Z

    move-result v2

    if-eqz v2, :cond_0

    goto :goto_0

    .line 135
    :cond_4
    new-instance v2, Ljava/lang/IllegalStateException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected phone type: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v5, 0x0

    const/4 v4, 0x1

    .line 90
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 91
    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->requestWindowFeature(I)Z

    .line 92
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->setContentView(I)V

    .line 94
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 95
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "network_err_type"

    const/4 v3, -0x1

    invoke-virtual {v1, v2, v3}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v2

    sput v2, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    .line 96
    const-string v2, "mobile_data_only"

    invoke-virtual {v1, v2, v5}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    sput-boolean v2, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    .line 99
    const/4 v2, 0x1

    :try_start_0
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getApplication()Landroid/app/Application;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/samsung/commonimsinterface/imsinterface/CommonIMSInterface;->getInstance(ILandroid/content/Context;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;

    iput-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mImsInterface:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 107
    :goto_0
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mImsInterface:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;

    if-eqz v2, :cond_0

    .line 108
    sput-boolean v4, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->supportVoLTE:Z

    .line 111
    :goto_1
    const-string v2, "PopupuiReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Data Connection Fail : popupType:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget v4, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " mobileDataNetworkOnly:"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-boolean v4, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 113
    sget v2, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->showDialog(I)V

    .line 116
    return-void

    .line 102
    :catch_0
    move-exception v0

    .line 104
    .local v0, "e":Ljava/lang/Exception;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mImsInterface:Lcom/samsung/commonimsinterface/imsinterface/IMSInterfaceForPresence;

    .line 105
    invoke-virtual {v0}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_0

    .line 110
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    sput-boolean v5, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->supportVoLTE:Z

    goto :goto_1
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 33
    .param p1, "id"    # I

    .prologue
    .line 150
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v14

    .line 151
    .local v14, "factory":Landroid/view/LayoutInflater;
    const v30, 0x7f030008

    const/16 v31, 0x0

    move/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v14, v0, v1}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v25

    .line 156
    .local v25, "popupEntryView":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060015

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 158
    .local v28, "title":Ljava/lang/String;
    const v8, 0x7f060007

    .line 159
    .local v8, "btnPositiveText":I
    const v7, 0x7f06002d

    .line 160
    .local v7, "btnNegativeText":I
    const/4 v11, 0x0

    .line 162
    .local v11, "checkBoxText":Ljava/lang/String;
    const/4 v5, 0x0

    .line 165
    .local v5, "addNegativeButton":Z
    packed-switch p1, :pswitch_data_0

    .line 272
    const-string v30, "PopupuiReceiver"

    const-string v31, "Data Connection Fail : invalid value"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 273
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->finish()V

    .line 274
    const/16 v30, 0x0

    .line 420
    :goto_0
    return-object v30

    .line 167
    :pswitch_0
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    if-eqz v30, :cond_4

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060019

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 170
    .local v12, "description":Ljava/lang/String;
    :goto_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001d

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 171
    const/4 v3, 0x0

    .line 277
    .local v3, "addCheckBox":Z
    :goto_2
    const v30, 0x7f090008

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v20

    check-cast v20, Landroid/widget/TextView;

    .line 278
    .local v20, "msg":Landroid/widget/TextView;
    move-object/from16 v0, v20

    invoke-virtual {v0, v12}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 280
    const v30, 0x7f090007

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/CheckBox;

    .line 282
    .local v10, "checkBox":Landroid/widget/CheckBox;
    invoke-virtual {v10, v11}, Landroid/widget/CheckBox;->setText(Ljava/lang/CharSequence;)V

    .line 284
    const v30, 0x7f09000a

    move-object/from16 v0, v25

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    .line 285
    .local v4, "addMsg":Landroid/widget/TextView;
    const/16 v30, 0x2

    move/from16 v0, p1

    move/from16 v1, v30

    if-ne v0, v1, :cond_10

    .line 286
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060027

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 291
    :cond_0
    :goto_3
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z

    if-eqz v30, :cond_12

    .line 292
    const/16 v30, 0x1

    move/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 293
    const/16 v30, 0x2

    move/from16 v0, p1

    move/from16 v1, v30

    if-eq v0, v1, :cond_1

    const/16 v30, 0x1

    move/from16 v0, p1

    move/from16 v1, v30

    if-ne v0, v1, :cond_11

    .line 294
    :cond_1
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 302
    :goto_4
    new-instance v23, Lcom/sec/android/app/popupuireceiver/popupNetworkError$1;

    move-object/from16 v0, v23

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct {v0, v1, v10, v2, v4}, Lcom/sec/android/app/popupuireceiver/popupNetworkError$1;-><init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;Landroid/widget/CheckBox;ILandroid/widget/TextView;)V

    .line 320
    .local v23, "onCheckClick":Landroid/view/View$OnClickListener;
    move-object/from16 v0, v23

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 322
    if-eqz v3, :cond_13

    .line 323
    invoke-virtual {v10}, Landroid/widget/CheckBox;->getParent()Landroid/view/ViewParent;

    move-result-object v30

    check-cast v30, Landroid/view/View;

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/view/View;->setVisibility(I)V

    .line 324
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 332
    :goto_5
    const-string v30, "enterprise_policy"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v19

    check-cast v19, Landroid/app/enterprise/EnterpriseDeviceManager;

    .line 334
    .local v19, "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    if-eqz v19, :cond_2

    .line 335
    invoke-virtual/range {v19 .. v19}, Landroid/app/enterprise/EnterpriseDeviceManager;->getRoamingPolicy()Landroid/app/enterprise/RoamingPolicy;

    move-result-object v30

    invoke-virtual/range {v30 .. v30}, Landroid/app/enterprise/RoamingPolicy;->isRoamingDataEnabled()Z

    move-result v30

    move/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setEnabled(Z)V

    .line 339
    :cond_2
    new-instance v30, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    move-object/from16 v0, v30

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v30

    const/16 v31, 0x0

    invoke-virtual/range {v30 .. v31}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v30

    new-instance v31, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    move/from16 v2, p1

    invoke-direct {v0, v1, v2, v3}, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;-><init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;IZ)V

    move-object/from16 v0, v30

    move-object/from16 v1, v31

    invoke-virtual {v0, v8, v1}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v30

    new-instance v31, Lcom/sec/android/app/popupuireceiver/popupNetworkError$2;

    move-object/from16 v0, v31

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError$2;-><init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;)V

    invoke-virtual/range {v30 .. v31}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v9

    .line 400
    .local v9, "builder":Landroid/app/AlertDialog$Builder;
    if-eqz v5, :cond_3

    .line 401
    new-instance v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError$4;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError$4;-><init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v7, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 409
    :cond_3
    new-instance v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError$5;

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError$5;-><init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;)V

    move-object/from16 v0, v30

    invoke-virtual {v9, v0}, Landroid/app/AlertDialog$Builder;->setOnKeyListener(Landroid/content/DialogInterface$OnKeyListener;)Landroid/app/AlertDialog$Builder;

    .line 419
    invoke-virtual {v9}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->dlg:Landroid/app/AlertDialog;

    .line 420
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->dlg:Landroid/app/AlertDialog;

    move-object/from16 v30, v0

    goto/16 :goto_0

    .line 167
    .end local v3    # "addCheckBox":Z
    .end local v4    # "addMsg":Landroid/widget/TextView;
    .end local v9    # "builder":Landroid/app/AlertDialog$Builder;
    .end local v10    # "checkBox":Landroid/widget/CheckBox;
    .end local v12    # "description":Ljava/lang/String;
    .end local v19    # "mEDM":Landroid/app/enterprise/EnterpriseDeviceManager;
    .end local v20    # "msg":Landroid/widget/TextView;
    .end local v23    # "onCheckClick":Landroid/view/View$OnClickListener;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060018

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto/16 :goto_1

    .line 174
    :pswitch_1
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001a

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 175
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->supportVoLTE:Z

    if-eqz v30, :cond_5

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001b

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 178
    .restart local v12    # "description":Ljava/lang/String;
    :goto_6
    const/4 v3, 0x0

    .line 179
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 175
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001c

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_6

    .line 181
    :pswitch_2
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    if-eqz v30, :cond_6

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060023

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 184
    .restart local v12    # "description":Ljava/lang/String;
    :goto_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060021

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 185
    const/4 v3, 0x0

    .line 186
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 181
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001e

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_7

    .line 188
    :pswitch_3
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    if-eqz v30, :cond_7

    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060025

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 191
    .restart local v12    # "description":Ljava/lang/String;
    :goto_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060026

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 192
    const/4 v3, 0x0

    .line 193
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 188
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :cond_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060024

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    goto :goto_8

    .line 195
    :pswitch_4
    const-string v30, "netpolicy"

    invoke-static/range {v30 .. v30}, Landroid/os/ServiceManager;->getService(Ljava/lang/String;)Landroid/os/IBinder;

    move-result-object v30

    invoke-static/range {v30 .. v30}, Landroid/net/INetworkPolicyManager$Stub;->asInterface(Landroid/os/IBinder;)Landroid/net/INetworkPolicyManager;

    move-result-object v24

    .line 198
    .local v24, "policyService":Landroid/net/INetworkPolicyManager;
    const-string v30, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v27

    check-cast v27, Landroid/telephony/TelephonyManager;

    .line 199
    .local v27, "telephony":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v27 .. v27}, Landroid/telephony/TelephonyManager;->getSubscriberId()Ljava/lang/String;

    move-result-object v26

    .line 201
    .local v26, "subscriberID":Ljava/lang/String;
    invoke-static/range {v26 .. v26}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v30

    if-nez v30, :cond_b

    .line 202
    const/4 v15, 0x0

    .line 203
    .local v15, "hasPolicy":Z
    const/16 v21, 0x0

    .line 206
    .local v21, "networkPolicies":[Landroid/net/NetworkPolicy;
    :try_start_0
    invoke-interface/range {v24 .. v24}, Landroid/net/INetworkPolicyManager;->getNetworkPolicies()[Landroid/net/NetworkPolicy;
    :try_end_0
    .catch Landroid/os/RemoteException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v21

    .line 211
    :goto_9
    if-eqz v21, :cond_9

    .line 212
    move-object/from16 v6, v21

    .local v6, "arr$":[Landroid/net/NetworkPolicy;
    array-length v0, v6

    move/from16 v18, v0

    .local v18, "len$":I
    const/16 v16, 0x0

    .local v16, "i$":I
    :goto_a
    move/from16 v0, v16

    move/from16 v1, v18

    if-ge v0, v1, :cond_9

    aget-object v22, v6, v16

    .line 213
    .local v22, "networkPolicy":Landroid/net/NetworkPolicy;
    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/net/NetworkTemplate;->getSubscriberId()Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v26

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v30

    if-eqz v30, :cond_8

    move-object/from16 v0, v22

    iget-object v0, v0, Landroid/net/NetworkPolicy;->template:Landroid/net/NetworkTemplate;

    move-object/from16 v30, v0

    invoke-virtual/range {v30 .. v30}, Landroid/net/NetworkTemplate;->getMatchRule()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-ne v0, v1, :cond_8

    .line 215
    const/4 v15, 0x1

    .line 212
    :cond_8
    add-int/lit8 v16, v16, 0x1

    goto :goto_a

    .line 207
    .end local v6    # "arr$":[Landroid/net/NetworkPolicy;
    .end local v16    # "i$":I
    .end local v18    # "len$":I
    .end local v22    # "networkPolicy":Landroid/net/NetworkPolicy;
    :catch_0
    move-exception v13

    .line 208
    .local v13, "e1":Landroid/os/RemoteException;
    invoke-virtual {v13}, Landroid/os/RemoteException;->printStackTrace()V

    goto :goto_9

    .line 220
    .end local v13    # "e1":Landroid/os/RemoteException;
    :cond_9
    if-eqz v15, :cond_a

    .line 221
    new-instance v17, Landroid/content/Intent;

    invoke-direct/range {v17 .. v17}, Landroid/content/Intent;-><init>()V

    .line 222
    .local v17, "intent":Landroid/content/Intent;
    new-instance v30, Landroid/content/ComponentName;

    const-string v31, "com.android.systemui"

    const-string v32, "com.android.systemui.net.NetworkOverLimitActivity"

    invoke-direct/range {v30 .. v32}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    .line 224
    const/high16 v30, 0x10000000

    move-object/from16 v0, v17

    move/from16 v1, v30

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 225
    invoke-static/range {v26 .. v26}, Landroid/net/NetworkTemplate;->buildTemplateMobileAll(Ljava/lang/String;)Landroid/net/NetworkTemplate;

    move-result-object v30

    move-object/from16 v0, v30

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mTemplate:Landroid/net/NetworkTemplate;

    .line 226
    const-string v30, "android.net.NETWORK_TEMPLATE"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mTemplate:Landroid/net/NetworkTemplate;

    move-object/from16 v31, v0

    move-object/from16 v0, v17

    move-object/from16 v1, v30

    move-object/from16 v2, v31

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Landroid/os/Parcelable;)Landroid/content/Intent;

    .line 227
    move-object/from16 v0, p0

    move-object/from16 v1, v17

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->startActivity(Landroid/content/Intent;)V

    .line 228
    const-string v30, "PopupuiReceiver"

    const-string v31, "Data Connection Fail -> start NetworkOverLimitActivity"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 238
    .end local v15    # "hasPolicy":Z
    .end local v17    # "intent":Landroid/content/Intent;
    .end local v21    # "networkPolicies":[Landroid/net/NetworkPolicy;
    :goto_b
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->finish()V

    .line 239
    const/16 v30, 0x0

    goto/16 :goto_0

    .line 231
    .restart local v15    # "hasPolicy":Z
    .restart local v21    # "networkPolicies":[Landroid/net/NetworkPolicy;
    :cond_a
    const-string v30, "PopupuiReceiver"

    const-string v31, "Data Connection Fail -> hasPolicy false"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 235
    .end local v15    # "hasPolicy":Z
    .end local v21    # "networkPolicies":[Landroid/net/NetworkPolicy;
    :cond_b
    const-string v30, "PopupuiReceiver"

    const-string v31, "Data Connection Fail -> subscriberID is null"

    invoke-static/range {v30 .. v31}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_b

    .line 241
    .end local v24    # "policyService":Landroid/net/INetworkPolicyManager;
    .end local v26    # "subscriberID":Ljava/lang/String;
    .end local v27    # "telephony":Landroid/telephony/TelephonyManager;
    :pswitch_5
    const-string v30, "phone"

    move-object/from16 v0, p0

    move-object/from16 v1, v30

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v29

    check-cast v29, Landroid/telephony/TelephonyManager;

    .line 242
    .local v29, "tm":Landroid/telephony/TelephonyManager;
    invoke-virtual/range {v29 .. v29}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v30

    const/16 v31, 0x1

    move/from16 v0, v30

    move/from16 v1, v31

    if-eq v0, v1, :cond_c

    invoke-virtual/range {v29 .. v29}, Landroid/telephony/TelephonyManager;->getSimState()I

    move-result v30

    if-nez v30, :cond_d

    .line 244
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06002e

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 250
    .restart local v12    # "description":Ljava/lang/String;
    :goto_c
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    if-eqz v30, :cond_e

    const v8, 0x7f060007

    .line 253
    :goto_d
    const/4 v3, 0x0

    .line 254
    .restart local v3    # "addCheckBox":Z
    sget-boolean v30, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->mobileDataNetworkOnly:Z

    if-eqz v30, :cond_f

    const/4 v5, 0x0

    .line 255
    :goto_e
    goto/16 :goto_2

    .line 248
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06002b

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .restart local v12    # "description":Ljava/lang/String;
    goto :goto_c

    .line 250
    :cond_e
    const v8, 0x7f06002c

    goto :goto_d

    .line 254
    .restart local v3    # "addCheckBox":Z
    :cond_f
    const/4 v5, 0x1

    goto :goto_e

    .line 257
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    .end local v29    # "tm":Landroid/telephony/TelephonyManager;
    :pswitch_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06002f

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 258
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060030

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 259
    .restart local v12    # "description":Ljava/lang/String;
    const/4 v3, 0x0

    .line 260
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 262
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :pswitch_7
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06002b

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 263
    .restart local v12    # "description":Ljava/lang/String;
    const/4 v3, 0x0

    .line 264
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 266
    .end local v3    # "addCheckBox":Z
    .end local v12    # "description":Ljava/lang/String;
    :pswitch_8
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060020

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v28

    .line 267
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f06001f

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    .line 268
    .restart local v12    # "description":Ljava/lang/String;
    const/4 v3, 0x0

    .line 269
    .restart local v3    # "addCheckBox":Z
    goto/16 :goto_2

    .line 287
    .restart local v4    # "addMsg":Landroid/widget/TextView;
    .restart local v10    # "checkBox":Landroid/widget/CheckBox;
    .restart local v20    # "msg":Landroid/widget/TextView;
    :cond_10
    const/16 v30, 0x1

    move/from16 v0, p1

    move/from16 v1, v30

    if-ne v0, v1, :cond_0

    .line 288
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v30

    const v31, 0x7f060022

    invoke-virtual/range {v30 .. v31}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v30

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_3

    .line 296
    :cond_11
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 298
    :cond_12
    const/16 v30, 0x0

    move/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 299
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 326
    .restart local v23    # "onCheckClick":Landroid/view/View$OnClickListener;
    :cond_13
    const/16 v30, 0x8

    move/from16 v0, v30

    invoke-virtual {v10, v0}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 327
    invoke-virtual {v10}, Landroid/widget/CheckBox;->getParent()Landroid/view/ViewParent;

    move-result-object v30

    check-cast v30, Landroid/view/View;

    const/16 v31, 0x8

    invoke-virtual/range {v30 .. v31}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_5

    .line 165
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_7
        :pswitch_8
        :pswitch_6
        :pswitch_1
    .end packed-switch
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 427
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z

    .line 428
    return-void
.end method

.method protected onPrepareDialog(ILandroid/app/Dialog;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "dialog"    # Landroid/app/Dialog;

    .prologue
    .line 143
    invoke-super {p0, p1, p2}, Landroid/app/Activity;->onPrepareDialog(ILandroid/app/Dialog;)V

    .line 144
    invoke-virtual {p0, p1}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->removeDialog(I)V

    .line 145
    return-void
.end method

.method protected onResume()V
    .locals 6

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 432
    invoke-super {p0}, Landroid/app/Activity;->onResume()V

    .line 433
    sget v4, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    const/16 v5, 0x8

    if-ne v4, v5, :cond_0

    .line 434
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v4

    const-string v5, "airplane_mode_on"

    invoke-static {v4, v5, v3}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v4

    if-ne v4, v2, :cond_1

    move v1, v2

    .line 435
    .local v1, "flightModeEnabled":Z
    :goto_0
    if-nez v1, :cond_0

    .line 436
    sget v4, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I

    invoke-virtual {p0, v4}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->removeDialog(I)V

    .line 437
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f040001

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 438
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 439
    .local v0, "cintent":Landroid/content/Intent;
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->overridePendingTransition(II)V

    .line 440
    const/high16 v4, 0x10000

    invoke-virtual {v0, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 441
    const-string v4, "network_err_type"

    const/4 v5, 0x6

    invoke-virtual {v0, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 443
    const-string v4, "mobile_data_only"

    invoke-virtual {v0, v4, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 444
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->finish()V

    .line 445
    invoke-virtual {p0, v3, v3}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->overridePendingTransition(II)V

    .line 446
    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->startActivity(Landroid/content/Intent;)V

    .line 450
    .end local v0    # "cintent":Landroid/content/Intent;
    .end local v1    # "flightModeEnabled":Z
    :cond_0
    return-void

    :cond_1
    move v1, v3

    .line 434
    goto :goto_0
.end method
