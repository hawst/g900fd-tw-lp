.class Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;
.super Ljava/lang/Object;
.source "SviewCoverFake.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->showPopupSviewCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)V
    .locals 0

    .prologue
    .line 184
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v1, 0x0

    .line 187
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    invoke-virtual {v2}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    const-string v3, "SVIEW_COVER_POPUP"

    invoke-virtual {v2, v3, v1}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 189
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    if-eqz p2, :cond_0

    .line 190
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->access$100(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)Landroid/os/Handler;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 191
    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->access$100(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)Landroid/os/Handler;

    move-result-object v2

    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;
    invoke-static {v3}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->access$200(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)Ljava/lang/Runnable;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 193
    :cond_0
    const-string v2, "show_fake_sviewcover_popup"

    if-nez p2, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-interface {v0, v2, v1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 194
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 195
    return-void
.end method
