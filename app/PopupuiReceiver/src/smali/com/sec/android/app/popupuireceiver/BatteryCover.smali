.class public Lcom/sec/android/app/popupuireceiver/BatteryCover;
.super Landroid/app/Activity;
.source "BatteryCover.java"


# instance fields
.field private FirstValue:Ljava/lang/String;

.field private isNAOperator:Z

.field private mBatteryCoverTask:Ljava/lang/Runnable;

.field private mDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;

.field private mShowBatteryCoverPopup:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 40
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    .line 42
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mShowBatteryCoverPopup:Z

    .line 43
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    .line 46
    const-string v0, "firstvalue"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->FirstValue:Ljava/lang/String;

    .line 78
    new-instance v0, Lcom/sec/android/app/popupuireceiver/BatteryCover$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover$1;-><init>(Lcom/sec/android/app/popupuireceiver/BatteryCover;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/popupuireceiver/BatteryCover;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/BatteryCover;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 34
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupuireceiver/BatteryCover;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/BatteryCover;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/BatteryCover;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/BatteryCover;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method private showPopupBatteryCover()V
    .locals 24

    .prologue
    .line 85
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    if-eqz v19, :cond_0

    .line 216
    :goto_0
    return-void

    .line 87
    :cond_0
    const-string v19, "VZW"

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "KVZW"

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "SPR"

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-nez v19, :cond_1

    const-string v19, "KSPORTSPR"

    const-string v20, ""

    invoke-virtual/range {v19 .. v20}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v19

    if-eqz v19, :cond_2

    .line 91
    :cond_1
    const/16 v19, 0x1

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    .line 92
    const-string v19, "PopupuiReceiver"

    const-string v20, "SPR, VZW Enable+"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    :cond_2
    const/4 v12, 0x0

    .line 95
    .local v12, "isTalkbackOn":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v10

    .line 96
    .local v10, "cr":Landroid/content/ContentResolver;
    const-string v19, "enabled_accessibility_services"

    move-object/from16 v0, v19

    invoke-static {v10, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v17

    .line 97
    .local v17, "talkbackString":Ljava/lang/String;
    const-string v19, "accessibility_enabled"

    const/16 v20, 0x0

    move-object/from16 v0, v19

    move/from16 v1, v20

    invoke-static {v10, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v19

    const/16 v20, 0x1

    move/from16 v0, v19

    move/from16 v1, v20

    if-ne v0, v1, :cond_3

    if-eqz v17, :cond_3

    const-string v19, "(?i).*talkback.*"

    move-object/from16 v0, v17

    move-object/from16 v1, v19

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v19

    if-eqz v19, :cond_3

    .line 99
    const/4 v12, 0x1

    .line 102
    :cond_3
    const-string v19, "PopupuiReceiver"

    const-string v20, "showPopupBatteryCover()+"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 104
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    move/from16 v19, v0

    if-eqz v19, :cond_6

    .line 105
    const-string v19, "PopupuiReceiver"

    const-string v20, "else layout in+ isNAOperator true"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 106
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    const v20, 0x7f030002

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 119
    .local v14, "popupView":Landroid/view/View;
    :goto_1
    new-instance v19, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v19 .. v19}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v19

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    .line 120
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    move-object/from16 v0, v19

    invoke-virtual {v0, v14}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 121
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 122
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v19

    new-instance v20, Landroid/graphics/drawable/ColorDrawable;

    const/16 v21, 0x0

    invoke-direct/range {v20 .. v21}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual/range {v19 .. v20}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 123
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    new-instance v20, Lcom/sec/android/app/popupuireceiver/BatteryCover$3;

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover$3;-><init>(Lcom/sec/android/app/popupuireceiver/BatteryCover;)V

    invoke-virtual/range {v19 .. v20}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 132
    const v19, 0x7f090001

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v11

    check-cast v11, Landroid/widget/ImageView;

    .line 133
    .local v11, "imgView":Landroid/widget/ImageView;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    move/from16 v19, v0

    if-nez v19, :cond_7

    .line 134
    if-nez v12, :cond_4

    .line 135
    new-instance v19, Landroid/os/Handler;

    invoke-direct/range {v19 .. v19}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    .line 136
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    const-wide/16 v22, 0xfa0

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 138
    :cond_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    const/16 v20, 0x1

    invoke-virtual/range {v19 .. v20}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 139
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/AlertDialog;->show()V

    .line 140
    if-eqz v11, :cond_5

    .line 141
    const v19, 0x7f02001f

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 142
    invoke-virtual {v11}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    .line 143
    .local v4, "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 145
    .end local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_5
    const v19, 0x7f090002

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 146
    .local v5, "batteryCoverText":Landroid/widget/TextView;
    const v19, 0x7f060035

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 147
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    invoke-virtual/range {v19 .. v19}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    .line 148
    .local v15, "res":Landroid/content/res/Resources;
    const v19, 0x7f050003

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v18

    .line 149
    .local v18, "topPadding":I
    const v19, 0x7f050004

    move/from16 v0, v19

    invoke-virtual {v15, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v7

    .line 150
    .local v7, "bottomPadding":I
    const/16 v19, 0x0

    const/16 v20, 0x0

    move/from16 v0, v19

    move/from16 v1, v18

    move/from16 v2, v20

    invoke-virtual {v5, v0, v1, v2, v7}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0

    .line 108
    .end local v5    # "batteryCoverText":Landroid/widget/TextView;
    .end local v7    # "bottomPadding":I
    .end local v11    # "imgView":Landroid/widget/ImageView;
    .end local v14    # "popupView":Landroid/view/View;
    .end local v15    # "res":Landroid/content/res/Resources;
    .end local v18    # "topPadding":I
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getBaseContext()Landroid/content/Context;

    move-result-object v19

    const v20, 0x7f030001

    const/16 v21, 0x0

    invoke-static/range {v19 .. v21}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v14

    .line 109
    .restart local v14    # "popupView":Landroid/view/View;
    new-instance v19, Lcom/sec/android/app/popupuireceiver/BatteryCover$2;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover$2;-><init>(Lcom/sec/android/app/popupuireceiver/BatteryCover;)V

    move-object/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 153
    .restart local v11    # "imgView":Landroid/widget/ImageView;
    :cond_7
    const-string v19, "BatteryCover"

    const-string v20, "NA WaterProofPopup Concept"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 154
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    const-string v20, "BATTERY_COVER_POPUP"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v19

    const-string v20, "show_batterycover_popup"

    const/16 v21, 0x1

    invoke-interface/range {v19 .. v21}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v19

    move/from16 v0, v19

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mShowBatteryCoverPopup:Z

    .line 155
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mShowBatteryCoverPopup:Z

    move/from16 v19, v0

    if-eqz v19, :cond_d

    .line 157
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    invoke-virtual/range {v19 .. v19}, Landroid/app/AlertDialog;->show()V

    .line 158
    if-eqz v11, :cond_8

    .line 159
    const v19, 0x7f020020

    move/from16 v0, v19

    invoke-virtual {v11, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 160
    invoke-virtual {v11}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    .line 161
    .restart local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 163
    .end local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_8
    const-string v19, "BatteryCover"

    const-string v20, "NA Show WaterProofPopup"

    invoke-static/range {v19 .. v20}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 164
    const v19, 0x7f090002

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 165
    .restart local v5    # "batteryCoverText":Landroid/widget/TextView;
    const v19, 0x7f060036

    move/from16 v0, v19

    invoke-virtual {v5, v0}, Landroid/widget/TextView;->setText(I)V

    .line 166
    const v19, 0x7f090003

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/LinearLayout;

    .line 167
    .local v9, "checkboxlayout":Landroid/widget/LinearLayout;
    const v19, 0x7f090004

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v8

    check-cast v8, Landroid/widget/CheckBox;

    .line 168
    .local v8, "checkBox":Landroid/widget/CheckBox;
    const v19, 0x7f090005

    move/from16 v0, v19

    invoke-virtual {v14, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/Button;

    .line 169
    .local v13, "okBtn":Landroid/widget/Button;
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 170
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 171
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v19

    const-string v20, "BATTERY_COVER_POPUP"

    const/16 v21, 0x0

    invoke-virtual/range {v19 .. v21}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v16

    .line 172
    .local v16, "sp":Landroid/content/SharedPreferences;
    const-string v19, "show_batterycover_count"

    const/16 v20, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v19

    move/from16 v2, v20

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 173
    .local v6, "bootCount":I
    const/16 v19, 0x1

    move/from16 v0, v19

    if-ne v6, v0, :cond_9

    .line 174
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 175
    const v19, 0x7f060007

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    .line 192
    :goto_2
    new-instance v19, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/BatteryCover$4;-><init>(Lcom/sec/android/app/popupuireceiver/BatteryCover;)V

    move-object/from16 v0, v19

    invoke-virtual {v8, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 176
    :cond_9
    const/16 v19, 0x4

    move/from16 v0, v19

    if-ge v6, v0, :cond_b

    .line 177
    const/16 v19, 0x8

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 178
    if-nez v12, :cond_a

    .line 179
    new-instance v19, Landroid/os/Handler;

    invoke-direct/range {v19 .. v19}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    .line 180
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x1770

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 182
    :cond_a
    const v19, 0x7f060008

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 184
    :cond_b
    const/16 v19, 0x0

    move/from16 v0, v19

    invoke-virtual {v9, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 185
    if-nez v12, :cond_c

    .line 186
    new-instance v19, Landroid/os/Handler;

    invoke-direct/range {v19 .. v19}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    .line 187
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    move-object/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    move-object/from16 v20, v0

    const-wide/16 v22, 0x1770

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    move-wide/from16 v2, v22

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 189
    :cond_c
    const v19, 0x7f060008

    move/from16 v0, v19

    invoke-virtual {v13, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_2

    .line 213
    .end local v5    # "batteryCoverText":Landroid/widget/TextView;
    .end local v6    # "bootCount":I
    .end local v8    # "checkBox":Landroid/widget/CheckBox;
    .end local v9    # "checkboxlayout":Landroid/widget/LinearLayout;
    .end local v13    # "okBtn":Landroid/widget/Button;
    .end local v16    # "sp":Landroid/content/SharedPreferences;
    :cond_d
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->finish()V

    goto/16 :goto_0
.end method


# virtual methods
.method public closeBatterycoverPopup(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->finish()V

    .line 220
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 50
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 51
    const v3, 0x7f030006

    invoke-virtual {p0, v3}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->setContentView(I)V

    .line 57
    :try_start_0
    const-string v3, "PopupuiReceiver"

    const-string v4, "on BatteryCover Delete to FirstValue"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 58
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->FirstValue:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v0

    .line 59
    .local v0, "FirstBoot":Landroid/content/SharedPreferences;
    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 60
    .local v1, "FirstBootEdit":Landroid/content/SharedPreferences$Editor;
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->FirstValue:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-interface {v1, v3, v4}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 61
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 63
    const-string v3, "PopupuiReceiver"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "on BatteryCover  : "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->FirstValue:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 67
    .end local v0    # "FirstBoot":Landroid/content/SharedPreferences;
    .end local v1    # "FirstBootEdit":Landroid/content/SharedPreferences$Editor;
    :goto_0
    return-void

    .line 64
    :catch_0
    move-exception v2

    .line 65
    .local v2, "s2":Ljava/lang/Exception;
    const-string v3, "BatteryCover"

    const-string v4, "on BatteryCover preferenced failed!"

    invoke-static {v3, v4}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0
.end method

.method protected onDestroy()V
    .locals 8

    .prologue
    const/4 v7, 0x0

    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 225
    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    if-eqz v3, :cond_0

    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mShowBatteryCoverPopup:Z

    if-eqz v3, :cond_0

    .line 226
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "BATTERY_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 227
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v3, "show_batterycover_count"

    invoke-interface {v2, v3, v6}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 228
    .local v0, "bootCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "BATTERY_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 229
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "show_batterycover_count"

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 230
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 233
    .end local v0    # "bootCount":I
    .end local v1    # "editor":Landroid/content/SharedPreferences$Editor;
    .end local v2    # "sp":Landroid/content/SharedPreferences;
    :cond_0
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    if-eqz v3, :cond_1

    .line 234
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mHandler:Landroid/os/Handler;

    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    invoke-virtual {v3, v4}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 235
    :cond_1
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    if-eqz v3, :cond_2

    .line 236
    iget-object v3, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v3}, Landroid/app/AlertDialog;->dismiss()V

    .line 237
    iput-object v7, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mDialog:Landroid/app/AlertDialog;

    .line 239
    :cond_2
    iput-boolean v6, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mShowBatteryCoverPopup:Z

    .line 240
    iput-boolean v5, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->isNAOperator:Z

    .line 241
    iput-object v7, p0, Lcom/sec/android/app/popupuireceiver/BatteryCover;->mBatteryCoverTask:Ljava/lang/Runnable;

    .line 242
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 244
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 72
    if-eqz p1, :cond_0

    .line 73
    invoke-direct {p0}, Lcom/sec/android/app/popupuireceiver/BatteryCover;->showPopupBatteryCover()V

    .line 75
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 76
    return-void
.end method
