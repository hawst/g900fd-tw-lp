.class public Lcom/sec/android/app/popupuireceiver/PopupuiService;
.super Landroid/app/Service;
.source "PopupuiService.java"


# instance fields
.field private isNAOperator:Z

.field private mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

.field private mContext:Landroid/content/Context;

.field private mHandler:Landroid/os/Handler;

.field private mUSBCDetacheddDialog:Landroid/app/Dialog;

.field private mWaterDamageTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 66
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->isNAOperator:Z

    .line 71
    new-instance v0, Lcom/sec/android/app/popupuireceiver/PopupuiService$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService$1;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    .line 108
    new-instance v0, Lcom/sec/android/app/popupuireceiver/PopupuiService$2;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService$2;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/content/BroadcastReceiver;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/PopupuiService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    return-object v0
.end method

.method static synthetic access$002(Lcom/sec/android/app/popupuireceiver/PopupuiService;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/PopupuiService;
    .param p1, "x1"    # Landroid/content/BroadcastReceiver;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/PopupuiService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/PopupuiService;

    .prologue
    .line 59
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method static synthetic access$302(Lcom/sec/android/app/popupuireceiver/PopupuiService;Landroid/app/Dialog;)Landroid/app/Dialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/PopupuiService;
    .param p1, "x1"    # Landroid/app/Dialog;

    .prologue
    .line 59
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    return-object p1
.end method


# virtual methods
.method public calculatePopupShowCnt()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 285
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "USB_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v2

    .line 286
    .local v2, "sp":Landroid/content/SharedPreferences;
    const-string v3, "show_count"

    const/4 v4, 0x1

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    .line 287
    .local v0, "bootCount":I
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v3

    const-string v4, "USB_COVER_POPUP"

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v3

    invoke-interface {v3}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    .line 288
    .local v1, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v3, "show_count"

    add-int/lit8 v0, v0, 0x1

    invoke-interface {v1, v3, v0}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    .line 289
    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 290
    return-void
.end method

.method dismissUSBCDetacheddDialog()V
    .locals 2

    .prologue
    .line 116
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 117
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 118
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    .line 121
    const-string v0, "PopupuiReceiver"

    const-string v1, "PopupuiService.java: dismissUSBCDetacheddDialog() unReigister"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 124
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 125
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 127
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 128
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 130
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->onDestroy()V

    .line 131
    return-void
.end method

.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 88
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()V
    .locals 2

    .prologue
    .line 80
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 81
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    .line 82
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.intent.action.CLOSE_SYSTEM_DIALOGS"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    .line 83
    .local v0, "filter":Landroid/content/IntentFilter;
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v1, v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 84
    return-void
.end method

.method public onDestroy()V
    .locals 4

    .prologue
    .line 294
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 295
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 296
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;

    .line 299
    const-string v0, "PopupuiReceiver"

    const-string v1, "PopupuiService.java: onDestroy() unReigister"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 302
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_1

    .line 303
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 305
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    if-eqz v0, :cond_2

    .line 306
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    invoke-virtual {v0}, Landroid/app/Dialog;->dismiss()V

    .line 308
    :cond_2
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->isNAOperator:Z

    .line 309
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    if-eqz v0, :cond_3

    .line 310
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    new-instance v1, Landroid/content/Intent;

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    const-class v3, Lcom/sec/android/app/popupuireceiver/PopupuiService;

    invoke-direct {v1, v2, v3}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, v1}, Landroid/content/Context;->stopService(Landroid/content/Intent;)Z

    .line 311
    :cond_3
    invoke-super {p0}, Landroid/app/Service;->onDestroy()V

    .line 312
    return-void
.end method

.method public onStartCommand(Landroid/content/Intent;II)I
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "flags"    # I
    .param p3, "startId"    # I

    .prologue
    const/4 v3, 0x2

    .line 93
    if-nez p1, :cond_0

    .line 105
    :goto_0
    return v3

    .line 96
    :cond_0
    const-string v1, "isDisconnect"

    const/4 v2, 0x0

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v0

    .line 97
    .local v0, "showDialog":Z
    if-eqz v0, :cond_2

    .line 98
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    if-nez v1, :cond_1

    .line 99
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    iput-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    .line 101
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mContext:Landroid/content/Context;

    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->showUSBCDetacheddDialog(Landroid/content/Context;)V

    goto :goto_0

    .line 103
    :cond_2
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->dismissUSBCDetacheddDialog()V

    goto :goto_0
.end method

.method showUSBCDetacheddDialog(Landroid/content/Context;)V
    .locals 22
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 133
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    move-object/from16 v18, v0

    if-nez v18, :cond_6

    if-eqz p1, :cond_6

    .line 134
    const/16 v17, 0x0

    .line 135
    .local v17, "v":Landroid/view/View;
    const-string v18, "PopupuiReceiver"

    const-string v19, "showUSBDetach+"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 137
    const v18, 0x7f03000a

    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v18

    move-object/from16 v2, v19

    invoke-static {v0, v1, v2}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v17

    .line 138
    const-string v18, "PopupuiReceiver"

    const-string v19, "OPEN layout in+"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 140
    const-string v18, "VZW"

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "KVZW"

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "SPR"

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-nez v18, :cond_0

    const-string v18, "KSPORTSPR"

    const-string v19, ""

    invoke-virtual/range {v18 .. v19}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v18

    if-eqz v18, :cond_1

    .line 144
    :cond_0
    const/16 v18, 0x1

    move/from16 v0, v18

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiService;->isNAOperator:Z

    .line 145
    const-string v18, "PopupuiReceiver"

    const-string v19, "SPR, VZW Enable+"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 148
    :cond_1
    const/4 v11, 0x0

    .line 149
    .local v11, "isTalkbackOn":Z
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v8

    .line 150
    .local v8, "cr":Landroid/content/ContentResolver;
    const-string v18, "enabled_accessibility_services"

    move-object/from16 v0, v18

    invoke-static {v8, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v16

    .line 151
    .local v16, "talkbackString":Ljava/lang/String;
    const-string v18, "accessibility_enabled"

    const/16 v19, 0x0

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-static {v8, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v18

    const/16 v19, 0x1

    move/from16 v0, v18

    move/from16 v1, v19

    if-ne v0, v1, :cond_2

    if-eqz v16, :cond_2

    const-string v18, "(?i).*talkback.*"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v18

    if-eqz v18, :cond_2

    .line 153
    const/4 v11, 0x1

    .line 155
    :cond_2
    new-instance v9, Lcom/sec/android/app/popupuireceiver/PopupuiService$3;

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-direct {v9, v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService$3;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;Landroid/content/Context;)V

    .line 162
    .local v9, "d":Landroid/app/Dialog;
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->isNAOperator:Z

    move/from16 v18, v0

    if-nez v18, :cond_3

    .line 163
    new-instance v18, Lcom/sec/android/app/popupuireceiver/PopupuiService$4;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService$4;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 173
    :cond_3
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/app/Dialog;->setCancelable(Z)V

    .line 174
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/app/Dialog;->requestWindowFeature(I)Z

    .line 175
    new-instance v18, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    move-object/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/app/Dialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 192
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    const/16 v19, 0x2

    invoke-virtual/range {v18 .. v19}, Landroid/view/Window;->clearFlags(I)V

    .line 193
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    new-instance v19, Landroid/graphics/drawable/ColorDrawable;

    const/16 v20, 0x0

    invoke-direct/range {v19 .. v20}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual/range {v18 .. v19}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 194
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    const/16 v19, 0x50

    invoke-virtual/range {v18 .. v19}, Landroid/view/Window;->setGravity(I)V

    .line 195
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    const/16 v19, 0x7d9

    invoke-virtual/range {v18 .. v19}, Landroid/view/Window;->setType(I)V

    .line 196
    invoke-virtual {v9}, Landroid/app/Dialog;->getWindow()Landroid/view/Window;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    move/from16 v19, v0

    or-int/lit8 v19, v19, 0x10

    move/from16 v0, v19

    move-object/from16 v1, v18

    iput v0, v1, Landroid/view/WindowManager$LayoutParams;->privateFlags:I

    .line 198
    move-object/from16 v0, v17

    invoke-virtual {v9, v0}, Landroid/app/Dialog;->setContentView(Landroid/view/View;)V

    .line 199
    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;

    .line 201
    const v18, 0x7f090011

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/LinearLayout;

    .line 202
    .local v7, "checkboxlayout":Landroid/widget/LinearLayout;
    const v18, 0x7f090012

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/CheckBox;

    .line 203
    .local v6, "checkBox":Landroid/widget/CheckBox;
    const v18, 0x7f090013

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/Button;

    .line 204
    .local v12, "okBtn":Landroid/widget/Button;
    const v18, 0x7f09000f

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v13

    check-cast v13, Landroid/widget/TextView;

    .line 205
    .local v13, "popupText":Landroid/widget/TextView;
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 206
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 208
    const v18, 0x7f090010

    invoke-virtual/range {v17 .. v18}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/ImageView;

    .line 209
    .local v10, "imgView":Landroid/widget/ImageView;
    if-eqz v10, :cond_4

    .line 210
    const v18, 0x7f020021

    move/from16 v0, v18

    invoke-virtual {v10, v0}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 211
    invoke-virtual {v10}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v4

    check-cast v4, Landroid/graphics/drawable/AnimationDrawable;

    .line 212
    .local v4, "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    invoke-virtual {v4}, Landroid/graphics/drawable/AnimationDrawable;->start()V

    .line 214
    .end local v4    # "batteryAnimation":Landroid/graphics/drawable/AnimationDrawable;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->isNAOperator:Z

    move/from16 v18, v0

    if-nez v18, :cond_7

    .line 215
    const v18, 0x7f060038

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(I)V

    .line 216
    if-nez v11, :cond_5

    .line 217
    new-instance v18, Landroid/os/Handler;

    invoke-direct/range {v18 .. v18}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    .line 218
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    const-wide/16 v20, 0xfa0

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 220
    :cond_5
    const/16 v18, 0x1

    move/from16 v0, v18

    invoke-virtual {v9, v0}, Landroid/app/Dialog;->setCanceledOnTouchOutside(Z)V

    .line 221
    invoke-virtual {v9}, Landroid/app/Dialog;->show()V

    .line 282
    .end local v6    # "checkBox":Landroid/widget/CheckBox;
    .end local v7    # "checkboxlayout":Landroid/widget/LinearLayout;
    .end local v8    # "cr":Landroid/content/ContentResolver;
    .end local v9    # "d":Landroid/app/Dialog;
    .end local v10    # "imgView":Landroid/widget/ImageView;
    .end local v11    # "isTalkbackOn":Z
    .end local v12    # "okBtn":Landroid/widget/Button;
    .end local v13    # "popupText":Landroid/widget/TextView;
    .end local v16    # "talkbackString":Ljava/lang/String;
    .end local v17    # "v":Landroid/view/View;
    :cond_6
    :goto_0
    return-void

    .line 224
    .restart local v6    # "checkBox":Landroid/widget/CheckBox;
    .restart local v7    # "checkboxlayout":Landroid/widget/LinearLayout;
    .restart local v8    # "cr":Landroid/content/ContentResolver;
    .restart local v9    # "d":Landroid/app/Dialog;
    .restart local v10    # "imgView":Landroid/widget/ImageView;
    .restart local v11    # "isTalkbackOn":Z
    .restart local v12    # "okBtn":Landroid/widget/Button;
    .restart local v13    # "popupText":Landroid/widget/TextView;
    .restart local v16    # "talkbackString":Ljava/lang/String;
    .restart local v17    # "v":Landroid/view/View;
    :cond_7
    const-string v18, "USBCoverPopup"

    const-string v19, "NA WaterProofPopup Concept"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 225
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const-string v19, "USB_COVER_POPUP"

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    const-string v19, "show_usbcover_popup"

    const/16 v20, 0x1

    invoke-interface/range {v18 .. v20}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v18

    invoke-static/range {v18 .. v18}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v14

    .line 226
    .local v14, "showUSBCoverPopup":Ljava/lang/Boolean;
    invoke-virtual {v14}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v18

    if-eqz v18, :cond_c

    .line 228
    const-string v18, "USBCoverPopup"

    const-string v19, "NA showUSBCoverPopup"

    invoke-static/range {v18 .. v19}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 229
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 230
    new-instance v18, Lcom/sec/android/app/popupuireceiver/PopupuiService$6;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService$6;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    move-object/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 235
    const v18, 0x7f060039

    move/from16 v0, v18

    invoke-virtual {v13, v0}, Landroid/widget/TextView;->setText(I)V

    .line 236
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->getApplicationContext()Landroid/content/Context;

    move-result-object v18

    const-string v19, "USB_COVER_POPUP"

    const/16 v20, 0x0

    invoke-virtual/range {v18 .. v20}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v15

    .line 237
    .local v15, "sp":Landroid/content/SharedPreferences;
    const-string v18, "show_count"

    const/16 v19, 0x1

    move-object/from16 v0, v18

    move/from16 v1, v19

    invoke-interface {v15, v0, v1}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v5

    .line 238
    .local v5, "bootCount":I
    const/16 v18, 0x1

    move/from16 v0, v18

    if-ne v5, v0, :cond_8

    .line 239
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 240
    const v18, 0x7f060007

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setText(I)V

    .line 256
    :goto_1
    new-instance v18, Lcom/sec/android/app/popupuireceiver/PopupuiService$7;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService$7;-><init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V

    move-object/from16 v0, v18

    invoke-virtual {v6, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    .line 274
    invoke-virtual {v9}, Landroid/app/Dialog;->show()V

    .line 275
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->calculatePopupShowCnt()V

    goto/16 :goto_0

    .line 241
    :cond_8
    const/16 v18, 0x4

    move/from16 v0, v18

    if-ge v5, v0, :cond_a

    .line 242
    const/16 v18, 0x8

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 243
    if-nez v11, :cond_9

    .line 244
    new-instance v18, Landroid/os/Handler;

    invoke-direct/range {v18 .. v18}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    .line 245
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    const-wide/16 v20, 0x1770

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 247
    :cond_9
    const v18, 0x7f060008

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    .line 249
    :cond_a
    const/16 v18, 0x0

    move/from16 v0, v18

    invoke-virtual {v7, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 250
    if-nez v11, :cond_b

    .line 251
    new-instance v18, Landroid/os/Handler;

    invoke-direct/range {v18 .. v18}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    .line 252
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;

    move-object/from16 v18, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;

    move-object/from16 v19, v0

    const-wide/16 v20, 0x1770

    invoke-virtual/range {v18 .. v21}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 254
    :cond_b
    const v18, 0x7f060008

    move/from16 v0, v18

    invoke-virtual {v12, v0}, Landroid/widget/Button;->setText(I)V

    goto :goto_1

    .line 278
    .end local v5    # "bootCount":I
    .end local v15    # "sp":Landroid/content/SharedPreferences;
    :cond_c
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->dismissUSBCDetacheddDialog()V

    goto/16 :goto_0
.end method
