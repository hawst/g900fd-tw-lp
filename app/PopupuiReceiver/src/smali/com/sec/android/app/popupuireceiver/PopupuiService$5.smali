.class Lcom/sec/android/app/popupuireceiver/PopupuiService$5;
.super Ljava/lang/Object;
.source "PopupuiService.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/PopupuiService;->showUSBCDetacheddDialog(Landroid/content/Context;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/PopupuiService;)V
    .locals 0

    .prologue
    .line 175
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    const/4 v2, 0x0

    .line 178
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # getter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$000(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/content/BroadcastReceiver;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 179
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # getter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$000(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/content/BroadcastReceiver;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 180
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # setter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mCloseSystemDialogsReceiver:Landroid/content/BroadcastReceiver;
    invoke-static {v0, v2}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$002(Lcom/sec/android/app/popupuireceiver/PopupuiService;Landroid/content/BroadcastReceiver;)Landroid/content/BroadcastReceiver;

    .line 183
    const-string v0, "PopupuiReceiver"

    const-string v1, "PopupuiService.java: onDimiss() unReigister"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 186
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # getter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$100(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/os/Handler;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 187
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # getter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mHandler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$100(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # getter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mWaterDamageTask:Ljava/lang/Runnable;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$200(Lcom/sec/android/app/popupuireceiver/PopupuiService;)Ljava/lang/Runnable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 188
    :cond_1
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    # setter for: Lcom/sec/android/app/popupuireceiver/PopupuiService;->mUSBCDetacheddDialog:Landroid/app/Dialog;
    invoke-static {v0, v2}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->access$302(Lcom/sec/android/app/popupuireceiver/PopupuiService;Landroid/app/Dialog;)Landroid/app/Dialog;

    .line 189
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiService$5;->this$0:Lcom/sec/android/app/popupuireceiver/PopupuiService;

    invoke-virtual {v0}, Lcom/sec/android/app/popupuireceiver/PopupuiService;->onDestroy()V

    .line 190
    return-void
.end method
