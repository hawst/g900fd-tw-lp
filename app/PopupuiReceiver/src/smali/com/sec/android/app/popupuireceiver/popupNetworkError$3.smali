.class Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;
.super Ljava/lang/Object;
.source "popupNetworkError.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/popupNetworkError;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

.field final synthetic val$addCheckBox:Z

.field final synthetic val$id:I


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/popupNetworkError;IZ)V
    .locals 0

    .prologue
    .line 341
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    iput p2, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->val$id:I

    iput-boolean p3, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->val$addCheckBox:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 11
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v8, 0x0

    const/4 v7, 0x1

    .line 343
    iget v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->val$id:I

    const/4 v9, 0x4

    if-ne v6, v9, :cond_0

    .line 344
    # setter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z
    invoke-static {v7}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$002(Z)Z

    .line 347
    :cond_0
    iget-boolean v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->val$addCheckBox:Z

    if-eqz v6, :cond_1

    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$000()Z

    move-result v6

    if-eqz v6, :cond_1

    .line 348
    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$100()I

    move-result v6

    if-nez v6, :cond_4

    .line 349
    const-string v6, "PopupuiReceiver"

    const-string v9, "Data Connection Fail -> Disable Flight mode"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 350
    const/4 v2, 0x0

    .line 351
    .local v2, "enabled":Z
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v9

    const-string v10, "airplane_mode_on"

    if-eqz v2, :cond_3

    move v6, v7

    :goto_0
    invoke-static {v9, v10, v6}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 353
    new-instance v3, Landroid/content/Intent;

    const-string v6, "android.intent.action.AIRPLANE_MODE"

    invoke-direct {v3, v6}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 354
    .local v3, "intent":Landroid/content/Intent;
    const/high16 v6, 0x20000000

    invoke-virtual {v3, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 355
    const-string v6, "state"

    invoke-virtual {v3, v6, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 356
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6, v3}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->sendBroadcast(Landroid/content/Intent;)V

    .line 369
    .end local v2    # "enabled":Z
    .end local v3    # "intent":Landroid/content/Intent;
    :cond_1
    :goto_1
    const-string v6, "PopupuiReceiver"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Data Connection Fail : isChecked "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$000()Z

    move-result v10

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getIntent()Landroid/content/Intent;

    move-result-object v4

    .line 371
    .local v4, "recvi":Landroid/content/Intent;
    const-string v6, "retVal"

    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$000()Z

    move-result v9

    invoke-virtual {v4, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 372
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    const/4 v9, -0x1

    invoke-virtual {v6, v9, v4}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->setResult(ILandroid/content/Intent;)V

    .line 373
    # setter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->isChecked:Z
    invoke-static {v8}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$002(Z)Z

    .line 375
    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$100()I

    move-result v6

    const/16 v9, 0x8

    if-ne v6, v9, :cond_2

    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v9, 0x7f040001

    invoke-virtual {v6, v9}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v6

    if-ne v6, v7, :cond_2

    .line 376
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 377
    .local v0, "cintent":Landroid/content/Intent;
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6, v8, v8}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->overridePendingTransition(II)V

    .line 378
    const/high16 v6, 0x10000

    invoke-virtual {v0, v6}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 379
    const-string v6, "network_err_type"

    const/4 v9, 0x6

    invoke-virtual {v0, v6, v9}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 381
    const-string v6, "mobile_data_only"

    invoke-virtual {v0, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 383
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->finish()V

    .line 384
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6, v8, v8}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->overridePendingTransition(II)V

    .line 385
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6, v0}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->startActivity(Landroid/content/Intent;)V

    .line 387
    .end local v0    # "cintent":Landroid/content/Intent;
    :cond_2
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    invoke-virtual {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->finish()V

    .line 389
    return-void

    .end local v4    # "recvi":Landroid/content/Intent;
    .restart local v2    # "enabled":Z
    :cond_3
    move v6, v8

    .line 351
    goto/16 :goto_0

    .line 357
    .end local v2    # "enabled":Z
    :cond_4
    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$100()I

    move-result v6

    const/4 v9, 0x2

    if-ne v6, v9, :cond_5

    .line 358
    const-string v6, "PopupuiReceiver"

    const-string v9, "Data Connection Fail -> Enable data roaming"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 359
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    const-string v9, "phone"

    invoke-virtual {v6, v9}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/telephony/TelephonyManager;

    .line 360
    .local v5, "telephony":Landroid/telephony/TelephonyManager;
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    # invokes: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->setCdmaGsmDataRoamingEnabled()V
    invoke-static {v6}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$200(Lcom/sec/android/app/popupuireceiver/popupNetworkError;)V

    .line 361
    invoke-virtual {v5, v7}, Landroid/telephony/TelephonyManager;->setDataRoamingEnabled(Z)V

    goto/16 :goto_1

    .line 362
    .end local v5    # "telephony":Landroid/telephony/TelephonyManager;
    :cond_5
    # getter for: Lcom/sec/android/app/popupuireceiver/popupNetworkError;->popupType:I
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->access$100()I

    move-result v6

    if-ne v6, v7, :cond_1

    .line 363
    const-string v6, "PopupuiReceiver"

    const-string v9, "Data Connection Fail -> Enable mobile data"

    invoke-static {v6, v9}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    iget-object v6, p0, Lcom/sec/android/app/popupuireceiver/popupNetworkError$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNetworkError;

    const-string v9, "connectivity"

    invoke-virtual {v6, v9}, Lcom/sec/android/app/popupuireceiver/popupNetworkError;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/net/ConnectivityManager;

    .line 365
    .local v1, "cm":Landroid/net/ConnectivityManager;
    invoke-virtual {v1, v7}, Landroid/net/ConnectivityManager;->setMobileDataEnabled(Z)V

    goto/16 :goto_1
.end method
