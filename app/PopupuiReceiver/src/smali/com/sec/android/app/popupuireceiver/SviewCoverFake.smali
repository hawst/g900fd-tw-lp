.class public Lcom/sec/android/app/popupuireceiver/SviewCoverFake;
.super Landroid/app/Activity;
.source "SviewCoverFake.java"


# instance fields
.field private mDialog:Landroid/app/AlertDialog;

.field private mHandler:Landroid/os/Handler;

.field private mIsAttached:Z

.field private mIsBootComplete:Z

.field private mOriginalSviewCover:Z

.field private mSviewCoverTask:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 30
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 36
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    .line 40
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    .line 41
    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsBootComplete:Z

    .line 42
    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsAttached:Z

    .line 108
    new-instance v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$1;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$1;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    return-void
.end method

.method static synthetic access$002(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;Landroid/app/AlertDialog;)Landroid/app/AlertDialog;
    .locals 0
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCoverFake;
    .param p1, "x1"    # Landroid/app/AlertDialog;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    return-object p1
.end method

.method static synthetic access$100(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$200(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)Ljava/lang/Runnable;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/SviewCoverFake;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    return-object v0
.end method

.method private showPopupSviewCover()V
    .locals 22

    .prologue
    .line 115
    const-string v17, "PopupuiReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "SviewCoverFake showPopupSviewCover()+ mDialog : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 116
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    if-eqz v17, :cond_0

    .line 218
    :goto_0
    return-void

    .line 119
    :cond_0
    const/4 v8, 0x0

    .line 120
    .local v8, "fakeSviewCover":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->isChinaModel()Z

    move-result v17

    if-eqz v17, :cond_1

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 121
    const/4 v8, 0x1

    .line 124
    :cond_1
    const/4 v9, 0x0

    .line 125
    .local v9, "isTalkbackOn":Z
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v7

    .line 126
    .local v7, "cr":Landroid/content/ContentResolver;
    const-string v17, "enabled_accessibility_services"

    move-object/from16 v0, v17

    invoke-static {v7, v0}, Landroid/provider/Settings$Secure;->getString(Landroid/content/ContentResolver;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v15

    .line 127
    .local v15, "talkbackString":Ljava/lang/String;
    const-string v17, "accessibility_enabled"

    const/16 v18, 0x0

    move-object/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v7, v0, v1}, Landroid/provider/Settings$Secure;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v17

    const/16 v18, 0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_2

    if-eqz v15, :cond_2

    const-string v17, "(?i).*talkback.*"

    move-object/from16 v0, v17

    invoke-virtual {v15, v0}, Ljava/lang/String;->matches(Ljava/lang/String;)Z

    move-result v17

    if-eqz v17, :cond_2

    .line 129
    const/4 v9, 0x1

    .line 132
    :cond_2
    const-string v17, "PopupuiReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "SviewCoverFake showPopupSviewCover()+ fakeSviewCover : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v8}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 134
    if-eqz v8, :cond_4

    .line 135
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    const v18, 0x7f030004

    const/16 v19, 0x0

    invoke-static/range {v17 .. v19}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 150
    .local v11, "popupView":Landroid/view/View;
    :goto_1
    new-instance v17, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    .line 151
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v11}, Landroid/app/AlertDialog;->setView(Landroid/view/View;)V

    .line 152
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 153
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->getWindow()Landroid/view/Window;

    move-result-object v17

    new-instance v18, Landroid/graphics/drawable/ColorDrawable;

    const/16 v19, 0x0

    invoke-direct/range {v18 .. v19}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual/range {v17 .. v18}, Landroid/view/Window;->setBackgroundDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 154
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    new-instance v18, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$3;

    move-object/from16 v0, v18

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$3;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)V

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setOnDismissListener(Landroid/content/DialogInterface$OnDismissListener;)V

    .line 163
    if-eqz v8, :cond_7

    .line 164
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getApplicationContext()Landroid/content/Context;

    move-result-object v17

    const-string v18, "SVIEW_COVER_POPUP"

    const/16 v19, 0x0

    invoke-virtual/range {v17 .. v19}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v17

    const-string v18, "show_fake_sviewcover_popup"

    const/16 v19, 0x1

    invoke-interface/range {v17 .. v19}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v13

    .line 165
    .local v13, "showFakeSviewCoverPopup":Z
    const-string v17, "PopupuiReceiver"

    new-instance v18, Ljava/lang/StringBuilder;

    invoke-direct/range {v18 .. v18}, Ljava/lang/StringBuilder;-><init>()V

    const-string v19, "China Fake View cover popup showFakeSviewCoverPopup : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, v18

    invoke-virtual {v0, v13}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    const-string v19, " mIsBootComplete : "

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsBootComplete:Z

    move/from16 v19, v0

    invoke-virtual/range {v18 .. v19}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v18

    invoke-static/range {v17 .. v18}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 166
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsBootComplete:Z

    move/from16 v17, v0

    if-eqz v17, :cond_3

    if-eqz v13, :cond_6

    .line 168
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setCancelable(Z)V

    .line 170
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->show()V

    .line 171
    const v17, 0x7f090002

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 172
    .local v14, "sviewCoverText":Landroid/widget/TextView;
    const v17, 0x7f060048

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(I)V

    .line 174
    const v17, 0x7f090003

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/LinearLayout;

    .line 175
    .local v6, "checkboxlayout":Landroid/widget/LinearLayout;
    const v17, 0x7f090004

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 176
    .local v5, "checkBox":Landroid/widget/CheckBox;
    const v17, 0x7f090005

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v10

    check-cast v10, Landroid/widget/Button;

    .line 178
    .local v10, "okBtn":Landroid/widget/Button;
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v6, v0}, Landroid/widget/LinearLayout;->setVisibility(I)V

    .line 179
    if-nez v13, :cond_5

    const/16 v17, 0x1

    :goto_2
    move/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setChecked(Z)V

    .line 181
    const/16 v17, 0x0

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 182
    const v17, 0x7f060007

    move/from16 v0, v17

    invoke-virtual {v10, v0}, Landroid/widget/Button;->setText(I)V

    .line 184
    new-instance v17, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$4;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)V

    move-object/from16 v0, v17

    invoke-virtual {v5, v0}, Landroid/widget/CheckBox;->setOnCheckedChangeListener(Landroid/widget/CompoundButton$OnCheckedChangeListener;)V

    goto/16 :goto_0

    .line 138
    .end local v5    # "checkBox":Landroid/widget/CheckBox;
    .end local v6    # "checkboxlayout":Landroid/widget/LinearLayout;
    .end local v10    # "okBtn":Landroid/widget/Button;
    .end local v11    # "popupView":Landroid/view/View;
    .end local v13    # "showFakeSviewCoverPopup":Z
    .end local v14    # "sviewCoverText":Landroid/widget/TextView;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    const v18, 0x7f030001

    const/16 v19, 0x0

    invoke-static/range {v17 .. v19}, Landroid/view/View;->inflate(Landroid/content/Context;ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v11

    .line 139
    .restart local v11    # "popupView":Landroid/view/View;
    new-instance v17, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$2;

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    invoke-direct {v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake$2;-><init>(Lcom/sec/android/app/popupuireceiver/SviewCoverFake;)V

    move-object/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    goto/16 :goto_1

    .line 179
    .restart local v5    # "checkBox":Landroid/widget/CheckBox;
    .restart local v6    # "checkboxlayout":Landroid/widget/LinearLayout;
    .restart local v10    # "okBtn":Landroid/widget/Button;
    .restart local v13    # "showFakeSviewCoverPopup":Z
    .restart local v14    # "sviewCoverText":Landroid/widget/TextView;
    :cond_5
    const/16 v17, 0x0

    goto :goto_2

    .line 200
    .end local v5    # "checkBox":Landroid/widget/CheckBox;
    .end local v6    # "checkboxlayout":Landroid/widget/LinearLayout;
    .end local v10    # "okBtn":Landroid/widget/Button;
    .end local v14    # "sviewCoverText":Landroid/widget/TextView;
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->finish()V

    goto/16 :goto_0

    .line 204
    .end local v13    # "showFakeSviewCoverPopup":Z
    :cond_7
    if-nez v9, :cond_8

    .line 205
    new-instance v17, Landroid/os/Handler;

    invoke-direct/range {v17 .. v17}, Landroid/os/Handler;-><init>()V

    move-object/from16 v0, v17

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    .line 206
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    move-object/from16 v18, v0

    const-wide/16 v20, 0xfa0

    move-object/from16 v0, v17

    move-object/from16 v1, v18

    move-wide/from16 v2, v20

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 208
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    const/16 v18, 0x1

    invoke-virtual/range {v17 .. v18}, Landroid/app/AlertDialog;->setCanceledOnTouchOutside(Z)V

    .line 209
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/app/AlertDialog;->show()V

    .line 210
    const v17, 0x7f090002

    move/from16 v0, v17

    invoke-virtual {v11, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v14

    check-cast v14, Landroid/widget/TextView;

    .line 211
    .restart local v14    # "sviewCoverText":Landroid/widget/TextView;
    const v17, 0x7f06003a

    move/from16 v0, v17

    invoke-virtual {v14, v0}, Landroid/widget/TextView;->setText(I)V

    .line 213
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getBaseContext()Landroid/content/Context;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    .line 214
    .local v12, "res":Landroid/content/res/Resources;
    const v17, 0x7f050005

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v16

    .line 215
    .local v16, "topPadding":I
    const v17, 0x7f050006

    move/from16 v0, v17

    invoke-virtual {v12, v0}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    .line 216
    .local v4, "bottomPadding":I
    const/16 v17, 0x0

    const/16 v18, 0x0

    move/from16 v0, v17

    move/from16 v1, v16

    move/from16 v2, v18

    invoke-virtual {v14, v0, v1, v2, v4}, Landroid/widget/TextView;->setPadding(IIII)V

    goto/16 :goto_0
.end method


# virtual methods
.method public closeBatterycoverPopup(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 221
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->finish()V

    .line 222
    return-void
.end method

.method isChinaModel()Z
    .locals 2

    .prologue
    .line 241
    const-string v0, "CHINA"

    const-string v1, "ro.csc.country_code"

    invoke-static {v1}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 242
    const/4 v0, 0x1

    .line 245
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 46
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 47
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->setContentView(I)V

    .line 49
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getIntent()Landroid/content/Intent;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->setupIntentValue(Landroid/content/Intent;Z)V

    .line 50
    return-void
.end method

.method protected onDestroy()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 226
    const-string v0, "PopupuiReceiver"

    const-string v1, "SviewCoverFake----onDestroy "

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 228
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 230
    :cond_0
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    if-eqz v0, :cond_1

    .line 231
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v0}, Landroid/app/AlertDialog;->dismiss()V

    .line 232
    iput-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    .line 234
    :cond_1
    iput-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    .line 235
    invoke-super {p0}, Landroid/app/Activity;->onDestroy()V

    .line 237
    return-void
.end method

.method protected onNewIntent(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 55
    invoke-super {p0, p1}, Landroid/app/Activity;->onNewIntent(Landroid/content/Intent;)V

    .line 57
    const/4 v0, 0x1

    invoke-virtual {p0, p1, v0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->setupIntentValue(Landroid/content/Intent;Z)V

    .line 58
    return-void
.end method

.method public onWindowFocusChanged(Z)V
    .locals 0
    .param p1, "hasFocus"    # Z

    .prologue
    .line 102
    if-eqz p1, :cond_0

    .line 103
    invoke-direct {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->showPopupSviewCover()V

    .line 105
    :cond_0
    invoke-super {p0, p1}, Landroid/app/Activity;->onWindowFocusChanged(Z)V

    .line 106
    return-void
.end method

.method setupIntentValue(Landroid/content/Intent;Z)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "newIntent"    # Z

    .prologue
    const/4 v3, 0x0

    .line 62
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->isChinaModel()Z

    move-result v1

    if-eqz v1, :cond_0

    if-nez p1, :cond_1

    .line 97
    :cond_0
    :goto_0
    return-void

    .line 67
    :cond_1
    const-string v1, "verified"

    const/4 v2, 0x1

    invoke-virtual {p1, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    .line 68
    const-string v1, "isBoot"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsBootComplete:Z

    .line 69
    const-string v1, "attached"

    invoke-virtual {p1, v1, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v1

    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsAttached:Z

    .line 71
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SVIEW_COVER_POPUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 72
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "originalSviewCover"

    iget-boolean v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 73
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 75
    iget-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    if-nez v1, :cond_2

    iget-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsAttached:Z

    if-nez v1, :cond_2

    .line 76
    const-string v1, "PopupuiReceiver"

    const-string v2, "SviewCoverFake--Fake sview cover detached"

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 77
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->finish()V

    goto :goto_0

    .line 81
    :cond_2
    const-string v1, "PopupuiReceiver"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "SviewCoverFake verifyIntentValue() newIntent :  "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mOriginalSviewCover : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsBootComplete : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsBootComplete:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " mIsAttached : "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-boolean v3, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsAttached:Z

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 83
    iget-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mOriginalSviewCover:Z

    if-nez v1, :cond_3

    iget-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mIsAttached:Z

    if-nez v1, :cond_3

    .line 84
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->finish()V

    goto/16 :goto_0

    .line 86
    :cond_3
    if-eqz p2, :cond_0

    .line 87
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    if-eqz v1, :cond_4

    .line 88
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mSviewCoverTask:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 90
    :cond_4
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    if-eqz v1, :cond_5

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    invoke-virtual {v1}, Landroid/app/AlertDialog;->dismiss()V

    .line 92
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->mDialog:Landroid/app/AlertDialog;

    .line 95
    :cond_5
    invoke-direct {p0}, Lcom/sec/android/app/popupuireceiver/SviewCoverFake;->showPopupSviewCover()V

    goto/16 :goto_0
.end method
