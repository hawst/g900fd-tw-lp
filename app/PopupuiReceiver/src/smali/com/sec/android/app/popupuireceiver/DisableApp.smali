.class public Lcom/sec/android/app/popupuireceiver/DisableApp;
.super Landroid/app/Activity;
.source "DisableApp.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# static fields
.field private static package_Name:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method static synthetic access$000()Ljava/lang/String;
    .locals 1

    .prologue
    .line 24
    sget-object v0, Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 34
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/DisableApp;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 37
    .local v0, "intent":Landroid/content/Intent;
    const-string v2, "app_package_name"

    invoke-virtual {v0, v2}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    sput-object v2, Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;

    .line 39
    const-string v2, "PopupuiReceiver"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "App Name: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    sget-object v4, Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 41
    invoke-virtual {p0}, Lcom/sec/android/app/popupuireceiver/DisableApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    .line 49
    .local v1, "mPkgMgr":Landroid/content/pm/PackageManager;
    const/4 v2, 0x1

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/DisableApp;->requestWindowFeature(I)Z

    .line 50
    const v2, 0x7f030006

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/DisableApp;->setContentView(I)V

    .line 52
    const/4 v2, 0x0

    invoke-virtual {p0, v2}, Lcom/sec/android/app/popupuireceiver/DisableApp;->showDialog(I)V

    .line 54
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 17
    .param p1, "id"    # I

    .prologue
    .line 58
    invoke-static/range {p0 .. p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v8

    .line 59
    .local v8, "factory":Landroid/view/LayoutInflater;
    const v12, 0x7f030008

    const/4 v13, 0x0

    invoke-virtual {v8, v12, v13}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v10

    .line 64
    .local v10, "popupEntryView":Landroid/view/View;
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/DisableApp;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060032

    invoke-virtual {v12, v13}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v11

    .line 66
    .local v11, "title":Ljava/lang/String;
    const v3, 0x7f060031

    .line 67
    .local v3, "btnPositiveText":I
    const v2, 0x7f060007

    .line 70
    .local v2, "btnNegativeText":I
    const v12, 0x7f090007

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/CheckBox;

    .line 71
    .local v5, "checkBox":Landroid/widget/CheckBox;
    const/16 v12, 0x8

    invoke-virtual {v5, v12}, Landroid/widget/CheckBox;->setVisibility(I)V

    .line 72
    const v12, 0x7f09000a

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    .line 73
    .local v1, "addMsg":Landroid/widget/TextView;
    const/16 v12, 0x8

    invoke-virtual {v1, v12}, Landroid/widget/TextView;->setVisibility(I)V

    .line 74
    invoke-virtual/range {p0 .. p0}, Lcom/sec/android/app/popupuireceiver/DisableApp;->getResources()Landroid/content/res/Resources;

    move-result-object v12

    const v13, 0x7f060033

    const/4 v14, 0x1

    new-array v14, v14, [Ljava/lang/Object;

    const/4 v15, 0x0

    sget-object v16, Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;

    aput-object v16, v14, v15

    invoke-virtual {v12, v13, v14}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .line 75
    .local v6, "description":Ljava/lang/String;
    const v12, 0x7f090008

    invoke-virtual {v10, v12}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v9

    check-cast v9, Landroid/widget/TextView;

    .line 78
    .local v9, "msg":Landroid/widget/TextView;
    invoke-virtual {v9, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 79
    new-instance v12, Landroid/app/AlertDialog$Builder;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v12, v11}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    invoke-virtual {v12, v10}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v12

    new-instance v13, Lcom/sec/android/app/popupuireceiver/DisableApp$1;

    move-object/from16 v0, p0

    invoke-direct {v13, v0}, Lcom/sec/android/app/popupuireceiver/DisableApp$1;-><init>(Lcom/sec/android/app/popupuireceiver/DisableApp;)V

    invoke-virtual {v12, v2, v13}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    .line 87
    .local v4, "builder":Landroid/app/AlertDialog$Builder;
    new-instance v12, Lcom/sec/android/app/popupuireceiver/DisableApp$2;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/popupuireceiver/DisableApp$2;-><init>(Lcom/sec/android/app/popupuireceiver/DisableApp;)V

    invoke-virtual {v4, v3, v12}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 113
    new-instance v12, Lcom/sec/android/app/popupuireceiver/DisableApp$3;

    move-object/from16 v0, p0

    invoke-direct {v12, v0}, Lcom/sec/android/app/popupuireceiver/DisableApp$3;-><init>(Lcom/sec/android/app/popupuireceiver/DisableApp;)V

    invoke-virtual {v4, v12}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    .line 121
    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v7

    .line 122
    .local v7, "dlg":Landroid/app/AlertDialog;
    return-object v7
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 130
    return-void
.end method
