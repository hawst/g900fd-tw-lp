.class Lcom/sec/android/app/popupuireceiver/popupAudio$1;
.super Landroid/content/BroadcastReceiver;
.source "popupAudio.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/sec/android/app/popupuireceiver/popupAudio;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/popupAudio;)V
    .locals 0

    .prologue
    .line 39
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$1;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    return-void
.end method


# virtual methods
.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 42
    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v0

    const-string v1, "android.intent.action.ACTION_HDMI_AUDIO_CH_POPUP"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 43
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$1;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    const-string v1, "state"

    const/4 v2, 0x0

    invoke-virtual {p2, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v1

    # setter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I
    invoke-static {v0, v1}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$002(Lcom/sec/android/app/popupuireceiver/popupAudio;I)I

    .line 45
    const-string v0, "PopupAudio"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Audio Receiver onReceive() getAction : "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 46
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$1;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    # getter for: Lcom/sec/android/app/popupuireceiver/popupAudio;->nAudiostate:I
    invoke-static {v0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->access$000(Lcom/sec/android/app/popupuireceiver/popupAudio;)I

    move-result v0

    if-nez v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupAudio$1;->this$0:Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-virtual {v0}, Lcom/sec/android/app/popupuireceiver/popupAudio;->finish()V

    .line 50
    :cond_0
    return-void
.end method
