.class public Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;
.super Landroid/content/BroadcastReceiver;
.source "PopupuiReceiver.java"


# static fields
.field private static IPS_package_name:Ljava/lang/String;

.field private static IPS_package_name_Tablet:Ljava/lang/String;

.field private static airplanePopupShown:Z

.field private static mToast:Landroid/widget/Toast;


# instance fields
.field private final KNOX_POPUP_URI1:Ljava/lang/String;

.field private final KNOX_POPUP_URI2:Ljava/lang/String;

.field private MCCvalue:Ljava/lang/String;

.field private final TAG_KNOX:Ljava/lang/String;

.field private checkval:I

.field private getCheckCoverPopupState:Z

.field private getSealedHideNotificationMessages:I

.field private getSealedState:Z

.field private prevMCCvalue:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 52
    const/4 v0, 0x0

    sput-boolean v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->airplanePopupShown:Z

    .line 55
    const/4 v0, 0x0

    sput-object v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    .line 68
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name:Ljava/lang/String;

    .line 69
    const-string v0, ""

    sput-object v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name_Tablet:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 32
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 53
    const-string v0, "mccstrcmp"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    .line 54
    const-string v0, "0"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    .line 58
    const-string v0, "content://com.sec.knox.provider2/KnoxCustomManagerService"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->KNOX_POPUP_URI1:Ljava/lang/String;

    .line 59
    const-string v0, "content://com.sec.knox.provider2/KnoxCustomManagerService2"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->KNOX_POPUP_URI2:Ljava/lang/String;

    .line 60
    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    .line 61
    iput v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    .line 62
    iput-boolean v1, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    .line 63
    const-string v0, "PopupuiReceiver_KNOX"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->TAG_KNOX:Ljava/lang/String;

    .line 67
    const/4 v0, 0x0

    iput v0, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->checkval:I

    return-void
.end method


# virtual methods
.method getKnoxValue(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Z
    .locals 8
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "findapi"    # Ljava/lang/String;
    .param p3, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, 0x0

    .line 75
    const/4 v7, 0x1

    .line 76
    .local v7, "ret_value":Z
    invoke-virtual {p3}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v0

    move-object v1, p1

    move-object v3, p2

    move-object v4, v2

    move-object v5, v2

    invoke-virtual/range {v0 .. v5}, Landroid/content/ContentResolver;->query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v6

    .line 78
    .local v6, "cr":Landroid/database/Cursor;
    if-eqz v6, :cond_0

    .line 82
    :try_start_0
    invoke-interface {v6}, Landroid/database/Cursor;->moveToFirst()Z

    .line 83
    invoke-interface {v6, p2}, Landroid/database/Cursor;->getColumnIndex(Ljava/lang/String;)I

    move-result v0

    invoke-interface {v6, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    const-string v1, "true"

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    if-eqz v0, :cond_1

    .line 84
    const/4 v7, 0x1

    .line 89
    :goto_0
    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    .line 93
    :cond_0
    return v7

    .line 86
    :cond_1
    const/4 v7, 0x0

    goto :goto_0

    .line 89
    :catchall_0
    move-exception v0

    invoke-interface {v6}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 43
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 135
    if-eqz p1, :cond_0

    if-eqz p2, :cond_0

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    if-nez v40, :cond_2

    .line 136
    :cond_0
    const-string v40, "PopupuiReceiver"

    const-string v41, "onReceive(): context, intent.getAction() is null"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    :cond_1
    :goto_0
    return-void

    .line 141
    :cond_2
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "onReceive() getAction : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v42

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 142
    const v40, 0x7f080002

    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->setTheme(I)V

    .line 144
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v17

    .line 146
    .local v17, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    const-string v40, "content://com.sec.knox.provider2/KnoxCustomManagerService"

    invoke-static/range {v40 .. v40}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v38

    .line 147
    .local v38, "uri":Landroid/net/Uri;
    const-string v40, "getSealedState"

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, v40

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getKnoxValue(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    .line 148
    const-string v40, "PopupuiReceiver_KNOX"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "getSealedState : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 149
    const-string v40, "content://com.sec.knox.provider2/KnoxCustomManagerService2"

    invoke-static/range {v40 .. v40}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v38

    .line 150
    const-string v40, "getSealedHideNotificationMessages"

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, v40

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getKnoxValue(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v40

    if-eqz v40, :cond_3

    .line 151
    const/16 v40, 0x1

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    .line 155
    :goto_1
    const-string v40, "PopupuiReceiver_KNOX"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "getSealedHideNotificationMessages : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 156
    const-string v40, "getCheckCoverPopupState"

    move-object/from16 v0, p0

    move-object/from16 v1, v38

    move-object/from16 v2, v40

    move-object/from16 v3, p1

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getKnoxValue(Landroid/net/Uri;Ljava/lang/String;Landroid/content/Context;)Z

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    .line 158
    const-string v40, "PopupuiReceiver_KNOX"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "getCheckCoverPopupState : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    move/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.NETWORK_SET_TIMEZONE"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_6

    .line 162
    :try_start_0
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v19

    .line 164
    .local v19, "loadMCCstrForCompare":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "-111"

    move-object/from16 v0, v19

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    .line 165
    const-string v35, ""

    .line 166
    .local v35, "tempNumeric":Ljava/lang/String;
    const-string v26, ""

    .line 168
    .local v26, "numericMCCvalue":Ljava/lang/String;
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "NETWORK_SET_TIME prevMCCvalue : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 170
    const-string v40, "gsm.operator.numeric"

    invoke-static/range {v40 .. v40}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    if-eqz v40, :cond_4

    .line 171
    const-string v40, "gsm.operator.numeric"

    invoke-static/range {v40 .. v40}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v35

    .line 172
    const/16 v40, 0x0

    const/16 v41, 0x3

    move-object/from16 v0, v35

    move/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v26

    .line 174
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "gsm.operator.numeric : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 183
    :goto_2
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_5

    .line 185
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "SAME to prevMCCvalue : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    goto/16 :goto_0

    .line 201
    .end local v19    # "loadMCCstrForCompare":Landroid/content/SharedPreferences;
    .end local v26    # "numericMCCvalue":Ljava/lang/String;
    .end local v35    # "tempNumeric":Ljava/lang/String;
    :catch_0
    move-exception v40

    goto/16 :goto_0

    .line 153
    :cond_3
    const/16 v40, 0x0

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    goto/16 :goto_1

    .line 177
    .restart local v19    # "loadMCCstrForCompare":Landroid/content/SharedPreferences;
    .restart local v26    # "numericMCCvalue":Ljava/lang/String;
    .restart local v35    # "tempNumeric":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v26, "-111"

    .line 179
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "gsm.operator.numeric is null : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_2

    .line 186
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v26

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    move-result v40

    if-nez v40, :cond_1

    .line 189
    :try_start_2
    new-instance v12, Landroid/content/Intent;

    const-string v40, "forexit.action.NITZPOPUP"

    move-object/from16 v0, v40

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 190
    .local v12, "iNITZ":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->removeStickyBroadcast(Landroid/content/Intent;)V

    .line 191
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v13

    .line 193
    .local v13, "initMCC":Landroid/content/SharedPreferences;
    invoke-interface {v13}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v14

    .line 194
    .local v14, "initMCCedit":Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "-111"

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-interface {v14, v0, v1}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 195
    invoke-interface {v14}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 197
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "MCC init : "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_2
    .catch Ljava/lang/Exception; {:try_start_2 .. :try_end_2} :catch_1

    goto/16 :goto_0

    .line 198
    .end local v12    # "iNITZ":Landroid/content/Intent;
    .end local v13    # "initMCC":Landroid/content/SharedPreferences;
    .end local v14    # "initMCCedit":Landroid/content/SharedPreferences$Editor;
    :catch_1
    move-exception v40

    goto/16 :goto_0

    .line 204
    .end local v19    # "loadMCCstrForCompare":Landroid/content/SharedPreferences;
    .end local v26    # "numericMCCvalue":Ljava/lang/String;
    .end local v35    # "tempNumeric":Ljava/lang/String;
    :cond_6
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.MCC_SET_TIME"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_8

    .line 207
    :try_start_3
    new-instance v28, Landroid/content/Intent;

    const-string v40, "android.intent.action.MCC_SET_TIME"

    move-object/from16 v0, v28

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 208
    .local v28, "removei":Landroid/content/Intent;
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/Context;->removeStickyBroadcast(Landroid/content/Intent;)V

    .line 210
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v18

    .line 212
    .local v18, "loadMCCstr":Landroid/content/SharedPreferences;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const-string v41, "-111"

    move-object/from16 v0, v18

    move-object/from16 v1, v40

    move-object/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v40

    move-object/from16 v0, v40

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    .line 214
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "ACTION_MCC_SET_TIME prevMCCvalue"

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v42, v0

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 216
    const-string v25, ""

    .line 217
    .local v25, "newMCCvalue":Ljava/lang/String;
    const-string v40, "MCC"

    move-object/from16 v0, p2

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v25

    .line 219
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "ACTION_MCC_SET_TIME read MCC = "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 220
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_7

    .line 222
    const-string v40, "PopupuiReceiver"

    const-string v41, "SAME to prevMCCvalue : do not display"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_3
    .catch Ljava/lang/Exception; {:try_start_3 .. :try_end_3} :catch_2

    goto/16 :goto_0

    .line 244
    .end local v18    # "loadMCCstr":Landroid/content/SharedPreferences;
    .end local v25    # "newMCCvalue":Ljava/lang/String;
    .end local v28    # "removei":Landroid/content/Intent;
    :catch_2
    move-exception v10

    .line 245
    .local v10, "eeee":Ljava/lang/Exception;
    const-string v40, "PopupuiReceiver"

    const-string v41, "into the catch "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 246
    invoke-virtual {v10}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 223
    .end local v10    # "eeee":Ljava/lang/Exception;
    .restart local v18    # "loadMCCstr":Landroid/content/SharedPreferences;
    .restart local v25    # "newMCCvalue":Ljava/lang/String;
    .restart local v28    # "removei":Landroid/content/Intent;
    :cond_7
    :try_start_4
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->prevMCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v40

    move-object/from16 v1, v25

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z
    :try_end_4
    .catch Ljava/lang/Exception; {:try_start_4 .. :try_end_4} :catch_2

    move-result v40

    if-nez v40, :cond_1

    .line 225
    :try_start_5
    new-instance v12, Landroid/content/Intent;

    const-string v40, "forexit.action.NITZPOPUP"

    move-object/from16 v0, v40

    invoke-direct {v12, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 226
    .restart local v12    # "iNITZ":Landroid/content/Intent;
    move-object/from16 v0, p1

    invoke-virtual {v0, v12}, Landroid/content/Context;->removeStickyBroadcast(Landroid/content/Intent;)V

    .line 227
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v30

    .line 229
    .local v30, "saveToMCC":Landroid/content/SharedPreferences;
    invoke-interface/range {v30 .. v30}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v31

    .line 231
    .local v31, "saveToMCCedit":Landroid/content/SharedPreferences$Editor;
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->MCCvalue:Ljava/lang/String;

    move-object/from16 v40, v0

    move-object/from16 v0, v31

    move-object/from16 v1, v40

    move-object/from16 v2, v25

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    .line 232
    invoke-interface/range {v31 .. v31}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 238
    invoke-virtual/range {p0 .. p1}, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->showNITZpopup(Landroid/content/Context;)V
    :try_end_5
    .catch Ljava/lang/Exception; {:try_start_5 .. :try_end_5} :catch_3

    goto/16 :goto_0

    .line 239
    .end local v12    # "iNITZ":Landroid/content/Intent;
    .end local v30    # "saveToMCC":Landroid/content/SharedPreferences;
    .end local v31    # "saveToMCCedit":Landroid/content/SharedPreferences$Editor;
    :catch_3
    move-exception v29

    .line 240
    .local v29, "s2":Ljava/lang/Exception;
    :try_start_6
    const-string v40, "PopupuiReceiver"

    const-string v41, "into the catch "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 241
    invoke-virtual/range {v29 .. v29}, Ljava/lang/Exception;->printStackTrace()V
    :try_end_6
    .catch Ljava/lang/Exception; {:try_start_6 .. :try_end_6} :catch_2

    goto/16 :goto_0

    .line 248
    .end local v18    # "loadMCCstr":Landroid/content/SharedPreferences;
    .end local v25    # "newMCCvalue":Ljava/lang/String;
    .end local v28    # "removei":Landroid/content/Intent;
    .end local v29    # "s2":Ljava/lang/Exception;
    :cond_8
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.NITZ_SET_TIME"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_9

    .line 251
    :try_start_7
    const-string v40, "PopupuiReceiver"

    const-string v41, "sendStickyBroadcast QUIT_NITZ_ACTION"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 252
    new-instance v32, Landroid/content/Intent;

    const-string v40, "forexit.action.NITZPOPUP"

    move-object/from16 v0, v32

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 253
    .local v32, "sendi":Landroid/content/Intent;
    move-object/from16 v0, p1

    move-object/from16 v1, v32

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendStickyBroadcast(Landroid/content/Intent;)V

    .line 254
    new-instance v28, Landroid/content/Intent;

    const-string v40, "android.intent.action.NITZ_SET_TIME"

    move-object/from16 v0, v28

    move-object/from16 v1, v40

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 255
    .restart local v28    # "removei":Landroid/content/Intent;
    move-object/from16 v0, p1

    move-object/from16 v1, v28

    invoke-virtual {v0, v1}, Landroid/content/Context;->removeStickyBroadcast(Landroid/content/Intent;)V
    :try_end_7
    .catch Ljava/lang/Exception; {:try_start_7 .. :try_end_7} :catch_4

    goto/16 :goto_0

    .line 256
    .end local v28    # "removei":Landroid/content/Intent;
    .end local v32    # "sendi":Landroid/content/Intent;
    :catch_4
    move-exception v40

    goto/16 :goto_0

    .line 258
    :cond_9
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.SOUND_OFF_TOAST"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_c

    .line 263
    if-eqz v17, :cond_a

    :try_start_8
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    move/from16 v40, v0

    if-eqz v40, :cond_a

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    move/from16 v40, v0

    and-int/lit8 v40, v40, 0x4

    if-nez v40, :cond_1

    .line 268
    :cond_a
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    if-eqz v40, :cond_b

    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v40

    if-eqz v40, :cond_1

    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->isShown()Z

    move-result v40

    if-nez v40, :cond_1

    .line 271
    :cond_b
    const v40, 0x7f06000d

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v40

    sput-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    .line 273
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->show()V
    :try_end_8
    .catch Ljava/lang/Exception; {:try_start_8 .. :try_end_8} :catch_5

    goto/16 :goto_0

    .line 279
    :catch_5
    move-exception v40

    goto/16 :goto_0

    .line 282
    :cond_c
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.EAR_PROTECT_TOAST"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_f

    .line 286
    if-eqz v17, :cond_d

    :try_start_9
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    move/from16 v40, v0

    if-eqz v40, :cond_d

    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    move/from16 v40, v0

    and-int/lit8 v40, v40, 0x4

    if-nez v40, :cond_1

    .line 291
    :cond_d
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    if-eqz v40, :cond_e

    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v40

    if-eqz v40, :cond_1

    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->getView()Landroid/view/View;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/view/View;->isShown()Z

    move-result v40

    if-nez v40, :cond_1

    .line 294
    :cond_e
    const v40, 0x7f06000e

    const/16 v41, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v40

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v40

    sput-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    .line 296
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->mToast:Landroid/widget/Toast;

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->show()V
    :try_end_9
    .catch Ljava/lang/Exception; {:try_start_9 .. :try_end_9} :catch_6

    goto/16 :goto_0

    .line 301
    :catch_6
    move-exception v40

    goto/16 :goto_0

    .line 304
    :cond_f
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.BOOT_COMPLETED"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_16

    .line 305
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040003

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_10

    .line 306
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v40

    const-string v41, "airplane_mode_on"

    const/16 v42, 0x0

    invoke-static/range {v40 .. v42}, Landroid/provider/Settings$System;->getInt(Landroid/content/ContentResolver;Ljava/lang/String;I)I

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_14

    const/4 v11, 0x1

    .line 307
    .local v11, "flightModeEnabled":Z
    :goto_3
    if-eqz v11, :cond_10

    .line 308
    const/16 v40, 0x1

    sput-boolean v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->airplanePopupShown:Z

    .line 309
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 310
    .local v6, "cintent":Landroid/content/Intent;
    const/high16 v40, 0x10000000

    move/from16 v0, v40

    invoke-virtual {v6, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 311
    const-string v40, "com.sec.android.app.popupuireceiver"

    const-string v41, "com.sec.android.app.popupuireceiver.popupNetworkError"

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 315
    const-string v40, "network_err_type"

    const/16 v41, 0x8

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 317
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    .line 320
    .end local v6    # "cintent":Landroid/content/Intent;
    .end local v11    # "flightModeEnabled":Z
    :cond_10
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040001

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_11

    sget-boolean v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->airplanePopupShown:Z

    if-nez v40, :cond_11

    .line 322
    :try_start_a
    const-string v40, "connectivity"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/net/ConnectivityManager;

    .line 324
    .local v7, "cm":Landroid/net/ConnectivityManager;
    if-nez v7, :cond_15

    .line 325
    const-string v40, "cm is null"

    const/16 v41, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->show()V

    .line 326
    const-string v40, "PopupuiReceiver"

    const-string v41, "ConnectivityManager is null"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_a
    .catch Ljava/lang/Exception; {:try_start_a .. :try_end_a} :catch_7

    .line 347
    .end local v7    # "cm":Landroid/net/ConnectivityManager;
    :cond_11
    :goto_4
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040002

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1

    .line 348
    const-string v40, "PopupuiReceiver"

    const-string v41, "Batterycover is on"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 349
    const-string v40, "true"

    const-string v41, "ril.domesticOtaStart"

    const-string v42, ""

    invoke-static/range {v41 .. v42}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 350
    const-string v40, "PopupuiReceiver"

    const-string v41, "ril. condition"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 352
    const-string v40, "activity"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    .line 353
    .local v4, "activityManager":Landroid/app/ActivityManager;
    const/16 v40, 0x1

    move/from16 v0, v40

    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v34

    .line 354
    .local v34, "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/16 v40, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v40

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 355
    .local v37, "topTask":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v36

    .line 356
    .local v36, "topActivity":Ljava/lang/String;
    if-eqz v36, :cond_12

    const-string v40, "com.sec.android.app.popupuireceiver.SviewCover"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 359
    :cond_12
    invoke-static {}, Landroid/os/UserHandle;->myUserId()I

    move-result v39

    .line 360
    .local v39, "userId":I
    const-string v40, "FINISH"

    const-string v41, "persist.sys.setupwizard"

    invoke-static/range {v41 .. v41}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_1

    if-nez v39, :cond_1

    .line 361
    const-string v40, "PopupuiReceiver"

    const-string v41, "FINISH condition"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 364
    if-eqz v17, :cond_13

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    move/from16 v40, v0

    if-eqz v40, :cond_1

    .line 368
    :cond_13
    new-instance v33, Landroid/content/Intent;

    const-class v40, Lcom/sec/android/app/popupuireceiver/BatteryCover;

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 369
    .local v33, "showBatteryCoverPopup":Landroid/content/Intent;
    const/high16 v40, 0x10000000

    move-object/from16 v0, v33

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 370
    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 306
    .end local v4    # "activityManager":Landroid/app/ActivityManager;
    .end local v33    # "showBatteryCoverPopup":Landroid/content/Intent;
    .end local v34    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v36    # "topActivity":Ljava/lang/String;
    .end local v37    # "topTask":Landroid/app/ActivityManager$RunningTaskInfo;
    .end local v39    # "userId":I
    :cond_14
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 327
    .restart local v7    # "cm":Landroid/net/ConnectivityManager;
    :cond_15
    :try_start_b
    invoke-virtual {v7}, Landroid/net/ConnectivityManager;->getMobileDataEnabled()Z

    move-result v40

    if-nez v40, :cond_11

    .line 328
    const-string v40, "PopupuiReceiver"

    const-string v41, "cm normally got"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 329
    const-string v40, "data off"

    const/16 v41, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-static {v0, v1, v2}, Landroid/widget/Toast;->makeText(Landroid/content/Context;Ljava/lang/CharSequence;I)Landroid/widget/Toast;

    move-result-object v40

    invoke-virtual/range {v40 .. v40}, Landroid/widget/Toast;->show()V

    .line 330
    new-instance v6, Landroid/content/Intent;

    invoke-direct {v6}, Landroid/content/Intent;-><init>()V

    .line 331
    .restart local v6    # "cintent":Landroid/content/Intent;
    const/high16 v40, 0x10000000

    move/from16 v0, v40

    invoke-virtual {v6, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 332
    const-string v40, "com.sec.android.app.popupuireceiver"

    const-string v41, "com.sec.android.app.popupuireceiver.popupNetworkError"

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 336
    const-string v40, "network_err_type"

    const/16 v41, 0x6

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 338
    const-string v40, "mobile_data_only"

    const/16 v41, 0x1

    move-object/from16 v0, v40

    move/from16 v1, v41

    invoke-virtual {v6, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 339
    move-object/from16 v0, p1

    invoke-virtual {v0, v6}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_b
    .catch Ljava/lang/Exception; {:try_start_b .. :try_end_b} :catch_7

    goto/16 :goto_4

    .line 341
    .end local v6    # "cintent":Landroid/content/Intent;
    .end local v7    # "cm":Landroid/net/ConnectivityManager;
    :catch_7
    move-exception v9

    .line 342
    .local v9, "e5":Ljava/lang/Exception;
    const-string v40, "PopupuiReceiver"

    const-string v41, "into the catch "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 343
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_4

    .line 378
    .end local v9    # "e5":Ljava/lang/Exception;
    :cond_16
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "com.sec.android.app.secsetupwizard.SETUPWIZARD_COMPLETE"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_17

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "com.sec.android.app.popupuireceiver.action.BATTERY_COVER_WATERPROOF_POPUP"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_19

    .line 380
    :cond_17
    const-string v40, "PopupuiReceiver"

    const-string v41, "insert and remove, setupwizard finished"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 381
    const-string v40, "true"

    const-string v41, "ril.domesticOtaStart"

    const-string v42, ""

    invoke-static/range {v41 .. v42}, Landroid/os/SystemProperties;->get(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 382
    const-string v40, "PopupuiReceiver"

    const-string v41, "ril.domestic in"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 383
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040002

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1

    .line 384
    const-string v40, "PopupuiReceiver"

    const-string v41, "Battery cover true"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 387
    if-eqz v17, :cond_18

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    move/from16 v40, v0

    if-eqz v40, :cond_1

    .line 391
    :cond_18
    new-instance v33, Landroid/content/Intent;

    const-class v40, Lcom/sec/android/app/popupuireceiver/BatteryCover;

    move-object/from16 v0, v33

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 392
    .restart local v33    # "showBatteryCoverPopup":Landroid/content/Intent;
    const/high16 v40, 0x10000000

    move-object/from16 v0, v33

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 393
    move-object/from16 v0, p1

    move-object/from16 v1, v33

    invoke-virtual {v0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 399
    .end local v33    # "showBatteryCoverPopup":Landroid/content/Intent;
    :cond_19
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "com.android.systemui.power.action.ACTION_CABLE_CONNECTED"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_1f

    .line 400
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040002

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1a

    const-string v40, ""

    const-string v41, "KMINIBATT"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_1a

    .line 403
    const-string v40, ""

    const-string v41, "USB"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_1a

    .line 405
    new-instance v16, Landroid/content/Intent;

    const-class v40, Lcom/sec/android/app/popupuireceiver/PopupuiService;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 406
    .local v16, "it":Landroid/content/Intent;
    const-string v40, "isDisconnect"

    const/16 v41, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 407
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 411
    .end local v16    # "it":Landroid/content/Intent;
    :cond_1a
    const-string v40, "power_sharing"

    const/16 v41, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1c

    .line 413
    invoke-static/range {p1 .. p1}, Lcom/sec/android/emergencymode/EmergencyManager;->isEmergencyMode(Landroid/content/Context;)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1b

    .line 415
    const-string v40, "PopupuiReceiver"

    const-string v41, "Emergency Mode is Enable!!"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 419
    :cond_1b
    const-string v40, "ro.build.characteristics"

    invoke-static/range {v40 .. v40}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v21

    .line 420
    .local v21, "mDeviceType":Ljava/lang/String;
    if-eqz v21, :cond_1d

    const-string v40, "tablet"

    move-object/from16 v0, v21

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-eqz v40, :cond_1d

    const/16 v22, 0x1

    .line 421
    .local v22, "mIsTablet":Z
    :goto_5
    const-string v40, "PopupuiReceiver"

    const-string v41, "power sharing start"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 422
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v27

    .line 426
    .local v27, "pm":Landroid/content/pm/PackageManager;
    :try_start_c
    const-string v40, "CheckShow"

    const/16 v41, 0x0

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v20

    .line 428
    .local v20, "loadPowerSharingCheckval":Landroid/content/SharedPreferences;
    const-string v40, "CheckShow"

    const/16 v41, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v40

    move/from16 v0, v40

    move-object/from16 v1, p0

    iput v0, v1, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->checkval:I
    :try_end_c
    .catch Ljava/lang/Exception; {:try_start_c .. :try_end_c} :catch_8

    .line 439
    .end local v20    # "loadPowerSharingCheckval":Landroid/content/SharedPreferences;
    :goto_6
    const/16 v40, 0x1

    move/from16 v0, v22

    move/from16 v1, v40

    if-ne v0, v1, :cond_1e

    .line 441
    const v40, 0x7f060003

    :try_start_d
    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    sput-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name_Tablet:Ljava/lang/String;

    .line 442
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name_Tablet:Ljava/lang/String;

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 443
    const-string v40, "PopupuiReceiver"

    const-string v41, "check to Tablet Package"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_d
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_d .. :try_end_d} :catch_9

    .line 472
    .end local v21    # "mDeviceType":Ljava/lang/String;
    .end local v22    # "mIsTablet":Z
    .end local v27    # "pm":Landroid/content/pm/PackageManager;
    :cond_1c
    :goto_7
    const-string v40, "PopupuiReceiver"

    const-string v41, "Action cable connected ! on - "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_0

    .line 420
    .restart local v21    # "mDeviceType":Ljava/lang/String;
    :cond_1d
    const/16 v22, 0x0

    goto :goto_5

    .line 431
    .restart local v22    # "mIsTablet":Z
    .restart local v27    # "pm":Landroid/content/pm/PackageManager;
    :catch_8
    move-exception v9

    .line 432
    .restart local v9    # "e5":Ljava/lang/Exception;
    const-string v40, "PopupuiReceiver"

    const-string v41, "Fail to checkshowagain "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 433
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto :goto_6

    .line 446
    .end local v9    # "e5":Ljava/lang/Exception;
    :cond_1e
    const v40, 0x7f060002

    :try_start_e
    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    sput-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name:Ljava/lang/String;

    .line 447
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name:Ljava/lang/String;

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 448
    const-string v40, "PopupuiReceiver"

    const-string v41, "check to SmartPhone Package"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_e
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_e .. :try_end_e} :catch_9

    goto :goto_7

    .line 458
    :catch_9
    move-exception v8

    .line 460
    .local v8, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    move-object/from16 v0, p0

    iget v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->checkval:I

    move/from16 v40, v0

    if-nez v40, :cond_1c

    .line 462
    const-string v40, "PopupuiReceiver"

    const-string v41, "powersharing popup try to show"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 463
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 464
    .local v5, "call_intent":Landroid/content/Intent;
    const-string v40, "com.sec.android.app.popupuireceiver"

    const-string v41, "com.sec.android.app.popupuireceiver.popupPowerSharing"

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 466
    const/high16 v40, 0x10000000

    move/from16 v0, v40

    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 467
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_7

    .line 473
    .end local v5    # "call_intent":Landroid/content/Intent;
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v21    # "mDeviceType":Ljava/lang/String;
    .end local v22    # "mIsTablet":Z
    .end local v27    # "pm":Landroid/content/pm/PackageManager;
    :cond_1f
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "com.android.systemui.power.action.ACTION_CABLE_DISCONNECTED"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_22

    .line 474
    const-string v40, "activity"

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/app/ActivityManager;

    .line 475
    .restart local v4    # "activityManager":Landroid/app/ActivityManager;
    const/16 v40, 0x1

    move/from16 v0, v40

    invoke-virtual {v4, v0}, Landroid/app/ActivityManager;->getRunningTasks(I)Ljava/util/List;

    move-result-object v34

    .line 476
    .restart local v34    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    const/16 v40, 0x0

    move-object/from16 v0, v34

    move/from16 v1, v40

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v37

    check-cast v37, Landroid/app/ActivityManager$RunningTaskInfo;

    .line 477
    .restart local v37    # "topTask":Landroid/app/ActivityManager$RunningTaskInfo;
    move-object/from16 v0, v37

    iget-object v0, v0, Landroid/app/ActivityManager$RunningTaskInfo;->topActivity:Landroid/content/ComponentName;

    move-object/from16 v40, v0

    invoke-virtual/range {v40 .. v40}, Landroid/content/ComponentName;->toString()Ljava/lang/String;

    move-result-object v36

    .line 479
    .restart local v36    # "topActivity":Ljava/lang/String;
    if-eqz v36, :cond_20

    const-string v40, "com.android.incallui.InCallActivity"

    move-object/from16 v0, v36

    move-object/from16 v1, v40

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 482
    :cond_20
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v40

    const v41, 0x7f040002

    invoke-virtual/range {v40 .. v41}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_1

    const-string v40, ""

    const-string v41, "KMINIBATT"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 485
    const-string v40, ""

    const-string v41, "USB"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v40

    if-nez v40, :cond_1

    .line 487
    const-string v40, "PopupuiReceiver"

    const-string v41, "USB Water Proof popup showing"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 490
    if-eqz v17, :cond_21

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getCheckCoverPopupState:Z

    move/from16 v40, v0

    if-eqz v40, :cond_1

    .line 494
    :cond_21
    new-instance v16, Landroid/content/Intent;

    const-class v40, Lcom/sec/android/app/popupuireceiver/PopupuiService;

    move-object/from16 v0, v16

    move-object/from16 v1, p1

    move-object/from16 v2, v40

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 495
    .restart local v16    # "it":Landroid/content/Intent;
    const-string v40, "isDisconnect"

    const/16 v41, 0x1

    move-object/from16 v0, v16

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 496
    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/content/Context;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    goto/16 :goto_0

    .line 502
    .end local v4    # "activityManager":Landroid/app/ActivityManager;
    .end local v16    # "it":Landroid/content/Intent;
    .end local v34    # "tasks":Ljava/util/List;, "Ljava/util/List<Landroid/app/ActivityManager$RunningTaskInfo;>;"
    .end local v36    # "topActivity":Ljava/lang/String;
    .end local v37    # "topTask":Landroid/app/ActivityManager$RunningTaskInfo;
    :cond_22
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "android.intent.action.ACTION_HDMI_AUDIO_CH_POPUP"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_23

    .line 504
    :try_start_f
    const-string v40, "state"

    const/16 v41, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v24

    .line 505
    .local v24, "nAudiostate":I
    const-string v40, "channels"

    const/16 v41, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v23

    .line 514
    .local v23, "nAudiochannels":I
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "Broadcast Receiver: Got ACTION_HDMI_AUDIO, state = "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v24

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    const-string v42, "channels = "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    move/from16 v1, v23

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 518
    if-eqz v24, :cond_1

    .line 519
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move/from16 v2, v24

    move/from16 v3, v23

    invoke-virtual {v0, v1, v2, v3}, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->showAudiopopup(Landroid/content/Context;II)V
    :try_end_f
    .catch Ljava/lang/Exception; {:try_start_f .. :try_end_f} :catch_a

    goto/16 :goto_0

    .line 521
    .end local v23    # "nAudiochannels":I
    .end local v24    # "nAudiostate":I
    :catch_a
    move-exception v9

    .line 522
    .restart local v9    # "e5":Ljava/lang/Exception;
    const-string v40, "PopupuiReceiver"

    const-string v41, "into the catch "

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 523
    invoke-virtual {v9}, Ljava/lang/Exception;->printStackTrace()V

    goto/16 :goto_0

    .line 526
    .end local v9    # "e5":Ljava/lang/Exception;
    :cond_23
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v40

    const-string v41, "com.sec.android.app.popupuireceiver.action.montblanc"

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_25

    .line 527
    const-string v40, "PopupuiReceiver"

    const-string v41, "Action Mont Blanc"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 529
    const-string v40, "CheckShowMonblanc"

    const/16 v41, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v40

    const-string v41, "CheckShowMonblanc"

    const/16 v42, 0x0

    invoke-interface/range {v40 .. v42}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v40

    const/16 v41, 0x1

    move/from16 v0, v40

    move/from16 v1, v41

    if-ne v0, v1, :cond_24

    const/4 v15, 0x1

    .line 531
    .local v15, "isCheckedBoxChecked":Z
    :goto_8
    const-string v40, "PopupuiReceiver"

    new-instance v41, Ljava/lang/StringBuilder;

    invoke-direct/range {v41 .. v41}, Ljava/lang/StringBuilder;-><init>()V

    const-string v42, "Action Mont Blanc :: isCheckedBoxChecked -> "

    invoke-virtual/range {v41 .. v42}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v41

    move-object/from16 v0, v41

    invoke-virtual {v0, v15}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v41

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->i(Ljava/lang/String;Ljava/lang/String;)I

    .line 532
    if-nez v15, :cond_1

    .line 535
    invoke-virtual/range {p1 .. p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v27

    .line 538
    .restart local v27    # "pm":Landroid/content/pm/PackageManager;
    const v40, 0x7f060004

    :try_start_10
    move-object/from16 v0, p1

    move/from16 v1, v40

    invoke-virtual {v0, v1}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v40

    check-cast v40, Ljava/lang/String;

    sput-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name:Ljava/lang/String;

    .line 539
    sget-object v40, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->IPS_package_name:Ljava/lang/String;

    const/16 v41, 0x1

    move-object/from16 v0, v27

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    .line 540
    const-string v40, "PopupuiReceiver"

    const-string v41, "check to Mont Blanc Package"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I
    :try_end_10
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_10 .. :try_end_10} :catch_b

    goto/16 :goto_0

    .line 541
    :catch_b
    move-exception v8

    .line 543
    .restart local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    const-string v40, "PopupuiReceiver"

    const-string v41, "Mont Blanc popup try to show"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 544
    new-instance v5, Landroid/content/Intent;

    invoke-direct {v5}, Landroid/content/Intent;-><init>()V

    .line 545
    .restart local v5    # "call_intent":Landroid/content/Intent;
    const-string v40, "com.sec.android.app.popupuireceiver"

    const-string v41, "com.sec.android.app.popupuireceiver.popupMontblanc"

    move-object/from16 v0, v40

    move-object/from16 v1, v41

    invoke-virtual {v5, v0, v1}, Landroid/content/Intent;->setClassName(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 547
    const/high16 v40, 0x10000000

    move/from16 v0, v40

    invoke-virtual {v5, v0}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 548
    move-object/from16 v0, p1

    invoke-virtual {v0, v5}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_0

    .line 529
    .end local v5    # "call_intent":Landroid/content/Intent;
    .end local v8    # "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    .end local v15    # "isCheckedBoxChecked":Z
    .end local v27    # "pm":Landroid/content/pm/PackageManager;
    :cond_24
    const/4 v15, 0x0

    goto :goto_8

    .line 550
    :cond_25
    const-string v40, "android.intent.action.PACKAGE_REMOVED"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_1

    .line 551
    const-string v40, "PopupuiReceiver"

    const-string v41, "Action package removed"

    invoke-static/range {v40 .. v41}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 552
    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v40

    if-eqz v40, :cond_1

    .line 553
    const-string v40, "com.sec.android.app.montblanc"

    invoke-virtual/range {p2 .. p2}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v41

    invoke-virtual/range {v41 .. v41}, Landroid/net/Uri;->getSchemeSpecificPart()Ljava/lang/String;

    move-result-object v41

    invoke-virtual/range {v40 .. v41}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v40

    if-eqz v40, :cond_1

    .line 554
    const-string v40, "CheckShowMonblanc"

    const/16 v41, 0x1

    move-object/from16 v0, p1

    move-object/from16 v1, v40

    move/from16 v2, v41

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v40

    const-string v41, "CheckShowMonblanc"

    const/16 v42, 0x0

    invoke-interface/range {v40 .. v42}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v40

    invoke-interface/range {v40 .. v40}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto/16 :goto_0
.end method

.method showAudiopopup(Landroid/content/Context;II)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "nAudiostate"    # I
    .param p3, "nAudiochannels"    # I

    .prologue
    .line 115
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v1

    .line 116
    .local v1, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    if-eqz v2, :cond_0

    .line 117
    iget v2, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    and-int/lit8 v2, v2, 0x4

    if-eqz v2, :cond_0

    .line 128
    :goto_0
    return-void

    .line 123
    :cond_0
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/popupuireceiver/popupAudio;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 124
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 125
    const-string v2, "AudioState"

    invoke-virtual {v0, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 126
    const-string v2, "Audiochannels"

    invoke-virtual {v0, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 127
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method showNITZpopup(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 98
    invoke-static {}, Landroid/app/enterprise/knoxcustom/KnoxCustomManager;->getInstance()Landroid/app/enterprise/knoxcustom/KnoxCustomManager;

    move-result-object v1

    .line 100
    .local v1, "knoxCustomManager":Landroid/app/enterprise/knoxcustom/KnoxCustomManager;
    if-eqz v1, :cond_0

    iget-boolean v2, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedState:Z

    if-eqz v2, :cond_0

    .line 101
    iget v2, p0, Lcom/sec/android/app/popupuireceiver/PopupuiReceiver;->getSealedHideNotificationMessages:I

    and-int/lit8 v2, v2, 0x10

    if-eqz v2, :cond_0

    .line 111
    :goto_0
    return-void

    .line 107
    :cond_0
    const-string v2, "PopupuiReceiver"

    const-string v3, "showNITZpopup()"

    invoke-static {v2, v3}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 108
    new-instance v0, Landroid/content/Intent;

    const-class v2, Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-direct {v0, p1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    .line 109
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v2, 0x10000000

    invoke-virtual {v0, v2}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 110
    invoke-virtual {p1, v0}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method
