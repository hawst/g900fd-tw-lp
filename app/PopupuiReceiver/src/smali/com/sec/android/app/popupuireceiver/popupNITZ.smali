.class public Lcom/sec/android/app/popupuireceiver/popupNITZ;
.super Landroid/app/Activity;
.source "popupNITZ.java"

# interfaces
.implements Landroid/content/DialogInterface$OnDismissListener;


# instance fields
.field private MCCvalue:Ljava/lang/String;

.field NITZQUITReceiver:Landroid/content/BroadcastReceiver;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    .line 28
    const-string v0, "mccstrcmp"

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->MCCvalue:Ljava/lang/String;

    .line 147
    new-instance v0, Lcom/sec/android/app/popupuireceiver/popupNITZ$4;

    invoke-direct {v0, p0}, Lcom/sec/android/app/popupuireceiver/popupNITZ$4;-><init>(Lcom/sec/android/app/popupuireceiver/popupNITZ;)V

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    return-void
.end method

.method static synthetic access$000(Lcom/sec/android/app/popupuireceiver/popupNITZ;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/sec/android/app/popupuireceiver/popupNITZ;

    .prologue
    .line 23
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->MCCvalue:Ljava/lang/String;

    return-object v0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v1, 0x1

    .line 35
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 36
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->requestWindowFeature(I)Z

    .line 37
    const v0, 0x7f030006

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->setContentView(I)V

    .line 39
    invoke-virtual {p0, v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->showDialog(I)V

    .line 41
    return-void
.end method

.method protected onCreateDialog(I)Landroid/app/Dialog;
    .locals 7
    .param p1, "id"    # I

    .prologue
    .line 58
    new-instance v1, Landroid/content/IntentFilter;

    invoke-direct {v1}, Landroid/content/IntentFilter;-><init>()V

    .line 59
    .local v1, "filter":Landroid/content/IntentFilter;
    const-string v4, "forexit.action.NITZPOPUP"

    invoke-virtual {v1, v4}, Landroid/content/IntentFilter;->addAction(Ljava/lang/String;)V

    .line 60
    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v4, v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 62
    invoke-static {p0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 63
    .local v0, "factory":Landroid/view/LayoutInflater;
    const v4, 0x7f030007

    const/4 v5, 0x0

    invoke-virtual {v0, v4, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v3

    .line 67
    .local v3, "popupEntryView":Landroid/view/View;
    const v4, 0x7f090008

    invoke-virtual {v3, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    .line 68
    .local v2, "msg":Landroid/widget/TextView;
    const v4, 0x7f06000a

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setText(I)V

    .line 70
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-direct {v4, p0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v5, 0x7f06000c

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v3}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setCancelable(Z)Landroid/app/AlertDialog$Builder;

    move-result-object v5

    const-string v4, "VZW"

    const-string v6, "ro.csc.sales_code"

    invoke-static {v6}, Landroid/os/SystemProperties;->get(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v4, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_0

    const v4, 0x7f060006

    :goto_0
    new-instance v6, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;

    invoke-direct {v6, p0}, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;-><init>(Lcom/sec/android/app/popupuireceiver/popupNITZ;)V

    invoke-virtual {v5, v4, v6}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    new-instance v5, Lcom/sec/android/app/popupuireceiver/popupNITZ$2;

    invoke-direct {v5, p0}, Lcom/sec/android/app/popupuireceiver/popupNITZ$2;-><init>(Lcom/sec/android/app/popupuireceiver/popupNITZ;)V

    invoke-virtual {v4, v5}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    const v5, 0x7f060009

    new-instance v6, Lcom/sec/android/app/popupuireceiver/popupNITZ$1;

    invoke-direct {v6, p0}, Lcom/sec/android/app/popupuireceiver/popupNITZ$1;-><init>(Lcom/sec/android/app/popupuireceiver/popupNITZ;)V

    invoke-virtual {v4, v5, v6}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v4

    return-object v4

    :cond_0
    const v4, 0x7f060007

    goto :goto_0
.end method

.method public onDismiss(Landroid/content/DialogInterface;)V
    .locals 2
    .param p1, "arg0"    # Landroid/content/DialogInterface;

    .prologue
    .line 46
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v0, :cond_0

    .line 47
    iget-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {p0, v0}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 48
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    .line 51
    const-string v0, "PopupuiReceiver"

    const-string v1, "popupNITZ.java: onDestroy() unReigister NITZQUITReceiver"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 54
    :cond_0
    return-void
.end method
