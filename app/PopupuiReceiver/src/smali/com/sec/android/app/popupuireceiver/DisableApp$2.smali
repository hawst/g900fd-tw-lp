.class Lcom/sec/android/app/popupuireceiver/DisableApp$2;
.super Ljava/lang/Object;
.source "DisableApp.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/DisableApp;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/DisableApp;)V
    .locals 0

    .prologue
    .line 87
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/DisableApp$2;->this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 8
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    .line 89
    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/DisableApp$2;->this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;

    invoke-virtual {v4}, Lcom/sec/android/app/popupuireceiver/DisableApp;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    .line 91
    .local v3, "pm":Landroid/content/pm/PackageManager;
    :try_start_0
    # getter for: Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/DisableApp;->access$000()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/content/pm/PackageManager;->getApplicationEnabledSetting(Ljava/lang/String;)I

    move-result v0

    .line 93
    .local v0, "ApplicationState":I
    const-string v4, "PopupuiReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "package name : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    # getter for: Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/DisableApp;->access$000()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 94
    const-string v4, "PopupuiReceiver"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "state : "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 96
    const/4 v4, 0x3

    if-eq v0, v4, :cond_0

    const/4 v4, 0x2

    if-ne v0, v4, :cond_1

    .line 98
    :cond_0
    const-string v4, "PopupuiReceiver"

    const-string v5, "COMPONENT_ENABLED_STATE_DISABLED_USER"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    const-string v4, "PopupuiReceiver"

    const-string v5, "Open Detatil Setting"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 100
    new-instance v4, Landroid/content/Intent;

    invoke-direct {v4}, Landroid/content/Intent;-><init>()V

    const-string v5, "android.settings.APPLICATION_DETAILS_SETTINGS"

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setAction(Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v4

    const-string v5, "package"

    # getter for: Lcom/sec/android/app/popupuireceiver/DisableApp;->package_Name:Ljava/lang/String;
    invoke-static {}, Lcom/sec/android/app/popupuireceiver/DisableApp;->access$000()Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x0

    invoke-static {v5, v6, v7}, Landroid/net/Uri;->fromParts(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v5

    invoke-virtual {v4, v5}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v2

    .line 103
    .local v2, "in":Landroid/content/Intent;
    const/high16 v4, 0x10800000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 104
    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/DisableApp$2;->this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;

    invoke-virtual {v4, v2}, Lcom/sec/android/app/popupuireceiver/DisableApp;->startActivity(Landroid/content/Intent;)V

    .line 105
    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/DisableApp$2;->this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;

    invoke-virtual {v4}, Lcom/sec/android/app/popupuireceiver/DisableApp;->finish()V
    :try_end_0
    .catch Ljava/lang/IllegalArgumentException; {:try_start_0 .. :try_end_0} :catch_0

    .line 111
    .end local v0    # "ApplicationState":I
    .end local v2    # "in":Landroid/content/Intent;
    :cond_1
    :goto_0
    return-void

    .line 107
    :catch_0
    move-exception v1

    .line 108
    .local v1, "e":Ljava/lang/IllegalArgumentException;
    const-string v4, "PopupuiReceiver"

    const-string v5, "unknown package"

    invoke-static {v4, v5}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 109
    iget-object v4, p0, Lcom/sec/android/app/popupuireceiver/DisableApp$2;->this$0:Lcom/sec/android/app/popupuireceiver/DisableApp;

    invoke-virtual {v4}, Lcom/sec/android/app/popupuireceiver/DisableApp;->finish()V

    goto :goto_0
.end method
