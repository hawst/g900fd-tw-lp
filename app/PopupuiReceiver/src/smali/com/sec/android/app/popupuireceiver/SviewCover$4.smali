.class Lcom/sec/android/app/popupuireceiver/SviewCover$4;
.super Ljava/lang/Object;
.source "SviewCover.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/SviewCover;->showPopupSviewCover()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/SviewCover;)V
    .locals 0

    .prologue
    .line 225
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 4
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    const/4 v3, 0x0

    .line 228
    if-eqz p2, :cond_1

    .line 229
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/SviewCover;->access$100(Lcom/sec/android/app/popupuireceiver/SviewCover;)Landroid/os/Handler;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 230
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCover;->mHandler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/sec/android/app/popupuireceiver/SviewCover;->access$100(Lcom/sec/android/app/popupuireceiver/SviewCover;)Landroid/os/Handler;

    move-result-object v1

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    # getter for: Lcom/sec/android/app/popupuireceiver/SviewCover;->mSviewCoverTask:Ljava/lang/Runnable;
    invoke-static {v2}, Lcom/sec/android/app/popupuireceiver/SviewCover;->access$200(Lcom/sec/android/app/popupuireceiver/SviewCover;)Ljava/lang/Runnable;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 232
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SVIEW_COVER_POPUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 233
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "show_sviewcover_popup"

    invoke-interface {v0, v1, v3}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 234
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 241
    :goto_0
    return-void

    .line 237
    .end local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    :cond_1
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/SviewCover$4;->this$0:Lcom/sec/android/app/popupuireceiver/SviewCover;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/SviewCover;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const-string v2, "SVIEW_COVER_POPUP"

    invoke-virtual {v1, v2, v3}, Landroid/content/Context;->getSharedPreferences(Ljava/lang/String;I)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 238
    .restart local v0    # "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "show_sviewcover_popup"

    const/4 v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 239
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    goto :goto_0
.end method
