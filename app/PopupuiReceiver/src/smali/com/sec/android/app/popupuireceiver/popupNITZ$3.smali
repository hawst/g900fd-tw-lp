.class Lcom/sec/android/app/popupuireceiver/popupNITZ$3;
.super Ljava/lang/Object;
.source "popupNITZ.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/sec/android/app/popupuireceiver/popupNITZ;->onCreateDialog(I)Landroid/app/Dialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;


# direct methods
.method constructor <init>(Lcom/sec/android/app/popupuireceiver/popupNITZ;)V
    .locals 0

    .prologue
    .line 73
    iput-object p1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 4
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "whichButton"    # I

    .prologue
    const/4 v3, 0x0

    .line 77
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.settings.DATE_SETTINGS"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 78
    .local v0, "intent":Landroid/content/Intent;
    const/high16 v1, 0x14200000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 81
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_time"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 82
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    const-string v2, "auto_time_zone"

    invoke-static {v1, v2, v3}, Landroid/provider/Settings$Global;->putInt(Landroid/content/ContentResolver;Ljava/lang/String;I)Z

    .line 84
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->getApplicationContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f06000b

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Landroid/widget/Toast;->makeText(Landroid/content/Context;II)Landroid/widget/Toast;

    move-result-object v1

    invoke-virtual {v1}, Landroid/widget/Toast;->show()V

    .line 86
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-virtual {v1, v0}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->startActivity(Landroid/content/Intent;)V

    .line 89
    const-string v1, "PopupuiReceiver"

    const-string v2, "popupNITZ.java: OK button"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 91
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    iget-object v1, v1, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    if-eqz v1, :cond_0

    .line 92
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    iget-object v2, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    iget-object v2, v2, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    invoke-virtual {v1, v2}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 93
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    const/4 v2, 0x0

    iput-object v2, v1, Lcom/sec/android/app/popupuireceiver/popupNITZ;->NITZQUITReceiver:Landroid/content/BroadcastReceiver;

    .line 96
    const-string v1, "PopupuiReceiver"

    const-string v2, "popupNITZ.java: OK btn unReigister NITZQUITReceiver"

    invoke-static {v1, v2}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 99
    :cond_0
    iget-object v1, p0, Lcom/sec/android/app/popupuireceiver/popupNITZ$3;->this$0:Lcom/sec/android/app/popupuireceiver/popupNITZ;

    invoke-virtual {v1}, Lcom/sec/android/app/popupuireceiver/popupNITZ;->finish()V

    .line 100
    return-void
.end method
