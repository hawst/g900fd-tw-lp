.class public final Landroid/support/v7/appcompat/R$attr;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v7/appcompat/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "attr"
.end annotation


# static fields
.field public static final actionBarPopupTheme:I = 0x7f010030

.field public static final actionBarSize:I = 0x7f010035

.field public static final actionBarStyle:I = 0x7f010031

.field public static final actionBarTabBarStyle:I = 0x7f01002c

.field public static final actionBarTabStyle:I = 0x7f01002b

.field public static final actionBarTabTextStyle:I = 0x7f01002d

.field public static final actionBarTheme:I = 0x7f010033

.field public static final actionBarWidgetTheme:I = 0x7f010034

.field public static final actionDropDownStyle:I = 0x7f010049

.field public static final actionModePopupWindowStyle:I = 0x7f010046

.field public static final actionModeShareDrawable:I = 0x7f010043

.field public static final actionModeStyle:I = 0x7f01003a

.field public static final actionOverflowButtonStyle:I = 0x7f01002e

.field public static final actionOverflowMenuStyle:I = 0x7f01002f

.field public static final colorControlActivated:I = 0x7f010072

.field public static final colorControlNormal:I = 0x7f010071

.field public static final colorSwitchThumbNormal:I = 0x7f010075

.field public static final drawerArrowStyle:I = 0x7f0100c2

.field public static final dropDownListViewStyle:I = 0x7f010066

.field public static final homeAsUpIndicator:I = 0x7f01004d

.field public static final listPopupWindowStyle:I = 0x7f010067

.field public static final panelMenuListTheme:I = 0x7f01006c

.field public static final popupMenuStyle:I = 0x7f010058

.field public static final searchViewStyle:I = 0x7f010060

.field public static final switchStyle:I = 0x7f01005c

.field public static final textAllCaps:I = 0x7f0100a7

.field public static final textColorSearchUrl:I = 0x7f01005f

.field public static final toolbarNavigationButtonStyle:I = 0x7f010057

.field public static final toolbarStyle:I = 0x7f010056
