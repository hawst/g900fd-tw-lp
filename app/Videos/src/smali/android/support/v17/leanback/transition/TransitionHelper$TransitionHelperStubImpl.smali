.class final Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl;
.super Ljava/lang/Object;
.source "TransitionHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/transition/TransitionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TransitionHelperStubImpl"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 128
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 130
    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/transition/TransitionHelper$1;

    .prologue
    .line 128
    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public addTarget(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 288
    return-void
.end method

.method public addTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "transitionSet"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 228
    return-void
.end method

.method public createChangeBounds(Z)Ljava/lang/Object;
    .locals 2
    .param p1, "reparent"    # Z

    .prologue
    .line 191
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-object v0
.end method

.method public createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 292
    const/4 v0, 0x0

    return-object v0
.end method

.method public createFadeTransition(I)Ljava/lang/Object;
    .locals 2
    .param p1, "fadingMode"    # I

    .prologue
    .line 186
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-object v0
.end method

.method public createScale()Ljava/lang/Object;
    .locals 2

    .prologue
    .line 201
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-object v0
.end method

.method public createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;
    .locals 0
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 176
    return-object p2
.end method

.method public createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;
    .locals 2
    .param p1, "callback"    # Landroid/support/v17/leanback/transition/SlideCallback;

    .prologue
    .line 196
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-object v0
.end method

.method public createTransitionSet(Z)Ljava/lang/Object;
    .locals 2
    .param p1, "sequential"    # Z

    .prologue
    .line 223
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-object v0
.end method

.method public excludeChildren(Ljava/lang/Object;IZ)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "targetId"    # I
    .param p3, "exclude"    # Z

    .prologue
    .line 240
    return-void
.end method

.method public getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
    .locals 1
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 136
    const/4 v0, 0x0

    return-object v0
.end method

.method public runTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 3
    .param p1, "scene"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 269
    move-object v1, p2

    check-cast v1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    .line 270
    .local v1, "transitionStub":Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;
    if-eqz v1, :cond_0

    iget-object v2, v1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;->mTransitionListener:Landroid/support/v17/leanback/transition/TransitionListener;

    if-eqz v2, :cond_0

    .line 271
    iget-object v2, v1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;->mTransitionListener:Landroid/support/v17/leanback/transition/TransitionListener;

    invoke-virtual {v2, p2}, Landroid/support/v17/leanback/transition/TransitionListener;->onTransitionStart(Ljava/lang/Object;)V

    :cond_0
    move-object v0, p1

    .line 273
    check-cast v0, Ljava/lang/Runnable;

    .line 274
    .local v0, "r":Ljava/lang/Runnable;
    if-eqz v0, :cond_1

    .line 275
    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 277
    :cond_1
    if-eqz v1, :cond_2

    iget-object v2, v1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;->mTransitionListener:Landroid/support/v17/leanback/transition/TransitionListener;

    if-eqz v2, :cond_2

    .line 278
    iget-object v2, v1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;->mTransitionListener:Landroid/support/v17/leanback/transition/TransitionListener;

    invoke-virtual {v2, p2}, Landroid/support/v17/leanback/transition/TransitionListener;->onTransitionEnd(Ljava/lang/Object;)V

    .line 280
    :cond_2
    return-void
.end method

.method public setDuration(Ljava/lang/Object;J)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "duration"    # J

    .prologue
    .line 260
    return-void
.end method

.method public setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "timeInterpolator"    # Ljava/lang/Object;

    .prologue
    .line 284
    return-void
.end method

.method public setStartDelay(Ljava/lang/Object;J)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "startDelay"    # J

    .prologue
    .line 256
    return-void
.end method

.method public setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "listener"    # Landroid/support/v17/leanback/transition/TransitionListener;

    .prologue
    .line 264
    check-cast p1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;

    .end local p1    # "transition":Ljava/lang/Object;
    iput-object p2, p1, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl$TransitionStub;->mTransitionListener:Landroid/support/v17/leanback/transition/TransitionListener;

    .line 265
    return-void
.end method
