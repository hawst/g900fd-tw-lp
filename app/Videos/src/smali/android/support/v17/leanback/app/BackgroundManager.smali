.class public final Landroid/support/v17/leanback/app/BackgroundManager;
.super Ljava/lang/Object;
.source "BackgroundManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;,
        Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;,
        Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;,
        Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;
    }
.end annotation


# static fields
.field private static final FRAGMENT_TAG:Ljava/lang/String;


# instance fields
.field private mAttached:Z

.field private mBackgroundColor:I

.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mBgView:Landroid/view/View;

.field private mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

.field private mColorWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

.field private mContext:Landroid/content/Context;

.field private mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

.field private mHandler:Landroid/os/Handler;

.field private mHeightPx:I

.field private final mImageInListener:Landroid/animation/Animator$AnimatorListener;

.field private mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

.field private mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

.field private mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

.field private mLayerWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

.field private mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

.field private mThemeDrawable:Landroid/graphics/drawable/Drawable;

.field private mThemeDrawableResourceId:I

.field private mWidthPx:I

.field private mWindow:Landroid/view/Window;

.field private mWindowManager:Landroid/view/WindowManager;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 96
    const-class v0, Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    return-void
.end method

.method private constructor <init>(Landroid/app/Activity;Z)V
    .locals 5
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "isSupportFragmentActivity"    # Z

    .prologue
    const/4 v4, 0x0

    .line 371
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 733
    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager$1;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/app/BackgroundManager$1;-><init>(Landroid/support/v17/leanback/app/BackgroundManager;)V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInListener:Landroid/animation/Animator$AnimatorListener;

    .line 372
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    .line 373
    invoke-static {}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->getInstance()Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    .line 374
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHeightPx:I

    .line 375
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    iget v1, v1, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWidthPx:I

    .line 376
    new-instance v1, Landroid/os/Handler;

    invoke-direct {v1}, Landroid/os/Handler;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHandler:Landroid/os/Handler;

    .line 378
    invoke-virtual {p1}, Landroid/app/Activity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [I

    const v3, 0x1010054

    aput v3, v2, v4

    invoke-virtual {v1, v2}, Landroid/content/res/Resources$Theme;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 380
    .local v0, "ta":Landroid/content/res/TypedArray;
    const/4 v1, -0x1

    invoke-virtual {v0, v4, v1}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawableResourceId:I

    .line 381
    iget v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawableResourceId:I

    if-gez v1, :cond_0

    .line 384
    :cond_0
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 386
    if-eqz p2, :cond_1

    .line 387
    check-cast p1, Landroid/support/v4/app/FragmentActivity;

    .end local p1    # "activity":Landroid/app/Activity;
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BackgroundManager;->createSupportFragment(Landroid/support/v4/app/FragmentActivity;)V

    .line 391
    :goto_0
    return-void

    .line 389
    .restart local p1    # "activity":Landroid/app/Activity;
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BackgroundManager;->createFragment(Landroid/app/Activity;)V

    goto :goto_0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    return-object v0
.end method

.method static synthetic access$102(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;)Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;
    .param p1, "x1"    # Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    .prologue
    .line 79
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    return-object p1
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->applyBackgroundChanges()V

    return-void
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->lazyInit()V

    return-void
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method static synthetic access$302(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 79
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    return-object p1
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;
    .param p1, "x1"    # Landroid/graphics/drawable/Drawable;
    .param p2, "x2"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 79
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/app/BackgroundManager;->sameDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    return v0
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->releaseBackgroundBitmap()V

    return-void
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    return-object v0
.end method

.method static synthetic access$602(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;
    .param p1, "x1"    # Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .prologue
    .line 79
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    return-object p1
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    return-object v0
.end method

.method static synthetic access$702(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;
    .param p1, "x1"    # Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .prologue
    .line 79
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    return-object p1
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/LayerDrawable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 79
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    return-object v0
.end method

.method private applyBackgroundChanges()V
    .locals 6

    .prologue
    const/16 v5, 0x1f4

    const/4 v4, 0x0

    .line 700
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mAttached:Z

    if-eqz v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-nez v1, :cond_1

    .line 731
    :cond_0
    :goto_0
    return-void

    .line 706
    :cond_1
    const/4 v0, 0x0

    .line 708
    .local v0, "dimAlpha":I
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-eqz v1, :cond_2

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->isAnimationPending()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 710
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->startAnimation()V

    .line 711
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 712
    const/16 v0, 0xcc

    .line 715
    :cond_2
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-nez v1, :cond_3

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v1, :cond_3

    .line 717
    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {v1, v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 718
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v2, Landroid/support/v17/leanback/R$id;->background_imagein:I

    iget-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2, v3}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 720
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    .line 721
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1, v5, v4}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->fadeIn(II)V

    .line 722
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->startAnimation(Landroid/animation/Animator$AnimatorListener;)V

    .line 723
    const/16 v0, 0xff

    .line 726
    :cond_3
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    .line 728
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1, v5, v4, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->fade(III)V

    .line 729
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->startAnimation()V

    goto :goto_0
.end method

.method private attachToView(Landroid/view/View;)V
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/View;

    .prologue
    .line 513
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBgView:Landroid/view/View;

    .line 514
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mAttached:Z

    .line 515
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->syncWithService()V

    .line 516
    return-void
.end method

.method private createEmptyDrawable()Landroid/graphics/drawable/Drawable;
    .locals 3

    .prologue
    .line 837
    const/4 v0, 0x0

    .line 838
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V

    return-object v1
.end method

.method private createFragment(Landroid/app/Activity;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 395
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/BackgroundFragment;

    .line 397
    .local v0, "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    if-nez v0, :cond_1

    .line 398
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundFragment;

    .end local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    invoke-direct {v0}, Landroid/support/v17/leanback/app/BackgroundFragment;-><init>()V

    .line 399
    .restart local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    invoke-virtual {p1}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/app/FragmentTransaction;->add(Landroid/app/Fragment;Ljava/lang/String;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 406
    :cond_0
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/app/BackgroundFragment;->setBackgroundManager(Landroid/support/v17/leanback/app/BackgroundManager;)V

    .line 407
    return-void

    .line 401
    :cond_1
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundFragment;->getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 402
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Created duplicated BackgroundManager for same activity, please use getInstance() instead"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method private createSupportFragment(Landroid/support/v4/app/FragmentActivity;)V
    .locals 3
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 411
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;

    .line 413
    .local v0, "fragment":Landroid/support/v17/leanback/app/BackgroundSupportFragment;
    if-nez v0, :cond_1

    .line 414
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;

    .end local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundSupportFragment;
    invoke-direct {v0}, Landroid/support/v17/leanback/app/BackgroundSupportFragment;-><init>()V

    .line 415
    .restart local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundSupportFragment;
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v1, v0, v2}, Landroid/support/v4/app/FragmentTransaction;->add(Landroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 423
    :cond_0
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->setBackgroundManager(Landroid/support/v17/leanback/app/BackgroundManager;)V

    .line 424
    return-void

    .line 418
    :cond_1
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 419
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Created duplicated BackgroundManager for same activity, please use getInstance() instead"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1
.end method

.method public static getInstance(Landroid/app/Activity;)Landroid/support/v17/leanback/app/BackgroundManager;
    .locals 4
    .param p0, "activity"    # Landroid/app/Activity;

    .prologue
    .line 341
    instance-of v2, p0, Landroid/support/v4/app/FragmentActivity;

    if-eqz v2, :cond_1

    .line 342
    check-cast p0, Landroid/support/v4/app/FragmentActivity;

    .end local p0    # "activity":Landroid/app/Activity;
    invoke-static {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->getSupportInstance(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v1

    .line 354
    .local v0, "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    .restart local p0    # "activity":Landroid/app/Activity;
    :cond_0
    :goto_0
    return-object v1

    .line 344
    .end local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    :cond_1
    invoke-virtual {p0}, Landroid/app/Activity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v2

    sget-object v3, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/BackgroundFragment;

    .line 346
    .restart local v0    # "fragment":Landroid/support/v17/leanback/app/BackgroundFragment;
    if-eqz v0, :cond_2

    .line 347
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundFragment;->getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v1

    .line 348
    .local v1, "manager":Landroid/support/v17/leanback/app/BackgroundManager;
    if-nez v1, :cond_0

    .line 354
    .end local v1    # "manager":Landroid/support/v17/leanback/app/BackgroundManager;
    :cond_2
    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Landroid/support/v17/leanback/app/BackgroundManager;-><init>(Landroid/app/Activity;Z)V

    goto :goto_0
.end method

.method private static getSupportInstance(Landroid/support/v4/app/FragmentActivity;)Landroid/support/v17/leanback/app/BackgroundManager;
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;

    .prologue
    .line 358
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    sget-object v3, Landroid/support/v17/leanback/app/BackgroundManager;->FRAGMENT_TAG:Ljava/lang/String;

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;

    .line 360
    .local v0, "fragment":Landroid/support/v17/leanback/app/BackgroundSupportFragment;
    if-eqz v0, :cond_0

    .line 361
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v1

    .line 362
    .local v1, "manager":Landroid/support/v17/leanback/app/BackgroundManager;
    if-eqz v1, :cond_0

    .line 368
    .end local v1    # "manager":Landroid/support/v17/leanback/app/BackgroundManager;
    :goto_0
    return-object v1

    :cond_0
    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager;

    const/4 v2, 0x1

    invoke-direct {v1, p0, v2}, Landroid/support/v17/leanback/app/BackgroundManager;-><init>(Landroid/app/Activity;Z)V

    goto :goto_0
.end method

.method private getThemeDrawable()Landroid/graphics/drawable/Drawable;
    .locals 4

    .prologue
    .line 323
    const/4 v0, 0x0

    .line 324
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawableResourceId:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 325
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    iget v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawableResourceId:I

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 327
    :cond_0
    if-nez v0, :cond_1

    .line 328
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 330
    :cond_1
    return-object v0
.end method

.method private lazyInit()V
    .locals 3

    .prologue
    .line 460
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    if-eqz v0, :cond_0

    .line 477
    :goto_0
    return-void

    .line 464
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    sget v1, Landroid/support/v17/leanback/R$drawable;->lb_background:I

    invoke-static {v0, v1}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    check-cast v0, Landroid/graphics/drawable/LayerDrawable;

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    .line 466
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBgView:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 468
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_imageout:I

    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 470
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v2, Landroid/support/v17/leanback/R$id;->background_dim:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 473
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 475
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v2, Landroid/support/v17/leanback/R$id;->background_color:I

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/LayerDrawable;->findDrawableByLayerId(I)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mColorWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    goto :goto_0
.end method

.method private releaseBackgroundBitmap()V
    .locals 1

    .prologue
    .line 572
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 573
    return-void
.end method

.method private sameDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z
    .locals 4
    .param p1, "first"    # Landroid/graphics/drawable/Drawable;
    .param p2, "second"    # Landroid/graphics/drawable/Drawable;

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 767
    if-eqz p1, :cond_0

    if-nez p2, :cond_2

    :cond_0
    move v0, v1

    .line 778
    .end local p1    # "first":Landroid/graphics/drawable/Drawable;
    .end local p2    # "second":Landroid/graphics/drawable/Drawable;
    :cond_1
    :goto_0
    return v0

    .line 770
    .restart local p1    # "first":Landroid/graphics/drawable/Drawable;
    .restart local p2    # "second":Landroid/graphics/drawable/Drawable;
    :cond_2
    if-eq p1, p2, :cond_1

    .line 773
    instance-of v2, p1, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    if-eqz v2, :cond_3

    instance-of v2, p2, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    if-eqz v2, :cond_3

    .line 774
    check-cast p1, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    .end local p1    # "first":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p1}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v2

    check-cast p2, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    .end local p2    # "second":Landroid/graphics/drawable/Drawable;
    invoke-virtual {p2}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->getBitmap()Landroid/graphics/Bitmap;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/graphics/Bitmap;->sameAs(Landroid/graphics/Bitmap;)Z

    move-result v2

    if-nez v2, :cond_1

    :cond_3
    move v0, v1

    .line 778
    goto :goto_0
.end method

.method private setDrawableInternal(Landroid/graphics/drawable/Drawable;)V
    .locals 4
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 626
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mAttached:Z

    if-nez v0, :cond_0

    .line 627
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Must attach before setting background drawable"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 630
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    if-eqz v0, :cond_3

    .line 631
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->access$000(Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Landroid/support/v17/leanback/app/BackgroundManager;->sameDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 644
    :cond_1
    :goto_0
    return-void

    .line 635
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->cancel()V

    .line 637
    :cond_3
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    invoke-direct {v0, p0, p1}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;-><init>(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    .line 639
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-eqz v0, :cond_4

    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->isAnimationStarted()Z

    move-result v0

    if-nez v0, :cond_1

    .line 642
    :cond_4
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHandler:Landroid/os/Handler;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method private showWallpaper(Z)V
    .locals 3
    .param p1, "show"    # Z

    .prologue
    const/high16 v2, 0x100000

    .line 842
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindow:Landroid/view/Window;

    if-nez v1, :cond_1

    .line 862
    :cond_0
    :goto_0
    return-void

    .line 846
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindow:Landroid/view/Window;

    invoke-virtual {v1}, Landroid/view/Window;->getAttributes()Landroid/view/WindowManager$LayoutParams;

    move-result-object v0

    .line 847
    .local v0, "layoutParams":Landroid/view/WindowManager$LayoutParams;
    if-eqz p1, :cond_2

    .line 848
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v1, v2

    if-nez v1, :cond_0

    .line 852
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    or-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    .line 861
    :goto_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindow:Landroid/view/Window;

    invoke-virtual {v1, v0}, Landroid/view/Window;->setAttributes(Landroid/view/WindowManager$LayoutParams;)V

    goto :goto_0

    .line 854
    :cond_2
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    and-int/2addr v1, v2

    if-eqz v1, :cond_0

    .line 858
    iget v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    const v2, -0x100001

    and-int/2addr v1, v2

    iput v1, v0, Landroid/view/WindowManager$LayoutParams;->flags:I

    goto :goto_1
.end method

.method private syncWithService()V
    .locals 3

    .prologue
    .line 446
    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->getColor()I

    move-result v0

    .line 447
    .local v0, "color":I
    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    .line 452
    .local v1, "drawable":Landroid/graphics/drawable/Drawable;
    iput v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundColor:I

    .line 453
    if-nez v1, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 456
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->updateImmediate()V

    .line 457
    return-void

    .line 453
    :cond_0
    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v2}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    goto :goto_0
.end method

.method private updateImmediate()V
    .locals 3

    .prologue
    const/4 v1, 0x0

    .line 576
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->lazyInit()V

    .line 578
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mColorWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundColor:I

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setColor(I)V

    .line 579
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-eqz v0, :cond_0

    .line 580
    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundColor:I

    if-nez v0, :cond_3

    move v0, v1

    :goto_0
    invoke-virtual {v2, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    .line 582
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundColor:I

    if-nez v0, :cond_1

    const/4 v1, 0x1

    :cond_1
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/app/BackgroundManager;->showWallpaper(Z)V

    .line 584
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->getThemeDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawable:Landroid/graphics/drawable/Drawable;

    .line 585
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_theme:I

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 587
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_4

    .line 588
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_imagein:I

    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 597
    :cond_2
    :goto_1
    return-void

    .line 580
    :cond_3
    const/16 v0, 0xcc

    goto :goto_0

    .line 591
    :cond_4
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 592
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_imagein:I

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 593
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    if-eqz v0, :cond_2

    .line 594
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    const/16 v1, 0xff

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    goto :goto_1
.end method


# virtual methods
.method public attach(Landroid/view/Window;)V
    .locals 1
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 486
    invoke-virtual {p1}, Landroid/view/Window;->getDecorView()Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/BackgroundManager;->attachToView(Landroid/view/View;)V

    .line 488
    return-void
.end method

.method detach()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 525
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->release()V

    .line 527
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindowManager:Landroid/view/WindowManager;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBgView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 528
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindowManager:Landroid/view/WindowManager;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBgView:Landroid/view/View;

    invoke-interface {v0, v1}, Landroid/view/WindowManager;->removeViewImmediate(Landroid/view/View;)V

    .line 531
    :cond_0
    iput-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindowManager:Landroid/view/WindowManager;

    .line 532
    iput-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWindow:Landroid/view/Window;

    .line 533
    iput-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBgView:Landroid/view/View;

    .line 534
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mAttached:Z

    .line 536
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    if-eqz v0, :cond_1

    .line 537
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->unref()V

    .line 538
    iput-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    .line 540
    :cond_1
    return-void
.end method

.method onActivityResume()V
    .locals 2

    .prologue
    .line 430
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    if-nez v0, :cond_0

    .line 443
    :goto_0
    return-void

    .line 433
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    if-nez v0, :cond_1

    .line 436
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->syncWithService()V

    goto :goto_0

    .line 440
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    iget v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->setColor(I)V

    .line 441
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 553
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    if-eqz v0, :cond_0

    .line 554
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_imagein:I

    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 555
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    sget v1, Landroid/support/v17/leanback/R$id;->background_imageout:I

    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 556
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;

    .line 558
    :cond_0
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mLayerWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 559
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 560
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 561
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mColorWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 562
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mDimWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 563
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mThemeDrawable:Landroid/graphics/drawable/Drawable;

    .line 564
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    if-eqz v0, :cond_1

    .line 565
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->cancel()V

    .line 566
    iput-object v3, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    .line 568
    :cond_1
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager;->releaseBackgroundBitmap()V

    .line 569
    return-void
.end method

.method public setBitmap(Landroid/graphics/Bitmap;)V
    .locals 9
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 657
    if-nez p1, :cond_1

    .line 658
    const/4 v7, 0x0

    invoke-direct {p0, v7}, Landroid/support/v17/leanback/app/BackgroundManager;->setDrawableInternal(Landroid/graphics/drawable/Drawable;)V

    .line 697
    :cond_0
    :goto_0
    return-void

    .line 662
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    if-lez v7, :cond_0

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    if-lez v7, :cond_0

    .line 669
    const/4 v4, 0x0

    .line 671
    .local v4, "matrix":Landroid/graphics/Matrix;
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v7

    iget v8, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWidthPx:I

    if-ne v7, v8, :cond_2

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    iget v8, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHeightPx:I

    if-eq v7, v8, :cond_3

    .line 672
    :cond_2
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v2

    .line 673
    .local v2, "dwidth":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v1

    .line 677
    .local v1, "dheight":I
    iget v7, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHeightPx:I

    mul-int/2addr v7, v2

    iget v8, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWidthPx:I

    mul-int/2addr v8, v1

    if-le v7, v8, :cond_4

    .line 678
    iget v7, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mHeightPx:I

    int-to-float v7, v7

    int-to-float v8, v1

    div-float v5, v7, v8

    .line 683
    .local v5, "scale":F
    :goto_1
    iget v7, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWidthPx:I

    int-to-float v7, v7

    div-float/2addr v7, v5

    float-to-int v7, v7

    invoke-static {v7, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    .line 684
    .local v6, "subX":I
    const/4 v7, 0x0

    sub-int v8, v2, v6

    div-int/lit8 v8, v8, 0x2

    invoke-static {v7, v8}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 686
    .local v3, "dx":I
    new-instance v4, Landroid/graphics/Matrix;

    .end local v4    # "matrix":Landroid/graphics/Matrix;
    invoke-direct {v4}, Landroid/graphics/Matrix;-><init>()V

    .line 687
    .restart local v4    # "matrix":Landroid/graphics/Matrix;
    invoke-virtual {v4, v5, v5}, Landroid/graphics/Matrix;->setScale(FF)V

    .line 688
    neg-int v7, v3

    int-to-float v7, v7

    const/4 v8, 0x0

    invoke-virtual {v4, v7, v8}, Landroid/graphics/Matrix;->preTranslate(FF)Z

    .line 694
    .end local v1    # "dheight":I
    .end local v2    # "dwidth":I
    .end local v3    # "dx":I
    .end local v5    # "scale":F
    .end local v6    # "subX":I
    :cond_3
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;

    iget-object v7, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mContext:Landroid/content/Context;

    invoke-virtual {v7}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    invoke-direct {v0, v7, p1, v4}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 696
    .local v0, "bitmapDrawable":Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/BackgroundManager;->setDrawableInternal(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 680
    .end local v0    # "bitmapDrawable":Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;
    .restart local v1    # "dheight":I
    .restart local v2    # "dwidth":I
    :cond_4
    iget v7, p0, Landroid/support/v17/leanback/app/BackgroundManager;->mWidthPx:I

    int-to-float v7, v7

    int-to-float v8, v2

    div-float v5, v7, v8

    .restart local v5    # "scale":F
    goto :goto_1
.end method
