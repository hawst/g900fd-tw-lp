.class Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;
.super Ljava/lang/Object;
.source "VerticalGridSupportFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/VerticalGridSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V
    .locals 0

    .prologue
    .line 154
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 158
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$000(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 159
    .local v0, "position":I
    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VerticalGridSupportFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "row selected position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 160
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->onRowSelected(I)V
    invoke-static {v1, v0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$200(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;I)V

    .line 161
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$300(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 162
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$300(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    invoke-interface {v1, p2, p4}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 164
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$400(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 165
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$400(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    invoke-interface {v1, p1, p2, p3, p4}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 168
    :cond_2
    return-void
.end method
