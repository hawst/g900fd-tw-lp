.class final Landroid/support/v17/leanback/widget/RoundedRectHelper;
.super Ljava/lang/Object;
.source "RoundedRectHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/RoundedRectHelper$1;,
        Landroid/support/v17/leanback/widget/RoundedRectHelper$Api21Impl;,
        Landroid/support/v17/leanback/widget/RoundedRectHelper$StubImpl;,
        Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;
    }
.end annotation


# static fields
.field private static final sInstance:Landroid/support/v17/leanback/widget/RoundedRectHelper;


# instance fields
.field private mImpl:Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 26
    new-instance v0, Landroid/support/v17/leanback/widget/RoundedRectHelper;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/RoundedRectHelper;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->sInstance:Landroid/support/v17/leanback/widget/RoundedRectHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 95
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 96
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 97
    new-instance v0, Landroid/support/v17/leanback/widget/RoundedRectHelper$Api21Impl;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/widget/RoundedRectHelper$Api21Impl;-><init>(Landroid/support/v17/leanback/widget/RoundedRectHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->mImpl:Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;

    .line 101
    :goto_0
    return-void

    .line 99
    :cond_0
    new-instance v0, Landroid/support/v17/leanback/widget/RoundedRectHelper$StubImpl;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/widget/RoundedRectHelper$StubImpl;-><init>(Landroid/support/v17/leanback/widget/RoundedRectHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->mImpl:Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;

    goto :goto_0
.end method

.method public static getInstance()Landroid/support/v17/leanback/widget/RoundedRectHelper;
    .locals 1

    .prologue
    .line 33
    sget-object v0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->sInstance:Landroid/support/v17/leanback/widget/RoundedRectHelper;

    return-object v0
.end method


# virtual methods
.method public clearBackground(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 54
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->mImpl:Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;->clearBackground(Landroid/view/View;)V

    .line 55
    return-void
.end method

.method public setRoundedRectBackground(Landroid/view/View;I)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 45
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RoundedRectHelper;->mImpl:Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;->setRoundedRectBackground(Landroid/view/View;I)V

    .line 46
    return-void
.end method
