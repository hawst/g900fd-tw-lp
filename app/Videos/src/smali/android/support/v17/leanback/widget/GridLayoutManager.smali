.class final Landroid/support/v17/leanback/widget/GridLayoutManager;
.super Landroid/support/v7/widget/RecyclerView$LayoutManager;
.source "GridLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;,
        Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    }
.end annotation


# instance fields
.field private final mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

.field private mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

.field private mChildVisibility:I

.field final mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

.field private mFirstVisiblePos:I

.field private mFixedRowSizeSecondary:I

.field private mFocusOutEnd:Z

.field private mFocusOutFront:Z

.field private mFocusPosition:I

.field private mFocusPositionOffset:I

.field private mFocusScrollStrategy:I

.field private mFocusSearchDisabled:Z

.field private mForceFullLayout:Z

.field private mGravity:I

.field private mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

.field private mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

.field private mHorizontalMargin:I

.field private mInLayout:Z

.field private mInSelection:Z

.field private final mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

.field private mLastVisiblePos:I

.field private mLayoutEnabled:Z

.field private mMarginPrimary:I

.field private mMarginSecondary:I

.field private mMaxSizeSecondary:I

.field private mMeasuredDimension:[I

.field private mNumRows:I

.field private mNumRowsRequested:I

.field private mOrientation:I

.field private mPruneChild:Z

.field private mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

.field private final mRequestLayoutRunnable:Ljava/lang/Runnable;

.field private mRowSecondarySizeRefresh:Z

.field private mRowSizeSecondary:[I

.field private mRowSizeSecondaryRequested:I

.field private mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

.field private mScrollEnabled:Z

.field private mScrollOffsetPrimary:I

.field private mScrollOffsetSecondary:I

.field private mSizePrimary:I

.field private mState:Landroid/support/v7/widget/RecyclerView$State;

.field private mTempDeltas:[I

.field private mVerticalMargin:I

.field private final mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/BaseGridView;)V
    .locals 5
    .param p1, "baseGridView"    # Landroid/support/v17/leanback/widget/BaseGridView;

    .prologue
    const/4 v4, 0x2

    const/4 v3, -0x1

    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 355
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$LayoutManager;-><init>()V

    .line 179
    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    .line 184
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    .line 185
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInSelection:Z

    .line 187
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    .line 194
    iput v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 202
    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 212
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    .line 217
    iput v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I

    .line 272
    const/16 v0, 0x33

    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGravity:I

    .line 281
    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    .line 304
    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    .line 308
    new-instance v0, Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    .line 313
    new-instance v0, Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    .line 339
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    .line 344
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    .line 346
    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I

    .line 351
    new-array v0, v4, [I

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMeasuredDimension:[I

    .line 353
    new-instance v0, Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ViewsStateBundle;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    .line 901
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/GridLayoutManager$1;-><init>(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRequestLayoutRunnable:Ljava/lang/Runnable;

    .line 1048
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/GridLayoutManager$2;-><init>(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    .line 356
    iput-object p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    .line 357
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/GridLayoutManager;)Landroid/support/v7/widget/RecyclerView$State;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    return v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    return v0
.end method

.method static synthetic access$102(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    return p1
.end method

.method static synthetic access$110(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 2
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    add-int/lit8 v1, v0, -0x1

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    return v0
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/widget/GridLayoutManager;)Landroid/support/v17/leanback/widget/StaggeredGrid;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    return-object v0
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    return v0
.end method

.method static synthetic access$1300(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # I

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowStartSecondary(I)I

    move-result v0

    return v0
.end method

.method static synthetic access$1400(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    return v0
.end method

.method static synthetic access$1500(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    return v0
.end method

.method static synthetic access$1600(Landroid/support/v17/leanback/widget/GridLayoutManager;ILandroid/view/View;III)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # I
    .param p4, "x4"    # I
    .param p5, "x5"    # I

    .prologue
    .line 48
    invoke-direct/range {p0 .. p5}, Landroid/support/v17/leanback/widget/GridLayoutManager;->layoutChild(ILandroid/view/View;III)V

    return-void
.end method

.method static synthetic access$1700(Landroid/support/v17/leanback/widget/GridLayoutManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMin()V

    return-void
.end method

.method static synthetic access$1800(Landroid/support/v17/leanback/widget/GridLayoutManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMax()V

    return-void
.end method

.method static synthetic access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    return v0
.end method

.method static synthetic access$2000(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/view/View;[I)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # Landroid/view/View;
    .param p2, "x2"    # [I

    .prologue
    .line 48
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getScrollPosition(Landroid/view/View;[I)Z

    move-result v0

    return v0
.end method

.method static synthetic access$202(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # I

    .prologue
    .line 48
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    return p1
.end method

.method static synthetic access$208(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 2
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    add-int/lit8 v1, v0, 0x1

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    return v0
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I

    return v0
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/widget/GridLayoutManager;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    return v0
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    return v0
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/widget/GridLayoutManager;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->dispatchChildSelected()V

    return-void
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->measureChild(Landroid/view/View;)V

    return-void
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/widget/GridLayoutManager;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    return v0
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/GridLayoutManager;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    return-object v0
.end method

.method private appendOneVisibleItem()Z
    .locals 7

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    const/4 v6, -0x1

    .line 1251
    :cond_0
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-eq v4, v6, :cond_1

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_1

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result v5

    if-ge v4, v5, :cond_1

    .line 1254
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    add-int/lit8 v0, v4, 0x1

    .line 1255
    .local v0, "index":I
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v4, v0}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v4

    iget v1, v4, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 1256
    .local v1, "row":I
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    invoke-interface {v4, v0, v1, v3}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->createItem(IIZ)V

    .line 1257
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v4, v4, -0x1

    if-ne v1, v4, :cond_0

    .line 1266
    .end local v0    # "index":I
    .end local v1    # "row":I
    :goto_0
    return v2

    .line 1260
    :cond_1
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-ne v4, v6, :cond_2

    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v4

    if-gtz v4, :cond_3

    :cond_2
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-eq v4, v6, :cond_4

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v5}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v5

    add-int/lit8 v5, v5, -0x1

    if-ge v4, v5, :cond_4

    .line 1263
    :cond_3
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mSizePrimary:I

    add-int/2addr v4, v5

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/StaggeredGrid;->appendItems(I)V

    goto :goto_0

    :cond_4
    move v2, v3

    .line 1266
    goto :goto_0
.end method

.method private appendVisibleItems()V
    .locals 1

    .prologue
    .line 1272
    :cond_0
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->needsAppendVisibleItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1273
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendOneVisibleItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1277
    :cond_1
    return-void
.end method

.method private discardLayoutInfo()V
    .locals 2

    .prologue
    const/4 v1, -0x1

    const/4 v0, 0x0

    .line 2593
    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    .line 2594
    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    .line 2595
    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    .line 2596
    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    .line 2597
    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    .line 2598
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSecondarySizeRefresh:Z

    .line 2599
    return-void
.end method

.method private dispatchChildSelected()V
    .locals 11

    .prologue
    const-wide/16 v8, -0x1

    const/4 v7, -0x1

    .line 519
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    if-nez v0, :cond_0

    .line 532
    :goto_0
    return-void

    .line 522
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-eq v0, v7, :cond_2

    .line 523
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v2

    .line 524
    .local v2, "view":Landroid/view/View;
    if-eqz v2, :cond_2

    .line 525
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/BaseGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v10

    .line 526
    .local v10, "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-nez v10, :cond_1

    move-wide v4, v8

    :goto_1
    invoke-interface/range {v0 .. v5}, Landroid/support/v17/leanback/widget/OnChildSelectedListener;->onChildSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V

    goto :goto_0

    :cond_1
    invoke-virtual {v10}, Landroid/support/v7/widget/RecyclerView$ViewHolder;->getItemId()J

    move-result-wide v4

    goto :goto_1

    .line 531
    .end local v2    # "view":Landroid/view/View;
    .end local v10    # "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    :cond_2
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    const/4 v6, 0x0

    invoke-interface/range {v4 .. v9}, Landroid/support/v17/leanback/widget/OnChildSelectedListener;->onChildSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V

    goto :goto_0
.end method

.method private findImmediateChildIndex(Landroid/view/View;)I
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 2268
    :goto_0
    if-eqz p1, :cond_1

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    if-eq p1, v1, :cond_1

    .line 2269
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 2270
    .local v0, "index":I
    if-ltz v0, :cond_0

    .line 2275
    .end local v0    # "index":I
    :goto_1
    return v0

    .line 2273
    .restart local v0    # "index":I
    :cond_0
    invoke-virtual {p1}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p1

    .end local p1    # "view":Landroid/view/View;
    check-cast p1, Landroid/view/View;

    .line 2274
    .restart local p1    # "view":Landroid/view/View;
    goto :goto_0

    .line 2275
    .end local v0    # "index":I
    :cond_1
    const/4 v0, -0x1

    goto :goto_1
.end method

.method private forceRequestLayout()V
    .locals 2

    .prologue
    .line 898
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRequestLayoutRunnable:Ljava/lang/Runnable;

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->postOnAnimation(Landroid/view/View;Ljava/lang/Runnable;)V

    .line 899
    return-void
.end method

.method private getAlignedPosition(Landroid/view/View;[I)Z
    .locals 5
    .param p1, "view"    # Landroid/view/View;
    .param p2, "deltas"    # [I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2202
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPrimarySystemScrollPosition(Landroid/view/View;)I

    move-result v0

    .line 2203
    .local v0, "scrollPrimary":I
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSecondarySystemScrollPosition(Landroid/view/View;)I

    move-result v1

    .line 2208
    .local v1, "scrollSecondary":I
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int/2addr v0, v4

    .line 2209
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    sub-int/2addr v1, v4

    .line 2210
    if-nez v0, :cond_0

    if-eqz v1, :cond_1

    .line 2211
    :cond_0
    aput v0, p2, v2

    .line 2212
    aput v1, p2, v3

    move v2, v3

    .line 2215
    :cond_1
    return v2
.end method

.method private getMovement(I)I
    .locals 3
    .param p1, "direction"    # I

    .prologue
    .line 2525
    const/16 v0, 0x11

    .line 2527
    .local v0, "movement":I
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v1, :cond_1

    .line 2528
    sparse-switch p1, :sswitch_data_0

    .line 2559
    :cond_0
    :goto_0
    return v0

    .line 2530
    :sswitch_0
    const/4 v0, 0x0

    .line 2531
    goto :goto_0

    .line 2533
    :sswitch_1
    const/4 v0, 0x1

    .line 2534
    goto :goto_0

    .line 2536
    :sswitch_2
    const/4 v0, 0x2

    .line 2537
    goto :goto_0

    .line 2539
    :sswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 2542
    :cond_1
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 2543
    sparse-switch p1, :sswitch_data_1

    goto :goto_0

    .line 2545
    :sswitch_4
    const/4 v0, 0x2

    .line 2546
    goto :goto_0

    .line 2548
    :sswitch_5
    const/4 v0, 0x3

    .line 2549
    goto :goto_0

    .line 2551
    :sswitch_6
    const/4 v0, 0x0

    .line 2552
    goto :goto_0

    .line 2554
    :sswitch_7
    const/4 v0, 0x1

    goto :goto_0

    .line 2528
    nop

    :sswitch_data_0
    .sparse-switch
        0x11 -> :sswitch_0
        0x21 -> :sswitch_2
        0x42 -> :sswitch_1
        0x82 -> :sswitch_3
    .end sparse-switch

    .line 2543
    :sswitch_data_1
    .sparse-switch
        0x11 -> :sswitch_4
        0x21 -> :sswitch_6
        0x42 -> :sswitch_5
        0x82 -> :sswitch_7
    .end sparse-switch
.end method

.method private getNoneAlignedPosition(Landroid/view/View;[I)Z
    .locals 14
    .param p1, "view"    # Landroid/view/View;
    .param p2, "deltas"    # [I

    .prologue
    .line 2126
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v4

    .line 2127
    .local v4, "pos":I
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v11

    .line 2128
    .local v11, "viewMin":I
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v10

    .line 2131
    .local v10, "viewMax":I
    const/4 v1, 0x0

    .line 2132
    .local v1, "firstView":Landroid/view/View;
    const/4 v2, 0x0

    .line 2133
    .local v2, "lastView":Landroid/view/View;
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v12}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v3

    .line 2134
    .local v3, "paddingLow":I
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v12}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v12

    invoke-virtual {v12}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getClientSize()I

    move-result v0

    .line 2135
    .local v0, "clientSize":I
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v12, v4}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v12

    iget v6, v12, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 2136
    .local v6, "row":I
    if-ge v11, v3, :cond_4

    .line 2138
    move-object v1, p1

    .line 2139
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_1

    .line 2142
    :cond_0
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependOneVisibleItem()Z

    move-result v12

    if-nez v12, :cond_1

    .line 2143
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget v13, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    invoke-virtual {v12, v13, v4}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getItemPositionsInRows(II)[Ljava/util/List;

    move-result-object v12

    aget-object v5, v12, v6

    .line 2145
    .local v5, "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v12, 0x0

    invoke-interface {v5, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 2146
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v12

    sub-int v12, v10, v12

    if-le v12, v0, :cond_0

    .line 2147
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    const/4 v13, 0x1

    if-le v12, v13, :cond_1

    .line 2148
    const/4 v12, 0x1

    invoke-interface {v5, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 2176
    .end local v5    # "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_1
    :goto_0
    const/4 v7, 0x0

    .line 2177
    .local v7, "scrollPrimary":I
    const/4 v8, 0x0

    .line 2178
    .local v8, "scrollSecondary":I
    if-eqz v1, :cond_8

    .line 2179
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v12

    sub-int v7, v12, v3

    .line 2184
    :cond_2
    :goto_1
    if-eqz v1, :cond_9

    .line 2185
    move-object v9, v1

    .line 2191
    .local v9, "secondaryAlignedView":Landroid/view/View;
    :goto_2
    invoke-direct {p0, v9}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSecondarySystemScrollPosition(Landroid/view/View;)I

    move-result v8

    .line 2192
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    sub-int/2addr v8, v12

    .line 2193
    if-nez v7, :cond_3

    if-eqz v8, :cond_b

    .line 2194
    :cond_3
    const/4 v12, 0x0

    aput v7, p2, v12

    .line 2195
    const/4 v12, 0x1

    aput v8, p2, v12

    .line 2196
    const/4 v12, 0x1

    .line 2198
    :goto_3
    return v12

    .line 2154
    .end local v7    # "scrollPrimary":I
    .end local v8    # "scrollSecondary":I
    .end local v9    # "secondaryAlignedView":Landroid/view/View;
    :cond_4
    add-int v12, v0, v3

    if-le v10, v12, :cond_1

    .line 2156
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_7

    .line 2158
    move-object v1, p1

    .line 2160
    :cond_5
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget v13, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    invoke-virtual {v12, v4, v13}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getItemPositionsInRows(II)[Ljava/util/List;

    move-result-object v12

    aget-object v5, v12, v6

    .line 2162
    .restart local v5    # "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {v5}, Ljava/util/List;->size()I

    move-result v12

    add-int/lit8 v12, v12, -0x1

    invoke-interface {v5, v12}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v2

    .line 2163
    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v12

    sub-int/2addr v12, v11

    if-le v12, v0, :cond_6

    .line 2164
    const/4 v2, 0x0

    .line 2168
    :goto_4
    if-eqz v2, :cond_1

    .line 2170
    const/4 v1, 0x0

    goto :goto_0

    .line 2167
    :cond_6
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendOneVisibleItem()Z

    move-result v12

    if-eqz v12, :cond_5

    goto :goto_4

    .line 2173
    .end local v5    # "positions":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :cond_7
    move-object v2, p1

    goto :goto_0

    .line 2180
    .restart local v7    # "scrollPrimary":I
    .restart local v8    # "scrollSecondary":I
    :cond_8
    if-eqz v2, :cond_2

    .line 2181
    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v12

    add-int v13, v3, v0

    sub-int v7, v12, v13

    goto :goto_1

    .line 2186
    :cond_9
    if-eqz v2, :cond_a

    .line 2187
    move-object v9, v2

    .restart local v9    # "secondaryAlignedView":Landroid/view/View;
    goto :goto_2

    .line 2189
    .end local v9    # "secondaryAlignedView":Landroid/view/View;
    :cond_a
    move-object v9, p1

    .restart local v9    # "secondaryAlignedView":Landroid/view/View;
    goto :goto_2

    .line 2198
    :cond_b
    const/4 v12, 0x0

    goto :goto_3
.end method

.method private getPositionByIndex(I)I
    .locals 1
    .param p1, "index"    # I

    .prologue
    .line 515
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method private getPositionByView(Landroid/view/View;)I
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v1, -0x1

    .line 503
    if-nez p1, :cond_1

    .line 511
    :cond_0
    :goto_0
    return v1

    .line 506
    :cond_1
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 507
    .local v0, "params":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->isItemRemoved()Z

    move-result v2

    if-nez v2, :cond_0

    .line 511
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getViewPosition()I

    move-result v1

    goto :goto_0
.end method

.method private getPrimarySystemScrollPosition(Landroid/view/View;)I
    .locals 13
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v9, 0x1

    const/4 v10, 0x0

    .line 2048
    iget v11, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenter(Landroid/view/View;)I

    move-result v12

    add-int v8, v11, v12

    .line 2049
    .local v8, "viewCenterPrimary":I
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v5

    .line 2050
    .local v5, "pos":I
    iget-object v11, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v11, v5}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v4

    .line 2051
    .local v4, "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    iget v7, v4, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 2052
    .local v7, "row":I
    iget v11, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-nez v11, :cond_2

    move v1, v9

    .line 2054
    .local v1, "isFirst":Z
    :goto_0
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget-object v11, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    if-nez v11, :cond_3

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getItemCount()I

    move-result v11

    :goto_1
    add-int/lit8 v11, v11, -0x1

    if-ne v12, v11, :cond_4

    move v2, v9

    .line 2056
    .local v2, "isLast":Z
    :goto_2
    if-nez v1, :cond_0

    if-eqz v2, :cond_6

    .line 2057
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v9

    add-int/lit8 v0, v9, -0x1

    .local v0, "i":I
    :goto_3
    if-ltz v0, :cond_6

    .line 2058
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByIndex(I)I

    move-result v6

    .line 2059
    .local v6, "position":I
    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v9, v6}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v3

    .line 2060
    .local v3, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    if-eqz v3, :cond_1

    iget v9, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    if-ne v9, v7, :cond_1

    .line 2061
    if-ge v6, v5, :cond_5

    .line 2062
    const/4 v1, 0x0

    .line 2057
    :cond_1
    :goto_4
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .end local v0    # "i":I
    .end local v1    # "isFirst":Z
    .end local v2    # "isLast":Z
    .end local v3    # "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v6    # "position":I
    :cond_2
    move v1, v10

    .line 2052
    goto :goto_0

    .line 2054
    .restart local v1    # "isFirst":Z
    :cond_3
    iget-object v11, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v11}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v11

    goto :goto_1

    :cond_4
    move v2, v10

    goto :goto_2

    .line 2063
    .restart local v0    # "i":I
    .restart local v2    # "isLast":Z
    .restart local v3    # "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .restart local v6    # "position":I
    :cond_5
    if-le v6, v5, :cond_1

    .line 2064
    const/4 v2, 0x0

    goto :goto_4

    .line 2069
    .end local v0    # "i":I
    .end local v3    # "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v6    # "position":I
    :cond_6
    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v9}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v9

    invoke-virtual {v9, v8, v1, v2}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getSystemScrollPos(IZZ)I

    move-result v9

    return v9
.end method

.method private getRowSizeSecondary(I)I
    .locals 1
    .param p1, "rowIndex"    # I

    .prologue
    .line 766
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    if-eqz v0, :cond_0

    .line 767
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    .line 772
    :goto_0
    return v0

    .line 769
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    if-nez v0, :cond_1

    .line 770
    const/4 v0, 0x0

    goto :goto_0

    .line 772
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    aget v0, v0, p1

    goto :goto_0
.end method

.method private getRowStartSecondary(I)I
    .locals 4
    .param p1, "rowIndex"    # I

    .prologue
    .line 776
    const/4 v1, 0x0

    .line 777
    .local v1, "start":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, p1, :cond_0

    .line 778
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowSizeSecondary(I)I

    move-result v2

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    add-int/2addr v2, v3

    add-int/2addr v1, v2

    .line 777
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 780
    :cond_0
    return v1
.end method

.method private getScrollPosition(Landroid/view/View;[I)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "deltas"    # [I

    .prologue
    .line 2115
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    packed-switch v0, :pswitch_data_0

    .line 2118
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getAlignedPosition(Landroid/view/View;[I)Z

    move-result v0

    .line 2121
    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getNoneAlignedPosition(Landroid/view/View;[I)Z

    move-result v0

    goto :goto_0

    .line 2115
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method private getSecondarySystemScrollPosition(Landroid/view/View;)I
    .locals 10
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 2073
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterSecondary(Landroid/view/View;)I

    move-result v9

    add-int v5, v8, v9

    .line 2074
    .local v5, "viewCenterSecondary":I
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v3

    .line 2075
    .local v3, "pos":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8, v3}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v2

    .line 2076
    .local v2, "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    iget v4, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 2077
    .local v4, "row":I
    if-nez v4, :cond_0

    move v0, v6

    .line 2078
    .local v0, "isFirst":Z
    :goto_0
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getNumRows()I

    move-result v8

    add-int/lit8 v8, v8, -0x1

    if-ne v4, v8, :cond_1

    move v1, v6

    .line 2079
    .local v1, "isLast":Z
    :goto_1
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment;->secondAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v6

    invoke-virtual {v6, v5, v0, v1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getSystemScrollPos(IZZ)I

    move-result v6

    return v6

    .end local v0    # "isFirst":Z
    .end local v1    # "isLast":Z
    :cond_0
    move v0, v7

    .line 2077
    goto :goto_0

    .restart local v0    # "isFirst":Z
    :cond_1
    move v1, v7

    .line 2078
    goto :goto_1
.end method

.method private getSizeSecondary()I
    .locals 2

    .prologue
    .line 784
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v0, v0, -0x1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowStartSecondary(I)I

    move-result v0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v1, v1, -0x1

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowSizeSecondary(I)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private getTag()Ljava/lang/String;
    .locals 2

    .prologue
    .line 171
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "GridLayoutManager:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/BaseGridView;->getId()I

    move-result v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private getViewCenter(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 601
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterX(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterY(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private getViewCenterSecondary(Landroid/view/View;)I
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 605
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterY(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterX(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private getViewCenterX(Landroid/view/View;)I
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 609
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 610
    .local v0, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalLeft(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getAlignX()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method private getViewCenterY(Landroid/view/View;)I
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 614
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 615
    .local v0, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalTop(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getAlignY()I

    move-result v2

    add-int/2addr v1, v2

    return v1
.end method

.method private getViewMax(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 597
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getOpticalRight(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getOpticalBottom(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private getViewMin(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 593
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getOpticalLeft(Landroid/view/View;)I

    move-result v0

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getOpticalTop(Landroid/view/View;)I

    move-result v0

    goto :goto_0
.end method

.method private gridOnRequestFocusInDescendantsAligned(Landroid/support/v7/widget/RecyclerView;ILandroid/graphics/Rect;)Z
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2477
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 2478
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 2479
    invoke-virtual {v1, p2, p3}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v0

    .line 2480
    .local v0, "result":Z
    if-nez v0, :cond_0

    .line 2485
    .end local v0    # "result":Z
    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private gridOnRequestFocusInDescendantsUnaligned(Landroid/support/v7/widget/RecyclerView;ILandroid/graphics/Rect;)Z
    .locals 9
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2494
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v1

    .line 2495
    .local v1, "count":I
    and-int/lit8 v8, p2, 0x2

    if-eqz v8, :cond_0

    .line 2496
    const/4 v5, 0x0

    .line 2497
    .local v5, "index":I
    const/4 v4, 0x1

    .line 2498
    .local v4, "increment":I
    move v2, v1

    .line 2504
    .local v2, "end":I
    :goto_0
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v6

    .line 2505
    .local v6, "left":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getClientSize()I

    move-result v8

    add-int v7, v8, v6

    .line 2506
    .local v7, "right":I
    move v3, v5

    .local v3, "i":I
    :goto_1
    if-eq v3, v2, :cond_2

    .line 2507
    invoke-virtual {p0, v3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 2508
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v8

    if-nez v8, :cond_1

    .line 2509
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v8

    if-lt v8, v6, :cond_1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v8

    if-gt v8, v7, :cond_1

    .line 2510
    invoke-virtual {v0, p2, p3}, Landroid/view/View;->requestFocus(ILandroid/graphics/Rect;)Z

    move-result v8

    if-eqz v8, :cond_1

    .line 2511
    const/4 v8, 0x1

    .line 2516
    .end local v0    # "child":Landroid/view/View;
    :goto_2
    return v8

    .line 2500
    .end local v2    # "end":I
    .end local v3    # "i":I
    .end local v4    # "increment":I
    .end local v5    # "index":I
    .end local v6    # "left":I
    .end local v7    # "right":I
    :cond_0
    add-int/lit8 v5, v1, -0x1

    .line 2501
    .restart local v5    # "index":I
    const/4 v4, -0x1

    .line 2502
    .restart local v4    # "increment":I
    const/4 v2, -0x1

    .restart local v2    # "end":I
    goto :goto_0

    .line 2506
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    .restart local v6    # "left":I
    .restart local v7    # "right":I
    :cond_1
    add-int/2addr v3, v4

    goto :goto_1

    .line 2516
    .end local v0    # "child":Landroid/view/View;
    :cond_2
    const/4 v8, 0x0

    goto :goto_2
.end method

.method private init(I)I
    .locals 13
    .param p1, "focusPosition"    # I

    .prologue
    const/4 v12, -0x1

    const v11, 0x7fffffff

    const/4 v10, 0x0

    .line 647
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v5

    .line 649
    .local v5, "newItemCount":I
    if-ne p1, v12, :cond_0

    if-lez v5, :cond_0

    .line 651
    const/4 p1, 0x0

    .line 656
    :cond_0
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    if-eqz v8, :cond_d

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    array-length v9, v9

    if-ne v8, v9, :cond_d

    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    if-eqz v8, :cond_d

    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getSize()I

    move-result v8

    if-lez v8, :cond_d

    if-ltz p1, :cond_d

    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getFirstIndex()I

    move-result v8

    if-lt p1, v8, :cond_d

    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result v8

    if-gt p1, v8, :cond_d

    .line 661
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8, p1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->stripDownTo(I)V

    .line 663
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getFirstIndex()I

    move-result v0

    .line 664
    .local v0, "firstIndex":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result v3

    .line 668
    .local v3, "lastIndex":I
    move v2, v3

    .local v2, "i":I
    :goto_0
    if-lt v2, v0, :cond_2

    .line 669
    if-lt v2, v5, :cond_1

    .line 670
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->removeLast()V

    .line 668
    :cond_1
    add-int/lit8 v2, v2, -0x1

    goto :goto_0

    .line 673
    :cond_2
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getSize()I

    move-result v8

    if-nez v8, :cond_3

    .line 674
    add-int/lit8 p1, v5, -0x1

    .line 676
    const/4 v2, 0x0

    :goto_1
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v2, v8, :cond_c

    .line 677
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iput v10, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 678
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iput v10, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 676
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 683
    :cond_3
    const/4 v2, 0x0

    :goto_2
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v2, v8, :cond_4

    .line 684
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iput v11, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 685
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    const/high16 v9, -0x80000000

    iput v9, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 683
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 687
    :cond_4
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getFirstIndex()I

    move-result v0

    .line 688
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result v3

    .line 689
    if-le p1, v3, :cond_5

    .line 690
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result p1

    .line 697
    :cond_5
    move v2, v0

    :goto_3
    if-gt v2, v3, :cond_8

    .line 698
    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v7

    .line 699
    .local v7, "v":Landroid/view/View;
    if-nez v7, :cond_7

    .line 697
    :cond_6
    :goto_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 702
    :cond_7
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8, v2}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v8

    iget v6, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 703
    .local v6, "row":I
    invoke-direct {p0, v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v8

    iget v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int v4, v8, v9

    .line 704
    .local v4, "low":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v6

    iget v8, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ge v4, v8, :cond_6

    .line 705
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v6

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v9, v9, v6

    iput v4, v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iput v4, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    goto :goto_4

    .line 708
    .end local v4    # "low":I
    .end local v6    # "row":I
    .end local v7    # "v":Landroid/view/View;
    :cond_8
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v9, v0}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v9

    iget v9, v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    aget-object v8, v8, v9

    iget v1, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 709
    .local v1, "firstItemRowPosition":I
    if-ne v1, v11, :cond_9

    .line 710
    const/4 v1, 0x0

    .line 712
    :cond_9
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView$State;->didStructureChange()Z

    move-result v8

    if-eqz v8, :cond_a

    .line 715
    const/4 v2, 0x0

    :goto_5
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v2, v8, :cond_c

    .line 716
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iput v1, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 717
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iput v1, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 715
    add-int/lit8 v2, v2, 0x1

    goto :goto_5

    .line 721
    :cond_a
    const/4 v2, 0x0

    :goto_6
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v2, v8, :cond_c

    .line 722
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iget v8, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ne v8, v11, :cond_b

    .line 723
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v2

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v9, v9, v2

    iput v1, v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iput v1, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 721
    :cond_b
    add-int/lit8 v2, v2, 0x1

    goto :goto_6

    .line 730
    .end local v1    # "firstItemRowPosition":I
    :cond_c
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {p0, v8}, Landroid/support/v17/leanback/widget/GridLayoutManager;->detachAndScrapAttachedViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 754
    .end local v0    # "firstIndex":I
    .end local v3    # "lastIndex":I
    :goto_7
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/StaggeredGrid;->setProvider(Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;)V

    .line 756
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    invoke-virtual {v8, v9}, Landroid/support/v17/leanback/widget/StaggeredGrid;->setRows([Landroid/support/v17/leanback/widget/StaggeredGrid$Row;)V

    .line 757
    iput v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iput v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    .line 759
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->initScrollController()V

    .line 760
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollSecondAxis()V

    .line 762
    return p1

    .line 734
    .end local v2    # "i":I
    :cond_d
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    new-array v8, v8, [Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    iput-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    .line 736
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_8
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v2, v8, :cond_e

    .line 737
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    new-instance v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    invoke-direct {v9}, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;-><init>()V

    aput-object v9, v8, v2

    .line 736
    add-int/lit8 v2, v2, 0x1

    goto :goto_8

    .line 739
    :cond_e
    new-instance v8, Landroid/support/v17/leanback/widget/StaggeredGridDefault;

    invoke-direct {v8}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;-><init>()V

    iput-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    .line 740
    if-nez v5, :cond_10

    .line 741
    const/4 p1, -0x1

    .line 747
    :cond_f
    :goto_9
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {p0, v8}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 749
    iput v10, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    .line 750
    iput v10, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    .line 751
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->reset()V

    goto :goto_7

    .line 742
    :cond_10
    if-lt p1, v5, :cond_f

    .line 743
    add-int/lit8 p1, v5, -0x1

    goto :goto_9
.end method

.method private initScrollController()V
    .locals 5

    .prologue
    .line 1833
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v2, :cond_0

    .line 1834
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v3, v3, Landroid/support/v17/leanback/widget/WindowAlignment;->horizontal:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v3

    sub-int v0, v2, v3

    .line 1835
    .local v0, "paddingPrimaryDiff":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v3, v3, Landroid/support/v17/leanback/widget/WindowAlignment;->vertical:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v3

    sub-int v1, v2, v3

    .line 1840
    .local v1, "paddingSecondaryDiff":I
    :goto_0
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int/2addr v2, v0

    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    .line 1841
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    sub-int/2addr v2, v1

    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    .line 1843
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v2, v2, Landroid/support/v17/leanback/widget/WindowAlignment;->horizontal:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getWidth()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setSize(I)V

    .line 1844
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v2, v2, Landroid/support/v17/leanback/widget/WindowAlignment;->vertical:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getHeight()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setSize(I)V

    .line 1845
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v2, v2, Landroid/support/v17/leanback/widget/WindowAlignment;->horizontal:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingRight()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setPadding(II)V

    .line 1846
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v2, v2, Landroid/support/v17/leanback/widget/WindowAlignment;->vertical:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingTop()I

    move-result v3

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setPadding(II)V

    .line 1847
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getSize()I

    move-result v2

    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mSizePrimary:I

    .line 1853
    return-void

    .line 1837
    .end local v0    # "paddingPrimaryDiff":I
    .end local v1    # "paddingSecondaryDiff":I
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingTop()I

    move-result v2

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v3, v3, Landroid/support/v17/leanback/widget/WindowAlignment;->vertical:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v3

    sub-int v0, v2, v3

    .line 1838
    .restart local v0    # "paddingPrimaryDiff":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    iget-object v3, v3, Landroid/support/v17/leanback/widget/WindowAlignment;->horizontal:Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v3

    sub-int v1, v2, v3

    .restart local v1    # "paddingSecondaryDiff":I
    goto :goto_0
.end method

.method private layoutChild(ILandroid/view/View;III)V
    .locals 9
    .param p1, "rowIndex"    # I
    .param p2, "v"    # Landroid/view/View;
    .param p3, "start"    # I
    .param p4, "end"    # I
    .param p5, "startSecondary"    # I

    .prologue
    const/4 v1, 0x1

    .line 1163
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_3

    invoke-virtual {p2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    .line 1165
    .local v7, "sizeSecondary":I
    :goto_0
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    if-lez v0, :cond_0

    .line 1166
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    invoke-static {v7, v0}, Ljava/lang/Math;->min(II)I

    move-result v7

    .line 1168
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGravity:I

    and-int/lit8 v8, v0, 0x70

    .line 1169
    .local v8, "verticalGravity":I
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGravity:I

    and-int/lit8 v6, v0, 0x7

    .line 1170
    .local v6, "horizontalGravity":I
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_1

    const/16 v0, 0x30

    if-eq v8, v0, :cond_2

    :cond_1
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-ne v0, v1, :cond_4

    const/4 v0, 0x3

    if-ne v6, v0, :cond_4

    .line 1181
    :cond_2
    :goto_1
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_a

    .line 1182
    move v2, p3

    .line 1183
    .local v2, "left":I
    move v3, p5

    .line 1184
    .local v3, "top":I
    move v4, p4

    .line 1185
    .local v4, "right":I
    add-int v5, p5, v7

    .line 1192
    .local v5, "bottom":I
    :goto_2
    invoke-virtual {p2, v2, v3, v4, v5}, Landroid/view/View;->layout(IIII)V

    move-object v0, p0

    move-object v1, p2

    .line 1193
    invoke-direct/range {v0 .. v5}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildOpticalInsets(Landroid/view/View;IIII)V

    .line 1194
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments(Landroid/view/View;)V

    .line 1195
    return-void

    .line 1163
    .end local v2    # "left":I
    .end local v3    # "top":I
    .end local v4    # "right":I
    .end local v5    # "bottom":I
    .end local v6    # "horizontalGravity":I
    .end local v7    # "sizeSecondary":I
    .end local v8    # "verticalGravity":I
    :cond_3
    invoke-virtual {p2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v7

    goto :goto_0

    .line 1173
    .restart local v6    # "horizontalGravity":I
    .restart local v7    # "sizeSecondary":I
    .restart local v8    # "verticalGravity":I
    :cond_4
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_5

    const/16 v0, 0x50

    if-eq v8, v0, :cond_6

    :cond_5
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-ne v0, v1, :cond_7

    const/4 v0, 0x5

    if-ne v6, v0, :cond_7

    .line 1175
    :cond_6
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowSizeSecondary(I)I

    move-result v0

    sub-int/2addr v0, v7

    add-int/2addr p5, v0

    goto :goto_1

    .line 1176
    :cond_7
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_8

    const/16 v0, 0x10

    if-eq v8, v0, :cond_9

    :cond_8
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-ne v0, v1, :cond_2

    if-ne v6, v1, :cond_2

    .line 1178
    :cond_9
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowSizeSecondary(I)I

    move-result v0

    sub-int/2addr v0, v7

    div-int/lit8 v0, v0, 0x2

    add-int/2addr p5, v0

    goto :goto_1

    .line 1187
    :cond_a
    move v3, p3

    .line 1188
    .restart local v3    # "top":I
    move v2, p5

    .line 1189
    .restart local v2    # "left":I
    move v5, p4

    .line 1190
    .restart local v5    # "bottom":I
    add-int v4, p5, v7

    .restart local v4    # "right":I
    goto :goto_2
.end method

.method private leaveContext()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 633
    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    .line 634
    iput-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    .line 635
    return-void
.end method

.method private measureChild(Landroid/view/View;)V
    .locals 7
    .param p1, "child"    # Landroid/view/View;

    .prologue
    const/4 v6, 0x0

    .line 1020
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 1021
    .local v1, "lp":Landroid/view/ViewGroup$LayoutParams;
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    const/4 v5, -0x2

    if-ne v4, v5, :cond_0

    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 1026
    .local v2, "secondarySpec":I
    :goto_0
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v4, :cond_1

    .line 1027
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v4, v6, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v3

    .line 1030
    .local v3, "widthSpec":I
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v2, v6, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 1038
    .local v0, "heightSpec":I
    :goto_1
    invoke-virtual {p1, v3, v0}, Landroid/view/View;->measure(II)V

    .line 1046
    return-void

    .line 1021
    .end local v0    # "heightSpec":I
    .end local v2    # "secondarySpec":I
    .end local v3    # "widthSpec":I
    :cond_0
    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    const/high16 v5, 0x40000000    # 2.0f

    invoke-static {v4, v5}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    goto :goto_0

    .line 1032
    .restart local v2    # "secondarySpec":I
    :cond_1
    invoke-static {v6, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    iget v5, v1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-static {v4, v6, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 1035
    .restart local v0    # "heightSpec":I
    iget v4, v1, Landroid/view/ViewGroup$LayoutParams;->width:I

    invoke-static {v2, v6, v4}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v3

    .restart local v3    # "widthSpec":I
    goto :goto_1
.end method

.method private measureScrapChild(III[I)V
    .locals 6
    .param p1, "position"    # I
    .param p2, "widthSpec"    # I
    .param p3, "heightSpec"    # I
    .param p4, "measuredDimension"    # [I

    .prologue
    .line 789
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {v4, p1}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v3

    .line 790
    .local v3, "view":Landroid/view/View;
    if-eqz v3, :cond_0

    .line 791
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 792
    .local v2, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingRight()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v2, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->width:I

    invoke-static {p2, v4, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v1

    .line 794
    .local v1, "childWidthSpec":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingBottom()I

    move-result v5

    add-int/2addr v4, v5

    iget v5, v2, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->height:I

    invoke-static {p3, v4, v5}, Landroid/view/ViewGroup;->getChildMeasureSpec(III)I

    move-result v0

    .line 796
    .local v0, "childHeightSpec":I
    invoke-virtual {v3, v1, v0}, Landroid/view/View;->measure(II)V

    .line 797
    const/4 v4, 0x0

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredWidth()I

    move-result v5

    aput v5, p4, v4

    .line 798
    const/4 v4, 0x1

    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    aput v5, p4, v4

    .line 799
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {v4, v3}, Landroid/support/v7/widget/RecyclerView$Recycler;->recycleView(Landroid/view/View;)V

    .line 801
    .end local v0    # "childHeightSpec":I
    .end local v1    # "childWidthSpec":I
    .end local v2    # "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    :cond_0
    return-void
.end method

.method private needsAppendVisibleItem()Z
    .locals 5

    .prologue
    const/4 v2, 0x1

    .line 1216
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-ge v3, v4, :cond_1

    .line 1229
    :cond_0
    :goto_0
    return v2

    .line 1219
    :cond_1
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mSizePrimary:I

    add-int v1, v3, v4

    .line 1220
    .local v1, "right":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v0, v3, :cond_4

    .line 1221
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v4, v4, v0

    iget v4, v4, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-ne v3, v4, :cond_3

    .line 1222
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-lt v3, v1, :cond_0

    .line 1220
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1225
    :cond_3
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    sub-int v4, v1, v4

    if-ge v3, v4, :cond_2

    goto :goto_0

    .line 1229
    :cond_4
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private needsPrependVisibleItem()Z
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 1233
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-le v2, v3, :cond_1

    .line 1245
    :cond_0
    :goto_0
    return v1

    .line 1236
    :cond_1
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v0, v2, :cond_4

    .line 1237
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v3, v3, v0

    iget v3, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-ne v2, v3, :cond_3

    .line 1238
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    if-gt v2, v3, :cond_0

    .line 1236
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 1241
    :cond_3
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v2, v2, v0

    iget v2, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    sub-int/2addr v2, v3

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    if-le v2, v3, :cond_2

    goto :goto_0

    .line 1245
    :cond_4
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private offsetChildrenPrimary(I)V
    .locals 4
    .param p1, "increment"    # I

    .prologue
    .line 1624
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .line 1625
    .local v0, "childCount":I
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    const/4 v3, 0x1

    if-ne v2, v3, :cond_0

    .line 1626
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1627
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1626
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1630
    .end local v1    # "i":I
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 1631
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1630
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1634
    :cond_1
    return-void
.end method

.method private offsetChildrenSecondary(I)V
    .locals 3
    .param p1, "increment"    # I

    .prologue
    .line 1611
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .line 1612
    .local v0, "childCount":I
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v2, :cond_0

    .line 1613
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 1614
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1613
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1617
    .end local v1    # "i":I
    :cond_0
    const/4 v1, 0x0

    .restart local v1    # "i":I
    :goto_1
    if-ge v1, v0, :cond_1

    .line 1618
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, p1}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1617
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 1621
    :cond_1
    return-void
.end method

.method private prependOneVisibleItem()Z
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 1282
    :cond_0
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-lez v3, :cond_2

    .line 1283
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getFirstIndex()I

    move-result v4

    if-le v3, v4, :cond_1

    .line 1285
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    add-int/lit8 v0, v3, -0x1

    .line 1286
    .local v0, "index":I
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v3, v0}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v3

    iget v1, v3, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 1287
    .local v1, "row":I
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    invoke-interface {v3, v0, v1, v2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->createItem(IIZ)V

    .line 1288
    if-nez v1, :cond_0

    .line 1296
    .end local v0    # "index":I
    .end local v1    # "row":I
    :goto_0
    return v2

    .line 1292
    :cond_1
    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/StaggeredGrid;->prependItems(I)V

    goto :goto_0

    .line 1296
    :cond_2
    const/4 v2, 0x1

    goto :goto_0
.end method

.method private prependVisibleItems()V
    .locals 1

    .prologue
    .line 1302
    :cond_0
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->needsPrependVisibleItem()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 1303
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependOneVisibleItem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1307
    :cond_1
    return-void
.end method

.method private processRowSizeSecondary(Z)Z
    .locals 15
    .param p1, "measure"    # Z

    .prologue
    .line 804
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    if-eqz v12, :cond_1

    .line 805
    const/4 v0, 0x0

    .line 874
    :cond_0
    return v0

    .line 808
    :cond_1
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    if-nez v12, :cond_3

    const/4 v7, 0x0

    .line 810
    .local v7, "rows":[Ljava/util/List;, "[Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    const/4 v0, 0x0

    .line 811
    .local v0, "changed":Z
    const/4 v9, -0x1

    .line 812
    .local v9, "scrapChildWidth":I
    const/4 v8, -0x1

    .line 814
    .local v8, "scrapChildHeight":I
    const/4 v4, 0x0

    .local v4, "rowIndex":I
    :goto_1
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v4, v12, :cond_0

    .line 815
    if-nez v7, :cond_4

    const/4 v5, 0x0

    .line 819
    .local v5, "rowItemCount":I
    :goto_2
    const/4 v6, -0x1

    .line 820
    .local v6, "rowSize":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, v5, :cond_8

    .line 821
    aget-object v12, v7, v4

    invoke-interface {v12, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v12

    check-cast v12, Ljava/lang/Integer;

    invoke-virtual {v12}, Ljava/lang/Integer;->intValue()I

    move-result v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v11

    .line 822
    .local v11, "view":Landroid/view/View;
    if-nez v11, :cond_5

    .line 820
    :cond_2
    :goto_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 808
    .end local v0    # "changed":Z
    .end local v1    # "i":I
    .end local v4    # "rowIndex":I
    .end local v5    # "rowItemCount":I
    .end local v6    # "rowSize":I
    .end local v7    # "rows":[Ljava/util/List;, "[Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v8    # "scrapChildHeight":I
    .end local v9    # "scrapChildWidth":I
    .end local v11    # "view":Landroid/view/View;
    :cond_3
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    iget v13, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    iget v14, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    invoke-virtual {v12, v13, v14}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getItemPositionsInRows(II)[Ljava/util/List;

    move-result-object v7

    goto :goto_0

    .line 815
    .restart local v0    # "changed":Z
    .restart local v4    # "rowIndex":I
    .restart local v7    # "rows":[Ljava/util/List;, "[Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v8    # "scrapChildHeight":I
    .restart local v9    # "scrapChildWidth":I
    :cond_4
    aget-object v12, v7, v4

    invoke-interface {v12}, Ljava/util/List;->size()I

    move-result v5

    goto :goto_2

    .line 825
    .restart local v1    # "i":I
    .restart local v5    # "rowItemCount":I
    .restart local v6    # "rowSize":I
    .restart local v11    # "view":Landroid/view/View;
    :cond_5
    if-eqz p1, :cond_6

    invoke-virtual {v11}, Landroid/view/View;->isLayoutRequested()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 826
    invoke-direct {p0, v11}, Landroid/support/v17/leanback/widget/GridLayoutManager;->measureChild(Landroid/view/View;)V

    .line 828
    :cond_6
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v12, :cond_7

    invoke-virtual {v11}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    .line 830
    .local v10, "secondarySize":I
    :goto_5
    if-le v10, v6, :cond_2

    .line 831
    move v6, v10

    goto :goto_4

    .line 828
    .end local v10    # "secondarySize":I
    :cond_7
    invoke-virtual {v11}, Landroid/view/View;->getMeasuredWidth()I

    move-result v10

    goto :goto_5

    .line 835
    .end local v11    # "view":Landroid/view/View;
    :cond_8
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v12}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v2

    .line 836
    .local v2, "itemCount":I
    if-eqz p1, :cond_a

    if-gez v6, :cond_a

    if-lez v2, :cond_a

    .line 837
    if-gez v9, :cond_9

    if-gez v8, :cond_9

    .line 839
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v13, -0x1

    if-ne v12, v13, :cond_d

    .line 840
    const/4 v3, 0x0

    .line 846
    .local v3, "position":I
    :goto_6
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v12

    const/4 v13, 0x0

    const/4 v14, 0x0

    invoke-static {v13, v14}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v13

    iget-object v14, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMeasuredDimension:[I

    invoke-direct {p0, v3, v12, v13, v14}, Landroid/support/v17/leanback/widget/GridLayoutManager;->measureScrapChild(III[I)V

    .line 850
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMeasuredDimension:[I

    const/4 v13, 0x0

    aget v9, v12, v13

    .line 851
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMeasuredDimension:[I

    const/4 v13, 0x1

    aget v8, v12, v13

    .line 855
    .end local v3    # "position":I
    :cond_9
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v12, :cond_f

    move v6, v8

    .line 858
    :cond_a
    :goto_7
    if-gez v6, :cond_b

    .line 859
    const/4 v6, 0x0

    .line 865
    :cond_b
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    aget v12, v12, v4

    if-eq v12, v6, :cond_c

    .line 869
    iget-object v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    aput v6, v12, v4

    .line 870
    const/4 v0, 0x1

    .line 814
    :cond_c
    add-int/lit8 v4, v4, 0x1

    goto/16 :goto_1

    .line 841
    :cond_d
    iget v12, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-lt v12, v2, :cond_e

    .line 842
    add-int/lit8 v3, v2, -0x1

    .restart local v3    # "position":I
    goto :goto_6

    .line 844
    .end local v3    # "position":I
    :cond_e
    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .restart local v3    # "position":I
    goto :goto_6

    .end local v3    # "position":I
    :cond_f
    move v6, v9

    .line 855
    goto :goto_7
.end method

.method private removeChildAt(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 1310
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v0

    .line 1311
    .local v0, "v":Landroid/view/View;
    if-eqz v0, :cond_0

    .line 1315
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v1, v0, p1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->saveOffscreenView(Landroid/view/View;I)V

    .line 1316
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {p0, v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeAndRecycleView(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 1318
    :cond_0
    return-void
.end method

.method private removeInvisibleViewsAtEnd()V
    .locals 4

    .prologue
    .line 1321
    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    if-nez v2, :cond_1

    .line 1338
    :cond_0
    :goto_0
    return-void

    .line 1324
    :cond_1
    const/4 v0, 0x0

    .line 1325
    .local v0, "update":Z
    :goto_1
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-le v2, v3, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-le v2, v3, :cond_2

    .line 1326
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 1327
    .local v1, "view":Landroid/view/View;
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v2

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mSizePrimary:I

    if-le v2, v3, :cond_2

    .line 1328
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeChildAt(I)V

    .line 1329
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    add-int/lit8 v2, v2, -0x1

    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    .line 1330
    const/4 v0, 0x1

    .line 1334
    goto :goto_1

    .line 1335
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    if-eqz v0, :cond_0

    .line 1336
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowsMinMax()V

    goto :goto_0
.end method

.method private removeInvisibleViewsAtFront()V
    .locals 4

    .prologue
    .line 1341
    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    if-nez v2, :cond_1

    .line 1358
    :cond_0
    :goto_0
    return-void

    .line 1344
    :cond_1
    const/4 v0, 0x0

    .line 1345
    .local v0, "update":Z
    :goto_1
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-le v2, v3, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-ge v2, v3, :cond_2

    .line 1346
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 1347
    .local v1, "view":Landroid/view/View;
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v2

    if-gez v2, :cond_2

    .line 1348
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeChildAt(I)V

    .line 1349
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    add-int/lit8 v2, v2, 0x1

    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    .line 1350
    const/4 v0, 0x1

    .line 1354
    goto :goto_1

    .line 1355
    .end local v1    # "view":Landroid/view/View;
    :cond_2
    if-eqz v0, :cond_0

    .line 1356
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowsMinMax()V

    goto :goto_0
.end method

.method private saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 2
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 622
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    if-eqz v0, :cond_1

    .line 623
    :cond_0
    const-string v0, "GridLayoutManager"

    const-string v1, "Recycler information was not released, bug!"

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 625
    :cond_1
    iput-object p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    .line 626
    iput-object p2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    .line 627
    return-void
.end method

.method private scrollDirectionPrimary(I)I
    .locals 7
    .param p1, "da"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 1672
    if-lez p1, :cond_2

    .line 1673
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMaxUnknown()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1674
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMaxScroll()I

    move-result v1

    .line 1675
    .local v1, "maxScroll":I
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int/2addr v6, p1

    if-le v6, v1, :cond_0

    .line 1676
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int p1, v1, v6

    .line 1687
    .end local v1    # "maxScroll":I
    :cond_0
    :goto_0
    if-nez p1, :cond_3

    move p1, v5

    .line 1719
    .end local p1    # "da":I
    :cond_1
    :goto_1
    return p1

    .line 1679
    .restart local p1    # "da":I
    :cond_2
    if-gez p1, :cond_0

    .line 1680
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMinUnknown()Z

    move-result v6

    if-nez v6, :cond_0

    .line 1681
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v6

    invoke-virtual {v6}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMinScroll()I

    move-result v2

    .line 1682
    .local v2, "minScroll":I
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int/2addr v6, p1

    if-ge v6, v2, :cond_0

    .line 1683
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int p1, v2, v6

    goto :goto_0

    .line 1690
    .end local v2    # "minScroll":I
    :cond_3
    neg-int v6, p1

    invoke-direct {p0, v6}, Landroid/support/v17/leanback/widget/GridLayoutManager;->offsetChildrenPrimary(I)V

    .line 1691
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int/2addr v6, p1

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    .line 1692
    iget-boolean v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    if-nez v6, :cond_1

    .line 1696
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .line 1699
    .local v0, "childCount":I
    if-lez p1, :cond_7

    .line 1700
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendVisibleItems()V

    .line 1704
    :cond_4
    :goto_2
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v6

    if-le v6, v0, :cond_8

    move v3, v4

    .line 1705
    .local v3, "updated":Z
    :goto_3
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .line 1707
    if-lez p1, :cond_9

    .line 1708
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtFront()V

    .line 1712
    :cond_5
    :goto_4
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v6

    if-ge v6, v0, :cond_a

    :goto_5
    or-int/2addr v3, v4

    .line 1714
    if-eqz v3, :cond_6

    .line 1715
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowSecondarySizeRefresh()V

    .line 1718
    :cond_6
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/BaseGridView;->invalidate()V

    goto :goto_1

    .line 1701
    .end local v3    # "updated":Z
    :cond_7
    if-gez p1, :cond_4

    .line 1702
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependVisibleItems()V

    goto :goto_2

    :cond_8
    move v3, v5

    .line 1704
    goto :goto_3

    .line 1709
    .restart local v3    # "updated":Z
    :cond_9
    if-gez p1, :cond_5

    .line 1710
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtEnd()V

    goto :goto_4

    :cond_a
    move v4, v5

    .line 1712
    goto :goto_5
.end method

.method private scrollDirectionSecondary(I)I
    .locals 1
    .param p1, "dy"    # I

    .prologue
    .line 1724
    if-nez p1, :cond_0

    .line 1725
    const/4 p1, 0x0

    .line 1730
    .end local p1    # "dy":I
    :goto_0
    return p1

    .line 1727
    .restart local p1    # "dy":I
    :cond_0
    neg-int v0, p1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->offsetChildrenSecondary(I)V

    .line 1728
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    add-int/2addr v0, p1

    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    .line 1729
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/BaseGridView;->invalidate()V

    goto :goto_0
.end method

.method private scrollGrid(IIZ)V
    .locals 3
    .param p1, "scrollPrimary"    # I
    .param p2, "scrollSecondary"    # I
    .param p3, "smooth"    # Z

    .prologue
    .line 2219
    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    if-eqz v2, :cond_0

    .line 2220
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionPrimary(I)I

    .line 2221
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionSecondary(I)I

    .line 2238
    :goto_0
    return-void

    .line 2225
    :cond_0
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v2, :cond_1

    .line 2226
    move v0, p1

    .line 2227
    .local v0, "scrollX":I
    move v1, p2

    .line 2232
    .local v1, "scrollY":I
    :goto_1
    if-eqz p3, :cond_2

    .line 2233
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v2, v0, v1}, Landroid/support/v17/leanback/widget/BaseGridView;->smoothScrollBy(II)V

    goto :goto_0

    .line 2229
    .end local v0    # "scrollX":I
    .end local v1    # "scrollY":I
    :cond_1
    move v0, p2

    .line 2230
    .restart local v0    # "scrollX":I
    move v1, p1

    .restart local v1    # "scrollY":I
    goto :goto_1

    .line 2235
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v2, v0, v1}, Landroid/support/v17/leanback/widget/BaseGridView;->scrollBy(II)V

    goto :goto_0
.end method

.method private scrollToSelection(Landroid/support/v7/widget/RecyclerView;IZ)V
    .locals 4
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I
    .param p3, "smooth"    # Z

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 1874
    invoke-virtual {p0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 1875
    .local v1, "view":Landroid/view/View;
    if-eqz v1, :cond_1

    .line 1876
    iput-boolean v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInSelection:Z

    .line 1877
    invoke-direct {p0, v1, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToView(Landroid/view/View;Z)V

    .line 1878
    iput-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInSelection:Z

    .line 1935
    :cond_0
    :goto_0
    return-void

    .line 1880
    :cond_1
    iput p2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 1881
    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1882
    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    if-eqz v2, :cond_0

    .line 1885
    if-eqz p3, :cond_3

    .line 1886
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->hasDoneFirstLayout()Z

    move-result v2

    if-nez v2, :cond_2

    .line 1887
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getTag()Ljava/lang/String;

    move-result-object v2

    const-string v3, "setSelectionSmooth should not be called before first layout pass"

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 1891
    :cond_2
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;

    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager$3;-><init>(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/content/Context;)V

    .line 1928
    .local v0, "linearSmoothScroller":Landroid/support/v7/widget/LinearSmoothScroller;
    invoke-virtual {v0, p2}, Landroid/support/v7/widget/LinearSmoothScroller;->setTargetPosition(I)V

    .line 1929
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->startSmoothScroll(Landroid/support/v7/widget/RecyclerView$SmoothScroller;)V

    goto :goto_0

    .line 1931
    .end local v0    # "linearSmoothScroller":Landroid/support/v7/widget/LinearSmoothScroller;
    :cond_3
    iput-boolean v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    .line 1932
    invoke-virtual {p1}, Landroid/support/v7/widget/RecyclerView;->requestLayout()V

    goto :goto_0
.end method

.method private scrollToView(Landroid/view/View;Z)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "smooth"    # Z

    .prologue
    const/4 v2, 0x0

    .line 2087
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v0

    .line 2088
    .local v0, "newFocusPosition":I
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-eq v0, v1, :cond_0

    .line 2089
    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 2090
    iput v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 2091
    iget-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    if-nez v1, :cond_0

    .line 2092
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->dispatchChildSelected()V

    .line 2095
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/BaseGridView;->isChildrenDrawingOrderEnabledInternal()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 2096
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/BaseGridView;->invalidate()V

    .line 2098
    :cond_1
    if-nez p1, :cond_3

    .line 2112
    :cond_2
    :goto_0
    return-void

    .line 2101
    :cond_3
    invoke-virtual {p1}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-nez v1, :cond_4

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/BaseGridView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 2104
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 2106
    :cond_4
    iget-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    if-eqz v1, :cond_2

    .line 2109
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I

    invoke-direct {p0, p1, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getScrollPosition(Landroid/view/View;[I)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 2110
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I

    aget v1, v1, v2

    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I

    const/4 v3, 0x1

    aget v2, v2, v3

    invoke-direct {p0, v1, v2, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollGrid(IIZ)V

    goto :goto_0
.end method

.method private updateChildAlignments()V
    .locals 3

    .prologue
    .line 1210
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .local v0, "c":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 1211
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments(Landroid/view/View;)V

    .line 1210
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1213
    :cond_0
    return-void
.end method

.method private updateChildAlignments(Landroid/view/View;)V
    .locals 2
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 1204
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 1205
    .local v0, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/ItemAlignment;->horizontal:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->getAlignmentPosition(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->setAlignX(I)V

    .line 1206
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/ItemAlignment;->vertical:Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->getAlignmentPosition(Landroid/view/View;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->setAlignY(I)V

    .line 1207
    return-void
.end method

.method private updateChildOpticalInsets(Landroid/view/View;IIII)V
    .locals 5
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 1198
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 1199
    .local v0, "p":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    invoke-virtual {p1}, Landroid/view/View;->getLeft()I

    move-result v1

    sub-int v1, p2, v1

    invoke-virtual {p1}, Landroid/view/View;->getTop()I

    move-result v2

    sub-int v2, p3, v2

    invoke-virtual {p1}, Landroid/view/View;->getRight()I

    move-result v3

    sub-int/2addr v3, p4

    invoke-virtual {p1}, Landroid/view/View;->getBottom()I

    move-result v4

    sub-int/2addr v4, p5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->setOpticalInsets(IIII)V

    .line 1201
    return-void
.end method

.method private updateRowSecondarySizeRefresh()V
    .locals 1

    .prologue
    .line 881
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->processRowSizeSecondary(Z)Z

    move-result v0

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSecondarySizeRefresh:Z

    .line 882
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSecondarySizeRefresh:Z

    if-eqz v0, :cond_0

    .line 884
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->forceRequestLayout()V

    .line 886
    :cond_0
    return-void
.end method

.method private updateRowsMinMax()V
    .locals 7

    .prologue
    .line 1361
    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-gez v5, :cond_1

    .line 1380
    :cond_0
    return-void

    .line 1364
    :cond_1
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v1, v5, :cond_2

    .line 1365
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v1

    const v6, 0x7fffffff

    iput v6, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 1366
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v1

    const/high16 v6, -0x80000000

    iput v6, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 1364
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1368
    :cond_2
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    :goto_1
    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-gt v1, v5, :cond_0

    .line 1369
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v4

    .line 1370
    .local v4, "view":Landroid/view/View;
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v5, v1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v5

    iget v3, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 1371
    .local v3, "row":I
    invoke-direct {p0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v5

    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int v2, v5, v6

    .line 1372
    .local v2, "low":I
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iget v5, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ge v2, v5, :cond_3

    .line 1373
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iput v2, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 1375
    :cond_3
    invoke-direct {p0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v5

    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    add-int v0, v5, v6

    .line 1376
    .local v0, "high":I
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iget v5, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-le v0, v5, :cond_4

    .line 1377
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iput v0, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 1368
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_1
.end method

.method private updateScrollMax()V
    .locals 10

    .prologue
    .line 1734
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-gez v8, :cond_1

    .line 1775
    :cond_0
    :goto_0
    return-void

    .line 1737
    :cond_1
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    iget-object v9, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    invoke-virtual {v9}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v9

    add-int/lit8 v9, v9, -0x1

    if-ne v8, v9, :cond_4

    const/4 v1, 0x1

    .line 1738
    .local v1, "lastAvailable":Z
    :goto_1
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMaxUnknown()Z

    move-result v5

    .line 1739
    .local v5, "maxUnknown":Z
    if-nez v1, :cond_2

    if-nez v5, :cond_0

    .line 1742
    :cond_2
    const/high16 v3, -0x80000000

    .line 1743
    .local v3, "maxEdge":I
    const/4 v6, -0x1

    .line 1744
    .local v6, "rowIndex":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_2
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    array-length v8, v8

    if-ge v0, v8, :cond_5

    .line 1745
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v0

    iget v8, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-le v8, v3, :cond_3

    .line 1746
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v0

    iget v3, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 1747
    move v6, v0

    .line 1744
    :cond_3
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 1737
    .end local v0    # "i":I
    .end local v1    # "lastAvailable":Z
    .end local v3    # "maxEdge":I
    .end local v5    # "maxUnknown":Z
    .end local v6    # "rowIndex":I
    :cond_4
    const/4 v1, 0x0

    goto :goto_1

    .line 1750
    .restart local v0    # "i":I
    .restart local v1    # "lastAvailable":Z
    .restart local v3    # "maxEdge":I
    .restart local v5    # "maxUnknown":Z
    .restart local v6    # "rowIndex":I
    :cond_5
    const v4, 0x7fffffff

    .line 1751
    .local v4, "maxScroll":I
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    :goto_3
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-lt v0, v8, :cond_6

    .line 1752
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v2

    .line 1753
    .local v2, "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    if-eqz v2, :cond_7

    iget v8, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    if-ne v8, v6, :cond_7

    .line 1754
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMaxEdge()I

    move-result v7

    .line 1755
    .local v7, "savedMaxEdge":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMaxEdge(I)V

    .line 1756
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v8

    invoke-direct {p0, v8}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPrimarySystemScrollPosition(Landroid/view/View;)I

    move-result v4

    .line 1757
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMaxEdge(I)V

    .line 1761
    .end local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v7    # "savedMaxEdge":I
    :cond_6
    if-eqz v1, :cond_8

    .line 1762
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMaxEdge(I)V

    .line 1763
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMaxScroll(I)V

    goto/16 :goto_0

    .line 1751
    .restart local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    :cond_7
    add-int/lit8 v0, v0, -0x1

    goto :goto_3

    .line 1769
    .end local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    :cond_8
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMaxScroll()I

    move-result v8

    if-le v4, v8, :cond_0

    .line 1770
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->invalidateScrollMax()V

    goto/16 :goto_0
.end method

.method private updateScrollMin()V
    .locals 9

    .prologue
    .line 1778
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-gez v8, :cond_1

    .line 1819
    :cond_0
    :goto_0
    return-void

    .line 1781
    :cond_1
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-nez v8, :cond_4

    const/4 v0, 0x1

    .line 1782
    .local v0, "firstAvailable":Z
    :goto_1
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->isMinUnknown()Z

    move-result v5

    .line 1783
    .local v5, "minUnknown":Z
    if-nez v0, :cond_2

    if-nez v5, :cond_0

    .line 1786
    :cond_2
    const v3, 0x7fffffff

    .line 1787
    .local v3, "minEdge":I
    const/4 v6, -0x1

    .line 1788
    .local v6, "rowIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_2
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    array-length v8, v8

    if-ge v1, v8, :cond_5

    .line 1789
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v1

    iget v8, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ge v8, v3, :cond_3

    .line 1790
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v8, v8, v1

    iget v3, v8, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 1791
    move v6, v1

    .line 1788
    :cond_3
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1781
    .end local v0    # "firstAvailable":Z
    .end local v1    # "i":I
    .end local v3    # "minEdge":I
    .end local v5    # "minUnknown":Z
    .end local v6    # "rowIndex":I
    :cond_4
    const/4 v0, 0x0

    goto :goto_1

    .line 1794
    .restart local v0    # "firstAvailable":Z
    .restart local v1    # "i":I
    .restart local v3    # "minEdge":I
    .restart local v5    # "minUnknown":Z
    .restart local v6    # "rowIndex":I
    :cond_5
    const/high16 v4, -0x80000000

    .line 1795
    .local v4, "minScroll":I
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    :goto_3
    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    if-gt v1, v8, :cond_6

    .line 1796
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v8, v1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v2

    .line 1797
    .local v2, "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    if-eqz v2, :cond_7

    iget v8, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    if-ne v8, v6, :cond_7

    .line 1798
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMinEdge()I

    move-result v7

    .line 1799
    .local v7, "savedMinEdge":I
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMinEdge(I)V

    .line 1800
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v8

    invoke-direct {p0, v8}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPrimarySystemScrollPosition(Landroid/view/View;)I

    move-result v4

    .line 1801
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v7}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMinEdge(I)V

    .line 1805
    .end local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v7    # "savedMinEdge":I
    :cond_6
    if-eqz v0, :cond_8

    .line 1806
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v3}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMinEdge(I)V

    .line 1807
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8, v4}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMinScroll(I)V

    goto :goto_0

    .line 1795
    .restart local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    :cond_7
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 1813
    .end local v2    # "location":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    :cond_8
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getMinScroll()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 1814
    iget-object v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v8

    invoke-virtual {v8}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->invalidateScrollMin()V

    goto/16 :goto_0
.end method

.method private updateScrollSecondAxis()V
    .locals 2

    .prologue
    .line 1822
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->secondAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMinEdge(I)V

    .line 1823
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->secondAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSizeSecondary()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setMaxEdge(I)V

    .line 1824
    return-void
.end method


# virtual methods
.method public canScrollHorizontally()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 538
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public canScrollVertically()Z
    .locals 2

    .prologue
    const/4 v0, 0x1

    .line 545
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-eq v1, v0, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-le v1, v0, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected fastRelayout(Z)V
    .locals 21
    .param p1, "scrollToFocus"    # Z

    .prologue
    .line 1384
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->initScrollController()V

    .line 1386
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    move/from16 v20, v0

    move/from16 v0, v19

    move/from16 v1, v20

    invoke-virtual {v2, v0, v1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getItemPositionsInRows(II)[Ljava/util/List;

    move-result-object v17

    .line 1389
    .local v17, "rows":[Ljava/util/List;, "[Ljava/util/List<Ljava/lang/Integer;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ge v3, v2, :cond_6

    .line 1390
    aget-object v16, v17, v3

    .line 1391
    .local v16, "row":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    move-object/from16 v0, p0

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowStartSecondary(I)I

    move-result v2

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    move/from16 v19, v0

    sub-int v7, v2, v19

    .line 1392
    .local v7, "startSecondary":I
    const/4 v10, 0x0

    .local v10, "j":I
    invoke-interface/range {v16 .. v16}, Ljava/util/List;->size()I

    move-result v18

    .local v18, "size":I
    :goto_1
    move/from16 v0, v18

    if-ge v10, v0, :cond_5

    .line 1393
    move-object/from16 v0, v16

    invoke-interface {v0, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v14

    .line 1394
    .local v14, "position":I
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v4

    .line 1397
    .local v4, "view":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v5

    .line 1398
    .local v5, "start":I
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v2, :cond_2

    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    .line 1402
    .local v13, "oldPrimarySize":I
    :goto_2
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v12

    check-cast v12, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .line 1403
    .local v12, "lp":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    invoke-virtual {v12}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->viewNeedsUpdate()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 1404
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/BaseGridView;->indexOfChild(Landroid/view/View;)I

    move-result v9

    .line 1405
    .local v9, "index":I
    move-object/from16 v0, p0

    iget-object v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->detachAndScrapView(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 1406
    move-object/from16 v0, p0

    invoke-virtual {v0, v14}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewForPosition(I)Landroid/view/View;

    move-result-object v4

    .line 1407
    move-object/from16 v0, p0

    invoke-virtual {v0, v4, v9}, Landroid/support/v17/leanback/widget/GridLayoutManager;->addView(Landroid/view/View;I)V

    .line 1410
    .end local v9    # "index":I
    :cond_0
    invoke-virtual {v4}, Landroid/view/View;->isLayoutRequested()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 1411
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->measureChild(Landroid/view/View;)V

    .line 1414
    :cond_1
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v2, :cond_3

    .line 1415
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    add-int v6, v5, v2

    .line 1416
    .local v6, "end":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredWidth()I

    move-result v2

    sub-int v15, v2, v13

    .line 1417
    .local v15, "primaryDelta":I
    if-eqz v15, :cond_4

    .line 1418
    add-int/lit8 v11, v10, 0x1

    .local v11, "k":I
    :goto_3
    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 1419
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/view/View;->offsetLeftAndRight(I)V

    .line 1418
    add-int/lit8 v11, v11, 0x1

    goto :goto_3

    .line 1398
    .end local v6    # "end":I
    .end local v11    # "k":I
    .end local v12    # "lp":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    .end local v13    # "oldPrimarySize":I
    .end local v15    # "primaryDelta":I
    :cond_2
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v13

    goto :goto_2

    .line 1423
    .restart local v12    # "lp":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    .restart local v13    # "oldPrimarySize":I
    :cond_3
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    add-int v6, v5, v2

    .line 1424
    .restart local v6    # "end":I
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    sub-int v15, v2, v13

    .line 1425
    .restart local v15    # "primaryDelta":I
    if-eqz v15, :cond_4

    .line 1426
    add-int/lit8 v11, v10, 0x1

    .restart local v11    # "k":I
    :goto_4
    move/from16 v0, v18

    if-ge v11, v0, :cond_4

    .line 1427
    move-object/from16 v0, v16

    invoke-interface {v0, v11}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v2

    invoke-virtual {v2, v15}, Landroid/view/View;->offsetTopAndBottom(I)V

    .line 1426
    add-int/lit8 v11, v11, 0x1

    goto :goto_4

    .end local v11    # "k":I
    :cond_4
    move-object/from16 v2, p0

    .line 1431
    invoke-direct/range {v2 .. v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->layoutChild(ILandroid/view/View;III)V

    .line 1392
    add-int/lit8 v10, v10, 0x1

    goto/16 :goto_1

    .line 1389
    .end local v4    # "view":Landroid/view/View;
    .end local v5    # "start":I
    .end local v6    # "end":I
    .end local v12    # "lp":Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;
    .end local v13    # "oldPrimarySize":I
    .end local v14    # "position":I
    .end local v15    # "primaryDelta":I
    :cond_5
    add-int/lit8 v3, v3, 0x1

    goto/16 :goto_0

    .line 1435
    .end local v7    # "startSecondary":I
    .end local v10    # "j":I
    .end local v16    # "row":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v18    # "size":I
    :cond_6
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowsMinMax()V

    .line 1436
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendVisibleItems()V

    .line 1437
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependVisibleItems()V

    .line 1439
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowsMinMax()V

    .line 1440
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMin()V

    .line 1441
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMax()V

    .line 1442
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollSecondAxis()V

    .line 1444
    if-eqz p1, :cond_7

    .line 1445
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/16 v19, -0x1

    move/from16 v0, v19

    if-ne v2, v0, :cond_8

    const/4 v2, 0x0

    :goto_5
    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v8

    .line 1446
    .local v8, "focusView":Landroid/view/View;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v0, v8, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToView(Landroid/view/View;Z)V

    .line 1448
    .end local v8    # "focusView":Landroid/view/View;
    :cond_7
    return-void

    .line 1445
    :cond_8
    move-object/from16 v0, p0

    iget v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    goto :goto_5
.end method

.method public generateDefaultLayoutParams()Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 550
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/content/Context;Landroid/util/AttributeSet;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 556
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v7/widget/RecyclerView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 561
    instance-of v0, p1, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    if-eqz v0, :cond_0

    .line 562
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    check-cast p1, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    .end local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;)V

    .line 568
    :goto_0
    return-object v0

    .line 563
    .restart local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    instance-of v0, p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    if-eqz v0, :cond_1

    .line 564
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    check-cast p1, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    .end local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/support/v7/widget/RecyclerView$LayoutParams;)V

    goto :goto_0

    .line 565
    .restart local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_1
    instance-of v0, p1, Landroid/view/ViewGroup$MarginLayoutParams;

    if-eqz v0, :cond_2

    .line 566
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    check-cast p1, Landroid/view/ViewGroup$MarginLayoutParams;

    .end local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$MarginLayoutParams;)V

    goto :goto_0

    .line 568
    .restart local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_2
    new-instance v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method getChildDrawingOrder(Landroid/support/v7/widget/RecyclerView;II)I
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "childCount"    # I
    .param p3, "i"    # I

    .prologue
    .line 2563
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v1

    .line 2564
    .local v1, "view":Landroid/view/View;
    if-nez v1, :cond_1

    .line 2575
    .end local p3    # "i":I
    :cond_0
    :goto_0
    return p3

    .line 2567
    .restart local p3    # "i":I
    :cond_1
    invoke-virtual {p1, v1}, Landroid/support/v7/widget/RecyclerView;->indexOfChild(Landroid/view/View;)I

    move-result v0

    .line 2570
    .local v0, "focusIndex":I
    if-lt p3, v0, :cond_0

    .line 2572
    add-int/lit8 v2, p2, -0x1

    if-ge p3, v2, :cond_2

    .line 2573
    add-int v2, v0, p2

    add-int/lit8 v2, v2, -0x1

    sub-int p3, v2, p3

    goto :goto_0

    :cond_2
    move p3, v0

    .line 2575
    goto :goto_0
.end method

.method public getFocusScrollStrategy()I
    .locals 1

    .prologue
    .line 372
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    return v0
.end method

.method public getHorizontalMargin()I
    .locals 1

    .prologue
    .line 487
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mHorizontalMargin:I

    return v0
.end method

.method public getItemAlignmentOffset()I
    .locals 1

    .prologue
    .line 409
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->getItemAlignmentOffset()I

    move-result v0

    return v0
.end method

.method public getItemAlignmentOffsetPercent()F
    .locals 1

    .prologue
    .line 427
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->getItemAlignmentOffsetPercent()F

    move-result v0

    return v0
.end method

.method public getItemAlignmentViewId()I
    .locals 1

    .prologue
    .line 436
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->getItemAlignmentViewId()I

    move-result v0

    return v0
.end method

.method final getOpticalBottom(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 589
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalBottom(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method final getOpticalLeft(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 577
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalLeft(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method final getOpticalRight(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 581
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalRight(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method final getOpticalTop(Landroid/view/View;)I
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 585
    invoke-virtual {p1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager$LayoutParams;->getOpticalTop(Landroid/view/View;)I

    move-result v0

    return v0
.end method

.method public getSelection()I
    .locals 1

    .prologue
    .line 1864
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    return v0
.end method

.method public getVerticalMargin()I
    .locals 1

    .prologue
    .line 483
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mVerticalMargin:I

    return v0
.end method

.method protected getViewForPosition(I)Landroid/view/View;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 573
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRecycler:Landroid/support/v7/widget/RecyclerView$Recycler;

    invoke-virtual {v0, p1}, Landroid/support/v7/widget/RecyclerView$Recycler;->getViewForPosition(I)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public getViewSelectedOffsets(Landroid/view/View;[I)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "offsets"    # [I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 2038
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    .line 2039
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPrimarySystemScrollPosition(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int/2addr v0, v1

    aput v0, p2, v2

    .line 2040
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSecondarySystemScrollPosition(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    sub-int/2addr v0, v1

    aput v0, p2, v3

    .line 2045
    :goto_0
    return-void

    .line 2042
    :cond_0
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPrimarySystemScrollPosition(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    sub-int/2addr v0, v1

    aput v0, p2, v3

    .line 2043
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSecondarySystemScrollPosition(Landroid/view/View;)I

    move-result v0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    sub-int/2addr v0, v1

    aput v0, p2, v2

    goto :goto_0
.end method

.method public getWindowAlignment()I
    .locals 1

    .prologue
    .line 384
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getWindowAlignment()I

    move-result v0

    return v0
.end method

.method public getWindowAlignmentOffset()I
    .locals 1

    .prologue
    .line 392
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getWindowAlignmentOffset()I

    move-result v0

    return v0
.end method

.method public getWindowAlignmentOffsetPercent()F
    .locals 1

    .prologue
    .line 400
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getWindowAlignmentOffsetPercent()F

    move-result v0

    return v0
.end method

.method gridOnRequestFocusInDescendants(Landroid/support/v7/widget/RecyclerView;ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "direction"    # I
    .param p3, "previouslyFocusedRect"    # Landroid/graphics/Rect;

    .prologue
    .line 2463
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    packed-switch v0, :pswitch_data_0

    .line 2466
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->gridOnRequestFocusInDescendantsAligned(Landroid/support/v7/widget/RecyclerView;ILandroid/graphics/Rect;)Z

    move-result v0

    .line 2470
    :goto_0
    return v0

    :pswitch_0
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->gridOnRequestFocusInDescendantsUnaligned(Landroid/support/v7/widget/RecyclerView;ILandroid/graphics/Rect;)Z

    move-result v0

    goto :goto_0

    .line 2463
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected hasDoneFirstLayout()Z
    .locals 1

    .prologue
    .line 495
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method hasPreviousViewInSameRow(I)Z
    .locals 7
    .param p1, "pos"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 2295
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    if-eqz v6, :cond_0

    const/4 v6, -0x1

    if-ne p1, v6, :cond_2

    :cond_0
    move v4, v5

    .line 2311
    :cond_1
    :goto_0
    return v4

    .line 2298
    :cond_2
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    if-gtz v6, :cond_1

    .line 2301
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v6, p1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v6

    iget v0, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 2302
    .local v0, "focusedRow":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v6

    add-int/lit8 v1, v6, -0x1

    .local v1, "i":I
    :goto_1
    if-ltz v1, :cond_4

    .line 2303
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByIndex(I)I

    move-result v3

    .line 2304
    .local v3, "position":I
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    invoke-virtual {v6, v3}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v2

    .line 2305
    .local v2, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    if-eqz v2, :cond_3

    iget v6, v2, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    if-ne v6, v0, :cond_3

    .line 2306
    if-lt v3, p1, :cond_1

    .line 2302
    :cond_3
    add-int/lit8 v1, v1, -0x1

    goto :goto_1

    .end local v2    # "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v3    # "position":I
    :cond_4
    move v4, v5

    .line 2311
    goto :goto_0
.end method

.method isFocusSearchDisabled()Z
    .locals 1

    .prologue
    .line 2283
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusSearchDisabled:Z

    return v0
.end method

.method public isItemAlignmentOffsetWithPadding()Z
    .locals 1

    .prologue
    .line 418
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->isItemAlignmentOffsetWithPadding()Z

    move-result v0

    return v0
.end method

.method public isScrollEnabled()Z
    .locals 1

    .prologue
    .line 2264
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    return v0
.end method

.method public onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V
    .locals 1
    .param p1, "oldAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;
    .param p2, "newAdapter"    # Landroid/support/v7/widget/RecyclerView$Adapter;

    .prologue
    .line 2583
    if-eqz p1, :cond_0

    .line 2584
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->discardLayoutInfo()V

    .line 2585
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 2586
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 2587
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->clear()V

    .line 2589
    :cond_0
    invoke-super {p0, p1, p2}, Landroid/support/v7/widget/RecyclerView$LayoutManager;->onAdapterChanged(Landroid/support/v7/widget/RecyclerView$Adapter;Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 2590
    return-void
.end method

.method public onAddFocusables(Landroid/support/v7/widget/RecyclerView;Ljava/util/ArrayList;II)Z
    .locals 18
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "direction"    # I
    .param p4, "focusableMode"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/support/v7/widget/RecyclerView;",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;II)Z"
        }
    .end annotation

    .prologue
    .line 2317
    .local p2, "views":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/view/View;>;"
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusSearchDisabled:Z

    move/from16 v17, v0

    if-eqz v17, :cond_0

    .line 2318
    const/16 v17, 0x1

    .line 2398
    :goto_0
    return v17

    .line 2327
    :cond_0
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/widget/RecyclerView;->hasFocus()Z

    move-result v17

    if-eqz v17, :cond_b

    .line 2328
    move-object/from16 v0, p0

    move/from16 v1, p3

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getMovement(I)I

    move-result v14

    .line 2329
    .local v14, "movement":I
    if-eqz v14, :cond_1

    const/16 v17, 0x1

    move/from16 v0, v17

    if-eq v14, v0, :cond_1

    .line 2331
    const/16 v17, 0x0

    goto :goto_0

    .line 2333
    :cond_1
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/widget/RecyclerView;->findFocus()Landroid/view/View;

    move-result-object v7

    .line 2334
    .local v7, "focused":Landroid/view/View;
    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findImmediateChildIndex(Landroid/view/View;)I

    move-result v17

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByIndex(I)I

    move-result v8

    .line 2336
    .local v8, "focusedPos":I
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_2

    .line 2337
    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v17

    move-object/from16 v0, v17

    move-object/from16 v1, p2

    move/from16 v2, p3

    move/from16 v3, p4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2339
    :cond_2
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    if-eqz v17, :cond_4

    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v8}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v17

    move-object/from16 v0, v17

    iget v9, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 2342
    .local v9, "focusedRow":I
    :goto_1
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    if-eqz v17, :cond_a

    .line 2343
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2344
    .local v6, "focusableCount":I
    const/4 v10, 0x0

    .local v10, "i":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v5

    .local v5, "count":I
    :goto_2
    if-ge v10, v5, :cond_a

    .line 2345
    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v14, v0, :cond_5

    move v11, v10

    .line 2346
    .local v11, "index":I
    :goto_3
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2347
    .local v4, "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-eqz v17, :cond_6

    .line 2344
    :cond_3
    add-int/lit8 v10, v10, 0x1

    goto :goto_2

    .line 2339
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "count":I
    .end local v6    # "focusableCount":I
    .end local v9    # "focusedRow":I
    .end local v10    # "i":I
    .end local v11    # "index":I
    :cond_4
    const/4 v9, -0x1

    goto :goto_1

    .line 2345
    .restart local v5    # "count":I
    .restart local v6    # "focusableCount":I
    .restart local v9    # "focusedRow":I
    .restart local v10    # "i":I
    :cond_5
    add-int/lit8 v17, v5, -0x1

    sub-int v11, v17, v10

    goto :goto_3

    .line 2350
    .restart local v4    # "child":Landroid/view/View;
    .restart local v11    # "index":I
    :cond_6
    move-object/from16 v0, p0

    invoke-direct {v0, v11}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByIndex(I)I

    move-result v15

    .line 2351
    .local v15, "position":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    invoke-virtual {v0, v15}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v13

    .line 2352
    .local v13, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v9, v0, :cond_7

    if-eqz v13, :cond_3

    iget v0, v13, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v9, :cond_3

    .line 2353
    :cond_7
    const/16 v17, -0x1

    move/from16 v0, v17

    if-eq v8, v0, :cond_9

    const/16 v17, 0x1

    move/from16 v0, v17

    if-ne v14, v0, :cond_8

    if-gt v15, v8, :cond_9

    :cond_8
    if-nez v14, :cond_3

    if-ge v15, v8, :cond_3

    .line 2356
    :cond_9
    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v4, v0, v1, v2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2357
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-le v0, v6, :cond_3

    .line 2398
    .end local v4    # "child":Landroid/view/View;
    .end local v5    # "count":I
    .end local v6    # "focusableCount":I
    .end local v7    # "focused":Landroid/view/View;
    .end local v8    # "focusedPos":I
    .end local v9    # "focusedRow":I
    .end local v10    # "i":I
    .end local v11    # "index":I
    .end local v13    # "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    .end local v14    # "movement":I
    .end local v15    # "position":I
    :cond_a
    :goto_4
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 2365
    :cond_b
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    move/from16 v17, v0

    if-eqz v17, :cond_11

    .line 2367
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getPaddingLow()I

    move-result v12

    .line 2368
    .local v12, "left":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getClientSize()I

    move-result v17

    add-int v16, v17, v12

    .line 2369
    .local v16, "right":I
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v6

    .line 2370
    .restart local v6    # "focusableCount":I
    const/4 v10, 0x0

    .restart local v10    # "i":I
    invoke-virtual/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v5

    .restart local v5    # "count":I
    :goto_5
    if-ge v10, v5, :cond_d

    .line 2371
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2372
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_c

    .line 2373
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMin(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    if-lt v0, v12, :cond_c

    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewMax(Landroid/view/View;)I

    move-result v17

    move/from16 v0, v17

    move/from16 v1, v16

    if-gt v0, v1, :cond_c

    .line 2374
    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v4, v0, v1, v2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2370
    :cond_c
    add-int/lit8 v10, v10, 0x1

    goto :goto_5

    .line 2379
    .end local v4    # "child":Landroid/view/View;
    :cond_d
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-ne v0, v6, :cond_10

    .line 2380
    const/4 v10, 0x0

    invoke-virtual/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v5

    :goto_6
    if-ge v10, v5, :cond_f

    .line 2381
    move-object/from16 v0, p0

    invoke-virtual {v0, v10}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2382
    .restart local v4    # "child":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v17

    if-nez v17, :cond_e

    .line 2383
    move-object/from16 v0, p2

    move/from16 v1, p3

    move/from16 v2, p4

    invoke-virtual {v4, v0, v1, v2}, Landroid/view/View;->addFocusables(Ljava/util/ArrayList;II)V

    .line 2380
    :cond_e
    add-int/lit8 v10, v10, 0x1

    goto :goto_6

    .line 2386
    .end local v4    # "child":Landroid/view/View;
    :cond_f
    invoke-virtual/range {p2 .. p2}, Ljava/util/ArrayList;->size()I

    move-result v17

    move/from16 v0, v17

    if-eq v0, v6, :cond_11

    .line 2387
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 2390
    :cond_10
    const/16 v17, 0x1

    goto/16 :goto_0

    .line 2394
    .end local v5    # "count":I
    .end local v6    # "focusableCount":I
    .end local v10    # "i":I
    .end local v12    # "left":I
    .end local v16    # "right":I
    :cond_11
    invoke-virtual/range {p1 .. p1}, Landroid/support/v7/widget/RecyclerView;->isFocusable()Z

    move-result v17

    if-eqz v17, :cond_a

    .line 2395
    move-object/from16 v0, p2

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    goto/16 :goto_4
.end method

.method public onFocusSearchFailed(Landroid/view/View;ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)Landroid/view/View;
    .locals 8
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I
    .param p3, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p4, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x1

    .line 2406
    const/4 v4, 0x0

    .line 2407
    .local v4, "view":Landroid/view/View;
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getMovement(I)I

    move-result v2

    .line 2408
    .local v2, "movement":I
    iget-object v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v7}, Landroid/support/v17/leanback/widget/BaseGridView;->getScrollState()I

    move-result v7

    if-eqz v7, :cond_1

    move v1, v5

    .line 2409
    .local v1, "isScroll":Z
    :goto_0
    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-ne v7, v5, :cond_7

    .line 2412
    if-ne v2, v5, :cond_4

    .line 2413
    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int v3, v5, v6

    .line 2414
    .local v3, "newPos":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getItemCount()I

    move-result v5

    if-ge v3, v5, :cond_2

    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    if-eqz v5, :cond_2

    .line 2415
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {p0, v5, v3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setSelectionSmooth(Landroid/support/v7/widget/RecyclerView;I)V

    .line 2416
    move-object v4, p1

    .line 2458
    .end local v3    # "newPos":I
    :cond_0
    :goto_1
    return-object v4

    .line 2408
    .end local v1    # "isScroll":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 2418
    .restart local v1    # "isScroll":Z
    .restart local v3    # "newPos":I
    :cond_2
    if-nez v1, :cond_3

    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutEnd:Z

    if-nez v5, :cond_0

    .line 2419
    :cond_3
    move-object v4, p1

    goto :goto_1

    .line 2422
    .end local v3    # "newPos":I
    :cond_4
    if-nez v2, :cond_0

    .line 2423
    iget v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    sub-int v3, v5, v6

    .line 2424
    .restart local v3    # "newPos":I
    if-ltz v3, :cond_5

    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    if-eqz v5, :cond_5

    .line 2425
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {p0, v5, v3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setSelectionSmooth(Landroid/support/v7/widget/RecyclerView;I)V

    .line 2426
    move-object v4, p1

    goto :goto_1

    .line 2428
    :cond_5
    if-nez v1, :cond_6

    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutFront:Z

    if-nez v5, :cond_0

    .line 2429
    :cond_6
    move-object v4, p1

    goto :goto_1

    .line 2433
    .end local v3    # "newPos":I
    :cond_7
    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-le v7, v5, :cond_0

    .line 2436
    invoke-direct {p0, p3, p4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 2437
    invoke-static {}, Landroid/view/FocusFinder;->getInstance()Landroid/view/FocusFinder;

    move-result-object v0

    .line 2438
    .local v0, "ff":Landroid/view/FocusFinder;
    if-ne v2, v5, :cond_8

    .line 2439
    :goto_2
    if-nez v4, :cond_9

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendOneVisibleItem()Z

    move-result v7

    if-nez v7, :cond_9

    .line 2440
    iget-object v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v0, v7, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    goto :goto_2

    .line 2442
    :cond_8
    if-nez v2, :cond_9

    .line 2443
    :goto_3
    if-nez v4, :cond_9

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependOneVisibleItem()Z

    move-result v7

    if-nez v7, :cond_9

    .line 2444
    iget-object v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    invoke-virtual {v0, v7, p1, p2}, Landroid/view/FocusFinder;->findNextFocus(Landroid/view/ViewGroup;Landroid/view/View;I)Landroid/view/View;

    move-result-object v4

    goto :goto_3

    .line 2447
    :cond_9
    if-nez v4, :cond_a

    .line 2449
    if-nez v2, :cond_c

    .line 2450
    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutFront:Z

    if-eqz v5, :cond_b

    if-nez v1, :cond_b

    move-object v4, v6

    .line 2455
    :cond_a
    :goto_4
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->leaveContext()V

    goto :goto_1

    :cond_b
    move-object v4, p1

    .line 2450
    goto :goto_4

    .line 2451
    :cond_c
    if-ne v2, v5, :cond_a

    .line 2452
    iget-boolean v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutEnd:Z

    if-eqz v5, :cond_d

    if-nez v1, :cond_d

    move-object v4, v6

    :goto_5
    goto :goto_4

    :cond_d
    move-object v4, p1

    goto :goto_5
.end method

.method public onInterceptFocusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 1
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I

    .prologue
    .line 2288
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusSearchDisabled:Z

    if-eqz v0, :cond_0

    .line 2291
    .end local p1    # "focused":Landroid/view/View;
    :goto_0
    return-object p1

    .restart local p1    # "focused":Landroid/view/View;
    :cond_0
    const/4 p1, 0x0

    goto :goto_0
.end method

.method public onItemsAdded(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 1941
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1943
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    add-int v0, v1, v2

    .line 1944
    .local v0, "pos":I
    if-gt p2, v0, :cond_0

    .line 1945
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    add-int/2addr v1, p3

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1948
    .end local v0    # "pos":I
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->clear()V

    .line 1949
    return-void
.end method

.method public onItemsChanged(Landroid/support/v7/widget/RecyclerView;)V
    .locals 1
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;

    .prologue
    .line 1954
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1955
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->clear()V

    .line 1956
    return-void
.end method

.method public onItemsMoved(Landroid/support/v7/widget/RecyclerView;III)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "fromPosition"    # I
    .param p3, "toPosition"    # I
    .param p4, "itemCount"    # I

    .prologue
    .line 1982
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    const/high16 v2, -0x80000000

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1984
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    add-int v0, v1, v2

    .line 1985
    .local v0, "pos":I
    if-gt p2, v0, :cond_1

    add-int v1, p2, p4

    if-ge v0, v1, :cond_1

    .line 1987
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    sub-int v2, p3, p2

    add-int/2addr v1, v2

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1996
    .end local v0    # "pos":I
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->clear()V

    .line 1997
    return-void

    .line 1988
    .restart local v0    # "pos":I
    :cond_1
    if-ge p2, v0, :cond_2

    sub-int v1, v0, p4

    if-le p3, v1, :cond_2

    .line 1990
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    sub-int/2addr v1, p4

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    goto :goto_0

    .line 1991
    :cond_2
    if-le p2, v0, :cond_0

    if-ge p3, v0, :cond_0

    .line 1993
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    add-int/2addr v1, p4

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    goto :goto_0
.end method

.method public onItemsRemoved(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 4
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    const/high16 v3, -0x80000000

    .line 1962
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    if-eq v1, v3, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 1964
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    add-int v0, v1, v2

    .line 1965
    .local v0, "pos":I
    if-gt p2, v0, :cond_0

    .line 1966
    add-int v1, p2, p3

    if-le v1, v0, :cond_1

    .line 1968
    iput v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1974
    .end local v0    # "pos":I
    :cond_0
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->clear()V

    .line 1975
    return-void

    .line 1970
    .restart local v0    # "pos":I
    :cond_1
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    sub-int/2addr v1, p3

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    goto :goto_0
.end method

.method public onItemsUpdated(Landroid/support/v7/widget/RecyclerView;II)V
    .locals 3
    .param p1, "recyclerView"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "positionStart"    # I
    .param p3, "itemCount"    # I

    .prologue
    .line 2001
    move v1, p2

    .local v1, "i":I
    add-int v0, p2, p3

    .local v0, "end":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2002
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->remove(I)V

    .line 2001
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2004
    :cond_0
    return-void
.end method

.method public onLayoutChildren(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V
    .locals 21
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 1469
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    move/from16 v17, v0

    if-nez v17, :cond_1

    .line 1608
    :cond_0
    :goto_0
    return-void

    .line 1473
    :cond_1
    invoke-virtual/range {p2 .. p2}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v11

    .line 1474
    .local v11, "itemCount":I
    if-ltz v11, :cond_0

    .line 1478
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    move/from16 v17, v0

    if-nez v17, :cond_2

    .line 1479
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->discardLayoutInfo()V

    .line 1480
    invoke-virtual/range {p0 .. p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V

    goto :goto_0

    .line 1483
    :cond_2
    const/16 v17, 0x1

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    .line 1485
    invoke-virtual/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->isSmoothScrolling()Z

    move-result v17

    if-nez v17, :cond_7

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    move/from16 v17, v0

    if-nez v17, :cond_7

    const/16 v16, 0x1

    .line 1487
    .local v16, "scrollToFocus":Z
    :goto_1
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    move/from16 v17, v0

    const/high16 v18, -0x80000000

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_3

    .line 1488
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    move/from16 v18, v0

    add-int v17, v17, v18

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 1489
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 1491
    :cond_3
    invoke-direct/range {p0 .. p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 1497
    const/4 v3, 0x0

    .local v3, "delta":I
    const/4 v4, 0x0

    .line 1498
    .local v4, "deltaSecondary":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-eq v0, v1, :cond_4

    if-eqz v16, :cond_4

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/BaseGridView;->getScrollState()I

    move-result v17

    if-eqz v17, :cond_4

    .line 1501
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v7

    .line 1502
    .local v7, "focusView":Landroid/view/View;
    if-eqz v7, :cond_4

    .line 1503
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenter(Landroid/view/View;)I

    move-result v19

    add-int v18, v18, v19

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getSystemScrollPos(IZZ)I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I

    move/from16 v18, v0

    sub-int v3, v17, v18

    .line 1505
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->secondAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    move/from16 v18, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewCenterSecondary(Landroid/view/View;)I

    move-result v19

    add-int v18, v18, v19

    const/16 v19, 0x0

    const/16 v20, 0x0

    invoke-virtual/range {v17 .. v20}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->getSystemScrollPos(IZZ)I

    move-result v17

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I

    move/from16 v18, v0

    sub-int v4, v17, v18

    .line 1511
    .end local v7    # "focusView":Landroid/view/View;
    :cond_4
    invoke-virtual/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->hasDoneFirstLayout()Z

    move-result v9

    .line 1512
    .local v9, "hasDoneFirstLayout":Z
    move-object/from16 v0, p0

    iget v15, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 1513
    .local v15, "savedFocusPos":I
    const/4 v5, 0x0

    .line 1514
    .local v5, "fastRelayout":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v7/widget/RecyclerView$State;->didStructureChange()Z

    move-result v17

    if-nez v17, :cond_8

    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    move/from16 v17, v0

    if-nez v17, :cond_8

    if-eqz v9, :cond_8

    .line 1515
    const/4 v5, 0x1

    .line 1516
    move-object/from16 v0, p0

    move/from16 v1, v16

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->fastRelayout(Z)V

    .line 1578
    :goto_2
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    .line 1580
    if-eqz v16, :cond_5

    .line 1581
    neg-int v0, v3

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionPrimary(I)I

    .line 1582
    neg-int v0, v4

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionSecondary(I)I

    .line 1584
    :cond_5
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendVisibleItems()V

    .line 1585
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependVisibleItems()V

    .line 1586
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtFront()V

    .line 1587
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtEnd()V

    .line 1596
    move-object/from16 v0, p0

    iget-boolean v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSecondarySizeRefresh:Z

    move/from16 v17, v0

    if-eqz v17, :cond_f

    .line 1597
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSecondarySizeRefresh:Z

    .line 1602
    :goto_3
    if-eqz v5, :cond_6

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v15, :cond_6

    .line 1603
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->dispatchChildSelected()V

    .line 1605
    :cond_6
    const/16 v17, 0x0

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput-boolean v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    .line 1606
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->leaveContext()V

    goto/16 :goto_0

    .line 1485
    .end local v3    # "delta":I
    .end local v4    # "deltaSecondary":I
    .end local v5    # "fastRelayout":Z
    .end local v9    # "hasDoneFirstLayout":Z
    .end local v15    # "savedFocusPos":I
    .end local v16    # "scrollToFocus":Z
    :cond_7
    const/16 v16, 0x0

    goto/16 :goto_1

    .line 1518
    .restart local v3    # "delta":I
    .restart local v4    # "deltaSecondary":I
    .restart local v5    # "fastRelayout":Z
    .restart local v9    # "hasDoneFirstLayout":Z
    .restart local v15    # "savedFocusPos":I
    .restart local v16    # "scrollToFocus":Z
    :cond_8
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/BaseGridView;->hasFocus()Z

    move-result v8

    .line 1520
    .local v8, "hadFocus":Z
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->init(I)I

    move-result v17

    move/from16 v0, v17

    move-object/from16 v1, p0

    iput v0, v1, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 1521
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-eq v0, v15, :cond_9

    .line 1525
    :cond_9
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    const/16 v18, -0x1

    move/from16 v0, v17

    move/from16 v1, v18

    if-ne v0, v1, :cond_a

    .line 1526
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/BaseGridView;->clearFocus()V

    .line 1529
    :cond_a
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->invalidateScrollMin()V

    .line 1530
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v17

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->invalidateScrollMax()V

    .line 1533
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getSize()I

    move-result v17

    if-nez v17, :cond_e

    .line 1536
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v18, v0

    const/16 v19, -0x1

    invoke-virtual/range {v17 .. v19}, Landroid/support/v17/leanback/widget/StaggeredGrid;->setStart(II)V

    .line 1539
    const/4 v4, 0x0

    move v3, v4

    .line 1554
    :cond_b
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendVisibleItems()V

    .line 1556
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependVisibleItems()V

    .line 1562
    :cond_c
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMin()V

    .line 1563
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMax()V

    .line 1564
    move-object/from16 v0, p0

    iget v13, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    .line 1565
    .local v13, "oldFirstVisible":I
    move-object/from16 v0, p0

    iget v14, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    .line 1566
    .local v14, "oldLastVisible":I
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    move/from16 v17, v0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->findViewByPosition(I)Landroid/view/View;

    move-result-object v7

    .line 1568
    .restart local v7    # "focusView":Landroid/view/View;
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-direct {v0, v7, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToView(Landroid/view/View;Z)V

    .line 1569
    if-eqz v7, :cond_d

    if-eqz v8, :cond_d

    .line 1570
    invoke-virtual {v7}, Landroid/view/View;->requestFocus()Z

    .line 1572
    :cond_d
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->appendVisibleItems()V

    .line 1573
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->prependVisibleItems()V

    .line 1574
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtFront()V

    .line 1575
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeInvisibleViewsAtEnd()V

    .line 1576
    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v13, :cond_c

    move-object/from16 v0, p0

    iget v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I

    move/from16 v17, v0

    move/from16 v0, v17

    if-ne v0, v14, :cond_c

    goto/16 :goto_2

    .line 1546
    .end local v7    # "focusView":Landroid/view/View;
    .end local v13    # "oldFirstVisible":I
    .end local v14    # "oldLastVisible":I
    :cond_e
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getFirstIndex()I

    move-result v6

    .line 1547
    .local v6, "firstIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v17, v0

    invoke-virtual/range {v17 .. v17}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLastIndex()I

    move-result v12

    .line 1548
    .local v12, "lastIndex":I
    move v10, v6

    .local v10, "i":I
    :goto_4
    if-gt v10, v12, :cond_b

    .line 1549
    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGridProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    move-object/from16 v17, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-object/from16 v18, v0

    move-object/from16 v0, v18

    invoke-virtual {v0, v10}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v18

    move-object/from16 v0, v18

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    move/from16 v18, v0

    const/16 v19, 0x1

    move-object/from16 v0, v17

    move/from16 v1, v18

    move/from16 v2, v19

    invoke-interface {v0, v10, v1, v2}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->createItem(IIZ)V

    .line 1548
    add-int/lit8 v10, v10, 0x1

    goto :goto_4

    .line 1599
    .end local v6    # "firstIndex":I
    .end local v8    # "hadFocus":Z
    .end local v10    # "i":I
    .end local v12    # "lastIndex":I
    :cond_f
    invoke-direct/range {p0 .. p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateRowSecondarySizeRefresh()V

    goto/16 :goto_3
.end method

.method public onMeasure(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;II)V
    .locals 9
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "widthSpec"    # I
    .param p4, "heightSpec"    # I

    .prologue
    const/4 v7, 0x1

    .line 911
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 915
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v6, :cond_2

    .line 916
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 917
    .local v4, "sizePrimary":I
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 918
    .local v5, "sizeSecondary":I
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 919
    .local v2, "modeSecondary":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingTop()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingBottom()I

    move-result v8

    add-int v3, v6, v8

    .line 931
    .local v3, "paddingSecondary":I
    :goto_0
    iput v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMaxSizeSecondary:I

    .line 933
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    const/4 v8, -0x2

    if-ne v6, v8, :cond_5

    .line 934
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    if-nez v6, :cond_3

    move v6, v7

    :goto_1
    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    .line 935
    const/4 v6, 0x0

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    .line 937
    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    if-eqz v6, :cond_0

    iget-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    array-length v6, v6

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    if-eq v6, v8, :cond_1

    .line 938
    :cond_0
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    new-array v6, v6, [I

    iput-object v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondary:[I

    .line 942
    :cond_1
    invoke-direct {p0, v7}, Landroid/support/v17/leanback/widget/GridLayoutManager;->processRowSizeSecondary(Z)Z

    .line 944
    sparse-switch v2, :sswitch_data_0

    .line 956
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "wrong spec"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 921
    .end local v2    # "modeSecondary":I
    .end local v3    # "paddingSecondary":I
    .end local v4    # "sizePrimary":I
    .end local v5    # "sizeSecondary":I
    :cond_2
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v5

    .line 922
    .restart local v5    # "sizeSecondary":I
    invoke-static {p4}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 923
    .restart local v4    # "sizePrimary":I
    invoke-static {p3}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v2

    .line 924
    .restart local v2    # "modeSecondary":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingLeft()I

    move-result v6

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPaddingRight()I

    move-result v8

    add-int v3, v6, v8

    .restart local v3    # "paddingSecondary":I
    goto :goto_0

    .line 934
    :cond_3
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    goto :goto_1

    .line 946
    :sswitch_0
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSizeSecondary()I

    move-result v6

    add-int v1, v6, v3

    .line 1004
    .local v1, "measuredSizeSecondary":I
    :cond_4
    :goto_2
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v6, :cond_c

    .line 1005
    invoke-virtual {p0, v4, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setMeasuredDimension(II)V

    .line 1016
    :goto_3
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->leaveContext()V

    .line 1017
    return-void

    .line 949
    .end local v1    # "measuredSizeSecondary":I
    :sswitch_1
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSizeSecondary()I

    move-result v6

    add-int/2addr v6, v3

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMaxSizeSecondary:I

    invoke-static {v6, v7}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 951
    .restart local v1    # "measuredSizeSecondary":I
    goto :goto_2

    .line 953
    .end local v1    # "measuredSizeSecondary":I
    :sswitch_2
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMaxSizeSecondary:I

    .line 954
    .restart local v1    # "measuredSizeSecondary":I
    goto :goto_2

    .line 960
    .end local v1    # "measuredSizeSecondary":I
    :cond_5
    sparse-switch v2, :sswitch_data_1

    .line 1001
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "wrong spec"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 962
    :sswitch_3
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    if-nez v6, :cond_7

    .line 963
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v6, :cond_6

    .line 964
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Must specify rowHeight or view height"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 966
    :cond_6
    new-instance v6, Ljava/lang/IllegalStateException;

    const-string v7, "Must specify columnWidth or view width"

    invoke-direct {v6, v7}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v6

    .line 969
    :cond_7
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    .line 970
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    if-nez v6, :cond_8

    :goto_4
    iput v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    .line 971
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    mul-int/2addr v6, v7

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    add-int/2addr v6, v7

    add-int v1, v6, v3

    .line 973
    .restart local v1    # "measuredSizeSecondary":I
    goto :goto_2

    .line 970
    .end local v1    # "measuredSizeSecondary":I
    :cond_8
    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    goto :goto_4

    .line 976
    :sswitch_4
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    if-nez v6, :cond_9

    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    if-nez v6, :cond_9

    .line 977
    iput v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    .line 978
    sub-int v6, v5, v3

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    .line 991
    :goto_5
    move v1, v5

    .line 992
    .restart local v1    # "measuredSizeSecondary":I
    const/high16 v6, -0x80000000

    if-ne v2, v6, :cond_4

    .line 993
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    mul-int/2addr v6, v7

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    add-int/2addr v6, v7

    add-int v0, v6, v3

    .line 995
    .local v0, "childrenSize":I
    if-ge v0, v1, :cond_4

    .line 996
    move v1, v0

    goto :goto_2

    .line 979
    .end local v0    # "childrenSize":I
    .end local v1    # "measuredSizeSecondary":I
    :cond_9
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    if-nez v6, :cond_a

    .line 980
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    .line 981
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    add-int/2addr v6, v5

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    add-int/2addr v7, v8

    div-int/2addr v6, v7

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    goto :goto_5

    .line 983
    :cond_a
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    if-nez v6, :cond_b

    .line 984
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    .line 985
    sub-int v6, v5, v3

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    iget v8, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    add-int/lit8 v8, v8, -0x1

    mul-int/2addr v7, v8

    sub-int/2addr v6, v7

    iget v7, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    div-int/2addr v6, v7

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    goto :goto_5

    .line 988
    :cond_b
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I

    .line 989
    iget v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    iput v6, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFixedRowSizeSecondary:I

    goto :goto_5

    .line 1007
    .restart local v1    # "measuredSizeSecondary":I
    :cond_c
    invoke-virtual {p0, v1, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setMeasuredDimension(II)V

    goto/16 :goto_3

    .line 944
    :sswitch_data_0
    .sparse-switch
        -0x80000000 -> :sswitch_1
        0x0 -> :sswitch_0
        0x40000000 -> :sswitch_2
    .end sparse-switch

    .line 960
    :sswitch_data_1
    .sparse-switch
        -0x80000000 -> :sswitch_4
        0x0 -> :sswitch_3
        0x40000000 -> :sswitch_4
    .end sparse-switch
.end method

.method public onRequestChildFocus(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/view/View;)Z
    .locals 3
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "child"    # Landroid/view/View;
    .param p3, "focused"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x1

    .line 2008
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusSearchDisabled:Z

    if-eqz v0, :cond_1

    .line 2019
    :cond_0
    :goto_0
    return v2

    .line 2011
    :cond_1
    invoke-direct {p0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2016
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mInSelection:Z

    if-nez v0, :cond_0

    .line 2017
    invoke-direct {p0, p2, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToView(Landroid/view/View;Z)V

    goto :goto_0
.end method

.method public onRestoreInstanceState(Landroid/os/Parcelable;)V
    .locals 3
    .param p1, "state"    # Landroid/os/Parcelable;

    .prologue
    .line 2675
    instance-of v1, p1, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;

    if-nez v1, :cond_0

    .line 2685
    :goto_0
    return-void

    :cond_0
    move-object v0, p1

    .line 2678
    check-cast v0, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;

    .line 2679
    .local v0, "loadingState":Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;
    iget v1, v0, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;->index:I

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    .line 2680
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPositionOffset:I

    .line 2681
    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;->childStates:Landroid/os/Bundle;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->loadFromBundle(Landroid/os/Bundle;)V

    .line 2682
    const/4 v1, 0x1

    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    .line 2683
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->requestLayout()V

    goto :goto_0
.end method

.method public onSaveInstanceState()Landroid/os/Parcelable;
    .locals 6

    .prologue
    .line 2660
    new-instance v3, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;

    invoke-direct {v3}, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;-><init>()V

    .line 2661
    .local v3, "ss":Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-ge v1, v0, :cond_1

    .line 2662
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 2663
    .local v4, "view":Landroid/view/View;
    invoke-direct {p0, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPositionByView(Landroid/view/View;)I

    move-result v2

    .line 2664
    .local v2, "position":I
    const/4 v5, -0x1

    if-eq v2, v5, :cond_0

    .line 2665
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v5, v4, v2}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->saveOnScreenView(Landroid/view/View;I)V

    .line 2661
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2668
    .end local v2    # "position":I
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getSelection()I

    move-result v5

    iput v5, v3, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;->index:I

    .line 2669
    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->saveAsBundle()Landroid/os/Bundle;

    move-result-object v5

    iput-object v5, v3, Landroid/support/v17/leanback/widget/GridLayoutManager$SavedState;->childStates:Landroid/os/Bundle;

    .line 2670
    return-object v3
.end method

.method public removeAndRecycleAllViews(Landroid/support/v7/widget/RecyclerView$Recycler;)V
    .locals 2
    .param p1, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;

    .prologue
    .line 1452
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 1453
    invoke-virtual {p0, v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->removeAndRecycleViewAt(ILandroid/support/v7/widget/RecyclerView$Recycler;)V

    .line 1452
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 1455
    :cond_0
    return-void
.end method

.method public requestChildRectangleOnScreen(Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Landroid/graphics/Rect;Z)Z
    .locals 1
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "rect"    # Landroid/graphics/Rect;
    .param p4, "immediate"    # Z

    .prologue
    .line 2026
    const/4 v0, 0x0

    return v0
.end method

.method public scrollHorizontallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 2
    .param p1, "dx"    # I
    .param p2, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 1639
    iget-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->hasDoneFirstLayout()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1640
    :cond_0
    const/4 v0, 0x0

    .line 1650
    :goto_0
    return v0

    .line 1642
    :cond_1
    invoke-direct {p0, p2, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 1644
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v1, :cond_2

    .line 1645
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionPrimary(I)I

    move-result v0

    .line 1649
    .local v0, "result":I
    :goto_1
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->leaveContext()V

    goto :goto_0

    .line 1647
    .end local v0    # "result":I
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionSecondary(I)I

    move-result v0

    .restart local v0    # "result":I
    goto :goto_1
.end method

.method public scrollVerticallyBy(ILandroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)I
    .locals 3
    .param p1, "dy"    # I
    .param p2, "recycler"    # Landroid/support/v7/widget/RecyclerView$Recycler;
    .param p3, "state"    # Landroid/support/v7/widget/RecyclerView$State;

    .prologue
    .line 1656
    iget-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->hasDoneFirstLayout()Z

    move-result v1

    if-nez v1, :cond_1

    .line 1657
    :cond_0
    const/4 v0, 0x0

    .line 1667
    :goto_0
    return v0

    .line 1659
    :cond_1
    invoke-direct {p0, p2, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->saveContext(Landroid/support/v7/widget/RecyclerView$Recycler;Landroid/support/v7/widget/RecyclerView$State;)V

    .line 1661
    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 1662
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionPrimary(I)I

    move-result v0

    .line 1666
    .local v0, "result":I
    :goto_1
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->leaveContext()V

    goto :goto_0

    .line 1664
    .end local v0    # "result":I
    :cond_2
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollDirectionSecondary(I)I

    move-result v0

    .restart local v0    # "result":I
    goto :goto_1
.end method

.method setChildrenVisibility(I)V
    .locals 4
    .param p1, "visiblity"    # I

    .prologue
    .line 2609
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I

    .line 2610
    iget v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    .line 2611
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildCount()I

    move-result v0

    .line 2612
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 2613
    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 2612
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 2616
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_0
    return-void
.end method

.method public setFocusOutAllowed(ZZ)V
    .locals 0
    .param p1, "throughFront"    # Z
    .param p2, "throughEnd"    # Z

    .prologue
    .line 440
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutFront:Z

    .line 441
    iput-boolean p2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusOutEnd:Z

    .line 442
    return-void
.end method

.method public setFocusScrollStrategy(I)V
    .locals 0
    .param p1, "focusScrollStrategy"    # I

    .prologue
    .line 376
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    .line 377
    return-void
.end method

.method setFocusSearchDisabled(Z)V
    .locals 0
    .param p1, "disabled"    # Z

    .prologue
    .line 2279
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusSearchDisabled:Z

    .line 2280
    return-void
.end method

.method public setGravity(I)V
    .locals 0
    .param p1, "gravity"    # I

    .prologue
    .line 491
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mGravity:I

    .line 492
    return-void
.end method

.method public setHorizontalMargin(I)V
    .locals 1
    .param p1, "margin"    # I

    .prologue
    .line 475
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    .line 476
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mHorizontalMargin:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    .line 480
    :goto_0
    return-void

    .line 478
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mHorizontalMargin:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    goto :goto_0
.end method

.method public setItemAlignmentOffset(I)V
    .locals 1
    .param p1, "alignmentOffset"    # I

    .prologue
    .line 404
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->setItemAlignmentOffset(I)V

    .line 405
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments()V

    .line 406
    return-void
.end method

.method public setItemAlignmentOffsetPercent(F)V
    .locals 1
    .param p1, "offsetPercent"    # F

    .prologue
    .line 422
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->setItemAlignmentOffsetPercent(F)V

    .line 423
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments()V

    .line 424
    return-void
.end method

.method public setItemAlignmentOffsetWithPadding(Z)V
    .locals 1
    .param p1, "withPadding"    # Z

    .prologue
    .line 413
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->setItemAlignmentOffsetWithPadding(Z)V

    .line 414
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments()V

    .line 415
    return-void
.end method

.method public setItemAlignmentViewId(I)V
    .locals 1
    .param p1, "viewId"    # I

    .prologue
    .line 431
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemAlignment;->mainAxis()Landroid/support/v17/leanback/widget/ItemAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemAlignment$Axis;->setItemAlignmentViewId(I)V

    .line 432
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->updateChildAlignments()V

    .line 433
    return-void
.end method

.method public setItemMargin(I)V
    .locals 0
    .param p1, "margin"    # I

    .prologue
    .line 462
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mHorizontalMargin:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mVerticalMargin:I

    .line 463
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    .line 464
    return-void
.end method

.method public setLayoutEnabled(Z)V
    .locals 1
    .param p1, "layoutEnabled"    # Z

    .prologue
    .line 2602
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    if-eq v0, p1, :cond_0

    .line 2603
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mLayoutEnabled:Z

    .line 2604
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->requestLayout()V

    .line 2606
    :cond_0
    return-void
.end method

.method public setNumRows(I)V
    .locals 1
    .param p1, "numRows"    # I

    .prologue
    .line 445
    if-gez p1, :cond_0

    new-instance v0, Ljava/lang/IllegalArgumentException;

    invoke-direct {v0}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v0

    .line 446
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRowsRequested:I

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    .line 448
    return-void
.end method

.method public setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    .prologue
    .line 499
    iput-object p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    .line 500
    return-void
.end method

.method public setOrientation(I)V
    .locals 2
    .param p1, "orientation"    # I

    .prologue
    const/4 v1, 0x1

    .line 360
    if-eqz p1, :cond_0

    if-eq p1, v1, :cond_0

    .line 369
    :goto_0
    return-void

    .line 365
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    .line 366
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/WindowAlignment;->setOrientation(I)V

    .line 367
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mItemAlignment:Landroid/support/v17/leanback/widget/ItemAlignment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemAlignment;->setOrientation(I)V

    .line 368
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mForceFullLayout:Z

    goto :goto_0
.end method

.method public setPruneChild(Z)V
    .locals 1
    .param p1, "pruneChild"    # Z

    .prologue
    .line 2241
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    if-eq v0, p1, :cond_0

    .line 2242
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    .line 2243
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mPruneChild:Z

    if-eqz v0, :cond_0

    .line 2244
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->requestLayout()V

    .line 2247
    :cond_0
    return-void
.end method

.method public setRowHeight(I)V
    .locals 3
    .param p1, "height"    # I

    .prologue
    .line 454
    if-gez p1, :cond_0

    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    .line 455
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mRowSizeSecondaryRequested:I

    .line 459
    return-void

    .line 457
    :cond_1
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid row height: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setScrollEnabled(Z)V
    .locals 3
    .param p1, "scrollEnabled"    # Z

    .prologue
    .line 2254
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    if-eq v0, p1, :cond_0

    .line 2255
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    .line 2256
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollEnabled:Z

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusScrollStrategy:I

    if-nez v0, :cond_0

    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 2258
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mBaseGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    iget v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    const/4 v2, 0x1

    invoke-direct {p0, v0, v1, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToSelection(Landroid/support/v7/widget/RecyclerView;IZ)V

    .line 2261
    :cond_0
    return-void
.end method

.method public setSelection(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I

    .prologue
    .line 1856
    const/4 v0, 0x0

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setSelection(Landroid/support/v7/widget/RecyclerView;IZ)V

    .line 1857
    return-void
.end method

.method public setSelection(Landroid/support/v7/widget/RecyclerView;IZ)V
    .locals 1
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I
    .param p3, "smooth"    # Z

    .prologue
    .line 1868
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I

    if-eq v0, p2, :cond_0

    const/4 v0, -0x1

    if-eq p2, v0, :cond_0

    .line 1869
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToSelection(Landroid/support/v7/widget/RecyclerView;IZ)V

    .line 1871
    :cond_0
    return-void
.end method

.method public setSelectionSmooth(Landroid/support/v7/widget/RecyclerView;I)V
    .locals 1
    .param p1, "parent"    # Landroid/support/v7/widget/RecyclerView;
    .param p2, "position"    # I

    .prologue
    .line 1860
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setSelection(Landroid/support/v7/widget/RecyclerView;IZ)V

    .line 1861
    return-void
.end method

.method public setVerticalMargin(I)V
    .locals 1
    .param p1, "margin"    # I

    .prologue
    .line 467
    iget v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I

    if-nez v0, :cond_0

    .line 468
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mVerticalMargin:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginSecondary:I

    .line 472
    :goto_0
    return-void

    .line 470
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mVerticalMargin:I

    iput p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I

    goto :goto_0
.end method

.method public setWindowAlignment(I)V
    .locals 1
    .param p1, "windowAlignment"    # I

    .prologue
    .line 380
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setWindowAlignment(I)V

    .line 381
    return-void
.end method

.method public setWindowAlignmentOffset(I)V
    .locals 1
    .param p1, "alignmentOffset"    # I

    .prologue
    .line 388
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setWindowAlignmentOffset(I)V

    .line 389
    return-void
.end method

.method public setWindowAlignmentOffsetPercent(F)V
    .locals 1
    .param p1, "offsetPercent"    # F

    .prologue
    .line 396
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mWindowAlignment:Landroid/support/v17/leanback/widget/WindowAlignment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/WindowAlignment;->mainAxis()Landroid/support/v17/leanback/widget/WindowAlignment$Axis;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/WindowAlignment$Axis;->setWindowAlignmentOffsetPercent(F)V

    .line 397
    return-void
.end method
