.class Landroid/support/v17/leanback/widget/ActionPresenterSelector;
.super Landroid/support/v17/leanback/widget/PresenterSelector;
.source "ActionPresenterSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ActionPresenterSelector$TwoLineActionPresenter;,
        Landroid/support/v17/leanback/widget/ActionPresenterSelector$OneLineActionPresenter;,
        Landroid/support/v17/leanback/widget/ActionPresenterSelector$ActionViewHolder;
    }
.end annotation


# instance fields
.field private final mOneLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field private final mTwoLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PresenterSelector;-><init>()V

    .line 25
    new-instance v0, Landroid/support/v17/leanback/widget/ActionPresenterSelector$OneLineActionPresenter;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/ActionPresenterSelector$OneLineActionPresenter;-><init>(Landroid/support/v17/leanback/widget/ActionPresenterSelector;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ActionPresenterSelector;->mOneLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 26
    new-instance v0, Landroid/support/v17/leanback/widget/ActionPresenterSelector$TwoLineActionPresenter;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/ActionPresenterSelector$TwoLineActionPresenter;-><init>(Landroid/support/v17/leanback/widget/ActionPresenterSelector;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ActionPresenterSelector;->mTwoLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 70
    return-void
.end method


# virtual methods
.method public getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;
    .locals 2
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 30
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/Action;

    .line 31
    .local v0, "action":Landroid/support/v17/leanback/widget/Action;
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/Action;->getLabel2()Ljava/lang/CharSequence;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 32
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ActionPresenterSelector;->mOneLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 34
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ActionPresenterSelector;->mTwoLineActionPresenter:Landroid/support/v17/leanback/widget/Presenter;

    goto :goto_0
.end method
