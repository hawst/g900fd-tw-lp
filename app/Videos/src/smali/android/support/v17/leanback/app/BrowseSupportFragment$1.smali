.class Landroid/support/v17/leanback/app/BrowseSupportFragment$1;
.super Ljava/lang/Object;
.source "BrowseSupportFragment.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;->startHeadersTransitionInternal(Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

.field final synthetic val$withHeaders:Z


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V
    .locals 0

    .prologue
    .line 489
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    iput-boolean p2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->val$withHeaders:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 492
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->onTransitionStart()V

    .line 493
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->createHeadersTransition()V
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$400(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    .line 494
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

    move-result-object v2

    if-eqz v2, :cond_0

    .line 495
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

    move-result-object v2

    iget-boolean v3, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->val$withHeaders:Z

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;->onHeadersTransitionStart(Z)V

    .line 497
    :cond_0
    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$900()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v3

    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->val$withHeaders:Z

    if-eqz v2, :cond_2

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithHeaders:Ljava/lang/Object;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;

    move-result-object v2

    :goto_0
    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;
    invoke-static {v4}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$800(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v2, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 499
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersBackStackEnabled:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 500
    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->val$withHeaders:Z

    if-nez v2, :cond_3

    .line 501
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mWithHeadersBackStackName:Ljava/lang/String;
    invoke-static {v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 512
    :cond_1
    :goto_1
    return-void

    .line 497
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithoutHeaders:Ljava/lang/Object;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$700(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;

    move-result-object v2

    goto :goto_0

    .line 504
    :cond_3
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    move-result-object v2

    iget v1, v2, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 505
    .local v1, "index":I
    if-ltz v1, :cond_1

    .line 506
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v0

    .line 507
    .local v0, "entry":Landroid/support/v4/app/FragmentManager$BackStackEntry;
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-interface {v0}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getId()I

    move-result v3

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/support/v4/app/FragmentManager;->popBackStackImmediate(II)Z

    goto :goto_1
.end method
