.class Landroid/support/v17/leanback/app/BrowseSupportFragment$12;
.super Ljava/lang/Object;
.source "BrowseSupportFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemSelectedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 0

    .prologue
    .line 866
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "item"    # Ljava/lang/Object;
    .param p2, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 869
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v0

    .line 870
    .local v0, "position":I
    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1400()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "BrowseSupportFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "header selected position "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 871
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$12;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->onRowSelected(I)V
    invoke-static {v1, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1800(Landroid/support/v17/leanback/app/BrowseSupportFragment;I)V

    .line 872
    return-void
.end method
