.class public final Landroid/support/v17/leanback/transition/TransitionHelper;
.super Ljava/lang/Object;
.source "TransitionHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/transition/TransitionHelper$1;,
        Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperApi21Impl;,
        Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;,
        Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl;,
        Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;
    }
.end annotation


# static fields
.field private static final sHelper:Landroid/support/v17/leanback/transition/TransitionHelper;


# instance fields
.field mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 36
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-direct {v0}, Landroid/support/v17/leanback/transition/TransitionHelper;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/transition/TransitionHelper;->sHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    return-void
.end method

.method private constructor <init>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 529
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 530
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 531
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperApi21Impl;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperApi21Impl;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    .line 537
    :goto_0
    return-void

    .line 532
    :cond_0
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->systemSupportsTransitions()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 533
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    goto :goto_0

    .line 535
    :cond_1
    new-instance v0, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl;

    invoke-direct {v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperStubImpl;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    goto :goto_0
.end method

.method public static getInstance()Landroid/support/v17/leanback/transition/TransitionHelper;
    .locals 1

    .prologue
    .line 526
    sget-object v0, Landroid/support/v17/leanback/transition/TransitionHelper;->sHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    return-object v0
.end method

.method public static systemSupportsTransitions()Z
    .locals 2

    .prologue
    .line 45
    sget v0, Landroid/os/Build$VERSION;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_0

    .line 47
    const/4 v0, 0x1

    .line 49
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public addTarget(Ljava/lang/Object;Landroid/view/View;)V
    .locals 1
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 664
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->addTarget(Ljava/lang/Object;Landroid/view/View;)V

    .line 665
    return-void
.end method

.method public addTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "transitionSet"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 608
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 609
    return-void
.end method

.method public createChangeBounds(Z)Ljava/lang/Object;
    .locals 1
    .param p1, "reparent"    # Z

    .prologue
    .line 576
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createChangeBounds(Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 668
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createFadeTransition(I)Ljava/lang/Object;
    .locals 1
    .param p1, "fadeMode"    # I

    .prologue
    .line 648
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createScale()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 604
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createScale()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 572
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;
    .locals 1
    .param p1, "callback"    # Landroid/support/v17/leanback/transition/SlideCallback;

    .prologue
    .line 600
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createTransitionSet(Z)Ljava/lang/Object;
    .locals 1
    .param p1, "sequential"    # Z

    .prologue
    .line 596
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->createTransitionSet(Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public excludeChildren(Ljava/lang/Object;IZ)V
    .locals 1
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "targetId"    # I
    .param p3, "exclude"    # Z

    .prologue
    .line 620
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 621
    return-void
.end method

.method public getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
    .locals 1
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 540
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public runTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "scene"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 656
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 657
    return-void
.end method

.method public setDuration(Ljava/lang/Object;J)V
    .locals 2
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "duration"    # J

    .prologue
    .line 640
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->setDuration(Ljava/lang/Object;J)V

    .line 641
    return-void
.end method

.method public setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "timeInterpolator"    # Ljava/lang/Object;

    .prologue
    .line 660
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 661
    return-void
.end method

.method public setStartDelay(Ljava/lang/Object;J)V
    .locals 2
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "startDelay"    # J

    .prologue
    .line 636
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->setStartDelay(Ljava/lang/Object;J)V

    .line 637
    return-void
.end method

.method public setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V
    .locals 1
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "listener"    # Landroid/support/v17/leanback/transition/TransitionListener;

    .prologue
    .line 652
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelper;->mImpl:Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;

    invoke-interface {v0, p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 653
    return-void
.end method
