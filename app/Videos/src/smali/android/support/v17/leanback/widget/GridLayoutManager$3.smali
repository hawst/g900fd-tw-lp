.class Landroid/support/v17/leanback/widget/GridLayoutManager$3;
.super Landroid/support/v7/widget/LinearSmoothScroller;
.source "GridLayoutManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/GridLayoutManager;->scrollToSelection(Landroid/support/v7/widget/RecyclerView;IZ)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 1892
    iput-object p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-direct {p0, p2}, Landroid/support/v7/widget/LinearSmoothScroller;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method public computeScrollVectorForPosition(I)Landroid/graphics/PointF;
    .locals 6
    .param p1, "targetPosition"    # I

    .prologue
    const/4 v5, 0x0

    .line 1895
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->getChildCount()I

    move-result v2

    if-nez v2, :cond_0

    .line 1896
    const/4 v2, 0x0

    .line 1903
    :goto_0
    return-object v2

    .line 1898
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    iget-object v3, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getChildAt(I)Landroid/view/View;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getPosition(Landroid/view/View;)I

    move-result v1

    .line 1899
    .local v1, "firstChildPos":I
    if-ge p1, v1, :cond_1

    const/4 v0, -0x1

    .line 1900
    .local v0, "direction":I
    :goto_1
    iget-object v2, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I
    invoke-static {v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$800(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v2

    if-nez v2, :cond_2

    .line 1901
    new-instance v2, Landroid/graphics/PointF;

    int-to-float v3, v0

    invoke-direct {v2, v3, v5}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0

    .line 1899
    .end local v0    # "direction":I
    :cond_1
    const/4 v0, 0x1

    goto :goto_1

    .line 1903
    .restart local v0    # "direction":I
    :cond_2
    new-instance v2, Landroid/graphics/PointF;

    int-to-float v3, v0

    invoke-direct {v2, v5, v3}, Landroid/graphics/PointF;-><init>(FF)V

    goto :goto_0
.end method

.method protected onTargetFound(Landroid/view/View;Landroid/support/v7/widget/RecyclerView$State;Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;)V
    .locals 8
    .param p1, "targetView"    # Landroid/view/View;
    .param p2, "state"    # Landroid/support/v7/widget/RecyclerView$State;
    .param p3, "action"    # Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;

    .prologue
    const/4 v7, 0x1

    const/4 v6, 0x0

    .line 1909
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->hasFocus()Z

    move-result v4

    if-eqz v4, :cond_0

    .line 1910
    invoke-virtual {p1}, Landroid/view/View;->requestFocus()Z

    .line 1912
    :cond_0
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->dispatchChildSelected()V
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$600(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    .line 1913
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    iget-object v5, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I
    invoke-static {v5}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I

    move-result-object v5

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->getScrollPosition(Landroid/view/View;[I)Z
    invoke-static {v4, p1, v5}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$2000(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/view/View;[I)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 1915
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$800(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v4

    if-nez v4, :cond_2

    .line 1916
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I

    move-result-object v4

    aget v1, v4, v6

    .line 1917
    .local v1, "dx":I
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I

    move-result-object v4

    aget v2, v4, v7

    .line 1922
    .local v2, "dy":I
    :goto_0
    mul-int v4, v1, v1

    mul-int v5, v2, v2

    add-int/2addr v4, v5

    int-to-double v4, v4

    invoke-static {v4, v5}, Ljava/lang/Math;->sqrt(D)D

    move-result-wide v4

    double-to-int v0, v4

    .line 1923
    .local v0, "distance":I
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->calculateTimeForDeceleration(I)I

    move-result v3

    .line 1924
    .local v3, "time":I
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->mDecelerateInterpolator:Landroid/view/animation/DecelerateInterpolator;

    invoke-virtual {p3, v1, v2, v3, v4}, Landroid/support/v7/widget/RecyclerView$SmoothScroller$Action;->update(IIILandroid/view/animation/Interpolator;)V

    .line 1926
    .end local v0    # "distance":I
    .end local v1    # "dx":I
    .end local v2    # "dy":I
    .end local v3    # "time":I
    :cond_1
    return-void

    .line 1919
    :cond_2
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I

    move-result-object v4

    aget v1, v4, v7

    .line 1920
    .restart local v1    # "dx":I
    iget-object v4, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$3;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mTempDeltas:[I
    invoke-static {v4}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[I

    move-result-object v4

    aget v2, v4, v6

    .restart local v2    # "dy":I
    goto :goto_0
.end method
