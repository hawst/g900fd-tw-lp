.class final Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperStubImpl;
.super Ljava/lang/Object;
.source "ShadowHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperVersionImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/ShadowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ShadowHelperStubImpl"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 49
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/widget/ShadowHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/ShadowHelper$1;

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ShadowHelper$ShadowHelperStubImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public addShadow(Landroid/view/ViewGroup;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "shadowContainer"    # Landroid/view/ViewGroup;
    .param p2, "roundedCorners"    # Z

    .prologue
    .line 59
    const/4 v0, 0x0

    return-object v0
.end method

.method public prepareParent(Landroid/view/ViewGroup;)V
    .locals 0
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 54
    return-void
.end method

.method public setShadowFocusLevel(Ljava/lang/Object;F)V
    .locals 0
    .param p1, "impl"    # Ljava/lang/Object;
    .param p2, "level"    # F

    .prologue
    .line 65
    return-void
.end method

.method public setZ(Landroid/view/View;F)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "z"    # F

    .prologue
    .line 70
    return-void
.end method
