.class Landroid/support/v17/leanback/widget/ControlBarPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "ControlBarPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;,
        Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;,
        Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;,
        Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;
    }
.end annotation


# static fields
.field private static sChildMarginDefault:I

.field private static sControlIconWidth:I


# instance fields
.field private mLayoutResourceId:I

.field private mOnControlClickedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;

.field private mOnControlSelectedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "layoutResourceId"    # I

    .prologue
    .line 193
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 194
    iput p1, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mLayoutResourceId:I

    .line 195
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/ControlBarPresenter;)Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ControlBarPresenter;

    .prologue
    .line 31
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mOnControlSelectedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/widget/ControlBarPresenter;)Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ControlBarPresenter;

    .prologue
    .line 31
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mOnControlClickedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;

    return-object v0
.end method


# virtual methods
.method getChildMarginDefault(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 265
    sget v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sChildMarginDefault:I

    if-nez v0, :cond_0

    .line 266
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_controls_child_margin_default:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sChildMarginDefault:I

    .line 269
    :cond_0
    sget v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sChildMarginDefault:I

    return v0
.end method

.method getControlIconWidth(Landroid/content/Context;)I
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 273
    sget v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sControlIconWidth:I

    if-nez v0, :cond_0

    .line 274
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_control_icon_width:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sControlIconWidth:I

    .line 277
    :cond_0
    sget v0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->sControlIconWidth:I

    return v0
.end method

.method public getLayoutResourceId()I
    .locals 1

    .prologue
    .line 201
    iget v0, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mLayoutResourceId:I

    return v0
.end method

.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 241
    move-object v1, p1

    check-cast v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;

    .local v1, "vh":Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;
    move-object v0, p2

    .line 242
    check-cast v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;

    .line 243
    .local v0, "data":Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;->adapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eq v2, v3, :cond_0

    .line 244
    iget-object v2, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;->adapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iput-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 245
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v2, :cond_0

    .line 246
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v3, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 249
    :cond_0
    iget-object v2, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;->presenter:Landroid/support/v17/leanback/widget/Presenter;

    iput-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 250
    iput-object v0, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mData:Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;

    .line 251
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->showControls(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 252
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 234
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ControlBarPresenter;->getLayoutResourceId()I

    move-result v2

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 236
    .local v0, "v":Landroid/view/View;
    new-instance v1, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;

    invoke-direct {v1, p0, v0}, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;-><init>(Landroid/support/v17/leanback/widget/ControlBarPresenter;Landroid/view/View;)V

    return-object v1
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    const/4 v3, 0x0

    .line 256
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;

    .line 257
    .local v0, "vh":Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v1, :cond_0

    .line 258
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->unregisterObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 259
    iput-object v3, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 261
    :cond_0
    iput-object v3, v0, Landroid/support/v17/leanback/widget/ControlBarPresenter$ViewHolder;->mData:Landroid/support/v17/leanback/widget/ControlBarPresenter$BoundData;

    .line 262
    return-void
.end method

.method public setOnControlClickedListener(Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;

    .prologue
    .line 208
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mOnControlClickedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlClickedListener;

    .line 209
    return-void
.end method

.method public setOnControlSelectedListener(Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;

    .prologue
    .line 222
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ControlBarPresenter;->mOnControlSelectedListener:Landroid/support/v17/leanback/widget/ControlBarPresenter$OnControlSelectedListener;

    .line 223
    return-void
.end method
