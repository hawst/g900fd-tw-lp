.class public Landroid/support/v17/leanback/widget/SearchOrbView$Colors;
.super Ljava/lang/Object;
.source "SearchOrbView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/SearchOrbView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Colors"
.end annotation


# instance fields
.field public brightColor:I

.field public color:I

.field public iconColor:I


# direct methods
.method public constructor <init>(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p1}, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;-><init>(II)V

    .line 68
    return-void
.end method

.method public constructor <init>(II)V
    .locals 1
    .param p1, "color"    # I
    .param p2, "brightColor"    # I

    .prologue
    .line 78
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;-><init>(III)V

    .line 79
    return-void
.end method

.method public constructor <init>(III)V
    .locals 0
    .param p1, "color"    # I
    .param p2, "brightColor"    # I
    .param p3, "iconColor"    # I

    .prologue
    .line 88
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 89
    iput p1, p0, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;->color:I

    .line 90
    if-ne p2, p1, :cond_0

    invoke-static {p1}, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;->getBrightColor(I)I

    move-result p2

    .end local p2    # "brightColor":I
    :cond_0
    iput p2, p0, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;->brightColor:I

    .line 91
    iput p3, p0, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;->iconColor:I

    .line 92
    return-void
.end method

.method public static getBrightColor(I)I
    .locals 8
    .param p0, "color"    # I

    .prologue
    const/high16 v7, 0x42190000    # 38.25f

    const v6, 0x3f59999a    # 0.85f

    .line 113
    const/high16 v2, 0x42190000    # 38.25f

    .line 114
    .local v2, "brightnessValue":F
    invoke-static {p0}, Landroid/graphics/Color;->red(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v4, v5

    .line 115
    .local v4, "red":I
    invoke-static {p0}, Landroid/graphics/Color;->green(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v3, v5

    .line 116
    .local v3, "green":I
    invoke-static {p0}, Landroid/graphics/Color;->blue(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v1, v5

    .line 117
    .local v1, "blue":I
    invoke-static {p0}, Landroid/graphics/Color;->alpha(I)I

    move-result v5

    int-to-float v5, v5

    mul-float/2addr v5, v6

    add-float/2addr v5, v7

    float-to-int v0, v5

    .line 118
    .local v0, "alpha":I
    invoke-static {v0, v4, v3, v1}, Landroid/graphics/Color;->argb(IIII)I

    move-result v5

    return v5
.end method
