.class Landroid/support/v17/leanback/widget/GridLayoutManager$2;
.super Ljava/lang/Object;
.source "GridLayoutManager.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/GridLayoutManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/GridLayoutManager;)V
    .locals 0

    .prologue
    .line 1048
    iput-object p1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createItem(IIZ)V
    .locals 12
    .param p1, "index"    # I
    .param p2, "rowIndex"    # I
    .param p3, "append"    # Z

    .prologue
    const/4 v10, 0x0

    .line 1057
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->getViewForPosition(I)Landroid/view/View;

    move-result-object v2

    .line 1058
    .local v2, "v":Landroid/view/View;
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ltz v0, :cond_1

    .line 1061
    if-eqz p3, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    if-eq p1, v0, :cond_0

    .line 1062
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 1063
    :cond_0
    if-nez p3, :cond_1

    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-eq p1, v0, :cond_1

    .line 1064
    new-instance v0, Ljava/lang/RuntimeException;

    invoke-direct {v0}, Ljava/lang/RuntimeException;-><init>()V

    throw v0

    .line 1069
    :cond_1
    invoke-virtual {v2}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView$LayoutParams;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$LayoutParams;->isItemRemoved()Z

    move-result v0

    if-nez v0, :cond_4

    .line 1070
    if-eqz p3, :cond_8

    .line 1071
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->addView(Landroid/view/View;)V

    .line 1075
    :goto_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$300(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    const/4 v1, -0x1

    if-eq v0, v1, :cond_2

    .line 1076
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildVisibility:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$300(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    invoke-virtual {v2, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1080
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mInLayout:Z
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$400(Landroid/support/v17/leanback/widget/GridLayoutManager;)Z

    move-result v0

    if-eqz v0, :cond_3

    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFocusPosition:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$500(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ne p1, v0, :cond_3

    .line 1081
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->dispatchChildSelected()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$600(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    .line 1084
    :cond_3
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->measureChild(Landroid/view/View;)V
    invoke-static {v0, v2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$700(Landroid/support/v17/leanback/widget/GridLayoutManager;Landroid/view/View;)V

    .line 1087
    :cond_4
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mOrientation:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$800(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-nez v0, :cond_9

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredWidth()I

    move-result v9

    .line 1089
    .local v9, "length":I
    :goto_1
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v1

    aget-object v1, v1, p2

    iget v1, v1, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-ne v0, v1, :cond_5

    const/4 v10, 0x1

    .line 1090
    .local v10, "rowIsEmpty":Z
    :cond_5
    if-eqz p3, :cond_d

    .line 1091
    if-nez v10, :cond_a

    .line 1093
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1000(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    add-int v11, v0, v1

    .line 1109
    .local v11, "start":I
    :goto_2
    add-int v6, v11, v9

    .line 1110
    .local v6, "end":I
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iput v6, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 1136
    :goto_3
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-gez v0, :cond_11

    .line 1137
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # setter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v1, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$202(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I

    move-result v1

    # setter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$102(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I

    .line 1146
    :goto_4
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->getRowStartSecondary(I)I
    invoke-static {v0, p2}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1300(Landroid/support/v17/leanback/widget/GridLayoutManager;I)I

    move-result v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetSecondary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1400(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    sub-int v5, v0, v1

    .line 1147
    .local v5, "startSecondary":I
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/GridLayoutManager;->mChildrenStates:Landroid/support/v17/leanback/widget/ViewsStateBundle;

    invoke-virtual {v0, v2, p1}, Landroid/support/v17/leanback/widget/ViewsStateBundle;->loadView(Landroid/view/View;I)V

    .line 1148
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1500(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    sub-int v3, v11, v1

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mScrollOffsetPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1500(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    sub-int v4, v6, v1

    move v1, p2

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->layoutChild(ILandroid/view/View;III)V
    invoke-static/range {v0 .. v5}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1600(Landroid/support/v17/leanback/widget/GridLayoutManager;ILandroid/view/View;III)V

    .line 1153
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ne p1, v0, :cond_6

    .line 1154
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMin()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1700(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    .line 1156
    :cond_6
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ne p1, v0, :cond_7

    .line 1157
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # invokes: Landroid/support/v17/leanback/widget/GridLayoutManager;->updateScrollMax()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1800(Landroid/support/v17/leanback/widget/GridLayoutManager;)V

    .line 1159
    :cond_7
    return-void

    .line 1073
    .end local v5    # "startSecondary":I
    .end local v6    # "end":I
    .end local v9    # "length":I
    .end local v10    # "rowIsEmpty":Z
    .end local v11    # "start":I
    :cond_8
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v0, v2, v10}, Landroid/support/v17/leanback/widget/GridLayoutManager;->addView(Landroid/view/View;I)V

    goto/16 :goto_0

    .line 1087
    :cond_9
    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v9

    goto/16 :goto_1

    .line 1095
    .restart local v9    # "length":I
    .restart local v10    # "rowIsEmpty":Z
    :cond_a
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ltz v0, :cond_c

    .line 1096
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1100(Landroid/support/v17/leanback/widget/GridLayoutManager;)Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v0

    iget v8, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 1099
    .local v8, "lastRow":I
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mNumRows:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1200(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    if-ge v8, v0, :cond_b

    .line 1100
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, v8

    iget v11, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 1107
    .end local v8    # "lastRow":I
    .restart local v11    # "start":I
    :goto_5
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iput v11, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    goto/16 :goto_2

    .line 1102
    .end local v11    # "start":I
    .restart local v8    # "lastRow":I
    :cond_b
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, v8

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1000(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    add-int v11, v0, v1

    .restart local v11    # "start":I
    goto :goto_5

    .line 1105
    .end local v8    # "lastRow":I
    .end local v11    # "start":I
    :cond_c
    const/4 v11, 0x0

    .restart local v11    # "start":I
    goto :goto_5

    .line 1112
    .end local v11    # "start":I
    :cond_d
    if-nez v10, :cond_e

    .line 1114
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1000(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    sub-int v6, v0, v1

    .line 1115
    .restart local v6    # "end":I
    sub-int v11, v6, v9

    .line 1134
    .restart local v11    # "start":I
    :goto_6
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iput v11, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    goto/16 :goto_3

    .line 1117
    .end local v6    # "end":I
    .end local v11    # "start":I
    :cond_e
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v0

    if-ltz v0, :cond_10

    .line 1118
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mGrid:Landroid/support/v17/leanback/widget/StaggeredGrid;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1100(Landroid/support/v17/leanback/widget/GridLayoutManager;)Landroid/support/v17/leanback/widget/StaggeredGrid;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$100(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/StaggeredGrid;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v0

    iget v7, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 1121
    .local v7, "firstRow":I
    if-lez v7, :cond_f

    .line 1122
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, v7

    iget v11, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 1123
    .restart local v11    # "start":I
    add-int v6, v11, v9

    .line 1132
    .end local v7    # "firstRow":I
    .restart local v6    # "end":I
    :goto_7
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, p2

    iput v6, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    goto :goto_6

    .line 1125
    .end local v6    # "end":I
    .end local v11    # "start":I
    .restart local v7    # "firstRow":I
    :cond_f
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$900(Landroid/support/v17/leanback/widget/GridLayoutManager;)[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    move-result-object v0

    aget-object v0, v0, v7

    iget v0, v0, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    iget-object v1, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mMarginPrimary:I
    invoke-static {v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$1000(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    move-result v1

    sub-int v6, v0, v1

    .line 1126
    .restart local v6    # "end":I
    sub-int v11, v6, v9

    .restart local v11    # "start":I
    goto :goto_7

    .line 1129
    .end local v6    # "end":I
    .end local v7    # "firstRow":I
    .end local v11    # "start":I
    :cond_10
    const/4 v11, 0x0

    .line 1130
    .restart local v11    # "start":I
    move v6, v9

    .restart local v6    # "end":I
    goto :goto_7

    .line 1139
    :cond_11
    if-eqz p3, :cond_12

    .line 1140
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # operator++ for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mLastVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$208(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    goto/16 :goto_4

    .line 1142
    :cond_12
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # operator-- for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mFirstVisiblePos:I
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$110(Landroid/support/v17/leanback/widget/GridLayoutManager;)I

    goto/16 :goto_4
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 1052
    iget-object v0, p0, Landroid/support/v17/leanback/widget/GridLayoutManager$2;->this$0:Landroid/support/v17/leanback/widget/GridLayoutManager;

    # getter for: Landroid/support/v17/leanback/widget/GridLayoutManager;->mState:Landroid/support/v7/widget/RecyclerView$State;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/GridLayoutManager;->access$000(Landroid/support/v17/leanback/widget/GridLayoutManager;)Landroid/support/v7/widget/RecyclerView$State;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView$State;->getItemCount()I

    move-result v0

    return v0
.end method
