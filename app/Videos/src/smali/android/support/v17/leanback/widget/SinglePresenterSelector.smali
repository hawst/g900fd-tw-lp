.class public final Landroid/support/v17/leanback/widget/SinglePresenterSelector;
.super Landroid/support/v17/leanback/widget/PresenterSelector;
.source "SinglePresenterSelector.java"


# instance fields
.field private final mPresenter:Landroid/support/v17/leanback/widget/Presenter;


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;

    .prologue
    .line 27
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PresenterSelector;-><init>()V

    .line 28
    iput-object p1, p0, Landroid/support/v17/leanback/widget/SinglePresenterSelector;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 29
    return-void
.end method


# virtual methods
.method public getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;
    .locals 1
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 33
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SinglePresenterSelector;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    return-object v0
.end method
