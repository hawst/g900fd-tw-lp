.class Landroid/support/v17/leanback/app/BrowseSupportFragment$8;
.super Landroid/support/v17/leanback/transition/TransitionListener;
.source "BrowseSupportFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;->createHeadersTransition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 0

    .prologue
    .line 772
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Ljava/lang/Object;)V
    .locals 4
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 778
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    const/4 v3, 0x0

    # setter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;
    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$802(Landroid/support/v17/leanback/app/BrowseSupportFragment;Ljava/lang/Object;)Ljava/lang/Object;

    .line 779
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->onTransitionEnd()V

    .line 780
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->onTransitionEnd()V

    .line 781
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 782
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 783
    .local v0, "headerGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 784
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    .line 792
    .end local v0    # "headerGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    :cond_0
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

    move-result-object v2

    if-eqz v2, :cond_1

    .line 793
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

    move-result-object v2

    iget-object v3, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;->onHeadersTransitionStop(Z)V

    .line 795
    :cond_1
    return-void

    .line 787
    :cond_2
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    .line 788
    .local v1, "rowsGridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v2

    if-nez v2, :cond_0

    .line 789
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    goto :goto_0
.end method

.method public onTransitionStart(Ljava/lang/Object;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 775
    return-void
.end method
