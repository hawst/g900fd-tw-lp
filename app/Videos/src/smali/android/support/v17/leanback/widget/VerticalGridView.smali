.class public Landroid/support/v17/leanback/widget/VerticalGridView;
.super Landroid/support/v17/leanback/widget/BaseGridView;
.source "VerticalGridView.java"


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 30
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 31
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 34
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 35
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 38
    invoke-direct {p0, p1, p2, p3}, Landroid/support/v17/leanback/widget/BaseGridView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 39
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridView;->mLayoutManager:Landroid/support/v17/leanback/widget/GridLayoutManager;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setOrientation(I)V

    .line 40
    invoke-virtual {p0, p1, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 41
    return-void
.end method


# virtual methods
.method public bridge synthetic dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "x0"    # Landroid/view/MotionEvent;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->dispatchGenericFocusedEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic dispatchKeyEvent(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "x0"    # Landroid/view/KeyEvent;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->dispatchKeyEvent(Landroid/view/KeyEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "x0"    # Landroid/view/MotionEvent;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic getChildDrawingOrder(II)I
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # I

    .prologue
    .line 27
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/BaseGridView;->getChildDrawingOrder(II)I

    move-result v0

    return v0
.end method

.method public bridge synthetic getFocusScrollStrategy()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getFocusScrollStrategy()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getHorizontalMargin()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getHorizontalMargin()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItemAlignmentOffset()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getItemAlignmentOffset()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getItemAlignmentOffsetPercent()F
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getItemAlignmentOffsetPercent()F

    move-result v0

    return v0
.end method

.method public bridge synthetic getItemAlignmentViewId()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getItemAlignmentViewId()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getSelectedPosition()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getSelectedPosition()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getVerticalMargin()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getVerticalMargin()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getViewSelectedOffsets(Landroid/view/View;[I)V
    .locals 0
    .param p1, "x0"    # Landroid/view/View;
    .param p2, "x1"    # [I

    .prologue
    .line 27
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/BaseGridView;->getViewSelectedOffsets(Landroid/view/View;[I)V

    return-void
.end method

.method public bridge synthetic getWindowAlignment()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getWindowAlignment()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getWindowAlignmentOffset()I
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getWindowAlignmentOffset()I

    move-result v0

    return v0
.end method

.method public bridge synthetic getWindowAlignmentOffsetPercent()F
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->getWindowAlignmentOffsetPercent()F

    move-result v0

    return v0
.end method

.method public bridge synthetic hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->hasOverlappingRendering()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic hasPreviousViewInSameRow(I)Z
    .locals 1
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->hasPreviousViewInSameRow(I)Z

    move-result v0

    return v0
.end method

.method protected initAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 44
    invoke-virtual {p0, p1, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->initBaseGridViewAttributes(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 45
    sget-object v1, Landroid/support/v17/leanback/R$styleable;->lbVerticalGridView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 46
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setColumnWidth(Landroid/content/res/TypedArray;)V

    .line 47
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbVerticalGridView_numberOfColumns:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v1

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setNumColumns(I)V

    .line 48
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 49
    return-void
.end method

.method public bridge synthetic isChildLayoutAnimated()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->isChildLayoutAnimated()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isFocusDrawingOrderEnabled()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->isFocusDrawingOrderEnabled()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isItemAlignmentOffsetWithPadding()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->isItemAlignmentOffsetWithPadding()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic isScrollEnabled()Z
    .locals 1

    .prologue
    .line 27
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseGridView;->isScrollEnabled()Z

    move-result v0

    return v0
.end method

.method public bridge synthetic onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/graphics/Rect;

    .prologue
    .line 27
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/BaseGridView;->onRequestFocusInDescendants(ILandroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method public bridge synthetic setAnimateChildLayout(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setAnimateChildLayout(Z)V

    return-void
.end method

.method public bridge synthetic setChildrenVisibility(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setChildrenVisibility(I)V

    return-void
.end method

.method public setColumnWidth(I)V
    .locals 1
    .param p1, "width"    # I

    .prologue
    .line 77
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridView;->mLayoutManager:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setRowHeight(I)V

    .line 78
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestLayout()V

    .line 79
    return-void
.end method

.method setColumnWidth(Landroid/content/res/TypedArray;)V
    .locals 5
    .param p1, "array"    # Landroid/content/res/TypedArray;

    .prologue
    const/4 v4, 0x0

    .line 52
    sget v2, Landroid/support/v17/leanback/R$styleable;->lbVerticalGridView_columnWidth:I

    invoke-virtual {p1, v2}, Landroid/content/res/TypedArray;->peekValue(I)Landroid/util/TypedValue;

    move-result-object v1

    .line 54
    .local v1, "typedValue":Landroid/util/TypedValue;
    if-eqz v1, :cond_0

    iget v2, v1, Landroid/util/TypedValue;->type:I

    const/4 v3, 0x5

    if-ne v2, v3, :cond_0

    .line 55
    sget v2, Landroid/support/v17/leanback/R$styleable;->lbVerticalGridView_columnWidth:I

    invoke-virtual {p1, v2, v4}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v0

    .line 59
    .local v0, "size":I
    :goto_0
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setColumnWidth(I)V

    .line 60
    return-void

    .line 57
    .end local v0    # "size":I
    :cond_0
    sget v2, Landroid/support/v17/leanback/R$styleable;->lbVerticalGridView_columnWidth:I

    invoke-virtual {p1, v2, v4}, Landroid/content/res/TypedArray;->getInt(II)I

    move-result v0

    .restart local v0    # "size":I
    goto :goto_0
.end method

.method public bridge synthetic setFocusDrawingOrderEnabled(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setFocusDrawingOrderEnabled(Z)V

    return-void
.end method

.method public bridge synthetic setFocusScrollStrategy(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setFocusScrollStrategy(I)V

    return-void
.end method

.method public bridge synthetic setGravity(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setGravity(I)V

    return-void
.end method

.method public bridge synthetic setHasOverlappingRendering(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setHasOverlappingRendering(Z)V

    return-void
.end method

.method public bridge synthetic setHorizontalMargin(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setHorizontalMargin(I)V

    return-void
.end method

.method public bridge synthetic setItemAlignmentOffset(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setItemAlignmentOffset(I)V

    return-void
.end method

.method public bridge synthetic setItemAlignmentOffsetPercent(F)V
    .locals 0
    .param p1, "x0"    # F

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setItemAlignmentOffsetPercent(F)V

    return-void
.end method

.method public bridge synthetic setItemAlignmentOffsetWithPadding(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setItemAlignmentOffsetWithPadding(Z)V

    return-void
.end method

.method public bridge synthetic setItemAlignmentViewId(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setItemAlignmentViewId(I)V

    return-void
.end method

.method public bridge synthetic setItemMargin(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setItemMargin(I)V

    return-void
.end method

.method public bridge synthetic setLayoutEnabled(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setLayoutEnabled(Z)V

    return-void
.end method

.method public setNumColumns(I)V
    .locals 1
    .param p1, "numColumns"    # I

    .prologue
    .line 66
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridView;->mLayoutManager:Landroid/support/v17/leanback/widget/GridLayoutManager;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/GridLayoutManager;->setNumRows(I)V

    .line 67
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestLayout()V

    .line 68
    return-void
.end method

.method public bridge synthetic setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V

    return-void
.end method

.method public bridge synthetic setOnKeyInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setOnKeyInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;)V

    return-void
.end method

.method public bridge synthetic setOnMotionInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setOnMotionInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;)V

    return-void
.end method

.method public bridge synthetic setOnTouchInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setOnTouchInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;)V

    return-void
.end method

.method public bridge synthetic setPruneChild(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setPruneChild(Z)V

    return-void
.end method

.method public bridge synthetic setScrollEnabled(Z)V
    .locals 0
    .param p1, "x0"    # Z

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setScrollEnabled(Z)V

    return-void
.end method

.method public bridge synthetic setSelectedPosition(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setSelectedPosition(I)V

    return-void
.end method

.method public bridge synthetic setSelectedPositionSmooth(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setSelectedPositionSmooth(I)V

    return-void
.end method

.method public bridge synthetic setVerticalMargin(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setVerticalMargin(I)V

    return-void
.end method

.method public bridge synthetic setWindowAlignment(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setWindowAlignment(I)V

    return-void
.end method

.method public bridge synthetic setWindowAlignmentOffset(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setWindowAlignmentOffset(I)V

    return-void
.end method

.method public bridge synthetic setWindowAlignmentOffsetPercent(F)V
    .locals 0
    .param p1, "x0"    # F

    .prologue
    .line 27
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/BaseGridView;->setWindowAlignmentOffsetPercent(F)V

    return-void
.end method
