.class public final Landroid/support/v17/leanback/R$dimen;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "dimen"
.end annotation


# static fields
.field public static final lb_action_padding_horizontal:I = 0x7f0e0066

.field public static final lb_action_with_icon_padding_left:I = 0x7f0e0067

.field public static final lb_action_with_icon_padding_right:I = 0x7f0e0068

.field public static final lb_browse_expanded_row_no_hovercard_bottom_padding:I = 0x7f0e0049

.field public static final lb_browse_expanded_selected_row_top_padding:I = 0x7f0e0048

.field public static final lb_browse_header_select_duration:I = 0x7f0e0040

.field public static final lb_browse_header_select_scale:I = 0x7f0e0041

.field public static final lb_browse_selected_row_top_padding:I = 0x7f0e0047

.field public static final lb_control_icon_width:I = 0x7f0e0082

.field public static final lb_details_description_body_line_spacing:I = 0x7f0e0060

.field public static final lb_details_description_title_baseline:I = 0x7f0e0061

.field public static final lb_details_description_title_line_spacing:I = 0x7f0e005f

.field public static final lb_details_description_under_subtitle_baseline_margin:I = 0x7f0e0063

.field public static final lb_details_description_under_title_baseline_margin:I = 0x7f0e0062

.field public static final lb_details_overview_actions_fade_size:I = 0x7f0e005a

.field public static final lb_details_overview_height_large:I = 0x7f0e004a

.field public static final lb_details_overview_height_small:I = 0x7f0e004b

.field public static final lb_details_overview_image_margin_horizontal:I = 0x7f0e0053

.field public static final lb_details_overview_image_margin_vertical:I = 0x7f0e0054

.field public static final lb_details_rows_align_top:I = 0x7f0e005b

.field public static final lb_error_under_image_baseline_margin:I = 0x7f0e0087

.field public static final lb_error_under_message_baseline_margin:I = 0x7f0e0088

.field public static final lb_material_shadow_focused_z:I = 0x7f0e00ab

.field public static final lb_material_shadow_normal_z:I = 0x7f0e00aa

.field public static final lb_playback_controls_align_bottom:I = 0x7f0e006c

.field public static final lb_playback_controls_child_margin_bigger:I = 0x7f0e007c

.field public static final lb_playback_controls_child_margin_biggest:I = 0x7f0e007d

.field public static final lb_playback_controls_child_margin_default:I = 0x7f0e007b

.field public static final lb_playback_controls_padding_bottom:I = 0x7f0e006d

.field public static final lb_playback_controls_z:I = 0x7f0e00b0

.field public static final lb_playback_major_fade_translate_y:I = 0x7f0e006e

.field public static final lb_playback_minor_fade_translate_y:I = 0x7f0e006f

.field public static final lb_rounded_rect_corner_radius:I = 0x7f0e00b2

.field public static final lb_search_bar_height:I = 0x7f0e0089

.field public static final lb_search_orb_focused_z:I = 0x7f0e00ae

.field public static final lb_search_orb_unfocused_z:I = 0x7f0e00ad
