.class Landroid/support/v17/leanback/app/RowsSupportFragment$1;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
.source "RowsSupportFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/RowsSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/RowsSupportFragment;)V
    .locals 0

    .prologue
    .line 367
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V
    .locals 2
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;
    .param p2, "type"    # I

    .prologue
    .line 370
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$000(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V

    move-object v0, p1

    .line 371
    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$100(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 372
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 373
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V

    .line 375
    :cond_0
    return-void
.end method

.method public onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 401
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$600(Landroid/support/v17/leanback/app/RowsSupportFragment;)Z

    move-result v0

    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$700(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V

    .line 402
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$800(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v0

    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$900(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 403
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$1000(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v0

    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$1100(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 404
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 407
    :cond_0
    return-void
.end method

.method public onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 420
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 421
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 423
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 5
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 378
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 379
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/RowPresenter;->canDrawOutOfBounds()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 380
    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/VerticalGridView;->setClipChildren(Z)V

    .line 382
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    invoke-static {v1, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$300(Landroid/support/v17/leanback/app/RowsSupportFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 383
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # setter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z
    invoke-static {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$402(Landroid/support/v17/leanback/app/RowsSupportFragment;Z)Z

    .line 384
    new-instance v1, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;

    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-direct {v1, v2, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;-><init>(Landroid/support/v17/leanback/app/RowsSupportFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    invoke-virtual {p1, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->setExtraObject(Ljava/lang/Object;)V

    .line 388
    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    invoke-static {p1, v3, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 389
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 390
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 392
    :cond_1
    return-void
.end method

.method public onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 410
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$1200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 411
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$1200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    invoke-static {v0, v1, v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 412
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    const/4 v1, 0x0

    # setter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$1202(Landroid/support/v17/leanback/app/RowsSupportFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 414
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 415
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 417
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 426
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getExtraObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;

    .line 427
    .local v0, "extra":Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;->endAnimations()V

    .line 428
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 429
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 431
    :cond_0
    return-void
.end method
