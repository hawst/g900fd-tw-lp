.class Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;
.super Ljava/lang/Object;
.source "BackgroundManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BackgroundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BackgroundContinuityService"
.end annotation


# static fields
.field private static DEBUG:Z

.field private static sService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;


# instance fields
.field private mColor:I

.field private mCount:I

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mLastThemeDrawable:Ljava/lang/ref/WeakReference;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/ref/WeakReference",
            "<",
            "Landroid/graphics/drawable/Drawable;",
            ">;"
        }
    .end annotation
.end field

.field private mLastThemeDrawableId:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 262
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->DEBUG:Z

    .line 264
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->sService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 274
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 275
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->reset()V

    .line 276
    return-void
.end method

.method public static getInstance()Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;
    .locals 4

    .prologue
    .line 284
    sget-object v1, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->sService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    iget v0, v1, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    add-int/lit8 v2, v0, 0x1

    iput v2, v1, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    .line 285
    .local v0, "count":I
    sget-boolean v1, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->DEBUG:Z

    if-eqz v1, :cond_0

    const-string v1, "BackgroundContinuityService"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Returning instance with new count "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 286
    :cond_0
    sget-object v1, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->sService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    return-object v1
.end method

.method private reset()V
    .locals 1

    .prologue
    .line 279
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mColor:I

    .line 280
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 281
    return-void
.end method


# virtual methods
.method public getColor()I
    .locals 1

    .prologue
    .line 297
    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mColor:I

    return v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 300
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public getThemeDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "themeDrawableId"    # I

    .prologue
    .line 309
    const/4 v0, 0x0

    .line 310
    .local v0, "drawable":Landroid/graphics/drawable/Drawable;
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mLastThemeDrawable:Ljava/lang/ref/WeakReference;

    if-eqz v1, :cond_0

    iget v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mLastThemeDrawableId:I

    if-ne v1, p2, :cond_0

    .line 311
    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mLastThemeDrawable:Ljava/lang/ref/WeakReference;

    invoke-virtual {v1}, Ljava/lang/ref/WeakReference;->get()Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    check-cast v0, Landroid/graphics/drawable/Drawable;

    .line 313
    .restart local v0    # "drawable":Landroid/graphics/drawable/Drawable;
    :cond_0
    if-nez v0, :cond_1

    .line 314
    invoke-static {p1, p2}, Landroid/support/v4/content/ContextCompat;->getDrawable(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 315
    new-instance v1, Ljava/lang/ref/WeakReference;

    invoke-direct {v1, v0}, Ljava/lang/ref/WeakReference;-><init>(Ljava/lang/Object;)V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mLastThemeDrawable:Ljava/lang/ref/WeakReference;

    .line 316
    iput p2, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mLastThemeDrawableId:I

    .line 318
    :cond_1
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;

    move-result-object v1

    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/graphics/drawable/Drawable$ConstantState;->newDrawable(Landroid/content/res/Resources;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    move-result-object v1

    return-object v1
.end method

.method public setColor(I)V
    .locals 0
    .param p1, "color"    # I

    .prologue
    .line 303
    iput p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mColor:I

    .line 304
    return-void
.end method

.method public setDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 306
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 307
    return-void
.end method

.method public unref()V
    .locals 3

    .prologue
    .line 290
    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    if-gtz v0, :cond_0

    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Can\'t unref, count "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 291
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->mCount:I

    if-nez v0, :cond_2

    .line 292
    sget-boolean v0, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->DEBUG:Z

    if-eqz v0, :cond_1

    const-string v0, "BackgroundContinuityService"

    const-string v1, "mCount is zero, resetting"

    invoke-static {v0, v1}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 293
    :cond_1
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->reset()V

    .line 295
    :cond_2
    return-void
.end method
