.class public Landroid/support/v17/leanback/widget/BaseCardView;
.super Landroid/widget/FrameLayout;
.source "BaseCardView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/BaseCardView$InfoAlphaAnimation;,
        Landroid/support/v17/leanback/widget/BaseCardView$InfoHeightAnimation;,
        Landroid/support/v17/leanback/widget/BaseCardView$InfoOffsetAnimation;,
        Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    }
.end annotation


# static fields
.field public static final CARD_REGION_VISIBLE_ACTIVATED:I = 0x1

.field public static final CARD_REGION_VISIBLE_ALWAYS:I = 0x0

.field public static final CARD_REGION_VISIBLE_SELECTED:I = 0x2

.field public static final CARD_TYPE_INFO_OVER:I = 0x1

.field public static final CARD_TYPE_INFO_UNDER:I = 0x2

.field public static final CARD_TYPE_INFO_UNDER_WITH_EXTRA:I = 0x3

.field public static final CARD_TYPE_MAIN_ONLY:I

.field private static final LB_PRESSED_STATE_SET:[I


# instance fields
.field private final mActivatedAnimDuration:I

.field private mAnim:Landroid/view/animation/Animation;

.field private final mAnimationTrigger:Ljava/lang/Runnable;

.field private mCardType:I

.field private mDelaySelectedAnim:Z

.field private mExtraViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mExtraVisibility:I

.field private mInfoAlpha:F

.field private mInfoOffset:F

.field private mInfoViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mInfoVisFraction:F

.field private mInfoVisibility:I

.field private mMainViewList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end field

.field private mMeasuredHeight:I

.field private mMeasuredWidth:I

.field private final mSelectedAnimDuration:I

.field private mSelectedAnimationDelay:I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 132
    const/4 v0, 0x1

    new-array v0, v0, [I

    const/4 v1, 0x0

    const v2, 0x10100a7

    aput v2, v0, v1

    sput-object v0, Landroid/support/v17/leanback/widget/BaseCardView;->LB_PRESSED_STATE_SET:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 143
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Landroid/support/v17/leanback/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 144
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 147
    sget v0, Landroid/support/v17/leanback/R$attr;->baseCardViewStyle:I

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 148
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v5, 0x1

    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 151
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 129
    const/high16 v1, 0x3f800000    # 1.0f

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoAlpha:F

    .line 135
    new-instance v1, Landroid/support/v17/leanback/widget/BaseCardView$1;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/widget/BaseCardView$1;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;)V

    iput-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    .line 153
    sget-object v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView:[I

    invoke-virtual {p1, p2, v1, p3, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[III)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 156
    .local v0, "a":Landroid/content/res/TypedArray;
    :try_start_0
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_cardType:I

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    .line 157
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_infoVisibility:I

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    .line 159
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_extraVisibility:I

    const/4 v2, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    .line 162
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    iget v2, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    if-ge v1, v2, :cond_0

    .line 163
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    .line 166
    :cond_0
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_selectedAnimationDelay:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$integer;->lb_card_selected_animation_delay:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mSelectedAnimationDelay:I

    .line 170
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_selectedAnimationDuration:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$integer;->lb_card_selected_animation_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mSelectedAnimDuration:I

    .line 174
    sget v1, Landroid/support/v17/leanback/R$styleable;->lbBaseCardView_activatedAnimationDuration:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$integer;->lb_card_activated_animation_duration:I

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mActivatedAnimDuration:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 178
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 181
    iput-boolean v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mDelaySelectedAnim:Z

    .line 183
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    .line 184
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    .line 185
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    .line 187
    iput v4, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    .line 188
    iput v4, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    .line 189
    return-void

    .line 178
    :catchall_0
    move-exception v1

    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    throw v1
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/BaseCardView;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;
    .param p1, "x1"    # Z

    .prologue
    .line 52
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->animateInfoOffset(Z)V

    return-void
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/widget/BaseCardView;)F
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;

    .prologue
    .line 52
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    return v0
.end method

.method static synthetic access$102(Landroid/support/v17/leanback/widget/BaseCardView;F)F
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    return p1
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/widget/BaseCardView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/widget/BaseCardView;)F
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;

    .prologue
    .line 52
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoAlpha:F

    return v0
.end method

.method static synthetic access$302(Landroid/support/v17/leanback/widget/BaseCardView;F)F
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoAlpha:F

    return p1
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/widget/BaseCardView;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$502(Landroid/support/v17/leanback/widget/BaseCardView;F)F
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/BaseCardView;
    .param p1, "x1"    # F

    .prologue
    .line 52
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    return p1
.end method

.method private animateInfoAlpha(Z)V
    .locals 4
    .param p1, "shown"    # Z

    .prologue
    .line 731
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->cancelAnimations()V

    .line 733
    if-eqz p1, :cond_0

    .line 734
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_0

    .line 735
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 734
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 739
    .end local v0    # "i":I
    :cond_0
    new-instance v2, Landroid/support/v17/leanback/widget/BaseCardView$InfoAlphaAnimation;

    iget v3, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoAlpha:F

    if-eqz p1, :cond_1

    const/high16 v1, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {v2, p0, v3, v1}, Landroid/support/v17/leanback/widget/BaseCardView$InfoAlphaAnimation;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;FF)V

    iput-object v2, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    .line 740
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    iget v2, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mActivatedAnimDuration:I

    int-to-long v2, v2

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 741
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v2, Landroid/view/animation/DecelerateInterpolator;

    invoke-direct {v2}, Landroid/view/animation/DecelerateInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 742
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v2, Landroid/support/v17/leanback/widget/BaseCardView$4;

    invoke-direct {v2, p0}, Landroid/support/v17/leanback/widget/BaseCardView$4;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;)V

    invoke-virtual {v1, v2}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 761
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/BaseCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 762
    return-void

    .line 739
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private animateInfoHeight(Z)V
    .locals 8
    .param p1, "shown"    # Z

    .prologue
    const/4 v7, 0x0

    .line 686
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->cancelAnimations()V

    .line 688
    const/4 v0, 0x0

    .line 689
    .local v0, "extraHeight":I
    if-eqz p1, :cond_0

    .line 690
    iget v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 691
    .local v4, "widthSpec":I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 693
    .local v2, "heightSpec":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 694
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 695
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 696
    invoke-virtual {v1, v4, v2}, Landroid/view/View;->measure(II)V

    .line 697
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 693
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 701
    .end local v1    # "extraView":Landroid/view/View;
    .end local v2    # "heightSpec":I
    .end local v3    # "i":I
    .end local v4    # "widthSpec":I
    :cond_0
    new-instance v6, Landroid/support/v17/leanback/widget/BaseCardView$InfoHeightAnimation;

    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    if-eqz p1, :cond_1

    const/high16 v5, 0x3f800000    # 1.0f

    :goto_1
    invoke-direct {v6, p0, v7, v5}, Landroid/support/v17/leanback/widget/BaseCardView$InfoHeightAnimation;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;FF)V

    iput-object v6, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    .line 702
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    iget v6, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mSelectedAnimDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 703
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 704
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/support/v17/leanback/widget/BaseCardView$3;

    invoke-direct {v6, p0}, Landroid/support/v17/leanback/widget/BaseCardView$3;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 723
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v5}, Landroid/support/v17/leanback/widget/BaseCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 724
    return-void

    .line 701
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private animateInfoOffset(Z)V
    .locals 8
    .param p1, "shown"    # Z

    .prologue
    const/4 v7, 0x0

    .line 643
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->cancelAnimations()V

    .line 645
    const/4 v0, 0x0

    .line 646
    .local v0, "extraHeight":I
    if-eqz p1, :cond_0

    .line 647
    iget v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    const/high16 v6, 0x40000000    # 2.0f

    invoke-static {v5, v6}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    .line 648
    .local v4, "widthSpec":I
    invoke-static {v7, v7}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    .line 650
    .local v2, "heightSpec":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5}, Ljava/util/ArrayList;->size()I

    move-result v5

    if-ge v3, v5, :cond_0

    .line 651
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v5, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 652
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1, v7}, Landroid/view/View;->setVisibility(I)V

    .line 653
    invoke-virtual {v1, v4, v2}, Landroid/view/View;->measure(II)V

    .line 654
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v5

    invoke-static {v0, v5}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 650
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 658
    .end local v1    # "extraView":Landroid/view/View;
    .end local v2    # "heightSpec":I
    .end local v3    # "i":I
    .end local v4    # "widthSpec":I
    :cond_0
    new-instance v6, Landroid/support/v17/leanback/widget/BaseCardView$InfoOffsetAnimation;

    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    if-eqz p1, :cond_1

    int-to-float v5, v0

    :goto_1
    invoke-direct {v6, p0, v7, v5}, Landroid/support/v17/leanback/widget/BaseCardView$InfoOffsetAnimation;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;FF)V

    iput-object v6, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    .line 659
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    iget v6, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mSelectedAnimDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v6, v7}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 660
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v6}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 661
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    new-instance v6, Landroid/support/v17/leanback/widget/BaseCardView$2;

    invoke-direct {v6, p0}, Landroid/support/v17/leanback/widget/BaseCardView$2;-><init>(Landroid/support/v17/leanback/widget/BaseCardView;)V

    invoke-virtual {v5, v6}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 680
    iget-object v5, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {p0, v5}, Landroid/support/v17/leanback/widget/BaseCardView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 681
    return-void

    .line 658
    :cond_1
    const/4 v5, 0x0

    goto :goto_1
.end method

.method private applyActiveState(Z)V
    .locals 2
    .param p1, "active"    # Z

    .prologue
    const/4 v1, 0x1

    .line 572
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasInfoRegion()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    if-gt v0, v1, :cond_0

    .line 573
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->setInfoViewVisibility(Z)V

    .line 575
    :cond_0
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasExtraRegion()Z

    move-result v0

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    if-gt v0, v1, :cond_1

    .line 578
    :cond_1
    return-void
.end method

.method private applySelectedState(Z)V
    .locals 4
    .param p1, "focused"    # Z

    .prologue
    .line 613
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 615
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_3

    .line 617
    if-eqz p1, :cond_2

    .line 618
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mDelaySelectedAnim:Z

    if-nez v0, :cond_1

    .line 619
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->post(Ljava/lang/Runnable;)Z

    .line 620
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mDelaySelectedAnim:Z

    .line 630
    :cond_0
    :goto_0
    return-void

    .line 622
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mSelectedAnimationDelay:I

    int-to-long v2, v1

    invoke-virtual {p0, v0, v2, v3}, Landroid/support/v17/leanback/widget/BaseCardView;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 625
    :cond_2
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->animateInfoOffset(Z)V

    goto :goto_0

    .line 627
    :cond_3
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 628
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->setInfoViewVisibility(Z)V

    goto :goto_0
.end method

.method private cancelAnimations()V
    .locals 1

    .prologue
    .line 633
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    if-eqz v0, :cond_0

    .line 634
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    invoke-virtual {v0}, Landroid/view/animation/Animation;->cancel()V

    .line 635
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnim:Landroid/view/animation/Animation;

    .line 637
    :cond_0
    return-void
.end method

.method private findChildrenViews()V
    .locals 12

    .prologue
    const/16 v9, 0x8

    const/4 v11, 0x0

    const/4 v10, 0x2

    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 508
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 509
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 510
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v8}, Ljava/util/ArrayList;->clear()V

    .line 512
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getChildCount()I

    move-result v1

    .line 514
    .local v1, "count":I
    iget v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    invoke-direct {p0, v8}, Landroid/support/v17/leanback/widget/BaseCardView;->isRegionVisible(I)Z

    move-result v4

    .line 515
    .local v4, "infoVisible":Z
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasExtraRegion()Z

    move-result v8

    if-eqz v8, :cond_1

    iget v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    cmpl-float v8, v8, v11

    if-lez v8, :cond_1

    move v2, v6

    .line 517
    .local v2, "extraVisible":Z
    :goto_0
    iget v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    if-ne v8, v10, :cond_0

    iget v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    if-ne v8, v10, :cond_0

    .line 518
    if-eqz v4, :cond_2

    iget v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    cmpl-float v8, v8, v11

    if-lez v8, :cond_2

    move v4, v6

    .line 521
    :cond_0
    :goto_1
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_2
    if-ge v3, v1, :cond_8

    .line 522
    invoke-virtual {p0, v3}, Landroid/support/v17/leanback/widget/BaseCardView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 524
    .local v0, "child":Landroid/view/View;
    if-nez v0, :cond_3

    .line 521
    :goto_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .end local v0    # "child":Landroid/view/View;
    .end local v2    # "extraVisible":Z
    .end local v3    # "i":I
    :cond_1
    move v2, v7

    .line 515
    goto :goto_0

    .restart local v2    # "extraVisible":Z
    :cond_2
    move v4, v7

    .line 518
    goto :goto_1

    .line 528
    .restart local v0    # "child":Landroid/view/View;
    .restart local v3    # "i":I
    :cond_3
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    .line 530
    .local v5, "lp":Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    iget v8, v5, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;->viewType:I

    if-ne v8, v6, :cond_5

    .line 531
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 532
    if-eqz v4, :cond_4

    move v8, v7

    :goto_4
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_4
    move v8, v9

    goto :goto_4

    .line 533
    :cond_5
    iget v8, v5, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;->viewType:I

    if-ne v8, v10, :cond_7

    .line 534
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 535
    if-eqz v2, :cond_6

    move v8, v7

    :goto_5
    invoke-virtual {v0, v8}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    :cond_6
    move v8, v9

    goto :goto_5

    .line 538
    :cond_7
    iget-object v8, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v8, v0}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 539
    invoke-virtual {v0, v7}, Landroid/view/View;->setVisibility(I)V

    goto :goto_3

    .line 543
    .end local v0    # "child":Landroid/view/View;
    .end local v5    # "lp":Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    :cond_8
    return-void
.end method

.method private hasExtraRegion()Z
    .locals 2

    .prologue
    .line 490
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v1, 0x3

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private hasInfoRegion()Z
    .locals 1

    .prologue
    .line 486
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isRegionVisible(I)Z
    .locals 3
    .param p1, "regionVisibility"    # I

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 494
    packed-switch p1, :pswitch_data_0

    move v0, v1

    .line 503
    :cond_0
    :goto_0
    :pswitch_0
    return v0

    .line 498
    :pswitch_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isActivated()Z

    move-result v0

    goto :goto_0

    .line 500
    :pswitch_2
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isActivated()Z

    move-result v2

    if-eqz v2, :cond_1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isSelected()Z

    move-result v2

    if-nez v2, :cond_0

    :cond_1
    move v0, v1

    goto :goto_0

    .line 494
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setInfoViewVisibility(Z)V
    .locals 6
    .param p1, "visible"    # Z

    .prologue
    const/4 v5, 0x2

    const/4 v3, 0x0

    const/16 v4, 0x8

    .line 581
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_4

    .line 584
    if-eqz p1, :cond_0

    .line 585
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 586
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    .line 585
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 589
    .end local v0    # "i":I
    :cond_0
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_1

    .line 590
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 589
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 592
    :cond_1
    const/4 v0, 0x0

    :goto_2
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 593
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    invoke-virtual {v1, v4}, Landroid/view/View;->setVisibility(I)V

    .line 592
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 595
    :cond_2
    const/4 v1, 0x0

    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    .line 610
    .end local v0    # "i":I
    :cond_3
    :goto_3
    return-void

    .line 597
    :cond_4
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    if-ne v1, v5, :cond_7

    .line 599
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    if-ne v1, v5, :cond_5

    .line 600
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->animateInfoHeight(Z)V

    goto :goto_3

    .line 602
    :cond_5
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_4
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    if-ge v0, v1, :cond_3

    .line 603
    iget-object v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    if-eqz p1, :cond_6

    move v2, v3

    :goto_5
    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 602
    add-int/lit8 v0, v0, 0x1

    goto :goto_4

    :cond_6
    move v2, v4

    .line 603
    goto :goto_5

    .line 606
    .end local v0    # "i":I
    :cond_7
    iget v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 608
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->animateInfoAlpha(Z)V

    goto :goto_3
.end method


# virtual methods
.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "p"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 786
    instance-of v0, p1, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    return v0
.end method

.method protected generateDefaultLayoutParams()Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    .locals 2

    .prologue
    const/4 v1, -0x2

    .line 771
    new-instance v0, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    invoke-direct {v0, v1, v1}, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;-><init>(II)V

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->generateDefaultLayoutParams()Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateDefaultLayoutParams()Landroid/widget/FrameLayout$LayoutParams;
    .locals 1

    .prologue
    .line 52
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->generateDefaultLayoutParams()Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 766
    new-instance v0, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;
    .locals 1
    .param p1, "lp"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 777
    instance-of v0, p1, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    if-eqz v0, :cond_0

    .line 778
    new-instance v0, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    check-cast p1, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    .end local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;-><init>(Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;)V

    .line 780
    :goto_0
    return-object v0

    .restart local p1    # "lp":Landroid/view/ViewGroup$LayoutParams;
    :cond_0
    new-instance v0, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;)V

    goto :goto_0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/FrameLayout$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 52
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/BaseCardView;->generateLayoutParams(Landroid/util/AttributeSet;)Landroid/support/v17/leanback/widget/BaseCardView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public getCardType()I
    .locals 1

    .prologue
    .line 249
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    return v0
.end method

.method public getExtraVisibility()I
    .locals 1

    .prologue
    .line 298
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    return v0
.end method

.method public getInfoVisibility()I
    .locals 1

    .prologue
    .line 276
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    return v0
.end method

.method public isSelectedAnimationDelayed()Z
    .locals 1

    .prologue
    .line 221
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mDelaySelectedAnim:Z

    return v0
.end method

.method protected onCreateDrawableState(I)[I
    .locals 7
    .param p1, "extraSpace"    # I

    .prologue
    .line 548
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onCreateDrawableState(I)[I

    move-result-object v4

    .line 549
    .local v4, "s":[I
    array-length v0, v4

    .line 550
    .local v0, "N":I
    const/4 v3, 0x0

    .line 551
    .local v3, "pressed":Z
    const/4 v1, 0x0

    .line 552
    .local v1, "enabled":Z
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v0, :cond_2

    .line 553
    aget v5, v4, v2

    const v6, 0x10100a7

    if-ne v5, v6, :cond_0

    .line 554
    const/4 v3, 0x1

    .line 556
    :cond_0
    aget v5, v4, v2

    const v6, 0x101009e

    if-ne v5, v6, :cond_1

    .line 557
    const/4 v1, 0x1

    .line 552
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 560
    :cond_2
    if-eqz v3, :cond_3

    if-eqz v1, :cond_3

    .line 561
    sget-object v5, Landroid/view/View;->PRESSED_ENABLED_STATE_SET:[I

    .line 567
    :goto_1
    return-object v5

    .line 562
    :cond_3
    if-eqz v3, :cond_4

    .line 563
    sget-object v5, Landroid/support/v17/leanback/widget/BaseCardView;->LB_PRESSED_STATE_SET:[I

    goto :goto_1

    .line 564
    :cond_4
    if-eqz v1, :cond_5

    .line 565
    sget-object v5, Landroid/view/View;->ENABLED_STATE_SET:[I

    goto :goto_1

    .line 567
    :cond_5
    sget-object v5, Landroid/view/View;->EMPTY_STATE_SET:[I

    goto :goto_1
.end method

.method protected onDetachedFromWindow()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 478
    invoke-super {p0}, Landroid/widget/FrameLayout;->onDetachedFromWindow()V

    .line 479
    iget-object v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mAnimationTrigger:Ljava/lang/Runnable;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 480
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->cancelAnimations()V

    .line 481
    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    .line 482
    iput v1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    .line 483
    return-void
.end method

.method protected onLayout(ZIIII)V
    .locals 11
    .param p1, "changed"    # Z
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I

    .prologue
    .line 406
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingTop()I

    move-result v7

    int-to-float v0, v7

    .line 409
    .local v0, "currBottom":F
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_1

    .line 410
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/view/View;

    .line 411
    .local v5, "mainView":Landroid/view/View;
    invoke-virtual {v5}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_0

    .line 412
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v5, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 416
    invoke-virtual {v5}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v0, v7

    .line 409
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 420
    .end local v5    # "mainView":Landroid/view/View;
    :cond_1
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasInfoRegion()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 421
    const/4 v3, 0x0

    .line 422
    .local v3, "infoHeight":F
    const/4 v2, 0x0

    :goto_1
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_2

    .line 423
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v3, v7

    .line 422
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 426
    :cond_2
    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v8, 0x1

    if-ne v7, v8, :cond_7

    .line 428
    sub-float/2addr v0, v3

    .line 429
    const/4 v7, 0x0

    cmpg-float v7, v0, v7

    if-gez v7, :cond_3

    .line 430
    const/4 v0, 0x0

    .line 440
    :cond_3
    :goto_2
    const/4 v2, 0x0

    :goto_3
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_5

    .line 441
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/View;

    .line 442
    .local v4, "infoView":Landroid/view/View;
    invoke-virtual {v4}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_9

    .line 443
    invoke-virtual {v4}, Landroid/view/View;->getMeasuredHeight()I

    move-result v6

    .line 444
    .local v6, "viewHeight":I
    int-to-float v7, v6

    cmpl-float v7, v7, v3

    if-lez v7, :cond_4

    .line 445
    float-to-int v6, v3

    .line 447
    :cond_4
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    int-to-float v10, v6

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v4, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 451
    int-to-float v7, v6

    add-float/2addr v0, v7

    .line 452
    int-to-float v7, v6

    sub-float/2addr v3, v7

    .line 453
    const/4 v7, 0x0

    cmpg-float v7, v3, v7

    if-gtz v7, :cond_9

    .line 459
    .end local v4    # "infoView":Landroid/view/View;
    .end local v6    # "viewHeight":I
    :cond_5
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasExtraRegion()Z

    move-result v7

    if-eqz v7, :cond_a

    .line 460
    const/4 v2, 0x0

    :goto_4
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v7}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_a

    .line 461
    iget-object v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v7, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/view/View;

    .line 462
    .local v1, "extraView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getVisibility()I

    move-result v7

    const/16 v8, 0x8

    if-eq v7, v8, :cond_6

    .line 463
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v7

    float-to-int v8, v0

    iget v9, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v10

    add-int/2addr v9, v10

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v10

    int-to-float v10, v10

    add-float/2addr v10, v0

    float-to-int v10, v10

    invoke-virtual {v1, v7, v8, v9, v10}, Landroid/view/View;->layout(IIII)V

    .line 467
    invoke-virtual {v1}, Landroid/view/View;->getMeasuredHeight()I

    move-result v7

    int-to-float v7, v7

    add-float/2addr v0, v7

    .line 460
    :cond_6
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 432
    .end local v1    # "extraView":Landroid/view/View;
    :cond_7
    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_8

    .line 433
    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    const/4 v8, 0x2

    if-ne v7, v8, :cond_3

    .line 434
    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    mul-float/2addr v3, v7

    goto/16 :goto_2

    .line 437
    :cond_8
    iget v7, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    sub-float/2addr v0, v7

    goto/16 :goto_2

    .line 440
    .restart local v4    # "infoView":Landroid/view/View;
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto/16 :goto_3

    .line 473
    .end local v3    # "infoHeight":F
    .end local v4    # "infoView":Landroid/view/View;
    :cond_a
    const/4 v7, 0x0

    const/4 v8, 0x0

    sub-int v9, p4, p2

    sub-int v10, p5, p3

    invoke-virtual {p0, v7, v8, v9, v10}, Landroid/support/v17/leanback/widget/BaseCardView;->onSizeChanged(IIII)V

    .line 474
    return-void
.end method

.method protected onMeasure(II)V
    .locals 15
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 341
    const/4 v12, 0x0

    iput v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    .line 342
    const/4 v12, 0x0

    iput v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredHeight:I

    .line 343
    const/4 v10, 0x0

    .line 344
    .local v10, "state":I
    const/4 v8, 0x0

    .line 345
    .local v8, "mainHeight":I
    const/4 v6, 0x0

    .line 346
    .local v6, "infoHeight":I
    const/4 v2, 0x0

    .line 348
    .local v2, "extraHeight":I
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->findChildrenViews()V

    .line 350
    const/4 v12, 0x0

    const/4 v13, 0x0

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v11

    .line 352
    .local v11, "unspecifiedSpec":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_1

    .line 353
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMainViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Landroid/view/View;

    .line 354
    .local v9, "mainView":Landroid/view/View;
    invoke-virtual {v9}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_0

    .line 355
    invoke-virtual {p0, v9, v11, v11}, Landroid/support/v17/leanback/widget/BaseCardView;->measureChild(Landroid/view/View;II)V

    .line 356
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    invoke-virtual {v9}, Landroid/view/View;->getMeasuredWidth()I

    move-result v13

    invoke-static {v12, v13}, Ljava/lang/Math;->max(II)I

    move-result v12

    iput v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    .line 357
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v8, v12

    .line 358
    invoke-virtual {v9}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 352
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 361
    .end local v9    # "mainView":Landroid/view/View;
    :cond_1
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    div-int/lit8 v12, v12, 0x2

    int-to-float v12, v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/BaseCardView;->setPivotX(F)V

    .line 362
    div-int/lit8 v12, v8, 0x2

    int-to-float v12, v12

    invoke-virtual {p0, v12}, Landroid/support/v17/leanback/widget/BaseCardView;->setPivotY(F)V

    .line 366
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    const/high16 v13, 0x40000000    # 2.0f

    invoke-static {v12, v13}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 368
    .local v1, "cardWidthMeasureSpec":I
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasInfoRegion()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 369
    const/4 v4, 0x0

    :goto_1
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_4

    .line 370
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Landroid/view/View;

    .line 371
    .local v7, "infoView":Landroid/view/View;
    invoke-virtual {v7}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_3

    .line 372
    invoke-virtual {p0, v7, v1, v11}, Landroid/support/v17/leanback/widget/BaseCardView;->measureChild(Landroid/view/View;II)V

    .line 373
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    const/4 v13, 0x1

    if-eq v12, v13, :cond_2

    .line 374
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v6, v12

    .line 376
    :cond_2
    invoke-virtual {v7}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 369
    :cond_3
    add-int/lit8 v4, v4, 0x1

    goto :goto_1

    .line 380
    .end local v7    # "infoView":Landroid/view/View;
    :cond_4
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasExtraRegion()Z

    move-result v12

    if-eqz v12, :cond_6

    .line 381
    const/4 v4, 0x0

    :goto_2
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v12}, Ljava/util/ArrayList;->size()I

    move-result v12

    if-ge v4, v12, :cond_6

    .line 382
    iget-object v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraViewList:Ljava/util/ArrayList;

    invoke-virtual {v12, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/View;

    .line 383
    .local v3, "extraView":Landroid/view/View;
    invoke-virtual {v3}, Landroid/view/View;->getVisibility()I

    move-result v12

    const/16 v13, 0x8

    if-eq v12, v13, :cond_5

    .line 384
    invoke-virtual {p0, v3, v1, v11}, Landroid/support/v17/leanback/widget/BaseCardView;->measureChild(Landroid/view/View;II)V

    .line 385
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int/2addr v2, v12

    .line 386
    invoke-virtual {v3}, Landroid/view/View;->getMeasuredState()I

    move-result v12

    invoke-static {v10, v12}, Landroid/view/View;->combineMeasuredStates(II)I

    move-result v10

    .line 381
    :cond_5
    add-int/lit8 v4, v4, 0x1

    goto :goto_2

    .line 392
    .end local v3    # "extraView":Landroid/view/View;
    :cond_6
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->hasInfoRegion()Z

    move-result v12

    if-eqz v12, :cond_7

    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    const/4 v13, 0x2

    if-ne v12, v13, :cond_7

    const/4 v5, 0x1

    .line 393
    .local v5, "infoAnimating":Z
    :goto_3
    int-to-float v13, v8

    if-eqz v5, :cond_8

    int-to-float v12, v6

    iget v14, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    mul-float/2addr v12, v14

    :goto_4
    add-float/2addr v12, v13

    int-to-float v13, v2

    add-float/2addr v13, v12

    if-eqz v5, :cond_9

    const/4 v12, 0x0

    :goto_5
    sub-float v12, v13, v12

    float-to-int v12, v12

    iput v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredHeight:I

    .line 398
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredWidth:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingLeft()I

    move-result v13

    add-int/2addr v12, v13

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingRight()I

    move-result v13

    add-int/2addr v12, v13

    move/from16 v0, p1

    invoke-static {v12, v0, v10}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v12

    iget v13, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mMeasuredHeight:I

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingTop()I

    move-result v14

    add-int/2addr v13, v14

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->getPaddingBottom()I

    move-result v14

    add-int/2addr v13, v14

    shl-int/lit8 v14, v10, 0x10

    move/from16 v0, p2

    invoke-static {v13, v0, v14}, Landroid/view/View;->resolveSizeAndState(III)I

    move-result v13

    invoke-virtual {p0, v12, v13}, Landroid/support/v17/leanback/widget/BaseCardView;->setMeasuredDimension(II)V

    .line 402
    return-void

    .line 392
    .end local v5    # "infoAnimating":Z
    :cond_7
    const/4 v5, 0x0

    goto :goto_3

    .line 393
    .restart local v5    # "infoAnimating":Z
    :cond_8
    int-to-float v12, v6

    goto :goto_4

    :cond_9
    iget v12, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoOffset:F

    goto :goto_5
.end method

.method public setActivated(Z)V
    .locals 1
    .param p1, "activated"    # Z

    .prologue
    .line 312
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isActivated()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 313
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setActivated(Z)V

    .line 314
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isActivated()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->applyActiveState(Z)V

    .line 316
    :cond_0
    return-void
.end method

.method public setCardType(I)V
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 230
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    if-eq v0, p1, :cond_0

    .line 231
    if-ltz p1, :cond_1

    const/4 v0, 0x4

    if-ge p1, v0, :cond_1

    .line 233
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    .line 239
    :goto_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->requestLayout()V

    .line 241
    :cond_0
    return-void

    .line 235
    :cond_1
    const-string v0, "BaseCardView"

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid card type specified: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ". Defaulting to type CARD_TYPE_MAIN_ONLY."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 237
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mCardType:I

    goto :goto_0
.end method

.method public setExtraVisibility(I)V
    .locals 1
    .param p1, "visibility"    # I

    .prologue
    .line 288
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    if-eq v0, p1, :cond_0

    .line 289
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mExtraVisibility:I

    .line 290
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->requestLayout()V

    .line 292
    :cond_0
    return-void
.end method

.method public setInfoVisibility(I)V
    .locals 2
    .param p1, "visibility"    # I

    .prologue
    .line 261
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    if-eq v0, p1, :cond_0

    .line 262
    iput p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    .line 263
    iget v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisibility:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isSelected()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 264
    const/high16 v0, 0x3f800000    # 1.0f

    iput v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    .line 268
    :goto_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->requestLayout()V

    .line 270
    :cond_0
    return-void

    .line 266
    :cond_1
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mInfoVisFraction:F

    goto :goto_0
.end method

.method public setSelected(Z)V
    .locals 1
    .param p1, "selected"    # Z

    .prologue
    .line 328
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isSelected()Z

    move-result v0

    if-eq p1, v0, :cond_0

    .line 329
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->setSelected(Z)V

    .line 330
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->isSelected()Z

    move-result v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/BaseCardView;->applySelectedState(Z)V

    .line 332
    :cond_0
    return-void
.end method

.method public setSelectedAnimationDelayed(Z)V
    .locals 0
    .param p1, "delay"    # Z

    .prologue
    .line 209
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/BaseCardView;->mDelaySelectedAnim:Z

    .line 210
    return-void
.end method

.method public shouldDelayChildPressedState()Z
    .locals 1

    .prologue
    .line 336
    const/4 v0, 0x0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 1

    .prologue
    .line 937
    invoke-super {p0}, Landroid/widget/FrameLayout;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
