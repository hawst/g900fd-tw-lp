.class public final Landroid/support/v17/leanback/R$id;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "id"
.end annotation


# static fields
.field public static final background_color:I = 0x7f0f021c

.field public static final background_dim:I = 0x7f0f021f

.field public static final background_imagein:I = 0x7f0f021e

.field public static final background_imageout:I = 0x7f0f021d

.field public static final background_theme:I = 0x7f0f021b

.field public static final bottom_spacer:I = 0x7f0f0164

.field public static final browse_badge:I = 0x7f0f0172

.field public static final browse_container_dock:I = 0x7f0f012a

.field public static final browse_frame:I = 0x7f0f0129

.field public static final browse_grid:I = 0x7f0f0175

.field public static final browse_grid_dock:I = 0x7f0f0176

.field public static final browse_headers:I = 0x7f0f0154

.field public static final browse_headers_dock:I = 0x7f0f012b

.field public static final browse_orb:I = 0x7f0f0174

.field public static final browse_title:I = 0x7f0f0173

.field public static final browse_title_group:I = 0x7f0f012c

.field public static final button:I = 0x7f0f0131

.field public static final container_list:I = 0x7f0f0167

.field public static final content_text:I = 0x7f0f012f

.field public static final control_bar:I = 0x7f0f00f0

.field public static final controls_card:I = 0x7f0f0160

.field public static final controls_dock:I = 0x7f0f0162

.field public static final current_time:I = 0x7f0f015e

.field public static final description:I = 0x7f0f0102

.field public static final description_dock:I = 0x7f0f0161

.field public static final details_frame:I = 0x7f0f013e

.field public static final details_overview:I = 0x7f0f013f

.field public static final details_overview_actions:I = 0x7f0f0143

.field public static final details_overview_description:I = 0x7f0f0142

.field public static final details_overview_image:I = 0x7f0f0140

.field public static final details_overview_right_panel:I = 0x7f0f0141

.field public static final error_frame:I = 0x7f0f0151

.field public static final extra_badge:I = 0x7f0f0157

.field public static final fade_mask:I = 0x7f0f0158

.field public static final fade_out_edge:I = 0x7f0f0155

.field public static final fragment_dock:I = 0x7f0f013d

.field public static final icon:I = 0x7f0f008a

.field public static final image:I = 0x7f0f00a5

.field public static final info_field:I = 0x7f0f012d

.field public static final lb_action_button:I = 0x7f0f0127

.field public static final lb_control_more_actions:I = 0x7f0f0010

.field public static final lb_details_description_body:I = 0x7f0f013a

.field public static final lb_details_description_subtitle:I = 0x7f0f0139

.field public static final lb_details_description_title:I = 0x7f0f0132

.field public static final lb_focus_animator:I = 0x7f0f0009

.field public static final lb_row_container_header_dock:I = 0x7f0f0165

.field public static final lb_search_bar_badge:I = 0x7f0f016a

.field public static final lb_search_bar_items:I = 0x7f0f0169

.field public static final lb_search_bar_speech_orb:I = 0x7f0f0168

.field public static final lb_search_text_editor:I = 0x7f0f016b

.field public static final lb_shadow_focused:I = 0x7f0f0171

.field public static final lb_shadow_normal:I = 0x7f0f0170

.field public static final lb_slide_transition_value:I = 0x7f0f000a

.field public static final main_image:I = 0x7f0f0156

.field public static final message:I = 0x7f0f0152

.field public static final more_actions_dock:I = 0x7f0f015d

.field public static final playback_progress:I = 0x7f0f015b

.field public static final row_content:I = 0x7f0f0159

.field public static final scale_frame:I = 0x7f0f0166

.field public static final search_orb:I = 0x7f0f016f

.field public static final secondary_controls_dock:I = 0x7f0f0163

.field public static final spacer:I = 0x7f0f00e0

.field public static final title:I = 0x7f0f00a8

.field public static final title_text:I = 0x7f0f012e

.field public static final total_time:I = 0x7f0f015f
