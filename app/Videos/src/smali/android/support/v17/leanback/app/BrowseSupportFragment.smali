.class public Landroid/support/v17/leanback/app/BrowseSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "BrowseSupportFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;,
        Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;,
        Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;
    }
.end annotation


# static fields
.field private static final ARG_BADGE_URI:Ljava/lang/String;

.field private static final ARG_HEADERS_STATE:Ljava/lang/String;

.field private static final ARG_TITLE:Ljava/lang/String;

.field private static DEBUG:Z

.field private static sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

.field private mBadgeDrawable:Landroid/graphics/drawable/Drawable;

.field private mBrandColor:I

.field private mBrandColorSet:Z

.field private mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

.field private mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

.field private mCanShowHeaders:Z

.field private mContainerListAlignTop:I

.field private mContainerListMarginLeft:I

.field private mExternalOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

.field private mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

.field private mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mHeadersBackStackEnabled:Z

.field private mHeadersState:I

.field private mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

.field private mHeadersTransition:Ljava/lang/Object;

.field private mHeadersTransitionDuration:I

.field private mHeadersTransitionStartDelay:I

.field private final mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

.field private final mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mReparentHeaderId:I

.field private mRowScaleEnabled:Z

.field private mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

.field private mSceneWithHeaders:Ljava/lang/Object;

.field private mSceneWithTitle:Ljava/lang/Object;

.field private mSceneWithoutHeaders:Ljava/lang/Object;

.field private mSceneWithoutTitle:Ljava/lang/Object;

.field private mSearchAffordanceColorSet:Z

.field private mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

.field private mSelectedPosition:I

.field private final mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;

.field private mShowingHeaders:Z

.field private mShowingTitle:Z

.field private mTitle:Ljava/lang/String;

.field private mTitleDownTransition:Ljava/lang/Object;

.field private mTitleUpTransition:Ljava/lang/Object;

.field private mTitleView:Landroid/support/v17/leanback/widget/TitleView;

.field private mWithHeadersBackStackName:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 161
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z

    .line 207
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->getInstance()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    .line 221
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".title"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_TITLE:Ljava/lang/String;

    .line 222
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".badge"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_BADGE_URI:Ljava/lang/String;

    .line 223
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-class v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v1}, Ljava/lang/Class;->getCanonicalName()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ".headersState"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 73
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 181
    iput v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersState:I

    .line 182
    const/4 v0, 0x0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColor:I

    .line 187
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    .line 188
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersBackStackEnabled:Z

    .line 190
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    .line 191
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    .line 194
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowScaleEnabled:Z

    .line 202
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSelectedPosition:I

    .line 208
    invoke-static {}, Landroid/view/View;->generateViewId()I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mReparentHeaderId:I

    .line 524
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    .line 559
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$3;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$3;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

    .line 831
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

    .line 843
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$10;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$10;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 857
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$11;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$11;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 866
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$12;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$12;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 900
    new-instance v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;

    const/4 v1, 0x0

    invoke-direct {v0, p0, v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;Landroid/support/v17/leanback/app/BrowseSupportFragment$1;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;

    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    return v0
.end method

.method static synthetic access$002(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    return p1
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mWithHeadersBackStackName:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersBackStackEnabled:Z

    return v0
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    return-object v0
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    return v0
.end method

.method static synthetic access$1300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/widget/TitleView;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    return-object v0
.end method

.method static synthetic access$1400()Z
    .locals 1

    .prologue
    .line 73
    sget-boolean v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z

    return v0
.end method

.method static synthetic access$1500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->isVerticalScrolling()Z

    move-result v0

    return v0
.end method

.method static synthetic access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    return-object v0
.end method

.method static synthetic access$1700(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->showHeaders(Z)V

    return-void
.end method

.method static synthetic access$1800(Landroid/support/v17/leanback/app/BrowseSupportFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->onRowSelected(I)V

    return-void
.end method

.method static synthetic access$1900(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->startHeadersTransitionInternal(Z)V

    return-void
.end method

.method static synthetic access$2000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$2100(Landroid/support/v17/leanback/app/BrowseSupportFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # I

    .prologue
    .line 73
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setSelection(I)V

    return-void
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->createHeadersTransition()V

    return-void
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseTransitionListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BrowseTransitionListener;

    return-object v0
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithHeaders:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithoutHeaders:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/Object;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;

    .prologue
    .line 73
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    return-object v0
.end method

.method static synthetic access$802(Landroid/support/v17/leanback/app/BrowseSupportFragment;Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BrowseSupportFragment;
    .param p1, "x1"    # Ljava/lang/Object;

    .prologue
    .line 73
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    return-object p1
.end method

.method static synthetic access$900()Landroid/support/v17/leanback/transition/TransitionHelper;
    .locals 1

    .prologue
    .line 73
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    return-object v0
.end method

.method private createHeadersTransition()V
    .locals 10

    .prologue
    const/4 v9, 0x1

    const/4 v8, 0x0

    .line 737
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v8}, Landroid/support/v17/leanback/transition/TransitionHelper;->createTransitionSet(Z)Ljava/lang/Object;

    move-result-object v5

    iput-object v5, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    .line 738
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    sget v7, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v5, v6, v7, v9}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 739
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v8}, Landroid/support/v17/leanback/transition/TransitionHelper;->createChangeBounds(Z)Ljava/lang/Object;

    move-result-object v0

    .line 740
    .local v0, "changeBounds":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5, v9}, Landroid/support/v17/leanback/transition/TransitionHelper;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v2

    .line 741
    .local v2, "fadeIn":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    const/4 v6, 0x2

    invoke-virtual {v5, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v3

    .line 742
    .local v3, "fadeOut":Ljava/lang/Object;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScale()Ljava/lang/Object;

    move-result-object v4

    .line 743
    .local v4, "scale":Ljava/lang/Object;
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->systemSupportsTransitions()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 744
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getView()Landroid/view/View;

    move-result-object v5

    invoke-virtual {v5}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .line 745
    .local v1, "context":Landroid/content/Context;
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v0, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 747
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v2, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 749
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v3, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 751
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    sget-object v6, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-virtual {v6, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 755
    .end local v1    # "context":Landroid/content/Context;
    :cond_0
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v3, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 756
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v3}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 758
    iget-boolean v5, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    if-eqz v5, :cond_1

    .line 759
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 760
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 762
    :cond_1
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v0, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 763
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v0}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 764
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v6}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v6

    invoke-virtual {v5, v4, v6}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTarget(Ljava/lang/Object;Landroid/view/View;)V

    .line 765
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v4, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 766
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 768
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionDuration:I

    int-to-long v6, v6

    invoke-virtual {v5, v2, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setDuration(Ljava/lang/Object;J)V

    .line 769
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionStartDelay:I

    int-to-long v6, v6

    invoke-virtual {v5, v2, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setStartDelay(Ljava/lang/Object;J)V

    .line 770
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    invoke-virtual {v5, v6, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 772
    sget-object v5, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v6, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    new-instance v7, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;

    invoke-direct {v7, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$8;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    invoke-virtual {v5, v6, v7}, Landroid/support/v17/leanback/transition/TransitionHelper;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 797
    return-void
.end method

.method private isVerticalScrolling()Z
    .locals 1

    .prologue
    .line 518
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->getScrollState()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->getScrollState()I

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onRowSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 876
    iget v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSelectedPosition:I

    if-eq p1, v0, :cond_1

    .line 877
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;

    iput p1, v0, Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;->mPosition:I

    .line 878
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->getHandler()Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSetSelectionRunnable:Landroid/support/v17/leanback/app/BrowseSupportFragment$SetSelectionRunnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 880
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    if-eqz v0, :cond_0

    if-nez p1, :cond_2

    .line 881
    :cond_0
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    if-nez v0, :cond_1

    .line 882
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 883
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    .line 890
    :cond_1
    :goto_0
    return-void

    .line 885
    :cond_2
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    if-eqz v0, :cond_1

    .line 886
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 887
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    goto :goto_0
.end method

.method private readArguments(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "args"    # Landroid/os/Bundle;

    .prologue
    .line 955
    if-nez p1, :cond_1

    .line 964
    :cond_0
    :goto_0
    return-void

    .line 958
    :cond_1
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 959
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_TITLE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setTitle(Ljava/lang/String;)V

    .line 961
    :cond_2
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 962
    sget-object v0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->ARG_HEADERS_STATE:Ljava/lang/String;

    invoke-virtual {p1, v0}, Landroid/os/Bundle;->getInt(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setHeadersState(I)V

    goto :goto_0
.end method

.method private setSelection(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 903
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 904
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setSelectedPosition(I)V

    .line 905
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setSelectedPosition(I)V

    .line 907
    :cond_0
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSelectedPosition:I

    .line 908
    return-void
.end method

.method private showHeaders(Z)V
    .locals 6
    .param p1, "show"    # Z

    .prologue
    const/4 v3, 0x0

    .line 813
    sget-boolean v2, Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z

    if-eqz v2, :cond_0

    const-string v2, "BrowseSupportFragment"

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "showHeaders "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, p1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v4}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 814
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v2, p1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setHeadersEnabled(Z)V

    .line 818
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 819
    .local v0, "containerList":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 820
    .local v1, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListMarginLeft:I

    :goto_0
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 821
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 823
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    .line 824
    invoke-virtual {v0}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .end local v1    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    check-cast v1, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 825
    .restart local v1    # "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    if-eqz p1, :cond_3

    move v2, v3

    :goto_1
    iput v2, v1, Landroid/view/ViewGroup$MarginLayoutParams;->leftMargin:I

    .line 826
    invoke-virtual {v0, v1}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 828
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    if-nez p1, :cond_1

    const/4 v3, 0x1

    :cond_1
    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setExpand(Z)V

    .line 829
    return-void

    :cond_2
    move v2, v3

    .line 820
    goto :goto_0

    .line 825
    :cond_3
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListMarginLeft:I

    neg-int v2, v2

    goto :goto_1
.end method

.method private startHeadersTransitionInternal(Z)V
    .locals 3
    .param p1, "withHeaders"    # Z

    .prologue
    .line 485
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->isDestroyed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 514
    :goto_0
    return-void

    .line 488
    :cond_0
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    .line 489
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    if-nez p1, :cond_1

    const/4 v0, 0x1

    :goto_1
    new-instance v2, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;

    invoke-direct {v2, p0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment$1;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->onExpandTransitionStart(ZLjava/lang/Runnable;)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 292
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method public isInHeadersTransition()Z
    .locals 1

    .prologue
    .line 451
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransition:Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 4
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    .line 606
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onCreate(Landroid/os/Bundle;)V

    .line 607
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    sget-object v2, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme:[I

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentActivity;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 608
    .local v0, "ta":Landroid/content/res/TypedArray;
    sget v1, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme_browseRowsMarginStart:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListMarginLeft:I

    .line 610
    sget v1, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme_browseRowsMarginTop:I

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v1

    float-to-int v1, v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListAlignTop:I

    .line 612
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 614
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$integer;->lb_browse_headers_transition_delay:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionStartDelay:I

    .line 616
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$integer;->lb_browse_headers_transition_duration:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    iput v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersTransitionDuration:I

    .line 619
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->readArguments(Landroid/os/Bundle;)V

    .line 621
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-eqz v1, :cond_0

    .line 622
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersBackStackEnabled:Z

    if-eqz v1, :cond_1

    .line 623
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "lbHeadersBackStack_"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mWithHeadersBackStackName:Ljava/lang/String;

    .line 624
    new-instance v1, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    .line 625
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    invoke-virtual {v1, v2}, Landroid/support/v4/app/FragmentManager;->addOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 626
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->load(Landroid/os/Bundle;)V

    .line 634
    :cond_0
    :goto_0
    return-void

    .line 628
    :cond_1
    if-eqz p1, :cond_0

    .line 629
    const-string v1, "headerShow"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    goto :goto_0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 647
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    if-nez v1, :cond_5

    .line 648
    new-instance v1, Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-direct {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    .line 649
    new-instance v1, Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-direct {v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;-><init>()V

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    .line 650
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_headers_dock:I

    iget-object v5, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    iget-object v5, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v1, v4, v5}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 660
    :goto_0
    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-nez v1, :cond_6

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setHeadersGone(Z)V

    .line 662
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 663
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_0

    .line 664
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 666
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 668
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-boolean v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowScaleEnabled:Z

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->enableRowScaling(Z)V

    .line 669
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 670
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 671
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 672
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setOnHeaderClickedListener(Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;)V

    .line 673
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V

    .line 674
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 676
    sget v1, Landroid/support/v17/leanback/R$layout;->lb_browse_fragment:I

    invoke-virtual {p1, v1, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 678
    .local v0, "root":Landroid/view/View;
    sget v1, Landroid/support/v17/leanback/R$id;->browse_frame:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    .line 679
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->setOnFocusSearchListener(Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;)V

    .line 680
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mOnChildFocusListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->setOnChildFocusListener(Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnChildFocusListener;)V

    .line 682
    sget v1, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/TitleView;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    .line 683
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 684
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 685
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSearchAffordanceColorSet:Z

    if-eqz v1, :cond_1

    .line 686
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 688
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_2

    .line 689
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/TitleView;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 692
    :cond_2
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColorSet:Z

    if-eqz v1, :cond_3

    .line 693
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColor:I

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setBackgroundColor(I)V

    .line 696
    :cond_3
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseSupportFragment$4;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$4;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithTitle:Ljava/lang/Object;

    .line 702
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseSupportFragment$5;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$5;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    .line 708
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseSupportFragment$6;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$6;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithHeaders:Ljava/lang/Object;

    .line 714
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    new-instance v5, Landroid/support/v17/leanback/app/BrowseSupportFragment$7;

    invoke-direct {v5, p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment$7;-><init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V

    invoke-virtual {v1, v4, v5}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSceneWithoutHeaders:Ljava/lang/Object;

    .line 720
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleUp(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    .line 721
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleDown(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    .line 723
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->browse_headers:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 724
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->browse_headers:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 725
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 726
    sget-object v1, Landroid/support/v17/leanback/app/BrowseSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v4, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    sget v5, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {v1, v4, v5, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 728
    if-eqz p3, :cond_4

    .line 729
    const-string v1, "titleShow"

    invoke-virtual {p3, v1}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v1

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    .line 731
    :cond_4
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-boolean v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    if-eqz v2, :cond_7

    :goto_2
    invoke-virtual {v1, v3}, Landroid/support/v17/leanback/widget/TitleView;->setVisibility(I)V

    .line 733
    return-object v0

    .line 654
    .end local v0    # "root":Landroid/view/View;
    :cond_5
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_headers_dock:I

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    .line 656
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    sget v4, Landroid/support/v17/leanback/R$id;->browse_container_dock:I

    invoke-virtual {v1, v4}, Landroid/support/v4/app/FragmentManager;->findFragmentById(I)Landroid/support/v4/app/Fragment;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/app/RowsSupportFragment;

    iput-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    goto/16 :goto_0

    :cond_6
    move v1, v3

    .line 660
    goto/16 :goto_1

    .line 731
    .restart local v0    # "root":Landroid/view/View;
    :cond_7
    const/4 v3, 0x4

    goto :goto_2
.end method

.method public onDestroy()V
    .locals 2

    .prologue
    .line 638
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    if-eqz v0, :cond_0

    .line 639
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentManager;->removeOnBackStackChangedListener(Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;)V

    .line 641
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 642
    return-void
.end method

.method public onSaveInstanceState(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 596
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    if-eqz v0, :cond_0

    .line 597
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBackStackChangedListener:Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->save(Landroid/os/Bundle;)V

    .line 601
    :goto_0
    const-string v0, "titleShow"

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingTitle:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 602
    return-void

    .line 599
    :cond_0
    const-string v0, "headerShow"

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 912
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 913
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListAlignTop:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setWindowAlignmentFromTop(I)V

    .line 914
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setItemAlignment()V

    .line 915
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListAlignTop:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setWindowAlignmentFromTop(I)V

    .line 916
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setItemAlignment()V

    .line 918
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setPivotX(F)V

    .line 919
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mContainerListAlignTop:I

    int-to-float v1, v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setPivotY(F)V

    .line 921
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_2

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    if-eqz v0, :cond_2

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 922
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 927
    :cond_0
    :goto_0
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_1

    .line 928
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->showHeaders(Z)V

    .line 930
    :cond_1
    return-void

    .line 923
    :cond_2
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-eqz v0, :cond_3

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    if-nez v0, :cond_0

    :cond_3
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 925
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 281
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 282
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 284
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 286
    :cond_0
    return-void
.end method

.method public setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 972
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    if-eq v0, p1, :cond_0

    .line 973
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    .line 974
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 975
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 978
    :cond_0
    return-void
.end method

.method public setBrandColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 254
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColor:I

    .line 255
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColorSet:Z

    .line 257
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    if-eqz v0, :cond_0

    .line 258
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mBrandColor:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setBackgroundColor(I)V

    .line 260
    :cond_0
    return-void
.end method

.method public setHeadersState(I)V
    .locals 5
    .param p1, "headersState"    # I

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 1014
    if-lt p1, v0, :cond_0

    const/4 v2, 0x3

    if-le p1, v2, :cond_1

    .line 1015
    :cond_0
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Invalid headers state: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 1017
    :cond_1
    sget-boolean v2, Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z

    if-eqz v2, :cond_2

    const-string v2, "BrowseSupportFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "setHeadersState "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 1019
    :cond_2
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersState:I

    if-eq p1, v2, :cond_3

    .line 1020
    iput p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersState:I

    .line 1021
    packed-switch p1, :pswitch_data_0

    .line 1035
    const-string v2, "BrowseSupportFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unknown headers state: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 1038
    :goto_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    if-eqz v2, :cond_3

    .line 1039
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;

    iget-boolean v3, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    if-nez v3, :cond_4

    :goto_1
    invoke-virtual {v2, v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setHeadersGone(Z)V

    .line 1042
    :cond_3
    return-void

    .line 1023
    :pswitch_0
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    .line 1024
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    goto :goto_0

    .line 1027
    :pswitch_1
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    .line 1028
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    goto :goto_0

    .line 1031
    :pswitch_2
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z

    .line 1032
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z

    goto :goto_0

    :cond_4
    move v0, v1

    .line 1039
    goto :goto_1

    .line 1021
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 310
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 311
    return-void
.end method

.method public setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 380
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    .line 381
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 384
    :cond_0
    return-void
.end method

.method public setSearchAffordanceColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 417
    new-instance v0, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-direct {v0, p1}, Landroid/support/v17/leanback/widget/SearchOrbView$Colors;-><init>(I)V

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 418
    return-void
.end method

.method public setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V
    .locals 2
    .param p1, "colors"    # Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    .prologue
    .line 390
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    .line 391
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSearchAffordanceColorSet:Z

    .line 392
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/TitleView;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 395
    :cond_0
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 993
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitle:Ljava/lang/String;

    .line 994
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 995
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 997
    :cond_0
    return-void
.end method
