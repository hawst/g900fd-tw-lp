.class Landroid/support/v17/leanback/app/RowsFragment$1;
.super Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
.source "RowsFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/RowsFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/RowsFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/RowsFragment;)V
    .locals 0

    .prologue
    .line 365
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V
    .locals 2
    .param p1, "presenter"    # Landroid/support/v17/leanback/widget/Presenter;
    .param p2, "type"    # I

    .prologue
    .line 368
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$000(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V

    move-object v0, p1

    .line 369
    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$100(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 370
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 371
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1, p2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V

    .line 373
    :cond_0
    return-void
.end method

.method public onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 399
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExpand:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$600(Landroid/support/v17/leanback/app/RowsFragment;)Z

    move-result v0

    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$700(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V

    .line 400
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$800(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v0

    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$900(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 401
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$1000(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v0

    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$1100(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 402
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 403
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 405
    :cond_0
    return-void
.end method

.method public onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 418
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 419
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 421
    :cond_0
    return-void
.end method

.method public onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 5
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 376
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 377
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/RowPresenter;->canDrawOutOfBounds()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 378
    invoke-virtual {v0, v3}, Landroid/support/v17/leanback/widget/VerticalGridView;->setClipChildren(Z)V

    .line 380
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    invoke-static {v1, p1}, Landroid/support/v17/leanback/app/RowsFragment;->access$300(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 381
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # setter for: Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z
    invoke-static {v1, v4}, Landroid/support/v17/leanback/app/RowsFragment;->access$402(Landroid/support/v17/leanback/app/RowsFragment;Z)Z

    .line 382
    new-instance v1, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;

    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    invoke-direct {v1, v2, p1}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;-><init>(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    invoke-virtual {p1, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->setExtraObject(Ljava/lang/Object;)V

    .line 386
    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    invoke-static {p1, v3, v4}, Landroid/support/v17/leanback/app/RowsFragment;->access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 387
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 388
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 390
    :cond_1
    return-void
.end method

.method public onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 408
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$1200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-result-object v0

    if-ne v0, p1, :cond_0

    .line 409
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$1200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    # invokes: Landroid/support/v17/leanback/app/RowsFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    invoke-static {v0, v1, v2}, Landroid/support/v17/leanback/app/RowsFragment;->access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 410
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    const/4 v1, 0x0

    # setter for: Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$1202(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 412
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 413
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v0}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 415
    :cond_1
    return-void
.end method

.method public onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 424
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getExtraObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;

    .line 425
    .local v0, "extra":Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;
    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->endAnimations()V

    .line 426
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 427
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment$1;->this$0:Landroid/support/v17/leanback/app/RowsFragment;

    # getter for: Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    invoke-static {v1}, Landroid/support/v17/leanback/app/RowsFragment;->access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    move-result-object v1

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 429
    :cond_0
    return-void
.end method
