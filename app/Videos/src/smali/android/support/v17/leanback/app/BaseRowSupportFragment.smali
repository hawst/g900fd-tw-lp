.class abstract Landroid/support/v17/leanback/app/BaseRowSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "BaseRowSupportFragment.java"


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

.field protected mInTransition:Z

.field private mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private final mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

.field private mSelectedPosition:I

.field private mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 34
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 39
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    .line 45
    new-instance v0, Landroid/support/v17/leanback/app/BaseRowSupportFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment$1;-><init>(Landroid/support/v17/leanback/app/BaseRowSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    return-void
.end method


# virtual methods
.method protected findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 64
    check-cast p1, Landroid/support/v17/leanback/widget/VerticalGridView;

    .end local p1    # "view":Landroid/view/View;
    return-object p1
.end method

.method public final getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 111
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method protected final getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    return-object v0
.end method

.method protected abstract getLayoutResourceId()I
.end method

.method final getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1

    .prologue
    .line 132
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 58
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->getLayoutResourceId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 59
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 60
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 80
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 82
    return-void
.end method

.method protected onRowSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 53
    return-void
.end method

.method onTransitionEnd()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 172
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    .line 173
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAnimateChildLayout(Z)V

    .line 174
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setPruneChild(Z)V

    .line 175
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setFocusSearchDisabled(Z)V

    .line 177
    :cond_0
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mInTransition:Z

    .line 178
    return-void
.end method

.method onTransitionStart()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 163
    iput-boolean v2, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mInTransition:Z

    .line 164
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    .line 165
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAnimateChildLayout(Z)V

    .line 166
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setPruneChild(Z)V

    .line 167
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setFocusSearchDisabled(Z)V

    .line 169
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 69
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    if-eqz v0, :cond_0

    .line 70
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 71
    iget v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 72
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 75
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V

    .line 76
    return-void
.end method

.method public final setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "rowsAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 103
    iput-object p1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 104
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->updateAdapter()V

    .line 105
    return-void
.end method

.method setItemAlignment()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffset(I)V

    .line 184
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffsetPercent(F)V

    .line 187
    :cond_0
    return-void
.end method

.method public final setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 0
    .param p1, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 88
    iput-object p1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 89
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->updateAdapter()V

    .line 90
    return-void
.end method

.method public setSelectedPosition(I)V
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 125
    iput p1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    .line 126
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->getAdapter()Landroid/support/v7/widget/RecyclerView$Adapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 127
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPositionSmooth(I)V

    .line 129
    :cond_0
    return-void
.end method

.method setWindowAlignmentFromTop(I)V
    .locals 2
    .param p1, "alignedTop"    # I

    .prologue
    .line 190
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_0

    .line 192
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 193
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffsetPercent(F)V

    .line 195
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignment(I)V

    .line 197
    :cond_0
    return-void
.end method

.method protected updateAdapter()V
    .locals 3

    .prologue
    .line 136
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .line 138
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v0, :cond_0

    .line 140
    new-instance v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-direct {v0, v1, v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;-><init>(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .line 142
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_1

    .line 143
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 144
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 145
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget v1, p0, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 148
    :cond_1
    return-void
.end method
