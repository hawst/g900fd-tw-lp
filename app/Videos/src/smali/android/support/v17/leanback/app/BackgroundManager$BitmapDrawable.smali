.class Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;
.super Landroid/graphics/drawable/Drawable;
.source "BackgroundManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BackgroundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "BitmapDrawable"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;
    }
.end annotation


# instance fields
.field private mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;


# direct methods
.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 133
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;-><init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V

    .line 134
    return-void
.end method

.method constructor <init>(Landroid/content/res/Resources;Landroid/graphics/Bitmap;Landroid/graphics/Matrix;)V
    .locals 2
    .param p1, "resources"    # Landroid/content/res/Resources;
    .param p2, "bitmap"    # Landroid/graphics/Bitmap;
    .param p3, "matrix"    # Landroid/graphics/Matrix;

    .prologue
    .line 136
    invoke-direct {p0}, Landroid/graphics/drawable/Drawable;-><init>()V

    .line 130
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    .line 137
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iput-object p2, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mBitmap:Landroid/graphics/Bitmap;

    .line 138
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    if-eqz p3, :cond_0

    .end local p3    # "matrix":Landroid/graphics/Matrix;
    :goto_0
    iput-object p3, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mMatrix:Landroid/graphics/Matrix;

    .line 139
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    new-instance v1, Landroid/graphics/Paint;

    invoke-direct {v1}, Landroid/graphics/Paint;-><init>()V

    iput-object v1, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mPaint:Landroid/graphics/Paint;

    .line 140
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mPaint:Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setFilterBitmap(Z)V

    .line 141
    return-void

    .line 138
    .restart local p3    # "matrix":Landroid/graphics/Matrix;
    :cond_0
    new-instance p3, Landroid/graphics/Matrix;

    .end local p3    # "matrix":Landroid/graphics/Matrix;
    invoke-direct {p3}, Landroid/graphics/Matrix;-><init>()V

    goto :goto_0
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 3
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 149
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mBitmap:Landroid/graphics/Bitmap;

    if-nez v0, :cond_0

    .line 153
    :goto_0
    return-void

    .line 152
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mBitmap:Landroid/graphics/Bitmap;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v1, v1, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mMatrix:Landroid/graphics/Matrix;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v2, v2, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1, v2}, Landroid/graphics/Canvas;->drawBitmap(Landroid/graphics/Bitmap;Landroid/graphics/Matrix;Landroid/graphics/Paint;)V

    goto :goto_0
.end method

.method getBitmap()Landroid/graphics/Bitmap;
    .locals 1

    .prologue
    .line 144
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mBitmap:Landroid/graphics/Bitmap;

    return-object v0
.end method

.method public bridge synthetic getConstantState()Landroid/graphics/drawable/Drawable$ConstantState;
    .locals 1

    .prologue
    .line 112
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->getConstantState()Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    move-result-object v0

    return-object v0
.end method

.method public getConstantState()Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    return-object v0
.end method

.method public getOpacity()I
    .locals 1

    .prologue
    .line 157
    const/4 v0, -0x1

    return v0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 162
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0}, Landroid/graphics/Paint;->getAlpha()I

    move-result v0

    if-eq v0, p1, :cond_0

    .line 163
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->mState:Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;

    iget-object v0, v0, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable$ConstantState;->mPaint:Landroid/graphics/Paint;

    invoke-virtual {v0, p1}, Landroid/graphics/Paint;->setAlpha(I)V

    .line 164
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BackgroundManager$BitmapDrawable;->invalidateSelf()V

    .line 166
    :cond_0
    return-void
.end method

.method public setColorFilter(Landroid/graphics/ColorFilter;)V
    .locals 0
    .param p1, "cf"    # Landroid/graphics/ColorFilter;

    .prologue
    .line 171
    return-void
.end method
