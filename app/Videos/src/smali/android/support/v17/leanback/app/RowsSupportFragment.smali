.class public Landroid/support/v17/leanback/app/RowsSupportFragment;
.super Landroid/support/v17/leanback/app/BaseRowSupportFragment;
.source "RowsSupportFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;,
        Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;
    }
.end annotation


# instance fields
.field private mAlignedTop:I

.field private final mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mExpand:Z

.field private mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mInTransition:Z

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mPresenterMapper:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

.field private mRowScaleEnabled:Z

.field private mRowScaleFactor:F

.field private mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

.field mSelectAnimatorDuration:I

.field mSelectAnimatorInterpolator:Landroid/view/animation/Interpolator;

.field private mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

.field private mViewsCreated:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 48
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;-><init>()V

    .line 121
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z

    .line 138
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectAnimatorInterpolator:Landroid/view/animation/Interpolator;

    .line 366
    new-instance v0, Landroid/support/v17/leanback/app/RowsSupportFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/RowsSupportFragment$1;-><init>(Landroid/support/v17/leanback/app/RowsSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 477
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 48
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    return-void
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    return-object v0
.end method

.method static synthetic access$1202(Landroid/support/v17/leanback/app/RowsSupportFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 48
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    return-object p1
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/RowsSupportFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 48
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    return-void
.end method

.method static synthetic access$402(Landroid/support/v17/leanback/app/RowsSupportFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z

    return p1
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 48
    invoke-static {p0, p1, p2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    return-void
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/RowsSupportFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z

    return v0
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 48
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V

    return-void
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/RowsSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsSupportFragment;

    .prologue
    .line 48
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 48
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    return-void
.end method

.method private freezeRows(Z)V
    .locals 7
    .param p1, "freeze"    # Z

    .prologue
    .line 565
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v4

    .line 566
    .local v4, "verticalView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v4, :cond_0

    .line 567
    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 568
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 569
    invoke-virtual {v4, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v6

    invoke-virtual {v4, v6}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 571
    .local v2, "ibvh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/RowPresenter;

    .line 572
    .local v3, "rowPresenter":Landroid/support/v17/leanback/widget/RowPresenter;
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v6

    invoke-virtual {v3, v6}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v5

    .line 573
    .local v5, "vh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    invoke-virtual {v3, v5, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->freeze(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    .line 568
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 576
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v2    # "ibvh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v3    # "rowPresenter":Landroid/support/v17/leanback/widget/RowPresenter;
    .end local v5    # "vh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    :cond_0
    return-void
.end method

.method private needsScale()Z
    .locals 1

    .prologue
    .line 524
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 1
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 358
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 359
    return-void
.end method

.method private static setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 1
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 363
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 364
    return-void
.end method

.method private static setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    .locals 2
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "expanded"    # Z

    .prologue
    .line 346
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setRowViewExpanded(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V

    .line 347
    return-void
.end method

.method private static setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    .locals 3
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "selected"    # Z
    .param p2, "immediate"    # Z

    .prologue
    .line 351
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getExtraObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;

    .line 352
    .local v0, "extra":Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;
    invoke-virtual {v0, p1, p2}, Landroid/support/v17/leanback/app/RowsSupportFragment$RowViewHolderExtra;->animateSelect(ZZ)V

    .line 353
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setRowViewSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V

    .line 354
    return-void
.end method

.method private setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 5
    .param p1, "bridgeVh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 435
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    .line 436
    .local v1, "rowPresenter":Landroid/support/v17/leanback/widget/RowPresenter;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v2

    .line 438
    .local v2, "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    instance-of v4, v2, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    if-eqz v4, :cond_0

    move-object v4, v2

    .line 439
    check-cast v4, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v3

    .line 441
    .local v3, "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    if-nez v4, :cond_1

    .line 442
    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    .line 447
    :goto_0
    check-cast v2, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .end local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v0

    .line 449
    .local v0, "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mPresenterMapper:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 450
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->getPresenterMapper()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mPresenterMapper:Ljava/util/ArrayList;

    .line 455
    .end local v0    # "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    .end local v3    # "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    :cond_0
    :goto_1
    return-void

    .line 444
    .restart local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .restart local v3    # "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    :cond_1
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setRecycledViewPool(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;)V

    goto :goto_0

    .line 452
    .end local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .restart local v0    # "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    :cond_2
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mPresenterMapper:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setPresenterMapper(Ljava/util/ArrayList;)V

    goto :goto_1
.end method

.method private updateRowScaling()V
    .locals 2

    .prologue
    .line 528
    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->needsScale()Z

    move-result v1

    if-eqz v1, :cond_0

    iget v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleFactor:F

    .line 529
    .local v0, "scaleFactor":F
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setLayoutScaleY(F)V

    .line 530
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setScaleY(F)V

    .line 531
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->setScaleX(F)V

    .line 532
    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->updateWindowAlignOffset()V

    .line 533
    return-void

    .line 528
    .end local v0    # "scaleFactor":F
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    goto :goto_0
.end method

.method private updateWindowAlignOffset()V
    .locals 3

    .prologue
    .line 536
    iget v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mAlignedTop:I

    .line 537
    .local v0, "alignOffset":I
    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->needsScale()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 538
    int-to-float v1, v0

    iget v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleFactor:F

    div-float/2addr v1, v2

    const/high16 v2, 0x3f000000    # 0.5f

    add-float/2addr v1, v2

    float-to-int v0, v1

    .line 540
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 541
    return-void
.end method


# virtual methods
.method public enableRowScaling(Z)V
    .locals 0
    .param p1, "enable"    # Z

    .prologue
    .line 261
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleEnabled:Z

    .line 262
    return-void
.end method

.method protected findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 147
    sget v0, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method

.method protected getLayoutResourceId()I
    .locals 1

    .prologue
    .line 288
    sget v0, Landroid/support/v17/leanback/R$layout;->lb_rows_fragment:I

    return v0
.end method

.method getScaleFrameLayout()Landroid/support/v17/leanback/widget/ScaleFrameLayout;
    .locals 1

    .prologue
    .line 342
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    return-object v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 293
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onCreate(Landroid/os/Bundle;)V

    .line 294
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$integer;->lb_browse_rows_anim_duration:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectAnimatorDuration:I

    .line 296
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$fraction;->lb_browse_rows_scale:I

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleFactor:F

    .line 298
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 303
    invoke-super {p0, p1, p2, p3}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 304
    .local v0, "view":Landroid/view/View;
    sget v1, Landroid/support/v17/leanback/R$id;->scale_frame:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    .line 305
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 325
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z

    .line 326
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onDestroyView()V

    .line 327
    return-void
.end method

.method onExpandTransitionStart(ZLjava/lang/Runnable;)V
    .locals 1
    .param p1, "expand"    # Z
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 513
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->onTransitionStart()V

    .line 514
    if-eqz p1, :cond_0

    .line 515
    invoke-interface {p2}, Ljava/lang/Runnable;->run()V

    .line 521
    :goto_0
    return-void

    .line 520
    :cond_0
    new-instance v0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;

    invoke-direct {v0, p0, p2}, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;-><init>(Landroid/support/v17/leanback/app/RowsSupportFragment;Ljava/lang/Runnable;)V

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->execute()V

    goto :goto_0
.end method

.method protected onRowSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 266
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 267
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-nez v0, :cond_1

    .line 284
    :cond_0
    :goto_0
    return-void

    .line 270
    :cond_1
    if-nez p2, :cond_3

    const/4 v1, 0x0

    .line 273
    .local v1, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :goto_1
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eq v2, v1, :cond_0

    .line 276
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eqz v2, :cond_2

    .line 277
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-static {v2, v4, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 279
    :cond_2
    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 280
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eqz v2, :cond_0

    .line 281
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    const/4 v3, 0x1

    invoke-static {v2, v3, v4}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    goto :goto_0

    .line 270
    .end local v1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_3
    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-object v1, v2

    goto :goto_1
.end method

.method onTransitionEnd()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 558
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onTransitionEnd()V

    .line 559
    iput-boolean v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mInTransition:Z

    .line 560
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->needsScale()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 561
    invoke-direct {p0, v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->freezeRows(Z)V

    .line 562
    return-void

    :cond_0
    move v1, v2

    .line 560
    goto :goto_0
.end method

.method onTransitionStart()V
    .locals 3

    .prologue
    const/4 v2, 0x1

    .line 471
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onTransitionStart()V

    .line 472
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRowScaleEnabled:Z

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 473
    iput-boolean v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mInTransition:Z

    .line 474
    invoke-direct {p0, v2}, Landroid/support/v17/leanback/app/RowsSupportFragment;->freezeRows(Z)V

    .line 475
    return-void

    .line 472
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 311
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 314
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$id;->row_content:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentViewId(I)V

    .line 315
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSaveChildrenPolicy(I)V

    .line 317
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->needsScale()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 319
    iput-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    .line 320
    iput-object v2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mPresenterMapper:Ljava/util/ArrayList;

    .line 321
    return-void

    .line 317
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public setExpand(Z)V
    .locals 7
    .param p1, "expand"    # Z

    .prologue
    .line 198
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z

    .line 199
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    .line 200
    .local v2, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_2

    .line 201
    iget-boolean v5, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mInTransition:Z

    if-nez v5, :cond_0

    iget-object v5, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    check-cast v5, Landroid/view/ViewGroup;

    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->needsScale()Z

    move-result v6

    if-nez v6, :cond_1

    const/4 v6, 0x1

    :goto_0
    invoke-virtual {v5, v6}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 202
    :cond_0
    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->updateRowScaling()V

    .line 203
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 205
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 206
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 207
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 208
    .local v3, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-boolean v5, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mExpand:Z

    invoke-static {v3, v5}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V

    .line 205
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 201
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_1
    const/4 v6, 0x0

    goto :goto_0

    .line 211
    :cond_2
    return-void
.end method

.method setItemAlignment()V
    .locals 2

    .prologue
    .line 331
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->setItemAlignment()V

    .line 332
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 333
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffsetWithPadding(Z)V

    .line 335
    :cond_0
    return-void
.end method

.method public setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .prologue
    .line 158
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .line 159
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z

    if-eqz v0, :cond_0

    .line 160
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Item clicked listener must be set before views are created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 163
    :cond_0
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 218
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 219
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    .line 220
    .local v2, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_0

    .line 221
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 222
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 223
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 224
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 226
    .local v3, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-static {v3, v5}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 222
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 229
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 180
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 181
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z

    if-eqz v0, :cond_0

    .line 182
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Item clicked listener must be set before views are created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 185
    :cond_0
    return-void
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 235
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 236
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    .line 237
    .local v2, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_0

    .line 238
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 239
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 240
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 241
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 243
    .local v3, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-static {v3, v5}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 239
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public bridge synthetic setSelectedPosition(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 48
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->setSelectedPosition(I)V

    return-void
.end method

.method setWindowAlignmentFromTop(I)V
    .locals 2
    .param p1, "alignedTop"    # I

    .prologue
    .line 545
    iput p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mAlignedTop:I

    .line 546
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 547
    .local v0, "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    .line 548
    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->updateWindowAlignOffset()V

    .line 550
    const/high16 v1, -0x40800000    # -1.0f

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffsetPercent(F)V

    .line 552
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignment(I)V

    .line 554
    :cond_0
    return-void
.end method

.method protected updateAdapter()V
    .locals 2

    .prologue
    .line 459
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->updateAdapter()V

    .line 460
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 461
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mViewsCreated:Z

    .line 463
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v0

    .line 464
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    if-eqz v0, :cond_0

    .line 465
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment;->mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 467
    :cond_0
    return-void
.end method
