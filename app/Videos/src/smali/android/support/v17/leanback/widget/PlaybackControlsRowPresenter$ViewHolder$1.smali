.class Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;
.super Ljava/lang/Object;
.source "PlaybackControlsRowPresenter.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;)V
    .locals 0

    .prologue
    .line 64
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;->this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBufferedProgressChanged(I)V
    .locals 2
    .param p1, "ms"    # I

    .prologue
    .line 71
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;->this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    # getter for: Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->mPlaybackControlsPresenter:Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->access$000(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;)Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;->this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->mControlsVh:Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->setSecondaryProgress(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V

    .line 72
    return-void
.end method

.method public onCurrentTimeChanged(I)V
    .locals 2
    .param p1, "ms"    # I

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;->this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->this$0:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    # getter for: Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->mPlaybackControlsPresenter:Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->access$000(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;)Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder$1;->this$1:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;->mControlsVh:Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/PlaybackControlsPresenter;->setCurrentTime(Landroid/support/v17/leanback/widget/PlaybackControlsPresenter$ViewHolder;I)V

    .line 68
    return-void
.end method
