.class public abstract Landroid/support/v17/leanback/widget/PresenterSwitcher;
.super Ljava/lang/Object;
.source "PresenterSwitcher.java"


# instance fields
.field private mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

.field private mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

.field private mParent:Landroid/view/ViewGroup;

.field private mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private showView(Z)V
    .locals 1
    .param p1, "show"    # Z

    .prologue
    .line 56
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 57
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {p0, v0, p1}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->showView(Landroid/view/View;Z)V

    .line 59
    :cond_0
    return-void
.end method

.method private switchView(Ljava/lang/Object;)V
    .locals 3
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 62
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-virtual {v1, p1}, Landroid/support/v17/leanback/widget/PresenterSelector;->getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    .line 63
    .local v0, "presenter":Landroid/support/v17/leanback/widget/Presenter;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    if-eq v0, v1, :cond_2

    .line 64
    const/4 v1, 0x0

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->showView(Z)V

    .line 65
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->clear()V

    .line 66
    iput-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 67
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    if-nez v1, :cond_1

    .line 80
    :cond_0
    :goto_0
    return-void

    .line 70
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mParent:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .line 71
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->insertView(Landroid/view/View;)V

    .line 78
    :goto_1
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2, p1}, Landroid/support/v17/leanback/widget/Presenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 79
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {p0, v1}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->onViewSelected(Landroid/view/View;)V

    goto :goto_0

    .line 73
    :cond_2
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    if-eqz v1, :cond_0

    .line 76
    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    goto :goto_1
.end method


# virtual methods
.method public clear()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 98
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    if-eqz v0, :cond_0

    .line 99
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/Presenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 100
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mParent:Landroid/view/ViewGroup;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 101
    iput-object v2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentViewHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .line 102
    iput-object v2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mCurrentPresenter:Landroid/support/v17/leanback/widget/Presenter;

    .line 104
    :cond_0
    return-void
.end method

.method public final getParentViewGroup()Landroid/view/ViewGroup;
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mParent:Landroid/view/ViewGroup;

    return-object v0
.end method

.method public init(Landroid/view/ViewGroup;Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 0
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 37
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->clear()V

    .line 38
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mParent:Landroid/view/ViewGroup;

    .line 39
    iput-object p2, p0, Landroid/support/v17/leanback/widget/PresenterSwitcher;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 40
    return-void
.end method

.method protected abstract insertView(Landroid/view/View;)V
.end method

.method protected onViewSelected(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 88
    return-void
.end method

.method public select(Ljava/lang/Object;)V
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 43
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->switchView(Ljava/lang/Object;)V

    .line 44
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->showView(Z)V

    .line 45
    return-void
.end method

.method protected showView(Landroid/view/View;Z)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "visible"    # Z

    .prologue
    .line 91
    if-eqz p2, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/view/View;->setVisibility(I)V

    .line 92
    return-void

    .line 91
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public unselect()V
    .locals 1

    .prologue
    .line 48
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/PresenterSwitcher;->showView(Z)V

    .line 49
    return-void
.end method
