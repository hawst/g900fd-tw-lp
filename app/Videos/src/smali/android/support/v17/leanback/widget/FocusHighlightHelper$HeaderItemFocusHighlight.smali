.class Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;
.super Ljava/lang/Object;
.source "FocusHighlightHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/FocusHighlightHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/FocusHighlightHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "HeaderItemFocusHighlight"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;
    }
.end annotation


# static fields
.field private static sDuration:I

.field private static sInitialized:Z

.field private static sSelectScale:F


# instance fields
.field private mGridView:Landroid/support/v17/leanback/widget/BaseGridView;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/BaseGridView;)V
    .locals 1
    .param p1, "gridView"    # Landroid/support/v17/leanback/widget/BaseGridView;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 195
    iput-object p1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->mGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    .line 196
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/BaseGridView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->lazyInit(Landroid/content/res/Resources;)V

    .line 197
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;)Landroid/support/v17/leanback/widget/BaseGridView;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;

    .prologue
    .line 188
    iget-object v0, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->mGridView:Landroid/support/v17/leanback/widget/BaseGridView;

    return-object v0
.end method

.method private static lazyInit(Landroid/content/res/Resources;)V
    .locals 1
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 200
    sget-boolean v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sInitialized:Z

    if-nez v0, :cond_0

    .line 201
    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_browse_header_select_scale:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sSelectScale:F

    .line 203
    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_browse_header_select_duration:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sDuration:I

    .line 205
    const/4 v0, 0x1

    sput-boolean v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sInitialized:Z

    .line 207
    :cond_0
    return-void
.end method

.method private viewFocused(Landroid/view/View;Z)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 230
    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    .line 231
    sget v1, Landroid/support/v17/leanback/R$id;->lb_focus_animator:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;

    .line 232
    .local v0, "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    if-nez v0, :cond_0

    .line 233
    new-instance v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;

    .end local v0    # "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    sget v1, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sSelectScale:F

    sget v2, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->sDuration:I

    invoke-direct {v0, p0, p1, v1, v2}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight$HeaderFocusAnimator;-><init>(Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;Landroid/view/View;FI)V

    .line 234
    .restart local v0    # "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    sget v1, Landroid/support/v17/leanback/R$id;->lb_focus_animator:I

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 236
    :cond_0
    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->animateFocus(ZZ)V

    .line 237
    return-void
.end method


# virtual methods
.method public onInitializeView(Landroid/view/View;)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 246
    return-void
.end method

.method public onItemFocused(Landroid/view/View;Z)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 241
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$HeaderItemFocusHighlight;->viewFocused(Landroid/view/View;Z)V

    .line 242
    return-void
.end method
