.class public Landroid/support/v17/leanback/app/VerticalGridSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "VerticalGridSupportFragment.java"


# static fields
.field private static DEBUG:Z

.field private static sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mBadgeDrawable:Landroid/graphics/drawable/Drawable;

.field private mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

.field private mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

.field private mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

.field private mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

.field private final mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private final mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mSceneWithTitle:Ljava/lang/Object;

.field private mSceneWithoutTitle:Ljava/lang/Object;

.field private mSearchAffordanceColorSet:Z

.field private mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

.field private mSelectedPosition:I

.field private mShowingTitle:Z

.field private mTitle:Ljava/lang/String;

.field private mTitleDownTransition:Ljava/lang/Object;

.field private mTitleUpTransition:Ljava/lang/Object;

.field private mTitleView:Landroid/support/v17/leanback/widget/TitleView;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 51
    const/4 v0, 0x0

    sput-boolean v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->DEBUG:Z

    .line 72
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelper;->getInstance()Landroid/support/v17/leanback/transition/TransitionHelper;

    move-result-object v0

    sput-object v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 49
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 64
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSelectedPosition:I

    .line 69
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mShowingTitle:Z

    .line 153
    new-instance v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$1;-><init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 299
    new-instance v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;-><init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    return-object v0
.end method

.method static synthetic access$100()Z
    .locals 1

    .prologue
    .line 49
    sget-boolean v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->DEBUG:Z

    return v0
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/VerticalGridSupportFragment;
    .param p1, "x1"    # I

    .prologue
    .line 49
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->onRowSelected(I)V

    return-void
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/TitleView;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    return-object v0
.end method

.method private onRowSelected(I)V
    .locals 3
    .param p1, "position"    # I

    .prologue
    .line 187
    iget v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSelectedPosition:I

    if-eq p1, v0, :cond_1

    .line 188
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasPreviousViewInSameRow(I)Z

    move-result v0

    if-nez v0, :cond_2

    .line 190
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mShowingTitle:Z

    if-nez v0, :cond_0

    .line 191
    sget-object v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSceneWithTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mShowingTitle:Z

    .line 198
    :cond_0
    :goto_0
    iput p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSelectedPosition:I

    .line 200
    :cond_1
    return-void

    .line 194
    :cond_2
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mShowingTitle:Z

    if-eqz v0, :cond_0

    .line 195
    sget-object v0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 196
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mShowingTitle:Z

    goto :goto_0
.end method

.method private updateAdapter()V
    .locals 3

    .prologue
    .line 391
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 392
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 393
    iget v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSelectedPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 394
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 397
    :cond_0
    return-void
.end method


# virtual methods
.method public getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 150
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method public getGridPresenter()Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 5
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 323
    sget v1, Landroid/support/v17/leanback/R$layout;->lb_vertical_grid_fragment:I

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 326
    .local v0, "root":Landroid/view/ViewGroup;
    sget v1, Landroid/support/v17/leanback/R$id;->browse_frame:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    .line 327
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mBrowseFrame:Landroid/support/v17/leanback/widget/BrowseFrameLayout;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnFocusSearchListener:Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/BrowseFrameLayout;->setOnFocusSearchListener(Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;)V

    .line 329
    sget v1, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/TitleView;

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    .line 330
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 331
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 332
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSearchAffordanceColorSet:Z

    if-eqz v1, :cond_0

    .line 333
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSearchAffordanceColors:Landroid/support/v17/leanback/widget/SearchOrbView$Colors;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/TitleView;->setSearchAffordanceColors(Landroid/support/v17/leanback/widget/SearchOrbView$Colors;)V

    .line 335
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    if-eqz v1, :cond_1

    .line 336
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mExternalOnSearchClickedListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/TitleView;->setOnSearchClickedListener(Landroid/view/View$OnClickListener;)V

    .line 339
    :cond_1
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    new-instance v2, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$3;

    invoke-direct {v2, p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$3;-><init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSceneWithTitle:Ljava/lang/Object;

    .line 345
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    new-instance v2, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$4;

    invoke-direct {v2, p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$4;-><init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V

    invoke-virtual {v1, v0, v2}, Landroid/support/v17/leanback/transition/TransitionHelper;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mSceneWithoutTitle:Ljava/lang/Object;

    .line 351
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleUp(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    .line 352
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-static {v1}, Landroid/support/v17/leanback/app/TitleTransitionHelper;->createTransitionTitleDown(Landroid/support/v17/leanback/transition/TransitionHelper;)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    .line 353
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleUpTransition:Ljava/lang/Object;

    sget v3, Landroid/support/v17/leanback/R$id;->browse_grid_dock:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 354
    sget-object v1, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->sTransitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    iget-object v2, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleDownTransition:Ljava/lang/Object;

    sget v3, Landroid/support/v17/leanback/R$id;->browse_grid_dock:I

    invoke-virtual {v1, v2, v3, v4}, Landroid/support/v17/leanback/transition/TransitionHelper;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 356
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 376
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 377
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    .line 378
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 370
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 371
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    .line 372
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 361
    sget v1, Landroid/support/v17/leanback/R$id;->browse_grid_dock:I

    invoke-virtual {p1, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 362
    .local v0, "gridDock":Landroid/view/ViewGroup;
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    .line 363
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 365
    invoke-direct {p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->updateAdapter()V

    .line 366
    return-void
.end method

.method public setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 142
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 143
    invoke-direct {p0}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->updateAdapter()V

    .line 144
    return-void
.end method

.method public setGridPresenter(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V
    .locals 2
    .param p1, "gridPresenter"    # Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    .prologue
    .line 118
    if-nez p1, :cond_0

    .line 119
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Grid presenter may not be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 121
    :cond_0
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    .line 122
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 123
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    if-eqz v0, :cond_1

    .line 124
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 126
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    if-eqz v0, :cond_2

    .line 127
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridPresenter:Landroid/support/v17/leanback/widget/VerticalGridPresenter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V

    .line 129
    :cond_2
    return-void
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 183
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 184
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 2
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 101
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitle:Ljava/lang/String;

    .line 102
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 103
    iget-object v0, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 105
    :cond_0
    return-void
.end method
