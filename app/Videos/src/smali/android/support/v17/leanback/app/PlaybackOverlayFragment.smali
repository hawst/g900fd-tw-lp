.class public Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
.super Landroid/support/v17/leanback/app/DetailsFragment;
.source "PlaybackOverlayFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;,
        Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    }
.end annotation


# static fields
.field private static START_FADE_OUT:I


# instance fields
.field private final mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mAlignPosition:I

.field private mAnimationTranslateY:I

.field private mBackgroundType:I

.field private mBgAlpha:I

.field private mBgDarkColor:I

.field private mBgFadeInAnimator:Landroid/animation/ValueAnimator;

.field private mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

.field private mBgLightColor:I

.field private mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

.field private mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

.field private mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

.field private mDescriptionFadeOutAnimator:Landroid/animation/ValueAnimator;

.field private mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

.field private final mFadeListener:Landroid/animation/Animator$AnimatorListener;

.field private mFadingEnabled:Z

.field private mFadingStatus:I

.field private final mHandler:Landroid/os/Handler;

.field private mLogAccelerateInterpolator:Landroid/animation/TimeInterpolator;

.field private mLogDecelerateInterpolator:Landroid/animation/TimeInterpolator;

.field private mMajorFadeTranslateY:I

.field private mMinorFadeTranslateY:I

.field private final mObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

.field private final mOnKeyInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;

.field private final mOnMotionInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;

.field private final mOnTouchInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;

.field private mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

.field private mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

.field private mPaddingBottom:I

.field private mResetControlsToPrimaryActionsPending:Z

.field private mRootView:Landroid/view/View;

.field private mShowTimeMs:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 81
    const/4 v0, 0x1

    sput v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    return-void
.end method

.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0x64

    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 53
    invoke-direct {p0}, Landroid/support/v17/leanback/app/DetailsFragment;-><init>()V

    .line 91
    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBackgroundType:I

    .line 98
    iput-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    .line 99
    iput v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    .line 109
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$1;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeListener:Landroid/animation/Animator$AnimatorListener;

    .line 144
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$2;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    .line 153
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$3;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$3;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnTouchInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;

    .line 160
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$4;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$4;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnMotionInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;

    .line 167
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$5;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$5;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnKeyInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;

    .line 331
    new-instance v0, Landroid/support/v17/leanback/animation/LogDecelerateInterpolator;

    invoke-direct {v0, v2, v1}, Landroid/support/v17/leanback/animation/LogDecelerateInterpolator;-><init>(II)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogDecelerateInterpolator:Landroid/animation/TimeInterpolator;

    .line 332
    new-instance v0, Landroid/support/v17/leanback/animation/LogAccelerateInterpolator;

    invoke-direct {v0, v2, v1}, Landroid/support/v17/leanback/animation/LogAccelerateInterpolator;-><init>(II)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogAccelerateInterpolator:Landroid/animation/TimeInterpolator;

    .line 624
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$12;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 675
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$13;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$13;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    .line 681
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->enableVerticalGridAnimations(Z)V

    return-void
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    return v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;I)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setBgAlpha(I)V

    return-void
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getControlRowView()Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAnimationTranslateY:I

    return v0
.end method

.method static synthetic access$1300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mResetControlsToPrimaryActionsPending:Z

    return v0
.end method

.method static synthetic access$1400(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->updateControlsBottomSpace(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    return-void
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->startFadeTimer()V

    return-void
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeCompleteListener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$OnFadeCompleteListener;

    return-object v0
.end method

.method static synthetic access$400(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->resetControlsToPrimaryActions(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    return-void
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    return v0
.end method

.method static synthetic access$502(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;I)I
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # I

    .prologue
    .line 53
    iput p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    return p1
.end method

.method static synthetic access$600()I
    .locals 1

    .prologue
    .line 53
    sget v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    return v0
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    .prologue
    .line 53
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    return v0
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->fade(Z)V

    return-void
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/view/InputEvent;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/PlaybackOverlayFragment;
    .param p1, "x1"    # Landroid/view/InputEvent;

    .prologue
    .line 53
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->onInterceptInputEvent(Landroid/view/InputEvent;)Z

    move-result v0

    return v0
.end method

.method private enableVerticalGridAnimations(Z)V
    .locals 1
    .param p1, "enable"    # Z

    .prologue
    .line 182
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 183
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAnimateChildLayout(Z)V

    .line 185
    :cond_0
    return-void
.end method

.method private fade(Z)V
    .locals 6
    .param p1, "fadeIn"    # Z

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 455
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getView()Landroid/view/View;

    move-result-object v2

    if-nez v2, :cond_1

    .line 506
    :cond_0
    :goto_0
    return-void

    .line 458
    :cond_1
    if-eqz p1, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-eq v2, v3, :cond_0

    :cond_2
    if-nez p1, :cond_3

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-eq v2, v4, :cond_0

    .line 462
    :cond_3
    if-eqz p1, :cond_4

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    const/16 v5, 0xff

    if-eq v2, v5, :cond_0

    :cond_4
    if-nez p1, :cond_5

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    if-eqz v2, :cond_0

    .line 467
    :cond_5
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getSelectedPosition()I

    move-result v2

    if-nez v2, :cond_6

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mMajorFadeTranslateY:I

    :goto_1
    iput v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAnimationTranslateY:I

    .line 470
    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-nez v2, :cond_8

    .line 471
    if-eqz p1, :cond_7

    .line 472
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 473
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 474
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 475
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 498
    :goto_2
    if-eqz p1, :cond_a

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-nez v2, :cond_a

    .line 499
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 500
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_3
    if-ge v1, v0, :cond_a

    .line 501
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v2

    iget v5, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAnimationTranslateY:I

    int-to-float v5, v5

    invoke-virtual {v2, v5}, Landroid/view/View;->setTranslationY(F)V

    .line 500
    add-int/lit8 v1, v1, 0x1

    goto :goto_3

    .line 467
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_6
    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mMinorFadeTranslateY:I

    goto :goto_1

    .line 477
    :cond_7
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 478
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 479
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    .line 480
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->start()V

    goto :goto_2

    .line 483
    :cond_8
    if-eqz p1, :cond_9

    .line 484
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 485
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 486
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 487
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_2

    .line 489
    :cond_9
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 490
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 491
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    .line 492
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2}, Landroid/animation/ValueAnimator;->reverse()V

    goto :goto_2

    .line 505
    :cond_a
    if-eqz p1, :cond_b

    move v2, v3

    :goto_4
    iput v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    goto/16 :goto_0

    :cond_b
    move v2, v4

    goto :goto_4
.end method

.method private getControlRowView()Landroid/view/View;
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 335
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    if-nez v2, :cond_1

    .line 342
    :cond_0
    :goto_0
    return-object v1

    .line 338
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/VerticalGridView;->findViewHolderForPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .line 339
    .local v0, "vh":Landroid/support/v7/widget/RecyclerView$ViewHolder;
    if-eqz v0, :cond_0

    .line 342
    iget-object v1, v0, Landroid/support/v7/widget/RecyclerView$ViewHolder;->itemView:Landroid/view/View;

    goto :goto_0
.end method

.method private static isConsumableKey(Landroid/view/KeyEvent;)Z
    .locals 1
    .param p0, "keyEvent"    # Landroid/view/KeyEvent;

    .prologue
    .line 265
    invoke-virtual {p0}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 266
    const/4 v0, 0x0

    .line 268
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method private static loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "resId"    # I

    .prologue
    .line 309
    invoke-static {p0, p1}, Landroid/animation/AnimatorInflater;->loadAnimator(Landroid/content/Context;I)Landroid/animation/Animator;

    move-result-object v0

    check-cast v0, Landroid/animation/ValueAnimator;

    .line 310
    .local v0, "animator":Landroid/animation/ValueAnimator;
    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->getDuration()J

    move-result-wide v2

    const-wide/16 v4, 0x1

    mul-long/2addr v2, v4

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 311
    return-object v0
.end method

.method private loadBgAnimator()V
    .locals 3

    .prologue
    .line 315
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$6;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$6;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 322
    .local v0, "listener":Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$animator;->lb_playback_bg_fade_in:I

    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeInAnimator:Landroid/animation/ValueAnimator;

    .line 323
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 324
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeInAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 326
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$animator;->lb_playback_bg_fade_out:I

    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 327
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 328
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgFadeOutAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadeListener:Landroid/animation/Animator$AnimatorListener;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 329
    return-void
.end method

.method private loadControlRowAnimator()V
    .locals 4

    .prologue
    .line 346
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$7;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$7;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 355
    .local v0, "listener":Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;
    new-instance v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$8;

    invoke-direct {v1, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$8;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 368
    .local v1, "updateListener":Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$animator;->lb_playback_controls_fade_in:I

    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    .line 370
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 371
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 372
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeInAnimator:Landroid/animation/ValueAnimator;

    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogDecelerateInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 374
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$animator;->lb_playback_controls_fade_out:I

    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 376
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 377
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 378
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mControlRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogAccelerateInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 379
    return-void
.end method

.method private loadDescriptionAnimator()V
    .locals 3

    .prologue
    .line 424
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$11;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$11;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 443
    .local v0, "listener":Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$animator;->lb_playback_description_fade_in:I

    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

    .line 445
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 446
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeInAnimator:Landroid/animation/ValueAnimator;

    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogDecelerateInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v1, v2}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 448
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$animator;->lb_playback_description_fade_out:I

    invoke-static {v1, v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 450
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mDescriptionFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v1, v0}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 451
    return-void
.end method

.method private loadOtherRowAnimator()V
    .locals 4

    .prologue
    .line 382
    new-instance v0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$9;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$9;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)V

    .line 397
    .local v0, "listener":Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;
    new-instance v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;

    invoke-direct {v1, p0, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;-><init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;)V

    .line 410
    .local v1, "updateListener":Landroid/animation/ValueAnimator$AnimatorUpdateListener;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$animator;->lb_playback_controls_fade_in:I

    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    .line 412
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 413
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 414
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeInAnimator:Landroid/animation/ValueAnimator;

    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mLogDecelerateInterpolator:Landroid/animation/TimeInterpolator;

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 416
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getActivity()Landroid/app/Activity;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$animator;->lb_playback_controls_fade_out:I

    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadAnimator(Landroid/content/Context;I)Landroid/animation/ValueAnimator;

    move-result-object v2

    iput-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    .line 418
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v0}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 419
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v2, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 420
    iget-object v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOtherRowFadeOutAnimator:Landroid/animation/ValueAnimator;

    new-instance v3, Landroid/view/animation/AccelerateInterpolator;

    invoke-direct {v3}, Landroid/view/animation/AccelerateInterpolator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 421
    return-void
.end method

.method private onInterceptInputEvent(Landroid/view/InputEvent;)Z
    .locals 3
    .param p1, "event"    # Landroid/view/InputEvent;

    .prologue
    .line 273
    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-nez v2, :cond_2

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    if-nez v2, :cond_2

    const/4 v0, 0x1

    .line 274
    .local v0, "consumeEvent":Z
    :goto_0
    instance-of v2, p1, Landroid/view/KeyEvent;

    if-eqz v2, :cond_3

    .line 275
    if-eqz v0, :cond_0

    move-object v2, p1

    .line 276
    check-cast v2, Landroid/view/KeyEvent;

    invoke-static {v2}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isConsumableKey(Landroid/view/KeyEvent;)Z

    move-result v0

    .line 278
    :cond_0
    check-cast p1, Landroid/view/KeyEvent;

    .end local p1    # "event":Landroid/view/InputEvent;
    invoke-virtual {p1}, Landroid/view/KeyEvent;->getKeyCode()I

    move-result v1

    .line 280
    .local v1, "keyCode":I
    const/4 v2, 0x4

    if-eq v1, v2, :cond_1

    .line 281
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->tickle()V

    .line 286
    .end local v1    # "keyCode":I
    :cond_1
    :goto_1
    return v0

    .line 273
    .end local v0    # "consumeEvent":Z
    .restart local p1    # "event":Landroid/view/InputEvent;
    :cond_2
    const/4 v0, 0x0

    goto :goto_0

    .line 284
    .restart local v0    # "consumeEvent":Z
    :cond_3
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->tickle()V

    goto :goto_1
.end method

.method private resetControlsToPrimaryActions(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/4 v1, 0x0

    .line 188
    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 189
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->findViewHolderForPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object p1

    .end local p1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    check-cast p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 191
    .restart local p1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_0
    if-nez p1, :cond_2

    .line 192
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mResetControlsToPrimaryActionsPending:Z

    .line 198
    :cond_1
    :goto_0
    return-void

    .line 193
    :cond_2
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    instance-of v0, v0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    if-eqz v0, :cond_1

    .line 194
    iput-boolean v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mResetControlsToPrimaryActionsPending:Z

    .line 195
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->showPrimaryActions(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;)V

    goto :goto_0
.end method

.method private setBgAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 175
    iput p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    .line 176
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 179
    :cond_0
    return-void
.end method

.method private static setBottomPadding(Landroid/view/View;I)V
    .locals 3
    .param p0, "view"    # Landroid/view/View;
    .param p1, "padding"    # I

    .prologue
    .line 542
    invoke-virtual {p0}, Landroid/view/View;->getPaddingLeft()I

    move-result v0

    invoke-virtual {p0}, Landroid/view/View;->getPaddingTop()I

    move-result v1

    invoke-virtual {p0}, Landroid/view/View;->getPaddingRight()I

    move-result v2

    invoke-virtual {p0, v0, v1, v2, p1}, Landroid/view/View;->setPadding(IIII)V

    .line 544
    return-void
.end method

.method private startFadeTimer()V
    .locals 4

    .prologue
    .line 302
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    if-eqz v0, :cond_0

    .line 303
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    sget v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 304
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    sget v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    iget v2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mShowTimeMs:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 306
    :cond_0
    return-void
.end method

.method private updateBackground()V
    .locals 3

    .prologue
    .line 599
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 600
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgDarkColor:I

    .line 601
    .local v0, "color":I
    iget v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBackgroundType:I

    packed-switch v1, :pswitch_data_0

    .line 606
    :goto_0
    :pswitch_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    new-instance v2, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v2, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 608
    .end local v0    # "color":I
    :cond_0
    return-void

    .line 603
    .restart local v0    # "color":I
    :pswitch_1
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgLightColor:I

    goto :goto_0

    .line 604
    :pswitch_2
    const/4 v0, 0x0

    goto :goto_0

    .line 601
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private updateControlsBottomSpace(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 5
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 612
    if-nez p1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 613
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1, v3}, Landroid/support/v17/leanback/widget/VerticalGridView;->findViewHolderForPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object p1

    .end local p1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    check-cast p1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 616
    .restart local p1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_0
    if-eqz p1, :cond_2

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    instance-of v1, v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    if-eqz v1, :cond_2

    .line 617
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v1

    if-nez v1, :cond_3

    move v0, v3

    .line 618
    .local v0, "adapterSize":I
    :goto_0
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;

    if-le v0, v4, :cond_1

    move v3, v4

    :cond_1
    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;->showBottomSpace(Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter$ViewHolder;Z)V

    .line 622
    .end local v0    # "adapterSize":I
    :cond_2
    return-void

    .line 617
    :cond_3
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public onCreate(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 548
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/DetailsFragment;->onCreate(Landroid/os/Bundle;)V

    .line 550
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_controls_align_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAlignPosition:I

    .line 552
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_controls_padding_bottom:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mPaddingBottom:I

    .line 554
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$color;->lb_playback_controls_background_dark:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgDarkColor:I

    .line 556
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$color;->lb_playback_controls_background_light:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgLightColor:I

    .line 558
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$integer;->lb_playback_controls_show_time_ms:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mShowTimeMs:I

    .line 560
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_major_fade_translate_y:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mMajorFadeTranslateY:I

    .line 562
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_playback_minor_fade_translate_y:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mMinorFadeTranslateY:I

    .line 565
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadBgAnimator()V

    .line 566
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadControlRowAnimator()V

    .line 567
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadOtherRowAnimator()V

    .line 568
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadDescriptionAnimator()V

    .line 569
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 662
    invoke-super {p0, p1, p2, p3}, Landroid/support/v17/leanback/app/DetailsFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    .line 663
    const/16 v0, 0xff

    iput v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mBgAlpha:I

    .line 664
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->updateBackground()V

    .line 665
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getRowsFragment()Landroid/support/v17/leanback/app/RowsFragment;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsFragment;->setExternalAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 666
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 671
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mRootView:Landroid/view/View;

    .line 672
    invoke-super {p0}, Landroid/support/v17/leanback/app/DetailsFragment;->onDestroyView()V

    .line 673
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 291
    invoke-super {p0}, Landroid/support/v17/leanback/app/DetailsFragment;->onResume()V

    .line 292
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    if-eqz v0, :cond_0

    .line 293
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setBgAlpha(I)V

    .line 294
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->fade(Z)V

    .line 296
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnTouchInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnTouchInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnTouchInterceptListener;)V

    .line 297
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnMotionInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnMotionInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnMotionInterceptListener;)V

    .line 298
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mOnKeyInterceptListener:Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnKeyInterceptListener(Landroid/support/v17/leanback/widget/BaseGridView$OnKeyInterceptListener;)V

    .line 299
    return-void
.end method

.method public setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 513
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 514
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->unregisterObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 516
    :cond_0
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/DetailsFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 517
    if-eqz p1, :cond_1

    .line 518
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 520
    :cond_1
    return-void
.end method

.method public setFadingEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 209
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    if-eq p1, v0, :cond_0

    .line 210
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    .line 211
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isResumed()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 212
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    if-eqz v0, :cond_1

    .line 213
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingStatus:I

    if-nez v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    sget v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    .line 214
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->startFadeTimer()V

    .line 223
    :cond_0
    :goto_0
    return-void

    .line 218
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    sget v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 219
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->fade(Z)V

    goto :goto_0
.end method

.method setVerticalGridViewLayout(Landroid/support/v17/leanback/widget/VerticalGridView;)V
    .locals 2
    .param p1, "listview"    # Landroid/support/v17/leanback/widget/VerticalGridView;

    .prologue
    const/high16 v1, 0x42c80000    # 100.0f

    .line 524
    if-nez p1, :cond_0

    .line 539
    :goto_0
    return-void

    .line 529
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mPaddingBottom:I

    invoke-static {p1, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->setBottomPadding(Landroid/view/View;I)V

    .line 532
    iget v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAlignPosition:I

    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffset(I)V

    .line 533
    invoke-virtual {p1, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentOffsetPercent(F)V

    .line 536
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffset(I)V

    .line 537
    invoke-virtual {p1, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignmentOffsetPercent(F)V

    .line 538
    const/4 v0, 0x2

    invoke-virtual {p1, v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->setWindowAlignment(I)V

    goto :goto_0
.end method

.method public tickle()V
    .locals 2

    .prologue
    .line 253
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mFadingEnabled:Z

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->isResumed()Z

    move-result v0

    if-nez v0, :cond_1

    .line 262
    :cond_0
    :goto_0
    return-void

    .line 256
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mHandler:Landroid/os/Handler;

    sget v1, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->START_FADE_OUT:I

    invoke-virtual {v0, v1}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 258
    invoke-direct {p0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->startFadeTimer()V

    goto :goto_0

    .line 260
    :cond_2
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->fade(Z)V

    goto :goto_0
.end method
