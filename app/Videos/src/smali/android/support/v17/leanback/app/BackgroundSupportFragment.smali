.class public final Landroid/support/v17/leanback/app/BackgroundSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "BackgroundSupportFragment.java"


# instance fields
.field private mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;
    .locals 1

    .prologue
    .line 32
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 48
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 52
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    if-eqz v0, :cond_0

    .line 53
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->detach()V

    .line 55
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 37
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 41
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    if-eqz v0, :cond_0

    .line 42
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->onActivityResume()V

    .line 44
    :cond_0
    return-void
.end method

.method setBackgroundManager(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0
    .param p1, "backgroundManager"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 28
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundSupportFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    .line 29
    return-void
.end method
