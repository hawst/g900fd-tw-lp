.class public final Landroid/support/v17/leanback/app/BackgroundFragment;
.super Landroid/app/Fragment;
.source "BackgroundFragment.java"


# instance fields
.field private mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    return-void
.end method


# virtual methods
.method getBackgroundManager()Landroid/support/v17/leanback/app/BackgroundManager;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    return-object v0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 46
    invoke-super {p0}, Landroid/app/Fragment;->onDestroy()V

    .line 50
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    if-eqz v0, :cond_0

    .line 51
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->detach()V

    .line 53
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 35
    invoke-super {p0}, Landroid/app/Fragment;->onResume()V

    .line 39
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    if-eqz v0, :cond_0

    .line 40
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->onActivityResume()V

    .line 42
    :cond_0
    return-void
.end method

.method setBackgroundManager(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0
    .param p1, "backgroundManager"    # Landroid/support/v17/leanback/app/BackgroundManager;

    .prologue
    .line 26
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundFragment;->mBackgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    .line 27
    return-void
.end method
