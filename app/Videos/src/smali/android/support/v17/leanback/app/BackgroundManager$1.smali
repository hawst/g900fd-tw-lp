.class Landroid/support/v17/leanback/app/BackgroundManager$1;
.super Ljava/lang/Object;
.source "BackgroundManager.java"

# interfaces
.implements Landroid/animation/Animator$AnimatorListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BackgroundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BackgroundManager;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BackgroundManager;)V
    .locals 0

    .prologue
    .line 733
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$1;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 749
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 1
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 742
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$1;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$100(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 744
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$1;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$100(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->run()V

    .line 746
    :cond_0
    return-void
.end method

.method public onAnimationRepeat(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 739
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 736
    return-void
.end method
