.class public abstract Landroid/support/v17/leanback/widget/RowPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "RowPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;,
        Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;
    }
.end annotation


# instance fields
.field private mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field mSelectEffectEnabled:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 73
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 155
    new-instance v0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    .line 161
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mSelectEffectEnabled:Z

    return-void
.end method

.method private updateHeaderViewVisibility(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 286
    iget-object v1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    if-eqz v1, :cond_0

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v1, :cond_0

    .line 287
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mContainerViewHolder:Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    iget-object v0, v1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;->view:Landroid/view/View;

    check-cast v0, Landroid/support/v17/leanback/widget/RowContainerView;

    .line 288
    .local v0, "containerView":Landroid/support/v17/leanback/widget/RowContainerView;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->isExpanded()Z

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowContainerView;->showHeader(Z)V

    .line 290
    .end local v0    # "containerView":Landroid/support/v17/leanback/widget/RowContainerView;
    :cond_0
    return-void
.end method


# virtual methods
.method public canDrawOutOfBounds()Z
    .locals 1

    .prologue
    .line 371
    const/4 v0, 0x0

    return v0
.end method

.method protected abstract createRowViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
.end method

.method public freeze(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 0
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "freeze"    # Z

    .prologue
    .line 495
    return-void
.end method

.method public final getHeaderPresenter()Landroid/support/v17/leanback/widget/RowHeaderPresenter;
    .locals 1

    .prologue
    .line 218
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    return-object v0
.end method

.method public final getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;
    .locals 1

    .prologue
    .line 454
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    return-object v0
.end method

.method public final getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1

    .prologue
    .line 434
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method public final getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1

    .prologue
    .line 488
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method public final getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1

    .prologue
    .line 470
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method public final getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 226
    instance-of v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    if-eqz v0, :cond_0

    .line 227
    check-cast p1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    .end local p1    # "holder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    iget-object p1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;->mRowViewHolder:Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .line 229
    :goto_0
    return-object p1

    .restart local p1    # "holder":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :cond_0
    check-cast p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    goto :goto_0
.end method

.method public final getSelectEffectEnabled()Z
    .locals 1

    .prologue
    .line 347
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mSelectEffectEnabled:Z

    return v0
.end method

.method public final getSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)F
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 308
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    iget v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mSelectLevel:F

    return v0
.end method

.method protected initializeRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 202
    const/4 v0, 0x1

    iput-boolean v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mInitialzed:Z

    .line 203
    return-void
.end method

.method public isUsingDefaultSelectEffect()Z
    .locals 1

    .prologue
    .line 356
    const/4 v0, 0x1

    return v0
.end method

.method final needsDefaultSelectEffect()Z
    .locals 1

    .prologue
    .line 360
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->isUsingDefaultSelectEffect()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->getSelectEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final needsRowContainerView()Z
    .locals 1

    .prologue
    .line 364
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    if-nez v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->needsDefaultSelectEffect()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 380
    move-object v0, p2

    check-cast v0, Landroid/support/v17/leanback/widget/Row;

    iput-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    .line 381
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 382
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-virtual {v0, v1, p2}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 384
    :cond_0
    return-void
.end method

.method public final onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 376
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    invoke-virtual {p0, v0, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    .line 377
    return-void
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 165
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->createRowViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v2

    .line 166
    .local v2, "vh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    const/4 v3, 0x0

    iput-boolean v3, v2, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mInitialzed:Z

    .line 168
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->needsRowContainerView()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 169
    new-instance v0, Landroid/support/v17/leanback/widget/RowContainerView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v3

    invoke-direct {v0, v3}, Landroid/support/v17/leanback/widget/RowContainerView;-><init>(Landroid/content/Context;)V

    .line 170
    .local v0, "containerView":Landroid/support/v17/leanback/widget/RowContainerView;
    iget-object v3, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    if-eqz v3, :cond_0

    .line 171
    iget-object v4, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v3, v2, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    check-cast v3, Landroid/view/ViewGroup;

    invoke-virtual {v4, v3}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    iput-object v3, v2, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    .line 174
    :cond_0
    new-instance v1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    invoke-direct {v1, v0, v2}, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;-><init>(Landroid/support/v17/leanback/widget/RowContainerView;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 178
    .end local v0    # "containerView":Landroid/support/v17/leanback/widget/RowContainerView;
    .local v1, "result":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :goto_0
    invoke-virtual {p0, v2}, Landroid/support/v17/leanback/widget/RowPresenter;->initializeRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 179
    iget-boolean v3, v2, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mInitialzed:Z

    if-nez v3, :cond_2

    .line 180
    new-instance v3, Ljava/lang/RuntimeException;

    const-string v4, "super.initializeRowViewHolder() must be called"

    invoke-direct {v3, v4}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 176
    .end local v1    # "result":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    :cond_1
    move-object v1, v2

    .restart local v1    # "result":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    goto :goto_0

    .line 182
    :cond_2
    return-object v1
.end method

.method protected onRowViewAttachedToWindow(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 404
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 405
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onViewAttachedToWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 407
    :cond_0
    return-void
.end method

.method protected onRowViewDetachedFromWindow(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 415
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 416
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onViewDetachedFromWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 418
    :cond_0
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-static {v0}, Landroid/support/v17/leanback/widget/RowPresenter;->cancelAnimationsRecursive(Landroid/view/View;)V

    .line 419
    return-void
.end method

.method protected onRowViewExpanded(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "expanded"    # Z

    .prologue
    .line 264
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->updateHeaderViewVisibility(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 265
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v0, p2}, Landroid/view/View;->setActivated(Z)V

    .line 266
    return-void
.end method

.method protected onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "selected"    # Z

    .prologue
    const/4 v2, 0x0

    .line 274
    if-eqz p2, :cond_1

    .line 275
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    if-eqz v0, :cond_0

    .line 276
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->getRow()Landroid/support/v17/leanback/widget/Row;

    move-result-object v1

    invoke-interface {v0, v2, v2, p1, v1}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 278
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    if-eqz v0, :cond_1

    .line 279
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->getRow()Landroid/support/v17/leanback/widget/Row;

    move-result-object v1

    invoke-interface {v0, v2, v1}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 282
    :cond_1
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->updateHeaderViewVisibility(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 283
    return-void
.end method

.method protected onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 3
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 320
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->getSelectEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 321
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mColorDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    iget v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mSelectLevel:F

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->setActiveLevel(F)V

    .line 322
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 323
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    iget v2, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mSelectLevel:F

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->setSelectLevel(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;F)V

    .line 325
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/RowPresenter;->isUsingDefaultSelectEffect()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 326
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mContainerViewHolder:Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;->view:Landroid/view/View;

    check-cast v0, Landroid/support/v17/leanback/widget/RowContainerView;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mColorDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v1}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->getPaint()Landroid/graphics/Paint;

    move-result-object v1

    invoke-virtual {v1}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowContainerView;->setForegroundColor(I)V

    .line 330
    :cond_1
    return-void
.end method

.method protected onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 392
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    if-eqz v0, :cond_0

    .line 393
    iget-object v0, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mHeaderViewHolder:Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 395
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    .line 396
    return-void
.end method

.method public final onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 1
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 388
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/RowPresenter;->onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 389
    return-void
.end method

.method public final onViewAttachedToWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 400
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewAttachedToWindow(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 401
    return-void
.end method

.method public final onViewDetachedFromWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 411
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewDetachedFromWindow(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 412
    return-void
.end method

.method public final setHeaderPresenter(Landroid/support/v17/leanback/widget/RowHeaderPresenter;)V
    .locals 0
    .param p1, "headerPresenter"    # Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    .prologue
    .line 210
    iput-object p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mHeaderPresenter:Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    .line 211
    return-void
.end method

.method public final setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .prologue
    .line 447
    iput-object p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .line 448
    return-void
.end method

.method public final setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 427
    iput-object p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 428
    return-void
.end method

.method public final setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 481
    iput-object p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 482
    return-void
.end method

.method public final setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 463
    iput-object p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 464
    return-void
.end method

.method public final setRowViewExpanded(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "expanded"    # Z

    .prologue
    .line 240
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    .line 241
    .local v0, "rowViewHolder":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    iput-boolean p2, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mExpanded:Z

    .line 242
    invoke-virtual {p0, v0, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewExpanded(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    .line 243
    return-void
.end method

.method public final setRowViewSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "selected"    # Z

    .prologue
    .line 252
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    .line 253
    .local v0, "rowViewHolder":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    iput-boolean p2, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mSelected:Z

    .line 254
    invoke-virtual {p0, v0, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    .line 255
    return-void
.end method

.method public final setSelectEffectEnabled(Z)V
    .locals 0
    .param p1, "applyDimOnSelect"    # Z

    .prologue
    .line 338
    iput-boolean p1, p0, Landroid/support/v17/leanback/widget/RowPresenter;->mSelectEffectEnabled:Z

    .line 339
    return-void
.end method

.method public final setSelectLevel(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;F)V
    .locals 1
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "level"    # F

    .prologue
    .line 298
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v0

    .line 299
    .local v0, "rowViewHolder":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    iput p2, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->mSelectLevel:F

    .line 300
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/RowPresenter;->onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 301
    return-void
.end method
