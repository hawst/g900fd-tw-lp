.class public Landroid/support/v17/leanback/widget/ListRow;
.super Landroid/support/v17/leanback/widget/Row;
.source "ListRow.java"


# instance fields
.field private final mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "header"    # Landroid/support/v17/leanback/widget/HeaderItem;
    .param p2, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 31
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/Row;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;)V

    .line 32
    iput-object p2, p0, Landroid/support/v17/leanback/widget/ListRow;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 33
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/ListRow;->verify()V

    .line 34
    return-void
.end method

.method private verify()V
    .locals 2

    .prologue
    .line 49
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRow;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-nez v0, :cond_0

    .line 50
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "ObjectAdapter cannot be null"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 52
    :cond_0
    return-void
.end method


# virtual methods
.method public final getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ListRow;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method
