.class public Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
.super Landroid/support/v7/widget/RecyclerView$Adapter;
.source "ItemBridgeAdapter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;,
        Landroid/support/v17/leanback/widget/ItemBridgeAdapter$OnFocusChangeListener;,
        Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;,
        Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    }
.end annotation


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

.field private mFocusHighlight:Landroid/support/v17/leanback/widget/FocusHighlightHandler;

.field private mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private mPresenters:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 165
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    .line 137
    new-instance v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$1;-><init>(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    .line 166
    return-void
.end method

.method public constructor <init>(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 1
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;
    .param p2, "presenterSelector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 156
    invoke-direct {p0}, Landroid/support/v7/widget/RecyclerView$Adapter;-><init>()V

    .line 63
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    .line 137
    new-instance v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$1;-><init>(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    .line 157
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 158
    iput-object p2, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 159
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;)Landroid/support/v17/leanback/widget/FocusHighlightHandler;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .prologue
    .line 27
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mFocusHighlight:Landroid/support/v17/leanback/widget/FocusHighlightHandler;

    return-object v0
.end method


# virtual methods
.method public clear()V
    .locals 1

    .prologue
    .line 197
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 198
    return-void
.end method

.method public getItemCount()I
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v0

    return v0
.end method

.method public getItemId(I)J
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 314
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->getId(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getItemViewType(I)I
    .locals 5
    .param p1, "position"    # I

    .prologue
    .line 215
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v4, :cond_1

    iget-object v2, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 217
    .local v2, "presenterSelector":Landroid/support/v17/leanback/widget/PresenterSelector;
    :goto_0
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v4, p1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 218
    .local v0, "item":Ljava/lang/Object;
    invoke-virtual {v2, v0}, Landroid/support/v17/leanback/widget/PresenterSelector;->getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    .line 219
    .local v1, "presenter":Landroid/support/v17/leanback/widget/Presenter;
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 220
    .local v3, "type":I
    if-gez v3, :cond_0

    .line 221
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 222
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    invoke-virtual {v4, v1}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v3

    .line 224
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v4, :cond_0

    .line 225
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v4, v1, v3}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAddPresenter(Landroid/support/v17/leanback/widget/Presenter;I)V

    .line 228
    :cond_0
    return v3

    .line 215
    .end local v0    # "item":Ljava/lang/Object;
    .end local v1    # "presenter":Landroid/support/v17/leanback/widget/Presenter;
    .end local v2    # "presenterSelector":Landroid/support/v17/leanback/widget/PresenterSelector;
    .end local v3    # "type":I
    :cond_1
    iget-object v4, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/ObjectAdapter;->getPresenterSelector()Landroid/support/v17/leanback/widget/PresenterSelector;

    move-result-object v2

    goto :goto_0
.end method

.method public getPresenterMapper()Ljava/util/ArrayList;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter;",
            ">;"
        }
    .end annotation

    .prologue
    .line 205
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    return-object v0
.end method

.method public onBindViewHolder(Landroid/support/v7/widget/RecyclerView$ViewHolder;I)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .param p2, "position"    # I

    .prologue
    .line 272
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 273
    .local v0, "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1, p2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v1

    iput-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    .line 275
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-virtual {v1, v2, v3}, Landroid/support/v17/leanback/widget/Presenter;->onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V

    .line 277
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v1, :cond_0

    .line 278
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onBind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 280
    :cond_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;I)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 7
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "viewType"    # I

    .prologue
    .line 239
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    invoke-virtual {v5, p2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/Presenter;

    .line 242
    .local v0, "presenter":Landroid/support/v17/leanback/widget/Presenter;
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    if-eqz v5, :cond_3

    .line 243
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    invoke-virtual {v5, p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;->createWrapper(Landroid/view/View;)Landroid/view/View;

    move-result-object v3

    .line 244
    .local v3, "view":Landroid/view/View;
    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/Presenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    .line 245
    .local v1, "presenterVh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    iget-object v6, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v5, v3, v6}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;->wrap(Landroid/view/View;Landroid/view/View;)V

    .line 250
    :goto_0
    new-instance v4, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-direct {v4, p0, v0, v3, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;-><init>(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;Landroid/support/v17/leanback/widget/Presenter;Landroid/view/View;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 251
    .local v4, "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v5, :cond_0

    .line 252
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v5, v4}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onCreate(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 254
    :cond_0
    iget-object v5, v4, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v2, v5, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    .line 255
    .local v2, "presenterView":Landroid/view/View;
    if-eqz v2, :cond_1

    .line 256
    iget-object v5, v4, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mFocusChangeListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$OnFocusChangeListener;

    invoke-virtual {v2}, Landroid/view/View;->getOnFocusChangeListener()Landroid/view/View$OnFocusChangeListener;

    move-result-object v6

    iput-object v6, v5, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$OnFocusChangeListener;->mChainedListener:Landroid/view/View$OnFocusChangeListener;

    .line 257
    iget-object v5, v4, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mFocusChangeListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$OnFocusChangeListener;

    invoke-virtual {v2, v5}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 259
    :cond_1
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mFocusHighlight:Landroid/support/v17/leanback/widget/FocusHighlightHandler;

    if-eqz v5, :cond_2

    .line 260
    iget-object v5, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mFocusHighlight:Landroid/support/v17/leanback/widget/FocusHighlightHandler;

    invoke-interface {v5, v3}, Landroid/support/v17/leanback/widget/FocusHighlightHandler;->onInitializeView(Landroid/view/View;)V

    .line 262
    :cond_2
    return-object v4

    .line 247
    .end local v1    # "presenterVh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .end local v2    # "presenterView":Landroid/view/View;
    .end local v3    # "view":Landroid/view/View;
    .end local v4    # "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_3
    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/Presenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    .line 248
    .restart local v1    # "presenterVh":Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    iget-object v3, v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    .restart local v3    # "view":Landroid/view/View;
    goto :goto_0
.end method

.method public onViewAttachedToWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 296
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 297
    .local v0, "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v1, :cond_0

    .line 298
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onAttachedToWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 300
    :cond_0
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onViewAttachedToWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 301
    return-void
.end method

.method public onViewDetachedFromWindow(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 305
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 306
    .local v0, "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onViewDetachedFromWindow(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 307
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v1, :cond_0

    .line 308
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onDetachedFromWindow(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 310
    :cond_0
    return-void
.end method

.method public onViewRecycled(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v7/widget/RecyclerView$ViewHolder;

    .prologue
    .line 284
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 285
    .local v0, "viewHolder":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mPresenter:Landroid/support/v17/leanback/widget/Presenter;

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/Presenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 287
    const/4 v1, 0x0

    iput-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    .line 289
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    if-eqz v1, :cond_0

    .line 290
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;->onUnbind(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    .line 292
    :cond_0
    return-void
.end method

.method public setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 2
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 169
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v0, :cond_0

    .line 170
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->unregisterObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 172
    :cond_0
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 173
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-nez v0, :cond_2

    .line 181
    :cond_1
    :goto_0
    return-void

    .line 177
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mDataObserver:Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->registerObserver(Landroid/support/v17/leanback/widget/ObjectAdapter$DataObserver;)V

    .line 178
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->hasStableIds()Z

    move-result v0

    iget-object v1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ObjectAdapter;->hasStableIds()Z

    move-result v1

    if-eq v0, v1, :cond_1

    .line 179
    iget-object v0, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->hasStableIds()Z

    move-result v0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setHasStableIds(Z)V

    goto :goto_0
.end method

.method public setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .prologue
    .line 266
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 267
    return-void
.end method

.method setFocusHighlight(Landroid/support/v17/leanback/widget/FocusHighlightHandler;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/FocusHighlightHandler;

    .prologue
    .line 192
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mFocusHighlight:Landroid/support/v17/leanback/widget/FocusHighlightHandler;

    .line 194
    return-void
.end method

.method public setPresenterMapper(Ljava/util/ArrayList;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 201
    .local p1, "presenters":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Presenter;>;"
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mPresenters:Ljava/util/ArrayList;

    .line 202
    return-void
.end method

.method public setWrapper(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;)V
    .locals 0
    .param p1, "wrapper"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    .prologue
    .line 184
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    .line 185
    return-void
.end method
