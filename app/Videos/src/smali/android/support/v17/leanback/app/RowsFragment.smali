.class public Landroid/support/v17/leanback/app/RowsFragment;
.super Landroid/support/v17/leanback/app/BaseRowFragment;
.source "RowsFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;
    }
.end annotation


# instance fields
.field private final mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mExpand:Z

.field private mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mPresenterMapper:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Presenter;",
            ">;"
        }
    .end annotation
.end field

.field private mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

.field private mRowScaleEnabled:Z

.field private mRowScaleFactor:F

.field private mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

.field mSelectAnimatorDuration:I

.field mSelectAnimatorInterpolator:Landroid/view/animation/Interpolator;

.field private mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

.field private mViewsCreated:Z


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 46
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BaseRowFragment;-><init>()V

    .line 119
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mExpand:Z

    .line 136
    new-instance v0, Landroid/view/animation/DecelerateInterpolator;

    const/high16 v1, 0x40000000    # 2.0f

    invoke-direct {v0, v1}, Landroid/view/animation/DecelerateInterpolator;-><init>(F)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectAnimatorInterpolator:Landroid/view/animation/Interpolator;

    .line 364
    new-instance v0, Landroid/support/v17/leanback/app/RowsFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/RowsFragment$1;-><init>(Landroid/support/v17/leanback/app/RowsFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 475
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method static synthetic access$1000(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method static synthetic access$1100(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 46
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    return-void
.end method

.method static synthetic access$1200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    return-object v0
.end method

.method static synthetic access$1202(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 46
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    return-object p1
.end method

.method static synthetic access$200(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    return-object v0
.end method

.method static synthetic access$300(Landroid/support/v17/leanback/app/RowsFragment;Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 46
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V

    return-void
.end method

.method static synthetic access$402(Landroid/support/v17/leanback/app/RowsFragment;Z)Z
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z

    return p1
.end method

.method static synthetic access$500(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Z
    .param p2, "x2"    # Z

    .prologue
    .line 46
    invoke-static {p0, p1, p2}, Landroid/support/v17/leanback/app/RowsFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    return-void
.end method

.method static synthetic access$600(Landroid/support/v17/leanback/app/RowsFragment;)Z
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mExpand:Z

    return v0
.end method

.method static synthetic access$700(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Z

    .prologue
    .line 46
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V

    return-void
.end method

.method static synthetic access$800(Landroid/support/v17/leanback/app/RowsFragment;)Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/RowsFragment;

    .prologue
    .line 46
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method static synthetic access$900(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 46
    invoke-static {p0, p1}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    return-void
.end method

.method private needsScale()Z
    .locals 1

    .prologue
    .line 522
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRowScaleEnabled:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mExpand:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 1
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 356
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 357
    return-void
.end method

.method private static setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 1
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 361
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {v0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 362
    return-void
.end method

.method private static setRowViewExpanded(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Z)V
    .locals 2
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "expanded"    # Z

    .prologue
    .line 344
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v1

    invoke-virtual {v0, v1, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setRowViewExpanded(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V

    .line 345
    return-void
.end method

.method private static setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V
    .locals 3
    .param p0, "vh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .param p1, "selected"    # Z
    .param p2, "immediate"    # Z

    .prologue
    .line 349
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getExtraObject()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;

    .line 350
    .local v0, "extra":Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;
    invoke-virtual {v0, p1, p2}, Landroid/support/v17/leanback/app/RowsFragment$RowViewHolderExtra;->animateSelect(ZZ)V

    .line 351
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v2

    invoke-virtual {v1, v2, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->setRowViewSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Z)V

    .line 352
    return-void
.end method

.method private setupSharedViewPool(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;)V
    .locals 5
    .param p1, "bridgeVh"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .prologue
    .line 433
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getPresenter()Landroid/support/v17/leanback/widget/Presenter;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/RowPresenter;

    .line 434
    .local v1, "rowPresenter":Landroid/support/v17/leanback/widget/RowPresenter;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->getViewHolder()Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/RowPresenter;->getRowViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    move-result-object v2

    .line 436
    .local v2, "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    instance-of v4, v2, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    if-eqz v4, :cond_0

    move-object v4, v2

    .line 437
    check-cast v4, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    invoke-virtual {v4}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v3

    .line 439
    .local v3, "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    if-nez v4, :cond_1

    .line 440
    invoke-virtual {v3}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getRecycledViewPool()Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    .line 445
    :goto_0
    check-cast v2, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .end local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v0

    .line 447
    .local v0, "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mPresenterMapper:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    .line 448
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->getPresenterMapper()Ljava/util/ArrayList;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mPresenterMapper:Ljava/util/ArrayList;

    .line 453
    .end local v0    # "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    .end local v3    # "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    :cond_0
    :goto_1
    return-void

    .line 442
    .restart local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .restart local v3    # "view":Landroid/support/v17/leanback/widget/HorizontalGridView;
    :cond_1
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    invoke-virtual {v3, v4}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setRecycledViewPool(Landroid/support/v7/widget/RecyclerView$RecycledViewPool;)V

    goto :goto_0

    .line 450
    .end local v2    # "rowVh":Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .restart local v0    # "bridgeAdapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    :cond_2
    iget-object v4, p0, Landroid/support/v17/leanback/app/RowsFragment;->mPresenterMapper:Ljava/util/ArrayList;

    invoke-virtual {v0, v4}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setPresenterMapper(Ljava/util/ArrayList;)V

    goto :goto_1
.end method


# virtual methods
.method protected findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 145
    sget v0, Landroid/support/v17/leanback/R$id;->container_list:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method

.method protected getLayoutResourceId()I
    .locals 1

    .prologue
    .line 286
    sget v0, Landroid/support/v17/leanback/R$layout;->lb_rows_fragment:I

    return v0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x1

    .line 291
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/BaseRowFragment;->onCreate(Landroid/os/Bundle;)V

    .line 292
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$integer;->lb_browse_rows_anim_duration:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectAnimatorDuration:I

    .line 294
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$fraction;->lb_browse_rows_scale:I

    invoke-virtual {v0, v1, v2, v2}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRowScaleFactor:F

    .line 296
    return-void
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 2
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 301
    invoke-super {p0, p1, p2, p3}, Landroid/support/v17/leanback/app/BaseRowFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, "view":Landroid/view/View;
    sget v1, Landroid/support/v17/leanback/R$id;->scale_frame:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    .line 303
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 323
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z

    .line 324
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowFragment;->onDestroyView()V

    .line 325
    return-void
.end method

.method protected onRowSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V
    .locals 5
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v4, 0x0

    .line 264
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 265
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-nez v0, :cond_1

    .line 282
    :cond_0
    :goto_0
    return-void

    .line 268
    :cond_1
    if-nez p2, :cond_3

    const/4 v1, 0x0

    .line 271
    .local v1, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :goto_1
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eq v2, v1, :cond_0

    .line 274
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eqz v2, :cond_2

    .line 275
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    invoke-static {v2, v4, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    .line 277
    :cond_2
    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 278
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    if-eqz v2, :cond_0

    .line 279
    iget-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    const/4 v3, 0x1

    invoke-static {v2, v3, v4}, Landroid/support/v17/leanback/app/RowsFragment;->setRowViewSelected(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;ZZ)V

    goto :goto_0

    .line 268
    .end local v1    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_3
    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v2

    check-cast v2, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-object v1, v2

    goto :goto_1
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, 0x0

    .line 309
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/app/BaseRowFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 312
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$id;->row_content:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setItemAlignmentViewId(I)V

    .line 313
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSaveChildrenPolicy(I)V

    .line 315
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mScaleFrameLayout:Landroid/support/v17/leanback/widget/ScaleFrameLayout;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ScaleFrameLayout;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    invoke-direct {p0}, Landroid/support/v17/leanback/app/RowsFragment;->needsScale()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 317
    iput-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mRecycledViewPool:Landroid/support/v7/widget/RecyclerView$RecycledViewPool;

    .line 318
    iput-object v2, p0, Landroid/support/v17/leanback/app/RowsFragment;->mPresenterMapper:Ljava/util/ArrayList;

    .line 319
    return-void

    .line 315
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method setExternalAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .prologue
    .line 336
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mExternalAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 337
    return-void
.end method

.method public setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .prologue
    .line 156
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .line 157
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z

    if-eqz v0, :cond_0

    .line 158
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Item clicked listener must be set before views are created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 161
    :cond_0
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 216
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 217
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    .line 218
    .local v2, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_0

    .line 219
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 220
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 221
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 222
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 224
    .local v3, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-static {v3, v5}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V

    .line 220
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method public setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 2
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 178
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 179
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z

    if-eqz v0, :cond_0

    .line 180
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "Item clicked listener must be set before views are created"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 183
    :cond_0
    return-void
.end method

.method public setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 6
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 233
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 234
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v2

    .line 235
    .local v2, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v2, :cond_0

    .line 236
    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildCount()I

    move-result v0

    .line 237
    .local v0, "count":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 238
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    .line 239
    .local v4, "view":Landroid/view/View;
    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v3

    check-cast v3, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 241
    .local v3, "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    iget-object v5, p0, Landroid/support/v17/leanback/app/RowsFragment;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    invoke-static {v3, v5}, Landroid/support/v17/leanback/app/RowsFragment;->setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V

    .line 237
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 244
    .end local v0    # "count":I
    .end local v1    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    .end local v4    # "view":Landroid/view/View;
    :cond_0
    return-void
.end method

.method protected updateAdapter()V
    .locals 2

    .prologue
    .line 457
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowFragment;->updateAdapter()V

    .line 458
    const/4 v1, 0x0

    iput-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mSelectedViewHolder:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 459
    const/4 v1, 0x0

    iput-boolean v1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mViewsCreated:Z

    .line 461
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/RowsFragment;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v0

    .line 462
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    if-eqz v0, :cond_0

    .line 463
    iget-object v1, p0, Landroid/support/v17/leanback/app/RowsFragment;->mBridgeAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 465
    :cond_0
    return-void
.end method
