.class public final Landroid/support/v17/leanback/R$integer;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "integer"
.end annotation


# static fields
.field public static final lb_browse_headers_transition_delay:I = 0x7f0c0002

.field public static final lb_browse_headers_transition_duration:I = 0x7f0c0003

.field public static final lb_browse_rows_anim_duration:I = 0x7f0c0004

.field public static final lb_card_activated_animation_duration:I = 0x7f0c000e

.field public static final lb_card_selected_animation_delay:I = 0x7f0c000c

.field public static final lb_card_selected_animation_duration:I = 0x7f0c000d

.field public static final lb_details_description_body_max_lines:I = 0x7f0c0007

.field public static final lb_details_description_body_min_lines:I = 0x7f0c0008

.field public static final lb_playback_controls_show_time_ms:I = 0x7f0c001a

.field public static final lb_search_bar_speech_mode_background_alpha:I = 0x7f0c0010

.field public static final lb_search_bar_text_mode_background_alpha:I = 0x7f0c000f

.field public static final lb_search_orb_pulse_duration_ms:I = 0x7f0c000a

.field public static final lb_search_orb_scale_duration_ms:I = 0x7f0c000b
