.class public Landroid/support/v17/leanback/widget/VerticalGridPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "VerticalGridPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    }
.end annotation


# instance fields
.field private mNumColumns:I

.field private mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

.field private mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

.field private mRoundedCornersEnabled:Z

.field private mShadowEnabled:Z

.field private mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

.field private mZoomFactor:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;-><init>(I)V

    .line 57
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "zoomFactor"    # I

    .prologue
    const/4 v1, 0x1

    .line 59
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 46
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    .line 48
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mShadowEnabled:Z

    .line 53
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mRoundedCornersEnabled:Z

    .line 157
    new-instance v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$1;-><init>(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    .line 60
    iput p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mZoomFactor:I

    .line 61
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/VerticalGridPresenter;Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/VerticalGridPresenter;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->selectChildView(Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;Landroid/view/View;)V

    return-void
.end method

.method private selectChildView(Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;Landroid/view/View;)V
    .locals 5
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 327
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 328
    if-nez p2, :cond_2

    move-object v0, v2

    .line 330
    .local v0, "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :goto_0
    if-nez v0, :cond_3

    .line 331
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    invoke-interface {v1, v2, v2}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 336
    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_0
    :goto_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 337
    if-nez p2, :cond_4

    move-object v0, v2

    .line 339
    .restart local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :goto_2
    if-nez v0, :cond_5

    .line 340
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    invoke-interface {v1, v2, v2, v2, v2}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 345
    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_1
    :goto_3
    return-void

    .line 328
    :cond_2
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-object v0, v1

    goto :goto_0

    .line 333
    .restart local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_3
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-interface {v1, v3, v2}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    goto :goto_1

    .line 337
    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_4
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    move-object v0, v1

    goto :goto_2

    .line 342
    .restart local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_5
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v4, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-interface {v1, v3, v4, v2, v2}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    goto :goto_3
.end method


# virtual methods
.method public final areChildRoundedCornersEnabled()Z
    .locals 1

    .prologue
    .line 121
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mRoundedCornersEnabled:Z

    return v0
.end method

.method protected createGridViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 152
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$layout;->lb_vertical_grid:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 154
    .local v0, "root":Landroid/view/View;
    new-instance v2, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    sget v1, Landroid/support/v17/leanback/R$id;->browse_grid:I

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/VerticalGridView;

    invoke-direct {v2, v1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;-><init>(Landroid/support/v17/leanback/widget/VerticalGridView;)V

    return-object v2
.end method

.method public getNumberOfColumns()I
    .locals 1

    .prologue
    .line 79
    iget v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    return v0
.end method

.method public final getOnItemClickedListener()Landroid/support/v17/leanback/widget/OnItemClickedListener;
    .locals 1

    .prologue
    .line 316
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    return-object v0
.end method

.method public final getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;
    .locals 1

    .prologue
    .line 272
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    return-object v0
.end method

.method public final getOnItemViewClickedListener()Landroid/support/v17/leanback/widget/OnItemViewClickedListener;
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    return-object v0
.end method

.method public final getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;
    .locals 1

    .prologue
    .line 287
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    return-object v0
.end method

.method public final getShadowEnabled()Z
    .locals 1

    .prologue
    .line 97
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mShadowEnabled:Z

    return v0
.end method

.method protected initializeGridViewHolder(Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;)V
    .locals 5
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 180
    iget v1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    const/4 v4, -0x1

    if-ne v1, v4, :cond_0

    .line 181
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Number of columns must be set"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 184
    :cond_0
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    iget v4, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/VerticalGridView;->setNumColumns(I)V

    .line 185
    iput-boolean v2, p1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mInitialized:Z

    .line 187
    iget-object v1, p1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget-object v4, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    invoke-virtual {v1, v4}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setWrapper(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;)V

    .line 188
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->needsDefaultShadow()Z

    move-result v1

    if-nez v1, :cond_1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->areChildRoundedCornersEnabled()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 189
    :cond_1
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->prepareParentForShadow(Landroid/view/ViewGroup;)V

    .line 190
    iget-object v1, p1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->view:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v3}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 192
    :cond_2
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v4

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->isUsingZOrder()Z

    move-result v1

    if-nez v1, :cond_3

    move v1, v2

    :goto_0
    invoke-virtual {v4, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setFocusDrawingOrderEnabled(Z)V

    .line 193
    iget-object v1, p1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget v3, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mZoomFactor:I

    invoke-static {v1, v3, v2}, Landroid/support/v17/leanback/widget/FocusHighlightHelper;->setupBrowseItemFocusHighlight(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;IZ)V

    .line 196
    move-object v0, p1

    .line 197
    .local v0, "gridViewHolder":Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    new-instance v2, Landroid/support/v17/leanback/widget/VerticalGridPresenter$2;

    invoke-direct {v2, p0, v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$2;-><init>(Landroid/support/v17/leanback/widget/VerticalGridPresenter;Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V

    .line 204
    iget-object v1, p1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    new-instance v2, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;

    invoke-direct {v2, p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$3;-><init>(Landroid/support/v17/leanback/widget/VerticalGridPresenter;)V

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 240
    return-void

    .end local v0    # "gridViewHolder":Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    :cond_3
    move v1, v3

    .line 192
    goto :goto_0
.end method

.method public isUsingDefaultShadow()Z
    .locals 1

    .prologue
    .line 106
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->supportsShadow()Z

    move-result v0

    return v0
.end method

.method public isUsingZOrder()Z
    .locals 1

    .prologue
    .line 130
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ShadowHelper;->usesZShadow()Z

    move-result v0

    return v0
.end method

.method final needsDefaultShadow()Z
    .locals 1

    .prologue
    .line 134
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->isUsingDefaultShadow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->getShadowEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 245
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    .line 246
    .local v0, "vh":Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    check-cast p2, Landroid/support/v17/leanback/widget/ObjectAdapter;

    .end local p2    # "item":Ljava/lang/Object;
    invoke-virtual {v1, p2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 247
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    iget-object v2, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 248
    return-void
.end method

.method public bridge synthetic onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 1
    .param p1, "x0"    # Landroid/view/ViewGroup;

    .prologue
    .line 27
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public final onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 139
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->createGridViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    move-result-object v0

    .line 140
    .local v0, "vh":Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    const/4 v1, 0x0

    iput-boolean v1, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mInitialized:Z

    .line 141
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->initializeGridViewHolder(Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;)V

    .line 142
    iget-boolean v1, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mInitialized:Z

    if-nez v1, :cond_0

    .line 143
    new-instance v1, Ljava/lang/RuntimeException;

    const-string v2, "super.initializeGridViewHolder() must be called"

    invoke-direct {v1, v2}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 145
    :cond_0
    return-object v0
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 3
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 253
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    .line 254
    .local v0, "vh":Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 255
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 256
    return-void
.end method

.method public setNumberOfColumns(I)V
    .locals 2
    .param p1, "numColumns"    # I

    .prologue
    .line 67
    if-gez p1, :cond_0

    .line 68
    new-instance v0, Ljava/lang/IllegalArgumentException;

    const-string v1, "Invalid number of columns"

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 70
    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    if-eq v0, p1, :cond_1

    .line 71
    iput p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mNumColumns:I

    .line 73
    :cond_1
    return-void
.end method

.method public final setOnItemClickedListener(Landroid/support/v17/leanback/widget/OnItemClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .prologue
    .line 298
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemClickedListener:Landroid/support/v17/leanback/widget/OnItemClickedListener;

    .line 299
    return-void
.end method

.method public final setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .prologue
    .line 308
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemViewClickedListener:Landroid/support/v17/leanback/widget/OnItemViewClickedListener;

    .line 309
    return-void
.end method

.method public final setOnItemViewSelectedListener(Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .prologue
    .line 280
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter;->mOnItemViewSelectedListener:Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    .line 281
    return-void
.end method
