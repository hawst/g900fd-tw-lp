.class Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;
.super Ljava/lang/Object;
.source "FocusHighlightHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/FocusHighlightHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/FocusHighlightHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "BrowseItemFocusHighlight"
.end annotation


# static fields
.field private static sScaleFactor:[F


# instance fields
.field private mScaleIndex:I

.field private final mUseDimmer:Z


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 115
    const/4 v0, 0x4

    new-array v0, v0, [F

    sput-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    return-void
.end method

.method constructor <init>(IZ)V
    .locals 1
    .param p1, "zoomIndex"    # I
    .param p2, "useDimmer"    # Z

    .prologue
    .line 120
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 121
    if-ltz p1, :cond_0

    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .end local p1    # "zoomIndex":I
    :goto_0
    iput p1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->mScaleIndex:I

    .line 123
    iput-boolean p2, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->mUseDimmer:Z

    .line 124
    return-void

    .line 121
    .restart local p1    # "zoomIndex":I
    :cond_0
    const/4 p1, 0x2

    goto :goto_0
.end method

.method private getOrCreateAnimator(Landroid/view/View;)Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    .locals 4
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 155
    sget v1, Landroid/support/v17/leanback/R$id;->lb_focus_animator:I

    invoke-virtual {p1, v1}, Landroid/view/View;->getTag(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;

    .line 156
    .local v0, "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    if-nez v0, :cond_0

    .line 157
    new-instance v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;

    .end local v0    # "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->getScale(Landroid/view/View;)F

    move-result v1

    iget-boolean v2, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->mUseDimmer:Z

    const/16 v3, 0x96

    invoke-direct {v0, p1, v1, v2, v3}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;-><init>(Landroid/view/View;FZI)V

    .line 158
    .restart local v0    # "animator":Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;
    sget v1, Landroid/support/v17/leanback/R$id;->lb_focus_animator:I

    invoke-virtual {p1, v1, v0}, Landroid/view/View;->setTag(ILjava/lang/Object;)V

    .line 160
    :cond_0
    return-object v0
.end method

.method private getScale(Landroid/view/View;)F
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 139
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->lazyInit(Landroid/content/res/Resources;)V

    .line 140
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    iget v1, p0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->mScaleIndex:I

    aget v0, v0, v1

    return v0
.end method

.method private static lazyInit(Landroid/content/res/Resources;)V
    .locals 4
    .param p0, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/4 v2, 0x0

    const/4 v3, 0x1

    .line 127
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    aget v0, v0, v2

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 128
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    const/high16 v1, 0x3f800000    # 1.0f

    aput v1, v0, v2

    .line 129
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    sget v1, Landroid/support/v17/leanback/R$fraction;->lb_focus_zoom_factor_small:I

    invoke-virtual {p0, v1, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v1

    aput v1, v0, v3

    .line 131
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    const/4 v1, 0x2

    sget v2, Landroid/support/v17/leanback/R$fraction;->lb_focus_zoom_factor_medium:I

    invoke-virtual {p0, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    aput v2, v0, v1

    .line 133
    sget-object v0, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->sScaleFactor:[F

    const/4 v1, 0x3

    sget v2, Landroid/support/v17/leanback/R$fraction;->lb_focus_zoom_factor_large:I

    invoke-virtual {p0, v2, v3, v3}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    aput v2, v0, v1

    .line 136
    :cond_0
    return-void
.end method


# virtual methods
.method public onInitializeView(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 151
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->getOrCreateAnimator(Landroid/view/View;)Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;

    move-result-object v0

    const/4 v1, 0x0

    const/4 v2, 0x1

    invoke-virtual {v0, v1, v2}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->animateFocus(ZZ)V

    .line 152
    return-void
.end method

.method public onItemFocused(Landroid/view/View;Z)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 145
    invoke-virtual {p1, p2}, Landroid/view/View;->setSelected(Z)V

    .line 146
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$BrowseItemFocusHighlight;->getOrCreateAnimator(Landroid/view/View;)Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, p2, v1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper$FocusAnimator;->animateFocus(ZZ)V

    .line 147
    return-void
.end method
