.class public final Landroid/support/v17/leanback/R$color;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "color"
.end annotation


# static fields
.field public static final lb_default_search_color:I = 0x7f0a0056

.field public static final lb_error_background_color_opaque:I = 0x7f0a0044

.field public static final lb_error_background_color_translucent:I = 0x7f0a0045

.field public static final lb_error_message_color_on_opaque:I = 0x7f0a0046

.field public static final lb_error_message_color_on_translucent:I = 0x7f0a0047

.field public static final lb_playback_controls_background_dark:I = 0x7f0a005d

.field public static final lb_playback_controls_background_light:I = 0x7f0a005c

.field public static final lb_search_bar_hint:I = 0x7f0a004b

.field public static final lb_search_bar_hint_speech_mode:I = 0x7f0a004c

.field public static final lb_search_bar_text:I = 0x7f0a0049

.field public static final lb_search_bar_text_speech_mode:I = 0x7f0a004a

.field public static final lb_speech_orb_not_recording:I = 0x7f0a004d

.field public static final lb_speech_orb_not_recording_icon:I = 0x7f0a0050

.field public static final lb_speech_orb_not_recording_pulsed:I = 0x7f0a004e

.field public static final lb_speech_orb_recording:I = 0x7f0a004f

.field public static final lb_view_dim_mask_color:I = 0x7f0a0040
