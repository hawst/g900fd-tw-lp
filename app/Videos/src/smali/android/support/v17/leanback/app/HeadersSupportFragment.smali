.class public Landroid/support/v17/leanback/app/HeadersSupportFragment;
.super Landroid/support/v17/leanback/app/BaseRowSupportFragment;
.source "HeadersSupportFragment.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/app/HeadersSupportFragment$NoOverlappingFrameLayout;,
        Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;
    }
.end annotation


# static fields
.field private static final sHeaderPresenter:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private static sLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;


# instance fields
.field private final mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

.field private mBackgroundColor:I

.field private mBackgroundColorSet:Z

.field private mHeadersEnabled:Z

.field private mHeadersGone:Z

.field private mOnHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

.field private mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

.field private final mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 55
    new-instance v0, Landroid/support/v17/leanback/widget/SinglePresenterSelector;

    new-instance v1, Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    sget v2, Landroid/support/v17/leanback/R$layout;->lb_header:I

    invoke-direct {v1, v2}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;-><init>(I)V

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/SinglePresenterSelector;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    sput-object v0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->sHeaderPresenter:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 111
    new-instance v0, Landroid/support/v17/leanback/app/HeadersSupportFragment$2;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment$2;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->sLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    return-void
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 58
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;-><init>()V

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersEnabled:Z

    .line 51
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersGone:Z

    .line 87
    new-instance v0, Landroid/support/v17/leanback/app/HeadersSupportFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment$1;-><init>(Landroid/support/v17/leanback/app/HeadersSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    .line 181
    new-instance v0, Landroid/support/v17/leanback/app/HeadersSupportFragment$3;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment$3;-><init>(Landroid/support/v17/leanback/app/HeadersSupportFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    .line 59
    sget-object v0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->sHeaderPresenter:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 60
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/HeadersSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/HeadersSupportFragment;

    .prologue
    .line 42
    iget-object v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

    return-object v0
.end method

.method static synthetic access$100(Landroid/support/v17/leanback/app/HeadersSupportFragment;)Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/HeadersSupportFragment;

    .prologue
    .line 42
    iget-object v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    return-object v0
.end method

.method static synthetic access$200()Landroid/view/View$OnLayoutChangeListener;
    .locals 1

    .prologue
    .line 42
    sget-object v0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->sLayoutChangeListener:Landroid/view/View$OnLayoutChangeListener;

    return-object v0
.end method

.method private updateFadingEdgeToBrandColor(I)V
    .locals 5
    .param p1, "backgroundColor"    # I

    .prologue
    const/4 v4, 0x0

    .line 216
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$id;->fade_out_edge:I

    invoke-virtual {v2, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 217
    .local v1, "fadingView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    .line 218
    .local v0, "background":Landroid/graphics/drawable/Drawable;
    instance-of v2, v0, Landroid/graphics/drawable/GradientDrawable;

    if-eqz v2, :cond_0

    .line 219
    invoke-virtual {v0}, Landroid/graphics/drawable/Drawable;->mutate()Landroid/graphics/drawable/Drawable;

    .line 220
    check-cast v0, Landroid/graphics/drawable/GradientDrawable;

    .end local v0    # "background":Landroid/graphics/drawable/Drawable;
    const/4 v2, 0x2

    new-array v2, v2, [I

    aput v4, v2, v4

    const/4 v3, 0x1

    aput p1, v2, v3

    invoke-virtual {v0, v2}, Landroid/graphics/drawable/GradientDrawable;->setColors([I)V

    .line 223
    :cond_0
    return-void
.end method

.method private updateListViewVisibility()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 141
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 142
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    .line 143
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v3

    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersGone:Z

    if-eqz v1, :cond_1

    const/16 v1, 0x8

    :goto_0
    invoke-virtual {v3, v1}, Landroid/view/View;->setVisibility(I)V

    .line 144
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersGone:Z

    if-nez v1, :cond_0

    .line 145
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersEnabled:Z

    if-eqz v1, :cond_2

    .line 146
    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setChildrenVisibility(I)V

    .line 152
    :cond_0
    :goto_1
    return-void

    :cond_1
    move v1, v2

    .line 143
    goto :goto_0

    .line 148
    :cond_2
    const/4 v1, 0x4

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setChildrenVisibility(I)V

    goto :goto_1
.end method


# virtual methods
.method protected findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 72
    sget v0, Landroid/support/v17/leanback/R$id;->browse_headers:I

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method

.method getBackgroundColor()I
    .locals 4

    .prologue
    .line 226
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    if-nez v1, :cond_0

    .line 227
    new-instance v1, Ljava/lang/IllegalStateException;

    const-string v2, "Activity must be attached"

    invoke-direct {v1, v2}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 230
    :cond_0
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColorSet:Z

    if-eqz v1, :cond_1

    .line 231
    iget v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColor:I

    .line 236
    :goto_0
    return v1

    .line 234
    :cond_1
    new-instance v0, Landroid/util/TypedValue;

    invoke-direct {v0}, Landroid/util/TypedValue;-><init>()V

    .line 235
    .local v0, "outValue":Landroid/util/TypedValue;
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/FragmentActivity;->getTheme()Landroid/content/res/Resources$Theme;

    move-result-object v1

    sget v2, Landroid/support/v17/leanback/R$attr;->defaultBrandColor:I

    const/4 v3, 0x1

    invoke-virtual {v1, v2, v0, v3}, Landroid/content/res/Resources$Theme;->resolveAttribute(ILandroid/util/TypedValue;Z)Z

    .line 236
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget v2, v0, Landroid/util/TypedValue;->resourceId:I

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    goto :goto_0
.end method

.method protected getLayoutResourceId()I
    .locals 1

    .prologue
    .line 122
    sget v0, Landroid/support/v17/leanback/R$layout;->lb_headers_fragment:I

    return v0
.end method

.method public bridge synthetic onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 1
    .param p1, "x0"    # Landroid/view/LayoutInflater;
    .param p2, "x1"    # Landroid/view/ViewGroup;
    .param p3, "x2"    # Landroid/os/Bundle;

    .prologue
    .line 42
    invoke-super {p0, p1, p2, p3}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onDestroyView()V
    .locals 0

    .prologue
    .line 42
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onDestroyView()V

    return-void
.end method

.method protected onRowSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    const/4 v2, 0x0

    .line 77
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    if-eqz v1, :cond_0

    .line 78
    if-ltz p3, :cond_1

    .line 79
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/Row;

    .line 80
    .local v0, "row":Landroid/support/v17/leanback/widget/Row;
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-interface {v1, v2, v0}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 85
    .end local v0    # "row":Landroid/support/v17/leanback/widget/Row;
    :cond_0
    :goto_0
    return-void

    .line 82
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    invoke-interface {v1, v2, v2}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    goto :goto_0
.end method

.method onTransitionEnd()V
    .locals 2

    .prologue
    .line 260
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersEnabled:Z

    if-eqz v1, :cond_0

    .line 261
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 262
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    .line 263
    const/high16 v1, 0x40000

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setDescendantFocusability(I)V

    .line 264
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 265
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    .line 269
    .end local v0    # "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    :cond_0
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onTransitionEnd()V

    .line 270
    return-void
.end method

.method onTransitionStart()V
    .locals 2

    .prologue
    .line 241
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onTransitionStart()V

    .line 242
    iget-boolean v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersEnabled:Z

    if-nez v1, :cond_0

    .line 248
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 249
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-eqz v0, :cond_0

    .line 250
    const/high16 v1, 0x20000

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setDescendantFocusability(I)V

    .line 251
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 252
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    .line 256
    .end local v0    # "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    :cond_0
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 127
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V

    .line 128
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    .line 129
    .local v0, "listView":Landroid/support/v17/leanback/widget/VerticalGridView;
    if-nez v0, :cond_0

    .line 138
    :goto_0
    return-void

    .line 132
    :cond_0
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 133
    invoke-static {v0}, Landroid/support/v17/leanback/widget/FocusHighlightHelper;->setupHeaderItemFocusHighlight(Landroid/support/v17/leanback/widget/VerticalGridView;)V

    .line 135
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getBackgroundColor()I

    move-result v1

    invoke-virtual {p1, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 136
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getBackgroundColor()I

    move-result v1

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->updateFadingEdgeToBrandColor(I)V

    .line 137
    invoke-direct {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->updateListViewVisibility()V

    goto :goto_0
.end method

.method setBackgroundColor(I)V
    .locals 2
    .param p1, "color"    # I

    .prologue
    .line 206
    iput p1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColor:I

    .line 207
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColorSet:Z

    .line 209
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    if-eqz v0, :cond_0

    .line 210
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getView()Landroid/view/View;

    move-result-object v0

    iget v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColor:I

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackgroundColor(I)V

    .line 211
    iget v0, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mBackgroundColor:I

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->updateFadingEdgeToBrandColor(I)V

    .line 213
    :cond_0
    return-void
.end method

.method setHeadersEnabled(Z)V
    .locals 0
    .param p1, "enabled"    # Z

    .prologue
    .line 155
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersEnabled:Z

    .line 156
    invoke-direct {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->updateListViewVisibility()V

    .line 157
    return-void
.end method

.method setHeadersGone(Z)V
    .locals 0
    .param p1, "gone"    # Z

    .prologue
    .line 160
    iput-boolean p1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mHeadersGone:Z

    .line 161
    invoke-direct {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->updateListViewVisibility()V

    .line 162
    return-void
.end method

.method public setOnHeaderClickedListener(Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

    .prologue
    .line 63
    iput-object p1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnHeaderClickedListener:Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;

    .line 64
    return-void
.end method

.method public setOnItemSelectedListener(Landroid/support/v17/leanback/widget/OnItemSelectedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .prologue
    .line 67
    iput-object p1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mOnItemSelectedListener:Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    .line 68
    return-void
.end method

.method public bridge synthetic setSelectedPosition(I)V
    .locals 0
    .param p1, "x0"    # I

    .prologue
    .line 42
    invoke-super {p0, p1}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->setSelectedPosition(I)V

    return-void
.end method

.method protected updateAdapter()V
    .locals 2

    .prologue
    .line 194
    invoke-super {p0}, Landroid/support/v17/leanback/app/BaseRowSupportFragment;->updateAdapter()V

    .line 195
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    move-result-object v0

    .line 196
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    if-eqz v0, :cond_0

    .line 197
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mAdapterListener:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 198
    iget-object v1, p0, Landroid/support/v17/leanback/app/HeadersSupportFragment;->mWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setWrapper(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;)V

    .line 200
    :cond_0
    if-eqz v0, :cond_1

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    if-eqz v1, :cond_1

    .line 201
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v17/leanback/widget/FocusHighlightHelper;->setupHeaderItemFocusHighlight(Landroid/support/v17/leanback/widget/VerticalGridView;)V

    .line 203
    :cond_1
    return-void
.end method
