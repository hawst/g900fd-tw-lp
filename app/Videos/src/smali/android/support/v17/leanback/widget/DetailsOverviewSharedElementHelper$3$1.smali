.class Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;
.super Landroid/support/v17/leanback/transition/TransitionListener;
.source "DetailsOverviewSharedElementHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->run()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$1:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;

.field final synthetic val$transitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;Landroid/support/v17/leanback/transition/TransitionHelper;)V
    .locals 0

    .prologue
    .line 172
    iput-object p1, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;->this$1:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;

    iput-object p2, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;->val$transitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onTransitionEnd(Ljava/lang/Object;)V
    .locals 2
    .param p1, "transition"    # Ljava/lang/Object;

    .prologue
    .line 180
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;->this$1:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # getter for: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->mViewHolder:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$200(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mActionsRow:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/HorizontalGridView;->isFocused()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;->this$1:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;

    iget-object v0, v0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3;->this$0:Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;

    # getter for: Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->mViewHolder:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;->access$200(Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper;)Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;

    move-result-object v0

    iget-object v0, v0, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter$ViewHolder;->mActionsRow:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/HorizontalGridView;->requestFocus()Z

    .line 183
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/DetailsOverviewSharedElementHelper$3$1;->val$transitionHelper:Landroid/support/v17/leanback/transition/TransitionHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, p1, v1}, Landroid/support/v17/leanback/transition/TransitionHelper;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 184
    return-void
.end method
