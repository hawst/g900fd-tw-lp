.class Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
.super Ljava/lang/Object;
.source "BackgroundManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BackgroundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ChangeBackgroundRunnable"
.end annotation


# instance fields
.field private mCancel:Z

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field final synthetic this$0:Landroid/support/v17/leanback/app/BackgroundManager;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p2, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 788
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 789
    iput-object p2, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 790
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;)Landroid/graphics/drawable/Drawable;
    .locals 1
    .param p0, "x0"    # Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    .prologue
    .line 784
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method private runTask()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 804
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # invokes: Landroid/support/v17/leanback/app/BackgroundManager;->lazyInit()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$200(Landroid/support/v17/leanback/app/BackgroundManager;)V

    .line 806
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mDrawable:Landroid/graphics/drawable/Drawable;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BackgroundManager;->access$300(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    # invokes: Landroid/support/v17/leanback/app/BackgroundManager;->sameDrawable(Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z
    invoke-static {v0, v1, v2}, Landroid/support/v17/leanback/app/BackgroundManager;->access$400(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;Landroid/graphics/drawable/Drawable;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 833
    :goto_0
    return-void

    .line 811
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # invokes: Landroid/support/v17/leanback/app/BackgroundManager;->releaseBackgroundBitmap()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$500(Landroid/support/v17/leanback/app/BackgroundManager;)V

    .line 813
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$600(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v0

    if-eqz v0, :cond_1

    .line 814
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    new-instance v1, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BackgroundManager;->access$600(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;-><init>(Landroid/graphics/drawable/Drawable;)V

    # setter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager;->access$702(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 815
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$700(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BackgroundManager;->access$600(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->getAlpha()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    .line 816
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$700(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v0

    const/16 v1, 0x1f4

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->fadeOut(I)V

    .line 820
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$900(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$id;->background_imagein:I

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # invokes: Landroid/support/v17/leanback/app/BackgroundManager;->createEmptyDrawable()Landroid/graphics/drawable/Drawable;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BackgroundManager;->access$800(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 821
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mLayerDrawable:Landroid/graphics/drawable/LayerDrawable;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$900(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/LayerDrawable;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$id;->background_imageout:I

    iget-object v2, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageOutWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BackgroundManager;->access$700(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/graphics/drawable/LayerDrawable;->setDrawableByLayerId(ILandroid/graphics/drawable/Drawable;)Z

    .line 823
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$600(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    .line 824
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # setter for: Landroid/support/v17/leanback/app/BackgroundManager;->mImageInWrapper:Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
    invoke-static {v0, v3}, Landroid/support/v17/leanback/app/BackgroundManager;->access$602(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;)Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;

    .line 827
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mDrawable:Landroid/graphics/drawable/Drawable;

    # setter for: Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager;->access$302(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/graphics/drawable/Drawable;)Landroid/graphics/drawable/Drawable;

    .line 828
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mService:Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$1000(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # getter for: Landroid/support/v17/leanback/app/BackgroundManager;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BackgroundManager;->access$300(Landroid/support/v17/leanback/app/BackgroundManager;)Landroid/graphics/drawable/Drawable;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager$BackgroundContinuityService;->setDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 830
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # invokes: Landroid/support/v17/leanback/app/BackgroundManager;->applyBackgroundChanges()V
    invoke-static {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->access$1100(Landroid/support/v17/leanback/app/BackgroundManager;)V

    .line 832
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->this$0:Landroid/support/v17/leanback/app/BackgroundManager;

    # setter for: Landroid/support/v17/leanback/app/BackgroundManager;->mChangeRunnable:Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;
    invoke-static {v0, v3}, Landroid/support/v17/leanback/app/BackgroundManager;->access$102(Landroid/support/v17/leanback/app/BackgroundManager;Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;)Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;

    goto/16 :goto_0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 793
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mCancel:Z

    .line 794
    return-void
.end method

.method public run()V
    .locals 1

    .prologue
    .line 798
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->mCancel:Z

    if-nez v0, :cond_0

    .line 799
    invoke-direct {p0}, Landroid/support/v17/leanback/app/BackgroundManager$ChangeBackgroundRunnable;->runTask()V

    .line 801
    :cond_0
    return-void
.end method
