.class final Landroid/support/v17/leanback/widget/RoundedRectHelper$StubImpl;
.super Ljava/lang/Object;
.source "RoundedRectHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/RoundedRectHelper$Impl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/RoundedRectHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "StubImpl"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 65
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/widget/RoundedRectHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/widget/RoundedRectHelper$1;

    .prologue
    .line 65
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/RoundedRectHelper$StubImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public clearBackground(Landroid/view/View;)V
    .locals 1
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 76
    const/4 v0, 0x0

    invoke-virtual {p1, v0}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 77
    return-void
.end method

.method public setRoundedRectBackground(Landroid/view/View;I)V
    .locals 0
    .param p1, "view"    # Landroid/view/View;
    .param p2, "color"    # I

    .prologue
    .line 71
    invoke-virtual {p1, p2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 72
    return-void
.end method
