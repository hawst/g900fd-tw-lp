.class Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;
.super Ljava/lang/Object;
.source "VerticalGridSupportFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/VerticalGridSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)V
    .locals 0

    .prologue
    .line 300
    iput-object p1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 4
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I

    .prologue
    .line 303
    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$100()Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "VerticalGridSupportFragment"

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "onFocusSearch focused "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, " + direction "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 305
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$500(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/TitleView;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/TitleView;->getSearchAffordanceView()Landroid/view/View;

    move-result-object v0

    .line 306
    .local v0, "searchOrbView":Landroid/view/View;
    if-ne p1, v0, :cond_3

    const/16 v1, 0x82

    if-eq p2, v1, :cond_1

    const/16 v1, 0x42

    if-ne p2, v1, :cond_3

    .line 308
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/VerticalGridSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/VerticalGridSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->mGridViewHolder:Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
    invoke-static {v1}, Landroid/support/v17/leanback/app/VerticalGridSupportFragment;->access$000(Landroid/support/v17/leanback/app/VerticalGridSupportFragment;)Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;

    move-result-object v1

    iget-object v0, v1, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->view:Landroid/view/View;

    .line 315
    .end local v0    # "searchOrbView":Landroid/view/View;
    :cond_2
    :goto_0
    return-object v0

    .line 310
    .restart local v0    # "searchOrbView":Landroid/view/View;
    :cond_3
    if-eq p1, v0, :cond_4

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v1

    if-nez v1, :cond_4

    const/16 v1, 0x21

    if-eq p2, v1, :cond_2

    .line 315
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method
