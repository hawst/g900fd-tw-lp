.class Landroid/support/v17/leanback/app/BrowseSupportFragment$9;
.super Ljava/lang/Object;
.source "BrowseSupportFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/app/HeadersSupportFragment$OnHeaderClickedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 0

    .prologue
    .line 832
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onHeaderClicked()V
    .locals 2

    .prologue
    .line 835
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1200(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->isInHeadersTransition()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 840
    :cond_0
    :goto_0
    return-void

    .line 838
    :cond_1
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    const/4 v1, 0x0

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->startHeadersTransitionInternal(Z)V
    invoke-static {v0, v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$200(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V

    .line 839
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$9;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/VerticalGridView;->requestFocus()Z

    goto :goto_0
.end method
