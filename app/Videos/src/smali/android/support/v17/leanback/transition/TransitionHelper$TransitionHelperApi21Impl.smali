.class final Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperApi21Impl;
.super Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;
.source "TransitionHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/transition/TransitionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TransitionHelperApi21Impl"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 1

    .prologue
    .line 468
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;-><init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/transition/TransitionHelper$1;

    .prologue
    .line 468
    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperApi21Impl;-><init>()V

    return-void
.end method


# virtual methods
.method public createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 517
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperApi21;->createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createScale()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 512
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelperApi21;->createScale()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
    .locals 1
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 472
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperApi21;->getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method
