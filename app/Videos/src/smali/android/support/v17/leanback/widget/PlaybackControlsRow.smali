.class public Landroid/support/v17/leanback/widget/PlaybackControlsRow;
.super Landroid/support/v17/leanback/widget/Row;
.source "PlaybackControlsRow.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;,
        Landroid/support/v17/leanback/widget/PlaybackControlsRow$MoreActions;
    }
.end annotation


# instance fields
.field private mBufferedProgressMs:I

.field private mCurrentTimeMs:I

.field private mImageDrawable:Landroid/graphics/drawable/Drawable;

.field private mItem:Ljava/lang/Object;

.field private mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

.field private mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mTotalTimeMs:I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 520
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Row;-><init>()V

    .line 521
    return-void
.end method

.method public constructor <init>(Ljava/lang/Object;)V
    .locals 0
    .param p1, "item"    # Ljava/lang/Object;

    .prologue
    .line 513
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Row;-><init>()V

    .line 514
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mItem:Ljava/lang/Object;

    .line 515
    return-void
.end method

.method private bufferedProgressChanged()V
    .locals 2

    .prologue
    .line 661
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    if-eqz v0, :cond_0

    .line 662
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    iget v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mBufferedProgressMs:I

    invoke-interface {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;->onBufferedProgressChanged(I)V

    .line 664
    :cond_0
    return-void
.end method

.method private currentTimeChanged()V
    .locals 2

    .prologue
    .line 655
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    if-eqz v0, :cond_0

    .line 656
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    iget v1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mCurrentTimeMs:I

    invoke-interface {v0, v1}, Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;->onCurrentTimeChanged(I)V

    .line 658
    :cond_0
    return-void
.end method


# virtual methods
.method public getBufferedProgress()I
    .locals 1

    .prologue
    .line 632
    iget v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mBufferedProgressMs:I

    return v0
.end method

.method public getCurrentTime()I
    .locals 1

    .prologue
    .line 615
    iget v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mCurrentTimeMs:I

    return v0
.end method

.method public final getImageDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 556
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mImageDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public final getItem()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 527
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mItem:Ljava/lang/Object;

    return-object v0
.end method

.method public final getPrimaryActionsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 577
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method public final getSecondaryActionsAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 584
    iget-object v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    return-object v0
.end method

.method public getTotalTime()I
    .locals 1

    .prologue
    .line 598
    iget v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mTotalTimeMs:I

    return v0
.end method

.method public setBufferedProgress(I)V
    .locals 1
    .param p1, "ms"    # I

    .prologue
    .line 622
    iget v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mBufferedProgressMs:I

    if-eq v0, p1, :cond_0

    .line 623
    iput p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mBufferedProgressMs:I

    .line 624
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->bufferedProgressChanged()V

    .line 626
    :cond_0
    return-void
.end method

.method public setCurrentTime(I)V
    .locals 1
    .param p1, "ms"    # I

    .prologue
    .line 605
    iget v0, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mCurrentTimeMs:I

    if-eq v0, p1, :cond_0

    .line 606
    iput p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mCurrentTimeMs:I

    .line 607
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->currentTimeChanged()V

    .line 609
    :cond_0
    return-void
.end method

.method public final setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 536
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mImageDrawable:Landroid/graphics/drawable/Drawable;

    .line 537
    return-void
.end method

.method public setOnPlaybackStateChangedListener(Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;)V
    .locals 0
    .param p1, "listener"    # Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    .prologue
    .line 644
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mListener:Landroid/support/v17/leanback/widget/PlaybackControlsRow$OnPlaybackStateChangedListener;

    .line 645
    return-void
.end method

.method public final setPrimaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 563
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mPrimaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 564
    return-void
.end method

.method public final setSecondaryActionsAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "adapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 570
    iput-object p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mSecondaryActionsAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 571
    return-void
.end method

.method public setTotalTime(I)V
    .locals 0
    .param p1, "ms"    # I

    .prologue
    .line 591
    iput p1, p0, Landroid/support/v17/leanback/widget/PlaybackControlsRow;->mTotalTimeMs:I

    .line 592
    return-void
.end method
