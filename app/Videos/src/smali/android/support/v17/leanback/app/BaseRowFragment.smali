.class abstract Landroid/support/v17/leanback/app/BaseRowFragment;
.super Landroid/app/Fragment;
.source "BaseRowFragment.java"


# instance fields
.field private mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

.field private mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

.field private mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private final mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

.field private mSelectedPosition:I

.field private mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/app/Fragment;-><init>()V

    .line 37
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mSelectedPosition:I

    .line 43
    new-instance v0, Landroid/support/v17/leanback/app/BaseRowFragment$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BaseRowFragment$1;-><init>(Landroid/support/v17/leanback/app/BaseRowFragment;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    return-void
.end method


# virtual methods
.method protected findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 0
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 62
    check-cast p1, Landroid/support/v17/leanback/widget/VerticalGridView;

    .end local p1    # "view":Landroid/view/View;
    return-object p1
.end method

.method protected final getBridgeAdapter()Landroid/support/v17/leanback/widget/ItemBridgeAdapter;
    .locals 1

    .prologue
    .line 116
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    return-object v0
.end method

.method protected abstract getLayoutResourceId()I
.end method

.method final getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1

    .prologue
    .line 130
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 56
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BaseRowFragment;->getLayoutResourceId()I

    move-result v1

    const/4 v2, 0x0

    invoke-virtual {p1, v1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 57
    .local v0, "view":Landroid/view/View;
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BaseRowFragment;->findGridViewFromRoot(Landroid/view/View;)Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    iput-object v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 58
    return-object v0
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 78
    invoke-super {p0}, Landroid/app/Fragment;->onDestroyView()V

    .line 79
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 80
    return-void
.end method

.method protected onRowSelected(Landroid/view/ViewGroup;Landroid/view/View;IJ)V
    .locals 0
    .param p1, "parent"    # Landroid/view/ViewGroup;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J

    .prologue
    .line 51
    return-void
.end method

.method public onViewCreated(Landroid/view/View;Landroid/os/Bundle;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    if-eqz v0, :cond_0

    .line 68
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 69
    iget v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mSelectedPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 70
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 73
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mRowSelectedListener:Landroid/support/v17/leanback/widget/OnChildSelectedListener;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V

    .line 74
    return-void
.end method

.method public final setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V
    .locals 0
    .param p1, "rowsAdapter"    # Landroid/support/v17/leanback/widget/ObjectAdapter;

    .prologue
    .line 101
    iput-object p1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    .line 102
    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BaseRowFragment;->updateAdapter()V

    .line 103
    return-void
.end method

.method protected updateAdapter()V
    .locals 3

    .prologue
    .line 134
    const/4 v0, 0x0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .line 136
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    if-eqz v0, :cond_0

    .line 138
    new-instance v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mAdapter:Landroid/support/v17/leanback/widget/ObjectAdapter;

    iget-object v2, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-direct {v0, v1, v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;-><init>(Landroid/support/v17/leanback/widget/ObjectAdapter;Landroid/support/v17/leanback/widget/PresenterSelector;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .line 140
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    if-eqz v0, :cond_1

    .line 141
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 142
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    if-eqz v0, :cond_1

    iget v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mSelectedPosition:I

    const/4 v1, -0x1

    if-eq v0, v1, :cond_1

    .line 143
    iget-object v0, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mVerticalGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    iget v1, p0, Landroid/support/v17/leanback/app/BaseRowFragment;->mSelectedPosition:I

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPosition(I)V

    .line 146
    :cond_1
    return-void
.end method
