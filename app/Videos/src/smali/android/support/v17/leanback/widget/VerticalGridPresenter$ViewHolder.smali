.class public Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;
.super Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
.source "VerticalGridPresenter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/widget/VerticalGridPresenter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ViewHolder"
.end annotation


# instance fields
.field final mGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

.field mInitialized:Z

.field final mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;


# direct methods
.method public constructor <init>(Landroid/support/v17/leanback/widget/VerticalGridView;)V
    .locals 1
    .param p1, "view"    # Landroid/support/v17/leanback/widget/VerticalGridView;

    .prologue
    .line 37
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 32
    new-instance v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    .line 38
    iput-object p1, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 39
    return-void
.end method


# virtual methods
.method public getGridView()Landroid/support/v17/leanback/widget/VerticalGridView;
    .locals 1

    .prologue
    .line 42
    iget-object v0, p0, Landroid/support/v17/leanback/widget/VerticalGridPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/VerticalGridView;

    return-object v0
.end method
