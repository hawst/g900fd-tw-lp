.class Landroid/support/v17/leanback/app/BrowseSupportFragment$2;
.super Ljava/lang/Object;
.source "BrowseSupportFragment.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/BrowseFrameLayout$OnFocusSearchListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 0

    .prologue
    .line 525
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onFocusSearch(Landroid/view/View;I)Landroid/view/View;
    .locals 5
    .param p1, "focused"    # Landroid/view/View;
    .param p2, "direction"    # I

    .prologue
    const/4 v1, 0x0

    .line 529
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mCanShowHeaders:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1200(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    move-object p1, v1

    .line 554
    .end local p1    # "focused":Landroid/view/View;
    :cond_0
    :goto_0
    return-object p1

    .line 531
    .restart local p1    # "focused":Landroid/view/View;
    :cond_1
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/widget/TitleView;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/TitleView;->getSearchAffordanceView()Landroid/view/View;

    move-result-object v0

    .line 533
    .local v0, "searchOrbView":Landroid/view/View;
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->isInHeadersTransition()Z

    move-result v2

    if-nez v2, :cond_0

    .line 534
    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->DEBUG:Z
    invoke-static {}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1400()Z

    move-result v2

    if-eqz v2, :cond_2

    const-string v2, "BrowseSupportFragment"

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "onFocusSearch focused "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " + direction "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Log;->v(Ljava/lang/String;Ljava/lang/String;)I

    .line 535
    :cond_2
    const/16 v2, 0x11

    if-ne p2, v2, :cond_3

    .line 536
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->isVerticalScrolling()Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 539
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object p1

    goto :goto_0

    .line 540
    :cond_3
    const/16 v2, 0x42

    if-ne p2, v2, :cond_4

    .line 541
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->isVerticalScrolling()Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1500(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v1

    if-nez v1, :cond_0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 544
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object p1

    goto :goto_0

    .line 545
    :cond_4
    if-ne p1, v0, :cond_6

    const/16 v2, 0x82

    if-ne p2, v2, :cond_6

    .line 546
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v1

    if-eqz v1, :cond_5

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mHeadersSupportFragment:Landroid/support/v17/leanback/app/HeadersSupportFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$300(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/HeadersSupportFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/HeadersSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    :goto_1
    move-object p1, v1

    goto/16 :goto_0

    :cond_5
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$2;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mRowsSupportFragment:Landroid/support/v17/leanback/app/RowsSupportFragment;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$1600(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Landroid/support/v17/leanback/app/RowsSupportFragment;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v1

    goto :goto_1

    .line 549
    :cond_6
    if-eq p1, v0, :cond_7

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v2

    if-nez v2, :cond_7

    const/16 v2, 0x21

    if-ne p2, v2, :cond_7

    move-object p1, v0

    .line 551
    goto/16 :goto_0

    :cond_7
    move-object p1, v1

    .line 554
    goto/16 :goto_0
.end method
