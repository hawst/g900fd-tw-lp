.class Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;
.super Landroid/transition/ChangeBounds;
.source "TransitionHelperKitkat.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/transition/TransitionHelperKitkat;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "CustomChangeBounds"
.end annotation


# instance fields
.field final mClassStartDelays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field mDefaultStartDelay:I

.field final mIdStartDelays:Landroid/util/SparseIntArray;

.field final mViewStartDelays:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/view/View;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method constructor <init>()V
    .locals 1

    .prologue
    .line 80
    invoke-direct {p0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 84
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mViewStartDelays:Ljava/util/HashMap;

    .line 86
    new-instance v0, Landroid/util/SparseIntArray;

    invoke-direct {v0}, Landroid/util/SparseIntArray;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mIdStartDelays:Landroid/util/SparseIntArray;

    .line 88
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mClassStartDelays:Ljava/util/HashMap;

    return-void
.end method

.method private getDelay(Landroid/view/View;)I
    .locals 5
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x1

    .line 91
    iget-object v2, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mViewStartDelays:Ljava/util/HashMap;

    invoke-virtual {v2, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    .line 92
    .local v0, "delay":Ljava/lang/Integer;
    if-eqz v0, :cond_1

    .line 93
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    .line 103
    :cond_0
    :goto_0
    return v1

    .line 95
    :cond_1
    iget-object v2, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mIdStartDelays:Landroid/util/SparseIntArray;

    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v3

    invoke-virtual {v2, v3, v4}, Landroid/util/SparseIntArray;->get(II)I

    move-result v1

    .line 96
    .local v1, "idStartDelay":I
    if-ne v1, v4, :cond_0

    .line 99
    iget-object v2, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mClassStartDelays:Ljava/util/HashMap;

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .end local v0    # "delay":Ljava/lang/Integer;
    check-cast v0, Ljava/lang/Integer;

    .line 100
    .restart local v0    # "delay":Ljava/lang/Integer;
    if-eqz v0, :cond_2

    .line 101
    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v1

    goto :goto_0

    .line 103
    :cond_2
    iget v1, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mDefaultStartDelay:I

    goto :goto_0
.end method


# virtual methods
.method public createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;
    .locals 4
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "startValues"    # Landroid/transition/TransitionValues;
    .param p3, "endValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 109
    invoke-super {p0, p1, p2, p3}, Landroid/transition/ChangeBounds;->createAnimator(Landroid/view/ViewGroup;Landroid/transition/TransitionValues;Landroid/transition/TransitionValues;)Landroid/animation/Animator;

    move-result-object v0

    .line 110
    .local v0, "animator":Landroid/animation/Animator;
    if-eqz v0, :cond_0

    if-eqz p3, :cond_0

    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    if-eqz v1, :cond_0

    .line 111
    iget-object v1, p3, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-direct {p0, v1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->getDelay(Landroid/view/View;)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/animation/Animator;->setStartDelay(J)V

    .line 113
    :cond_0
    return-object v0
.end method

.method public setDefaultStartDelay(I)V
    .locals 0
    .param p1, "startDelay"    # I

    .prologue
    .line 129
    iput p1, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mDefaultStartDelay:I

    .line 130
    return-void
.end method

.method public setStartDelay(II)V
    .locals 1
    .param p1, "viewId"    # I
    .param p2, "startDelay"    # I

    .prologue
    .line 121
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mIdStartDelays:Landroid/util/SparseIntArray;

    invoke-virtual {v0, p1, p2}, Landroid/util/SparseIntArray;->put(II)V

    .line 122
    return-void
.end method

.method public setStartDelay(Landroid/view/View;I)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "startDelay"    # I

    .prologue
    .line 117
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mViewStartDelays:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    return-void
.end method

.method public setStartDelay(Ljava/lang/String;I)V
    .locals 2
    .param p1, "className"    # Ljava/lang/String;
    .param p2, "startDelay"    # I

    .prologue
    .line 125
    iget-object v0, p0, Landroid/support/v17/leanback/transition/TransitionHelperKitkat$CustomChangeBounds;->mClassStartDelays:Ljava/util/HashMap;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, p1, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    return-void
.end method
