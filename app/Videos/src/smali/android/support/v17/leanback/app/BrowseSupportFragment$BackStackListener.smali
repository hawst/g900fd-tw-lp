.class final Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;
.super Ljava/lang/Object;
.source "BrowseSupportFragment.java"

# interfaces
.implements Landroid/support/v4/app/FragmentManager$OnBackStackChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BrowseSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x10
    name = "BackStackListener"
.end annotation


# instance fields
.field mIndexOfHeadersBackStack:I

.field mLastEntryCount:I

.field final synthetic this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/BrowseSupportFragment;)V
    .locals 1

    .prologue
    .line 86
    iput-object p1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 87
    invoke-virtual {p1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mLastEntryCount:I

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 89
    return-void
.end method


# virtual methods
.method load(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/4 v2, -0x1

    .line 92
    if-eqz p1, :cond_2

    .line 93
    const-string v0, "headerStackIndex"

    invoke-virtual {p1, v0, v2}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v0

    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 94
    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    iget v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    if-ne v0, v2, :cond_1

    const/4 v0, 0x1

    :goto_0
    # setter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v1, v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$002(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)Z

    .line 101
    :cond_0
    :goto_1
    return-void

    .line 94
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 96
    :cond_2
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 97
    iget-object v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    iget-object v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mWithHeadersBackStackName:Ljava/lang/String;
    invoke-static {v1}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentTransaction;->addToBackStack(Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    goto :goto_1
.end method

.method public onBackStackChanged()V
    .locals 5

    .prologue
    .line 110
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    if-nez v2, :cond_0

    .line 111
    const-string v2, "BrowseSupportFragment"

    const-string v3, "getFragmentManager() is null, stack:"

    new-instance v4, Ljava/lang/Exception;

    invoke-direct {v4}, Ljava/lang/Exception;-><init>()V

    invoke-static {v2, v3, v4}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)I

    .line 132
    :goto_0
    return-void

    .line 114
    :cond_0
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryCount()I

    move-result v0

    .line 117
    .local v0, "count":I
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mLastEntryCount:I

    if-le v0, v2, :cond_2

    .line 118
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    invoke-virtual {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->getFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    add-int/lit8 v3, v0, -0x1

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->getBackStackEntryAt(I)Landroid/support/v4/app/FragmentManager$BackStackEntry;

    move-result-object v1

    .line 119
    .local v1, "entry":Landroid/support/v4/app/FragmentManager$BackStackEntry;
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mWithHeadersBackStackName:Ljava/lang/String;
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$100(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {v1}, Landroid/support/v4/app/FragmentManager$BackStackEntry;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 120
    add-int/lit8 v2, v0, -0x1

    iput v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 131
    .end local v1    # "entry":Landroid/support/v4/app/FragmentManager$BackStackEntry;
    :cond_1
    :goto_1
    iput v0, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mLastEntryCount:I

    goto :goto_0

    .line 122
    :cond_2
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mLastEntryCount:I

    if-ge v0, v2, :cond_1

    .line 124
    iget v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    if-lt v2, v0, :cond_1

    .line 125
    const/4 v2, -0x1

    iput v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    .line 126
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    # getter for: Landroid/support/v17/leanback/app/BrowseSupportFragment;->mShowingHeaders:Z
    invoke-static {v2}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$000(Landroid/support/v17/leanback/app/BrowseSupportFragment;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 127
    iget-object v2, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->this$0:Landroid/support/v17/leanback/app/BrowseSupportFragment;

    const/4 v3, 0x1

    # invokes: Landroid/support/v17/leanback/app/BrowseSupportFragment;->startHeadersTransitionInternal(Z)V
    invoke-static {v2, v3}, Landroid/support/v17/leanback/app/BrowseSupportFragment;->access$200(Landroid/support/v17/leanback/app/BrowseSupportFragment;Z)V

    goto :goto_1
.end method

.method save(Landroid/os/Bundle;)V
    .locals 2
    .param p1, "outState"    # Landroid/os/Bundle;

    .prologue
    .line 104
    const-string v0, "headerStackIndex"

    iget v1, p0, Landroid/support/v17/leanback/app/BrowseSupportFragment$BackStackListener;->mIndexOfHeadersBackStack:I

    invoke-virtual {p1, v0, v1}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 105
    return-void
.end method
