.class Landroid/support/v17/leanback/widget/SearchBar$10;
.super Ljava/lang/Object;
.source "SearchBar.java"

# interfaces
.implements Landroid/speech/RecognitionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/widget/SearchBar;->startRecognition()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/widget/SearchBar;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/widget/SearchBar;)V
    .locals 0

    .prologue
    .line 556
    iput-object p1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onBeginningOfSpeech()V
    .locals 0

    .prologue
    .line 567
    return-void
.end method

.method public onBufferReceived([B)V
    .locals 0
    .param p1, "bytes"    # [B

    .prologue
    .line 579
    return-void
.end method

.method public onEndOfSpeech()V
    .locals 0

    .prologue
    .line 584
    return-void
.end method

.method public onError(I)V
    .locals 2
    .param p1, "error"    # I

    .prologue
    .line 589
    packed-switch p1, :pswitch_data_0

    .line 618
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer other error"

    invoke-static {v0, v1}, Landroid/util/Log;->d(Ljava/lang/String;Ljava/lang/String;)I

    .line 622
    :goto_0
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/SearchBar;->stopRecognition()V

    .line 623
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->playSearchFailure()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$1500(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 624
    return-void

    .line 591
    :pswitch_0
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer network timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 594
    :pswitch_1
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer network error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 597
    :pswitch_2
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer audio error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 600
    :pswitch_3
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer server error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 603
    :pswitch_4
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer client error"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 606
    :pswitch_5
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer speech timeout"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 609
    :pswitch_6
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer no match"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 612
    :pswitch_7
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer busy"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 615
    :pswitch_8
    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->TAG:Ljava/lang/String;
    invoke-static {}, Landroid/support/v17/leanback/widget/SearchBar;->access$1400()Ljava/lang/String;

    move-result-object v0

    const-string v1, "recognizer insufficient permissions"

    invoke-static {v0, v1}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto :goto_0

    .line 589
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
    .end packed-switch
.end method

.method public onEvent(ILandroid/os/Bundle;)V
    .locals 0
    .param p1, "i"    # I
    .param p2, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 669
    return-void
.end method

.method public onPartialResults(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    const/4 v4, 0x1

    .line 645
    const-string v3, "results_recognition"

    invoke-virtual {p1, v3}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v1

    .line 649
    .local v1, "results":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-nez v3, :cond_1

    .line 664
    :cond_0
    :goto_0
    return-void

    .line 655
    :cond_1
    const/4 v3, 0x0

    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    .line 660
    .local v2, "stableText":Ljava/lang/String;
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v3

    if-le v3, v4, :cond_2

    invoke-virtual {v1, v4}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object v0, v3

    .line 663
    .local v0, "pendingText":Ljava/lang/String;
    :goto_1
    iget-object v3, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mSearchTextEditor:Landroid/support/v17/leanback/widget/SearchEditText;
    invoke-static {v3}, Landroid/support/v17/leanback/widget/SearchBar;->access$200(Landroid/support/v17/leanback/widget/SearchBar;)Landroid/support/v17/leanback/widget/SearchEditText;

    move-result-object v3

    invoke-virtual {v3, v2, v0}, Landroid/support/v17/leanback/widget/SearchEditText;->updateRecognizedText(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 660
    .end local v0    # "pendingText":Ljava/lang/String;
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public onReadyForSpeech(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 560
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mSpeechOrbView:Landroid/support/v17/leanback/widget/SpeechOrbView;
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$1100(Landroid/support/v17/leanback/widget/SearchBar;)Landroid/support/v17/leanback/widget/SpeechOrbView;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/SpeechOrbView;->showListening()V

    .line 561
    iget-object v0, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->playSearchOpen()V
    invoke-static {v0}, Landroid/support/v17/leanback/widget/SearchBar;->access$1300(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 562
    return-void
.end method

.method public onResults(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "bundle"    # Landroid/os/Bundle;

    .prologue
    .line 629
    const-string v1, "results_recognition"

    invoke-virtual {p1, v1}, Landroid/os/Bundle;->getStringArrayList(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    .line 631
    .local v0, "matches":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    if-eqz v0, :cond_0

    .line 634
    iget-object v2, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    # setter for: Landroid/support/v17/leanback/widget/SearchBar;->mSearchQuery:Ljava/lang/String;
    invoke-static {v2, v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$702(Landroid/support/v17/leanback/widget/SearchBar;Ljava/lang/String;)Ljava/lang/String;

    .line 635
    iget-object v1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mSearchTextEditor:Landroid/support/v17/leanback/widget/SearchEditText;
    invoke-static {v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$200(Landroid/support/v17/leanback/widget/SearchBar;)Landroid/support/v17/leanback/widget/SearchEditText;

    move-result-object v1

    iget-object v2, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mSearchQuery:Ljava/lang/String;
    invoke-static {v2}, Landroid/support/v17/leanback/widget/SearchBar;->access$700(Landroid/support/v17/leanback/widget/SearchBar;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/SearchEditText;->setText(Ljava/lang/CharSequence;)V

    .line 636
    iget-object v1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->submitQuery()V
    invoke-static {v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$900(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 639
    :cond_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/SearchBar;->stopRecognition()V

    .line 640
    iget-object v1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # invokes: Landroid/support/v17/leanback/widget/SearchBar;->playSearchSuccess()V
    invoke-static {v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$1600(Landroid/support/v17/leanback/widget/SearchBar;)V

    .line 641
    return-void
.end method

.method public onRmsChanged(F)V
    .locals 2
    .param p1, "rmsdB"    # F

    .prologue
    .line 572
    const/4 v1, 0x0

    cmpg-float v1, p1, v1

    if-gez v1, :cond_0

    const/4 v0, 0x0

    .line 573
    .local v0, "level":I
    :goto_0
    iget-object v1, p0, Landroid/support/v17/leanback/widget/SearchBar$10;->this$0:Landroid/support/v17/leanback/widget/SearchBar;

    # getter for: Landroid/support/v17/leanback/widget/SearchBar;->mSpeechOrbView:Landroid/support/v17/leanback/widget/SpeechOrbView;
    invoke-static {v1}, Landroid/support/v17/leanback/widget/SearchBar;->access$1100(Landroid/support/v17/leanback/widget/SearchBar;)Landroid/support/v17/leanback/widget/SpeechOrbView;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/SpeechOrbView;->setSoundLevel(I)V

    .line 574
    return-void

    .line 572
    .end local v0    # "level":I
    :cond_0
    const/high16 v1, 0x41200000    # 10.0f

    mul-float/2addr v1, p1

    float-to-int v0, v1

    goto :goto_0
.end method
