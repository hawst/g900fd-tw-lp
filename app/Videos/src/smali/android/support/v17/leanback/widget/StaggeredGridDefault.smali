.class final Landroid/support/v17/leanback/widget/StaggeredGridDefault;
.super Landroid/support/v17/leanback/widget/StaggeredGrid;
.source "StaggeredGridDefault.java"


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/StaggeredGrid;-><init>()V

    return-void
.end method


# virtual methods
.method public appendItems(I)V
    .locals 9
    .param p1, "upTo"    # I

    .prologue
    const/4 v7, -0x1

    .line 26
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    invoke-interface {v6}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->getCount()I

    move-result v0

    .line 29
    .local v0, "count":I
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v6}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v6

    if-lez v6, :cond_1

    .line 30
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getLastIndex()I

    move-result v6

    add-int/lit8 v1, v6, 0x1

    .line 31
    .local v1, "itemIndex":I
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v6}, Landroid/support/v4/util/CircularArray;->getLast()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    iget v6, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    add-int/lit8 v6, v6, 0x1

    iget v8, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    rem-int v5, v6, v8

    .line 40
    .local v5, "rowIndex":I
    :goto_0
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v6}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v6

    if-lez v6, :cond_4

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMaxHighRowIndex()I

    move-result v4

    .line 41
    .local v4, "maxHighRowIndex":I
    :goto_1
    if-eq v4, v7, :cond_5

    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v6, v6, v4

    iget v3, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    .line 44
    .local v3, "maxHigh":I
    :goto_2
    iget v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    if-ge v5, v6, :cond_9

    .line 46
    if-ne v1, v0, :cond_6

    .line 70
    :cond_0
    return-void

    .line 33
    .end local v1    # "itemIndex":I
    .end local v3    # "maxHigh":I
    .end local v4    # "maxHighRowIndex":I
    .end local v5    # "rowIndex":I
    :cond_1
    iget v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartIndex:I

    if-eq v6, v7, :cond_2

    iget v1, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartIndex:I

    .line 34
    .restart local v1    # "itemIndex":I
    :goto_3
    iget v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartRow:I

    if-eq v6, v7, :cond_3

    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartRow:I

    .restart local v5    # "rowIndex":I
    :goto_4
    goto :goto_0

    .line 33
    .end local v1    # "itemIndex":I
    .end local v5    # "rowIndex":I
    :cond_2
    const/4 v1, 0x0

    goto :goto_3

    .line 34
    .restart local v1    # "itemIndex":I
    :cond_3
    iget v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    rem-int v5, v1, v6

    goto :goto_4

    .restart local v5    # "rowIndex":I
    :cond_4
    move v4, v7

    .line 40
    goto :goto_1

    .line 41
    .restart local v4    # "maxHighRowIndex":I
    :cond_5
    const/high16 v3, -0x80000000

    goto :goto_2

    .line 49
    .restart local v3    # "maxHigh":I
    :cond_6
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "itemIndex":I
    .local v2, "itemIndex":I
    invoke-virtual {p0, v1, v5}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->appendItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    .line 52
    if-ne v4, v7, :cond_8

    .line 53
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMaxHighRowIndex()I

    move-result v4

    .line 54
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v6, v6, v4

    iget v3, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    move v1, v2

    .line 44
    .end local v2    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    :cond_7
    :goto_5
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 55
    .end local v1    # "itemIndex":I
    .restart local v2    # "itemIndex":I
    :cond_8
    if-eq v5, v4, :cond_a

    move v1, v2

    .line 56
    .end local v2    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    :goto_6
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v6, v6, v5

    iget v6, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-ge v6, v3, :cond_7

    .line 57
    if-eq v1, v0, :cond_0

    .line 60
    add-int/lit8 v2, v1, 0x1

    .end local v1    # "itemIndex":I
    .restart local v2    # "itemIndex":I
    invoke-virtual {p0, v1, v5}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->appendItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move v1, v2

    .end local v2    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    goto :goto_6

    .line 64
    :cond_9
    iget-object v6, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMinHighRowIndex()I

    move-result v8

    aget-object v6, v6, v8

    iget v6, v6, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->high:I

    if-ge v6, p1, :cond_0

    .line 68
    const/4 v5, 0x0

    .line 69
    goto :goto_0

    .end local v1    # "itemIndex":I
    .restart local v2    # "itemIndex":I
    :cond_a
    move v1, v2

    .end local v2    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    goto :goto_5
.end method

.method public prependItems(I)V
    .locals 8
    .param p1, "downTo"    # I

    .prologue
    const/4 v6, -0x1

    .line 74
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mProvider:Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;

    invoke-interface {v5}, Landroid/support/v17/leanback/widget/StaggeredGrid$Provider;->getCount()I

    move-result v5

    if-gtz v5, :cond_1

    .line 116
    :cond_0
    :goto_0
    return-void

    .line 77
    :cond_1
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v5}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v5

    if-lez v5, :cond_3

    .line 78
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getFirstIndex()I

    move-result v5

    add-int/lit8 v0, v5, -0x1

    .line 79
    .local v0, "itemIndex":I
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v5}, Landroid/support/v4/util/CircularArray;->getFirst()Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    iget v4, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 80
    .local v4, "rowIndex":I
    if-nez v4, :cond_2

    .line 81
    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    add-int/lit8 v4, v5, -0x1

    .line 92
    :goto_1
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mLocations:Landroid/support/v4/util/CircularArray;

    invoke-virtual {v5}, Landroid/support/v4/util/CircularArray;->size()I

    move-result v5

    if-lez v5, :cond_6

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMinLowRowIndex()I

    move-result v3

    .line 93
    .local v3, "minLowRowIndex":I
    :goto_2
    if-eq v3, v6, :cond_7

    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iget v2, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .local v2, "minLow":I
    :goto_3
    move v1, v0

    .line 94
    .end local v0    # "itemIndex":I
    .local v1, "itemIndex":I
    :goto_4
    if-ltz v4, :cond_b

    .line 95
    if-gez v1, :cond_8

    move v0, v1

    .line 96
    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    goto :goto_0

    .line 83
    .end local v2    # "minLow":I
    .end local v3    # "minLowRowIndex":I
    :cond_2
    add-int/lit8 v4, v4, -0x1

    goto :goto_1

    .line 86
    .end local v0    # "itemIndex":I
    .end local v4    # "rowIndex":I
    :cond_3
    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartIndex:I

    if-eq v5, v6, :cond_4

    iget v0, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartIndex:I

    .line 87
    .restart local v0    # "itemIndex":I
    :goto_5
    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartRow:I

    if-eq v5, v6, :cond_5

    iget v4, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mStartRow:I

    .restart local v4    # "rowIndex":I
    :goto_6
    goto :goto_1

    .line 86
    .end local v0    # "itemIndex":I
    .end local v4    # "rowIndex":I
    :cond_4
    const/4 v0, 0x0

    goto :goto_5

    .line 87
    .restart local v0    # "itemIndex":I
    :cond_5
    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    rem-int v4, v0, v5

    goto :goto_6

    .restart local v4    # "rowIndex":I
    :cond_6
    move v3, v6

    .line 92
    goto :goto_2

    .line 93
    .restart local v3    # "minLowRowIndex":I
    :cond_7
    const v2, 0x7fffffff

    goto :goto_3

    .line 98
    .end local v0    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    .restart local v2    # "minLow":I
    :cond_8
    add-int/lit8 v0, v1, -0x1

    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    invoke-virtual {p0, v1, v4}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->prependItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    .line 99
    if-ne v3, v6, :cond_a

    .line 100
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMinLowRowIndex()I

    move-result v3

    .line 101
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v3

    iget v2, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    .line 94
    :cond_9
    add-int/lit8 v4, v4, -0x1

    move v1, v0

    .end local v0    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    goto :goto_4

    .line 102
    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    :cond_a
    if-eq v4, v3, :cond_9

    .line 103
    :goto_7
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    aget-object v5, v5, v4

    iget v5, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-le v5, v2, :cond_9

    .line 104
    if-ltz v0, :cond_0

    .line 107
    add-int/lit8 v1, v0, -0x1

    .end local v0    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    invoke-virtual {p0, v0, v4}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->prependItemToRow(II)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move v0, v1

    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    goto :goto_7

    .line 111
    .end local v0    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    :cond_b
    iget-object v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mRows:[Landroid/support/v17/leanback/widget/StaggeredGrid$Row;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getMaxLowRowIndex()I

    move-result v7

    aget-object v5, v5, v7

    iget v5, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Row;->low:I

    if-gt v5, p1, :cond_c

    move v0, v1

    .line 112
    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    goto/16 :goto_0

    .line 114
    .end local v0    # "itemIndex":I
    .restart local v1    # "itemIndex":I
    :cond_c
    iget v5, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    add-int/lit8 v4, v5, -0x1

    move v0, v1

    .line 115
    .end local v1    # "itemIndex":I
    .restart local v0    # "itemIndex":I
    goto :goto_1
.end method

.method public final stripDownTo(I)V
    .locals 10
    .param p1, "itemIndex"    # I

    .prologue
    .line 123
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v5

    .line 124
    .local v5, "loc":Landroid/support/v17/leanback/widget/StaggeredGrid$Location;
    if-nez v5, :cond_1

    .line 151
    :cond_0
    return-void

    .line 127
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getFirstIndex()I

    move-result v2

    .line 128
    .local v2, "firstIndex":I
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getLastIndex()I

    move-result v4

    .line 129
    .local v4, "lastIndex":I
    iget v6, v5, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    .line 131
    .local v6, "row":I
    move v0, p1

    .line 132
    .local v0, "endIndex":I
    move v1, v6

    .line 133
    .local v1, "endRow":I
    :goto_0
    iget v9, p0, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->mNumRows:I

    add-int/lit8 v9, v9, -0x1

    if-ge v1, v9, :cond_2

    if-ge v0, v4, :cond_2

    .line 134
    add-int/lit8 v0, v0, 0x1

    .line 135
    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v9

    iget v1, v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    goto :goto_0

    .line 138
    :cond_2
    move v7, p1

    .line 139
    .local v7, "startIndex":I
    move v8, v6

    .line 140
    .local v8, "startRow":I
    :goto_1
    if-lez v8, :cond_3

    if-le v7, v2, :cond_3

    .line 141
    add-int/lit8 v7, v7, -0x1

    .line 142
    invoke-virtual {p0, v7}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->getLocation(I)Landroid/support/v17/leanback/widget/StaggeredGrid$Location;

    move-result-object v9

    iget v8, v9, Landroid/support/v17/leanback/widget/StaggeredGrid$Location;->row:I

    goto :goto_1

    .line 145
    :cond_3
    move v3, v2

    .local v3, "i":I
    :goto_2
    if-ge v3, v7, :cond_4

    .line 146
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->removeFirst()V

    .line 145
    add-int/lit8 v3, v3, 0x1

    goto :goto_2

    .line 148
    :cond_4
    move v3, v0

    :goto_3
    if-ge v3, v4, :cond_0

    .line 149
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/StaggeredGridDefault;->removeLast()V

    .line 148
    add-int/lit8 v3, v3, 0x1

    goto :goto_3
.end method
