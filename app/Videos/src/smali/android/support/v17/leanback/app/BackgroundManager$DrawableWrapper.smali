.class Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;
.super Ljava/lang/Object;
.source "BackgroundManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/BackgroundManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "DrawableWrapper"
.end annotation


# instance fields
.field protected mAlpha:I

.field protected mAnimationPending:Z

.field private final mAnimationUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

.field protected mAnimator:Landroid/animation/ValueAnimator;

.field protected mDrawable:Landroid/graphics/drawable/Drawable;

.field private final mInterpolator:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/graphics/drawable/Drawable;)V
    .locals 1
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 194
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 185
    new-instance v0, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v0}, Landroid/view/animation/LinearInterpolator;-><init>()V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mInterpolator:Landroid/view/animation/Interpolator;

    .line 186
    new-instance v0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper$1;-><init>(Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;)V

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimationUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    .line 195
    iput-object p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 196
    const/16 v0, 0xff

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->setAlpha(I)V

    .line 197
    return-void
.end method


# virtual methods
.method public fade(III)V
    .locals 5
    .param p1, "durationMs"    # I
    .param p2, "delayMs"    # I
    .param p3, "alpha"    # I

    .prologue
    const/4 v4, 0x1

    .line 219
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 220
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->cancel()V

    .line 222
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [I

    const/4 v1, 0x0

    invoke-virtual {p0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->getAlpha()I

    move-result v2

    aput v2, v0, v1

    aput p3, v0, v4

    invoke-static {v0}, Landroid/animation/ValueAnimator;->ofInt([I)Landroid/animation/ValueAnimator;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    .line 223
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimationUpdateListener:Landroid/animation/ValueAnimator$AnimatorUpdateListener;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->addUpdateListener(Landroid/animation/ValueAnimator$AnimatorUpdateListener;)V

    .line 224
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    iget-object v1, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mInterpolator:Landroid/view/animation/Interpolator;

    invoke-virtual {v0, v1}, Landroid/animation/ValueAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 225
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v2, p1

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setDuration(J)Landroid/animation/ValueAnimator;

    .line 226
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    int-to-long v2, p2

    invoke-virtual {v0, v2, v3}, Landroid/animation/ValueAnimator;->setStartDelay(J)V

    .line 227
    iput-boolean v4, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimationPending:Z

    .line 228
    return-void
.end method

.method public fadeIn(II)V
    .locals 1
    .param p1, "durationMs"    # I
    .param p2, "delayMs"    # I

    .prologue
    .line 213
    const/16 v0, 0xff

    invoke-virtual {p0, p1, p2, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->fade(III)V

    .line 214
    return-void
.end method

.method public fadeOut(I)V
    .locals 1
    .param p1, "durationMs"    # I

    .prologue
    const/4 v0, 0x0

    .line 216
    invoke-virtual {p0, p1, v0, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->fade(III)V

    .line 217
    return-void
.end method

.method public getAlpha()I
    .locals 1

    .prologue
    .line 207
    iget v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAlpha:I

    return v0
.end method

.method public getDrawable()Landroid/graphics/drawable/Drawable;
    .locals 1

    .prologue
    .line 200
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mDrawable:Landroid/graphics/drawable/Drawable;

    return-object v0
.end method

.method public isAnimationPending()Z
    .locals 1

    .prologue
    .line 230
    iget-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimationPending:Z

    return v0
.end method

.method public isAnimationStarted()Z
    .locals 1

    .prologue
    .line 233
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    if-eqz v0, :cond_0

    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->isStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setAlpha(I)V
    .locals 1
    .param p1, "alpha"    # I

    .prologue
    .line 203
    iput p1, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAlpha:I

    .line 204
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/Drawable;->setAlpha(I)V

    .line 205
    return-void
.end method

.method public setColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 210
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mDrawable:Landroid/graphics/drawable/Drawable;

    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 211
    return-void
.end method

.method public startAnimation()V
    .locals 1

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->startAnimation(Landroid/animation/Animator$AnimatorListener;)V

    .line 237
    return-void
.end method

.method public startAnimation(Landroid/animation/Animator$AnimatorListener;)V
    .locals 1
    .param p1, "listener"    # Landroid/animation/Animator$AnimatorListener;

    .prologue
    .line 239
    if-eqz p1, :cond_0

    .line 240
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0, p1}, Landroid/animation/ValueAnimator;->addListener(Landroid/animation/Animator$AnimatorListener;)V

    .line 242
    :cond_0
    iget-object v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimator:Landroid/animation/ValueAnimator;

    invoke-virtual {v0}, Landroid/animation/ValueAnimator;->start()V

    .line 243
    const/4 v0, 0x0

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/BackgroundManager$DrawableWrapper;->mAnimationPending:Z

    .line 244
    return-void
.end method
