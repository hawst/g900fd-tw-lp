.class public final Landroid/support/v17/leanback/R$layout;
.super Ljava/lang/Object;
.source "R.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/R;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "layout"
.end annotation


# static fields
.field public static final lb_action_1_line:I = 0x7f040046

.field public static final lb_action_2_lines:I = 0x7f040047

.field public static final lb_browse_fragment:I = 0x7f040049

.field public static final lb_card_color_overlay:I = 0x7f04004b

.field public static final lb_control_bar:I = 0x7f04004e

.field public static final lb_control_button_primary:I = 0x7f04004f

.field public static final lb_control_button_secondary:I = 0x7f040050

.field public static final lb_details_description:I = 0x7f040051

.field public static final lb_details_fragment:I = 0x7f040052

.field public static final lb_details_overview:I = 0x7f040053

.field public static final lb_error_fragment:I = 0x7f040058

.field public static final lb_header:I = 0x7f040059

.field public static final lb_headers_fragment:I = 0x7f04005a

.field public static final lb_image_card_view:I = 0x7f04005b

.field public static final lb_list_row:I = 0x7f04005c

.field public static final lb_list_row_hovercard:I = 0x7f04005d

.field public static final lb_playback_controls:I = 0x7f04005e

.field public static final lb_playback_controls_row:I = 0x7f04005f

.field public static final lb_row_container:I = 0x7f040060

.field public static final lb_row_header:I = 0x7f040061

.field public static final lb_rows_fragment:I = 0x7f040062

.field public static final lb_search_bar:I = 0x7f040063

.field public static final lb_search_orb:I = 0x7f040065

.field public static final lb_shadow:I = 0x7f040066

.field public static final lb_speech_orb:I = 0x7f040067

.field public static final lb_title_view:I = 0x7f040068

.field public static final lb_vertical_grid:I = 0x7f040069

.field public static final lb_vertical_grid_fragment:I = 0x7f04006a
