.class Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;
.super Ljava/lang/Object;
.source "PlaybackOverlayFragment.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->loadOtherRowAnimator()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

.field final synthetic val$listener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;)V
    .locals 0

    .prologue
    .line 397
    iput-object p1, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    iput-object p2, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;->val$listener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 5
    .param p1, "arg0"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 400
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Float;

    invoke-virtual {v3}, Ljava/lang/Float;->floatValue()F

    move-result v0

    .line 401
    .local v0, "fraction":F
    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;->val$listener:Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;

    iget-object v3, v3, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$AnimatorListener;->mViews:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :cond_0
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_1

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/view/View;

    .line 402
    .local v2, "view":Landroid/view/View;
    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    invoke-virtual {v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->getChildPosition(Landroid/view/View;)I

    move-result v3

    if-lez v3, :cond_0

    .line 403
    invoke-virtual {v2, v0}, Landroid/view/View;->setAlpha(F)V

    .line 404
    iget-object v3, p0, Landroid/support/v17/leanback/app/PlaybackOverlayFragment$10;->this$0:Landroid/support/v17/leanback/app/PlaybackOverlayFragment;

    # getter for: Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->mAnimationTranslateY:I
    invoke-static {v3}, Landroid/support/v17/leanback/app/PlaybackOverlayFragment;->access$1200(Landroid/support/v17/leanback/app/PlaybackOverlayFragment;)I

    move-result v3

    int-to-float v3, v3

    const/high16 v4, 0x3f800000    # 1.0f

    sub-float/2addr v4, v0

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Landroid/view/View;->setTranslationY(F)V

    goto :goto_0

    .line 407
    .end local v2    # "view":Landroid/view/View;
    :cond_1
    return-void
.end method
