.class public Landroid/support/v17/leanback/widget/RowHeaderPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "RowHeaderPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;
    }
.end annotation


# instance fields
.field private final mFontMeasurePaint:Landroid/graphics/Paint;

.field private final mLayoutResourceId:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 34
    sget v0, Landroid/support/v17/leanback/R$layout;->lb_row_header:I

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;-><init>(I)V

    .line 35
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "layoutResourceId"    # I

    .prologue
    .line 40
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    .line 31
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->mFontMeasurePaint:Landroid/graphics/Paint;

    .line 41
    iput p1, p0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->mLayoutResourceId:I

    .line 42
    return-void
.end method

.method protected static getFontDescent(Landroid/widget/TextView;Landroid/graphics/Paint;)F
    .locals 2
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "fontMeasurePaint"    # Landroid/graphics/Paint;

    .prologue
    .line 109
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTextSize()F

    move-result v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    cmpl-float v0, v0, v1

    if-eqz v0, :cond_0

    .line 110
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 112
    :cond_0
    invoke-virtual {p1}, Landroid/graphics/Paint;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    if-eq v0, v1, :cond_1

    .line 113
    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v0

    invoke-virtual {p1, v0}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 115
    :cond_1
    invoke-virtual {p1}, Landroid/graphics/Paint;->descent()F

    move-result v0

    return v0
.end method


# virtual methods
.method public getSpaceUnderBaseline(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;)I
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    .prologue
    .line 101
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v0

    .line 102
    .local v0, "space":I
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->view:Landroid/view/View;

    instance-of v1, v1, Landroid/widget/TextView;

    if-eqz v1, :cond_0

    .line 103
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->view:Landroid/view/View;

    check-cast v1, Landroid/widget/TextView;

    iget-object v2, p0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->mFontMeasurePaint:Landroid/graphics/Paint;

    invoke-static {v1, v2}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->getFontDescent(Landroid/widget/TextView;Landroid/graphics/Paint;)F

    move-result v1

    float-to-int v1, v1

    add-int/2addr v0, v1

    .line 105
    :cond_0
    return v0
.end method

.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 5
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 71
    move-object v3, p1

    check-cast v3, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    const/4 v4, 0x0

    invoke-virtual {p0, v3, v4}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->setSelectLevel(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;F)V

    move-object v1, p2

    .line 72
    check-cast v1, Landroid/support/v17/leanback/widget/Row;

    .line 73
    .local v1, "rowItem":Landroid/support/v17/leanback/widget/Row;
    if-eqz v1, :cond_0

    .line 74
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/Row;->getHeaderItem()Landroid/support/v17/leanback/widget/HeaderItem;

    move-result-object v0

    .line 75
    .local v0, "headerItem":Landroid/support/v17/leanback/widget/HeaderItem;
    if-eqz v0, :cond_0

    .line 76
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/HeaderItem;->getName()Ljava/lang/String;

    move-result-object v2

    .line 77
    .local v2, "text":Ljava/lang/String;
    iget-object v3, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v3, Landroid/support/v17/leanback/widget/RowHeaderView;

    invoke-virtual {v3, v2}, Landroid/support/v17/leanback/widget/RowHeaderView;->setText(Ljava/lang/CharSequence;)V

    .line 80
    .end local v0    # "headerItem":Landroid/support/v17/leanback/widget/HeaderItem;
    .end local v2    # "text":Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 6
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const/4 v5, 0x1

    .line 59
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v2

    iget v3, p0, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->mLayoutResourceId:I

    const/4 v4, 0x0

    invoke-virtual {v2, v3, p1, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/RowHeaderView;

    .line 62
    .local v0, "headerView":Landroid/support/v17/leanback/widget/RowHeaderView;
    new-instance v1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    invoke-direct {v1, v0}, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;-><init>(Landroid/view/View;)V

    .line 63
    .local v1, "viewHolder":Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/RowHeaderView;->getCurrentTextColor()I

    move-result v2

    iput v2, v1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mOriginalTextColor:I

    .line 64
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    sget v3, Landroid/support/v17/leanback/R$fraction;->lb_browse_header_unselect_alpha:I

    invoke-virtual {v2, v3, v5, v5}, Landroid/content/res/Resources;->getFraction(III)F

    move-result v2

    iput v2, v1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mUnselectAlpha:F

    .line 66
    return-object v1
.end method

.method protected onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;)V
    .locals 5
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    .prologue
    .line 92
    iget-object v0, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->view:Landroid/view/View;

    iget v1, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mUnselectAlpha:F

    iget v2, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mSelectLevel:F

    const/high16 v3, 0x3f800000    # 1.0f

    iget v4, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mUnselectAlpha:F

    sub-float/2addr v3, v4

    mul-float/2addr v2, v3

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/View;->setAlpha(F)V

    .line 94
    return-void
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 84
    return-void
.end method

.method public final setSelectLevel(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;F)V
    .locals 0
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;
    .param p2, "selectLevel"    # F

    .prologue
    .line 87
    iput p2, p1, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->mSelectLevel:F

    .line 88
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;)V

    .line 89
    return-void
.end method
