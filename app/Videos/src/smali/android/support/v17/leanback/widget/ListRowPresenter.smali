.class public Landroid/support/v17/leanback/widget/ListRowPresenter;
.super Landroid/support/v17/leanback/widget/RowPresenter;
.source "ListRowPresenter.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    }
.end annotation


# static fields
.field private static sExpandedRowNoHovercardBottomPadding:I

.field private static sExpandedSelectedRowTopPadding:I

.field private static sSelectedRowTopPadding:I


# instance fields
.field private mBrowseRowsFadingEdgeLength:I

.field private mCardWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

.field private mExpandedRowHeight:I

.field private mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

.field private mRoundedCornersEnabled:Z

.field private mRowHeight:I

.field private mShadowEnabled:Z

.field private mZoomFactor:I


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 100
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>(I)V

    .line 101
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "zoomFactor"    # I

    .prologue
    const/4 v1, 0x1

    .line 112
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/RowPresenter;-><init>()V

    .line 87
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mShadowEnabled:Z

    .line 88
    const/4 v0, -0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mBrowseRowsFadingEdgeLength:I

    .line 89
    iput-boolean v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRoundedCornersEnabled:Z

    .line 160
    new-instance v0, Landroid/support/v17/leanback/widget/ListRowPresenter$1;

    invoke-direct {v0, p0}, Landroid/support/v17/leanback/widget/ListRowPresenter$1;-><init>(Landroid/support/v17/leanback/widget/ListRowPresenter;)V

    iput-object v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mCardWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    .line 113
    iput p1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mZoomFactor:I

    .line 114
    return-void
.end method

.method static synthetic access$000(Landroid/support/v17/leanback/widget/ListRowPresenter;Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;Landroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Landroid/support/v17/leanback/widget/ListRowPresenter;
    .param p1, "x1"    # Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 45
    invoke-direct {p0, p1, p2}, Landroid/support/v17/leanback/widget/ListRowPresenter;->selectChildView(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;Landroid/view/View;)V

    return-void
.end method

.method private getSpaceUnderBaseline(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)I
    .locals 2
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .prologue
    .line 315
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getHeaderViewHolder()Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;

    move-result-object v0

    .line 316
    .local v0, "headerViewHolder":Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;
    if-eqz v0, :cond_1

    .line 317
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getHeaderPresenter()Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    move-result-object v1

    if-eqz v1, :cond_0

    .line 318
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getHeaderPresenter()Landroid/support/v17/leanback/widget/RowHeaderPresenter;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/support/v17/leanback/widget/RowHeaderPresenter;->getSpaceUnderBaseline(Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;)I

    move-result v1

    .line 322
    :goto_0
    return v1

    .line 320
    :cond_0
    iget-object v1, v0, Landroid/support/v17/leanback/widget/RowHeaderPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getPaddingBottom()I

    move-result v1

    goto :goto_0

    .line 322
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static initStatics(Landroid/content/Context;)V
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 304
    sget v0, Landroid/support/v17/leanback/widget/ListRowPresenter;->sSelectedRowTopPadding:I

    if-nez v0, :cond_0

    .line 305
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_browse_selected_row_top_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/ListRowPresenter;->sSelectedRowTopPadding:I

    .line 307
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_browse_expanded_selected_row_top_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/ListRowPresenter;->sExpandedSelectedRowTopPadding:I

    .line 309
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    sget v1, Landroid/support/v17/leanback/R$dimen;->lb_browse_expanded_row_no_hovercard_bottom_padding:I

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    sput v0, Landroid/support/v17/leanback/widget/ListRowPresenter;->sExpandedRowNoHovercardBottomPadding:I

    .line 312
    :cond_0
    return-void
.end method

.method private selectChildView(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;Landroid/view/View;)V
    .locals 5
    .param p1, "rowViewHolder"    # Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    const/4 v3, 0x0

    .line 272
    const/4 v0, 0x0

    .line 273
    .local v0, "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    if-eqz p2, :cond_0

    .line 274
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v1, p2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getChildViewHolder(Landroid/view/View;)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 277
    .restart local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_0
    if-nez p2, :cond_4

    .line 278
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_1

    .line 279
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mHoverCardViewSwitcher:Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;->unselect()V

    .line 281
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_2

    .line 282
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    invoke-interface {v1, v3, v3, p1, v2}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 285
    :cond_2
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 286
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    iget-object v2, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    invoke-interface {v1, v3, v2}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    .line 301
    :cond_3
    :goto_0
    return-void

    .line 288
    :cond_4
    iget-boolean v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mExpanded:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mSelected:Z

    if-eqz v1, :cond_3

    .line 289
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_5

    .line 290
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mHoverCardViewSwitcher:Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;

    iget-object v2, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    invoke-virtual {v1, v2, p2, v3}, Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;->select(Landroid/support/v17/leanback/widget/HorizontalGridView;Landroid/view/View;Ljava/lang/Object;)V

    .line 293
    :cond_5
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_6

    .line 294
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemViewSelectedListener()Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;

    move-result-object v1

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mHolder:Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    iget-object v3, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    iget-object v4, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    invoke-interface {v1, v2, v3, p1, v4}, Landroid/support/v17/leanback/widget/OnItemViewSelectedListener;->onItemSelected(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V

    .line 297
    :cond_6
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    if-eqz v1, :cond_3

    .line 298
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getOnItemSelectedListener()Landroid/support/v17/leanback/widget/OnItemSelectedListener;

    move-result-object v1

    iget-object v2, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->mItem:Ljava/lang/Object;

    iget-object v3, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mRow:Landroid/support/v17/leanback/widget/Row;

    invoke-interface {v1, v2, v3}, Landroid/support/v17/leanback/widget/OnItemSelectedListener;->onItemSelected(Ljava/lang/Object;Landroid/support/v17/leanback/widget/Row;)V

    goto :goto_0
.end method

.method private setVerticalPadding(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V
    .locals 6
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .prologue
    .line 328
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->isExpanded()Z

    move-result v3

    if-eqz v3, :cond_2

    .line 329
    invoke-direct {p0, p1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getSpaceUnderBaseline(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)I

    move-result v0

    .line 331
    .local v0, "headerSpaceUnderBaseline":I
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_0

    sget v3, Landroid/support/v17/leanback/widget/ListRowPresenter;->sExpandedSelectedRowTopPadding:I

    :goto_0
    sub-int v2, v3, v0

    .line 333
    .local v2, "paddingTop":I
    iget-object v3, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-nez v3, :cond_1

    sget v1, Landroid/support/v17/leanback/widget/ListRowPresenter;->sExpandedRowNoHovercardBottomPadding:I

    .line 342
    .end local v0    # "headerSpaceUnderBaseline":I
    .local v1, "paddingBottom":I
    :goto_1
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v3

    iget v4, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingLeft:I

    iget v5, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingRight:I

    invoke-virtual {v3, v4, v2, v5, v1}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setPadding(IIII)V

    .line 344
    return-void

    .line 331
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingTop":I
    .restart local v0    # "headerSpaceUnderBaseline":I
    :cond_0
    iget v3, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingTop:I

    goto :goto_0

    .line 333
    .restart local v2    # "paddingTop":I
    :cond_1
    iget v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingBottom:I

    goto :goto_1

    .line 335
    .end local v0    # "headerSpaceUnderBaseline":I
    .end local v2    # "paddingTop":I
    :cond_2
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->isSelected()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 336
    sget v3, Landroid/support/v17/leanback/widget/ListRowPresenter;->sSelectedRowTopPadding:I

    iget v4, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingBottom:I

    sub-int v2, v3, v4

    .line 337
    .restart local v2    # "paddingTop":I
    sget v1, Landroid/support/v17/leanback/widget/ListRowPresenter;->sSelectedRowTopPadding:I

    .restart local v1    # "paddingBottom":I
    goto :goto_1

    .line 339
    .end local v1    # "paddingBottom":I
    .end local v2    # "paddingTop":I
    :cond_3
    const/4 v2, 0x0

    .line 340
    .restart local v2    # "paddingTop":I
    iget v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mPaddingBottom:I

    .restart local v1    # "paddingBottom":I
    goto :goto_1
.end method

.method private setupFadingEffect(Landroid/support/v17/leanback/widget/ListRowView;)V
    .locals 4
    .param p1, "rowView"    # Landroid/support/v17/leanback/widget/ListRowView;

    .prologue
    .line 387
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/ListRowView;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v0

    .line 388
    .local v0, "gridView":Landroid/support/v17/leanback/widget/HorizontalGridView;
    iget v2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mBrowseRowsFadingEdgeLength:I

    if-gez v2, :cond_0

    .line 389
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getContext()Landroid/content/Context;

    move-result-object v2

    sget-object v3, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme:[I

    invoke-virtual {v2, v3}, Landroid/content/Context;->obtainStyledAttributes([I)Landroid/content/res/TypedArray;

    move-result-object v1

    .line 391
    .local v1, "ta":Landroid/content/res/TypedArray;
    sget v2, Landroid/support/v17/leanback/R$styleable;->LeanbackTheme_browseRowsFadingEdgeLength:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, v3}, Landroid/content/res/TypedArray;->getDimension(IF)F

    move-result v2

    float-to-int v2, v2

    iput v2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mBrowseRowsFadingEdgeLength:I

    .line 393
    invoke-virtual {v1}, Landroid/content/res/TypedArray;->recycle()V

    .line 395
    .end local v1    # "ta":Landroid/content/res/TypedArray;
    :cond_0
    iget v2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mBrowseRowsFadingEdgeLength:I

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setFadingLeftEdgeLength(I)V

    .line 396
    return-void
.end method

.method private updateFooterViewSwitcher(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V
    .locals 4
    .param p1, "vh"    # Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .prologue
    .line 369
    iget-boolean v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mExpanded:Z

    if-eqz v1, :cond_3

    iget-boolean v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mSelected:Z

    if-eqz v1, :cond_3

    .line 370
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_0

    .line 371
    iget-object v2, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mHoverCardViewSwitcher:Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;

    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->view:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    iget-object v3, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    invoke-virtual {v2, v1, v3}, Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;->init(Landroid/view/ViewGroup;Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 374
    :cond_0
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    iget-object v2, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getSelectedPosition()I

    move-result v2

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->findViewHolderForPosition(I)Landroid/support/v7/widget/RecyclerView$ViewHolder;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;

    .line 377
    .local v0, "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    if-nez v0, :cond_2

    const/4 v1, 0x0

    :goto_0
    invoke-direct {p0, p1, v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->selectChildView(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;Landroid/view/View;)V

    .line 383
    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_1
    :goto_1
    return-void

    .line 377
    .restart local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_2
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;->itemView:Landroid/view/View;

    goto :goto_0

    .line 379
    .end local v0    # "ibh":Landroid/support/v17/leanback/widget/ItemBridgeAdapter$ViewHolder;
    :cond_3
    iget-object v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    if-eqz v1, :cond_1

    .line 380
    iget-object v1, p1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mHoverCardViewSwitcher:Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/HorizontalHoverCardSwitcher;->unselect()V

    goto :goto_1
.end method


# virtual methods
.method public final areChildRoundedCornersEnabled()Z
    .locals 1

    .prologue
    .line 494
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRoundedCornersEnabled:Z

    return v0
.end method

.method public canDrawOutOfBounds()Z
    .locals 1

    .prologue
    .line 503
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->needsDefaultShadow()Z

    move-result v0

    return v0
.end method

.method protected createRowViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .locals 3
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 348
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->initStatics(Landroid/content/Context;)V

    .line 349
    new-instance v0, Landroid/support/v17/leanback/widget/ListRowView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/support/v17/leanback/widget/ListRowView;-><init>(Landroid/content/Context;)V

    .line 350
    .local v0, "rowView":Landroid/support/v17/leanback/widget/ListRowView;
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setupFadingEffect(Landroid/support/v17/leanback/widget/ListRowView;)V

    .line 351
    iget v1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRowHeight:I

    if-eqz v1, :cond_0

    .line 352
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowView;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v1

    iget v2, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRowHeight:I

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setRowHeight(I)V

    .line 354
    :cond_0
    new-instance v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRowView;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v2

    invoke-direct {v1, v0, v2, p0}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;-><init>(Landroid/view/View;Landroid/support/v17/leanback/widget/HorizontalGridView;Landroid/support/v17/leanback/widget/ListRowPresenter;)V

    return-object v1
.end method

.method public freeze(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "freeze"    # Z

    .prologue
    .line 536
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 537
    .local v0, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    iget-object v2, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    if-nez p2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setScrollEnabled(Z)V

    .line 538
    return-void

    .line 537
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public getExpandedRowHeight()I
    .locals 1

    .prologue
    .line 150
    iget v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mExpandedRowHeight:I

    if-eqz v0, :cond_0

    iget v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mExpandedRowHeight:I

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRowHeight:I

    goto :goto_0
.end method

.method public getRowHeight()I
    .locals 1

    .prologue
    .line 131
    iget v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mRowHeight:I

    return v0
.end method

.method public final getShadowEnabled()Z
    .locals 1

    .prologue
    .line 479
    iget-boolean v0, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mShadowEnabled:Z

    return v0
.end method

.method protected initializeRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    const/4 v2, 0x0

    .line 179
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->initializeRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    move-object v0, p1

    .line 180
    check-cast v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 181
    .local v0, "rowViewHolder":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->needsDefaultListSelectEffect()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->needsDefaultShadow()Z

    move-result v1

    if-nez v1, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->areChildRoundedCornersEnabled()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 183
    :cond_0
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget-object v3, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mCardWrapper:Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;

    invoke-virtual {v1, v3}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setWrapper(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$Wrapper;)V

    .line 185
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->needsDefaultListSelectEffect()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 186
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-static {v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->prepareParentForShadow(Landroid/view/ViewGroup;)V

    .line 187
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->view:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 188
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mContainerViewHolder:Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    if-eqz v1, :cond_2

    .line 189
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mContainerViewHolder:Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;

    iget-object v1, v1, Landroid/support/v17/leanback/widget/RowPresenter$ContainerViewHolder;->view:Landroid/view/View;

    check-cast v1, Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 192
    :cond_2
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    iget v3, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mZoomFactor:I

    invoke-static {v1, v3, v2}, Landroid/support/v17/leanback/widget/FocusHighlightHelper;->setupBrowseItemFocusHighlight(Landroid/support/v17/leanback/widget/ItemBridgeAdapter;IZ)V

    .line 194
    iget-object v3, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->isUsingZOrder()Z

    move-result v1

    if-nez v1, :cond_3

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v1}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setFocusDrawingOrderEnabled(Z)V

    .line 195
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    new-instance v2, Landroid/support/v17/leanback/widget/ListRowPresenter$2;

    invoke-direct {v2, p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter$2;-><init>(Landroid/support/v17/leanback/widget/ListRowPresenter;Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setOnChildSelectedListener(Landroid/support/v17/leanback/widget/OnChildSelectedListener;)V

    .line 202
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    new-instance v2, Landroid/support/v17/leanback/widget/ListRowPresenter$3;

    invoke-direct {v2, p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter$3;-><init>(Landroid/support/v17/leanback/widget/ListRowPresenter;Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapterListener(Landroid/support/v17/leanback/widget/ItemBridgeAdapter$AdapterListener;)V

    .line 248
    return-void

    :cond_3
    move v1, v2

    .line 194
    goto :goto_0
.end method

.method public isUsingDefaultListSelectEffect()Z
    .locals 1

    .prologue
    .line 443
    const/4 v0, 0x1

    return v0
.end method

.method public final isUsingDefaultSelectEffect()Z
    .locals 1

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method public isUsingDefaultShadow()Z
    .locals 1

    .prologue
    .line 452
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->supportsShadow()Z

    move-result v0

    return v0
.end method

.method public isUsingZOrder()Z
    .locals 1

    .prologue
    .line 461
    invoke-static {}, Landroid/support/v17/leanback/widget/ShadowHelper;->getInstance()Landroid/support/v17/leanback/widget/ShadowHelper;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ShadowHelper;->usesZShadow()Z

    move-result v0

    return v0
.end method

.method final needsDefaultListSelectEffect()Z
    .locals 1

    .prologue
    .line 251
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->isUsingDefaultListSelectEffect()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getSelectEffectEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method final needsDefaultShadow()Z
    .locals 1

    .prologue
    .line 498
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->isUsingDefaultShadow()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getShadowEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 412
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    move-object v1, p1

    .line 413
    check-cast v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .local v1, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    move-object v0, p2

    .line 414
    check-cast v0, Landroid/support/v17/leanback/widget/ListRow;

    .line 415
    .local v0, "rowItem":Landroid/support/v17/leanback/widget/ListRow;
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ListRow;->getAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 416
    iget-object v2, v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    iget-object v3, v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v2, v3}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 417
    return-void
.end method

.method protected onRowViewExpanded(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "expanded"    # Z

    .prologue
    .line 400
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewExpanded(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    move-object v1, p1

    .line 401
    check-cast v1, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 402
    .local v1, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getRowHeight()I

    move-result v2

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getExpandedRowHeight()I

    move-result v3

    if-eq v2, v3, :cond_0

    .line 403
    if-eqz p2, :cond_1

    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getExpandedRowHeight()I

    move-result v0

    .line 404
    .local v0, "newHeight":I
    :goto_0
    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->getGridView()Landroid/support/v17/leanback/widget/HorizontalGridView;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setRowHeight(I)V

    .line 406
    .end local v0    # "newHeight":I
    :cond_0
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setVerticalPadding(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    .line 407
    invoke-direct {p0, v1}, Landroid/support/v17/leanback/widget/ListRowPresenter;->updateFooterViewSwitcher(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    .line 408
    return-void

    .line 403
    :cond_1
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->getRowHeight()I

    move-result v0

    goto :goto_0
.end method

.method protected onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V
    .locals 1
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "selected"    # Z

    .prologue
    .line 359
    invoke-super {p0, p1, p2}, Landroid/support/v17/leanback/widget/RowPresenter;->onRowViewSelected(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Z)V

    move-object v0, p1

    .line 360
    check-cast v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 361
    .local v0, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->setVerticalPadding(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    .line 362
    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->updateFooterViewSwitcher(Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;)V

    .line 363
    return-void
.end method

.method protected onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 6
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 520
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->onSelectLevelChanged(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 521
    invoke-virtual {p0}, Landroid/support/v17/leanback/widget/ListRowPresenter;->needsDefaultListSelectEffect()Z

    move-result v5

    if-eqz v5, :cond_1

    move-object v3, p1

    .line 522
    check-cast v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 523
    .local v3, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    iget-object v5, v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mColorDimmer:Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;

    invoke-virtual {v5}, Landroid/support/v17/leanback/graphics/ColorOverlayDimmer;->getPaint()Landroid/graphics/Paint;

    move-result-object v5

    invoke-virtual {v5}, Landroid/graphics/Paint;->getColor()I

    move-result v1

    .line 524
    .local v1, "dimmedColor":I
    const/4 v2, 0x0

    .local v2, "i":I
    iget-object v5, v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getChildCount()I

    move-result v0

    .local v0, "count":I
    :goto_0
    if-ge v2, v0, :cond_0

    .line 525
    iget-object v5, v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v5, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getChildAt(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;

    .line 526
    .local v4, "wrapper":Landroid/support/v17/leanback/widget/ShadowOverlayContainer;
    invoke-virtual {v4, v1}, Landroid/support/v17/leanback/widget/ShadowOverlayContainer;->setOverlayColor(I)V

    .line 524
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 528
    .end local v4    # "wrapper":Landroid/support/v17/leanback/widget/ShadowOverlayContainer;
    :cond_0
    iget-object v5, v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/HorizontalGridView;->getFadingLeftEdge()Z

    move-result v5

    if-eqz v5, :cond_1

    .line 529
    iget-object v5, v3, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    invoke-virtual {v5}, Landroid/support/v17/leanback/widget/HorizontalGridView;->invalidate()V

    .line 532
    .end local v0    # "count":I
    .end local v1    # "dimmedColor":I
    .end local v2    # "i":I
    .end local v3    # "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    :cond_1
    return-void
.end method

.method protected onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 3
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    .line 421
    move-object v0, p1

    check-cast v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;

    .line 422
    .local v0, "vh":Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mGridView:Landroid/support/v17/leanback/widget/HorizontalGridView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/HorizontalGridView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 423
    iget-object v1, v0, Landroid/support/v17/leanback/widget/ListRowPresenter$ViewHolder;->mItemBridgeAdapter:Landroid/support/v17/leanback/widget/ItemBridgeAdapter;

    invoke-virtual {v1}, Landroid/support/v17/leanback/widget/ItemBridgeAdapter;->clear()V

    .line 424
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/RowPresenter;->onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 425
    return-void
.end method

.method public final setHoverCardPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V
    .locals 0
    .param p1, "selector"    # Landroid/support/v17/leanback/widget/PresenterSelector;

    .prologue
    .line 258
    iput-object p1, p0, Landroid/support/v17/leanback/widget/ListRowPresenter;->mHoverCardPresenterSelector:Landroid/support/v17/leanback/widget/PresenterSelector;

    .line 259
    return-void
.end method
