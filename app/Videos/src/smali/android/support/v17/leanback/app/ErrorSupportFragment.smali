.class public Landroid/support/v17/leanback/app/ErrorSupportFragment;
.super Landroid/support/v4/app/Fragment;
.source "ErrorSupportFragment.java"


# instance fields
.field private mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private mBadgeDrawable:Landroid/graphics/drawable/Drawable;

.field private mButton:Landroid/widget/Button;

.field private mButtonClickListener:Landroid/view/View$OnClickListener;

.field private mButtonText:Ljava/lang/String;

.field private mDrawable:Landroid/graphics/drawable/Drawable;

.field private mErrorFrame:Landroid/view/View;

.field private mImageView:Landroid/widget/ImageView;

.field private mIsBackgroundTranslucent:Z

.field private mMessage:Ljava/lang/CharSequence;

.field private mTextView:Landroid/widget/TextView;

.field private mTitle:Ljava/lang/String;

.field private mTitleView:Landroid/support/v17/leanback/widget/TitleView;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 39
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mIsBackgroundTranslucent:Z

    return-void
.end method

.method private static getFontMetricsInt(Landroid/widget/TextView;)Landroid/graphics/Paint$FontMetricsInt;
    .locals 2
    .param p0, "textView"    # Landroid/widget/TextView;

    .prologue
    .line 284
    new-instance v0, Landroid/graphics/Paint;

    const/4 v1, 0x1

    invoke-direct {v0, v1}, Landroid/graphics/Paint;-><init>(I)V

    .line 285
    .local v0, "paint":Landroid/graphics/Paint;
    invoke-virtual {p0}, Landroid/widget/TextView;->getTextSize()F

    move-result v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTextSize(F)V

    .line 286
    invoke-virtual {p0}, Landroid/widget/TextView;->getTypeface()Landroid/graphics/Typeface;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setTypeface(Landroid/graphics/Typeface;)Landroid/graphics/Typeface;

    .line 287
    invoke-virtual {v0}, Landroid/graphics/Paint;->getFontMetricsInt()Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v1

    return-object v1
.end method

.method private static setTopMargin(Landroid/widget/TextView;I)V
    .locals 1
    .param p0, "textView"    # Landroid/widget/TextView;
    .param p1, "topMargin"    # I

    .prologue
    .line 291
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup$MarginLayoutParams;

    .line 292
    .local v0, "lp":Landroid/view/ViewGroup$MarginLayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$MarginLayoutParams;->topMargin:I

    .line 293
    invoke-virtual {p0, v0}, Landroid/widget/TextView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 294
    return-void
.end method

.method private updateBackground()V
    .locals 3

    .prologue
    .line 232
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    if-eqz v0, :cond_0

    .line 233
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    if-eqz v0, :cond_1

    .line 234
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/view/View;->setBackground(Landroid/graphics/drawable/Drawable;)V

    .line 242
    :cond_0
    :goto_0
    return-void

    .line 236
    :cond_1
    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mIsBackgroundTranslucent:Z

    if-eqz v0, :cond_2

    sget v0, Landroid/support/v17/leanback/R$color;->lb_error_background_color_translucent:I

    :goto_1
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/view/View;->setBackgroundColor(I)V

    goto :goto_0

    :cond_2
    sget v0, Landroid/support/v17/leanback/R$color;->lb_error_background_color_opaque:I

    goto :goto_1
.end method

.method private updateButton()V
    .locals 2

    .prologue
    .line 269
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    if-eqz v0, :cond_0

    .line 270
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButtonText:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 271
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButtonClickListener:Landroid/view/View$OnClickListener;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 272
    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButtonText:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/Button;->setVisibility(I)V

    .line 273
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 275
    :cond_0
    return-void

    .line 272
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateImageDrawable()V
    .locals 2

    .prologue
    .line 262
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mImageView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mImageView:Landroid/widget/ImageView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 264
    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mImageView:Landroid/widget/ImageView;

    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mDrawable:Landroid/graphics/drawable/Drawable;

    if-nez v0, :cond_1

    const/16 v0, 0x8

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 266
    :cond_0
    return-void

    .line 264
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private updateMessage()V
    .locals 3

    .prologue
    .line 252
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 253
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mMessage:Ljava/lang/CharSequence;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 254
    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    invoke-virtual {v0}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    iget-boolean v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mIsBackgroundTranslucent:Z

    if-eqz v0, :cond_1

    sget v0, Landroid/support/v17/leanback/R$color;->lb_error_message_color_on_translucent:I

    :goto_0
    invoke-virtual {v2, v0}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setTextColor(I)V

    .line 257
    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mMessage:Ljava/lang/CharSequence;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    const/16 v0, 0x8

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 259
    :cond_0
    return-void

    .line 254
    :cond_1
    sget v0, Landroid/support/v17/leanback/R$color;->lb_error_message_color_on_opaque:I

    goto :goto_0

    .line 257
    :cond_2
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private updateTitle()V
    .locals 2

    .prologue
    .line 245
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    if-eqz v0, :cond_0

    .line 246
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/TitleView;->setTitle(Ljava/lang/String;)V

    .line 247
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    iget-object v1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mBadgeDrawable:Landroid/graphics/drawable/Drawable;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/widget/TitleView;->setBadgeDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 249
    :cond_0
    return-void
.end method


# virtual methods
.method public getMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 162
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mMessage:Ljava/lang/CharSequence;

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 202
    sget v4, Landroid/support/v17/leanback/R$layout;->lb_error_fragment:I

    const/4 v5, 0x0

    invoke-virtual {p1, v4, p2, v5}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 204
    .local v1, "root":Landroid/view/View;
    sget v4, Landroid/support/v17/leanback/R$id;->error_frame:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    iput-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    .line 205
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateBackground()V

    .line 207
    sget v4, Landroid/support/v17/leanback/R$id;->image:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/ImageView;

    iput-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mImageView:Landroid/widget/ImageView;

    .line 208
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateImageDrawable()V

    .line 210
    sget v4, Landroid/support/v17/leanback/R$id;->message:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/TextView;

    iput-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    .line 211
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateMessage()V

    .line 213
    sget v4, Landroid/support/v17/leanback/R$id;->button:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/widget/Button;

    iput-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    .line 214
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateButton()V

    .line 216
    sget v4, Landroid/support/v17/leanback/R$id;->browse_title_group:I

    invoke-virtual {v1, v4}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    check-cast v4, Landroid/support/v17/leanback/widget/TitleView;

    iput-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitleView:Landroid/support/v17/leanback/widget/TitleView;

    .line 217
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateTitle()V

    .line 219
    iget-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    invoke-static {v4}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->getFontMetricsInt(Landroid/widget/TextView;)Landroid/graphics/Paint$FontMetricsInt;

    move-result-object v0

    .line 220
    .local v0, "metrics":Landroid/graphics/Paint$FontMetricsInt;
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Landroid/support/v17/leanback/R$dimen;->lb_error_under_image_baseline_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v2

    .line 222
    .local v2, "underImageBaselineMargin":I
    iget-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTextView:Landroid/widget/TextView;

    iget v5, v0, Landroid/graphics/Paint$FontMetricsInt;->ascent:I

    add-int/2addr v5, v2

    invoke-static {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setTopMargin(Landroid/widget/TextView;I)V

    .line 224
    invoke-virtual {p2}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    sget v5, Landroid/support/v17/leanback/R$dimen;->lb_error_under_message_baseline_margin:I

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v3

    .line 226
    .local v3, "underMessageBaselineMargin":I
    iget-object v4, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButton:Landroid/widget/Button;

    iget v5, v0, Landroid/graphics/Paint$FontMetricsInt;->descent:I

    sub-int v5, v3, v5

    invoke-static {v4, v5}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->setTopMargin(Landroid/widget/TextView;I)V

    .line 228
    return-object v1
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 279
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 280
    iget-object v0, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mErrorFrame:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    .line 281
    return-void
.end method

.method public setButtonClickListener(Landroid/view/View$OnClickListener;)V
    .locals 0
    .param p1, "clickListener"    # Landroid/view/View$OnClickListener;

    .prologue
    .line 188
    iput-object p1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButtonClickListener:Landroid/view/View$OnClickListener;

    .line 189
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateButton()V

    .line 190
    return-void
.end method

.method public setButtonText(Ljava/lang/String;)V
    .locals 0
    .param p1, "text"    # Ljava/lang/String;

    .prologue
    .line 171
    iput-object p1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mButtonText:Ljava/lang/String;

    .line 172
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateButton()V

    .line 173
    return-void
.end method

.method public setImageDrawable(Landroid/graphics/drawable/Drawable;)V
    .locals 0
    .param p1, "drawable"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 137
    iput-object p1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mDrawable:Landroid/graphics/drawable/Drawable;

    .line 138
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateImageDrawable()V

    .line 139
    return-void
.end method

.method public setMessage(Ljava/lang/CharSequence;)V
    .locals 0
    .param p1, "message"    # Ljava/lang/CharSequence;

    .prologue
    .line 154
    iput-object p1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mMessage:Ljava/lang/CharSequence;

    .line 155
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateMessage()V

    .line 156
    return-void
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 0
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 78
    iput-object p1, p0, Landroid/support/v17/leanback/app/ErrorSupportFragment;->mTitle:Ljava/lang/String;

    .line 79
    invoke-direct {p0}, Landroid/support/v17/leanback/app/ErrorSupportFragment;->updateTitle()V

    .line 80
    return-void
.end method
