.class final Landroid/support/v17/leanback/transition/TransitionHelperApi21;
.super Ljava/lang/Object;
.source "TransitionHelperApi21.java"


# direct methods
.method public static createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 64
    const v0, 0x10c000f

    invoke-static {p0, v0}, Landroid/view/animation/AnimationUtils;->loadInterpolator(Landroid/content/Context;I)Landroid/view/animation/Interpolator;

    move-result-object v0

    return-object v0
.end method

.method public static createScale()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 60
    new-instance v0, Landroid/transition/ChangeTransform;

    invoke-direct {v0}, Landroid/transition/ChangeTransform;-><init>()V

    return-object v0
.end method

.method public static getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
    .locals 1
    .param p0, "window"    # Landroid/view/Window;

    .prologue
    .line 28
    invoke-virtual {p0}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    return-object v0
.end method
