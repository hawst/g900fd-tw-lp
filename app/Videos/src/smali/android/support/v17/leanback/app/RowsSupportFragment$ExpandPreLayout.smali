.class Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;
.super Ljava/lang/Object;
.source "RowsSupportFragment.java"

# interfaces
.implements Landroid/view/ViewTreeObserver$OnPreDrawListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/app/RowsSupportFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = "ExpandPreLayout"
.end annotation


# instance fields
.field final mCallback:Ljava/lang/Runnable;

.field mState:I

.field final mVerticalView:Landroid/view/View;

.field final synthetic this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;


# direct methods
.method constructor <init>(Landroid/support/v17/leanback/app/RowsSupportFragment;Ljava/lang/Runnable;)V
    .locals 1
    .param p2, "callback"    # Ljava/lang/Runnable;

    .prologue
    .line 487
    iput-object p1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 488
    invoke-virtual {p1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->getVerticalGridView()Landroid/support/v17/leanback/widget/VerticalGridView;

    move-result-object v0

    iput-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    .line 489
    iput-object p2, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mCallback:Ljava/lang/Runnable;

    .line 490
    return-void
.end method


# virtual methods
.method execute()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 493
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->addOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 494
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setExpand(Z)V

    .line 495
    iput v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mState:I

    .line 496
    return-void
.end method

.method public onPreDraw()Z
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 500
    iget v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mState:I

    if-nez v0, :cond_1

    .line 501
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->this$0:Landroid/support/v17/leanback/app/RowsSupportFragment;

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/RowsSupportFragment;->setExpand(Z)V

    .line 502
    iput v1, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mState:I

    .line 508
    :cond_0
    :goto_0
    const/4 v0, 0x0

    return v0

    .line 503
    :cond_1
    iget v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mState:I

    if-ne v0, v1, :cond_0

    .line 504
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mCallback:Ljava/lang/Runnable;

    invoke-interface {v0}, Ljava/lang/Runnable;->run()V

    .line 505
    iget-object v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mVerticalView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getViewTreeObserver()Landroid/view/ViewTreeObserver;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/view/ViewTreeObserver;->removeOnPreDrawListener(Landroid/view/ViewTreeObserver$OnPreDrawListener;)V

    .line 506
    const/4 v0, 0x2

    iput v0, p0, Landroid/support/v17/leanback/app/RowsSupportFragment$ExpandPreLayout;->mState:I

    goto :goto_0
.end method
