.class public Landroid/support/v17/leanback/widget/Row;
.super Ljava/lang/Object;
.source "Row.java"


# instance fields
.field private mFlags:I

.field private mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

.field private mId:J


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 59
    return-void
.end method

.method public constructor <init>(Landroid/support/v17/leanback/widget/HeaderItem;)V
    .locals 2
    .param p1, "headerItem"    # Landroid/support/v17/leanback/widget/HeaderItem;

    .prologue
    .line 51
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    const/4 v0, 0x1

    iput v0, p0, Landroid/support/v17/leanback/widget/Row;->mFlags:I

    .line 31
    const-wide/16 v0, -0x1

    iput-wide v0, p0, Landroid/support/v17/leanback/widget/Row;->mId:J

    .line 52
    invoke-virtual {p0, p1}, Landroid/support/v17/leanback/widget/Row;->setHeaderItem(Landroid/support/v17/leanback/widget/HeaderItem;)V

    .line 53
    return-void
.end method


# virtual methods
.method public final getHeaderItem()Landroid/support/v17/leanback/widget/HeaderItem;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Landroid/support/v17/leanback/widget/Row;->mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

    return-object v0
.end method

.method public final setHeaderItem(Landroid/support/v17/leanback/widget/HeaderItem;)V
    .locals 0
    .param p1, "headerItem"    # Landroid/support/v17/leanback/widget/HeaderItem;

    .prologue
    .line 77
    iput-object p1, p0, Landroid/support/v17/leanback/widget/Row;->mHeaderItem:Landroid/support/v17/leanback/widget/HeaderItem;

    .line 78
    return-void
.end method
