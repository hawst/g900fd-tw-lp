.class Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;
.super Ljava/lang/Object;
.source "TransitionHelper.java"

# interfaces
.implements Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperVersionImpl;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Landroid/support/v17/leanback/transition/TransitionHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "TransitionHelperKitkatImpl"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 299
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Landroid/support/v17/leanback/transition/TransitionHelper$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/support/v17/leanback/transition/TransitionHelper$1;

    .prologue
    .line 299
    invoke-direct {p0}, Landroid/support/v17/leanback/transition/TransitionHelper$TransitionHelperKitkatImpl;-><init>()V

    return-void
.end method


# virtual methods
.method public addTarget(Ljava/lang/Object;Landroid/view/View;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 459
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->addTarget(Ljava/lang/Object;Landroid/view/View;)V

    .line 460
    return-void
.end method

.method public addTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "transitionSet"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 399
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->addTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 400
    return-void
.end method

.method public createChangeBounds(Z)Ljava/lang/Object;
    .locals 1
    .param p1, "reparent"    # Z

    .prologue
    .line 358
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createChangeBounds(Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createDefaultInterpolator(Landroid/content/Context;)Ljava/lang/Object;
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 464
    const/4 v0, 0x0

    return-object v0
.end method

.method public createFadeTransition(I)Ljava/lang/Object;
    .locals 1
    .param p1, "fadingMode"    # I

    .prologue
    .line 353
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createFadeTransition(I)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createScale()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 368
    invoke-static {}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createScale()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;
    .locals 1
    .param p1, "sceneRoot"    # Landroid/view/ViewGroup;
    .param p2, "r"    # Ljava/lang/Runnable;

    .prologue
    .line 343
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createScene(Landroid/view/ViewGroup;Ljava/lang/Runnable;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;
    .locals 1
    .param p1, "callback"    # Landroid/support/v17/leanback/transition/SlideCallback;

    .prologue
    .line 363
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createSlide(Landroid/support/v17/leanback/transition/SlideCallback;)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public createTransitionSet(Z)Ljava/lang/Object;
    .locals 1
    .param p1, "sequential"    # Z

    .prologue
    .line 394
    invoke-static {p1}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->createTransitionSet(Z)Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method public excludeChildren(Ljava/lang/Object;IZ)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "targetId"    # I
    .param p3, "exclude"    # Z

    .prologue
    .line 414
    invoke-static {p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->excludeChildren(Ljava/lang/Object;IZ)V

    .line 415
    return-void
.end method

.method public getSharedElementEnterTransition(Landroid/view/Window;)Ljava/lang/Object;
    .locals 1
    .param p1, "window"    # Landroid/view/Window;

    .prologue
    .line 303
    const/4 v0, 0x0

    return-object v0
.end method

.method public runTransition(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "scene"    # Ljava/lang/Object;
    .param p2, "transition"    # Ljava/lang/Object;

    .prologue
    .line 449
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->runTransition(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 450
    return-void
.end method

.method public setDuration(Ljava/lang/Object;J)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "duration"    # J

    .prologue
    .line 439
    invoke-static {p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->setDuration(Ljava/lang/Object;J)V

    .line 440
    return-void
.end method

.method public setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "timeInterpolator"    # Ljava/lang/Object;

    .prologue
    .line 454
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->setInterpolator(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 455
    return-void
.end method

.method public setStartDelay(Ljava/lang/Object;J)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "startDelay"    # J

    .prologue
    .line 434
    invoke-static {p1, p2, p3}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->setStartDelay(Ljava/lang/Object;J)V

    .line 435
    return-void
.end method

.method public setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V
    .locals 0
    .param p1, "transition"    # Ljava/lang/Object;
    .param p2, "listener"    # Landroid/support/v17/leanback/transition/TransitionListener;

    .prologue
    .line 444
    invoke-static {p1, p2}, Landroid/support/v17/leanback/transition/TransitionHelperKitkat;->setTransitionListener(Ljava/lang/Object;Landroid/support/v17/leanback/transition/TransitionListener;)V

    .line 445
    return-void
.end method
