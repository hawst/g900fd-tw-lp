.class Landroid/support/v17/leanback/widget/ShadowHelperApi21;
.super Ljava/lang/Object;
.source "ShadowHelperApi21.java"


# static fields
.field static sFocusedZ:I

.field static sNormalZ:I

.field static final sOutlineProvider:Landroid/view/ViewOutlineProvider;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 28
    const/high16 v0, -0x80000000

    sput v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    .line 30
    new-instance v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21$1;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ShadowHelperApi21$1;-><init>()V

    sput-object v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sOutlineProvider:Landroid/view/ViewOutlineProvider;

    return-void
.end method

.method public static addShadow(Landroid/view/ViewGroup;Z)Ljava/lang/Object;
    .locals 1
    .param p0, "shadowContainer"    # Landroid/view/ViewGroup;
    .param p1, "roundedCorners"    # Z

    .prologue
    .line 47
    invoke-virtual {p0}, Landroid/view/ViewGroup;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-static {v0}, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->initializeResources(Landroid/content/res/Resources;)V

    .line 48
    if-eqz p1, :cond_0

    .line 49
    const/4 v0, 0x0

    invoke-static {p0, v0}, Landroid/support/v17/leanback/widget/RoundedRectHelperApi21;->setRoundedRectBackground(Landroid/view/View;I)V

    .line 54
    :goto_0
    sget v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    int-to-float v0, v0

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setZ(F)V

    .line 55
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setTransitionGroup(Z)V

    .line 56
    return-object p0

    .line 52
    :cond_0
    sget-object v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sOutlineProvider:Landroid/view/ViewOutlineProvider;

    invoke-virtual {p0, v0}, Landroid/view/ViewGroup;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    goto :goto_0
.end method

.method private static initializeResources(Landroid/content/res/Resources;)V
    .locals 2
    .param p0, "res"    # Landroid/content/res/Resources;

    .prologue
    .line 39
    sget v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    const/high16 v1, -0x80000000

    if-ne v0, v1, :cond_0

    .line 40
    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_material_shadow_normal_z:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    .line 41
    sget v0, Landroid/support/v17/leanback/R$dimen;->lb_material_shadow_focused_z:I

    invoke-virtual {p0, v0}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    float-to-int v0, v0

    sput v0, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sFocusedZ:I

    .line 43
    :cond_0
    return-void
.end method

.method public static setShadowFocusLevel(Ljava/lang/Object;F)V
    .locals 4
    .param p0, "impl"    # Ljava/lang/Object;
    .param p1, "level"    # F

    .prologue
    .line 61
    move-object v0, p0

    check-cast v0, Landroid/view/ViewGroup;

    .line 62
    .local v0, "shadowContainer":Landroid/view/ViewGroup;
    sget v1, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    int-to-float v1, v1

    sget v2, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sFocusedZ:I

    sget v3, Landroid/support/v17/leanback/widget/ShadowHelperApi21;->sNormalZ:I

    sub-int/2addr v2, v3

    int-to-float v2, v2

    mul-float/2addr v2, p1

    add-float/2addr v1, v2

    invoke-virtual {v0, v1}, Landroid/view/ViewGroup;->setZ(F)V

    .line 63
    return-void
.end method

.method public static setZ(Landroid/view/View;F)V
    .locals 0
    .param p0, "view"    # Landroid/view/View;
    .param p1, "z"    # F

    .prologue
    .line 66
    invoke-virtual {p0, p1}, Landroid/view/View;->setZ(F)V

    .line 67
    return-void
.end method
