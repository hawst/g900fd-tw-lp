.class final Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;
.super Ljava/lang/Object;
.source "AudioTrack.java"

# interfaces
.implements Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/audio/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AudioTimestampCompatV19"
.end annotation


# instance fields
.field private final audioTimestamp:Landroid/media/AudioTimestamp;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 693
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 694
    new-instance v0, Landroid/media/AudioTimestamp;

    invoke-direct {v0}, Landroid/media/AudioTimestamp;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;->audioTimestamp:Landroid/media/AudioTimestamp;

    .line 695
    return-void
.end method


# virtual methods
.method public getFramePosition()J
    .locals 2

    .prologue
    .line 709
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;->audioTimestamp:Landroid/media/AudioTimestamp;

    iget-wide v0, v0, Landroid/media/AudioTimestamp;->framePosition:J

    return-wide v0
.end method

.method public getNanoTime()J
    .locals 2

    .prologue
    .line 704
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;->audioTimestamp:Landroid/media/AudioTimestamp;

    iget-wide v0, v0, Landroid/media/AudioTimestamp;->nanoTime:J

    return-wide v0
.end method

.method public update(Landroid/media/AudioTrack;)Z
    .locals 1
    .param p1, "audioTrack"    # Landroid/media/AudioTrack;

    .prologue
    .line 699
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;->audioTimestamp:Landroid/media/AudioTimestamp;

    invoke-virtual {p1, v0}, Landroid/media/AudioTrack;->getTimestamp(Landroid/media/AudioTimestamp;)Z

    move-result v0

    return v0
.end method
