.class public final Lcom/google/android/exoplayer/parser/mp4/Track;
.super Ljava/lang/Object;
.source "Track.java"


# instance fields
.field public final id:I

.field public final mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

.field public final sampleDescriptionEncryptionBoxes:[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

.field public final timescale:J

.field public final type:I


# direct methods
.method public constructor <init>(IIJLcom/google/android/exoplayer/MediaFormat;[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "type"    # I
    .param p3, "timescale"    # J
    .param p5, "mediaFormat"    # Lcom/google/android/exoplayer/MediaFormat;
    .param p6, "sampleDescriptionEncryptionBoxes"    # [Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    .prologue
    .line 68
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 69
    iput p1, p0, Lcom/google/android/exoplayer/parser/mp4/Track;->id:I

    .line 70
    iput p2, p0, Lcom/google/android/exoplayer/parser/mp4/Track;->type:I

    .line 71
    iput-wide p3, p0, Lcom/google/android/exoplayer/parser/mp4/Track;->timescale:J

    .line 72
    iput-object p5, p0, Lcom/google/android/exoplayer/parser/mp4/Track;->mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

    .line 73
    iput-object p6, p0, Lcom/google/android/exoplayer/parser/mp4/Track;->sampleDescriptionEncryptionBoxes:[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    .line 74
    return-void
.end method
