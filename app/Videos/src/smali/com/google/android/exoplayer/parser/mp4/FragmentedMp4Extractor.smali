.class public final Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
.super Ljava/lang/Object;
.source "FragmentedMp4Extractor.java"

# interfaces
.implements Lcom/google/android/exoplayer/parser/Extractor;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;
    }
.end annotation


# static fields
.field private static final AC3_BIT_RATES:[I

.field private static final AC3_CHANNEL_COUNTS:[I

.field private static final CONTAINER_TYPES:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final NAL_START_CODE:[B

.field private static final PARSED_ATOMS:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private static final PIFF_SAMPLE_ENCRYPTION_BOX_EXTENDED_TYPE:[B


# instance fields
.field private atomBytesRead:I

.field private atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

.field private final atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

.field private atomSize:I

.field private atomType:I

.field private final containerAtomEndPoints:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final containerAtoms:Ljava/util/Stack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Stack",
            "<",
            "Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;",
            ">;"
        }
    .end annotation
.end field

.field private final extendedTypeScratch:[B

.field private extendsDefaults:Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

.field private final fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

.field private lastSyncSampleIndex:I

.field private parserState:I

.field private pendingSeekSyncSampleIndex:I

.field private pendingSeekTimeMs:I

.field private final psshData:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/util/UUID;",
            "[B>;"
        }
    .end annotation
.end field

.field private rootAtomBytesRead:I

.field private sampleIndex:I

.field private segmentIndex:Lcom/google/android/exoplayer/parser/SegmentIndex;

.field private track:Lcom/google/android/exoplayer/parser/mp4/Track;

.field private final workaroundFlags:I


# direct methods
.method static constructor <clinit>()V
    .locals 8

    .prologue
    const v7, 0x6d766578

    const v6, 0x6d6f6f76

    const v5, 0x6d6f6f66

    const v4, 0x6d696e66

    const v3, 0x6d646961

    .line 65
    const/4 v2, 0x4

    new-array v2, v2, [B

    fill-array-data v2, :array_0

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->NAL_START_CODE:[B

    .line 66
    const/16 v2, 0x10

    new-array v2, v2, [B

    fill-array-data v2, :array_1

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->PIFF_SAMPLE_ENCRYPTION_BOX_EXTENDED_TYPE:[B

    .line 69
    const/16 v2, 0x8

    new-array v2, v2, [I

    fill-array-data v2, :array_2

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->AC3_CHANNEL_COUNTS:[I

    .line 71
    const/16 v2, 0x13

    new-array v2, v2, [I

    fill-array-data v2, :array_3

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->AC3_BIT_RATES:[I

    .line 87
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 88
    .local v1, "parsedAtoms":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    const v2, 0x61766331

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 89
    const v2, 0x61766333

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 90
    const v2, 0x65736473

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 91
    const v2, 0x68646c72    # 4.3148E24f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 92
    const v2, 0x6d646174

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 93
    const v2, 0x6d646864

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 94
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 95
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 96
    const v2, 0x6d703461

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 97
    const v2, 0x73696478

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 98
    const v2, 0x73747364

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 99
    const v2, 0x74666474

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 100
    const v2, 0x74666864

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 101
    const v2, 0x746b6864

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 102
    const v2, 0x74726166

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 103
    const v2, 0x7472616b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 104
    const v2, 0x74726578

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 105
    const v2, 0x7472756e

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 106
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 108
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 109
    const v2, 0x7374626c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 110
    const v2, 0x70737368    # 3.013775E29f

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 111
    const v2, 0x7361697a

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 112
    const v2, 0x75756964

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 113
    const v2, 0x73656e63

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 114
    const v2, 0x70617370

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 115
    invoke-static {v1}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->PARSED_ATOMS:Ljava/util/Set;

    .line 121
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    .line 122
    .local v0, "atomContainerTypes":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/Integer;>;"
    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 123
    const v2, 0x7472616b

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 124
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 125
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 126
    const v2, 0x7374626c

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 127
    const v2, 0x61766343

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 128
    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 129
    const v2, 0x74726166

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 130
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    .line 131
    invoke-static {v0}, Ljava/util/Collections;->unmodifiableSet(Ljava/util/Set;)Ljava/util/Set;

    move-result-object v2

    sput-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->CONTAINER_TYPES:Ljava/util/Set;

    .line 132
    return-void

    .line 65
    nop

    :array_0
    .array-data 1
        0x0t
        0x0t
        0x0t
        0x1t
    .end array-data

    .line 66
    :array_1
    .array-data 1
        -0x5et
        0x39t
        0x4ft
        0x52t
        0x5at
        -0x65t
        0x4ft
        0x14t
        -0x5et
        0x44t
        0x6ct
        0x42t
        0x7ct
        0x64t
        -0x73t
        -0xct
    .end array-data

    .line 69
    :array_2
    .array-data 4
        0x2
        0x1
        0x2
        0x3
        0x3
        0x4
        0x4
        0x5
    .end array-data

    .line 71
    :array_3
    .array-data 4
        0x20
        0x28
        0x30
        0x38
        0x40
        0x50
        0x60
        0x70
        0x80
        0xa0
        0xc0
        0xe0
        0x100
        0x140
        0x180
        0x1c0
        0x200
        0x240
        0x280
    .end array-data
.end method

.method public constructor <init>()V
    .locals 1

    .prologue
    .line 162
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;-><init>(I)V

    .line 163
    return-void
.end method

.method public constructor <init>(I)V
    .locals 2
    .param p1, "workaroundFlags"    # I

    .prologue
    .line 169
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 170
    iput p1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->workaroundFlags:I

    .line 171
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parserState:I

    .line 172
    new-instance v0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    const/16 v1, 0x8

    invoke-direct {v0, v1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .line 173
    const/16 v0, 0x10

    new-array v0, v0, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->extendedTypeScratch:[B

    .line 174
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    .line 175
    new-instance v0, Ljava/util/Stack;

    invoke-direct {v0}, Ljava/util/Stack;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    .line 176
    new-instance v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-direct {v0}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    .line 177
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->psshData:Ljava/util/HashMap;

    .line 178
    return-void
.end method

.method private enterState(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    const/4 v1, 0x0

    .line 279
    packed-switch p1, :pswitch_data_0

    .line 287
    :cond_0
    :goto_0
    iput p1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parserState:I

    .line 288
    return-void

    .line 281
    :pswitch_0
    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    .line 282
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 283
    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    goto :goto_0

    .line 279
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method private onContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)I
    .locals 2
    .param p1, "container"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    .prologue
    .line 375
    iget v0, p1, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->type:I

    const v1, 0x6d6f6f76

    if-ne v0, v1, :cond_0

    .line 376
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->onMoovContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)V

    .line 377
    const/16 v0, 0x8

    .line 383
    :goto_0
    return v0

    .line 378
    :cond_0
    iget v0, p1, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->type:I

    const v1, 0x6d6f6f66

    if-ne v0, v1, :cond_2

    .line 379
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->onMoofContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)V

    .line 383
    :cond_1
    :goto_1
    const/4 v0, 0x0

    goto :goto_0

    .line 380
    :cond_2
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 381
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->add(Lcom/google/android/exoplayer/parser/mp4/Atom;)V

    goto :goto_1
.end method

.method private onLeafAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;)I
    .locals 2
    .param p1, "leaf"    # Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    .prologue
    .line 365
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_1

    .line 366
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v0}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->add(Lcom/google/android/exoplayer/parser/mp4/Atom;)V

    .line 371
    :cond_0
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 367
    :cond_1
    iget v0, p1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->type:I

    const v1, 0x73696478

    if-ne v0, v1, :cond_0

    .line 368
    iget-object v0, p1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSidx(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->segmentIndex:Lcom/google/android/exoplayer/parser/SegmentIndex;

    .line 369
    const/16 v0, 0x10

    goto :goto_0
.end method

.method private onMoofContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)V
    .locals 8
    .param p1, "moof"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    .prologue
    const/4 v7, 0x0

    .line 407
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-virtual {v0}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->reset()V

    .line 408
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->extendsDefaults:Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->workaroundFlags:I

    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->extendedTypeScratch:[B

    move-object v2, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseMoof(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;I[B)V

    .line 409
    iput v7, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    .line 410
    iput v7, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->lastSyncSampleIndex:I

    .line 411
    iput v7, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekSyncSampleIndex:I

    .line 412
    iget v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    if-eqz v0, :cond_2

    .line 413
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v0, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-ge v6, v0, :cond_1

    .line 414
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleIsSyncFrameTable:[Z

    aget-boolean v0, v0, v6

    if-eqz v0, :cond_0

    .line 415
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->getSamplePresentationTime(I)J

    move-result-wide v0

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-gtz v0, :cond_0

    .line 416
    iput v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekSyncSampleIndex:I

    .line 413
    :cond_0
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 420
    :cond_1
    iput v7, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    .line 422
    .end local v6    # "i":I
    :cond_2
    return-void
.end method

.method private onMoovContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)V
    .locals 14
    .param p1, "moov"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    .prologue
    .line 387
    iget-object v4, p1, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    .line 388
    .local v4, "moovChildren":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/exoplayer/parser/mp4/Atom;>;"
    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v5

    .line 389
    .local v5, "moovChildrenSize":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v5, :cond_1

    .line 390
    invoke-interface {v4, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom;

    .line 391
    .local v0, "child":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget v9, v0, Lcom/google/android/exoplayer/parser/mp4/Atom;->type:I

    const v10, 0x70737368    # 3.013775E29f

    if-ne v9, v10, :cond_0

    .line 392
    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    .end local v0    # "child":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget-object v7, v0, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .line 393
    .local v7, "psshAtom":Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    const/16 v9, 0xc

    invoke-virtual {v7, v9}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 394
    new-instance v8, Ljava/util/UUID;

    invoke-virtual {v7}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readLong()J

    move-result-wide v10

    invoke-virtual {v7}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readLong()J

    move-result-wide v12

    invoke-direct {v8, v10, v11, v12, v13}, Ljava/util/UUID;-><init>(JJ)V

    .line 395
    .local v8, "uuid":Ljava/util/UUID;
    invoke-virtual {v7}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v2

    .line 396
    .local v2, "dataSize":I
    new-array v1, v2, [B

    .line 397
    .local v1, "data":[B
    const/4 v9, 0x0

    invoke-virtual {v7, v1, v9, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readBytes([BII)V

    .line 398
    iget-object v9, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->psshData:Ljava/util/HashMap;

    invoke-virtual {v9, v8, v1}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 389
    .end local v1    # "data":[B
    .end local v2    # "dataSize":I
    .end local v7    # "psshAtom":Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .end local v8    # "uuid":Ljava/util/UUID;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 401
    :cond_1
    const v9, 0x6d766578

    invoke-virtual {p1, v9}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v6

    .line 402
    .local v6, "mvex":Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    const v9, 0x74726578

    invoke-virtual {v6, v9}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v9

    iget-object v9, v9, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTrex(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->extendsDefaults:Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    .line 403
    const v9, 0x7472616b

    invoke-virtual {p1, v9}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v9

    invoke-static {v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTrak(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)Lcom/google/android/exoplayer/parser/mp4/Track;

    move-result-object v9

    iput-object v9, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    .line 404
    return-void
.end method

.method private static parseAc3SpecificBoxFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;
    .locals 8
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I

    .prologue
    .line 631
    add-int/lit8 v5, p1, 0x8

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 634
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v5

    and-int/lit16 v5, v5, 0xc0

    shr-int/lit8 v2, v5, 0x6

    .line 636
    .local v2, "fscod":I
    packed-switch v2, :pswitch_data_0

    .line 648
    const/4 v5, 0x0

    .line 664
    :goto_0
    return-object v5

    .line 638
    :pswitch_0
    const v4, 0xbb80

    .line 651
    .local v4, "sampleRate":I
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 654
    .local v3, "nextByte":I
    sget-object v5, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->AC3_CHANNEL_COUNTS:[I

    and-int/lit8 v6, v3, 0x38

    shr-int/lit8 v6, v6, 0x3

    aget v1, v5, v6

    .line 657
    .local v1, "channelCount":I
    and-int/lit8 v5, v3, 0x4

    if-eqz v5, :cond_0

    .line 658
    add-int/lit8 v1, v1, 0x1

    .line 662
    :cond_0
    sget-object v5, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->AC3_BIT_RATES:[I

    and-int/lit8 v6, v3, 0x3

    shl-int/lit8 v6, v6, 0x3

    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v7

    shr-int/lit8 v7, v7, 0x5

    add-int/2addr v6, v7

    aget v0, v5, v6

    .line 664
    .local v0, "bitrate":I
    new-instance v5, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;

    invoke-direct {v5, v1, v4, v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;-><init>(III)V

    goto :goto_0

    .line 641
    .end local v0    # "bitrate":I
    .end local v1    # "channelCount":I
    .end local v3    # "nextByte":I
    .end local v4    # "sampleRate":I
    :pswitch_1
    const v4, 0xac44

    .line 642
    .restart local v4    # "sampleRate":I
    goto :goto_1

    .line 644
    .end local v4    # "sampleRate":I
    :pswitch_2
    const/16 v4, 0x7d00

    .line 645
    .restart local v4    # "sampleRate":I
    goto :goto_1

    .line 636
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private static parseAudioSampleEntry(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;III)Landroid/util/Pair;
    .locals 16
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "atomType"    # I
    .param p2, "position"    # I
    .param p3, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;",
            "III)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer/MediaFormat;",
            "Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 567
    add-int/lit8 v6, p2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 568
    const/16 v6, 0x10

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 569
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v3

    .line 570
    .local v3, "channelCount":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v2

    .line 571
    .local v2, "sampleSize":I
    const/4 v6, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 572
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedFixedPoint1616()I

    move-result v4

    .line 573
    .local v4, "sampleRate":I
    const/4 v5, -0x1

    .line 575
    .local v5, "bitrate":I
    const/4 v14, 0x0

    .line 576
    .local v14, "initializationData":[B
    const/4 v15, 0x0

    .line 577
    .local v15, "trackEncryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v11

    .line 578
    .local v11, "childPosition":I
    :goto_0
    sub-int v6, v11, p2

    move/from16 v0, p3

    if-ge v6, v0, :cond_6

    .line 579
    move-object/from16 v0, p0

    invoke-virtual {v0, v11}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 580
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v12

    .line 581
    .local v12, "childStartPosition":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v9

    .line 582
    .local v9, "childAtomSize":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v10

    .line 583
    .local v10, "childAtomType":I
    const v6, 0x6d703461

    move/from16 v0, p1

    if-eq v0, v6, :cond_0

    const v6, 0x656e6361

    move/from16 v0, p1

    if-ne v0, v6, :cond_3

    .line 584
    :cond_0
    const v6, 0x65736473

    if-ne v10, v6, :cond_2

    .line 585
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseEsdsFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)[B

    move-result-object v14

    .line 588
    invoke-static {v14}, Lcom/google/android/exoplayer/util/CodecSpecificDataUtil;->parseAudioSpecificConfig([B)Landroid/util/Pair;

    move-result-object v8

    .line 590
    .local v8, "audioSpecificConfig":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    iget-object v6, v8, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v4

    .line 591
    iget-object v6, v8, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v6, Ljava/lang/Integer;

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v3

    .line 611
    .end local v8    # "audioSpecificConfig":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Integer;>;"
    :cond_1
    :goto_1
    add-int/2addr v11, v9

    .line 612
    goto :goto_0

    .line 592
    :cond_2
    const v6, 0x73696e66

    if-ne v10, v6, :cond_1

    .line 593
    move-object/from16 v0, p0

    invoke-static {v0, v12, v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSinfFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    move-result-object v15

    goto :goto_1

    .line 595
    :cond_3
    const v6, 0x61632d33

    move/from16 v0, p1

    if-ne v0, v6, :cond_5

    const v6, 0x64616333

    if-ne v10, v6, :cond_5

    .line 597
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseAc3SpecificBoxFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;

    move-result-object v7

    .line 599
    .local v7, "ac3Format":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;
    if-eqz v7, :cond_4

    .line 600
    iget v4, v7, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;->sampleRate:I

    .line 601
    iget v3, v7, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;->channelCount:I

    .line 602
    iget v5, v7, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;->bitrate:I

    .line 606
    :cond_4
    const/4 v15, 0x0

    .line 607
    goto :goto_1

    .end local v7    # "ac3Format":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor$Ac3Format;
    :cond_5
    const v6, 0x65632d33

    move/from16 v0, p1

    if-ne v0, v6, :cond_1

    const v6, 0x64656333

    if-ne v10, v6, :cond_1

    .line 608
    move-object/from16 v0, p0

    invoke-static {v0, v12}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseEc3SpecificBoxFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)I

    move-result v4

    .line 609
    const/4 v15, 0x0

    goto :goto_1

    .line 615
    .end local v9    # "childAtomSize":I
    .end local v10    # "childAtomType":I
    .end local v12    # "childStartPosition":I
    :cond_6
    const v6, 0x61632d33

    move/from16 v0, p1

    if-ne v0, v6, :cond_7

    .line 616
    const-string v1, "audio/ac3"

    .line 623
    .local v1, "mimeType":Ljava/lang/String;
    :goto_2
    if-nez v14, :cond_9

    const/4 v6, 0x0

    :goto_3
    invoke-static/range {v1 .. v6}, Lcom/google/android/exoplayer/MediaFormat;->createAudioFormat(Ljava/lang/String;IIIILjava/util/List;)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v13

    .line 626
    .local v13, "format":Lcom/google/android/exoplayer/MediaFormat;
    invoke-static {v13, v15}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    return-object v6

    .line 617
    .end local v1    # "mimeType":Ljava/lang/String;
    .end local v13    # "format":Lcom/google/android/exoplayer/MediaFormat;
    :cond_7
    const v6, 0x65632d33

    move/from16 v0, p1

    if-ne v0, v6, :cond_8

    .line 618
    const-string v1, "audio/eac3"

    .restart local v1    # "mimeType":Ljava/lang/String;
    goto :goto_2

    .line 620
    .end local v1    # "mimeType":Ljava/lang/String;
    :cond_8
    const-string v1, "audio/mp4a-latm"

    .restart local v1    # "mimeType":Ljava/lang/String;
    goto :goto_2

    .line 623
    :cond_9
    invoke-static {v14}, Ljava/util/Collections;->singletonList(Ljava/lang/Object;)Ljava/util/List;

    move-result-object v6

    goto :goto_3
.end method

.method private static parseAvcCFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)Ljava/util/List;
    .locals 6
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;",
            "I)",
            "Ljava/util/List",
            "<[B>;"
        }
    .end annotation

    .prologue
    .line 675
    add-int/lit8 v5, p1, 0x8

    add-int/lit8 v5, v5, 0x4

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 677
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v5

    and-int/lit8 v5, v5, 0x3

    add-int/lit8 v2, v5, 0x1

    .line 678
    .local v2, "nalUnitLength":I
    const/4 v5, 0x4

    if-eq v2, v5, :cond_0

    .line 681
    new-instance v5, Ljava/lang/IllegalStateException;

    invoke-direct {v5}, Ljava/lang/IllegalStateException;-><init>()V

    throw v5

    .line 683
    :cond_0
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 686
    .local v0, "initializationData":Ljava/util/List;, "Ljava/util/List<[B>;"
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v5

    and-int/lit8 v4, v5, 0x1f

    .line 687
    .local v4, "numSequenceParameterSets":I
    const/4 v1, 0x0

    .local v1, "j":I
    :goto_0
    if-ge v1, v4, :cond_1

    .line 688
    invoke-static {p0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseChildNalUnit(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)[B

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 687
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 690
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 691
    .local v3, "numPictureParamterSets":I
    const/4 v1, 0x0

    :goto_1
    if-ge v1, v3, :cond_2

    .line 692
    invoke-static {p0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseChildNalUnit(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)[B

    move-result-object v5

    invoke-interface {v0, v5}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 691
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 694
    :cond_2
    return-object v0
.end method

.method private static parseAvcFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Landroid/util/Pair;
    .locals 12
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I
    .param p2, "size"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;",
            "II)",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer/MediaFormat;",
            "Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 534
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 536
    const/16 v0, 0x18

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 537
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v2

    .line 538
    .local v2, "width":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v3

    .line 539
    .local v3, "height":I
    const/high16 v4, 0x3f800000    # 1.0f

    .line 540
    .local v4, "pixelWidthHeightRatio":F
    const/16 v0, 0x32

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 542
    const/4 v5, 0x0

    .line 543
    .local v5, "initializationData":Ljava/util/List;, "Ljava/util/List<[B>;"
    const/4 v11, 0x0

    .line 544
    .local v11, "trackEncryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v8

    .line 545
    .local v8, "childPosition":I
    :goto_0
    sub-int v0, v8, p1

    if-ge v0, p2, :cond_3

    .line 546
    invoke-virtual {p0, v8}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 547
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v9

    .line 548
    .local v9, "childStartPosition":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v6

    .line 549
    .local v6, "childAtomSize":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v7

    .line 550
    .local v7, "childAtomType":I
    const v0, 0x61766343

    if-ne v7, v0, :cond_1

    .line 551
    invoke-static {p0, v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseAvcCFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)Ljava/util/List;

    move-result-object v5

    .line 557
    :cond_0
    :goto_1
    add-int/2addr v8, v6

    .line 558
    goto :goto_0

    .line 552
    :cond_1
    const v0, 0x73696e66

    if-ne v7, v0, :cond_2

    .line 553
    invoke-static {p0, v9, v6}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSinfFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    move-result-object v11

    goto :goto_1

    .line 554
    :cond_2
    const v0, 0x70617370

    if-ne v7, v0, :cond_0

    .line 555
    invoke-static {p0, v9}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parsePaspFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)F

    move-result v4

    goto :goto_1

    .line 560
    .end local v6    # "childAtomSize":I
    .end local v7    # "childAtomType":I
    .end local v9    # "childStartPosition":I
    :cond_3
    const-string v0, "video/avc"

    const/4 v1, -0x1

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer/MediaFormat;->createVideoFormat(Ljava/lang/String;IIIFLjava/util/List;)Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v10

    .line 562
    .local v10, "format":Lcom/google/android/exoplayer/MediaFormat;
    invoke-static {v10, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method private static parseChildNalUnit(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)[B
    .locals 3
    .param p0, "atom"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    .line 698
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v0

    .line 699
    .local v0, "length":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v1

    .line 700
    .local v1, "offset":I
    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 701
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    invoke-static {v2, v1, v0}, Lcom/google/android/exoplayer/util/CodecSpecificDataUtil;->buildNalUnit([BII)[B

    move-result-object v2

    return-object v2
.end method

.method private static parseEc3SpecificBoxFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)I
    .locals 1
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I

    .prologue
    .line 669
    add-int/lit8 v0, p1, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 671
    const/4 v0, 0x0

    return v0
.end method

.method private static parseEsdsFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)[B
    .locals 8
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I

    .prologue
    const/16 v7, 0x7f

    const/4 v6, 0x2

    const/4 v5, 0x1

    .line 757
    add-int/lit8 v4, p1, 0x8

    add-int/lit8 v4, v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 759
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 760
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 761
    .local v3, "varIntByte":I
    :goto_0
    if-le v3, v7, :cond_0

    .line 762
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    goto :goto_0

    .line 764
    :cond_0
    invoke-virtual {p0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 766
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v0

    .line 767
    .local v0, "flags":I
    and-int/lit16 v4, v0, 0x80

    if-eqz v4, :cond_1

    .line 768
    invoke-virtual {p0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 770
    :cond_1
    and-int/lit8 v4, v0, 0x40

    if-eqz v4, :cond_2

    .line 771
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v4

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 773
    :cond_2
    and-int/lit8 v4, v0, 0x20

    if-eqz v4, :cond_3

    .line 774
    invoke-virtual {p0, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 778
    :cond_3
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 779
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 780
    :goto_1
    if-le v3, v7, :cond_4

    .line 781
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    goto :goto_1

    .line 783
    :cond_4
    const/16 v4, 0xd

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 786
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 787
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 788
    and-int/lit8 v2, v3, 0x7f

    .line 789
    .local v2, "varInt":I
    :goto_2
    if-le v3, v7, :cond_5

    .line 790
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v3

    .line 791
    shl-int/lit8 v2, v2, 0x8

    .line 792
    and-int/lit8 v4, v3, 0x7f

    or-int/2addr v2, v4

    goto :goto_2

    .line 794
    :cond_5
    new-array v1, v2, [B

    .line 795
    .local v1, "initializationData":[B
    const/4 v4, 0x0

    invoke-virtual {p0, v1, v4, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readBytes([BII)V

    .line 796
    return-object v1
.end method

.method private static parseFullAtomFlags(I)I
    .locals 1
    .param p0, "fullAtomInt"    # I

    .prologue
    .line 1242
    const v0, 0xffffff

    and-int/2addr v0, p0

    return v0
.end method

.method private static parseFullAtomVersion(I)I
    .locals 1
    .param p0, "fullAtomInt"    # I

    .prologue
    .line 1235
    shr-int/lit8 v0, p0, 0x18

    and-int/lit16 v0, v0, 0xff

    return v0
.end method

.method private static parseHdlr(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)I
    .locals 1
    .param p0, "hdlr"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    .line 486
    const/16 v0, 0x10

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 487
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v0

    return v0
.end method

.method private static parseMdhd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)J
    .locals 4
    .param p0, "mdhd"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    const/16 v2, 0x8

    .line 497
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 498
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v0

    .line 499
    .local v0, "fullAtom":I
    invoke-static {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomVersion(I)I

    move-result v1

    .line 501
    .local v1, "version":I
    if-nez v1, :cond_0

    :goto_0
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 502
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v2

    return-wide v2

    .line 501
    :cond_0
    const/16 v2, 0x10

    goto :goto_0
.end method

.method private static parseMoof(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;I[B)V
    .locals 6
    .param p0, "track"    # Lcom/google/android/exoplayer/parser/mp4/Track;
    .param p1, "extendsDefaults"    # Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .param p2, "moof"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    .param p3, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;
    .param p4, "workaroundFlags"    # I
    .param p5, "extendedTypeScratch"    # [B

    .prologue
    .line 801
    const v0, 0x74726166

    invoke-virtual {p2, v0}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v2

    move-object v0, p0

    move-object v1, p1

    move-object v3, p3

    move v4, p4

    move-object v5, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTraf(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;I[B)V

    .line 803
    return-void
.end method

.method private static parsePaspFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;I)F
    .locals 4
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I

    .prologue
    .line 729
    add-int/lit8 v2, p1, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 730
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v0

    .line 731
    .local v0, "hSpacing":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v1

    .line 732
    .local v1, "vSpacing":I
    int-to-float v2, v0

    int-to-float v3, v1

    div-float/2addr v2, v3

    return v2
.end method

.method private static parseSaiz(Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V
    .locals 13
    .param p0, "encryptionBox"    # Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    .param p1, "saiz"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p2, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    .prologue
    .line 843
    iget v9, p0, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;->initializationVectorSize:I

    .line 844
    .local v9, "vectorSize":I
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 845
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v2

    .line 846
    .local v2, "fullAtom":I
    invoke-static {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomFlags(I)I

    move-result v1

    .line 847
    .local v1, "flags":I
    and-int/lit8 v10, v1, 0x1

    const/4 v11, 0x1

    if-ne v10, v11, :cond_0

    .line 848
    const/16 v10, 0x8

    invoke-virtual {p1, v10}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 850
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v0

    .line 852
    .local v0, "defaultSampleInfoSize":I
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v4

    .line 853
    .local v4, "sampleCount":I
    iget v10, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-eq v4, v10, :cond_1

    .line 854
    new-instance v10, Ljava/lang/IllegalStateException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Length mismatch: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    const-string v12, ", "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    iget v12, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 857
    :cond_1
    const/4 v8, 0x0

    .line 858
    .local v8, "totalSize":I
    if-nez v0, :cond_3

    .line 859
    iget-object v5, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleHasSubsampleEncryptionTable:[Z

    .line 860
    .local v5, "sampleHasSubsampleEncryptionTable":[Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    if-ge v3, v4, :cond_4

    .line 861
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedByte()I

    move-result v6

    .line 862
    .local v6, "sampleInfoSize":I
    add-int/2addr v8, v6

    .line 863
    if-le v6, v9, :cond_2

    const/4 v10, 0x1

    :goto_1
    aput-boolean v10, v5, v3

    .line 860
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 863
    :cond_2
    const/4 v10, 0x0

    goto :goto_1

    .line 866
    .end local v3    # "i":I
    .end local v5    # "sampleHasSubsampleEncryptionTable":[Z
    .end local v6    # "sampleInfoSize":I
    :cond_3
    if-le v0, v9, :cond_5

    const/4 v7, 0x1

    .line 867
    .local v7, "subsampleEncryption":Z
    :goto_2
    mul-int v10, v0, v4

    add-int/2addr v8, v10

    .line 868
    iget-object v10, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleHasSubsampleEncryptionTable:[Z

    const/4 v11, 0x0

    invoke-static {v10, v11, v4, v7}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 870
    .end local v7    # "subsampleEncryption":Z
    :cond_4
    invoke-virtual {p2, v8}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->initEncryptionData(I)V

    .line 871
    return-void

    .line 866
    :cond_5
    const/4 v7, 0x0

    goto :goto_2
.end method

.method private static parseSchiFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    .locals 9
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I
    .param p2, "size"    # I

    .prologue
    const/4 v4, 0x1

    const/4 v7, 0x0

    .line 737
    add-int/lit8 v2, p1, 0x8

    .line 738
    .local v2, "childPosition":I
    :goto_0
    sub-int v8, v2, p1

    if-ge v8, p2, :cond_2

    .line 739
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 740
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v0

    .line 741
    .local v0, "childAtomSize":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v1

    .line 742
    .local v1, "childAtomType":I
    const v8, 0x74656e63

    if-ne v1, v8, :cond_1

    .line 743
    const/4 v8, 0x4

    invoke-virtual {p0, v8}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 744
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v6

    .line 745
    .local v6, "firstInt":I
    shr-int/lit8 v8, v6, 0x8

    if-ne v8, v4, :cond_0

    .line 746
    .local v4, "defaultIsEncrypted":Z
    :goto_1
    and-int/lit16 v3, v6, 0xff

    .line 747
    .local v3, "defaultInitVectorSize":I
    const/16 v8, 0x10

    new-array v5, v8, [B

    .line 748
    .local v5, "defaultKeyId":[B
    array-length v8, v5

    invoke-virtual {p0, v5, v7, v8}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readBytes([BII)V

    .line 749
    new-instance v7, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    invoke-direct {v7, v4, v3, v5}, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;-><init>(ZI[B)V

    .line 753
    .end local v0    # "childAtomSize":I
    .end local v1    # "childAtomType":I
    .end local v3    # "defaultInitVectorSize":I
    .end local v4    # "defaultIsEncrypted":Z
    .end local v5    # "defaultKeyId":[B
    .end local v6    # "firstInt":I
    :goto_2
    return-object v7

    .restart local v0    # "childAtomSize":I
    .restart local v1    # "childAtomType":I
    .restart local v6    # "firstInt":I
    :cond_0
    move v4, v7

    .line 745
    goto :goto_1

    .line 751
    .end local v6    # "firstInt":I
    :cond_1
    add-int/2addr v2, v0

    .line 752
    goto :goto_0

    .line 753
    .end local v0    # "childAtomSize":I
    .end local v1    # "childAtomType":I
    :cond_2
    const/4 v7, 0x0

    goto :goto_2
.end method

.method private static parseSenc(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;ILcom/google/android/exoplayer/parser/mp4/TrackFragment;)V
    .locals 7
    .param p0, "senc"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "offset"    # I
    .param p2, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    .prologue
    const/4 v4, 0x0

    .line 1006
    add-int/lit8 v5, p1, 0x8

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 1007
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v1

    .line 1008
    .local v1, "fullAtom":I
    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomFlags(I)I

    move-result v0

    .line 1010
    .local v0, "flags":I
    and-int/lit8 v5, v0, 0x1

    if-eqz v5, :cond_0

    .line 1012
    new-instance v4, Ljava/lang/IllegalStateException;

    const-string v5, "Overriding TrackEncryptionBox parameters is unsupported"

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 1015
    :cond_0
    and-int/lit8 v5, v0, 0x2

    if-eqz v5, :cond_1

    const/4 v3, 0x1

    .line 1016
    .local v3, "subsampleEncryption":Z
    :goto_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v2

    .line 1017
    .local v2, "sampleCount":I
    iget v5, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-eq v2, v5, :cond_2

    .line 1018
    new-instance v4, Ljava/lang/IllegalStateException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Length mismatch: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, ", "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget v6, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v4

    .end local v2    # "sampleCount":I
    .end local v3    # "subsampleEncryption":Z
    :cond_1
    move v3, v4

    .line 1015
    goto :goto_0

    .line 1021
    .restart local v2    # "sampleCount":I
    .restart local v3    # "subsampleEncryption":Z
    :cond_2
    iget-object v5, p2, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleHasSubsampleEncryptionTable:[Z

    invoke-static {v5, v4, v2, v3}, Ljava/util/Arrays;->fill([ZIIZ)V

    .line 1022
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->length()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v5

    sub-int/2addr v4, v5

    invoke-virtual {p2, v4}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->initEncryptionData(I)V

    .line 1023
    invoke-virtual {p2, p0}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->fillEncryptionData(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)V

    .line 1024
    return-void
.end method

.method private static parseSenc(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V
    .locals 1
    .param p0, "senc"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    .prologue
    .line 1002
    const/4 v0, 0x0

    invoke-static {p0, v0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSenc(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;ILcom/google/android/exoplayer/parser/mp4/TrackFragment;)V

    .line 1003
    return-void
.end method

.method private static parseSidx(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/SegmentIndex;
    .locals 30
    .param p0, "atom"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    .line 1030
    const/16 v2, 0x8

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 1031
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v11

    .line 1032
    .local v11, "fullAtom":I
    invoke-static {v11}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomVersion(I)I

    move-result v27

    .line 1034
    .local v27, "version":I
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 1035
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v24

    .line 1038
    .local v24, "timescale":J
    if-nez v27, :cond_0

    .line 1039
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v8

    .line 1040
    .local v8, "earliestPresentationTime":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v12

    .line 1046
    .local v12, "firstOffset":J
    :goto_0
    const/4 v2, 0x2

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 1048
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v15

    .line 1049
    .local v15, "referenceCount":I
    new-array v4, v15, [I

    .line 1050
    .local v4, "sizes":[I
    new-array v5, v15, [J

    .line 1051
    .local v5, "offsets":[J
    new-array v6, v15, [J

    .line 1052
    .local v6, "durationsUs":[J
    new-array v7, v15, [J

    .line 1054
    .local v7, "timesUs":[J
    move-wide/from16 v18, v12

    .line 1055
    .local v18, "offset":J
    move-wide/from16 v22, v8

    .line 1056
    .local v22, "time":J
    const/4 v14, 0x0

    .local v14, "i":I
    :goto_1
    if-ge v14, v15, :cond_2

    .line 1057
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v10

    .line 1059
    .local v10, "firstInt":I
    const/high16 v2, -0x80000000

    and-int v26, v2, v10

    .line 1060
    .local v26, "type":I
    if-eqz v26, :cond_1

    .line 1061
    new-instance v2, Ljava/lang/IllegalStateException;

    const-string v3, "Unhandled indirect reference"

    invoke-direct {v2, v3}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1042
    .end local v4    # "sizes":[I
    .end local v5    # "offsets":[J
    .end local v6    # "durationsUs":[J
    .end local v7    # "timesUs":[J
    .end local v8    # "earliestPresentationTime":J
    .end local v10    # "firstInt":I
    .end local v12    # "firstOffset":J
    .end local v14    # "i":I
    .end local v15    # "referenceCount":I
    .end local v18    # "offset":J
    .end local v22    # "time":J
    .end local v26    # "type":I
    :cond_0
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedLongToLong()J

    move-result-wide v8

    .line 1043
    .restart local v8    # "earliestPresentationTime":J
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedLongToLong()J

    move-result-wide v12

    .restart local v12    # "firstOffset":J
    goto :goto_0

    .line 1063
    .restart local v4    # "sizes":[I
    .restart local v5    # "offsets":[J
    .restart local v6    # "durationsUs":[J
    .restart local v7    # "timesUs":[J
    .restart local v10    # "firstInt":I
    .restart local v14    # "i":I
    .restart local v15    # "referenceCount":I
    .restart local v18    # "offset":J
    .restart local v22    # "time":J
    .restart local v26    # "type":I
    :cond_1
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v20

    .line 1065
    .local v20, "referenceDuration":J
    const v2, 0x7fffffff

    and-int/2addr v2, v10

    aput v2, v4, v14

    .line 1066
    aput-wide v18, v5, v14

    .line 1070
    const-wide/32 v2, 0xf4240

    mul-long v2, v2, v22

    div-long v2, v2, v24

    aput-wide v2, v7, v14

    .line 1071
    add-long v2, v22, v20

    const-wide/32 v28, 0xf4240

    mul-long v2, v2, v28

    div-long v16, v2, v24

    .line 1072
    .local v16, "nextTimeUs":J
    aget-wide v2, v7, v14

    sub-long v2, v16, v2

    aput-wide v2, v6, v14

    .line 1073
    add-long v22, v22, v20

    .line 1075
    const/4 v2, 0x4

    move-object/from16 v0, p0

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 1076
    aget v2, v4, v14

    int-to-long v2, v2

    add-long v18, v18, v2

    .line 1056
    add-int/lit8 v14, v14, 0x1

    goto :goto_1

    .line 1079
    .end local v10    # "firstInt":I
    .end local v16    # "nextTimeUs":J
    .end local v20    # "referenceDuration":J
    .end local v26    # "type":I
    :cond_2
    new-instance v2, Lcom/google/android/exoplayer/parser/SegmentIndex;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->length()I

    move-result v3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/exoplayer/parser/SegmentIndex;-><init>(I[I[J[J[J)V

    return-object v2
.end method

.method private static parseSinfFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    .locals 5
    .param p0, "parent"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "position"    # I
    .param p2, "size"    # I

    .prologue
    .line 706
    add-int/lit8 v2, p1, 0x8

    .line 708
    .local v2, "childPosition":I
    const/4 v3, 0x0

    .line 709
    .local v3, "trackEncryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    :goto_0
    sub-int v4, v2, p1

    if-ge v4, p2, :cond_3

    .line 710
    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 711
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v0

    .line 712
    .local v0, "childAtomSize":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v1

    .line 713
    .local v1, "childAtomType":I
    const v4, 0x66726d61

    if-ne v1, v4, :cond_1

    .line 714
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    .line 722
    :cond_0
    :goto_1
    add-int/2addr v2, v0

    .line 723
    goto :goto_0

    .line 715
    :cond_1
    const v4, 0x7363686d

    if-ne v1, v4, :cond_2

    .line 716
    const/4 v4, 0x4

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 717
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    .line 718
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    goto :goto_1

    .line 719
    :cond_2
    const v4, 0x73636869

    if-ne v1, v4, :cond_0

    .line 720
    invoke-static {p0, v2, v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSchiFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    move-result-object v3

    goto :goto_1

    .line 725
    .end local v0    # "childAtomSize":I
    .end local v1    # "childAtomType":I
    :cond_3
    return-object v3
.end method

.method private static parseStsd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Landroid/util/Pair;
    .locals 10
    .param p0, "stsd"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/exoplayer/MediaFormat;",
            "[",
            "Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;",
            ">;"
        }
    .end annotation

    .prologue
    .line 506
    const/16 v9, 0xc

    invoke-virtual {p0, v9}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 507
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v7

    .line 508
    .local v7, "numberOfEntries":I
    const/4 v6, 0x0

    .line 509
    .local v6, "mediaFormat":Lcom/google/android/exoplayer/MediaFormat;
    new-array v8, v7, [Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    .line 510
    .local v8, "trackEncryptionBoxes":[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    const/4 v5, 0x0

    .local v5, "i":I
    :goto_0
    if-ge v5, v7, :cond_4

    .line 511
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->getPosition()I

    move-result v4

    .line 512
    .local v4, "childStartPosition":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v2

    .line 513
    .local v2, "childAtomSize":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v3

    .line 514
    .local v3, "childAtomType":I
    const v9, 0x61766331

    if-eq v3, v9, :cond_0

    const v9, 0x61766333

    if-eq v3, v9, :cond_0

    const v9, 0x656e6376

    if-ne v3, v9, :cond_2

    .line 516
    :cond_0
    invoke-static {p0, v4, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseAvcFromParent(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;II)Landroid/util/Pair;

    move-result-object v1

    .line 518
    .local v1, "avc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    iget-object v6, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v6    # "mediaFormat":Lcom/google/android/exoplayer/MediaFormat;
    check-cast v6, Lcom/google/android/exoplayer/MediaFormat;

    .line 519
    .restart local v6    # "mediaFormat":Lcom/google/android/exoplayer/MediaFormat;
    iget-object v9, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    aput-object v9, v8, v5

    .line 527
    .end local v1    # "avc":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    :cond_1
    :goto_1
    add-int v9, v4, v2

    invoke-virtual {p0, v9}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 510
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 520
    :cond_2
    const v9, 0x6d703461

    if-eq v3, v9, :cond_3

    const v9, 0x656e6361

    if-eq v3, v9, :cond_3

    const v9, 0x61632d33

    if-ne v3, v9, :cond_1

    .line 522
    :cond_3
    invoke-static {p0, v3, v4, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseAudioSampleEntry(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;III)Landroid/util/Pair;

    move-result-object v0

    .line 524
    .local v0, "audioSampleEntry":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    iget-object v6, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    .end local v6    # "mediaFormat":Lcom/google/android/exoplayer/MediaFormat;
    check-cast v6, Lcom/google/android/exoplayer/MediaFormat;

    .line 525
    .restart local v6    # "mediaFormat":Lcom/google/android/exoplayer/MediaFormat;
    iget-object v9, v0, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v9, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    aput-object v9, v8, v5

    goto :goto_1

    .line 529
    .end local v0    # "audioSampleEntry":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    .end local v2    # "childAtomSize":I
    .end local v3    # "childAtomType":I
    .end local v4    # "childStartPosition":I
    :cond_4
    invoke-static {v6, v8}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v9

    return-object v9
.end method

.method private static parseTfdt(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)J
    .locals 4
    .param p0, "tfdt"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    .line 910
    const/16 v2, 0x8

    invoke-virtual {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 911
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v0

    .line 912
    .local v0, "fullAtom":I
    invoke-static {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomVersion(I)I

    move-result v1

    .line 913
    .local v1, "version":I
    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedLongToLong()J

    move-result-wide v2

    :goto_0
    return-wide v2

    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v2

    goto :goto_0
.end method

.method private static parseTfhd(Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .locals 8
    .param p0, "extendsDefaults"    # Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .param p1, "tfhd"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    const/16 v7, 0x8

    .line 881
    invoke-virtual {p1, v7}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 882
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v5

    .line 883
    .local v5, "fullAtom":I
    invoke-static {v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomFlags(I)I

    move-result v4

    .line 885
    .local v4, "flags":I
    const/4 v6, 0x4

    invoke-virtual {p1, v6}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 886
    and-int/lit8 v6, v4, 0x1

    if-eqz v6, :cond_0

    .line 887
    invoke-virtual {p1, v7}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 890
    :cond_0
    and-int/lit8 v6, v4, 0x2

    if-eqz v6, :cond_1

    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v6

    add-int/lit8 v0, v6, -0x1

    .line 893
    .local v0, "defaultSampleDescriptionIndex":I
    :goto_0
    and-int/lit8 v6, v4, 0x8

    if-eqz v6, :cond_2

    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v1

    .line 895
    .local v1, "defaultSampleDuration":I
    :goto_1
    and-int/lit8 v6, v4, 0x10

    if-eqz v6, :cond_3

    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v3

    .line 897
    .local v3, "defaultSampleSize":I
    :goto_2
    and-int/lit8 v6, v4, 0x20

    if-eqz v6, :cond_4

    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v2

    .line 899
    .local v2, "defaultSampleFlags":I
    :goto_3
    new-instance v6, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    invoke-direct {v6, v0, v1, v3, v2}, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;-><init>(IIII)V

    return-object v6

    .line 890
    .end local v0    # "defaultSampleDescriptionIndex":I
    .end local v1    # "defaultSampleDuration":I
    .end local v2    # "defaultSampleFlags":I
    .end local v3    # "defaultSampleSize":I
    :cond_1
    iget v0, p0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->sampleDescriptionIndex:I

    goto :goto_0

    .line 893
    .restart local v0    # "defaultSampleDescriptionIndex":I
    :cond_2
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->duration:I

    goto :goto_1

    .line 895
    .restart local v1    # "defaultSampleDuration":I
    :cond_3
    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->size:I

    goto :goto_2

    .line 897
    .restart local v3    # "defaultSampleSize":I
    :cond_4
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->flags:I

    goto :goto_3
.end method

.method private static parseTkhd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Landroid/util/Pair;
    .locals 7
    .param p0, "tkhd"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation

    .prologue
    const/16 v5, 0x8

    .line 466
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 467
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v2

    .line 468
    .local v2, "fullAtom":I
    invoke-static {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomVersion(I)I

    move-result v4

    .line 470
    .local v4, "version":I
    if-nez v4, :cond_0

    :goto_0
    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 472
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v3

    .line 473
    .local v3, "trackId":I
    const/4 v5, 0x4

    invoke-virtual {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 474
    if-nez v4, :cond_1

    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedInt()J

    move-result-wide v0

    .line 476
    .local v0, "duration":J
    :goto_1
    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v5, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v5

    return-object v5

    .line 470
    .end local v0    # "duration":J
    .end local v3    # "trackId":I
    :cond_0
    const/16 v5, 0x10

    goto :goto_0

    .line 474
    .restart local v3    # "trackId":I
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedLongToLong()J

    move-result-wide v0

    goto :goto_1
.end method

.method private static parseTraf(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;I[B)V
    .locals 18
    .param p0, "track"    # Lcom/google/android/exoplayer/parser/mp4/Track;
    .param p1, "extendsDefaults"    # Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .param p2, "traf"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    .param p3, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;
    .param p4, "workaroundFlags"    # I
    .param p5, "extendedTypeScratch"    # [B

    .prologue
    .line 810
    const v2, 0x74666474

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v14

    .line 811
    .local v14, "tfdtAtom":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    if-nez v14, :cond_3

    const-wide/16 v4, 0x0

    .line 813
    .local v4, "decodeTime":J
    :goto_0
    const v2, 0x74666864

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v15

    .line 814
    .local v15, "tfhd":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    iget-object v2, v15, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    move-object/from16 v0, p1

    invoke-static {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTfhd(Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    move-result-object v3

    .line 815
    .local v3, "fragmentHeader":Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    iget v2, v3, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->sampleDescriptionIndex:I

    move-object/from16 v0, p3

    iput v2, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleDescriptionIndex:I

    .line 817
    const v2, 0x7472756e

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v17

    .line 818
    .local v17, "trun":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    move-object/from16 v0, v17

    iget-object v7, v0, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    move-object/from16 v2, p0

    move/from16 v6, p4

    move-object/from16 v8, p3

    invoke-static/range {v2 .. v8}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTrun(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;JILcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V

    .line 820
    const v2, 0x7361697a

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v12

    .line 821
    .local v12, "saiz":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    if-eqz v12, :cond_0

    .line 822
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/parser/mp4/Track;->sampleDescriptionEncryptionBoxes:[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    iget v6, v3, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->sampleDescriptionIndex:I

    aget-object v16, v2, v6

    .line 824
    .local v16, "trackEncryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    iget-object v2, v12, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    move-object/from16 v0, v16

    move-object/from16 v1, p3

    invoke-static {v0, v2, v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSaiz(Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V

    .line 827
    .end local v16    # "trackEncryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    :cond_0
    const v2, 0x73656e63

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v13

    .line 828
    .local v13, "senc":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    if-eqz v13, :cond_1

    .line 829
    iget-object v2, v13, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    move-object/from16 v0, p3

    invoke-static {v2, v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSenc(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V

    .line 832
    :cond_1
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v10

    .line 833
    .local v10, "childrenSize":I
    const/4 v11, 0x0

    .local v11, "i":I
    :goto_1
    if-ge v11, v10, :cond_4

    .line 834
    move-object/from16 v0, p2

    iget-object v2, v0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v2, v11}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/exoplayer/parser/mp4/Atom;

    .line 835
    .local v9, "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget v2, v9, Lcom/google/android/exoplayer/parser/mp4/Atom;->type:I

    const v6, 0x75756964

    if-ne v2, v6, :cond_2

    .line 836
    check-cast v9, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    .end local v9    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget-object v2, v9, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    move-object/from16 v0, p3

    move-object/from16 v1, p5

    invoke-static {v2, v0, v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseUuid(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;[B)V

    .line 833
    :cond_2
    add-int/lit8 v11, v11, 0x1

    goto :goto_1

    .line 811
    .end local v3    # "fragmentHeader":Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .end local v4    # "decodeTime":J
    .end local v10    # "childrenSize":I
    .end local v11    # "i":I
    .end local v12    # "saiz":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .end local v13    # "senc":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .end local v15    # "tfhd":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .end local v17    # "trun":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    :cond_3
    const v2, 0x74666474

    move-object/from16 v0, p2

    invoke-virtual {v0, v2}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTfdt(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)J

    move-result-wide v4

    goto/16 :goto_0

    .line 839
    .restart local v3    # "fragmentHeader":Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .restart local v4    # "decodeTime":J
    .restart local v10    # "childrenSize":I
    .restart local v11    # "i":I
    .restart local v12    # "saiz":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .restart local v13    # "senc":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .restart local v15    # "tfhd":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .restart local v17    # "trun":Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    :cond_4
    return-void
.end method

.method private static parseTrak(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)Lcom/google/android/exoplayer/parser/mp4/Track;
    .locals 11
    .param p0, "trak"    # Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    .prologue
    .line 441
    const v1, 0x6d646961

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v8

    .line 442
    .local v8, "mdia":Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    const v1, 0x68646c72    # 4.3148E24f

    invoke-virtual {v8, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseHdlr(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)I

    move-result v3

    .line 443
    .local v3, "trackType":I
    const v1, 0x736f756e

    if-eq v3, v1, :cond_0

    const v1, 0x76696465

    if-ne v3, v1, :cond_1

    :cond_0
    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/exoplayer/util/Assertions;->checkState(Z)V

    .line 445
    const v1, 0x746b6864

    invoke-virtual {p0, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseTkhd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Landroid/util/Pair;

    move-result-object v0

    .line 446
    .local v0, "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Long;>;"
    iget-object v1, v0, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v2

    .line 451
    .local v2, "id":I
    const v1, 0x6d646864

    invoke-virtual {v8, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseMdhd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)J

    move-result-wide v4

    .line 452
    .local v4, "timescale":J
    const v1, 0x6d696e66

    invoke-virtual {v8, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v1

    const v6, 0x7374626c

    invoke-virtual {v1, v6}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    move-result-object v10

    .line 455
    .local v10, "stbl":Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    const v1, 0x73747364

    invoke-virtual {v10, v1}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;->data:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseStsd(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Landroid/util/Pair;

    move-result-object v9

    .line 457
    .local v9, "sampleDescriptions":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    new-instance v1, Lcom/google/android/exoplayer/parser/mp4/Track;

    iget-object v6, v9, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Lcom/google/android/exoplayer/MediaFormat;

    iget-object v7, v9, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v7, [Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    invoke-direct/range {v1 .. v7}, Lcom/google/android/exoplayer/parser/mp4/Track;-><init>(IIJLcom/google/android/exoplayer/MediaFormat;[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;)V

    return-object v1

    .line 443
    .end local v0    # "header":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/Integer;Ljava/lang/Long;>;"
    .end local v2    # "id":I
    .end local v4    # "timescale":J
    .end local v9    # "sampleDescriptions":Landroid/util/Pair;, "Landroid/util/Pair<Lcom/google/android/exoplayer/MediaFormat;[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;>;"
    .end local v10    # "stbl":Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private static parseTrex(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .locals 5
    .param p0, "trex"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .prologue
    .line 428
    const/16 v4, 0x10

    invoke-virtual {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 429
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v4

    add-int/lit8 v0, v4, -0x1

    .line 430
    .local v0, "defaultSampleDescriptionIndex":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v1

    .line 431
    .local v1, "defaultSampleDuration":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v3

    .line 432
    .local v3, "defaultSampleSize":I
    invoke-virtual {p0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v2

    .line 433
    .local v2, "defaultSampleFlags":I
    new-instance v4, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;

    invoke-direct {v4, v0, v1, v3, v2}, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;-><init>(IIII)V

    return-object v4
.end method

.method private static parseTrun(Lcom/google/android/exoplayer/parser/mp4/Track;Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;JILcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;)V
    .locals 28
    .param p0, "track"    # Lcom/google/android/exoplayer/parser/mp4/Track;
    .param p1, "defaultSampleValues"    # Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;
    .param p2, "decodeTime"    # J
    .param p4, "workaroundFlags"    # I
    .param p5, "trun"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p6, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    .prologue
    .line 927
    const/16 v25, 0x8

    move-object/from16 v0, p5

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 928
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v7

    .line 929
    .local v7, "fullAtom":I
    invoke-static {v7}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseFullAtomFlags(I)I

    move-result v6

    .line 931
    .local v6, "flags":I
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v11

    .line 932
    .local v11, "sampleCount":I
    and-int/lit8 v25, v6, 0x1

    if-eqz v25, :cond_0

    .line 933
    const/16 v25, 0x4

    move-object/from16 v0, p5

    move/from16 v1, v25

    invoke-virtual {v0, v1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 936
    :cond_0
    and-int/lit8 v25, v6, 0x4

    if-eqz v25, :cond_3

    const/4 v5, 0x1

    .line 937
    .local v5, "firstSampleFlagsPresent":Z
    :goto_0
    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->flags:I

    .line 938
    .local v4, "firstSampleFlags":I
    if-eqz v5, :cond_1

    .line 939
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v4

    .line 942
    :cond_1
    and-int/lit16 v0, v6, 0x100

    move/from16 v25, v0

    if-eqz v25, :cond_4

    const/4 v14, 0x1

    .line 943
    .local v14, "sampleDurationsPresent":Z
    :goto_1
    and-int/lit16 v0, v6, 0x200

    move/from16 v25, v0

    if-eqz v25, :cond_5

    const/16 v21, 0x1

    .line 944
    .local v21, "sampleSizesPresent":Z
    :goto_2
    and-int/lit16 v0, v6, 0x400

    move/from16 v25, v0

    if-eqz v25, :cond_6

    const/16 v16, 0x1

    .line 945
    .local v16, "sampleFlagsPresent":Z
    :goto_3
    and-int/lit16 v0, v6, 0x800

    move/from16 v25, v0

    if-eqz v25, :cond_7

    const/4 v10, 0x1

    .line 948
    .local v10, "sampleCompositionTimeOffsetsPresent":Z
    :goto_4
    move-object/from16 v0, p6

    invoke-virtual {v0, v11}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->initTables(I)V

    .line 949
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleSizeTable:[I

    move-object/from16 v20, v0

    .line 950
    .local v20, "sampleSizeTable":[I
    move-object/from16 v0, p6

    iget-object v9, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleCompositionTimeOffsetTable:[I

    .line 951
    .local v9, "sampleCompositionTimeOffsetTable":[I
    move-object/from16 v0, p6

    iget-object v12, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleDecodingTimeTable:[J

    .line 952
    .local v12, "sampleDecodingTimeTable":[J
    move-object/from16 v0, p6

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleIsSyncFrameTable:[Z

    move-object/from16 v17, v0

    .line 954
    .local v17, "sampleIsSyncFrameTable":[Z
    move-object/from16 v0, p0

    iget-wide v0, v0, Lcom/google/android/exoplayer/parser/mp4/Track;->timescale:J

    move-wide/from16 v22, v0

    .line 955
    .local v22, "timescale":J
    move-wide/from16 v2, p2

    .line 956
    .local v2, "cumulativeTime":J
    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/exoplayer/parser/mp4/Track;->type:I

    move/from16 v25, v0

    const v26, 0x76696465

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    and-int/lit8 v25, p4, 0x1

    const/16 v26, 0x1

    move/from16 v0, v25

    move/from16 v1, v26

    if-ne v0, v1, :cond_8

    const/16 v24, 0x1

    .line 959
    .local v24, "workaroundEveryVideoFrameIsSyncFrame":Z
    :goto_5
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_6
    if-ge v8, v11, :cond_f

    .line 961
    if-eqz v14, :cond_9

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v13

    .line 963
    .local v13, "sampleDuration":I
    :goto_7
    if-eqz v21, :cond_a

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v19

    .line 964
    .local v19, "sampleSize":I
    :goto_8
    if-nez v8, :cond_b

    if-eqz v5, :cond_b

    move v15, v4

    .line 966
    .local v15, "sampleFlags":I
    :goto_9
    if-eqz v10, :cond_d

    .line 972
    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v18

    .line 973
    .local v18, "sampleOffset":I
    move/from16 v0, v18

    mul-int/lit16 v0, v0, 0x3e8

    move/from16 v25, v0

    move/from16 v0, v25

    int-to-long v0, v0

    move-wide/from16 v26, v0

    div-long v26, v26, v22

    move-wide/from16 v0, v26

    long-to-int v0, v0

    move/from16 v25, v0

    aput v25, v9, v8

    .line 977
    .end local v18    # "sampleOffset":I
    :goto_a
    const-wide/16 v26, 0x3e8

    mul-long v26, v26, v2

    div-long v26, v26, v22

    aput-wide v26, v12, v8

    .line 978
    aput v19, v20, v8

    .line 979
    shr-int/lit8 v25, v15, 0x10

    and-int/lit8 v25, v25, 0x1

    if-nez v25, :cond_e

    if-eqz v24, :cond_2

    if-nez v8, :cond_e

    :cond_2
    const/16 v25, 0x1

    :goto_b
    aput-boolean v25, v17, v8

    .line 981
    int-to-long v0, v13

    move-wide/from16 v26, v0

    add-long v2, v2, v26

    .line 959
    add-int/lit8 v8, v8, 0x1

    goto :goto_6

    .line 936
    .end local v2    # "cumulativeTime":J
    .end local v4    # "firstSampleFlags":I
    .end local v5    # "firstSampleFlagsPresent":Z
    .end local v8    # "i":I
    .end local v9    # "sampleCompositionTimeOffsetTable":[I
    .end local v10    # "sampleCompositionTimeOffsetsPresent":Z
    .end local v12    # "sampleDecodingTimeTable":[J
    .end local v13    # "sampleDuration":I
    .end local v14    # "sampleDurationsPresent":Z
    .end local v15    # "sampleFlags":I
    .end local v16    # "sampleFlagsPresent":Z
    .end local v17    # "sampleIsSyncFrameTable":[Z
    .end local v19    # "sampleSize":I
    .end local v20    # "sampleSizeTable":[I
    .end local v21    # "sampleSizesPresent":Z
    .end local v22    # "timescale":J
    .end local v24    # "workaroundEveryVideoFrameIsSyncFrame":Z
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 942
    .restart local v4    # "firstSampleFlags":I
    .restart local v5    # "firstSampleFlagsPresent":Z
    :cond_4
    const/4 v14, 0x0

    goto/16 :goto_1

    .line 943
    .restart local v14    # "sampleDurationsPresent":Z
    :cond_5
    const/16 v21, 0x0

    goto/16 :goto_2

    .line 944
    .restart local v21    # "sampleSizesPresent":Z
    :cond_6
    const/16 v16, 0x0

    goto/16 :goto_3

    .line 945
    .restart local v16    # "sampleFlagsPresent":Z
    :cond_7
    const/4 v10, 0x0

    goto/16 :goto_4

    .line 956
    .restart local v2    # "cumulativeTime":J
    .restart local v9    # "sampleCompositionTimeOffsetTable":[I
    .restart local v10    # "sampleCompositionTimeOffsetsPresent":Z
    .restart local v12    # "sampleDecodingTimeTable":[J
    .restart local v17    # "sampleIsSyncFrameTable":[Z
    .restart local v20    # "sampleSizeTable":[I
    .restart local v22    # "timescale":J
    :cond_8
    const/16 v24, 0x0

    goto :goto_5

    .line 961
    .restart local v8    # "i":I
    .restart local v24    # "workaroundEveryVideoFrameIsSyncFrame":Z
    :cond_9
    move-object/from16 v0, p1

    iget v13, v0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->duration:I

    goto :goto_7

    .line 963
    .restart local v13    # "sampleDuration":I
    :cond_a
    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->size:I

    move/from16 v19, v0

    goto :goto_8

    .line 964
    .restart local v19    # "sampleSize":I
    :cond_b
    if-eqz v16, :cond_c

    invoke-virtual/range {p5 .. p5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v15

    goto :goto_9

    :cond_c
    move-object/from16 v0, p1

    iget v15, v0, Lcom/google/android/exoplayer/parser/mp4/DefaultSampleValues;->flags:I

    goto :goto_9

    .line 975
    .restart local v15    # "sampleFlags":I
    :cond_d
    const/16 v25, 0x0

    aput v25, v9, v8

    goto :goto_a

    .line 979
    :cond_e
    const/16 v25, 0x0

    goto :goto_b

    .line 983
    .end local v13    # "sampleDuration":I
    .end local v15    # "sampleFlags":I
    .end local v19    # "sampleSize":I
    :cond_f
    return-void
.end method

.method private static parseUuid(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/parser/mp4/TrackFragment;[B)V
    .locals 2
    .param p0, "uuid"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p1, "out"    # Lcom/google/android/exoplayer/parser/mp4/TrackFragment;
    .param p2, "extendedTypeScratch"    # [B

    .prologue
    const/16 v1, 0x10

    .line 987
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 988
    const/4 v0, 0x0

    invoke-virtual {p0, p2, v0, v1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readBytes([BII)V

    .line 991
    sget-object v0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->PIFF_SAMPLE_ENCRYPTION_BOX_EXTENDED_TYPE:[B

    invoke-static {p2, v0}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v0

    if-nez v0, :cond_0

    .line 999
    :goto_0
    return-void

    .line 998
    :cond_0
    invoke-static {p0, v1, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parseSenc(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;ILcom/google/android/exoplayer/parser/mp4/TrackFragment;)V

    goto :goto_0
.end method

.method private readAtomHeader(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I
    .locals 8
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    .prologue
    const/16 v7, 0x8

    const/4 v2, 0x2

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 291
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    rsub-int/lit8 v1, v5, 0x8

    .line 292
    .local v1, "remainingBytes":I
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    iget-object v5, v5, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    invoke-interface {p1, v5, v6, v1}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->read([BII)I

    move-result v0

    .line 293
    .local v0, "bytesRead":I
    const/4 v5, -0x1

    if-ne v0, v5, :cond_0

    .line 330
    :goto_0
    return v2

    .line 296
    :cond_0
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    add-int/2addr v5, v0

    iput v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    .line 297
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    add-int/2addr v5, v0

    iput v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    .line 298
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    if-eq v5, v7, :cond_1

    move v2, v3

    .line 299
    goto :goto_0

    .line 302
    :cond_1
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-virtual {v5, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->setPosition(I)V

    .line 303
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-virtual {v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    .line 304
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-virtual {v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readInt()I

    move-result v5

    iput v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    .line 306
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    const v6, 0x6d646174

    if-ne v5, v6, :cond_3

    .line 307
    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-boolean v3, v3, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleEncryptionDataNeedsFill:Z

    if-eqz v3, :cond_2

    .line 308
    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    :goto_1
    move v2, v4

    .line 312
    goto :goto_0

    .line 310
    :cond_2
    const/4 v2, 0x3

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    goto :goto_1

    .line 315
    :cond_3
    sget-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->PARSED_ATOMS:Ljava/util/Set;

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_5

    .line 316
    sget-object v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->CONTAINER_TYPES:Ljava/util/Set;

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v2, v5}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 317
    invoke-direct {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 318
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    new-instance v3, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    invoke-direct {v3, v5}, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;-><init>(I)V

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    .line 319
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    add-int/2addr v3, v5

    add-int/lit8 v3, v3, -0x8

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/Stack;->add(Ljava/lang/Object;)Z

    :goto_2
    move v2, v4

    .line 330
    goto :goto_0

    .line 321
    :cond_4
    new-instance v2, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    invoke-direct {v2, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .line 322
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomHeader:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    iget-object v2, v2, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    iget-object v5, v5, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    invoke-static {v2, v4, v5, v4, v7}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 323
    invoke-direct {p0, v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    goto :goto_2

    .line 326
    :cond_5
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .line 327
    invoke-direct {p0, v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    goto :goto_2
.end method

.method private readAtomPayload(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I
    .locals 6
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    .prologue
    .line 335
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    if-eqz v2, :cond_0

    .line 336
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    iget-object v2, v2, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    sub-int/2addr v4, v5

    invoke-interface {p1, v2, v3, v4}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->read([BII)I

    move-result v0

    .line 340
    .local v0, "bytesRead":I
    :goto_0
    const/4 v2, -0x1

    if-ne v0, v2, :cond_1

    .line 341
    const/4 v1, 0x2

    .line 361
    :goto_1
    return v1

    .line 338
    .end local v0    # "bytesRead":I
    :cond_0
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    sub-int/2addr v2, v3

    invoke-interface {p1, v2}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->skip(I)I

    move-result v0

    .restart local v0    # "bytesRead":I
    goto :goto_0

    .line 343
    :cond_1
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    .line 344
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    add-int/2addr v2, v0

    iput v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    .line 345
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomBytesRead:I

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomSize:I

    if-eq v2, v3, :cond_2

    .line 346
    const/4 v1, 0x1

    goto :goto_1

    .line 349
    :cond_2
    const/4 v1, 0x0

    .line 350
    .local v1, "results":I
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    if-eqz v2, :cond_3

    .line 351
    new-instance v2, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomType:I

    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->atomData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-direct {v2, v3, v4}, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;-><init>(ILcom/google/android/exoplayer/parser/mp4/ParsableByteArray;)V

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->onLeafAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;)I

    move-result v2

    or-int/2addr v1, v2

    .line 355
    :cond_3
    :goto_2
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_4

    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->peek()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/Integer;

    invoke-virtual {v2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->rootAtomBytesRead:I

    if-ne v2, v3, :cond_4

    .line 356
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    .line 357
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v2}, Ljava/util/Stack;->pop()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->onContainerAtomRead(Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;)I

    move-result v2

    or-int/2addr v1, v2

    goto :goto_2

    .line 360
    :cond_4
    const/4 v2, 0x0

    invoke-direct {p0, v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    goto :goto_1
.end method

.method private readEncryptionData(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I
    .locals 2
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    .prologue
    .line 1083
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-virtual {v1, p1}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->fillEncryptionData(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)Z

    move-result v0

    .line 1084
    .local v0, "success":Z
    if-nez v0, :cond_0

    .line 1085
    const/4 v1, 0x1

    .line 1088
    :goto_0
    return v1

    .line 1087
    :cond_0
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 1088
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private readOrSkipSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 6
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .param p2, "out"    # Lcom/google/android/exoplayer/SampleHolder;

    .prologue
    const/4 v1, 0x0

    .line 1109
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v3, v3, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-lt v2, v3, :cond_0

    .line 1111
    invoke-direct {p0, v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 1121
    :goto_0
    return v1

    .line 1114
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v1, v1, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleSizeTable:[I

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    aget v0, v1, v2

    .line 1115
    .local v0, "sampleSize":I
    invoke-interface {p1}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->getAvailableByteCount()J

    move-result-wide v2

    int-to-long v4, v0

    cmp-long v1, v2, v4

    if-gez v1, :cond_1

    .line 1116
    const/4 v1, 0x1

    goto :goto_0

    .line 1118
    :cond_1
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekSyncSampleIndex:I

    if-ge v1, v2, :cond_2

    .line 1119
    invoke-direct {p0, p1, v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->skipSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;I)I

    move-result v1

    goto :goto_0

    .line 1121
    :cond_2
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;ILcom/google/android/exoplayer/SampleHolder;)I

    move-result v1

    goto :goto_0
.end method

.method private readSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;ILcom/google/android/exoplayer/SampleHolder;)I
    .locals 9
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .param p2, "sampleSize"    # I
    .param p3, "out"    # Lcom/google/android/exoplayer/SampleHolder;

    .prologue
    const/4 v8, 0x0

    .line 1147
    if-nez p3, :cond_0

    .line 1148
    const/16 v4, 0x20

    .line 1188
    :goto_0
    return v4

    .line 1150
    :cond_0
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    invoke-virtual {v4, v5}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->getSamplePresentationTime(I)J

    move-result-wide v4

    const-wide/16 v6, 0x3e8

    mul-long/2addr v4, v6

    iput-wide v4, p3, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    .line 1151
    iput v8, p3, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 1152
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleIsSyncFrameTable:[Z

    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    aget-boolean v4, v4, v5

    if-eqz v4, :cond_1

    .line 1153
    iget v4, p3, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    or-int/lit8 v4, v4, 0x1

    iput v4, p3, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 1154
    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    iput v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->lastSyncSampleIndex:I

    .line 1156
    :cond_1
    iget-object v4, p3, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    if-eqz v4, :cond_2

    iget-object v4, p3, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    invoke-virtual {v4}, Ljava/nio/ByteBuffer;->capacity()I

    move-result v4

    if-ge v4, p2, :cond_3

    .line 1157
    :cond_2
    invoke-virtual {p3, p2}, Lcom/google/android/exoplayer/SampleHolder;->replaceBuffer(I)Z

    .line 1159
    :cond_3
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-boolean v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->definesEncryptionData:Z

    if-eqz v4, :cond_4

    .line 1160
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleEncryptionData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    invoke-direct {p0, v4, p3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readSampleEncryptionData(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/SampleHolder;)V

    .line 1163
    :cond_4
    iget-object v1, p3, Lcom/google/android/exoplayer/SampleHolder;->data:Ljava/nio/ByteBuffer;

    .line 1164
    .local v1, "outputData":Ljava/nio/ByteBuffer;
    if-nez v1, :cond_5

    .line 1165
    invoke-interface {p1, p2}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->skip(I)I

    .line 1166
    iput v8, p3, Lcom/google/android/exoplayer/SampleHolder;->size:I

    .line 1186
    :goto_1
    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    add-int/lit8 v4, v4, 0x1

    iput v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    .line 1187
    const/4 v4, 0x3

    invoke-direct {p0, v4}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 1188
    const/4 v4, 0x4

    goto :goto_0

    .line 1168
    :cond_5
    invoke-interface {p1, v1, p2}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->read(Ljava/nio/ByteBuffer;I)I

    .line 1169
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    iget v4, v4, Lcom/google/android/exoplayer/parser/mp4/Track;->type:I

    const v5, 0x76696465

    if-ne v4, v5, :cond_7

    .line 1172
    invoke-virtual {v1}, Ljava/nio/ByteBuffer;->position()I

    move-result v4

    sub-int v3, v4, p2

    .line 1173
    .local v3, "sampleOffset":I
    move v2, v3

    .line 1174
    .local v2, "position":I
    :goto_2
    add-int v4, v3, p2

    if-ge v2, v4, :cond_6

    .line 1175
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1176
    invoke-static {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readUnsignedIntToInt(Ljava/nio/ByteBuffer;)I

    move-result v0

    .line 1177
    .local v0, "length":I
    invoke-virtual {v1, v2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1178
    sget-object v4, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->NAL_START_CODE:[B

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->put([B)Ljava/nio/ByteBuffer;

    .line 1179
    add-int/lit8 v4, v0, 0x4

    add-int/2addr v2, v4

    .line 1180
    goto :goto_2

    .line 1181
    .end local v0    # "length":I
    :cond_6
    add-int v4, v3, p2

    invoke-virtual {v1, v4}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 1183
    .end local v2    # "position":I
    .end local v3    # "sampleOffset":I
    :cond_7
    iput p2, p3, Lcom/google/android/exoplayer/SampleHolder;->size:I

    goto :goto_1
.end method

.method private readSampleEncryptionData(Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;Lcom/google/android/exoplayer/SampleHolder;)V
    .locals 13
    .param p1, "sampleEncryptionData"    # Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .param p2, "out"    # Lcom/google/android/exoplayer/SampleHolder;

    .prologue
    .line 1193
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/Track;->sampleDescriptionEncryptionBoxes:[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    iget-object v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v6, v6, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleDescriptionIndex:I

    aget-object v7, v0, v6

    .line 1195
    .local v7, "encryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    iget-object v4, v7, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;->keyId:[B

    .line 1196
    .local v4, "keyId":[B
    iget-boolean v9, v7, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;->isEncrypted:Z

    .line 1197
    .local v9, "isEncrypted":Z
    iget v11, v7, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;->initializationVectorSize:I

    .line 1198
    .local v11, "vectorSize":I
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleHasSubsampleEncryptionTable:[Z

    iget v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    aget-boolean v10, v0, v6

    .line 1200
    .local v10, "subsampleEncryption":Z
    iget-object v0, p2, Lcom/google/android/exoplayer/SampleHolder;->cryptoInfo:Lcom/google/android/exoplayer/CryptoInfo;

    iget-object v5, v0, Lcom/google/android/exoplayer/CryptoInfo;->iv:[B

    .line 1201
    .local v5, "vector":[B
    if-eqz v5, :cond_0

    array-length v0, v5

    const/16 v6, 0x10

    if-eq v0, v6, :cond_1

    .line 1202
    :cond_0
    const/16 v0, 0x10

    new-array v5, v0, [B

    .line 1204
    :cond_1
    const/4 v0, 0x0

    invoke-virtual {p1, v5, v0, v11}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readBytes([BII)V

    .line 1206
    if-eqz v10, :cond_6

    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v1

    .line 1207
    .local v1, "subsampleCount":I
    :goto_0
    iget-object v0, p2, Lcom/google/android/exoplayer/SampleHolder;->cryptoInfo:Lcom/google/android/exoplayer/CryptoInfo;

    iget-object v2, v0, Lcom/google/android/exoplayer/CryptoInfo;->numBytesOfClearData:[I

    .line 1208
    .local v2, "clearDataSizes":[I
    if-eqz v2, :cond_2

    array-length v0, v2

    if-ge v0, v1, :cond_3

    .line 1209
    :cond_2
    new-array v2, v1, [I

    .line 1211
    :cond_3
    iget-object v0, p2, Lcom/google/android/exoplayer/SampleHolder;->cryptoInfo:Lcom/google/android/exoplayer/CryptoInfo;

    iget-object v3, v0, Lcom/google/android/exoplayer/CryptoInfo;->numBytesOfEncryptedData:[I

    .line 1212
    .local v3, "encryptedDataSizes":[I
    if-eqz v3, :cond_4

    array-length v0, v3

    if-ge v0, v1, :cond_5

    .line 1213
    :cond_4
    new-array v3, v1, [I

    .line 1215
    :cond_5
    if-eqz v10, :cond_7

    .line 1216
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    if-ge v8, v1, :cond_8

    .line 1217
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v0

    aput v0, v2, v8

    .line 1218
    invoke-virtual {p1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedIntToInt()I

    move-result v0

    aput v0, v3, v8

    .line 1216
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 1206
    .end local v1    # "subsampleCount":I
    .end local v2    # "clearDataSizes":[I
    .end local v3    # "encryptedDataSizes":[I
    .end local v8    # "i":I
    :cond_6
    const/4 v1, 0x1

    goto :goto_0

    .line 1221
    .restart local v1    # "subsampleCount":I
    .restart local v2    # "clearDataSizes":[I
    .restart local v3    # "encryptedDataSizes":[I
    :cond_7
    const/4 v0, 0x0

    const/4 v6, 0x0

    aput v6, v2, v0

    .line 1222
    const/4 v0, 0x0

    iget-object v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v6, v6, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleSizeTable:[I

    iget v12, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    aget v6, v6, v12

    aput v6, v3, v0

    .line 1224
    :cond_8
    iget-object v0, p2, Lcom/google/android/exoplayer/SampleHolder;->cryptoInfo:Lcom/google/android/exoplayer/CryptoInfo;

    if-eqz v9, :cond_a

    const/4 v6, 0x1

    :goto_2
    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer/CryptoInfo;->set(I[I[I[B[BI)V

    .line 1226
    if-eqz v9, :cond_9

    .line 1227
    iget v0, p2, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    or-int/lit8 v0, v0, 0x2

    iput v0, p2, Lcom/google/android/exoplayer/SampleHolder;->flags:I

    .line 1229
    :cond_9
    return-void

    .line 1224
    :cond_a
    const/4 v6, 0x0

    goto :goto_2
.end method

.method private static readUnsignedIntToInt(Ljava/nio/ByteBuffer;)I
    .locals 5
    .param p0, "data"    # Ljava/nio/ByteBuffer;

    .prologue
    .line 1252
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    and-int/lit16 v1, v2, 0xff

    .line 1253
    .local v1, "result":I
    const/4 v0, 0x1

    .local v0, "i":I
    :goto_0
    const/4 v2, 0x4

    if-ge v0, v2, :cond_0

    .line 1254
    shl-int/lit8 v1, v1, 0x8

    .line 1255
    invoke-virtual {p0}, Ljava/nio/ByteBuffer;->get()B

    move-result v2

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    .line 1253
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 1257
    :cond_0
    if-gez v1, :cond_1

    .line 1258
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Top bit not zero: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 1260
    :cond_1
    return v1
.end method

.method private skipSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;I)I
    .locals 7
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .param p2, "sampleSize"    # I

    .prologue
    .line 1125
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-boolean v5, v5, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->definesEncryptionData:Z

    if-eqz v5, :cond_0

    .line 1126
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v1, v5, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleEncryptionData:Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;

    .line 1127
    .local v1, "sampleEncryptionData":Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    iget-object v5, v5, Lcom/google/android/exoplayer/parser/mp4/Track;->sampleDescriptionEncryptionBoxes:[Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;

    iget-object v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v6, v6, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleDescriptionIndex:I

    aget-object v0, v5, v6

    .line 1129
    .local v0, "encryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    iget v4, v0, Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;->initializationVectorSize:I

    .line 1130
    .local v4, "vectorSize":I
    iget-object v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v5, v5, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleHasSubsampleEncryptionTable:[Z

    iget v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    aget-boolean v3, v5, v6

    .line 1131
    .local v3, "subsampleEncryption":Z
    invoke-virtual {v1, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 1132
    if-eqz v3, :cond_1

    invoke-virtual {v1}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->readUnsignedShort()I

    move-result v2

    .line 1133
    .local v2, "subsampleCount":I
    :goto_0
    if-eqz v3, :cond_0

    .line 1134
    mul-int/lit8 v5, v2, 0x6

    invoke-virtual {v1, v5}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->skip(I)V

    .line 1138
    .end local v0    # "encryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    .end local v1    # "sampleEncryptionData":Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .end local v2    # "subsampleCount":I
    .end local v3    # "subsampleEncryption":Z
    .end local v4    # "vectorSize":I
    :cond_0
    invoke-interface {p1, p2}, Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;->skip(I)I

    .line 1140
    iget v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    add-int/lit8 v5, v5, 0x1

    iput v5, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    .line 1141
    const/4 v5, 0x3

    invoke-direct {p0, v5}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 1142
    const/4 v5, 0x0

    return v5

    .line 1132
    .restart local v0    # "encryptionBox":Lcom/google/android/exoplayer/parser/mp4/TrackEncryptionBox;
    .restart local v1    # "sampleEncryptionData":Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
    .restart local v3    # "subsampleEncryption":Z
    .restart local v4    # "vectorSize":I
    :cond_1
    const/4 v2, 0x1

    goto :goto_0
.end method


# virtual methods
.method public getFormat()Lcom/google/android/exoplayer/MediaFormat;
    .locals 1

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->track:Lcom/google/android/exoplayer/parser/mp4/Track;

    iget-object v0, v0, Lcom/google/android/exoplayer/parser/mp4/Track;->mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

    goto :goto_0
.end method

.method public getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;
    .locals 1

    .prologue
    .line 211
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->segmentIndex:Lcom/google/android/exoplayer/parser/SegmentIndex;

    return-object v0
.end method

.method public getPsshInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->psshData:Ljava/util/HashMap;

    invoke-virtual {v0}, Ljava/util/HashMap;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->psshData:Ljava/util/HashMap;

    goto :goto_0
.end method

.method public hasRelativeIndexOffsets()Z
    .locals 1

    .prologue
    .line 216
    const/4 v0, 0x1

    return v0
.end method

.method public read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I
    .locals 3
    .param p1, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .param p2, "out"    # Lcom/google/android/exoplayer/SampleHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 228
    const/4 v1, 0x0

    .line 229
    .local v1, "results":I
    :goto_0
    and-int/lit8 v2, v1, 0x27

    if-nez v2, :cond_0

    .line 230
    :try_start_0
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->parserState:I

    packed-switch v2, :pswitch_data_0

    .line 241
    invoke-direct {p0, p1, p2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readOrSkipSample(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v2

    or-int/2addr v1, v2

    .line 242
    goto :goto_0

    .line 232
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readAtomHeader(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I

    move-result v2

    or-int/2addr v1, v2

    .line 233
    goto :goto_0

    .line 235
    :pswitch_1
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readAtomPayload(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I

    move-result v2

    or-int/2addr v1, v2

    .line 236
    goto :goto_0

    .line 238
    :pswitch_2
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->readEncryptionData(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result v2

    or-int/2addr v1, v2

    .line 239
    goto :goto_0

    .line 246
    :catch_0
    move-exception v0

    .line 247
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/google/android/exoplayer/ParserException;

    invoke-direct {v2, v0}, Lcom/google/android/exoplayer/ParserException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 245
    .end local v0    # "e":Ljava/lang/Exception;
    :cond_0
    return v1

    .line 230
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public seekTo(JZ)Z
    .locals 9
    .param p1, "seekTimeUs"    # J
    .param p3, "allowNoop"    # Z

    .prologue
    const/4 v3, 0x0

    .line 253
    const-wide/16 v4, 0x3e8

    div-long v4, p1, v4

    long-to-int v4, v4

    iput v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    .line 254
    if-eqz p3, :cond_3

    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    if-eqz v4, :cond_3

    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-lez v4, :cond_3

    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    int-to-long v4, v4

    iget-object v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-virtual {v6, v3}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->getSamplePresentationTime(I)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-ltz v4, :cond_3

    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    int-to-long v4, v4

    iget-object v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v7, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v7, v7, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    add-int/lit8 v7, v7, -0x1

    invoke-virtual {v6, v7}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->getSamplePresentationTime(I)J

    move-result-wide v6

    cmp-long v4, v4, v6

    if-gtz v4, :cond_3

    .line 257
    const/4 v1, 0x0

    .line 258
    .local v1, "sampleIndexFound":I
    const/4 v2, 0x0

    .line 259
    .local v2, "syncSampleIndexFound":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->length:I

    if-ge v0, v4, :cond_2

    .line 260
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    invoke-virtual {v4, v0}, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->getSamplePresentationTime(I)J

    move-result-wide v4

    iget v6, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    int-to-long v6, v6

    cmp-long v4, v4, v6

    if-gtz v4, :cond_1

    .line 261
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->fragmentRun:Lcom/google/android/exoplayer/parser/mp4/TrackFragment;

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/mp4/TrackFragment;->sampleIsSyncFrameTable:[Z

    aget-boolean v4, v4, v0

    if-eqz v4, :cond_0

    .line 262
    move v2, v0

    .line 264
    :cond_0
    move v1, v0

    .line 259
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 267
    :cond_2
    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->lastSyncSampleIndex:I

    if-ne v2, v4, :cond_3

    iget v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->sampleIndex:I

    if-lt v1, v4, :cond_3

    .line 268
    iput v3, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->pendingSeekTimeMs:I

    .line 275
    .end local v0    # "i":I
    .end local v1    # "sampleIndexFound":I
    .end local v2    # "syncSampleIndexFound":I
    :goto_1
    return v3

    .line 272
    :cond_3
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtoms:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->clear()V

    .line 273
    iget-object v4, p0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->containerAtomEndPoints:Ljava/util/Stack;

    invoke-virtual {v4}, Ljava/util/Stack;->clear()V

    .line 274
    invoke-direct {p0, v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->enterState(I)V

    .line 275
    const/4 v3, 0x1

    goto :goto_1
.end method
