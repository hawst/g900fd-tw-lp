.class interface abstract Lcom/google/android/exoplayer/parser/webm/EbmlReader;
.super Ljava/lang/Object;
.source "EbmlReader.java"


# virtual methods
.method public abstract getBytesRead()J
.end method

.method public abstract read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation
.end method

.method public abstract readBytes(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Ljava/nio/ByteBuffer;I)V
.end method

.method public abstract readBytes(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;[BI)V
.end method

.method public abstract readVarint(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;)J
.end method

.method public abstract reset()V
.end method

.method public abstract setEventHandler(Lcom/google/android/exoplayer/parser/webm/EbmlEventHandler;)V
.end method

.method public abstract skipBytes(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;I)V
.end method
