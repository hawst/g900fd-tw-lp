.class final Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;
.super Ljava/lang/Object;
.source "ParsableByteArray.java"


# instance fields
.field public data:[B

.field private position:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "length"    # I

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    new-array v0, p1, [B

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    .line 32
    return-void
.end method

.method private static shiftIntoInt([BII)I
    .locals 3
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 134
    aget-byte v2, p0, p1

    and-int/lit16 v1, v2, 0xff

    .line 135
    .local v1, "result":I
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_0
    add-int v2, p1, p2

    if-ge v0, v2, :cond_0

    .line 136
    shl-int/lit8 v1, v1, 0x8

    .line 137
    aget-byte v2, p0, v0

    and-int/lit16 v2, v2, 0xff

    or-int/2addr v1, v2

    .line 135
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 139
    :cond_0
    return v1
.end method

.method private static shiftIntoLong([BII)J
    .locals 6
    .param p0, "bytes"    # [B
    .param p1, "offset"    # I
    .param p2, "length"    # I

    .prologue
    .line 143
    aget-byte v1, p0, p1

    and-int/lit16 v1, v1, 0xff

    int-to-long v2, v1

    .line 144
    .local v2, "result":J
    add-int/lit8 v0, p1, 0x1

    .local v0, "i":I
    :goto_0
    add-int v1, p1, p2

    if-ge v0, v1, :cond_0

    .line 145
    const/16 v1, 0x8

    shl-long/2addr v2, v1

    .line 146
    aget-byte v1, p0, v0

    and-int/lit16 v1, v1, 0xff

    int-to-long v4, v1

    or-long/2addr v2, v4

    .line 144
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 148
    :cond_0
    return-wide v2
.end method


# virtual methods
.method public getPosition()I
    .locals 1

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    return v0
.end method

.method public length()I
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    array-length v0, v0

    return v0
.end method

.method public readBytes([BII)V
    .locals 2
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    .line 55
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    invoke-static {v0, v1, p1, p2, p3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 56
    iget v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/2addr v0, p3

    iput v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 57
    return-void
.end method

.method public readInt()I
    .locals 4

    .prologue
    .line 83
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoInt([BII)I

    move-result v0

    .line 84
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 85
    return v0
.end method

.method public readLong()J
    .locals 5

    .prologue
    .line 89
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/16 v4, 0x8

    invoke-static {v2, v3, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoLong([BII)J

    move-result-wide v0

    .line 90
    .local v0, "result":J
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 91
    return-wide v0
.end method

.method public readUnsignedByte()I
    .locals 4

    .prologue
    .line 65
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v3, 0x1

    invoke-static {v1, v2, v3}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoInt([BII)I

    move-result v0

    .line 66
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v1, v1, 0x1

    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 67
    return v0
.end method

.method public readUnsignedFixedPoint1616()I
    .locals 4

    .prologue
    .line 98
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoInt([BII)I

    move-result v0

    .line 99
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 100
    return v0
.end method

.method public readUnsignedInt()J
    .locals 5

    .prologue
    .line 77
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v4, 0x4

    invoke-static {v2, v3, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoLong([BII)J

    move-result-wide v0

    .line 78
    .local v0, "result":J
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v2, v2, 0x4

    iput v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 79
    return-wide v0
.end method

.method public readUnsignedIntToInt()I
    .locals 4

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v3, 0x4

    invoke-static {v1, v2, v3}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoInt([BII)I

    move-result v0

    .line 111
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v1, v1, 0x4

    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 112
    if-gez v0, :cond_0

    .line 113
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Top bit not zero: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 115
    :cond_0
    return v0
.end method

.method public readUnsignedLongToLong()J
    .locals 5

    .prologue
    .line 125
    iget-object v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v3, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/16 v4, 0x8

    invoke-static {v2, v3, v4}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoLong([BII)J

    move-result-wide v0

    .line 126
    .local v0, "result":J
    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v2, v2, 0x8

    iput v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 127
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 128
    new-instance v2, Ljava/lang/IllegalArgumentException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Top bit not zero: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 130
    :cond_0
    return-wide v0
.end method

.method public readUnsignedShort()I
    .locals 4

    .prologue
    .line 71
    iget-object v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->data:[B

    iget v2, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->shiftIntoInt([BII)I

    move-result v0

    .line 72
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 73
    return v0
.end method

.method public setPosition(I)V
    .locals 0
    .param p1, "position"    # I

    .prologue
    .line 43
    iput p1, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 44
    return-void
.end method

.method public skip(I)V
    .locals 1
    .param p1, "bytes"    # I

    .prologue
    .line 47
    iget v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    add-int/2addr v0, p1

    iput v0, p0, Lcom/google/android/exoplayer/parser/mp4/ParsableByteArray;->position:I

    .line 48
    return-void
.end method
