.class public Lcom/google/android/exoplayer/chunk/Format;
.super Ljava/lang/Object;
.source "Format.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer/chunk/Format$DecreasingBandwidthComparator;
    }
.end annotation


# instance fields
.field public final audioSamplingRate:I

.field public final bandwidth:I
    .annotation runtime Ljava/lang/Deprecated;
    .end annotation
.end field

.field public final bitrate:I

.field public final height:I

.field public final id:Ljava/lang/String;

.field public final language:Ljava/lang/String;

.field public final mimeType:Ljava/lang/String;

.field public final numChannels:I

.field public final width:I


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIII)V
    .locals 9
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "numChannels"    # I
    .param p6, "audioSamplingRate"    # I
    .param p7, "bitrate"    # I

    .prologue
    .line 101
    const/4 v8, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v4, p4

    move v5, p5

    move v6, p6

    move/from16 v7, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/exoplayer/chunk/Format;-><init>(Ljava/lang/String;Ljava/lang/String;IIIIILjava/lang/String;)V

    .line 102
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;IIIIILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "width"    # I
    .param p4, "height"    # I
    .param p5, "numChannels"    # I
    .param p6, "audioSamplingRate"    # I
    .param p7, "bitrate"    # I
    .param p8, "language"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    invoke-static {p1}, Lcom/google/android/exoplayer/util/Assertions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/google/android/exoplayer/chunk/Format;->mimeType:Ljava/lang/String;

    .line 118
    iput p3, p0, Lcom/google/android/exoplayer/chunk/Format;->width:I

    .line 119
    iput p4, p0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    .line 120
    iput p5, p0, Lcom/google/android/exoplayer/chunk/Format;->numChannels:I

    .line 121
    iput p6, p0, Lcom/google/android/exoplayer/chunk/Format;->audioSamplingRate:I

    .line 122
    iput p7, p0, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    .line 123
    iput-object p8, p0, Lcom/google/android/exoplayer/chunk/Format;->language:Ljava/lang/String;

    .line 124
    div-int/lit8 v0, p7, 0x8

    iput v0, p0, Lcom/google/android/exoplayer/chunk/Format;->bandwidth:I

    .line 125
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 3
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    .line 137
    if-ne p0, p1, :cond_0

    .line 138
    const/4 v1, 0x1

    .line 144
    :goto_0
    return v1

    .line 140
    :cond_0
    if-eqz p1, :cond_1

    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v1

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v2

    if-eq v1, v2, :cond_2

    .line 141
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 143
    check-cast v0, Lcom/google/android/exoplayer/chunk/Format;

    .line 144
    .local v0, "other":Lcom/google/android/exoplayer/chunk/Format;
    iget-object v1, v0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 1

    .prologue
    .line 129
    iget-object v0, p0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    return v0
.end method
