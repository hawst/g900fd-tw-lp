.class public final Lcom/google/android/exoplayer/audio/AudioTrack;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;,
        Lcom/google/android/exoplayer/audio/AudioTrack$NoopAudioTimestampCompat;,
        Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;,
        Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;
    }
.end annotation


# instance fields
.field private final audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

.field private audioTimestampSet:Z

.field private audioTrack:Landroid/media/AudioTrack;

.field private bufferSize:I

.field private channelConfig:I

.field private encoding:I

.field private frameSize:I

.field private getLatencyMethod:Ljava/lang/reflect/Method;

.field private lastPlayheadSampleTimeUs:J

.field private lastRawPlaybackHeadPosition:J

.field private lastTimestampSampleTimeUs:J

.field private latencyUs:J

.field private final minBufferMultiplicationFactor:F

.field private minBufferSize:I

.field private nextPlayheadOffsetIndex:I

.field private playheadOffsetCount:I

.field private final playheadOffsets:[J

.field private rawPlaybackHeadWrapCount:J

.field private final releasingConditionVariable:Landroid/os/ConditionVariable;

.field private resumeSystemTimeUs:J

.field private sampleRate:I

.field private smoothedPlayheadOffsetUs:J

.field private startMediaTimeState:I

.field private startMediaTimeUs:J

.field private submittedBytes:J

.field private temporaryBuffer:[B

.field private temporaryBufferOffset:I

.field private temporaryBufferSize:I

.field private volume:F


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 144
    const/high16 v0, 0x40800000    # 4.0f

    invoke-direct {p0, v0}, Lcom/google/android/exoplayer/audio/AudioTrack;-><init>(F)V

    .line 145
    return-void
.end method

.method public constructor <init>(F)V
    .locals 5
    .param p1, "minBufferMultiplicationFactor"    # F

    .prologue
    const/4 v3, 0x0

    const/4 v1, 0x1

    const/4 v2, 0x0

    const/high16 v4, 0x3f800000    # 1.0f

    .line 148
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 149
    cmpl-float v0, p1, v4

    if-ltz v0, :cond_1

    move v0, v1

    :goto_0
    invoke-static {v0}, Lcom/google/android/exoplayer/util/Assertions;->checkArgument(Z)V

    .line 150
    iput p1, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->minBufferMultiplicationFactor:F

    .line 151
    new-instance v0, Landroid/os/ConditionVariable;

    invoke-direct {v0, v1}, Landroid/os/ConditionVariable;-><init>(Z)V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->releasingConditionVariable:Landroid/os/ConditionVariable;

    .line 152
    sget v0, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_2

    .line 153
    new-instance v0, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;

    invoke-direct {v0}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompatV19;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    .line 157
    :goto_1
    sget v0, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v1, 0x12

    if-lt v0, v1, :cond_0

    .line 159
    :try_start_0
    const-class v1, Landroid/media/AudioTrack;

    const-string v3, "getLatency"

    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/Class;

    invoke-virtual {v1, v3, v0}, Ljava/lang/Class;->getMethod(Ljava/lang/String;[Ljava/lang/Class;)Ljava/lang/reflect/Method;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->getLatencyMethod:Ljava/lang/reflect/Method;
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    .line 165
    :cond_0
    :goto_2
    const/16 v0, 0xa

    new-array v0, v0, [J

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsets:[J

    .line 166
    iput v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->volume:F

    .line 167
    iput v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    .line 168
    return-void

    :cond_1
    move v0, v2

    .line 149
    goto :goto_0

    .line 155
    :cond_2
    new-instance v0, Lcom/google/android/exoplayer/audio/AudioTrack$NoopAudioTimestampCompat;

    invoke-direct {v0, v3}, Lcom/google/android/exoplayer/audio/AudioTrack$NoopAudioTimestampCompat;-><init>(Lcom/google/android/exoplayer/audio/AudioTrack$1;)V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    goto :goto_1

    .line 161
    :catch_0
    move-exception v0

    goto :goto_2
.end method

.method static synthetic access$100(Lcom/google/android/exoplayer/audio/AudioTrack;)Landroid/os/ConditionVariable;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer/audio/AudioTrack;

    .prologue
    .line 48
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->releasingConditionVariable:Landroid/os/ConditionVariable;

    return-object v0
.end method

.method private bytesToFrames(J)J
    .locals 3
    .param p1, "byteCount"    # J

    .prologue
    .line 623
    iget v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->frameSize:I

    int-to-long v0, v0

    div-long v0, p1, v0

    return-wide v0
.end method

.method private checkAudioTrackInitialized()V
    .locals 5
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;
        }
    .end annotation

    .prologue
    const/4 v2, 0x0

    .line 578
    iget-object v1, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->getState()I

    move-result v0

    .line 579
    .local v0, "state":I
    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 580
    return-void

    .line 584
    :cond_0
    :try_start_0
    iget-object v1, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v1}, Landroid/media/AudioTrack;->release()V
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 589
    iput-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    .line 592
    :goto_0
    new-instance v1, Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;

    iget v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->channelConfig:I

    iget v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    invoke-direct {v1, v0, v2, v3, v4}, Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;-><init>(IIII)V

    throw v1

    .line 585
    :catch_0
    move-exception v1

    .line 589
    iput-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    goto :goto_0

    :catchall_0
    move-exception v1

    iput-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    throw v1
.end method

.method private durationUsToFrames(J)J
    .locals 5
    .param p1, "durationUs"    # J

    .prologue
    .line 631
    iget v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    const-wide/32 v2, 0xf4240

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private framesToBytes(J)J
    .locals 3
    .param p1, "frameCount"    # J

    .prologue
    .line 619
    iget v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->frameSize:I

    int-to-long v0, v0

    mul-long/2addr v0, p1

    return-wide v0
.end method

.method private framesToDurationUs(J)J
    .locals 5
    .param p1, "frameCount"    # J

    .prologue
    .line 627
    const-wide/32 v0, 0xf4240

    mul-long/2addr v0, p1

    iget v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    int-to-long v2, v2

    div-long/2addr v0, v2

    return-wide v0
.end method

.method private getPlaybackPositionFrames()J
    .locals 6

    .prologue
    .line 605
    const-wide v2, 0xffffffffL

    iget-object v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v4}, Landroid/media/AudioTrack;->getPlaybackHeadPosition()I

    move-result v4

    int-to-long v4, v4

    and-long v0, v2, v4

    .line 606
    .local v0, "rawPlaybackHeadPosition":J
    iget-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastRawPlaybackHeadPosition:J

    cmp-long v2, v2, v0

    if-lez v2, :cond_0

    .line 608
    iget-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->rawPlaybackHeadWrapCount:J

    const-wide/16 v4, 0x1

    add-long/2addr v2, v4

    iput-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->rawPlaybackHeadWrapCount:J

    .line 610
    :cond_0
    iput-wide v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastRawPlaybackHeadPosition:J

    .line 611
    iget-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->rawPlaybackHeadWrapCount:J

    const/16 v4, 0x20

    shl-long/2addr v2, v4

    add-long/2addr v2, v0

    return-wide v2
.end method

.method private getPlaybackPositionUs()J
    .locals 2

    .prologue
    .line 615
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->getPlaybackPositionFrames()J

    move-result-wide v0

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToDurationUs(J)J

    move-result-wide v0

    return-wide v0
.end method

.method private hasCurrentPositionUs()Z
    .locals 4

    .prologue
    .line 508
    invoke-virtual {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private maybeSampleSyncParams()V
    .locals 14

    .prologue
    .line 513
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->getPlaybackPositionUs()J

    move-result-wide v4

    .line 514
    .local v4, "playbackPositionUs":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-nez v8, :cond_1

    .line 568
    :cond_0
    :goto_0
    return-void

    .line 518
    :cond_1
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v6, v8, v10

    .line 519
    .local v6, "systemClockUs":J
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastPlayheadSampleTimeUs:J

    sub-long v8, v6, v8

    const-wide/16 v10, 0x7530

    cmp-long v8, v8, v10

    if-ltz v8, :cond_3

    .line 521
    iget-object v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsets:[J

    iget v9, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->nextPlayheadOffsetIndex:I

    sub-long v10, v4, v6

    aput-wide v10, v8, v9

    .line 522
    iget v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->nextPlayheadOffsetIndex:I

    add-int/lit8 v8, v8, 0x1

    rem-int/lit8 v8, v8, 0xa

    iput v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->nextPlayheadOffsetIndex:I

    .line 523
    iget v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    const/16 v9, 0xa

    if-ge v8, v9, :cond_2

    .line 524
    iget v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    add-int/lit8 v8, v8, 0x1

    iput v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    .line 526
    :cond_2
    iput-wide v6, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastPlayheadSampleTimeUs:J

    .line 527
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->smoothedPlayheadOffsetUs:J

    .line 528
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_1
    iget v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    if-ge v3, v8, :cond_3

    .line 529
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->smoothedPlayheadOffsetUs:J

    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsets:[J

    aget-wide v10, v10, v3

    iget v12, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    int-to-long v12, v12

    div-long/2addr v10, v12

    add-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->smoothedPlayheadOffsetUs:J

    .line 528
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .line 533
    .end local v3    # "i":I
    :cond_3
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastTimestampSampleTimeUs:J

    sub-long v8, v6, v8

    const-wide/32 v10, 0x7a120

    cmp-long v8, v8, v10

    if-ltz v8, :cond_0

    .line 534
    iget-object v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    iget-object v9, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-interface {v8, v9}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;->update(Landroid/media/AudioTrack;)Z

    move-result v8

    iput-boolean v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    .line 535
    iget-boolean v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    if-eqz v8, :cond_4

    .line 537
    iget-object v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    invoke-interface {v8}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;->getNanoTime()J

    move-result-wide v8

    const-wide/16 v10, 0x3e8

    div-long v0, v8, v10

    .line 538
    .local v0, "audioTimestampUs":J
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->resumeSystemTimeUs:J

    cmp-long v8, v0, v8

    if-gez v8, :cond_6

    .line 540
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    .line 548
    .end local v0    # "audioTimestampUs":J
    :cond_4
    :goto_2
    iget-object v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->getLatencyMethod:Ljava/lang/reflect/Method;

    if-eqz v8, :cond_5

    .line 552
    :try_start_0
    iget-object v9, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->getLatencyMethod:Ljava/lang/reflect/Method;

    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    const/4 v8, 0x0

    check-cast v8, [Ljava/lang/Object;

    invoke-virtual {v9, v10, v8}, Ljava/lang/reflect/Method;->invoke(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v8

    check-cast v8, Ljava/lang/Integer;

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v8

    int-to-long v8, v8

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    iget v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    int-to-long v10, v10

    invoke-direct {p0, v10, v11}, Lcom/google/android/exoplayer/audio/AudioTrack;->bytesToFrames(J)J

    move-result-wide v10

    invoke-direct {p0, v10, v11}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToDurationUs(J)J

    move-result-wide v10

    sub-long/2addr v8, v10

    iput-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    .line 555
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    const-wide/16 v10, 0x0

    invoke-static {v8, v9, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    iput-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    .line 557
    iget-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    const-wide/32 v10, 0x989680

    cmp-long v8, v8, v10

    if-lez v8, :cond_5

    .line 558
    const-string v8, "AudioTrack"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Ignoring impossibly large audio latency: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-wide v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 559
    const-wide/16 v8, 0x0

    iput-wide v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 566
    :cond_5
    :goto_3
    iput-wide v6, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastTimestampSampleTimeUs:J

    goto/16 :goto_0

    .line 541
    .restart local v0    # "audioTimestampUs":J
    :cond_6
    sub-long v8, v0, v6

    invoke-static {v8, v9}, Ljava/lang/Math;->abs(J)J

    move-result-wide v8

    const-wide/32 v10, 0x989680

    cmp-long v8, v8, v10

    if-lez v8, :cond_4

    .line 543
    const/4 v8, 0x0

    iput-boolean v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    .line 544
    const-string v8, "AudioTrack"

    new-instance v9, Ljava/lang/StringBuilder;

    invoke-direct {v9}, Ljava/lang/StringBuilder;-><init>()V

    const-string v10, "Spurious audio timestamp: "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    invoke-interface {v10}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;->getFramePosition()J

    move-result-wide v10

    invoke-virtual {v9, v10, v11}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v0, v1}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    const-string v10, ", "

    invoke-virtual {v9, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9, v6, v7}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v9

    invoke-virtual {v9}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v9

    invoke-static {v8, v9}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    goto/16 :goto_2

    .line 561
    .end local v0    # "audioTimestampUs":J
    :catch_0
    move-exception v2

    .line 563
    .local v2, "e":Ljava/lang/Exception;
    const/4 v8, 0x0

    iput-object v8, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->getLatencyMethod:Ljava/lang/reflect/Method;

    goto :goto_3
.end method

.method private resetSyncParams()V
    .locals 4

    .prologue
    const-wide/16 v2, 0x0

    const/4 v0, 0x0

    .line 635
    iput-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->smoothedPlayheadOffsetUs:J

    .line 636
    iput v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    .line 637
    iput v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->nextPlayheadOffsetIndex:I

    .line 638
    iput-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastPlayheadSampleTimeUs:J

    .line 639
    iput-boolean v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    .line 640
    iput-wide v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastTimestampSampleTimeUs:J

    .line 641
    return-void
.end method

.method private static setVolumeV21(Landroid/media/AudioTrack;F)V
    .locals 0
    .param p0, "audioTrack"    # Landroid/media/AudioTrack;
    .param p1, "volume"    # F

    .prologue
    .line 456
    invoke-virtual {p0, p1}, Landroid/media/AudioTrack;->setVolume(F)I

    .line 457
    return-void
.end method

.method private static setVolumeV3(Landroid/media/AudioTrack;F)V
    .locals 0
    .param p0, "audioTrack"    # Landroid/media/AudioTrack;
    .param p1, "volume"    # F

    .prologue
    .line 461
    invoke-virtual {p0, p1, p1}, Landroid/media/AudioTrack;->setStereoVolume(FF)I

    .line 462
    return-void
.end method

.method private static writeNonBlockingV21(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I
    .locals 1
    .param p0, "audioTrack"    # Landroid/media/AudioTrack;
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "size"    # I

    .prologue
    .line 429
    const/4 v0, 0x1

    invoke-virtual {p0, p1, p2, v0}, Landroid/media/AudioTrack;->write(Ljava/nio/ByteBuffer;II)I

    move-result v0

    return v0
.end method


# virtual methods
.method public getCurrentPositionUs(Z)J
    .locals 14
    .param p1, "sourceEnded"    # Z

    .prologue
    const-wide/16 v12, 0x3e8

    .line 190
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->hasCurrentPositionUs()Z

    move-result v10

    if-nez v10, :cond_1

    .line 191
    const-wide/high16 v2, -0x8000000000000000L

    .line 222
    :cond_0
    :goto_0
    return-wide v2

    .line 194
    :cond_1
    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v10}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v10

    const/4 v11, 0x3

    if-ne v10, v11, :cond_2

    .line 195
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->maybeSampleSyncParams()V

    .line 198
    :cond_2
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v10

    div-long v8, v10, v12

    .line 200
    .local v8, "systemClockUs":J
    iget-boolean v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampSet:Z

    if-eqz v10, :cond_3

    .line 202
    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    invoke-interface {v10}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;->getNanoTime()J

    move-result-wide v10

    div-long/2addr v10, v12

    sub-long v6, v8, v10

    .line 203
    .local v6, "presentationDiff":J
    invoke-direct {p0, v6, v7}, Lcom/google/android/exoplayer/audio/AudioTrack;->durationUsToFrames(J)J

    move-result-wide v4

    .line 205
    .local v4, "framesDiff":J
    iget-object v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTimestampCompat:Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;

    invoke-interface {v10}, Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;->getFramePosition()J

    move-result-wide v10

    add-long v0, v10, v4

    .line 206
    .local v0, "currentFramePosition":J
    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToDurationUs(J)J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    add-long v2, v10, v12

    .line 207
    .local v2, "currentPositionUs":J
    goto :goto_0

    .line 208
    .end local v0    # "currentFramePosition":J
    .end local v2    # "currentPositionUs":J
    .end local v4    # "framesDiff":J
    .end local v6    # "presentationDiff":J
    :cond_3
    iget v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->playheadOffsetCount:I

    if-nez v10, :cond_4

    .line 210
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->getPlaybackPositionUs()J

    move-result-wide v10

    iget-wide v12, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    add-long v2, v10, v12

    .line 217
    .restart local v2    # "currentPositionUs":J
    :goto_1
    if-nez p1, :cond_0

    .line 218
    iget-wide v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->latencyUs:J

    sub-long/2addr v2, v10

    goto :goto_0

    .line 215
    .end local v2    # "currentPositionUs":J
    :cond_4
    iget-wide v10, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->smoothedPlayheadOffsetUs:J

    add-long/2addr v10, v8

    iget-wide v12, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    add-long v2, v10, v12

    .restart local v2    # "currentPositionUs":J
    goto :goto_1
.end method

.method public handleBuffer(Ljava/nio/ByteBuffer;IIJ)I
    .locals 16
    .param p1, "buffer"    # Ljava/nio/ByteBuffer;
    .param p2, "offset"    # I
    .param p3, "size"    # I
    .param p4, "presentationTimeUs"    # J

    .prologue
    .line 353
    const/4 v7, 0x0

    .line 355
    .local v7, "result":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    if-nez v11, :cond_0

    if-eqz p3, :cond_0

    .line 358
    move/from16 v0, p3

    int-to-long v12, v0

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/exoplayer/audio/AudioTrack;->bytesToFrames(J)J

    move-result-wide v12

    move-object/from16 v0, p0

    invoke-direct {v0, v12, v13}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToDurationUs(J)J

    move-result-wide v12

    sub-long v2, p4, v12

    .line 359
    .local v2, "bufferStartTime":J
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    const-wide/16 v14, 0x0

    cmp-long v11, v12, v14

    if-nez v11, :cond_1

    .line 360
    const-wide/16 v12, 0x0

    invoke-static {v12, v13, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v12

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    .line 361
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    .line 382
    .end local v2    # "bufferStartTime":J
    :cond_0
    :goto_0
    if-nez p3, :cond_3

    move v10, v7

    .line 423
    .end local v7    # "result":I
    .local v10, "result":I
    :goto_1
    return v10

    .line 364
    .end local v10    # "result":I
    .restart local v2    # "bufferStartTime":J
    .restart local v7    # "result":I
    :cond_1
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    move-object/from16 v0, p0

    iget-wide v14, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/exoplayer/audio/AudioTrack;->bytesToFrames(J)J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToDurationUs(J)J

    move-result-wide v14

    add-long v8, v12, v14

    .line 366
    .local v8, "expectedBufferStartTime":J
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    const/4 v12, 0x1

    if-ne v11, v12, :cond_2

    sub-long v12, v8, v2

    invoke-static {v12, v13}, Ljava/lang/Math;->abs(J)J

    move-result-wide v12

    const-wide/32 v14, 0x30d40

    cmp-long v11, v12, v14

    if-lez v11, :cond_2

    .line 368
    const-string v11, "AudioTrack"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "Discontinuity detected [expected "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v8, v9}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, ", got "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v2, v3}, Ljava/lang/StringBuilder;->append(J)Ljava/lang/StringBuilder;

    move-result-object v12

    const-string v13, "]"

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->e(Ljava/lang/String;Ljava/lang/String;)I

    .line 370
    const/4 v11, 0x2

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    .line 372
    :cond_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    const/4 v12, 0x2

    if-ne v11, v12, :cond_0

    .line 375
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    sub-long v14, v2, v8

    add-long/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    .line 376
    const/4 v11, 0x1

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    .line 377
    const/4 v7, 0x1

    goto :goto_0

    .line 386
    .end local v2    # "bufferStartTime":J
    .end local v8    # "expectedBufferStartTime":J
    :cond_3
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    if-nez v11, :cond_6

    .line 387
    move/from16 v0, p3

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    .line 388
    invoke-virtual/range {p1 .. p2}, Ljava/nio/ByteBuffer;->position(I)Ljava/nio/Buffer;

    .line 389
    sget v11, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v12, 0x15

    if-ge v11, v12, :cond_6

    .line 391
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBuffer:[B

    if-eqz v11, :cond_4

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBuffer:[B

    array-length v11, v11

    move/from16 v0, p3

    if-ge v11, v0, :cond_5

    .line 392
    :cond_4
    move/from16 v0, p3

    new-array v11, v0, [B

    move-object/from16 v0, p0

    iput-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBuffer:[B

    .line 394
    :cond_5
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBuffer:[B

    const/4 v12, 0x0

    move-object/from16 v0, p1

    move/from16 v1, p3

    invoke-virtual {v0, v11, v12, v1}, Ljava/nio/ByteBuffer;->get([BII)Ljava/nio/ByteBuffer;

    .line 395
    const/4 v11, 0x0

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferOffset:I

    .line 399
    :cond_6
    const/4 v6, 0x0

    .line 400
    .local v6, "bytesWritten":I
    sget v11, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v12, 0x15

    if-ge v11, v12, :cond_a

    .line 402
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    invoke-direct/range {p0 .. p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->getPlaybackPositionFrames()J

    move-result-wide v14

    move-object/from16 v0, p0

    invoke-direct {v0, v14, v15}, Lcom/google/android/exoplayer/audio/AudioTrack;->framesToBytes(J)J

    move-result-wide v14

    sub-long/2addr v12, v14

    long-to-int v4, v12

    .line 403
    .local v4, "bytesPending":I
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    sub-int v5, v11, v4

    .line 404
    .local v5, "bytesToWrite":I
    if-lez v5, :cond_7

    .line 405
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    invoke-static {v11, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    .line 406
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBuffer:[B

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferOffset:I

    invoke-virtual {v11, v12, v13, v5}, Landroid/media/AudioTrack;->write([BII)I

    move-result v6

    .line 407
    if-gez v6, :cond_9

    .line 408
    const-string v11, "AudioTrack"

    new-instance v12, Ljava/lang/StringBuilder;

    invoke-direct {v12}, Ljava/lang/StringBuilder;-><init>()V

    const-string v13, "AudioTrack.write returned error code: "

    invoke-virtual {v12, v13}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12, v6}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v12

    invoke-virtual {v12}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v12

    invoke-static {v11, v12}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/String;)I

    .line 417
    .end local v4    # "bytesPending":I
    .end local v5    # "bytesToWrite":I
    :cond_7
    :goto_2
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    sub-int/2addr v11, v6

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    .line 418
    move-object/from16 v0, p0

    iget-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    int-to-long v14, v6

    add-long/2addr v12, v14

    move-object/from16 v0, p0

    iput-wide v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    .line 419
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    if-nez v11, :cond_8

    .line 420
    or-int/lit8 v7, v7, 0x2

    :cond_8
    move v10, v7

    .line 423
    .end local v7    # "result":I
    .restart local v10    # "result":I
    goto/16 :goto_1

    .line 410
    .end local v10    # "result":I
    .restart local v4    # "bytesPending":I
    .restart local v5    # "bytesToWrite":I
    .restart local v7    # "result":I
    :cond_9
    move-object/from16 v0, p0

    iget v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferOffset:I

    add-int/2addr v11, v6

    move-object/from16 v0, p0

    iput v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferOffset:I

    goto :goto_2

    .line 414
    .end local v4    # "bytesPending":I
    .end local v5    # "bytesToWrite":I
    :cond_a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    move-object/from16 v0, p0

    iget v12, v0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    move-object/from16 v0, p1

    invoke-static {v11, v0, v12}, Lcom/google/android/exoplayer/audio/AudioTrack;->writeNonBlockingV21(Landroid/media/AudioTrack;Ljava/nio/ByteBuffer;I)I

    move-result v6

    goto :goto_2
.end method

.method public handleDiscontinuity()V
    .locals 2

    .prologue
    .line 333
    iget v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 334
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeState:I

    .line 336
    :cond_0
    return-void
.end method

.method public hasEnoughDataToBeginPlayback()Z
    .locals 4

    .prologue
    .line 439
    iget-wide v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    iget v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->minBufferSize:I

    int-to-long v2, v2

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hasPendingData()Z
    .locals 4

    .prologue
    .line 434
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    iget-wide v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    invoke-direct {p0, v0, v1}, Lcom/google/android/exoplayer/audio/AudioTrack;->bytesToFrames(J)J

    move-result-wide v0

    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->getPlaybackPositionFrames()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public initialize()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;
        }
    .end annotation

    .prologue
    .line 231
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/audio/AudioTrack;->initialize(I)I

    move-result v0

    return v0
.end method

.method public initialize(I)I
    .locals 8
    .param p1, "sessionId"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x3

    const/4 v6, 0x1

    .line 247
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->releasingConditionVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v0}, Landroid/os/ConditionVariable;->block()V

    .line 249
    if-nez p1, :cond_0

    .line 250
    new-instance v0, Landroid/media/AudioTrack;

    iget v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->channelConfig:I

    iget v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->encoding:I

    iget v5, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    invoke-direct/range {v0 .. v6}, Landroid/media/AudioTrack;-><init>(IIIIII)V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    .line 257
    :goto_0
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->checkAudioTrackInitialized()V

    .line 258
    iget v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->volume:F

    invoke-virtual {p0, v0}, Lcom/google/android/exoplayer/audio/AudioTrack;->setVolume(F)V

    .line 259
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->getAudioSessionId()I

    move-result v0

    return v0

    .line 254
    :cond_0
    new-instance v0, Landroid/media/AudioTrack;

    iget v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->channelConfig:I

    iget v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->encoding:I

    iget v5, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    move v7, p1

    invoke-direct/range {v0 .. v7}, Landroid/media/AudioTrack;-><init>(IIIIIII)V

    iput-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    goto :goto_0
.end method

.method public isInitialized()Z
    .locals 1

    .prologue
    .line 175
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 466
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    .line 467
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->resetSyncParams()V

    .line 468
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->pause()V

    .line 470
    :cond_0
    return-void
.end method

.method public play()V
    .locals 4

    .prologue
    .line 324
    invoke-virtual {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->isInitialized()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 325
    invoke-static {}, Ljava/lang/System;->nanoTime()J

    move-result-wide v0

    const-wide/16 v2, 0x3e8

    div-long/2addr v0, v2

    iput-wide v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->resumeSystemTimeUs:J

    .line 326
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v0}, Landroid/media/AudioTrack;->play()V

    .line 328
    :cond_0
    return-void
.end method

.method public reconfigure(Landroid/media/MediaFormat;)V
    .locals 2
    .param p1, "format"    # Landroid/media/MediaFormat;

    .prologue
    .line 267
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-virtual {p0, p1, v0, v1}, Lcom/google/android/exoplayer/audio/AudioTrack;->reconfigure(Landroid/media/MediaFormat;II)V

    .line 268
    return-void
.end method

.method public reconfigure(Landroid/media/MediaFormat;II)V
    .locals 6
    .param p1, "format"    # Landroid/media/MediaFormat;
    .param p2, "encoding"    # I
    .param p3, "bufferSize"    # I

    .prologue
    .line 281
    const-string v3, "channel-count"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v1

    .line 283
    .local v1, "channelCount":I
    packed-switch v1, :pswitch_data_0

    .line 297
    :pswitch_0
    new-instance v3, Ljava/lang/IllegalArgumentException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Unsupported channel count: "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v3

    .line 285
    :pswitch_1
    const/4 v0, 0x4

    .line 300
    .local v0, "channelConfig":I
    :goto_0
    const-string v3, "sample-rate"

    invoke-virtual {p1, v3}, Landroid/media/MediaFormat;->getInteger(Ljava/lang/String;)I

    move-result v2

    .line 303
    .local v2, "sampleRate":I
    iget-object v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v3, :cond_0

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    if-ne v3, v2, :cond_0

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->channelConfig:I

    if-ne v3, v0, :cond_0

    .line 320
    .end local p3    # "bufferSize":I
    :goto_1
    return-void

    .line 288
    .end local v0    # "channelConfig":I
    .end local v2    # "sampleRate":I
    .restart local p3    # "bufferSize":I
    :pswitch_2
    const/16 v0, 0xc

    .line 289
    .restart local v0    # "channelConfig":I
    goto :goto_0

    .line 291
    .end local v0    # "channelConfig":I
    :pswitch_3
    const/16 v0, 0xfc

    .line 292
    .restart local v0    # "channelConfig":I
    goto :goto_0

    .line 294
    .end local v0    # "channelConfig":I
    :pswitch_4
    const/16 v0, 0x3fc

    .line 295
    .restart local v0    # "channelConfig":I
    goto :goto_0

    .line 309
    .restart local v2    # "sampleRate":I
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->reset()V

    .line 311
    invoke-static {v2, v0, p2}, Landroid/media/AudioTrack;->getMinBufferSize(III)I

    move-result v3

    iput v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->minBufferSize:I

    .line 313
    iput p2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->encoding:I

    .line 314
    if-nez p3, :cond_1

    iget v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->minBufferMultiplicationFactor:F

    iget v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->minBufferSize:I

    int-to-float v4, v4

    mul-float/2addr v3, v4

    float-to-int p3, v3

    .end local p3    # "bufferSize":I
    :cond_1
    iput p3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->bufferSize:I

    .line 316
    iput v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->sampleRate:I

    .line 317
    iput v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->channelConfig:I

    .line 319
    mul-int/lit8 v3, v1, 0x2

    iput v3, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->frameSize:I

    goto :goto_1

    .line 283
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_3
        :pswitch_0
        :pswitch_4
    .end packed-switch
.end method

.method public reset()V
    .locals 6

    .prologue
    const-wide/16 v4, 0x0

    .line 478
    iget-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v2, :cond_1

    .line 479
    iput-wide v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->submittedBytes:J

    .line 480
    const/4 v2, 0x0

    iput v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->temporaryBufferSize:I

    .line 481
    iput-wide v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->lastRawPlaybackHeadPosition:J

    .line 482
    iput-wide v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->rawPlaybackHeadWrapCount:J

    .line 483
    iput-wide v4, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->startMediaTimeUs:J

    .line 484
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack;->resetSyncParams()V

    .line 485
    iget-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->getPlayState()I

    move-result v0

    .line 486
    .local v0, "playState":I
    const/4 v2, 0x3

    if-ne v0, v2, :cond_0

    .line 487
    iget-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-virtual {v2}, Landroid/media/AudioTrack;->pause()V

    .line 490
    :cond_0
    iget-object v1, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    .line 491
    .local v1, "toRelease":Landroid/media/AudioTrack;
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    .line 492
    iget-object v2, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->releasingConditionVariable:Landroid/os/ConditionVariable;

    invoke-virtual {v2}, Landroid/os/ConditionVariable;->close()V

    .line 493
    new-instance v2, Lcom/google/android/exoplayer/audio/AudioTrack$1;

    invoke-direct {v2, p0, v1}, Lcom/google/android/exoplayer/audio/AudioTrack$1;-><init>(Lcom/google/android/exoplayer/audio/AudioTrack;Landroid/media/AudioTrack;)V

    invoke-virtual {v2}, Lcom/google/android/exoplayer/audio/AudioTrack$1;->start()V

    .line 504
    .end local v0    # "playState":I
    .end local v1    # "toRelease":Landroid/media/AudioTrack;
    :cond_1
    return-void
.end method

.method public setVolume(F)V
    .locals 2
    .param p1, "volume"    # F

    .prologue
    .line 444
    iput p1, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->volume:F

    .line 445
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    if-eqz v0, :cond_0

    .line 446
    sget v0, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_1

    .line 447
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer/audio/AudioTrack;->setVolumeV21(Landroid/media/AudioTrack;F)V

    .line 452
    :cond_0
    :goto_0
    return-void

    .line 449
    :cond_1
    iget-object v0, p0, Lcom/google/android/exoplayer/audio/AudioTrack;->audioTrack:Landroid/media/AudioTrack;

    invoke-static {v0, p1}, Lcom/google/android/exoplayer/audio/AudioTrack;->setVolumeV3(Landroid/media/AudioTrack;F)V

    goto :goto_0
.end method
