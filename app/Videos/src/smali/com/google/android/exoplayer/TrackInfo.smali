.class public final Lcom/google/android/exoplayer/TrackInfo;
.super Ljava/lang/Object;
.source "TrackInfo.java"


# instance fields
.field public final durationUs:J

.field public final mimeType:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;J)V
    .locals 0
    .param p1, "mimeType"    # Ljava/lang/String;
    .param p2, "durationUs"    # J

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p1, p0, Lcom/google/android/exoplayer/TrackInfo;->mimeType:Ljava/lang/String;

    .line 28
    iput-wide p2, p0, Lcom/google/android/exoplayer/TrackInfo;->durationUs:J

    .line 29
    return-void
.end method
