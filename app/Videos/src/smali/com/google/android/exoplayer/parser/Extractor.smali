.class public interface abstract Lcom/google/android/exoplayer/parser/Extractor;
.super Ljava/lang/Object;
.source "Extractor.java"


# virtual methods
.method public abstract getFormat()Lcom/google/android/exoplayer/MediaFormat;
.end method

.method public abstract getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;
.end method

.method public abstract getPsshInfo()Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;"
        }
    .end annotation
.end method

.method public abstract hasRelativeIndexOffsets()Z
.end method

.method public abstract read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation
.end method

.method public abstract seekTo(JZ)Z
.end method
