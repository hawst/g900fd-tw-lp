.class public interface abstract Lcom/google/android/exoplayer/SampleSource;
.super Ljava/lang/Object;
.source "SampleSource.java"


# virtual methods
.method public abstract continueBuffering(J)Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract disable(I)V
.end method

.method public abstract enable(IJ)V
.end method

.method public abstract getBufferedPositionUs()J
.end method

.method public abstract getTrackCount()I
.end method

.method public abstract getTrackInfo(I)Lcom/google/android/exoplayer/TrackInfo;
.end method

.method public abstract prepare()Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract readData(IJLcom/google/android/exoplayer/MediaFormatHolder;Lcom/google/android/exoplayer/SampleHolder;Z)I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation
.end method

.method public abstract release()V
.end method

.method public abstract seekToUs(J)V
.end method
