.class public final Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;
.super Lcom/google/android/exoplayer/chunk/MediaChunk;
.source "Mp4MediaChunk.java"


# instance fields
.field private final extractor:Lcom/google/android/exoplayer/parser/Extractor;

.field private final maybeSelfContained:Z

.field private mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

.field private prepared:Z

.field private psshInfo:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;"
        }
    .end annotation
.end field

.field private final sampleOffsetUs:J


# direct methods
.method public constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;IJJILcom/google/android/exoplayer/parser/Extractor;ZJ)V
    .locals 1
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "dataSpec"    # Lcom/google/android/exoplayer/upstream/DataSpec;
    .param p3, "format"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p4, "trigger"    # I
    .param p5, "startTimeUs"    # J
    .param p7, "endTimeUs"    # J
    .param p9, "nextChunkIndex"    # I
    .param p10, "extractor"    # Lcom/google/android/exoplayer/parser/Extractor;
    .param p11, "maybeSelfContained"    # Z
    .param p12, "sampleOffsetUs"    # J

    .prologue
    .line 61
    invoke-direct/range {p0 .. p9}, Lcom/google/android/exoplayer/chunk/MediaChunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;IJJI)V

    .line 62
    iput-object p10, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    .line 63
    iput-boolean p11, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->maybeSelfContained:Z

    .line 64
    iput-wide p12, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->sampleOffsetUs:J

    .line 65
    return-void
.end method


# virtual methods
.method public getMediaFormat()Lcom/google/android/exoplayer/MediaFormat;
    .locals 1

    .prologue
    .line 127
    iget-object v0, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

    return-object v0
.end method

.method public getPsshInfo()Ljava/util/Map;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;"
        }
    .end annotation

    .prologue
    .line 132
    iget-object v0, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->psshInfo:Ljava/util/Map;

    return-object v0
.end method

.method public prepare()Z
    .locals 6
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 85
    iget-boolean v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->prepared:Z

    if-nez v2, :cond_0

    .line 86
    iget-boolean v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->maybeSelfContained:Z

    if-eqz v2, :cond_3

    .line 89
    invoke-virtual {p0}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->getNonBlockingInputStream()Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    move-result-object v0

    .line 90
    .local v0, "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    if-eqz v0, :cond_1

    move v2, v3

    :goto_0
    invoke-static {v2}, Lcom/google/android/exoplayer/util/Assertions;->checkState(Z)V

    .line 91
    iget-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    const/4 v5, 0x0

    invoke-interface {v2, v0, v5}, Lcom/google/android/exoplayer/parser/Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v1

    .line 92
    .local v1, "result":I
    and-int/lit8 v2, v1, 0x20

    if-eqz v2, :cond_2

    :goto_1
    iput-boolean v3, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->prepared:Z

    .line 98
    .end local v0    # "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .end local v1    # "result":I
    :goto_2
    iget-boolean v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->prepared:Z

    if-eqz v2, :cond_0

    .line 99
    iget-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    invoke-interface {v2}, Lcom/google/android/exoplayer/parser/Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->mediaFormat:Lcom/google/android/exoplayer/MediaFormat;

    .line 100
    iget-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    invoke-interface {v2}, Lcom/google/android/exoplayer/parser/Extractor;->getPsshInfo()Ljava/util/Map;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->psshInfo:Ljava/util/Map;

    .line 103
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->prepared:Z

    return v2

    .restart local v0    # "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    :cond_1
    move v2, v4

    .line 90
    goto :goto_0

    .restart local v1    # "result":I
    :cond_2
    move v3, v4

    .line 92
    goto :goto_1

    .line 96
    .end local v0    # "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .end local v1    # "result":I
    :cond_3
    iput-boolean v3, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->prepared:Z

    goto :goto_2
.end method

.method public read(Lcom/google/android/exoplayer/SampleHolder;)Z
    .locals 8
    .param p1, "holder"    # Lcom/google/android/exoplayer/SampleHolder;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 115
    invoke-virtual {p0}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->getNonBlockingInputStream()Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    move-result-object v0

    .line 116
    .local v0, "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    if-eqz v0, :cond_1

    move v3, v4

    :goto_0
    invoke-static {v3}, Lcom/google/android/exoplayer/util/Assertions;->checkState(Z)V

    .line 117
    iget-object v3, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    invoke-interface {v3, v0, p1}, Lcom/google/android/exoplayer/parser/Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v1

    .line 118
    .local v1, "result":I
    and-int/lit8 v3, v1, 0x4

    if-eqz v3, :cond_2

    move v2, v4

    .line 119
    .local v2, "sampleRead":Z
    :goto_1
    if-eqz v2, :cond_0

    .line 120
    iget-wide v4, p1, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    iget-wide v6, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->sampleOffsetUs:J

    sub-long/2addr v4, v6

    iput-wide v4, p1, Lcom/google/android/exoplayer/SampleHolder;->timeUs:J

    .line 122
    :cond_0
    return v2

    .end local v1    # "result":I
    .end local v2    # "sampleRead":Z
    :cond_1
    move v3, v5

    .line 116
    goto :goto_0

    .restart local v1    # "result":I
    :cond_2
    move v2, v5

    .line 118
    goto :goto_1
.end method

.method public sampleAvailable()Z
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 108
    invoke-virtual {p0}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->getNonBlockingInputStream()Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;

    move-result-object v0

    .line 109
    .local v0, "inputStream":Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    iget-object v2, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    const/4 v3, 0x0

    invoke-interface {v2, v0, v3}, Lcom/google/android/exoplayer/parser/Extractor;->read(Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;Lcom/google/android/exoplayer/SampleHolder;)I

    move-result v1

    .line 110
    .local v1, "result":I
    and-int/lit8 v2, v1, 0x20

    if-eqz v2, :cond_0

    const/4 v2, 0x1

    :goto_0
    return v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public seekTo(JZ)Z
    .locals 7
    .param p1, "positionUs"    # J
    .param p3, "allowNoop"    # Z

    .prologue
    .line 75
    iget-wide v4, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->sampleOffsetUs:J

    add-long v2, p1, v4

    .line 76
    .local v2, "seekTimeUs":J
    iget-object v1, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    invoke-interface {v1, v2, v3, p3}, Lcom/google/android/exoplayer/parser/Extractor;->seekTo(JZ)Z

    move-result v0

    .line 77
    .local v0, "isDiscontinuous":Z
    if-eqz v0, :cond_0

    .line 78
    invoke-virtual {p0}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->resetReadPosition()V

    .line 80
    :cond_0
    return v0
.end method

.method public seekToStart()V
    .locals 4

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->extractor:Lcom/google/android/exoplayer/parser/Extractor;

    const-wide/16 v2, 0x0

    const/4 v1, 0x0

    invoke-interface {v0, v2, v3, v1}, Lcom/google/android/exoplayer/parser/Extractor;->seekTo(JZ)Z

    .line 70
    invoke-virtual {p0}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;->resetReadPosition()V

    .line 71
    return-void
.end method
