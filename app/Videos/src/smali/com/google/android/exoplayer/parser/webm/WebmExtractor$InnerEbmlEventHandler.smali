.class final Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;
.super Ljava/lang/Object;
.source "WebmExtractor.java"

# interfaces
.implements Lcom/google/android/exoplayer/parser/webm/EbmlEventHandler;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/parser/webm/WebmExtractor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "InnerEbmlEventHandler"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;


# direct methods
.method private constructor <init>(Lcom/google/android/exoplayer/parser/webm/WebmExtractor;)V
    .locals 0

    .prologue
    .line 550
    iput-object p1, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer/parser/webm/WebmExtractor;Lcom/google/android/exoplayer/parser/webm/WebmExtractor$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/exoplayer/parser/webm/WebmExtractor;
    .param p2, "x1"    # Lcom/google/android/exoplayer/parser/webm/WebmExtractor$1;

    .prologue
    .line 550
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;-><init>(Lcom/google/android/exoplayer/parser/webm/WebmExtractor;)V

    return-void
.end method


# virtual methods
.method public getElementType(I)I
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 554
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->getElementType(I)I

    move-result v0

    return v0
.end method

.method public onBinaryElement(IJIILcom/google/android/exoplayer/upstream/NonBlockingInputStream;)Z
    .locals 8
    .param p1, "id"    # I
    .param p2, "elementOffsetBytes"    # J
    .param p4, "headerSizeBytes"    # I
    .param p5, "contentsSizeBytes"    # I
    .param p6, "inputStream"    # Lcom/google/android/exoplayer/upstream/NonBlockingInputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move v5, p5

    move-object v6, p6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onBinaryElement(IJIILcom/google/android/exoplayer/upstream/NonBlockingInputStream;)Z

    move-result v0

    return v0
.end method

.method public onFloatElement(ID)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "value"    # D

    .prologue
    .line 577
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onFloatElement(ID)Z

    .line 578
    return-void
.end method

.method public onIntegerElement(IJ)V
    .locals 2
    .param p1, "id"    # I
    .param p2, "value"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 572
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onIntegerElement(IJ)Z

    .line 573
    return-void
.end method

.method public onMasterElementEnd(I)V
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 567
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-virtual {v0, p1}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onMasterElementEnd(I)Z

    .line 568
    return-void
.end method

.method public onMasterElementStart(IJIJ)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "elementOffsetBytes"    # J
    .param p4, "headerSizeBytes"    # I
    .param p5, "contentsSizeBytes"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    move v1, p1

    move-wide v2, p2

    move v4, p4

    move-wide v5, p5

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onMasterElementStart(IJIJ)Z

    .line 563
    return-void
.end method

.method public onStringElement(ILjava/lang/String;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "value"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ParserException;
        }
    .end annotation

    .prologue
    .line 582
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor$InnerEbmlEventHandler;->this$0:Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;->onStringElement(ILjava/lang/String;)Z

    .line 583
    return-void
.end method
