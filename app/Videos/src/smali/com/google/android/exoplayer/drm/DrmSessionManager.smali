.class public interface abstract Lcom/google/android/exoplayer/drm/DrmSessionManager;
.super Ljava/lang/Object;
.source "DrmSessionManager.java"


# virtual methods
.method public abstract close()V
.end method

.method public abstract getError()Ljava/lang/Exception;
.end method

.method public abstract getMediaCrypto()Landroid/media/MediaCrypto;
.end method

.method public abstract getState()I
.end method

.method public abstract open(Ljava/util/Map;Ljava/lang/String;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map",
            "<",
            "Ljava/util/UUID;",
            "[B>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation
.end method

.method public abstract requiresSecureDecoderComponent(Ljava/lang/String;)Z
.end method
