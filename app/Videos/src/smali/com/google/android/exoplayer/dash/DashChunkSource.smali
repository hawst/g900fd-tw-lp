.class public Lcom/google/android/exoplayer/dash/DashChunkSource;
.super Ljava/lang/Object;
.source "DashChunkSource.java"

# interfaces
.implements Lcom/google/android/exoplayer/chunk/ChunkSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/exoplayer/dash/DashChunkSource$InitializationLoadable;
    }
.end annotation


# instance fields
.field private final dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

.field private final evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

.field private final evaluator:Lcom/google/android/exoplayer/chunk/FormatEvaluator;

.field private final extractors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/parser/Extractor;",
            ">;"
        }
    .end annotation
.end field

.field private final formats:[Lcom/google/android/exoplayer/chunk/Format;

.field private lastChunkWasInitialization:Z

.field private final maxHeight:I

.field private final maxWidth:I

.field private final representations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/dash/mpd/Representation;",
            ">;"
        }
    .end annotation
.end field

.field private final segmentIndexes:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/dash/DashSegmentIndex;",
            ">;"
        }
    .end annotation
.end field

.field private final trackInfo:Lcom/google/android/exoplayer/TrackInfo;


# direct methods
.method public varargs constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/chunk/FormatEvaluator;[Lcom/google/android/exoplayer/dash/mpd/Representation;)V
    .locals 12
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "evaluator"    # Lcom/google/android/exoplayer/chunk/FormatEvaluator;
    .param p3, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation;

    .prologue
    const/4 v7, 0x0

    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p1, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    .line 76
    iput-object p2, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluator:Lcom/google/android/exoplayer/chunk/FormatEvaluator;

    .line 77
    array-length v5, p3

    new-array v5, v5, [Lcom/google/android/exoplayer/chunk/Format;

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    .line 78
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->extractors:Ljava/util/HashMap;

    .line 79
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->segmentIndexes:Ljava/util/HashMap;

    .line 80
    new-instance v5, Ljava/util/HashMap;

    invoke-direct {v5}, Ljava/util/HashMap;-><init>()V

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->representations:Ljava/util/HashMap;

    .line 81
    new-instance v5, Lcom/google/android/exoplayer/TrackInfo;

    aget-object v6, p3, v7

    iget-object v6, v6, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v6, v6, Lcom/google/android/exoplayer/chunk/Format;->mimeType:Ljava/lang/String;

    aget-object v7, p3, v7

    iget-wide v8, v7, Lcom/google/android/exoplayer/dash/mpd/Representation;->periodDurationMs:J

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-direct {v5, v6, v8, v9}, Lcom/google/android/exoplayer/TrackInfo;-><init>(Ljava/lang/String;J)V

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    .line 83
    new-instance v5, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    invoke-direct {v5}, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;-><init>()V

    iput-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    .line 84
    const/4 v3, 0x0

    .line 85
    .local v3, "maxWidth":I
    const/4 v2, 0x0

    .line 86
    .local v2, "maxHeight":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v5, p3

    if-ge v1, v5, :cond_2

    .line 87
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, p3, v1

    iget-object v6, v6, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    aput-object v6, v5, v1

    .line 88
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/google/android/exoplayer/chunk/Format;->width:I

    invoke-static {v5, v3}, Ljava/lang/Math;->max(II)I

    move-result v3

    .line 89
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v5, v5, v1

    iget v5, v5, Lcom/google/android/exoplayer/chunk/Format;->height:I

    invoke-static {v5, v2}, Ljava/lang/Math;->max(II)I

    move-result v2

    .line 90
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v5, v5, v1

    iget-object v5, v5, Lcom/google/android/exoplayer/chunk/Format;->mimeType:Ljava/lang/String;

    invoke-direct {p0, v5}, Lcom/google/android/exoplayer/dash/DashChunkSource;->mimeTypeIsWebm(Ljava/lang/String;)Z

    move-result v5

    if-eqz v5, :cond_1

    new-instance v0, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;

    invoke-direct {v0}, Lcom/google/android/exoplayer/parser/webm/WebmExtractor;-><init>()V

    .line 92
    .local v0, "extractor":Lcom/google/android/exoplayer/parser/Extractor;
    :goto_1
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->extractors:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->representations:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    aget-object v7, p3, v1

    invoke-virtual {v5, v6, v7}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    aget-object v5, p3, v1

    invoke-virtual {v5}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getIndex()Lcom/google/android/exoplayer/dash/DashSegmentIndex;

    move-result-object v4

    .line 95
    .local v4, "segmentIndex":Lcom/google/android/exoplayer/dash/DashSegmentIndex;
    if-eqz v4, :cond_0

    .line 96
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->segmentIndexes:Ljava/util/HashMap;

    iget-object v6, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, v6, v1

    iget-object v6, v6, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v5, v6, v4}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 86
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 90
    .end local v0    # "extractor":Lcom/google/android/exoplayer/parser/Extractor;
    .end local v4    # "segmentIndex":Lcom/google/android/exoplayer/dash/DashSegmentIndex;
    :cond_1
    new-instance v0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-direct {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;-><init>()V

    goto :goto_1

    .line 99
    :cond_2
    iput v3, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->maxWidth:I

    .line 100
    iput v2, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->maxHeight:I

    .line 101
    iget-object v5, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    new-instance v6, Lcom/google/android/exoplayer/chunk/Format$DecreasingBandwidthComparator;

    invoke-direct {v6}, Lcom/google/android/exoplayer/chunk/Format$DecreasingBandwidthComparator;-><init>()V

    invoke-static {v5, v6}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 102
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/exoplayer/dash/DashChunkSource;)Ljava/util/HashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/exoplayer/dash/DashChunkSource;

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->segmentIndexes:Ljava/util/HashMap;

    return-object v0
.end method

.method private mimeTypeIsWebm(Ljava/lang/String;)Z
    .locals 1
    .param p1, "mimeType"    # Ljava/lang/String;

    .prologue
    .line 201
    const-string v0, "video/webm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_0

    const-string v0, "audio/webm"

    invoke-virtual {p1, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/RangedUri;Lcom/google/android/exoplayer/dash/mpd/RangedUri;Lcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 15
    .param p1, "initializationUri"    # Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    .param p2, "indexUri"    # Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    .param p3, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation;
    .param p4, "extractor"    # Lcom/google/android/exoplayer/parser/Extractor;
    .param p5, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p6, "trigger"    # I

    .prologue
    .line 207
    const/4 v11, 0x2

    .line 208
    .local v11, "expectedExtractorResult":I
    const-wide/16 v12, 0x0

    .line 210
    .local v12, "indexAnchor":J
    if-eqz p1, :cond_2

    .line 213
    or-int/lit8 v11, v11, 0x8

    .line 214
    invoke-virtual/range {p1 .. p2}, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->attemptMerge(Lcom/google/android/exoplayer/dash/mpd/RangedUri;)Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v14

    .line 215
    .local v14, "requestUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    if-eqz v14, :cond_1

    .line 216
    or-int/lit8 v11, v11, 0x10

    .line 217
    invoke-interface/range {p4 .. p4}, Lcom/google/android/exoplayer/parser/Extractor;->hasRelativeIndexOffsets()Z

    move-result v3

    if-eqz v3, :cond_0

    .line 218
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    add-long v12, v4, v6

    .line 230
    :cond_0
    :goto_0
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    invoke-virtual {v14}, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->getUri()Landroid/net/Uri;

    move-result-object v3

    iget-wide v4, v14, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    iget-wide v6, v14, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    invoke-virtual/range {p3 .. p3}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 232
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v4, Lcom/google/android/exoplayer/dash/DashChunkSource$InitializationLoadable;

    move-object/from16 v0, p3

    iget-object v9, v0, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object v5, p0

    move-object/from16 v6, p5

    move-object v7, v2

    move/from16 v8, p6

    move-object/from16 v10, p4

    invoke-direct/range {v4 .. v13}, Lcom/google/android/exoplayer/dash/DashChunkSource$InitializationLoadable;-><init>(Lcom/google/android/exoplayer/dash/DashChunkSource;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;ILcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/parser/Extractor;IJ)V

    return-object v4

    .line 221
    .end local v2    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    :cond_1
    move-object/from16 v14, p1

    goto :goto_0

    .line 224
    .end local v14    # "requestUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    :cond_2
    move-object/from16 v14, p2

    .line 225
    .restart local v14    # "requestUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    invoke-interface/range {p4 .. p4}, Lcom/google/android/exoplayer/parser/Extractor;->hasRelativeIndexOffsets()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 226
    move-object/from16 v0, p2

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    move-object/from16 v0, p2

    iget-wide v6, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    add-long v12, v4, v6

    .line 228
    :cond_3
    or-int/lit8 v11, v11, 0x10

    goto :goto_0
.end method

.method private newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/dash/DashSegmentIndex;Lcom/google/android/exoplayer/parser/Extractor;Lcom/google/android/exoplayer/upstream/DataSource;II)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 22
    .param p1, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation;
    .param p2, "segmentIndex"    # Lcom/google/android/exoplayer/dash/DashSegmentIndex;
    .param p3, "extractor"    # Lcom/google/android/exoplayer/parser/Extractor;
    .param p4, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p5, "segmentNum"    # I
    .param p6, "trigger"    # I

    .prologue
    .line 238
    invoke-interface/range {p2 .. p2}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getLastSegmentNum()I

    move-result v17

    .line 239
    .local v17, "lastSegmentNum":I
    move/from16 v0, p5

    move/from16 v1, v17

    if-ne v0, v1, :cond_0

    const/4 v12, -0x1

    .line 240
    .local v12, "nextSegmentNum":I
    :goto_0
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getTimeUs(I)J

    move-result-wide v20

    .line 241
    .local v20, "startTimeUs":J
    move/from16 v0, p5

    move/from16 v1, v17

    if-ge v0, v1, :cond_1

    add-int/lit8 v3, p5, 0x1

    move-object/from16 v0, p2

    invoke-interface {v0, v3}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getTimeUs(I)J

    move-result-wide v10

    .line 243
    .local v10, "endTimeUs":J
    :goto_1
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getSegmentUrl(I)Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v18

    .line 244
    .local v18, "segmentUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->getUri()Landroid/net/Uri;

    move-result-object v3

    move-object/from16 v0, v18

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    move-object/from16 v0, v18

    iget-wide v6, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 246
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v3, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;

    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    const/4 v14, 0x0

    const-wide/16 v15, 0x0

    move-object/from16 v4, p4

    move-object v5, v2

    move/from16 v7, p6

    move-wide/from16 v8, v20

    move-object/from16 v13, p3

    invoke-direct/range {v3 .. v16}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;IJJILcom/google/android/exoplayer/parser/Extractor;ZJ)V

    return-object v3

    .line 239
    .end local v2    # "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    .end local v10    # "endTimeUs":J
    .end local v12    # "nextSegmentNum":I
    .end local v18    # "segmentUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    .end local v20    # "startTimeUs":J
    :cond_0
    add-int/lit8 v12, p5, 0x1

    goto :goto_0

    .line 241
    .restart local v12    # "nextSegmentNum":I
    .restart local v20    # "startTimeUs":J
    :cond_1
    move-object/from16 v0, p2

    move/from16 v1, p5

    invoke-interface {v0, v1}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getDurationUs(I)J

    move-result-wide v4

    add-long v10, v20, v4

    goto :goto_1
.end method


# virtual methods
.method public continueBuffering(J)V
    .locals 0
    .param p1, "playbackPositionUs"    # J

    .prologue
    .line 129
    return-void
.end method

.method public disable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    iget-object v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluator:Lcom/google/android/exoplayer/chunk/FormatEvaluator;

    invoke-interface {v0}, Lcom/google/android/exoplayer/chunk/FormatEvaluator;->disable()V

    .line 124
    return-void
.end method

.method public enable()V
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluator:Lcom/google/android/exoplayer/chunk/FormatEvaluator;

    invoke-interface {v0}, Lcom/google/android/exoplayer/chunk/FormatEvaluator;->enable()V

    .line 119
    return-void
.end method

.method public final getChunkOperation(Ljava/util/List;JJLcom/google/android/exoplayer/chunk/ChunkOperationHolder;)V
    .locals 18
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "out"    # Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ",
            "Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 134
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    iput v7, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    .line 135
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget-object v2, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    if-eqz v2, :cond_0

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->lastChunkWasInitialization:Z

    if-nez v2, :cond_1

    .line 136
    :cond_0
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluator:Lcom/google/android/exoplayer/chunk/FormatEvaluator;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    move-object/from16 v3, p1

    move-wide/from16 v4, p4

    invoke-interface/range {v2 .. v7}, Lcom/google/android/exoplayer/chunk/FormatEvaluator;->evaluate(Ljava/util/List;J[Lcom/google/android/exoplayer/chunk/Format;Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)V

    .line 138
    :cond_1
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget-object v0, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v16, v0

    .line 139
    .local v16, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v2, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    move-object/from16 v0, p6

    iput v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    .line 141
    if-nez v16, :cond_3

    .line 142
    const/4 v2, 0x0

    move-object/from16 v0, p6

    iput-object v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    .line 188
    :cond_2
    :goto_0
    return-void

    .line 144
    :cond_3
    move-object/from16 v0, p6

    iget v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v7

    if-ne v2, v7, :cond_4

    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    if-eqz v2, :cond_4

    move-object/from16 v0, p6

    iget-object v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    iget-object v2, v2, Lcom/google/android/exoplayer/chunk/Chunk;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v2, v2, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 151
    :cond_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->representations:Ljava/util/HashMap;

    move-object/from16 v0, v16

    iget-object v7, v0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v5

    check-cast v5, Lcom/google/android/exoplayer/dash/mpd/Representation;

    .line 152
    .local v5, "selectedRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->extractors:Ljava/util/HashMap;

    iget-object v7, v5, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/exoplayer/parser/Extractor;

    .line 154
    .local v6, "extractor":Lcom/google/android/exoplayer/parser/Extractor;
    const/4 v3, 0x0

    .line 155
    .local v3, "pendingInitializationUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    const/4 v4, 0x0

    .line 156
    .local v4, "pendingIndexUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    invoke-interface {v6}, Lcom/google/android/exoplayer/parser/Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v2

    if-nez v2, :cond_5

    .line 157
    invoke-virtual {v5}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getInitializationUri()Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v3

    .line 159
    :cond_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->segmentIndexes:Ljava/util/HashMap;

    iget-object v7, v5, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->containsKey(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_6

    .line 160
    invoke-virtual {v5}, Lcom/google/android/exoplayer/dash/mpd/Representation;->getIndexUri()Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v4

    .line 162
    :cond_6
    if-nez v3, :cond_7

    if-eqz v4, :cond_8

    .line 164
    :cond_7
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v8, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    move-object/from16 v2, p0

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/dash/DashChunkSource;->newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/RangedUri;Lcom/google/android/exoplayer/dash/mpd/RangedUri;Lcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/parser/Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v14

    .line 166
    .local v14, "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->lastChunkWasInitialization:Z

    .line 167
    move-object/from16 v0, p6

    iput-object v14, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto :goto_0

    .line 172
    .end local v14    # "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->segmentIndexes:Ljava/util/HashMap;

    iget-object v7, v5, Lcom/google/android/exoplayer/dash/mpd/Representation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v2, v7}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Lcom/google/android/exoplayer/dash/DashSegmentIndex;

    .line 173
    .local v9, "segmentIndex":Lcom/google/android/exoplayer/dash/DashSegmentIndex;
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_9

    .line 174
    move-wide/from16 v0, p2

    invoke-interface {v9, v0, v1}, Lcom/google/android/exoplayer/dash/DashSegmentIndex;->getSegmentNum(J)I

    move-result v12

    .line 179
    .local v12, "nextSegmentNum":I
    :goto_1
    const/4 v2, -0x1

    if-ne v12, v2, :cond_a

    .line 180
    const/4 v2, 0x0

    move-object/from16 v0, p6

    iput-object v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto/16 :goto_0

    .line 176
    .end local v12    # "nextSegmentNum":I
    :cond_9
    move-object/from16 v0, p6

    iget v2, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    add-int/lit8 v2, v2, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/chunk/MediaChunk;

    iget v12, v2, Lcom/google/android/exoplayer/chunk/MediaChunk;->nextChunkIndex:I

    .restart local v12    # "nextSegmentNum":I
    goto :goto_1

    .line 184
    :cond_a
    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v13, v2, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    move-object/from16 v7, p0

    move-object v8, v5

    move-object v10, v6

    invoke-direct/range {v7 .. v13}, Lcom/google/android/exoplayer/dash/DashChunkSource;->newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation;Lcom/google/android/exoplayer/dash/DashSegmentIndex;Lcom/google/android/exoplayer/parser/Extractor;Lcom/google/android/exoplayer/upstream/DataSource;II)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v15

    .line 186
    .local v15, "nextMediaChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/exoplayer/dash/DashChunkSource;->lastChunkWasInitialization:Z

    .line 187
    move-object/from16 v0, p6

    iput-object v15, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto/16 :goto_0
.end method

.method public getError()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 192
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getMaxVideoDimensions(Lcom/google/android/exoplayer/MediaFormat;)V
    .locals 2
    .param p1, "out"    # Lcom/google/android/exoplayer/MediaFormat;

    .prologue
    .line 106
    iget-object v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    iget-object v0, v0, Lcom/google/android/exoplayer/TrackInfo;->mimeType:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 107
    iget v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->maxWidth:I

    iget v1, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->maxHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer/MediaFormat;->setMaxVideoDimensions(II)V

    .line 109
    :cond_0
    return-void
.end method

.method public final getTrackInfo()Lcom/google/android/exoplayer/TrackInfo;
    .locals 1

    .prologue
    .line 113
    iget-object v0, p0, Lcom/google/android/exoplayer/dash/DashChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    return-object v0
.end method

.method public onChunkLoadError(Lcom/google/android/exoplayer/chunk/Chunk;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "chunk"    # Lcom/google/android/exoplayer/chunk/Chunk;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 198
    return-void
.end method
