.class final Lcom/google/android/exoplayer/audio/AudioTrack$NoopAudioTimestampCompat;
.super Ljava/lang/Object;
.source "AudioTrack.java"

# interfaces
.implements Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/audio/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "NoopAudioTimestampCompat"
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 663
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/exoplayer/audio/AudioTrack$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/exoplayer/audio/AudioTrack$1;

    .prologue
    .line 663
    invoke-direct {p0}, Lcom/google/android/exoplayer/audio/AudioTrack$NoopAudioTimestampCompat;-><init>()V

    return-void
.end method


# virtual methods
.method public getFramePosition()J
    .locals 1

    .prologue
    .line 679
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getNanoTime()J
    .locals 1

    .prologue
    .line 673
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public update(Landroid/media/AudioTrack;)Z
    .locals 1
    .param p1, "audioTrack"    # Landroid/media/AudioTrack;

    .prologue
    .line 667
    const/4 v0, 0x0

    return v0
.end method
