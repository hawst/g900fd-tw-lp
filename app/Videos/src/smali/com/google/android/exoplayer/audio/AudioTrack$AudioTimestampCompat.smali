.class interface abstract Lcom/google/android/exoplayer/audio/AudioTrack$AudioTimestampCompat;
.super Ljava/lang/Object;
.source "AudioTrack.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/audio/AudioTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "AudioTimestampCompat"
.end annotation


# virtual methods
.method public abstract getFramePosition()J
.end method

.method public abstract getNanoTime()J
.end method

.method public abstract update(Landroid/media/AudioTrack;)Z
.end method
