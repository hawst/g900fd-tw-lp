.class public final Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
.super Lcom/google/android/exoplayer/parser/mp4/Atom;
.source "Atom.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/exoplayer/parser/mp4/Atom;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ContainerAtom"
.end annotation


# instance fields
.field public final children:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/exoplayer/parser/mp4/Atom;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "type"    # I

    .prologue
    .line 84
    invoke-direct {p0, p1}, Lcom/google/android/exoplayer/parser/mp4/Atom;-><init>(I)V

    .line 85
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    .line 86
    return-void
.end method


# virtual methods
.method public add(Lcom/google/android/exoplayer/parser/mp4/Atom;)V
    .locals 1
    .param p1, "atom"    # Lcom/google/android/exoplayer/parser/mp4/Atom;

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 90
    return-void
.end method

.method public getContainerAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 104
    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 105
    .local v1, "childrenSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 106
    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom;

    .line 107
    .local v0, "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget v3, v0, Lcom/google/android/exoplayer/parser/mp4/Atom;->type:I

    if-ne v3, p1, :cond_0

    .line 108
    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;

    .line 111
    .end local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :goto_1
    return-object v0

    .line 105
    .restart local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 111
    .end local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public getLeafAtomOfType(I)Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;
    .locals 4
    .param p1, "type"    # I

    .prologue
    .line 93
    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v3}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 94
    .local v1, "childrenSize":I
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    if-ge v2, v1, :cond_1

    .line 95
    iget-object v3, p0, Lcom/google/android/exoplayer/parser/mp4/Atom$ContainerAtom;->children:Ljava/util/ArrayList;

    invoke-virtual {v3, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom;

    .line 96
    .local v0, "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    iget v3, v0, Lcom/google/android/exoplayer/parser/mp4/Atom;->type:I

    if-ne v3, p1, :cond_0

    .line 97
    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/Atom$LeafAtom;

    .line 100
    .end local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :goto_1
    return-object v0

    .line 94
    .restart local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 100
    .end local v0    # "atom":Lcom/google/android/exoplayer/parser/mp4/Atom;
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method
