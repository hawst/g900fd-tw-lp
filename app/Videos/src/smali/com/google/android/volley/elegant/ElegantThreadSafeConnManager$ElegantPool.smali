.class public Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;
.super Lorg/apache/http/impl/conn/tsccm/ConnPoolByRoute;
.source "ElegantThreadSafeConnManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "ElegantPool"
.end annotation


# direct methods
.method public constructor <init>(Lorg/apache/http/conn/ClientConnectionOperator;Lorg/apache/http/params/HttpParams;)V
    .locals 0
    .param p1, "operator"    # Lorg/apache/http/conn/ClientConnectionOperator;
    .param p2, "params"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 102
    invoke-direct {p0, p1, p2}, Lorg/apache/http/impl/conn/tsccm/ConnPoolByRoute;-><init>(Lorg/apache/http/conn/ClientConnectionOperator;Lorg/apache/http/params/HttpParams;)V

    .line 103
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;)Ljava/util/concurrent/locks/Lock;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    return-object v0
.end method


# virtual methods
.method protected getEntryBlocking(Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;JLjava/util/concurrent/TimeUnit;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;
    .locals 21
    .param p1, "route"    # Lorg/apache/http/conn/routing/HttpRoute;
    .param p2, "state"    # Ljava/lang/Object;
    .param p3, "timeout"    # J
    .param p5, "tunit"    # Ljava/util/concurrent/TimeUnit;
    .param p6, "aborter"    # Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/apache/http/conn/ConnectionPoolTimeoutException;,
            Ljava/lang/InterruptedException;
        }
    .end annotation

    .prologue
    .line 132
    const/4 v4, 0x0

    .line 133
    .local v4, "deadline":Ljava/util/Date;
    const-wide/16 v14, 0x0

    cmp-long v14, p3, v14

    if-lez v14, :cond_0

    .line 134
    new-instance v4, Ljava/util/Date;

    .end local v4    # "deadline":Ljava/util/Date;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    move-object/from16 v0, p5

    move-wide/from16 v1, p3

    invoke-virtual {v0, v1, v2}, Ljava/util/concurrent/TimeUnit;->toMillis(J)J

    move-result-wide v16

    add-long v14, v14, v16

    invoke-direct {v4, v14, v15}, Ljava/util/Date;-><init>(J)V

    .line 138
    .restart local v4    # "deadline":Ljava/util/Date;
    :cond_0
    const/4 v5, 0x0

    .line 139
    .local v5, "entry":Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v10

    .line 140
    .local v10, "startTime":J
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->lock()V

    .line 143
    const/4 v14, 0x1

    :try_start_0
    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v14}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->getRoutePool(Lorg/apache/http/conn/routing/HttpRoute;Z)Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;

    move-result-object v9

    .line 144
    .local v9, "rospl":Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;
    const/4 v13, 0x0

    .line 146
    .local v13, "waitingThread":Lorg/apache/http/impl/conn/tsccm/WaitingThread;
    :cond_1
    :goto_0
    if-nez v5, :cond_4

    .line 148
    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->isShutDown:Z

    if-eqz v14, :cond_3

    .line 149
    new-instance v14, Ljava/lang/IllegalStateException;

    const-string v15, "Connection pool shut down."

    invoke-direct {v14, v15}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v14
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 227
    .end local v9    # "rospl":Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;
    .end local v13    # "waitingThread":Lorg/apache/http/impl/conn/tsccm/WaitingThread;
    :catchall_0
    move-exception v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v15}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 228
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 229
    .local v6, "endTime":J
    sub-long v16, v6, v10

    const-wide/16 v18, 0xa

    cmp-long v15, v16, v18

    if-lez v15, :cond_2

    .line 230
    const-string v15, "GetEntryBlocking() took %s ms"

    const/16 v16, 0x1

    move/from16 v0, v16

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    sub-long v18, v6, v10

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v18

    aput-object v18, v16, v17

    invoke-static/range {v15 .. v16}, Lcom/android/volley/VolleyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 232
    :cond_2
    throw v14

    .line 165
    .end local v6    # "endTime":J
    .restart local v9    # "rospl":Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;
    .restart local v13    # "waitingThread":Lorg/apache/http/impl/conn/tsccm/WaitingThread;
    :cond_3
    :try_start_1
    move-object/from16 v0, p0

    move-object/from16 v1, p2

    invoke-virtual {v0, v9, v1}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->getFreeEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Ljava/lang/Object;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v5

    .line 166
    if-eqz v5, :cond_6

    .line 227
    :cond_4
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->unlock()V

    .line 228
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    .line 229
    .restart local v6    # "endTime":J
    sub-long v14, v6, v10

    const-wide/16 v16, 0xa

    cmp-long v14, v14, v16

    if-lez v14, :cond_5

    .line 230
    const-string v14, "GetEntryBlocking() took %s ms"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    sub-long v18, v6, v10

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    aput-object v17, v15, v16

    invoke-static {v14, v15}, Lcom/android/volley/VolleyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 234
    :cond_5
    return-object v5

    .line 169
    .end local v6    # "endTime":J
    :cond_6
    :try_start_2
    const-string v14, "Constructed new connection to route=[%s]"

    const/4 v15, 0x1

    new-array v15, v15, [Ljava/lang/Object;

    const/16 v16, 0x0

    aput-object p1, v15, v16

    invoke-static {v14, v15}, Lcom/android/volley/VolleyLog;->v(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 173
    invoke-virtual {v9}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->getCapacity()I

    move-result v14

    if-lez v14, :cond_7

    const/4 v8, 0x1

    .line 181
    .local v8, "hasCapacity":Z
    :goto_1
    if-eqz v8, :cond_8

    move-object/from16 v0, p0

    iget v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->numConnections:I

    move-object/from16 v0, p0

    iget v15, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->maxTotalConnections:I

    if-ge v14, v15, :cond_8

    .line 182
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->operator:Lorg/apache/http/conn/ClientConnectionOperator;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v14}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->createEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Lorg/apache/http/conn/ClientConnectionOperator;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v5

    goto/16 :goto_0

    .line 173
    .end local v8    # "hasCapacity":Z
    :cond_7
    const/4 v8, 0x0

    goto :goto_1

    .line 184
    .restart local v8    # "hasCapacity":Z
    :cond_8
    if-eqz v8, :cond_9

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->freeConnections:Ljava/util/Queue;

    invoke-interface {v14}, Ljava/util/Queue;->isEmpty()Z

    move-result v14

    if-nez v14, :cond_9

    .line 186
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->deleteLeastUsedEntry()V

    .line 187
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->operator:Lorg/apache/http/conn/ClientConnectionOperator;

    move-object/from16 v0, p0

    invoke-virtual {v0, v9, v14}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->createEntry(Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;Lorg/apache/http/conn/ClientConnectionOperator;)Lorg/apache/http/impl/conn/tsccm/BasicPoolEntry;

    move-result-object v5

    goto/16 :goto_0

    .line 196
    :cond_9
    if-nez v13, :cond_a

    .line 197
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->poolLock:Ljava/util/concurrent/locks/Lock;

    invoke-interface {v14}, Ljava/util/concurrent/locks/Lock;->newCondition()Ljava/util/concurrent/locks/Condition;

    move-result-object v14

    move-object/from16 v0, p0

    invoke-virtual {v0, v14, v9}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->newWaitingThread(Ljava/util/concurrent/locks/Condition;Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;)Lorg/apache/http/impl/conn/tsccm/WaitingThread;

    move-result-object v13

    .line 199
    move-object/from16 v0, p6

    invoke-virtual {v0, v13}, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;->setWaitingThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 202
    :cond_a
    const/4 v12, 0x0

    .line 204
    .local v12, "success":Z
    :try_start_3
    invoke-virtual {v9, v13}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->queueThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    .line 205
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v14, v13}, Ljava/util/Queue;->add(Ljava/lang/Object;)Z

    .line 206
    invoke-virtual {v13, v4}, Lorg/apache/http/impl/conn/tsccm/WaitingThread;->await(Ljava/util/Date;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    move-result v12

    .line 213
    :try_start_4
    invoke-virtual {v9, v13}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->removeThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v14, v13}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    .line 218
    if-nez v12, :cond_1

    if-eqz v4, :cond_1

    invoke-virtual {v4}, Ljava/util/Date;->getTime()J

    move-result-wide v14

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v16

    cmp-long v14, v14, v16

    if-gtz v14, :cond_1

    .line 220
    new-instance v14, Lorg/apache/http/conn/ConnectionPoolTimeoutException;

    const-string v15, "Timeout waiting for connection"

    invoke-direct {v14, v15}, Lorg/apache/http/conn/ConnectionPoolTimeoutException;-><init>(Ljava/lang/String;)V

    throw v14

    .line 213
    :catchall_1
    move-exception v14

    invoke-virtual {v9, v13}, Lorg/apache/http/impl/conn/tsccm/RouteSpecificPool;->removeThread(Lorg/apache/http/impl/conn/tsccm/WaitingThread;)V

    .line 214
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;->waitingThreads:Ljava/util/Queue;

    invoke-interface {v15, v13}, Ljava/util/Queue;->remove(Ljava/lang/Object;)Z

    throw v14
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0
.end method

.method public requestPoolEntry(Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;)Lorg/apache/http/impl/conn/tsccm/PoolEntryRequest;
    .locals 2
    .param p1, "route"    # Lorg/apache/http/conn/routing/HttpRoute;
    .param p2, "state"    # Ljava/lang/Object;

    .prologue
    .line 107
    new-instance v0, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;

    invoke-direct {v0}, Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;-><init>()V

    .line 108
    .local v0, "aborter":Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;
    new-instance v1, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool$1;

    invoke-direct {v1, p0, v0, p1, p2}, Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool$1;-><init>(Lcom/google/android/volley/elegant/ElegantThreadSafeConnManager$ElegantPool;Lorg/apache/http/impl/conn/tsccm/WaitingThreadAborter;Lorg/apache/http/conn/routing/HttpRoute;Ljava/lang/Object;)V

    return-object v1
.end method
