.class public Lcom/google/android/videos/store/PurchaseStoreUtil;
.super Ljava/lang/Object;
.source "PurchaseStoreUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/PurchaseStoreUtil$ShowFullSyncTimestampQuery;
    }
.end annotation


# static fields
.field private static final NO_COLUMNS:[Ljava/lang/String;

.field private static final VALID_ASSET_TYPES:[I

.field private static final VALID_FORMAT_TYPES:[I

.field private static final VALID_PURCHASE_TYPES:[I


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v1, 0x2

    .line 53
    new-array v0, v1, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_PURCHASE_TYPES:[I

    .line 55
    new-array v0, v1, [I

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_FORMAT_TYPES:[I

    .line 57
    const/4 v0, 0x4

    new-array v0, v0, [I

    fill-array-data v0, :array_2

    sput-object v0, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_ASSET_TYPES:[I

    .line 59
    const/4 v0, 0x1

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "NULL"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/store/PurchaseStoreUtil;->NO_COLUMNS:[Ljava/lang/String;

    return-void

    .line 53
    :array_0
    .array-data 4
        0x1
        0x2
    .end array-data

    .line 55
    :array_1
    .array-data 4
        0x2
        0x1
    .end array-data

    .line 57
    :array_2
    .array-data 4
        0x6
        0x12
        0x13
        0x14
    .end array-data
.end method

.method private static addPlaybackContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/ContentValues;)Z
    .locals 4
    .param p0, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "values"    # Landroid/content/ContentValues;

    .prologue
    const/4 v1, 0x0

    .line 322
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    if-eqz v2, :cond_0

    .line 323
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v0, v2, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    .line 324
    .local v0, "playback":Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    const-string v2, "last_playback_is_dirty"

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {p1, v2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 325
    const-string v1, "last_playback_start_timestamp"

    iget-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->startTimestampMsec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 326
    const-string v1, "last_watched_timestamp"

    iget-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 327
    const-string v1, "resume_timestamp"

    iget-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->positionMsec:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {p1, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 328
    const/4 v1, 0x1

    .line 330
    .end local v0    # "playback":Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;
    :cond_0
    return v1
.end method

.method private static arrayContains([II)Z
    .locals 2
    .param p0, "array"    # [I
    .param p1, "value"    # I

    .prologue
    .line 656
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p0

    if-ge v0, v1, :cond_1

    .line 657
    aget v1, p0, v0

    if-ne v1, p1, :cond_0

    .line 658
    const/4 v1, 0x1

    .line 661
    :goto_1
    return v1

    .line 656
    :cond_0
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 661
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private static buildBaseAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "rootId"    # Ljava/lang/String;

    .prologue
    .line 629
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 630
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "assets_type"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 631
    const-string v1, "assets_id"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 632
    const-string v1, "root_id"

    invoke-virtual {v0, v1, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 633
    return-object v0
.end method

.method private static buildBaseVideoContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;J)Landroid/content/ContentValues;
    .locals 20
    .param p0, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "userCountry"    # Ljava/lang/String;
    .param p2, "syncTimestamp"    # J

    .prologue
    .line 402
    invoke-static {}, Lcom/google/android/videos/store/D$Videos$Compat;->newContentValues()Landroid/content/ContentValues;

    move-result-object v14

    .line 403
    .local v14, "values":Landroid/content/ContentValues;
    const-string v16, "video_synced_timestamp"

    invoke-static/range {p2 .. p3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 404
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v15, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 405
    .local v15, "videoId":Ljava/lang/String;
    const-string v16, "video_id"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0, v15}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 406
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 408
    .local v9, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    const-string v16, "title"

    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 409
    const-string v16, "description"

    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 411
    new-instance v5, Landroid/util/SparseArray;

    invoke-direct {v5}, Landroid/util/SparseArray;-><init>()V

    .line 412
    .local v5, "creditsByRole":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Ljava/util/List<Ljava/lang/String;>;>;"
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v6, v0, :cond_1

    .line 413
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->credits:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;

    move-object/from16 v16, v0

    aget-object v3, v16, v6

    .line 414
    .local v3, "credit":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    iget v0, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/List;

    .line 415
    .local v4, "credits":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-nez v4, :cond_0

    .line 416
    iget v0, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->role:I

    move/from16 v16, v0

    iget-object v0, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    move-object/from16 v17, v0

    const/16 v18, 0x0

    move/from16 v0, v18

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v18, v0

    invoke-static/range {v17 .. v18}, Lcom/google/android/videos/utils/CollectionUtil;->newArrayList(Ljava/lang/Object;[Ljava/lang/Object;)Ljava/util/ArrayList;

    move-result-object v17

    move/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v5, v0, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 412
    :goto_1
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 418
    :cond_0
    iget-object v0, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;->name:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 421
    .end local v3    # "credit":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Credit;
    .end local v4    # "credits":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_1
    const-string v17, "writers"

    const/16 v16, 0x4

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v14, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 422
    const-string v17, "directors"

    const/16 v16, 0x2

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v14, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 423
    const-string v17, "actors"

    const/16 v16, 0x1

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v14, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 424
    const-string v17, "producers"

    const/16 v16, 0x3

    move/from16 v0, v16

    invoke-virtual {v5, v0}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Ljava/util/List;

    move-object/from16 v0, v17

    move-object/from16 v1, v16

    invoke-static {v14, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 426
    const/4 v11, 0x0

    .line 427
    .local v11, "ratingFound":Z
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v12, v0

    .line 428
    .local v12, "ratingsCount":I
    const/16 v16, 0x1

    move/from16 v0, v16

    if-ne v12, v0, :cond_6

    .line 429
    const/4 v11, 0x1

    .line 430
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-object/from16 v16, v0

    const/16 v17, 0x0

    aget-object v10, v16, v17

    .line 431
    .local v10, "rating":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    const-string v16, "rating_id"

    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 432
    const-string v16, "rating_name"

    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 446
    .end local v10    # "rating":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    :cond_2
    if-nez v11, :cond_3

    .line 447
    const-string v16, "rating_id"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 448
    const-string v16, "rating_name"

    move-object/from16 v0, v16

    invoke-virtual {v14, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 451
    :cond_3
    const-string v16, "duration_seconds"

    iget v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 453
    const/4 v13, 0x1

    .line 454
    .local v13, "subtitleMode":I
    iget v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionMode:I

    move/from16 v16, v0

    packed-switch v16, :pswitch_data_0

    .line 463
    :goto_2
    const-string v16, "subtitle_mode"

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 464
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    move-object/from16 v16, v0

    invoke-static/range {v16 .. v16}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_4

    .line 465
    const-string v16, "default_subtitle_language"

    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->captionDefaultLanguage:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 467
    :cond_4
    const-string v16, "has_subtitles"

    iget-boolean v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 469
    iget-wide v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    move-wide/from16 v16, v0

    const-wide/16 v18, 0x0

    cmp-long v16, v16, v18

    if-eqz v16, :cond_5

    .line 470
    const-string v16, "release_year"

    iget-wide v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Lcom/google/android/videos/utils/TimeUtil;->getYear(J)I

    move-result v17

    invoke-static/range {v17 .. v17}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 471
    const-string v16, "publish_timestamp"

    iget-wide v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->releaseDateTimestampSec:J

    move-wide/from16 v18, v0

    invoke-static/range {v18 .. v19}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 474
    :cond_5
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-object/from16 v16, v0

    if-eqz v16, :cond_9

    .line 475
    const-string v16, "badge_surround_sound"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 476
    const-string v16, "badge_knowledge"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    move-object/from16 v17, v0

    move-object/from16 v0, v17

    iget-boolean v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->eastwood:Z

    move/from16 v17, v0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 482
    :goto_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->audioInfo:[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    .line 483
    .local v2, "audioInfo":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    if-eqz v2, :cond_b

    array-length v0, v2

    move/from16 v16, v0

    if-lez v16, :cond_b

    .line 484
    new-instance v8, Ljava/util/ArrayList;

    array-length v0, v2

    move/from16 v16, v0

    move/from16 v0, v16

    invoke-direct {v8, v0}, Ljava/util/ArrayList;-><init>(I)V

    .line 485
    .local v8, "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v6, 0x0

    :goto_4
    array-length v0, v2

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v6, v0, :cond_a

    .line 486
    aget-object v16, v2, v6

    move-object/from16 v0, v16

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    invoke-interface {v8, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 485
    add-int/lit8 v6, v6, 0x1

    goto :goto_4

    .line 433
    .end local v2    # "audioInfo":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v8    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .end local v13    # "subtitleMode":I
    :cond_6
    const/16 v16, 0x1

    move/from16 v0, v16

    if-le v12, v0, :cond_2

    invoke-static/range {p1 .. p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v16

    if-nez v16, :cond_2

    .line 434
    const/4 v6, 0x0

    :goto_5
    if-nez v11, :cond_2

    if-ge v6, v12, :cond_2

    .line 435
    iget-object v0, v9, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->contentRatings:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;

    move-object/from16 v16, v0

    aget-object v10, v16, v6

    .line 436
    .restart local v10    # "rating":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    const/4 v7, 0x0

    .local v7, "j":I
    :goto_6
    if-nez v11, :cond_7

    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, v16

    array-length v0, v0

    move/from16 v16, v0

    move/from16 v0, v16

    if-ge v7, v0, :cond_7

    .line 437
    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->countryCode:[Ljava/lang/String;

    move-object/from16 v16, v0

    aget-object v16, v16, v7

    move-object/from16 v0, p1

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v16

    if-eqz v16, :cond_8

    .line 438
    const/4 v11, 0x1

    .line 439
    const-string v16, "rating_id"

    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingId:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 440
    const-string v16, "rating_name"

    iget-object v0, v10, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;->contentRatingName:Ljava/lang/String;

    move-object/from16 v17, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 434
    :cond_7
    add-int/lit8 v6, v6, 0x1

    goto :goto_5

    .line 436
    :cond_8
    add-int/lit8 v7, v7, 0x1

    goto :goto_6

    .line 456
    .end local v7    # "j":I
    .end local v10    # "rating":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$ContentRating;
    .restart local v13    # "subtitleMode":I
    :pswitch_0
    const/4 v13, 0x3

    .line 457
    goto/16 :goto_2

    .line 459
    :pswitch_1
    const/4 v13, 0x2

    .line 460
    goto/16 :goto_2

    .line 478
    :cond_9
    const-string v16, "badge_surround_sound"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 479
    const-string v16, "badge_knowledge"

    const/16 v17, 0x0

    invoke-static/range {v17 .. v17}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v17

    move-object/from16 v0, v16

    move-object/from16 v1, v17

    invoke-virtual {v14, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto/16 :goto_3

    .line 488
    .restart local v2    # "audioInfo":[Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .restart local v8    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_a
    const-string v16, "audio_track_languages"

    move-object/from16 v0, v16

    invoke-static {v14, v0, v8}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 491
    .end local v8    # "languages":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_b
    return-object v14

    .line 454
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method

.method public static buildEpisodeAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;IILjava/lang/String;Z)Landroid/content/ContentValues;
    .locals 3
    .param p0, "episodeResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonSeqno"    # I
    .param p3, "episodeSeqno"    # I
    .param p4, "nextEpisodeId"    # Ljava/lang/String;
    .param p5, "nextEpisodeInSameSeason"    # Z

    .prologue
    .line 533
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x14

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 534
    invoke-static {p0, p1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 535
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "season_seqno"

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 536
    const-string v1, "episode_seqno"

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 537
    const-string v1, "next_episode_id"

    invoke-virtual {v0, v1, p4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 538
    const-string v1, "next_episode_in_same_season"

    invoke-static {p5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 539
    const-string v1, "end_credit_start_seconds"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 540
    const-string v1, "is_bonus_content"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-boolean v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 541
    return-object v0

    .line 533
    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static buildEpisodeContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "episodeResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "userCountry"    # Ljava/lang/String;
    .param p2, "syncTimestamp"    # J
    .param p4, "imageProvider"    # Lcom/google/android/videos/ui/AssetImageUriCreator;

    .prologue
    .line 388
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x14

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 390
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseVideoContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;J)Landroid/content/ContentValues;

    move-result-object v0

    .line 392
    .local v0, "values":Landroid/content/ContentValues;
    const/4 v1, 0x0

    invoke-virtual {p4, p0}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getEpisodeScreenshotUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->setVideoImageData(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 394
    const-string v1, "episode_season_id"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 395
    const-string v1, "episode_number_text"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 397
    return-object v0

    .line 388
    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static buildMovieAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/ContentValues;
    .locals 3
    .param p0, "movieResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 520
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_1

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 521
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 522
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "end_credit_start_seconds"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 523
    const-string v1, "is_bonus_content"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-boolean v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->bonusContent:Z

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 524
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 525
    const-string v1, "title_eidr_id"

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->titleEidrId:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 527
    :cond_0
    return-object v0

    .line 520
    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static buildMovieContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;
    .locals 4
    .param p0, "movieResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "userCountry"    # Ljava/lang/String;
    .param p2, "syncTimestamp"    # J
    .param p4, "imageProvider"    # Lcom/google/android/videos/ui/AssetImageUriCreator;

    .prologue
    .line 376
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v2, 0x6

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 377
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseVideoContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;J)Landroid/content/ContentValues;

    move-result-object v0

    .line 379
    .local v0, "values":Landroid/content/ContentValues;
    invoke-virtual {p4, p0}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getMoviePosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p4, p0}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getMovieScreenshotUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->setVideoImageData(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V

    .line 383
    return-object v0

    .line 376
    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static buildSeasonAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Landroid/content/ContentValues;
    .locals 3
    .param p0, "seasonResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "seasonSeqno"    # I

    .prologue
    .line 562
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x13

    if-ne v1, v2, :cond_0

    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 564
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {p0, v1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    .line 565
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "season_seqno"

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 566
    return-object v0

    .line 562
    .end local v0    # "values":Landroid/content/ContentValues;
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public static buildSeasonContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;J)Landroid/content/ContentValues;
    .locals 5
    .param p0, "seasonResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "syncTimestamp"    # J

    .prologue
    .line 549
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 550
    .local v1, "values":Landroid/content/ContentValues;
    const-string v2, "season_synced_timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 551
    const-string v2, "season_id"

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 552
    const-string v2, "show_id"

    iget-object v3, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 554
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 555
    .local v0, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    const-string v2, "season_title"

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 556
    const-string v2, "season_long_title"

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 557
    return-object v1
.end method

.method public static buildShowAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/ContentValues;
    .locals 2
    .param p0, "showResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v1, 0x12

    if-ne v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 624
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {p0, v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->buildBaseAssetContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Landroid/content/ContentValues;

    move-result-object v0

    return-object v0

    .line 623
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static buildShowContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;JLcom/google/android/videos/ui/AssetImageUriCreator;)Landroid/content/ContentValues;
    .locals 7
    .param p0, "showResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "syncTimestamp"    # J
    .param p3, "imageProvider"    # Lcom/google/android/videos/ui/AssetImageUriCreator;

    .prologue
    const/4 v6, 0x1

    .line 574
    new-instance v3, Landroid/content/ContentValues;

    invoke-direct {v3}, Landroid/content/ContentValues;-><init>()V

    .line 575
    .local v3, "values":Landroid/content/ContentValues;
    const-string v4, "shows_synced_timestamp"

    invoke-static {p1, p2}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 576
    const-string v4, "shows_id"

    iget-object v5, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 578
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 579
    .local v0, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    const-string v4, "shows_title"

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 580
    const-string v4, "shows_description"

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 581
    invoke-virtual {p3, p0}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getShowPosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v2

    .line 582
    .local v2, "showPosterUrl":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 583
    const-string v4, "shows_poster_uri"

    invoke-virtual {v3, v4, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 588
    :goto_0
    invoke-virtual {p3, p0}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getShowBannerUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v1

    .line 589
    .local v1, "showBannerUrl":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 590
    const-string v4, "shows_banner_uri"

    invoke-virtual {v3, v4, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 596
    :goto_1
    const-string v4, "broadcasters"

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    invoke-static {v5}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v5

    invoke-static {v3, v4, v5}, Lcom/google/android/videos/utils/DbUtils;->putStringList(Landroid/content/ContentValues;Ljava/lang/String;Ljava/util/List;)V

    .line 597
    return-object v3

    .line 585
    .end local v1    # "showBannerUrl":Ljava/lang/String;
    :cond_0
    const-string v4, "shows_poster_uri"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 586
    const-string v4, "shows_poster_synced"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_0

    .line 592
    .restart local v1    # "showBannerUrl":Ljava/lang/String;
    :cond_1
    const-string v4, "shows_banner_uri"

    invoke-virtual {v3, v4}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 593
    const-string v4, "shows_banner_synced"

    invoke-static {v6}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    goto :goto_1
.end method

.method private static checkAndMaybeSetImageSynced(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "imageSyncedColumn"    # Ljava/lang/String;
    .param p3, "imageUriColumn"    # Ljava/lang/String;
    .param p4, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p5, "imageTable"    # Ljava/lang/String;
    .param p6, "imageTableIdColumn"    # Ljava/lang/String;

    .prologue
    .line 639
    invoke-virtual {p0, p2}, Landroid/content/ContentValues;->getAsBoolean(Ljava/lang/String;)Ljava/lang/Boolean;

    move-result-object v11

    .line 640
    .local v11, "imageSynced":Ljava/lang/Boolean;
    if-eqz v11, :cond_0

    invoke-virtual {v11}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 653
    :goto_0
    return-void

    .line 643
    :cond_0
    sget-object v3, Lcom/google/android/videos/store/PurchaseStoreUtil;->NO_COLUMNS:[Ljava/lang/String;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    move-object/from16 v0, p6

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ? AND "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "image_uri"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, " = ?"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    const/4 v1, 0x2

    new-array v5, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    aput-object p1, v5, v1

    const/4 v1, 0x1

    invoke-virtual {p0, p3}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v5, v1

    const/4 v6, 0x0

    const/4 v7, 0x0

    const/4 v8, 0x0

    const-string v9, "1"

    move-object/from16 v1, p4

    move-object/from16 v2, p5

    invoke-virtual/range {v1 .. v9}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v10

    .line 647
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v10}, Landroid/database/Cursor;->moveToNext()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 648
    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, p2, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 651
    :cond_1
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :catchall_0
    move-exception v1

    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v1
.end method

.method public static checkMetadataAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 5
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
        }
    .end annotation

    .prologue
    const/16 v4, 0x13

    .line 345
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 346
    .local v0, "id":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 347
    :cond_0
    new-instance v2, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    const-string v3, "Asset has no id"

    invoke-direct {v2, v3}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 349
    :cond_1
    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_ASSET_TYPES:[I

    iget v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-static {v2, v3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->arrayContains([II)Z

    move-result v2

    if-nez v2, :cond_2

    .line 350
    new-instance v2, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Asset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has unknown type "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    iget v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 353
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 354
    .local v1, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    if-nez v1, :cond_3

    .line 355
    new-instance v2, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Asset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no metadata"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 358
    :cond_3
    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-eq v2, v4, :cond_4

    .line 360
    iget-object v2, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_4

    .line 361
    new-instance v2, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Asset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no title"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 365
    :cond_4
    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-eq v2, v4, :cond_5

    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v3, 0x14

    if-ne v2, v3, :cond_6

    .line 367
    :cond_5
    iget-object v2, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_6

    .line 368
    new-instance v2, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;

    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Asset "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, " has no sequence number"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;-><init>(Ljava/lang/String;)V

    throw v2

    .line 372
    :cond_6
    return-void
.end method

.method private static getBestPurchase([Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .locals 5
    .param p0, "purchases"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1, "previousBest"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .prologue
    .line 162
    if-nez p0, :cond_1

    const/4 v3, 0x0

    .line 163
    .local v3, "size":I
    :goto_0
    move-object v0, p1

    .line 164
    .local v0, "best":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v3, :cond_2

    .line 165
    aget-object v1, p0, v2

    .line 166
    .local v1, "current":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    invoke-static {v1, v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isCurrentPurchaseBetter(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 167
    move-object v0, v1

    .line 164
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 162
    .end local v0    # "best":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .end local v1    # "current":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .end local v2    # "i":I
    .end local v3    # "size":I
    :cond_1
    array-length v3, p0

    goto :goto_0

    .line 170
    .restart local v0    # "best":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .restart local v2    # "i":I
    .restart local v3    # "size":I
    :cond_2
    return-object v0
.end method

.method public static isAssetOfType(Lcom/google/wireless/android/video/magma/proto/AssetResource;I)Z
    .locals 1
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "type"    # I

    .prologue
    .line 334
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isCurrentPurchaseBetter(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Z
    .locals 12
    .param p0, "current"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    .param p1, "previous"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .prologue
    .line 174
    invoke-static {p0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isValidPurchase(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Z

    move-result v8

    if-nez v8, :cond_0

    .line 175
    const/4 v8, 0x0

    .line 215
    :goto_0
    return v8

    .line 177
    :cond_0
    if-nez p1, :cond_1

    .line 178
    const/4 v8, 0x1

    goto :goto_0

    .line 181
    :cond_1
    iget v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_2

    const/4 v0, 0x1

    .line 182
    .local v0, "currentIsActive":Z
    :goto_1
    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_3

    const/4 v3, 0x1

    .line 183
    .local v3, "previousIsActive":Z
    :goto_2
    if-eqz v0, :cond_4

    if-nez v3, :cond_4

    .line 184
    const/4 v8, 0x1

    goto :goto_0

    .line 181
    .end local v0    # "currentIsActive":Z
    .end local v3    # "previousIsActive":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 182
    .restart local v0    # "currentIsActive":Z
    :cond_3
    const/4 v3, 0x0

    goto :goto_2

    .line 185
    .restart local v3    # "previousIsActive":Z
    :cond_4
    if-nez v0, :cond_5

    if-eqz v3, :cond_5

    .line 186
    const/4 v8, 0x0

    goto :goto_0

    .line 189
    :cond_5
    iget v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_6

    const/4 v1, 0x1

    .line 190
    .local v1, "currentIsEst":Z
    :goto_3
    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_7

    const/4 v6, 0x1

    .line 191
    .local v6, "previousIsEst":Z
    :goto_4
    if-eqz v1, :cond_8

    if-nez v6, :cond_8

    .line 192
    const/4 v8, 0x1

    goto :goto_0

    .line 189
    .end local v1    # "currentIsEst":Z
    .end local v6    # "previousIsEst":Z
    :cond_6
    const/4 v1, 0x0

    goto :goto_3

    .line 190
    .restart local v1    # "currentIsEst":Z
    :cond_7
    const/4 v6, 0x0

    goto :goto_4

    .line 193
    .restart local v6    # "previousIsEst":Z
    :cond_8
    if-nez v1, :cond_9

    if-eqz v6, :cond_9

    .line 194
    const/4 v8, 0x0

    goto :goto_0

    .line 197
    :cond_9
    iget v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_a

    const/4 v2, 0x1

    .line 198
    .local v2, "currentIsHd":Z
    :goto_5
    iget v8, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    const/4 v9, 0x2

    if-ne v8, v9, :cond_b

    const/4 v7, 0x1

    .line 199
    .local v7, "previousIsHd":Z
    :goto_6
    if-eqz v2, :cond_c

    if-nez v7, :cond_c

    .line 200
    const/4 v8, 0x1

    goto :goto_0

    .line 197
    .end local v2    # "currentIsHd":Z
    .end local v7    # "previousIsHd":Z
    :cond_a
    const/4 v2, 0x0

    goto :goto_5

    .line 198
    .restart local v2    # "currentIsHd":Z
    :cond_b
    const/4 v7, 0x0

    goto :goto_6

    .line 201
    .restart local v7    # "previousIsHd":Z
    :cond_c
    if-nez v2, :cond_d

    if-eqz v7, :cond_d

    .line 202
    const/4 v8, 0x0

    goto :goto_0

    .line 205
    :cond_d
    if-nez v1, :cond_f

    .line 206
    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    iget-wide v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    sub-long v4, v8, v10

    .line 208
    .local v4, "difference":J
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-lez v8, :cond_e

    .line 209
    const/4 v8, 0x1

    goto :goto_0

    .line 210
    :cond_e
    const-wide/16 v8, 0x0

    cmp-long v8, v4, v8

    if-gez v8, :cond_f

    .line 211
    const/4 v8, 0x0

    goto :goto_0

    .line 215
    .end local v4    # "difference":J
    :cond_f
    iget-wide v8, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    iget-wide v10, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    cmp-long v8, v8, v10

    if-lez v8, :cond_10

    const/4 v8, 0x1

    goto :goto_0

    :cond_10
    const/4 v8, 0x0

    goto :goto_0
.end method

.method private static isValidParentId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Z
    .locals 1
    .param p0, "parentId"    # Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p1, "expectedType"    # I

    .prologue
    .line 118
    if-eqz p0, :cond_0

    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    iget v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    if-ne v0, p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static isValidPurchase(Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Z
    .locals 6
    .param p0, "purchase"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    .prologue
    const-wide/16 v4, 0x0

    const/4 v0, 0x1

    .line 219
    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_FORMAT_TYPES:[I

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    invoke-static {v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->arrayContains([II)Z

    move-result v1

    if-eqz v1, :cond_1

    sget-object v1, Lcom/google/android/videos/store/PurchaseStoreUtil;->VALID_PURCHASE_TYPES:[I

    iget v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    invoke-static {v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->arrayContains([II)Z

    move-result v1

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    if-ne v1, v0, :cond_0

    iget-wide v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    cmp-long v1, v2, v4

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    if-eqz v1, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static isValidUserLibraryAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Z
    .locals 3
    .param p0, "resource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v1, 0x0

    .line 98
    if-nez p0, :cond_1

    .line 113
    :cond_0
    :goto_0
    return v1

    .line 101
    :cond_1
    iget-object v0, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 102
    .local v0, "id":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    if-eqz v0, :cond_0

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 105
    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v2, :sswitch_data_0

    goto :goto_0

    .line 107
    :sswitch_0
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-nez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0

    .line 109
    :sswitch_1
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/16 v2, 0x13

    invoke-static {v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isValidParentId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Z

    move-result v1

    goto :goto_0

    .line 111
    :sswitch_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->parent:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    const/16 v2, 0x12

    invoke-static {v1, v2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->isValidParentId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;I)Z

    move-result v1

    goto :goto_0

    .line 105
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x13 -> :sswitch_2
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method public static loadMissingDataIntoShowContentValues(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 10
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v6, 0x0

    const/4 v5, 0x0

    .line 602
    const-string v0, "shows_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 603
    .local v9, "showId":Ljava/lang/String;
    const-string v0, "shows_full_sync_timestamp"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->containsKey(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 604
    const-string v1, "shows"

    sget-object v2, Lcom/google/android/videos/store/PurchaseStoreUtil$ShowFullSyncTimestampQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "shows_id = ?"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    aput-object v9, v4, v6

    move-object v0, p1

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 608
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 609
    const-string v0, "shows_full_sync_timestamp"

    const/4 v1, 0x0

    invoke-interface {v8, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 613
    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 616
    .end local v8    # "cursor":Landroid/database/Cursor;
    :cond_1
    const-string v2, "shows_poster_synced"

    const-string v3, "shows_poster_uri"

    const-string v5, "show_posters"

    const-string v6, "poster_show_id"

    move-object v0, p0

    move-object v1, v9

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkAndMaybeSetImageSynced(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 618
    const-string v2, "shows_banner_synced"

    const-string v3, "shows_banner_uri"

    const-string v5, "show_banners"

    const-string v6, "banner_show_id"

    move-object v0, p0

    move-object v1, v9

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkAndMaybeSetImageSynced(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 620
    return-void

    .line 613
    .restart local v8    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public static loadMissingDataIntoVideoContentValues(Landroid/content/ContentValues;Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 7
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 512
    const-string v0, "video_id"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->getAsString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 513
    .local v1, "videoId":Ljava/lang/String;
    const-string v2, "poster_synced"

    const-string v3, "poster_uri"

    const-string v5, "posters"

    const-string v6, "poster_video_id"

    move-object v0, p0

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkAndMaybeSetImageSynced(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 515
    const-string v2, "screenshot_synced"

    const-string v3, "screenshot_uri"

    const-string v5, "screenshots"

    const-string v6, "screenshot_video_id"

    move-object v0, p0

    move-object v4, p1

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStoreUtil;->checkAndMaybeSetImageSynced(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 517
    return-void
.end method

.method private static maybeUpdatePlaybackInfo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 6
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 308
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 309
    .local v0, "values":Landroid/content/ContentValues;
    invoke-static {p2, v0}, Lcom/google/android/videos/store/PurchaseStoreUtil;->addPlaybackContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/ContentValues;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 310
    const/4 v2, 0x4

    new-array v1, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    aput-object p1, v1, v2

    const/4 v2, 0x1

    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x2

    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    aput-object v3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->video:Lcom/google/wireless/android/video/magma/proto/VideoResource;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/VideoResource;->playback:Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;

    iget-wide v4, v3, Lcom/google/wireless/android/video/magma/proto/VideoResource$Playback;->stopTimestampMsec:J

    invoke-static {v4, v5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    .line 316
    .local v1, "whereArgs":[Ljava/lang/String;
    const-string v2, "purchased_assets"

    const-string v3, "account = ? AND asset_type = ? AND asset_id = ? AND (last_watched_timestamp IS NULL OR last_watched_timestamp < ?)"

    invoke-virtual {p0, v2, v0, v3, v1}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 319
    .end local v1    # "whereArgs":[Ljava/lang/String;
    :cond_0
    return-void
.end method

.method public static mergePurchasedAsset(Lcom/google/wireless/android/video/magma/proto/AssetResource;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 5
    .param p0, "newRes"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p1, "previouslyMerged"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v1, 0x0

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 134
    if-eqz p0, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v2, v2

    if-nez v2, :cond_1

    .line 150
    .end local p1    # "previouslyMerged":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_0
    :goto_0
    return-object p1

    .line 137
    .restart local p1    # "previouslyMerged":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_1
    if-nez p1, :cond_4

    .line 138
    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    invoke-static {v2, v1}, Lcom/google/android/videos/store/PurchaseStoreUtil;->getBestPurchase([Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v0

    .line 139
    .local v0, "bestPurchase":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    if-nez v0, :cond_2

    move-object p1, v1

    .line 140
    goto :goto_0

    .line 142
    :cond_2
    iget-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    array-length v1, v1

    if-eq v1, v3, :cond_3

    .line 143
    new-array v1, v3, [Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    aput-object v0, v1, v4

    iput-object v1, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    :cond_3
    move-object p1, p0

    .line 147
    goto :goto_0

    .line 149
    .end local v0    # "bestPurchase":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    :cond_4
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    iget-object v2, p0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    aget-object v3, v3, v4

    invoke-static {v2, v3}, Lcom/google/android/videos/store/PurchaseStoreUtil;->getBestPurchase([Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;)Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    move-result-object v2

    aput-object v2, v1, v4

    goto :goto_0
.end method

.method private static setVideoImageData(Landroid/content/ContentValues;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "values"    # Landroid/content/ContentValues;
    .param p1, "posterUrl"    # Ljava/lang/String;
    .param p2, "screenshotUrl"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x1

    .line 496
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 497
    const-string v0, "poster_uri"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 498
    const-string v0, "poster_synced"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 502
    :goto_0
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 503
    const-string v0, "screenshot_uri"

    invoke-virtual {p0, v0}, Landroid/content/ContentValues;->putNull(Ljava/lang/String;)V

    .line 504
    const-string v0, "screenshot_synced"

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 508
    :goto_1
    return-void

    .line 500
    :cond_0
    const-string v0, "poster_uri"

    invoke-virtual {p0, v0, p1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 506
    :cond_1
    const-string v0, "screenshot_uri"

    invoke-virtual {p0, v0, p2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_1
.end method

.method public static storePurchase(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)V
    .locals 18
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "purchasedAsset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "rootId"    # Ljava/lang/String;

    .prologue
    .line 234
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v12, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 235
    .local v2, "assetId":Ljava/lang/String;
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v3, v12, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    .line 236
    .local v3, "assetType":I
    move-object/from16 v0, p2

    iget-object v12, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->purchase:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;

    const/4 v13, 0x0

    aget-object v7, v12, v13

    .line 237
    .local v7, "purchase":Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;
    iget v9, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseType:I

    .line 238
    .local v9, "purchaseType":I
    iget v5, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseFormatType:I

    .line 239
    .local v5, "formatType":I
    iget v8, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseStatus:I

    .line 241
    .local v8, "purchaseStatus":I
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 242
    .local v10, "values":Landroid/content/ContentValues;
    const-string v12, "account"

    move-object/from16 v0, p1

    invoke-virtual {v10, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 243
    const-string v12, "asset_type"

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 244
    const-string v12, "asset_id"

    invoke-virtual {v10, v12, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 245
    const-string v12, "root_asset_id"

    move-object/from16 v0, p3

    invoke-virtual {v10, v12, v0}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v12, "purchase_type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 247
    const-string v12, "format_type"

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248
    const-string v12, "purchase_status"

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 249
    const-string v12, "purchase_timestamp_seconds"

    iget-wide v14, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 250
    const-string v13, "expiration_timestamp_seconds"

    iget-wide v14, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    const-wide/16 v16, 0x0

    cmp-long v12, v14, v16

    if-eqz v12, :cond_0

    iget-wide v14, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalExpirationTimestampSec:J

    invoke-static {v14, v15}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v12

    :goto_0
    invoke-virtual {v10, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 252
    const-string v13, "rental_short_timer_seconds"

    iget v12, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    if-eqz v12, :cond_1

    iget v12, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->rentalShortTimerSec:I

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    :goto_1
    invoke-virtual {v10, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 258
    const/4 v12, 0x4

    new-array v11, v12, [Ljava/lang/String;

    const/4 v12, 0x0

    aput-object p1, v11, v12

    const/4 v12, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object v2, v11, v12

    const/4 v12, 0x3

    iget-wide v14, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Purchase;->purchaseTimestampSec:J

    invoke-static {v14, v15}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    .line 264
    .local v11, "whereArgs":[Ljava/lang/String;
    const-string v12, "purchased_assets"

    const-string v13, "account = ? AND asset_type = ? AND asset_id = ? AND purchase_timestamp_seconds >= ?"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v10, v13, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 266
    .local v4, "count":I
    if-lez v4, :cond_2

    .line 267
    invoke-static/range {p0 .. p2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->maybeUpdatePlaybackInfo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 304
    :goto_2
    return-void

    .line 250
    .end local v4    # "count":I
    .end local v11    # "whereArgs":[Ljava/lang/String;
    :cond_0
    const/4 v12, 0x0

    goto :goto_0

    .line 252
    :cond_1
    const/4 v12, 0x0

    goto :goto_1

    .line 274
    .restart local v4    # "count":I
    .restart local v11    # "whereArgs":[Ljava/lang/String;
    :cond_2
    const/4 v12, 0x6

    if-eq v3, v12, :cond_3

    const/16 v12, 0x14

    if-ne v3, v12, :cond_4

    :cond_3
    const/4 v6, 0x1

    .line 275
    .local v6, "isVideo":Z
    :goto_3
    const-string v13, "hidden"

    if-eqz v6, :cond_5

    const/4 v12, 0x0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    :goto_4
    invoke-virtual {v10, v13, v12}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 277
    const/4 v12, 0x3

    new-array v11, v12, [Ljava/lang/String;

    .end local v11    # "whereArgs":[Ljava/lang/String;
    const/4 v12, 0x0

    aput-object p1, v11, v12

    const/4 v12, 0x1

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v13

    aput-object v13, v11, v12

    const/4 v12, 0x2

    aput-object v2, v11, v12

    .line 282
    .restart local v11    # "whereArgs":[Ljava/lang/String;
    const-string v12, "purchased_assets"

    const-string v13, "account = ? AND asset_type = ? AND asset_id = ?"

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v10, v13, v11}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v4

    .line 284
    if-lez v4, :cond_6

    .line 285
    invoke-static/range {p0 .. p2}, Lcom/google/android/videos/store/PurchaseStoreUtil;->maybeUpdatePlaybackInfo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_2

    .line 274
    .end local v6    # "isVideo":Z
    :cond_4
    const/4 v6, 0x0

    goto :goto_3

    .line 275
    .restart local v6    # "isVideo":Z
    :cond_5
    const/4 v12, 0x0

    goto :goto_4

    .line 291
    :cond_6
    if-eqz v6, :cond_7

    .line 292
    const-string v12, "last_playback_is_dirty"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 293
    const-string v12, "last_playback_start_timestamp"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 294
    const-string v12, "last_watched_timestamp"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 295
    const-string v12, "resume_timestamp"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 296
    const-string v12, "pinned"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 297
    const-string v12, "pinning_notification_active"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 298
    const-string v12, "is_new_notification_dismissed"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 299
    const-string v12, "have_subtitles"

    const/4 v13, 0x0

    invoke-static {v13}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v13

    invoke-virtual {v10, v12, v13}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 301
    :cond_7
    move-object/from16 v0, p2

    invoke-static {v0, v10}, Lcom/google/android/videos/store/PurchaseStoreUtil;->addPlaybackContentValues(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/ContentValues;)Z

    .line 303
    const-string v12, "purchased_assets"

    const/4 v13, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v12, v13, v10}, Landroid/database/sqlite/SQLiteDatabase;->insert(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    goto/16 :goto_2
.end method
