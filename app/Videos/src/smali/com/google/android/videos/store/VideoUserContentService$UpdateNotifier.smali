.class public Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "VideoUserContentService.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/welcome/Welcome$OnEligibilityChangedListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/VideoUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "UpdateNotifier"
.end annotation


# instance fields
.field private final context:Landroid/content/Context;

.field private final notifyIntent:Landroid/content/Intent;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/Database;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;

    .prologue
    .line 79
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 80
    iput-object p1, p0, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->context:Landroid/content/Context;

    .line 81
    invoke-virtual {p3, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 82
    invoke-virtual {p2, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 83
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.play.CONTENT_UPDATE"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v1, "Play.DataType"

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "Play.BackendId"

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->notifyIntent:Landroid/content/Intent;

    .line 88
    return-void
.end method


# virtual methods
.method public onEligibilityChanged()V
    .locals 0

    .prologue
    .line 112
    invoke-virtual {p0}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->update()V

    .line 113
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 97
    invoke-virtual {p0}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->update()V

    .line 98
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 102
    invoke-virtual {p0}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->update()V

    .line 103
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 107
    invoke-virtual {p0}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->update()V

    .line 108
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 92
    invoke-virtual {p0}, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->update()V

    .line 93
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;->notifyIntent:Landroid/content/Intent;

    invoke-virtual {v0, v1}, Landroid/content/Context;->sendBroadcast(Landroid/content/Intent;)V

    .line 118
    return-void
.end method
