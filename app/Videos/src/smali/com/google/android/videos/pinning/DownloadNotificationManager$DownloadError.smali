.class public Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;
.super Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadError"
.end annotation


# instance fields
.field public final downloadSize:J

.field public final drmErrorCode:Ljava/lang/Integer;

.field public final isRental:Z

.field public final pinningStatusReason:I


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V
    .locals 2
    .param p1, "videoData"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .prologue
    .line 118
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadSingle;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V

    .line 119
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->pinningStatusReason:I
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$800(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->pinningStatusReason:I

    .line 120
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->downloadSize:J

    .line 121
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->drmErrorCode:Ljava/lang/Integer;
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->drmErrorCode:Ljava/lang/Integer;

    .line 122
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->isRental:Z
    invoke-static {p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;->isRental:Z

    .line 123
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .param p2, "x1"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    .prologue
    .line 111
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    return-void
.end method
