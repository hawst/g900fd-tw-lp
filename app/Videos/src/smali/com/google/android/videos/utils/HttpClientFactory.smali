.class public final Lcom/google/android/videos/utils/HttpClientFactory;
.super Ljava/lang/Object;
.source "HttpClientFactory.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/HttpClientFactory$1;,
        Lcom/google/android/videos/utils/HttpClientFactory$GzipHttpEntityWrapper;,
        Lcom/google/android/videos/utils/HttpClientFactory$GzipResponseInterceptor;,
        Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;
    }
.end annotation


# direct methods
.method private static createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;
    .locals 5
    .param p0, "params"    # Lorg/apache/http/params/HttpParams;

    .prologue
    .line 84
    new-instance v0, Lorg/apache/http/conn/scheme/SchemeRegistry;

    invoke-direct {v0}, Lorg/apache/http/conn/scheme/SchemeRegistry;-><init>()V

    .line 85
    .local v0, "schemeRegistry":Lorg/apache/http/conn/scheme/SchemeRegistry;
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "http"

    invoke-static {}, Lorg/apache/http/conn/scheme/PlainSocketFactory;->getSocketFactory()Lorg/apache/http/conn/scheme/PlainSocketFactory;

    move-result-object v3

    const/16 v4, 0x50

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 86
    new-instance v1, Lorg/apache/http/conn/scheme/Scheme;

    const-string v2, "https"

    invoke-static {}, Lorg/apache/http/conn/ssl/SSLSocketFactory;->getSocketFactory()Lorg/apache/http/conn/ssl/SSLSocketFactory;

    move-result-object v3

    const/16 v4, 0x1bb

    invoke-direct {v1, v2, v3, v4}, Lorg/apache/http/conn/scheme/Scheme;-><init>(Ljava/lang/String;Lorg/apache/http/conn/scheme/SocketFactory;I)V

    invoke-virtual {v0, v1}, Lorg/apache/http/conn/scheme/SchemeRegistry;->register(Lorg/apache/http/conn/scheme/Scheme;)Lorg/apache/http/conn/scheme/Scheme;

    .line 87
    new-instance v1, Lorg/apache/http/conn/params/ConnPerRouteBean;

    const/16 v2, 0x10

    invoke-direct {v1, v2}, Lorg/apache/http/conn/params/ConnPerRouteBean;-><init>(I)V

    invoke-static {p0, v1}, Lorg/apache/http/conn/params/ConnManagerParams;->setMaxConnectionsPerRoute(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/params/ConnPerRoute;)V

    .line 88
    new-instance v1, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;

    invoke-direct {v1, p0, v0}, Lorg/apache/http/impl/conn/tsccm/ThreadSafeClientConnManager;-><init>(Lorg/apache/http/params/HttpParams;Lorg/apache/http/conn/scheme/SchemeRegistry;)V

    return-object v1
.end method

.method public static createCookieStoreHttpClient(Ljava/lang/String;Lorg/apache/http/client/CookieStore;)Lorg/apache/http/client/HttpClient;
    .locals 5
    .param p0, "userAgent"    # Ljava/lang/String;
    .param p1, "cookieStore"    # Lorg/apache/http/client/CookieStore;

    .prologue
    .line 61
    invoke-static {p0}, Lcom/google/android/videos/utils/HttpClientFactory;->createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    .line 62
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    invoke-static {v2}, Lcom/google/android/videos/utils/HttpClientFactory;->createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    .line 63
    .local v1, "manager":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v0, Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;

    invoke-direct {v0, p1, v1, v2}, Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;-><init>(Lorg/apache/http/client/CookieStore;Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 64
    .local v0, "httpClient":Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;
    new-instance v3, Lcom/google/android/videos/utils/HttpClientFactory$GzipResponseInterceptor;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/HttpClientFactory$GzipResponseInterceptor;-><init>(Lcom/google/android/videos/utils/HttpClientFactory$1;)V

    invoke-virtual {v0, v3}, Lcom/google/android/videos/utils/HttpClientFactory$CookieStoreHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 65
    return-object v0
.end method

.method public static createDefaultHttpClient(Ljava/lang/String;)Lorg/apache/http/client/HttpClient;
    .locals 5
    .param p0, "userAgent"    # Ljava/lang/String;

    .prologue
    .line 50
    invoke-static {p0}, Lcom/google/android/videos/utils/HttpClientFactory;->createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;

    move-result-object v2

    .line 51
    .local v2, "params":Lorg/apache/http/params/HttpParams;
    invoke-static {v2}, Lcom/google/android/videos/utils/HttpClientFactory;->createClientConnectionManager(Lorg/apache/http/params/HttpParams;)Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    .line 52
    .local v1, "manager":Lorg/apache/http/conn/ClientConnectionManager;
    new-instance v0, Lorg/apache/http/impl/client/DefaultHttpClient;

    invoke-direct {v0, v1, v2}, Lorg/apache/http/impl/client/DefaultHttpClient;-><init>(Lorg/apache/http/conn/ClientConnectionManager;Lorg/apache/http/params/HttpParams;)V

    .line 53
    .local v0, "httpClient":Lorg/apache/http/impl/client/DefaultHttpClient;
    new-instance v3, Lcom/google/android/videos/utils/HttpClientFactory$GzipResponseInterceptor;

    const/4 v4, 0x0

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/HttpClientFactory$GzipResponseInterceptor;-><init>(Lcom/google/android/videos/utils/HttpClientFactory$1;)V

    invoke-virtual {v0, v3}, Lorg/apache/http/impl/client/DefaultHttpClient;->addResponseInterceptor(Lorg/apache/http/HttpResponseInterceptor;)V

    .line 54
    return-object v0
.end method

.method private static createHttpParams(Ljava/lang/String;)Lorg/apache/http/params/HttpParams;
    .locals 3
    .param p0, "userAgent"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0x4e20

    .line 69
    new-instance v0, Lorg/apache/http/params/BasicHttpParams;

    invoke-direct {v0}, Lorg/apache/http/params/BasicHttpParams;-><init>()V

    .line 72
    .local v0, "params":Lorg/apache/http/params/HttpParams;
    const/4 v1, 0x0

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setStaleCheckingEnabled(Lorg/apache/http/params/HttpParams;Z)V

    .line 74
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setConnectionTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 75
    invoke-static {v0, v2}, Lorg/apache/http/params/HttpConnectionParams;->setSoTimeout(Lorg/apache/http/params/HttpParams;I)V

    .line 76
    const/16 v1, 0x2000

    invoke-static {v0, v1}, Lorg/apache/http/params/HttpConnectionParams;->setSocketBufferSize(Lorg/apache/http/params/HttpParams;I)V

    .line 77
    const/4 v1, 0x1

    invoke-static {v0, v1}, Lorg/apache/http/client/params/HttpClientParams;->setRedirecting(Lorg/apache/http/params/HttpParams;Z)V

    .line 79
    invoke-static {v0, p0}, Lorg/apache/http/params/HttpProtocolParams;->setUserAgent(Lorg/apache/http/params/HttpParams;Ljava/lang/String;)V

    .line 80
    return-object v0
.end method
