.class public Lcom/google/android/videos/ui/StatusBarDrawerLayout;
.super Lcom/google/android/play/drawer/PlayDrawerLayout;
.source "StatusBarDrawerLayout.java"


# static fields
.field public static final STATUS_BAR_BACKGROUND_COLOR:Landroid/util/Property;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Property",
            "<",
            "Lcom/google/android/videos/ui/StatusBarDrawerLayout;",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field private statusBarBackground:Landroid/graphics/drawable/ColorDrawable;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 59
    new-instance v0, Lcom/google/android/videos/ui/StatusBarDrawerLayout$1;

    sget-object v1, Ljava/lang/Integer;->TYPE:Ljava/lang/Class;

    const-string v2, "statusBarBackgroundColor"

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/ui/StatusBarDrawerLayout$1;-><init>(Ljava/lang/Class;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->STATUS_BAR_BACKGROUND_COLOR:Landroid/util/Property;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;-><init>(Landroid/content/Context;)V

    .line 20
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/drawer/PlayDrawerLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 24
    return-void
.end method


# virtual methods
.method public getStatusBarBackgroundColor()I
    .locals 1

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0}, Landroid/graphics/drawable/ColorDrawable;->getColor()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStatusBarBackground(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 43
    return-void
.end method

.method public setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V
    .locals 2
    .param p1, "bg"    # Landroid/graphics/drawable/Drawable;

    .prologue
    .line 28
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    if-ne p1, v0, :cond_0

    .line 30
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->invalidate()V

    .line 38
    :goto_0
    return-void

    .line 31
    :cond_0
    instance-of v0, p1, Landroid/graphics/drawable/ColorDrawable;

    if-eqz v0, :cond_1

    move-object v0, p1

    .line 32
    check-cast v0, Landroid/graphics/drawable/ColorDrawable;

    iput-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    .line 33
    invoke-super {p0, p1}, Lcom/google/android/play/drawer/PlayDrawerLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 34
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->invalidate()V

    goto :goto_0

    .line 36
    :cond_1
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    const-string v1, "Only ColorDrawables are supported."

    invoke-direct {v0, v1}, Ljava/lang/UnsupportedOperationException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method public setStatusBarBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    if-nez v0, :cond_0

    .line 48
    new-instance v0, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v0, p1}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->setStatusBarBackground(Landroid/graphics/drawable/Drawable;)V

    .line 53
    :goto_0
    return-void

    .line 50
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->statusBarBackground:Landroid/graphics/drawable/ColorDrawable;

    invoke-virtual {v0, p1}, Landroid/graphics/drawable/ColorDrawable;->setColor(I)V

    .line 51
    invoke-virtual {p0}, Lcom/google/android/videos/ui/StatusBarDrawerLayout;->invalidate()V

    goto :goto_0
.end method
