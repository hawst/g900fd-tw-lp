.class Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;
.super Ljava/lang/Object;
.source "PurchaseStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "GetPurchasesRunnable"
.end annotation


# instance fields
.field protected final allowUnratedContent:Z

.field protected final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field protected final contentRestrictionsEnabled:Z

.field protected final db:Landroid/database/sqlite/SQLiteDatabase;

.field protected final inAllowedRatingIdsSql:Ljava/lang/String;

.field protected final request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;


# direct methods
.method public constructor <init>(Landroid/database/sqlite/SQLiteDatabase;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;ZZLjava/lang/String;)V
    .locals 0
    .param p1, "db"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p2, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p4, "contentRestrictionsEnabled"    # Z
    .param p5, "allowUnratedContent"    # Z
    .param p6, "inAllowedRatingIdsSql"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/database/sqlite/SQLiteDatabase;",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;ZZ",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 246
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 247
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->db:Landroid/database/sqlite/SQLiteDatabase;

    .line 248
    iput-object p2, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 249
    iput-object p3, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->callback:Lcom/google/android/videos/async/Callback;

    .line 250
    iput-boolean p4, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->contentRestrictionsEnabled:Z

    .line 251
    iput-boolean p5, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->allowUnratedContent:Z

    .line 252
    iput-object p6, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->inAllowedRatingIdsSql:Ljava/lang/String;

    .line 253
    return-void
.end method

.method private getCursorRecursively(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    .locals 9
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    .line 265
    if-nez p1, :cond_2

    const/4 v1, 0x0

    .line 266
    .local v1, "cursor":Landroid/database/Cursor;
    :goto_0
    if-eqz v1, :cond_0

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->subRequestCreator:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$000(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    move-result-object v7

    if-nez v7, :cond_3

    :cond_0
    move-object v4, v1

    .line 306
    :cond_1
    return-object v4

    .line 265
    .end local v1    # "cursor":Landroid/database/Cursor;
    :cond_2
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->queryOne(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v1

    goto :goto_0

    .line 271
    .restart local v1    # "cursor":Landroid/database/Cursor;
    :cond_3
    const/4 v3, 0x0

    .line 272
    .local v3, "nestedCursor":Lcom/google/android/videos/store/NestedCursor;
    const/4 v6, 0x0

    .line 274
    .local v6, "subCursors":[Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v0

    .line 275
    .local v0, "count":I
    new-array v6, v0, [Landroid/database/Cursor;

    .line 276
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    if-ge v2, v0, :cond_8

    .line 277
    invoke-interface {v1, v2}, Landroid/database/Cursor;->moveToPosition(I)Z

    .line 278
    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->subRequestCreator:Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$000(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;

    move-result-object v7

    invoke-interface {v7, v1}, Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;->createSubRequest(Landroid/database/Cursor;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v7

    invoke-direct {p0, v7}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->getCursorRecursively(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_1

    move-result-object v5

    .line 283
    .local v5, "subCursor":Landroid/database/Cursor;
    if-eqz v5, :cond_4

    :try_start_1
    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v7

    if-eqz v7, :cond_4

    .line 284
    aput-object v5, v6, v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 285
    const/4 v5, 0x0

    .line 288
    :cond_4
    if-eqz v5, :cond_5

    .line 289
    :try_start_2
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    .line 276
    :cond_5
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 288
    :catchall_0
    move-exception v7

    if-eqz v5, :cond_6

    .line 289
    invoke-interface {v5}, Landroid/database/Cursor;->close()V

    :cond_6
    throw v7
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    .line 295
    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v5    # "subCursor":Landroid/database/Cursor;
    :catchall_1
    move-exception v7

    if-nez v3, :cond_a

    .line 296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 297
    if-eqz v6, :cond_a

    .line 298
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    array-length v8, v6

    if-ge v2, v8, :cond_a

    .line 299
    aget-object v8, v6, v2

    if-eqz v8, :cond_7

    .line 300
    aget-object v8, v6, v2

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 293
    .restart local v0    # "count":I
    :cond_8
    :try_start_3
    new-instance v4, Lcom/google/android/videos/store/NestedCursor;

    invoke-direct {v4, v1, v6}, Lcom/google/android/videos/store/NestedCursor;-><init>(Landroid/database/Cursor;[Landroid/database/Cursor;)V
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_1

    .line 295
    .end local v3    # "nestedCursor":Lcom/google/android/videos/store/NestedCursor;
    .local v4, "nestedCursor":Lcom/google/android/videos/store/NestedCursor;
    if-nez v4, :cond_1

    .line 296
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 297
    if-eqz v6, :cond_1

    .line 298
    const/4 v2, 0x0

    :goto_3
    array-length v7, v6

    if-ge v2, v7, :cond_1

    .line 299
    aget-object v7, v6, v2

    if-eqz v7, :cond_9

    .line 300
    aget-object v7, v6, v2

    invoke-interface {v7}, Landroid/database/Cursor;->close()V

    .line 298
    :cond_9
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .end local v0    # "count":I
    .end local v2    # "i":I
    .end local v4    # "nestedCursor":Lcom/google/android/videos/store/NestedCursor;
    .restart local v3    # "nestedCursor":Lcom/google/android/videos/store/NestedCursor;
    :cond_a
    throw v7
.end method

.method private queryOne(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;
    .locals 10
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .prologue
    const/4 v5, 0x0

    .line 310
    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->distinct:Z
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$100(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Z

    move-result v0

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->tables:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$200(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v1

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->columns:[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$300(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereClause:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$400(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->ratingIdColumn:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$500(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v4

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->restrictWhereClause(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->groupClause:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$600(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v4

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->orderClause:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$700(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Ljava/lang/String;

    move-result-object v6

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->limit:I
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$800(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)I

    move-result v7

    if-lez v7, :cond_0

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->limit:I
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$800(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)I

    move-result v7

    invoke-static {v7}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v7

    :goto_0
    invoke-static/range {v0 .. v7}, Landroid/database/sqlite/SQLiteQueryBuilder;->buildQueryString(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 314
    .local v9, "sql":Ljava/lang/String;
    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->whereArgs:[Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$900(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)[Ljava/lang/String;

    move-result-object v0

    # getter for: Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->cancellationSignalHolder:Lcom/google/android/videos/store/CancellationSignalHolder;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;->access$1000(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/CancellationSignalHolder;

    move-result-object v1

    invoke-virtual {p0, v9, v0, v1}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->rawQuery(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;

    move-result-object v8

    .line 315
    .local v8, "cursor":Landroid/database/Cursor;
    invoke-static {v8}, Lcom/google/android/videos/utils/DbUtils;->copyAndClose(Landroid/database/Cursor;)Lcom/google/android/videos/store/MemoryCursor;

    move-result-object v0

    return-object v0

    .end local v8    # "cursor":Landroid/database/Cursor;
    .end local v9    # "sql":Ljava/lang/String;
    :cond_0
    move-object v7, v5

    .line 310
    goto :goto_0
.end method

.method private restrictWhereClause(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p1, "whereClause"    # Ljava/lang/String;
    .param p2, "ratingIdColumn"    # Ljava/lang/String;

    .prologue
    .line 319
    iget-boolean v0, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->contentRestrictionsEnabled:Z

    if-eqz v0, :cond_0

    if-nez p2, :cond_1

    .line 322
    .end local p1    # "whereClause":Ljava/lang/String;
    :cond_0
    :goto_0
    return-object p1

    .restart local p1    # "whereClause":Ljava/lang/String;
    :cond_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-boolean v0, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->allowUnratedContent:Z

    if-eqz v0, :cond_2

    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " IS NULL OR "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    :goto_1
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->inAllowedRatingIdsSql:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p1

    goto :goto_0

    :cond_2
    const-string v0, ""

    goto :goto_1
.end method


# virtual methods
.method protected final getCursor()Landroid/database/Cursor;
    .locals 1

    .prologue
    .line 261
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    invoke-direct {p0, v0}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->getCursorRecursively(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method protected rawQuery(Ljava/lang/String;[Ljava/lang/String;Lcom/google/android/videos/store/CancellationSignalHolder;)Landroid/database/Cursor;
    .locals 1
    .param p1, "sql"    # Ljava/lang/String;
    .param p2, "whereArgs"    # [Ljava/lang/String;
    .param p3, "holder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 332
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->db:Landroid/database/sqlite/SQLiteDatabase;

    invoke-virtual {v0, p1, p2}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public run()V
    .locals 3

    .prologue
    .line 257
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->request:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    invoke-virtual {p0}, Lcom/google/android/videos/store/PurchaseStore$GetPurchasesRunnable;->getCursor()Landroid/database/Cursor;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 258
    return-void
.end method
