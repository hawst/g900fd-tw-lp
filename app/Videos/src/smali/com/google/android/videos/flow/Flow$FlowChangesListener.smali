.class interface abstract Lcom/google/android/videos/flow/Flow$FlowChangesListener;
.super Ljava/lang/Object;
.source "Flow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/flow/Flow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "FlowChangesListener"
.end annotation


# virtual methods
.method public abstract onDataSetChanged(Lcom/google/android/videos/flow/Flow;)V
.end method

.method public abstract onItemsChanged(Lcom/google/android/videos/flow/Flow;II)V
.end method

.method public abstract onItemsInserted(Lcom/google/android/videos/flow/Flow;II)V
.end method

.method public abstract onItemsRemoved(Lcom/google/android/videos/flow/Flow;II)V
.end method
