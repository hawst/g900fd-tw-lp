.class Lcom/google/android/videos/player/DirectorInitializer$14;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->getOfflineFileSizeCallback(J)Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/io/File;",
        "Ljava/lang/Long;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;

.field final synthetic val$expectedSize:J


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;J)V
    .locals 0

    .prologue
    .line 923
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iput-wide p2, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->val$expectedSize:J

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/io/File;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "request"    # Ljava/io/File;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 936
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Could not get offline file length: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 938
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0x14

    const v4, 0x7f0b0167

    move-object v5, p2

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->access$4100(Lcom/google/android/videos/player/DirectorInitializer;IZIILjava/lang/Exception;)V

    .line 940
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 923
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$14;->onError(Ljava/io/File;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/io/File;Ljava/lang/Long;)V
    .locals 6
    .param p1, "request"    # Ljava/io/File;
    .param p2, "downloadLength"    # Ljava/lang/Long;

    .prologue
    .line 926
    iget-wide v0, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->val$expectedSize:J

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v2

    cmp-long v0, v0, v2

    if-eqz v0, :cond_0

    .line 927
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    const/4 v1, 0x2

    const/4 v2, 0x1

    const/16 v3, 0x15

    const v4, 0x7f0b0168

    const/4 v5, 0x0

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onInitializationError(IZIILjava/lang/Exception;)V
    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/player/DirectorInitializer;->access$4100(Lcom/google/android/videos/player/DirectorInitializer;IZIILjava/lang/Exception;)V

    .line 932
    :goto_0
    return-void

    .line 930
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$14;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V

    goto :goto_0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 923
    check-cast p1, Ljava/io/File;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Long;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$14;->onResponse(Ljava/io/File;Ljava/lang/Long;)V

    return-void
.end method
