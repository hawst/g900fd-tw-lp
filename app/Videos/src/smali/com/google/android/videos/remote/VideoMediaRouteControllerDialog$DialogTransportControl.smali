.class final Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;
.super Lcom/google/android/videos/remote/TransportControl;
.source "VideoMediaRouteControllerDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "DialogTransportControl"
.end annotation


# instance fields
.field private currentTime:I

.field private duration:I

.field private prevState:I

.field final synthetic this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 4
    .param p2, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;

    .prologue
    const/4 v3, 0x0

    .line 148
    iput-object p1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    .line 149
    invoke-direct {p0, p2}, Lcom/google/android/videos/remote/TransportControl;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 150
    invoke-virtual {p2}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 151
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-nez v0, :cond_0

    move v2, v3

    :goto_0
    iput v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->currentTime:I

    .line 152
    invoke-virtual {p2}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v1

    .line 153
    .local v1, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-nez v1, :cond_1

    :goto_1
    iput v3, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->duration:I

    .line 154
    return-void

    .line 151
    .end local v1    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :cond_0
    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    goto :goto_0

    .line 153
    .restart local v1    # "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    :cond_1
    iget v3, v1, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    goto :goto_1
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;
    .param p2, "x1"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p3, "x2"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$1;

    .prologue
    .line 143
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;-><init>(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;Lcom/google/android/videos/remote/RemoteTracker;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->showWatchScreen()V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;

    .prologue
    .line 143
    invoke-direct {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->togglePlayback()V

    return-void
.end method

.method private showWatchScreen()V
    .locals 6

    .prologue
    .line 206
    iget-object v4, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v2

    .line 207
    .local v2, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v2, :cond_1

    iget v4, v2, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v5, 0x3

    if-eq v4, v5, :cond_0

    iget v4, v2, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_1

    .line 209
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->getListeners()Ljava/util/List;

    move-result-object v1

    .line 210
    .local v1, "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 211
    .local v3, "size":I
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_1

    .line 212
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/remote/TransportControl$Listener;

    invoke-interface {v4}, Lcom/google/android/videos/remote/TransportControl$Listener;->onSelect()V

    .line 211
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 215
    .end local v0    # "i":I
    .end local v1    # "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    .end local v3    # "size":I
    :cond_1
    return-void
.end method

.method private togglePlayback()V
    .locals 6

    .prologue
    .line 189
    iget-object v4, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v4}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v2

    .line 190
    .local v2, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v2, :cond_2

    .line 191
    invoke-virtual {p0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->getListeners()Ljava/util/List;

    move-result-object v1

    .line 192
    .local v1, "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v3

    .line 193
    .local v3, "size":I
    iget v4, v2, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_0

    iget v4, v2, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_1

    .line 194
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_2

    .line 195
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/remote/TransportControl$Listener;

    invoke-interface {v4}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPause()V

    .line 194
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 198
    .end local v0    # "i":I
    :cond_1
    const/4 v0, 0x0

    .restart local v0    # "i":I
    :goto_1
    if-ge v0, v3, :cond_2

    .line 199
    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/remote/TransportControl$Listener;

    invoke-interface {v4}, Lcom/google/android/videos/remote/TransportControl$Listener;->onPlay()V

    .line 198
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 203
    .end local v0    # "i":I
    .end local v1    # "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    .end local v3    # "size":I
    :cond_2
    return-void
.end method


# virtual methods
.method public onPlayerStateChanged()V
    .locals 4

    .prologue
    .line 158
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 159
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-eqz v0, :cond_0

    .line 160
    iget v1, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    iput v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->currentTime:I

    .line 161
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    iget v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->currentTime:I

    iget v3, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->duration:I

    # invokes: Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlaybackTime(II)V
    invoke-static {v1, v2, v3}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->access$400(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;II)V

    .line 162
    iget v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->prevState:I

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    if-eq v1, v2, :cond_0

    .line 163
    iget v1, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    iput v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->prevState:I

    .line 164
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    iget v2, v0, Lcom/google/android/videos/remote/PlayerState;->state:I

    # invokes: Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlayPauseIcon(I)V
    invoke-static {v1, v2}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->access$500(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;I)V

    .line 167
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->getMediaControlView()Landroid/view/View;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingAnyVideo()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 169
    return-void

    .line 167
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method

.method public onPosterBitmapChanged()V
    .locals 1

    .prologue
    .line 185
    iget-object v0, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    # invokes: Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updateThumbnailImage()V
    invoke-static {v0}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->access$700(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)V

    .line 186
    return-void
.end method

.method public onVideoInfoChanged()V
    .locals 4

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getVideoInfo()Lcom/google/android/videos/remote/RemoteVideoInfo;

    move-result-object v0

    .line 174
    .local v0, "videoInfo":Lcom/google/android/videos/remote/RemoteVideoInfo;
    if-eqz v0, :cond_0

    .line 175
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    # getter for: Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->playbackTitleText:Landroid/widget/TextView;
    invoke-static {v1}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->access$600(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;)Landroid/widget/TextView;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->videoTitle:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 176
    iget v1, v0, Lcom/google/android/videos/remote/RemoteVideoInfo;->durationMillis:I

    iput v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->duration:I

    .line 177
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    iget v2, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->currentTime:I

    iget v3, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->duration:I

    # invokes: Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->updatePlaybackTime(II)V
    invoke-static {v1, v2, v3}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->access$400(Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;II)V

    .line 179
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->this$0:Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog;->getMediaControlView()Landroid/view/View;

    move-result-object v2

    iget-object v1, p0, Lcom/google/android/videos/remote/VideoMediaRouteControllerDialog$DialogTransportControl;->remoteTracker:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->isPlayingAnyVideo()Z

    move-result v1

    if-eqz v1, :cond_1

    const/4 v1, 0x0

    :goto_0
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 181
    return-void

    .line 179
    :cond_1
    const/16 v1, 0x8

    goto :goto_0
.end method
