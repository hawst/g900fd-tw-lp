.class final Lcom/google/android/videos/store/NestedCursor$SubCursorView;
.super Landroid/database/CursorWrapper;
.source "NestedCursor.java"

# interfaces
.implements Lcom/google/android/videos/store/MasterSubCursor;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/NestedCursor;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "SubCursorView"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/NestedCursor;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/NestedCursor;Landroid/database/Cursor;)V
    .locals 0
    .param p2, "subCursor"    # Landroid/database/Cursor;

    .prologue
    .line 102
    iput-object p1, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    .line 103
    invoke-direct {p0, p2}, Landroid/database/CursorWrapper;-><init>(Landroid/database/Cursor;)V

    .line 104
    return-void
.end method


# virtual methods
.method public close()V
    .locals 0

    .prologue
    .line 211
    return-void
.end method

.method public deactivate()V
    .locals 0

    .prologue
    .line 200
    return-void
.end method

.method public getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;
    .locals 0

    .prologue
    .line 172
    return-object p0
.end method

.method public getMasterInt(I)I
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/store/NestedCursor;->getMasterInt(I)I

    move-result v0

    return v0
.end method

.method public getMasterLong(I)J
    .locals 2
    .param p1, "columnIndex"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/store/NestedCursor;->getMasterLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public getMasterString(I)Ljava/lang/String;
    .locals 1
    .param p1, "columnIndex"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/store/NestedCursor;->getMasterString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getNonEmptySubRangeCount()I
    .locals 1

    .prologue
    .line 167
    const/4 v0, 0x1

    return v0
.end method

.method public move(I)Z
    .locals 1
    .param p1, "offset"    # I

    .prologue
    .line 140
    invoke-virtual {p0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->getPosition()I

    move-result v0

    add-int/2addr v0, p1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToFirst()Z
    .locals 1

    .prologue
    .line 145
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToLast()Z
    .locals 1

    .prologue
    .line 150
    invoke-virtual {p0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->getCount()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToNext()Z
    .locals 1

    .prologue
    .line 155
    invoke-virtual {p0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public moveToPosition(I)Z
    .locals 6
    .param p1, "position"    # I

    .prologue
    .line 110
    invoke-virtual {p0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->getCount()I

    move-result v0

    .line 111
    .local v0, "count":I
    const/4 v4, 0x0

    .line 112
    .local v4, "valid":Z
    if-gez p1, :cond_2

    .line 113
    const/4 p1, -0x1

    .line 121
    :goto_0
    if-nez v4, :cond_0

    .line 122
    invoke-super {p0, p1}, Landroid/database/CursorWrapper;->moveToPosition(I)Z

    .line 125
    :cond_0
    const/4 v3, 0x0

    .line 126
    .local v3, "overallPosition":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_1
    iget-object v5, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    # getter for: Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;
    invoke-static {v5}, Lcom/google/android/videos/store/NestedCursor;->access$100(Lcom/google/android/videos/store/NestedCursor;)[Landroid/database/Cursor;

    move-result-object v5

    array-length v5, v5

    if-ge v1, v5, :cond_1

    .line 127
    iget-object v5, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    # getter for: Lcom/google/android/videos/store/NestedCursor;->subCursorViews:[Lcom/google/android/videos/store/NestedCursor$SubCursorView;
    invoke-static {v5}, Lcom/google/android/videos/store/NestedCursor;->access$200(Lcom/google/android/videos/store/NestedCursor;)[Lcom/google/android/videos/store/NestedCursor$SubCursorView;

    move-result-object v5

    aget-object v5, v5, v1

    if-ne v5, p0, :cond_4

    .line 128
    add-int/2addr v3, p1

    .line 134
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    invoke-virtual {v5, v3}, Lcom/google/android/videos/store/NestedCursor;->moveToPosition(I)Z

    move-result v2

    .line 135
    .local v2, "moveSuccessful":Z
    if-eqz v4, :cond_6

    if-eqz v2, :cond_6

    const/4 v5, 0x1

    :goto_2
    return v5

    .line 114
    .end local v1    # "i":I
    .end local v2    # "moveSuccessful":Z
    .end local v3    # "overallPosition":I
    :cond_2
    if-lt p1, v0, :cond_3

    .line 115
    move p1, v0

    goto :goto_0

    .line 117
    :cond_3
    const/4 v4, 0x1

    goto :goto_0

    .line 130
    .restart local v1    # "i":I
    .restart local v3    # "overallPosition":I
    :cond_4
    iget-object v5, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    # getter for: Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;
    invoke-static {v5}, Lcom/google/android/videos/store/NestedCursor;->access$100(Lcom/google/android/videos/store/NestedCursor;)[Landroid/database/Cursor;

    move-result-object v5

    aget-object v5, v5, v1

    if-eqz v5, :cond_5

    .line 131
    iget-object v5, p0, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->this$0:Lcom/google/android/videos/store/NestedCursor;

    # getter for: Lcom/google/android/videos/store/NestedCursor;->subCursors:[Landroid/database/Cursor;
    invoke-static {v5}, Lcom/google/android/videos/store/NestedCursor;->access$100(Lcom/google/android/videos/store/NestedCursor;)[Landroid/database/Cursor;

    move-result-object v5

    aget-object v5, v5, v1

    invoke-interface {v5}, Landroid/database/Cursor;->getCount()I

    move-result v5

    add-int/2addr v3, v5

    .line 126
    :cond_5
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 135
    .restart local v2    # "moveSuccessful":Z
    :cond_6
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public moveToPrevious()Z
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->getPosition()I

    move-result v0

    add-int/lit8 v0, v0, -0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/store/NestedCursor$SubCursorView;->moveToPosition(I)Z

    move-result v0

    return v0
.end method

.method public requery()Z
    .locals 1

    .prologue
    .line 205
    const/4 v0, 0x0

    return v0
.end method
