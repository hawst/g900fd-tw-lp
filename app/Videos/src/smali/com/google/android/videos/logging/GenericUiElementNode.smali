.class public Lcom/google/android/videos/logging/GenericUiElementNode;
.super Ljava/lang/Object;
.source "GenericUiElementNode.java"

# interfaces
.implements Lcom/google/android/videos/logging/UiElementNode;


# instance fields
.field private final parentNode:Lcom/google/android/videos/logging/UiElementNode;

.field private final uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;


# direct methods
.method public constructor <init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 1
    .param p1, "uiElementType"    # I
    .param p2, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p3, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiElementNode;

    iput-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->parentNode:Lcom/google/android/videos/logging/UiElementNode;

    .line 23
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 24
    new-instance v0, Lcom/google/android/videos/logging/UiElementWrapper;

    invoke-direct {v0}, Lcom/google/android/videos/logging/UiElementWrapper;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    .line 25
    iget-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    iget-object v0, v0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 26
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 3
    .param p1, "childNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 40
    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/logging/UiElementWrapper;

    .line 41
    .local v1, "newChild":Lcom/google/android/videos/logging/UiElementWrapper;
    iget-object v2, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-virtual {v2, p0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->addChild(Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiElementWrapper;)Z

    move-result v0

    .line 43
    .local v0, "childAdded":Z
    if-nez v0, :cond_0

    iget-object v2, v1, Lcom/google/android/videos/logging/UiElementWrapper;->children:Ljava/util/ArrayList;

    invoke-virtual {v2}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    .line 44
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/logging/GenericUiElementNode;->getParentNode()Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v2

    invoke-interface {v2, p0}, Lcom/google/android/videos/logging/UiElementNode;->childImpression(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 46
    :cond_1
    return-void
.end method

.method public getParentNode()Lcom/google/android/videos/logging/UiElementNode;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->parentNode:Lcom/google/android/videos/logging/UiElementNode;

    return-object v0
.end method

.method public getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/logging/GenericUiElementNode;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    return-object v0
.end method
