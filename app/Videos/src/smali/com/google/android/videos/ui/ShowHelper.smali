.class public Lcom/google/android/videos/ui/ShowHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "ShowHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/flow/ViewHolderCreator;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ShowHelper$1;,
        Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;,
        Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;,
        Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/Database$BaseListener;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/google/android/videos/ui/CursorHelper$Listener;",
        "Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Lcom/google/android/videos/activity/ShowActivity;

.field private broadcasters:Ljava/lang/String;

.field private currentSeasonIndex:I

.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final episodeBinder:Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;

.field private final episodeClickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

.field private episodeIdToSelect:Ljava/lang/String;

.field private final episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

.field private final episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final flowTopViewsCount:I

.field private headerBackgroundColor:I

.field private final headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private final mainView:Landroid/support/v7/widget/RecyclerView;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private ownedCompleteShow:Z

.field private pendingBitmapsUpdatedNotification:Z

.field private pendingEpisodesCursor:Z

.field private final pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private pinningErrorDialog:Z

.field private final playCountry:Ljava/lang/String;

.field private final progressBar:Landroid/view/View;

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final seasonDataList:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/ui/ShowHelper$SeasonData;",
            ">;"
        }
    .end annotation
.end field

.field private final seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow",
            "<",
            "Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;",
            "Lcom/google/android/videos/adapter/EpisodesDataSource;",
            ">;"
        }
    .end annotation
.end field

.field private seasonIdToSelect:Ljava/lang/String;

.field private final showId:Ljava/lang/String;

.field private startDownload:Z

.field private final suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

.field private final suggestionsEnabled:Z

.field private title:Ljava/lang/String;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/ShowActivity;Landroid/support/v7/widget/RecyclerView;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;Lcom/google/android/videos/Config;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 13
    .param p1, "activity"    # Lcom/google/android/videos/activity/ShowActivity;
    .param p2, "mainView"    # Landroid/support/v7/widget/RecyclerView;
    .param p3, "progressBar"    # Landroid/view/View;
    .param p4, "episodesCursorHelper"    # Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;
    .param p5, "config"    # Lcom/google/android/videos/Config;
    .param p6, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p7, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p8, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p9, "bitmapRequesters"    # Lcom/google/android/videos/bitmap/BitmapRequesters;
    .param p10, "apiRequesters"    # Lcom/google/android/videos/api/ApiRequesters;
    .param p11, "pinHelper"    # Lcom/google/android/videos/ui/PinHelper;
    .param p12, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p13, "suggestionOverflowMenuHelper"    # Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .param p14, "account"    # Ljava/lang/String;
    .param p15, "showId"    # Ljava/lang/String;
    .param p16, "playCountry"    # Ljava/lang/String;
    .param p17, "startDownload"    # Z
    .param p18, "pinningErrorDialog"    # Z
    .param p19, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p20, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p21, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 128
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/activity/ShowActivity;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    .line 129
    invoke-static/range {p5 .. p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 130
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    .line 131
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 132
    invoke-static/range {p8 .. p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 133
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 134
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 135
    invoke-static/range {p11 .. p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/PinHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 136
    invoke-static/range {p12 .. p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/logging/EventLogger;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 137
    invoke-static/range {p21 .. p21}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 138
    invoke-static/range {p13 .. p13}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    .line 139
    invoke-static/range {p14 .. p14}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;

    .line 140
    invoke-static/range {p15 .. p15}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;

    .line 141
    invoke-static/range {p16 .. p16}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->playCountry:Ljava/lang/String;

    .line 142
    move/from16 v0, p17

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->startDownload:Z

    .line 143
    move/from16 v0, p18

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->pinningErrorDialog:Z

    .line 144
    move-object/from16 v0, p20

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 145
    iput-object p2, p0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    .line 146
    move-object/from16 v0, p3

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->progressBar:Landroid/view/View;

    .line 147
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 149
    new-instance v2, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-direct {v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .line 150
    invoke-interface/range {p5 .. p5}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    .line 151
    iget-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    if-nez v2, :cond_0

    const/4 v2, 0x0

    :goto_0
    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 157
    new-instance v2, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;

    const/4 v3, 0x0

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/ui/ShowHelper$EpisodeClickListener;-><init>(Lcom/google/android/videos/ui/ShowHelper;Lcom/google/android/videos/ui/ShowHelper$1;)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodeClickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

    .line 158
    new-instance v2, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;

    const/4 v3, 0x2

    move-object/from16 v0, p8

    invoke-virtual {v0, v3}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    const/4 v4, 0x3

    move-object/from16 v0, p8

    invoke-virtual {v0, v4}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-interface/range {p9 .. p9}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-interface/range {p5 .. p5}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v7

    new-instance v8, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v12, 0xa

    move-object/from16 v0, p19

    move-object/from16 v1, p21

    invoke-direct {v8, v12, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;ZLcom/google/android/videos/logging/UiElementNode;)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodeBinder:Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;

    .line 166
    new-instance v11, Lcom/google/android/videos/flow/SingleViewFlow;

    const v2, 0x7f040018

    invoke-direct {v11, v2, p1}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    .line 167
    .local v11, "spacerFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    new-instance v2, Lcom/google/android/videos/flow/SingleViewFlow;

    const v3, 0x7f0400c0

    invoke-direct {v2, v3, p0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 168
    new-instance v2, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const/4 v3, 0x0

    const/4 v4, 0x0

    const v5, 0x7f040023

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper;->episodeBinder:Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$DetailsViewBinder;

    iget-object v7, p0, Lcom/google/android/videos/ui/ShowHelper;->episodeClickListener:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;

    const/4 v8, -0x1

    invoke-direct/range {v2 .. v8}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    .line 175
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setVisible(Z)V

    .line 177
    new-instance v9, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v2, 0x3

    new-array v2, v2, [Lcom/google/android/videos/flow/Flow;

    const/4 v3, 0x0

    aput-object v11, v2, v3

    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v4, v2, v3

    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    aput-object v4, v2, v3

    invoke-direct {v9, v2}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    .line 178
    .local v9, "flow":Lcom/google/android/videos/flow/Flow;
    const/4 v2, 0x2

    iput v2, p0, Lcom/google/android/videos/ui/ShowHelper;->flowTopViewsCount:I

    .line 180
    new-instance v10, Lcom/google/android/videos/flow/FlowAdapter;

    invoke-direct {v10, v9}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 181
    .local v10, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v2, 0x1

    invoke-virtual {v10, v2}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 182
    new-instance v2, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v3, "ShowActivity"

    invoke-direct {v2, v3}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {p2, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 183
    new-instance v2, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v2}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {p2, v2}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 184
    invoke-virtual {p2, v10}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 186
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    .line 187
    return-void

    .line 151
    .end local v9    # "flow":Lcom/google/android/videos/flow/Flow;
    .end local v10    # "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    .end local v11    # "spacerFlow":Lcom/google/android/videos/flow/SingleViewFlow;
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-interface/range {p10 .. p10}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-static {p1, v2, v3}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->createForEpisodes(Landroid/app/Activity;Lcom/google/android/videos/adapter/DataSource;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    move-result-object v2

    goto/16 :goto_0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/ui/ShowHelper;I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShowHelper;->getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    return-object v0
.end method

.method static synthetic access$1600(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    return-object v0
.end method

.method static synthetic access$1700(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionOverflowMenuHelper:Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    return-object v0
.end method

.method static synthetic access$1800(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method static synthetic access$1900(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/PinHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/ShowHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/videos/ui/ShowHelper;->headerBackgroundColor:I

    return v0
.end method

.method static synthetic access$2000(Lcom/google/android/videos/ui/ShowHelper;Landroid/database/Cursor;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;
    .param p1, "x1"    # Landroid/database/Cursor;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/ShowHelper;->showPinningErrorDialog(Landroid/database/Cursor;)V

    return-void
.end method

.method static synthetic access$2100(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->title:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->broadcasters:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/ui/ShowHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/ui/ShowHelper;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->ownedCompleteShow:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/ShowHelper;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/ui/ShowHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    return v0
.end method

.method static synthetic access$902(Lcom/google/android/videos/ui/ShowHelper;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/ShowHelper;
    .param p1, "x1"    # I

    .prologue
    .line 62
    iput p1, p0, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    return p1
.end method

.method private changeCursor(Lcom/google/android/videos/store/MasterSubCursor;)V
    .locals 24
    .param p1, "cursor"    # Lcom/google/android/videos/store/MasterSubCursor;

    .prologue
    .line 314
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/ShowHelper;->getCurrentSeasonId()Ljava/lang/String;

    move-result-object v8

    .line 316
    .local v8, "oldSeasonId":Ljava/lang/String;
    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/store/MasterSubCursor;->getNonEmptySubRangeCount()I

    move-result v9

    .line 317
    .local v9, "seasonCount":I
    if-lez v9, :cond_1

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->episodeIdToSelect:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_1

    const/4 v14, 0x1

    .line 318
    .local v14, "shouldFindEpisode":Z
    :goto_0
    if-lez v9, :cond_2

    if-nez v14, :cond_2

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonIdToSelect:Ljava/lang/String;

    move-object/from16 v20, v0

    invoke-static/range {v20 .. v20}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_2

    const/16 v17, 0x1

    .line 320
    .local v17, "shouldFindSeason":Z
    :goto_1
    if-lez v9, :cond_3

    if-nez v17, :cond_3

    invoke-static {v8}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v20

    if-nez v20, :cond_3

    const/16 v16, 0x1

    .line 322
    .local v16, "shouldFindOldSeason":Z
    :goto_2
    if-nez v14, :cond_4

    if-nez v17, :cond_4

    if-nez v16, :cond_4

    const/4 v15, 0x1

    .line 324
    .local v15, "shouldFindLastWatchedSeason":Z
    :goto_3
    const/4 v13, -0x1

    .line 325
    .local v13, "seasonToSelectIndex":I
    const/4 v6, -0x1

    .line 327
    .local v6, "episodeToSelectIndex":I
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Ljava/util/ArrayList;->clear()V

    .line 328
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v9}, Ljava/util/ArrayList;->ensureCapacity(I)V

    .line 330
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    move-object/from16 v1, p1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 338
    if-nez v9, :cond_5

    .line 339
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/ShowHelper;->ownedCompleteShow:Z

    .line 340
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setDataSource(Lcom/google/android/videos/adapter/DataSource;)V

    .line 414
    :cond_0
    :goto_4
    return-void

    .line 317
    .end local v6    # "episodeToSelectIndex":I
    .end local v13    # "seasonToSelectIndex":I
    .end local v14    # "shouldFindEpisode":Z
    .end local v15    # "shouldFindLastWatchedSeason":Z
    .end local v16    # "shouldFindOldSeason":Z
    .end local v17    # "shouldFindSeason":Z
    :cond_1
    const/4 v14, 0x0

    goto :goto_0

    .line 318
    .restart local v14    # "shouldFindEpisode":Z
    :cond_2
    const/16 v17, 0x0

    goto :goto_1

    .line 320
    .restart local v17    # "shouldFindSeason":Z
    :cond_3
    const/16 v16, 0x0

    goto :goto_2

    .line 322
    .restart local v16    # "shouldFindOldSeason":Z
    :cond_4
    const/4 v15, 0x0

    goto :goto_3

    .line 344
    .restart local v6    # "episodeToSelectIndex":I
    .restart local v13    # "seasonToSelectIndex":I
    .restart local v15    # "shouldFindLastWatchedSeason":Z
    :cond_5
    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/store/MasterSubCursor;->moveToFirst()Z

    .line 345
    const/16 v20, 0x6

    move-object/from16 v0, p1

    move/from16 v1, v20

    invoke-interface {v0, v1}, Lcom/google/android/videos/store/MasterSubCursor;->getMasterInt(I)I

    move-result v20

    if-eqz v20, :cond_b

    const/16 v20, 0x1

    :goto_5
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/ShowHelper;->ownedCompleteShow:Z

    .line 348
    const/4 v3, 0x0

    .line 349
    .local v3, "dataSourcePosition":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_6
    if-ge v7, v9, :cond_d

    .line 350
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->createSubRangeDataSource(I)Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    move-result-object v19

    .line 352
    .local v19, "subRangeDataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    const/16 v20, 0x0

    invoke-virtual/range {v19 .. v20}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v18

    .line 353
    .local v18, "subCursor":Landroid/database/Cursor;
    move-object/from16 v0, v19

    move-object/from16 v1, v18

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v11

    .line 354
    .local v11, "seasonId":Ljava/lang/String;
    move-object/from16 v0, p0

    move-object/from16 v1, v19

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/ShowHelper;->getSeasonTitle(Lcom/google/android/videos/adapter/EpisodesDataSource;)Ljava/lang/String;

    move-result-object v12

    .line 355
    .local v12, "seasonTitle":Ljava/lang/String;
    new-instance v10, Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    const/16 v20, 0x0

    move-object/from16 v0, v20

    invoke-direct {v10, v0}, Lcom/google/android/videos/ui/ShowHelper$SeasonData;-><init>(Lcom/google/android/videos/ui/ShowHelper$1;)V

    .line 356
    .local v10, "seasonData":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    iput-object v11, v10, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->seasonId:Ljava/lang/String;

    .line 357
    iput-object v12, v10, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->seasonTitle:Ljava/lang/String;

    .line 358
    move-object/from16 v0, v19

    iput-object v0, v10, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->dataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    .line 359
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-virtual {v0, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 361
    invoke-virtual/range {v19 .. v19}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getCount()I

    move-result v20

    add-int v3, v3, v20

    .line 363
    if-eqz v17, :cond_c

    .line 364
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonIdToSelect:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, v20

    invoke-static {v11, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 365
    const/16 v17, 0x0

    .line 366
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/ShowHelper;->seasonIdToSelect:Ljava/lang/String;

    .line 367
    move v13, v7

    .line 376
    :cond_6
    :goto_7
    if-eqz v14, :cond_9

    .line 377
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->episodeIdToSelect:Ljava/lang/String;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    move-object/from16 v1, v19

    move-object/from16 v2, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/ui/ShowHelper;->indexOfEpisodeWithId(Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;Ljava/lang/String;)I

    move-result v5

    .line 378
    .local v5, "episodeIndex":I
    if-ltz v5, :cond_9

    .line 379
    const/4 v14, 0x0

    .line 380
    const/16 v20, 0x0

    move-object/from16 v0, v20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/ShowHelper;->episodeIdToSelect:Ljava/lang/String;

    .line 381
    move v13, v7

    .line 382
    move v6, v5

    .line 383
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/ui/ShowHelper;->startDownload:Z

    move/from16 v20, v0

    if-eqz v20, :cond_7

    .line 385
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->episodeIdToSelect:Ljava/lang/String;

    move-object/from16 v23, v0

    invoke-virtual/range {v20 .. v23}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 386
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/ShowHelper;->startDownload:Z

    .line 388
    :cond_7
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/ui/ShowHelper;->pinningErrorDialog:Z

    move/from16 v20, v0

    if-eqz v20, :cond_9

    .line 389
    move-object/from16 v0, v19

    invoke-virtual {v0, v5}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v4

    .line 390
    .local v4, "episodeCursor":Landroid/database/Cursor;
    move-object/from16 v0, v19

    invoke-virtual {v0, v4}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v20

    const/16 v21, 0x4

    invoke-static/range {v21 .. v21}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v21

    invoke-static/range {v20 .. v21}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v20

    if-eqz v20, :cond_8

    .line 392
    move-object/from16 v0, p0

    invoke-direct {v0, v4}, Lcom/google/android/videos/ui/ShowHelper;->showPinningErrorDialog(Landroid/database/Cursor;)V

    .line 394
    :cond_8
    const/16 v20, 0x0

    move/from16 v0, v20

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/ui/ShowHelper;->pinningErrorDialog:Z

    .line 399
    .end local v4    # "episodeCursor":Landroid/database/Cursor;
    .end local v5    # "episodeIndex":I
    :cond_9
    if-eqz v15, :cond_a

    .line 400
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/ui/ShowHelper;->seasonIsLastViewed(Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;)Z

    move-result v20

    if-eqz v20, :cond_a

    .line 401
    move v13, v7

    .line 402
    const/4 v15, 0x0

    .line 349
    :cond_a
    add-int/lit8 v7, v7, 0x1

    goto/16 :goto_6

    .line 345
    .end local v3    # "dataSourcePosition":I
    .end local v7    # "i":I
    .end local v10    # "seasonData":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    .end local v11    # "seasonId":Ljava/lang/String;
    .end local v12    # "seasonTitle":Ljava/lang/String;
    .end local v18    # "subCursor":Landroid/database/Cursor;
    .end local v19    # "subRangeDataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    :cond_b
    const/16 v20, 0x0

    goto/16 :goto_5

    .line 369
    .restart local v3    # "dataSourcePosition":I
    .restart local v7    # "i":I
    .restart local v10    # "seasonData":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    .restart local v11    # "seasonId":Ljava/lang/String;
    .restart local v12    # "seasonTitle":Ljava/lang/String;
    .restart local v18    # "subCursor":Landroid/database/Cursor;
    .restart local v19    # "subRangeDataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    :cond_c
    if-eqz v16, :cond_6

    .line 370
    invoke-static {v11, v8}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v20

    if-eqz v20, :cond_6

    .line 371
    const/16 v16, 0x0

    .line 372
    move v13, v7

    goto/16 :goto_7

    .line 407
    .end local v10    # "seasonData":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    .end local v11    # "seasonId":Ljava/lang/String;
    .end local v12    # "seasonTitle":Ljava/lang/String;
    .end local v18    # "subCursor":Landroid/database/Cursor;
    .end local v19    # "subRangeDataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    :cond_d
    const/16 v20, -0x1

    move/from16 v0, v20

    if-ne v13, v0, :cond_e

    const/16 v20, 0x0

    :goto_8
    move/from16 v0, v20

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    .line 408
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    move-object/from16 v20, v0

    invoke-virtual/range {v20 .. v20}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 410
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    move/from16 v21, v0

    move-object/from16 v0, p0

    move/from16 v1, v21

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/ShowHelper;->getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    move-result-object v21

    move-object/from16 v0, v21

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->dataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    move-object/from16 v21, v0

    invoke-virtual/range {v20 .. v21}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setDataSource(Lcom/google/android/videos/adapter/DataSource;)V

    .line 411
    const/16 v20, -0x1

    move/from16 v0, v20

    if-eq v6, v0, :cond_0

    .line 412
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget v0, v0, Lcom/google/android/videos/ui/ShowHelper;->flowTopViewsCount:I

    move/from16 v21, v0

    add-int v21, v21, v6

    invoke-virtual/range {v20 .. v21}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    goto/16 :goto_4

    :cond_e
    move/from16 v20, v13

    .line 407
    goto :goto_8
.end method

.method private getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    .locals 1
    .param p1, "seasonIndex"    # I

    .prologue
    .line 453
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    if-eqz v0, :cond_0

    if-ltz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-lt p1, v0, :cond_1

    .line 455
    :cond_0
    const/4 v0, 0x0

    .line 457
    :goto_0
    return-object v0

    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    goto :goto_0
.end method

.method private getSeasonTitle(Lcom/google/android/videos/adapter/EpisodesDataSource;)Ljava/lang/String;
    .locals 7
    .param p1, "subRangeDataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource;

    .prologue
    const/4 v6, 0x0

    .line 417
    invoke-virtual {p1, v6}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    .line 418
    .local v1, "subCursor":Landroid/database/Cursor;
    invoke-virtual {p1, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 419
    .local v0, "seasonTitle":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 420
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    const v3, 0x7f0b00c7

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {p1, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v6

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/activity/ShowActivity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 423
    :cond_0
    return-object v0
.end method

.method private indexOfEpisodeWithId(Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;Ljava/lang/String;)I
    .locals 6
    .param p1, "subRangeDataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    .param p2, "id"    # Ljava/lang/String;

    .prologue
    const/4 v4, -0x1

    .line 427
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    if-eqz v5, :cond_1

    move v3, v4

    .line 438
    :cond_0
    :goto_0
    return v3

    .line 430
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getCount()I

    move-result v1

    .line 431
    .local v1, "subCount":I
    const/4 v3, 0x0

    .local v3, "subIndex":I
    :goto_1
    if-ge v3, v1, :cond_2

    .line 432
    invoke-virtual {p1, v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v2

    .line 433
    .local v2, "subCursor":Landroid/database/Cursor;
    invoke-virtual {p1, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    .line 434
    .local v0, "episodeId":Ljava/lang/String;
    invoke-static {v0, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v5

    if-nez v5, :cond_0

    .line 431
    add-int/lit8 v3, v3, 0x1

    goto :goto_1

    .end local v0    # "episodeId":Ljava/lang/String;
    .end local v2    # "subCursor":Landroid/database/Cursor;
    :cond_2
    move v3, v4

    .line 438
    goto :goto_0
.end method

.method private isBeingTouched()Z
    .locals 1

    .prologue
    .line 469
    const/4 v0, 0x0

    return v0
.end method

.method private onBitmapsUpdated()V
    .locals 1

    .prologue
    .line 461
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->isBeingTouched()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 462
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->pendingBitmapsUpdatedNotification:Z

    .line 466
    :goto_0
    return-void

    .line 465
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemStatesChanged()V

    goto :goto_0
.end method

.method private refreshEpisodesCursor()V
    .locals 3

    .prologue
    .line 292
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->isBeingTouched()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 293
    const/4 v2, 0x1

    iput-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->pendingEpisodesCursor:Z

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 297
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/MasterSubCursor;

    .line 298
    .local v0, "cursor":Lcom/google/android/videos/store/MasterSubCursor;
    if-eqz v0, :cond_2

    .line 299
    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/ShowHelper;->changeCursor(Lcom/google/android/videos/store/MasterSubCursor;)V

    .line 302
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowHelper;->updateVisibilities()V

    .line 304
    iget-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    if-eqz v2, :cond_0

    .line 305
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 306
    .local v1, "unpurchasedEpisodeIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getUnpurchasedEpisodes(Ljava/util/List;)I

    move-result v2

    if-eqz v2, :cond_0

    .line 307
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v2, v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->processUnpurchasedItems(Ljava/util/List;)V

    goto :goto_0
.end method

.method private static seasonIsLastViewed(Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;)Z
    .locals 4
    .param p0, "subRangeDataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    .prologue
    .line 442
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getCount()I

    move-result v0

    .line 443
    .local v0, "subCount":I
    const/4 v2, 0x0

    .local v2, "subIndex":I
    :goto_0
    if-ge v2, v0, :cond_1

    .line 444
    invoke-virtual {p0, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    .line 445
    .local v1, "subCursor":Landroid/database/Cursor;
    invoke-virtual {p0, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->isLastWatched(Landroid/database/Cursor;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 446
    const/4 v3, 0x1

    .line 449
    .end local v1    # "subCursor":Landroid/database/Cursor;
    :goto_1
    return v3

    .line 443
    .restart local v1    # "subCursor":Landroid/database/Cursor;
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 449
    .end local v1    # "subCursor":Landroid/database/Cursor;
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private showPinningErrorDialog(Landroid/database/Cursor;)V
    .locals 7
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 535
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v6, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    .line 541
    return-void
.end method


# virtual methods
.method public createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 5
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    const v4, 0x7f0400c0

    const/4 v3, 0x0

    .line 523
    if-ne p1, v4, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 524
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/ShowActivity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v2

    invoke-virtual {v2, v4, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 525
    .local v0, "view":Landroid/view/View;
    new-instance v1, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;-><init>(Lcom/google/android/videos/ui/ShowHelper;Landroid/view/View;)V

    .line 526
    .local v1, "viewHolder":Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 528
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/view/View;->setAlpha(F)V

    .line 530
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v2}, Lcom/google/android/videos/activity/ShowActivity;->onHeaderViewCreated()V

    .line 531
    return-object v1

    .end local v0    # "view":Landroid/view/View;
    .end local v1    # "viewHolder":Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;
    :cond_1
    move v2, v3

    .line 523
    goto :goto_0
.end method

.method public getCurrentEpisodeId()Ljava/lang/String;
    .locals 9

    .prologue
    const/4 v7, 0x0

    .line 214
    const/4 v5, -0x1

    .line 215
    .local v5, "referenceChildPosition":I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    iget-object v8, p0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8}, Landroid/support/v7/widget/RecyclerView;->getChildCount()I

    move-result v8

    if-ge v4, v8, :cond_0

    .line 216
    iget-object v8, p0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8, v4}, Landroid/support/v7/widget/RecyclerView;->getChildAt(I)Landroid/view/View;

    move-result-object v0

    .line 217
    .local v0, "child":Landroid/view/View;
    invoke-virtual {v0}, Landroid/view/View;->getBottom()I

    move-result v8

    if-lez v8, :cond_2

    .line 218
    iget-object v8, p0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v8, v0}, Landroid/support/v7/widget/RecyclerView;->getChildPosition(Landroid/view/View;)I

    move-result v5

    .line 222
    .end local v0    # "child":Landroid/view/View;
    :cond_0
    iget v8, p0, Lcom/google/android/videos/ui/ShowHelper;->flowTopViewsCount:I

    sub-int v3, v5, v8

    .line 223
    .local v3, "episodeIndex":I
    if-gez v3, :cond_3

    .line 232
    :cond_1
    :goto_1
    return-object v7

    .line 215
    .end local v3    # "episodeIndex":I
    .restart local v0    # "child":Landroid/view/View;
    :cond_2
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 226
    .end local v0    # "child":Landroid/view/View;
    .restart local v3    # "episodeIndex":I
    :cond_3
    iget v8, p0, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    invoke-direct {p0, v8}, Lcom/google/android/videos/ui/ShowHelper;->getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    move-result-object v1

    .line 227
    .local v1, "data":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    if-nez v1, :cond_4

    move-object v2, v7

    .line 228
    .local v2, "dataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    :goto_2
    if-eqz v2, :cond_1

    invoke-virtual {v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getCount()I

    move-result v8

    if-le v8, v3, :cond_1

    .line 231
    invoke-virtual {v2, v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v6

    .line 232
    .local v6, "subCursor":Landroid/database/Cursor;
    invoke-virtual {v2, v6}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v7

    goto :goto_1

    .line 227
    .end local v2    # "dataSource":Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    .end local v6    # "subCursor":Landroid/database/Cursor;
    :cond_4
    iget-object v2, v1, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->dataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    goto :goto_2
.end method

.method public getCurrentSeasonId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 239
    iget v1, p0, Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I

    invoke-direct {p0, v1}, Lcom/google/android/videos/ui/ShowHelper;->getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    move-result-object v0

    .line 240
    .local v0, "data":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->seasonId:Ljava/lang/String;

    goto :goto_0
.end method

.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 288
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->refreshEpisodesCursor()V

    .line 289
    return-void
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 724
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowHelper;->updateVisibilities()V

    .line 725
    return-void
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 714
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->onBitmapsUpdated()V

    .line 715
    return-void
.end method

.method public onShowBannerUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 719
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/ShowActivity;->loadBackgroundImage()V

    .line 720
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 709
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->onBitmapsUpdated()V

    .line 710
    return-void
.end method

.method public onStart()V
    .locals 3

    .prologue
    .line 258
    iget-boolean v0, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper;->playCountry:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->init(Ljava/lang/String;Ljava/lang/String;)V

    .line 261
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 262
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 263
    invoke-direct {p0}, Lcom/google/android/videos/ui/ShowHelper;->refreshEpisodesCursor()V

    .line 264
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 265
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ShowHelper;->update()V

    .line 266
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 267
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 270
    iget-boolean v1, p0, Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z

    if-eqz v1, :cond_0

    .line 271
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->reset()V

    .line 273
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 274
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    if-nez v1, :cond_1

    move v1, v2

    :goto_0
    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_1
    if-ltz v0, :cond_2

    .line 275
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    invoke-virtual {v1, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    iput-object v3, v1, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->dataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    .line 274
    add-int/lit8 v0, v0, -0x1

    goto :goto_1

    .end local v0    # "i":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;

    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v1

    goto :goto_0

    .line 277
    .restart local v0    # "i":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setDataSource(Lcom/google/android/videos/adapter/DataSource;)V

    .line 278
    iput-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->pendingEpisodesCursor:Z

    .line 279
    iput-boolean v2, p0, Lcom/google/android/videos/ui/ShowHelper;->pendingBitmapsUpdatedNotification:Z

    .line 280
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v1}, Lcom/google/android/videos/store/PurchaseStore;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v1

    invoke-virtual {v1, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 281
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesCursorHelper:Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 282
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 283
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 284
    return-void
.end method

.method public setEpisodeIdToSelect(Ljava/lang/String;)V
    .locals 0
    .param p1, "episodeIdToSelect"    # Ljava/lang/String;

    .prologue
    .line 202
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowHelper;->episodeIdToSelect:Ljava/lang/String;

    .line 203
    return-void
.end method

.method public setHeaderBackgroundColor(I)V
    .locals 1
    .param p1, "color"    # I

    .prologue
    .line 244
    iput p1, p0, Lcom/google/android/videos/ui/ShowHelper;->headerBackgroundColor:I

    .line 245
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 246
    return-void
.end method

.method public setSeasonIdToSelect(Ljava/lang/String;)V
    .locals 0
    .param p1, "seasonIdToSelect"    # Ljava/lang/String;

    .prologue
    .line 193
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonIdToSelect:Ljava/lang/String;

    .line 194
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 510
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->setNetworkConnected(Z)V

    .line 511
    return-void
.end method

.method public updateInfo(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "broadcasters"    # Ljava/lang/String;

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->title:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->broadcasters:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 251
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowHelper;->title:Ljava/lang/String;

    .line 252
    iput-object p2, p0, Lcom/google/android/videos/ui/ShowHelper;->broadcasters:Ljava/lang/String;

    .line 253
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 255
    :cond_1
    return-void
.end method

.method public updateVisibilities()V
    .locals 2

    .prologue
    .line 489
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/ShowActivity;->pendingEnterTransition()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/ShowActivity;->runningEnterTransition()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 491
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->progressBar:Landroid/view/View;

    if-eqz v0, :cond_1

    .line 492
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->progressBar:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 496
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->headerFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 503
    :cond_2
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->episodesDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 504
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->mainView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    .line 506
    :cond_3
    return-void

    .line 498
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->progressBar:Landroid/view/View;

    if-eqz v0, :cond_2

    .line 499
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper;->progressBar:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0
.end method
