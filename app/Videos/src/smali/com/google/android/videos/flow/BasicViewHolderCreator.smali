.class public Lcom/google/android/videos/flow/BasicViewHolderCreator;
.super Ljava/lang/Object;
.source "BasicViewHolderCreator.java"

# interfaces
.implements Lcom/google/android/videos/flow/ViewHolderCreator;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;",
        ">;"
    }
.end annotation


# instance fields
.field public final layoutId:I


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "layoutId"    # I

    .prologue
    .line 35
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 36
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 37
    iput p1, p0, Lcom/google/android/videos/flow/BasicViewHolderCreator;->layoutId:I

    .line 38
    return-void

    .line 36
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static addOrVerify(Landroid/util/SparseArray;II)V
    .locals 9
    .param p1, "viewType"    # I
    .param p2, "layoutId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/flow/ViewHolderCreator",
            "<*>;>;II)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalArgumentException;
        }
    .end annotation

    .prologue
    .local p0, "viewTypes":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/flow/ViewHolderCreator<*>;>;"
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 59
    invoke-virtual {p0, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/flow/ViewHolderCreator;

    .line 60
    .local v1, "existing":Lcom/google/android/videos/flow/ViewHolderCreator;, "Lcom/google/android/videos/flow/ViewHolderCreator<*>;"
    if-nez v1, :cond_1

    .line 61
    new-instance v2, Lcom/google/android/videos/flow/BasicViewHolderCreator;

    invoke-direct {v2, p2}, Lcom/google/android/videos/flow/BasicViewHolderCreator;-><init>(I)V

    invoke-virtual {p0, p1, v2}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 75
    :cond_0
    return-void

    .line 64
    :cond_1
    instance-of v2, v1, Lcom/google/android/videos/flow/BasicViewHolderCreator;

    if-nez v2, :cond_2

    .line 65
    new-instance v2, Ljava/lang/IllegalArgumentException;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Existing ViewHolderCreator for view type 0x%1$x is not a BasicViewHolderCreator"

    new-array v5, v8, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2

    :cond_2
    move-object v0, v1

    .line 69
    check-cast v0, Lcom/google/android/videos/flow/BasicViewHolderCreator;

    .line 70
    .local v0, "creator":Lcom/google/android/videos/flow/BasicViewHolderCreator;
    iget v2, v0, Lcom/google/android/videos/flow/BasicViewHolderCreator;->layoutId:I

    if-eq v2, p2, :cond_0

    .line 71
    new-instance v2, Ljava/lang/IllegalArgumentException;

    sget-object v3, Ljava/util/Locale;->US:Ljava/util/Locale;

    const-string v4, "Existing BasicViewHolderCreator for view type 0x%1$x is for layout 0x%2$x, but needed 0x%3$x"

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    iget v6, v0, Lcom/google/android/videos/flow/BasicViewHolderCreator;->layoutId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v8

    const/4 v6, 0x2

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-static {v3, v4, v5}, Ljava/lang/String;->format(Ljava/util/Locale;Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v2, v3}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v2
.end method


# virtual methods
.method public bridge synthetic createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 19
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/flow/BasicViewHolderCreator;->createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;
    .locals 4
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 42
    new-instance v0, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;

    invoke-virtual {p2}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/flow/BasicViewHolderCreator;->layoutId:I

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p2, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/flow/BasicViewHolderCreator$BasicViewHolder;-><init>(Landroid/view/View;)V

    return-object v0
.end method
