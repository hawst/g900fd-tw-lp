.class public Lcom/google/android/videos/ui/HelpHelper;
.super Ljava/lang/Object;
.source "HelpHelper.java"


# direct methods
.method public static startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V
    .locals 7
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "helpContext"    # Ljava/lang/String;

    .prologue
    .line 35
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v4

    .line 36
    .local v4, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/logging/EventLogger;->onHelpAndFeedbackOpened()V

    .line 37
    const v5, 0x7f0b0188

    invoke-virtual {p0, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 38
    .local v1, "fallbackSupportUri":Landroid/net/Uri;
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/Config;->gmsCoreAvailable()Z

    move-result v5

    if-nez v5, :cond_0

    .line 39
    invoke-static {p0, v1}, Lcom/google/android/videos/utils/BrowserUtil;->viewUri(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 52
    :goto_0
    return-void

    .line 44
    :cond_0
    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v5

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccount(Ljava/lang/String;)Landroid/accounts/Account;

    move-result-object v0

    .line 46
    .local v0, "currentAccount":Landroid/accounts/Account;
    new-instance v5, Lcom/google/android/gms/googlehelp/GoogleHelp;

    invoke-direct {v5, p1}, Lcom/google/android/gms/googlehelp/GoogleHelp;-><init>(Ljava/lang/String;)V

    invoke-virtual {v5, v0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setGoogleAccount(Landroid/accounts/Account;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v5

    invoke-virtual {v5, v1}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setFallbackSupportUri(Landroid/net/Uri;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v5

    invoke-static {p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->getScreenshot(Landroid/app/Activity;)Landroid/graphics/Bitmap;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/gms/googlehelp/GoogleHelp;->setScreenshot(Landroid/graphics/Bitmap;)Lcom/google/android/gms/googlehelp/GoogleHelp;

    move-result-object v2

    .line 50
    .local v2, "helpInstance":Lcom/google/android/gms/googlehelp/GoogleHelp;
    invoke-virtual {v2, p0}, Lcom/google/android/gms/googlehelp/GoogleHelp;->buildHelpIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v3

    .line 51
    .local v3, "helpIntent":Landroid/content/Intent;
    new-instance v5, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;

    invoke-direct {v5, p0}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;-><init>(Landroid/app/Activity;)V

    invoke-virtual {v5, v3}, Lcom/google/android/gms/googlehelp/GoogleHelpLauncher;->launch(Landroid/content/Intent;)V

    goto :goto_0
.end method
