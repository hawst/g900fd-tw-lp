.class Lcom/google/android/videos/player/DirectorInitializer$4;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->onAllTasksCompleted()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 721
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->listener:Lcom/google/android/videos/player/DirectorInitializer$Listener;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$700(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/DirectorInitializer$Listener;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorCanRetry:Z
    invoke-static {v1}, Lcom/google/android/videos/player/DirectorInitializer;->access$1000(Lcom/google/android/videos/player/DirectorInitializer;)Z

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorType:I
    invoke-static {v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$1100(Lcom/google/android/videos/player/DirectorInitializer;)I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->initializationErrorMessage:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/videos/player/DirectorInitializer;->access$1200(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/player/DirectorInitializer$4;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->initializationError:Ljava/lang/Exception;
    invoke-static {v4}, Lcom/google/android/videos/player/DirectorInitializer;->access$1300(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/Exception;

    move-result-object v4

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/videos/player/DirectorInitializer$Listener;->onError(ZILjava/lang/String;Ljava/lang/Exception;)V

    .line 726
    return-void
.end method
