.class public Lcom/google/android/videos/async/ControllableRequest;
.super Ljava/lang/Object;
.source "ControllableRequest.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field public final data:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TT;"
        }
    .end annotation
.end field

.field public final taskStatus:Lcom/google/android/videos/async/TaskStatus;


# direct methods
.method public constructor <init>(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)V
    .locals 0
    .param p2, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;",
            "Lcom/google/android/videos/async/TaskStatus;",
            ")V"
        }
    .end annotation

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TT;>;"
    .local p1, "data":Ljava/lang/Object;, "TT;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 14
    iput-object p1, p0, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    .line 15
    iput-object p2, p0, Lcom/google/android/videos/async/ControllableRequest;->taskStatus:Lcom/google/android/videos/async/TaskStatus;

    .line 16
    return-void
.end method

.method public static create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;
    .locals 1
    .param p1, "taskStatus"    # Lcom/google/android/videos/async/TaskStatus;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<T:",
            "Ljava/lang/Object;",
            ">(TT;",
            "Lcom/google/android/videos/async/TaskStatus;",
            ")",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TT;>;"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "data":Ljava/lang/Object;, "TT;"
    new-instance v0, Lcom/google/android/videos/async/ControllableRequest;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/async/ControllableRequest;-><init>(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)V

    return-object v0
.end method


# virtual methods
.method public isCancelled()Z
    .locals 1

    .prologue
    .line 19
    .local p0, "this":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/async/ControllableRequest;->taskStatus:Lcom/google/android/videos/async/TaskStatus;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskStatus;->isCancelled(Lcom/google/android/videos/async/TaskStatus;)Z

    move-result v0

    return v0
.end method
