.class public final Lcom/google/android/videos/async/IgnoreCallback;
.super Ljava/lang/Object;
.source "IgnoreCallback.java"

# interfaces
.implements Lcom/google/android/videos/async/NewCallback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/NewCallback",
        "<",
        "Ljava/lang/Object;",
        "Ljava/lang/Object;",
        ">;"
    }
.end annotation


# static fields
.field private static final instance:Lcom/google/android/videos/async/IgnoreCallback;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 14
    new-instance v0, Lcom/google/android/videos/async/IgnoreCallback;

    invoke-direct {v0}, Lcom/google/android/videos/async/IgnoreCallback;-><init>()V

    sput-object v0, Lcom/google/android/videos/async/IgnoreCallback;->instance:Lcom/google/android/videos/async/IgnoreCallback;

    return-void
.end method

.method private constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static get()Lcom/google/android/videos/async/NewCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">()",
            "Lcom/google/android/videos/async/NewCallback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 20
    sget-object v0, Lcom/google/android/videos/async/IgnoreCallback;->instance:Lcom/google/android/videos/async/IgnoreCallback;

    return-object v0
.end method


# virtual methods
.method public onCancelled(Ljava/lang/Object;)V
    .locals 0
    .param p1, "request"    # Ljava/lang/Object;

    .prologue
    .line 36
    return-void
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 31
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "request"    # Ljava/lang/Object;
    .param p2, "response"    # Ljava/lang/Object;

    .prologue
    .line 26
    return-void
.end method
