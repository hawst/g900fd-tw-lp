.class public final Lcom/google/android/videos/proto/FilmProtos$Person;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Person"
.end annotation


# static fields
.field private static volatile _emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Person;


# instance fields
.field public appearance:[B

.field public character:[Lcom/google/android/videos/proto/FilmProtos$Character;

.field public dateOfBirth:Ljava/lang/String;

.field public dateOfDeath:Ljava/lang/String;

.field public filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

.field public filmographyIndex:[I

.field public id:Ljava/lang/String;

.field public image:Lcom/google/android/videos/proto/FilmProtos$Image;

.field public localId:I

.field public mid:Ljava/lang/String;

.field public name:Ljava/lang/String;

.field public placeOfBirth:Ljava/lang/String;

.field public splitId:[I


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 919
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 920
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Person;->clear()Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 921
    return-void
.end method

.method public static emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Person;
    .locals 2

    .prologue
    .line 869
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Person;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-nez v0, :cond_1

    .line 870
    sget-object v1, Lcom/google/protobuf/nano/InternalNano;->LAZY_INIT_LOCK:Ljava/lang/Object;

    monitor-enter v1

    .line 872
    :try_start_0
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Person;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Person;

    if-nez v0, :cond_0

    .line 873
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/android/videos/proto/FilmProtos$Person;

    sput-object v0, Lcom/google/android/videos/proto/FilmProtos$Person;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 875
    :cond_0
    monitor-exit v1
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 877
    :cond_1
    sget-object v0, Lcom/google/android/videos/proto/FilmProtos$Person;->_emptyArray:[Lcom/google/android/videos/proto/FilmProtos$Person;

    return-object v0

    .line 875
    :catchall_0
    move-exception v0

    :try_start_1
    monitor-exit v1
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v0
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Person;
    .locals 1

    .prologue
    .line 924
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    .line 925
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    .line 926
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    .line 927
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 928
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Character;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Character;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    .line 929
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Film;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Film;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 930
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    .line 931
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    .line 932
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    .line 933
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    .line 934
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    .line 935
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_INT_ARRAY:[I

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    .line 936
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    .line 937
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->cachedSize:I

    .line 938
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 6

    .prologue
    .line 1125
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v3

    .line 1126
    .local v3, "size":I
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 1127
    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1130
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_1

    .line 1131
    const/4 v4, 0x2

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1134
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_2

    .line 1135
    const/4 v4, 0x3

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1138
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v4, :cond_3

    .line 1139
    const/4 v4, 0x4

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1142
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    if-eqz v4, :cond_5

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v4, v4

    if-lez v4, :cond_5

    .line 1143
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v4, v4

    if-ge v2, v4, :cond_5

    .line 1144
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    aget-object v1, v4, v2

    .line 1145
    .local v1, "element":Lcom/google/android/videos/proto/FilmProtos$Character;
    if-eqz v1, :cond_4

    .line 1146
    const/4 v4, 0x5

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1143
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1151
    .end local v1    # "element":Lcom/google/android/videos/proto/FilmProtos$Character;
    .end local v2    # "i":I
    :cond_5
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    if-eqz v4, :cond_7

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v4, v4

    if-lez v4, :cond_7

    .line 1152
    const/4 v0, 0x0

    .line 1153
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v4, v4

    if-ge v2, v4, :cond_6

    .line 1154
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    aget v1, v4, v2

    .line 1155
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1153
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1158
    .end local v1    # "element":I
    :cond_6
    add-int/2addr v3, v0

    .line 1159
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v4, v4

    mul-int/lit8 v4, v4, 0x1

    add-int/2addr v3, v4

    .line 1161
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_7
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v4, :cond_9

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v4, v4

    if-lez v4, :cond_9

    .line 1162
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v4, v4

    if-ge v2, v4, :cond_9

    .line 1163
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    aget-object v1, v4, v2

    .line 1164
    .local v1, "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v1, :cond_8

    .line 1165
    const/4 v4, 0x7

    invoke-static {v4, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1162
    :cond_8
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1170
    .end local v1    # "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    .end local v2    # "i":I
    :cond_9
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_a

    .line 1171
    const/16 v4, 0x8

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1174
    :cond_a
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_b

    .line 1175
    const/16 v4, 0x9

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1178
    :cond_b
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    const-string v5, ""

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-nez v4, :cond_c

    .line 1179
    const/16 v4, 0xa

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v4

    add-int/2addr v3, v4

    .line 1182
    :cond_c
    iget v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    if-eqz v4, :cond_d

    .line 1183
    const/16 v4, 0xb

    iget v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32Size(II)I

    move-result v4

    add-int/2addr v3, v4

    .line 1186
    :cond_d
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    if-eqz v4, :cond_f

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v4, v4

    if-lez v4, :cond_f

    .line 1187
    const/4 v0, 0x0

    .line 1188
    .restart local v0    # "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v4, v4

    if-ge v2, v4, :cond_e

    .line 1189
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    aget v1, v4, v2

    .line 1190
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v4

    add-int/2addr v0, v4

    .line 1188
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1193
    .end local v1    # "element":I
    :cond_e
    add-int/2addr v3, v0

    .line 1194
    add-int/lit8 v3, v3, 0x1

    .line 1195
    invoke-static {v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeRawVarint32Size(I)I

    move-result v4

    add-int/2addr v3, v4

    .line 1198
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_f
    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    sget-object v5, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v4, v5}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v4

    if-nez v4, :cond_10

    .line 1199
    const/16 v4, 0xd

    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    invoke-static {v4, v5}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v4

    add-int/2addr v3, v4

    .line 1202
    :cond_10
    return v3
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 943
    if-ne p1, p0, :cond_1

    .line 1023
    :cond_0
    :goto_0
    return v1

    .line 946
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Person;

    if-nez v3, :cond_2

    move v1, v2

    .line 947
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 949
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Person;

    .line 950
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Person;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 951
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 952
    goto :goto_0

    .line 954
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 955
    goto :goto_0

    .line 957
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 958
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 959
    goto :goto_0

    .line 961
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 962
    goto :goto_0

    .line 964
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 965
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 966
    goto :goto_0

    .line 968
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 969
    goto :goto_0

    .line 971
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_9

    .line 972
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_a

    move v1, v2

    .line 973
    goto :goto_0

    .line 976
    :cond_9
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/proto/FilmProtos$Image;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    move v1, v2

    .line 977
    goto :goto_0

    .line 980
    :cond_a
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    move v1, v2

    .line 982
    goto :goto_0

    .line 984
    :cond_b
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_c

    move v1, v2

    .line 986
    goto :goto_0

    .line 988
    :cond_c
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    if-nez v3, :cond_d

    .line 989
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    if-eqz v3, :cond_e

    move v1, v2

    .line 990
    goto/16 :goto_0

    .line 992
    :cond_d
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_e

    move v1, v2

    .line 993
    goto/16 :goto_0

    .line 995
    :cond_e
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    if-nez v3, :cond_f

    .line 996
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    if-eqz v3, :cond_10

    move v1, v2

    .line 997
    goto/16 :goto_0

    .line 999
    :cond_f
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_10

    move v1, v2

    .line 1000
    goto/16 :goto_0

    .line 1002
    :cond_10
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    if-nez v3, :cond_11

    .line 1003
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    if-eqz v3, :cond_12

    move v1, v2

    .line 1004
    goto/16 :goto_0

    .line 1006
    :cond_11
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_12

    move v1, v2

    .line 1007
    goto/16 :goto_0

    .line 1009
    :cond_12
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    iget v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    if-eq v3, v4, :cond_13

    move v1, v2

    .line 1010
    goto/16 :goto_0

    .line 1012
    :cond_13
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_14

    move v1, v2

    .line 1014
    goto/16 :goto_0

    .line 1016
    :cond_14
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([I[I)Z

    move-result v3

    if-nez v3, :cond_15

    move v1, v2

    .line 1018
    goto/16 :goto_0

    .line 1020
    :cond_15
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1021
    goto/16 :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1028
    const/16 v0, 0x11

    .line 1029
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 1031
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1033
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    if-nez v1, :cond_2

    move v1, v2

    :goto_2
    add-int v0, v3, v1

    .line 1035
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v1, :cond_3

    move v1, v2

    :goto_3
    add-int v0, v3, v1

    .line 1037
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 1039
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v3}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v3

    add-int v0, v1, v3

    .line 1041
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    if-nez v1, :cond_4

    move v1, v2

    :goto_4
    add-int v0, v3, v1

    .line 1043
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    if-nez v1, :cond_5

    move v1, v2

    :goto_5
    add-int v0, v3, v1

    .line 1045
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    if-nez v3, :cond_6

    :goto_6
    add-int v0, v1, v2

    .line 1047
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    add-int v0, v1, v2

    .line 1048
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 1050
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([I)I

    move-result v2

    add-int v0, v1, v2

    .line 1052
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int v0, v1, v2

    .line 1053
    return v0

    .line 1029
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1031
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1033
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_2

    .line 1035
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {v1}, Lcom/google/android/videos/proto/FilmProtos$Image;->hashCode()I

    move-result v1

    goto :goto_3

    .line 1041
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_4

    .line 1043
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_5

    .line 1045
    :cond_6
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_6
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Person;
    .locals 9
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v7, 0x0

    .line 1210
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v6

    .line 1211
    .local v6, "tag":I
    sparse-switch v6, :sswitch_data_0

    .line 1215
    invoke-static {p1, v6}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v8

    if-nez v8, :cond_0

    .line 1216
    :sswitch_0
    return-object p0

    .line 1221
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    goto :goto_0

    .line 1225
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    goto :goto_0

    .line 1229
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    goto :goto_0

    .line 1233
    :sswitch_4
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v8, :cond_1

    .line 1234
    new-instance v8, Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-direct {v8}, Lcom/google/android/videos/proto/FilmProtos$Image;-><init>()V

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 1236
    :cond_1
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    goto :goto_0

    .line 1240
    :sswitch_5
    const/16 v8, 0x2a

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1242
    .local v0, "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    if-nez v8, :cond_3

    move v1, v7

    .line 1243
    .local v1, "i":I
    :goto_1
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/videos/proto/FilmProtos$Character;

    .line 1245
    .local v4, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Character;
    if-eqz v1, :cond_2

    .line 1246
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1248
    :cond_2
    :goto_2
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_4

    .line 1249
    new-instance v8, Lcom/google/android/videos/proto/FilmProtos$Character;

    invoke-direct {v8}, Lcom/google/android/videos/proto/FilmProtos$Character;-><init>()V

    aput-object v8, v4, v1

    .line 1250
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1251
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1248
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1242
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Character;
    :cond_3
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v1, v8

    goto :goto_1

    .line 1254
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Character;
    :cond_4
    new-instance v8, Lcom/google/android/videos/proto/FilmProtos$Character;

    invoke-direct {v8}, Lcom/google/android/videos/proto/FilmProtos$Character;-><init>()V

    aput-object v8, v4, v1

    .line 1255
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1256
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    goto :goto_0

    .line 1260
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Character;
    :sswitch_6
    const/16 v8, 0x30

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1262
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    if-nez v8, :cond_6

    move v1, v7

    .line 1263
    .restart local v1    # "i":I
    :goto_3
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1264
    .local v4, "newArray":[I
    if-eqz v1, :cond_5

    .line 1265
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1267
    :cond_5
    :goto_4
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_7

    .line 1268
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1269
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1267
    add-int/lit8 v1, v1, 0x1

    goto :goto_4

    .line 1262
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_6
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v1, v8

    goto :goto_3

    .line 1272
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1273
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    goto/16 :goto_0

    .line 1277
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_7
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 1278
    .local v2, "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 1280
    .local v3, "limit":I
    const/4 v0, 0x0

    .line 1281
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 1282
    .local v5, "startPos":I
    :goto_5
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_8

    .line 1283
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 1284
    add-int/lit8 v0, v0, 0x1

    goto :goto_5

    .line 1286
    :cond_8
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 1287
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    if-nez v8, :cond_a

    move v1, v7

    .line 1288
    .restart local v1    # "i":I
    :goto_6
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1289
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_9

    .line 1290
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1292
    :cond_9
    :goto_7
    array-length v8, v4

    if-ge v1, v8, :cond_b

    .line 1293
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1292
    add-int/lit8 v1, v1, 0x1

    goto :goto_7

    .line 1287
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_a
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v1, v8

    goto :goto_6

    .line 1295
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_b
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    .line 1296
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1300
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[I
    .end local v5    # "startPos":I
    :sswitch_8
    const/16 v8, 0x3a

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1302
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-nez v8, :cond_d

    move v1, v7

    .line 1303
    .restart local v1    # "i":I
    :goto_8
    add-int v8, v1, v0

    new-array v4, v8, [Lcom/google/android/videos/proto/FilmProtos$Film;

    .line 1305
    .local v4, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v1, :cond_c

    .line 1306
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1308
    :cond_c
    :goto_9
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_e

    .line 1309
    new-instance v8, Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-direct {v8}, Lcom/google/android/videos/proto/FilmProtos$Film;-><init>()V

    aput-object v8, v4, v1

    .line 1310
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1311
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1308
    add-int/lit8 v1, v1, 0x1

    goto :goto_9

    .line 1302
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_d
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v1, v8

    goto :goto_8

    .line 1314
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :cond_e
    new-instance v8, Lcom/google/android/videos/proto/FilmProtos$Film;

    invoke-direct {v8}, Lcom/google/android/videos/proto/FilmProtos$Film;-><init>()V

    aput-object v8, v4, v1

    .line 1315
    aget-object v8, v4, v1

    invoke-virtual {p1, v8}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1316
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    goto/16 :goto_0

    .line 1320
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Film;
    :sswitch_9
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    goto/16 :goto_0

    .line 1324
    :sswitch_a
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    goto/16 :goto_0

    .line 1328
    :sswitch_b
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    goto/16 :goto_0

    .line 1332
    :sswitch_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    iput v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    goto/16 :goto_0

    .line 1336
    :sswitch_d
    const/16 v8, 0x60

    invoke-static {p1, v8}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1338
    .restart local v0    # "arrayLength":I
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    if-nez v8, :cond_10

    move v1, v7

    .line 1339
    .restart local v1    # "i":I
    :goto_a
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1340
    .local v4, "newArray":[I
    if-eqz v1, :cond_f

    .line 1341
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1343
    :cond_f
    :goto_b
    array-length v8, v4

    add-int/lit8 v8, v8, -0x1

    if-ge v1, v8, :cond_11

    .line 1344
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1345
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1343
    add-int/lit8 v1, v1, 0x1

    goto :goto_b

    .line 1338
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_10
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v1, v8

    goto :goto_a

    .line 1348
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_11
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1349
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    goto/16 :goto_0

    .line 1353
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :sswitch_e
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readRawVarint32()I

    move-result v2

    .line 1354
    .restart local v2    # "length":I
    invoke-virtual {p1, v2}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->pushLimit(I)I

    move-result v3

    .line 1356
    .restart local v3    # "limit":I
    const/4 v0, 0x0

    .line 1357
    .restart local v0    # "arrayLength":I
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getPosition()I

    move-result v5

    .line 1358
    .restart local v5    # "startPos":I
    :goto_c
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->getBytesUntilLimit()I

    move-result v8

    if-lez v8, :cond_12

    .line 1359
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    .line 1360
    add-int/lit8 v0, v0, 0x1

    goto :goto_c

    .line 1362
    :cond_12
    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->rewindToPosition(I)V

    .line 1363
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    if-nez v8, :cond_14

    move v1, v7

    .line 1364
    .restart local v1    # "i":I
    :goto_d
    add-int v8, v1, v0

    new-array v4, v8, [I

    .line 1365
    .restart local v4    # "newArray":[I
    if-eqz v1, :cond_13

    .line 1366
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    invoke-static {v8, v7, v4, v7, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1368
    :cond_13
    :goto_e
    array-length v8, v4

    if-ge v1, v8, :cond_15

    .line 1369
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readInt32()I

    move-result v8

    aput v8, v4, v1

    .line 1368
    add-int/lit8 v1, v1, 0x1

    goto :goto_e

    .line 1363
    .end local v1    # "i":I
    .end local v4    # "newArray":[I
    :cond_14
    iget-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v1, v8

    goto :goto_d

    .line 1371
    .restart local v1    # "i":I
    .restart local v4    # "newArray":[I
    :cond_15
    iput-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    .line 1372
    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->popLimit(I)V

    goto/16 :goto_0

    .line 1376
    .end local v0    # "arrayLength":I
    .end local v1    # "i":I
    .end local v2    # "length":I
    .end local v3    # "limit":I
    .end local v4    # "newArray":[I
    .end local v5    # "startPos":I
    :sswitch_f
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    goto/16 :goto_0

    .line 1211
    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x2a -> :sswitch_5
        0x30 -> :sswitch_6
        0x32 -> :sswitch_7
        0x3a -> :sswitch_8
        0x42 -> :sswitch_9
        0x4a -> :sswitch_a
        0x52 -> :sswitch_b
        0x58 -> :sswitch_c
        0x60 -> :sswitch_d
        0x62 -> :sswitch_e
        0x6a -> :sswitch_f
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 863
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Person;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Person;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 5
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1059
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1060
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->mid:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1062
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1063
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->id:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1065
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1066
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->name:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1068
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    if-eqz v3, :cond_3

    .line 1069
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->image:Lcom/google/android/videos/proto/FilmProtos$Image;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1071
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 1072
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    array-length v3, v3

    if-ge v2, v3, :cond_5

    .line 1073
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->character:[Lcom/google/android/videos/proto/FilmProtos$Character;

    aget-object v1, v3, v2

    .line 1074
    .local v1, "element":Lcom/google/android/videos/proto/FilmProtos$Character;
    if-eqz v1, :cond_4

    .line 1075
    const/4 v3, 0x5

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1072
    :cond_4
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 1079
    .end local v1    # "element":Lcom/google/android/videos/proto/FilmProtos$Character;
    .end local v2    # "i":I
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    if-eqz v3, :cond_6

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v3, v3

    if-lez v3, :cond_6

    .line 1080
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    array-length v3, v3

    if-ge v2, v3, :cond_6

    .line 1081
    const/4 v3, 0x6

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->splitId:[I

    aget v4, v4, v2

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1080
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 1084
    .end local v2    # "i":I
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    if-eqz v3, :cond_8

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v3, v3

    if-lez v3, :cond_8

    .line 1085
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_2
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    array-length v3, v3

    if-ge v2, v3, :cond_8

    .line 1086
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmography:[Lcom/google/android/videos/proto/FilmProtos$Film;

    aget-object v1, v3, v2

    .line 1087
    .local v1, "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    if-eqz v1, :cond_7

    .line 1088
    const/4 v3, 0x7

    invoke-virtual {p1, v3, v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1085
    :cond_7
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 1092
    .end local v1    # "element":Lcom/google/android/videos/proto/FilmProtos$Film;
    .end local v2    # "i":I
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_9

    .line 1093
    const/16 v3, 0x8

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1095
    :cond_9
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_a

    .line 1096
    const/16 v3, 0x9

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1098
    :cond_a
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_b

    .line 1099
    const/16 v3, 0xa

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->placeOfBirth:Ljava/lang/String;

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1101
    :cond_b
    iget v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    if-eqz v3, :cond_c

    .line 1102
    const/16 v3, 0xb

    iget v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->localId:I

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32(II)V

    .line 1104
    :cond_c
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    if-eqz v3, :cond_e

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v3, v3

    if-lez v3, :cond_e

    .line 1105
    const/4 v0, 0x0

    .line 1106
    .local v0, "dataSize":I
    const/4 v2, 0x0

    .restart local v2    # "i":I
    :goto_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v3, v3

    if-ge v2, v3, :cond_d

    .line 1107
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    aget v1, v3, v2

    .line 1108
    .local v1, "element":I
    invoke-static {v1}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeInt32SizeNoTag(I)I

    move-result v3

    add-int/2addr v0, v3

    .line 1106
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 1111
    .end local v1    # "element":I
    :cond_d
    const/16 v3, 0x62

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 1112
    invoke-virtual {p1, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeRawVarint32(I)V

    .line 1113
    const/4 v2, 0x0

    :goto_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    array-length v3, v3

    if-ge v2, v3, :cond_e

    .line 1114
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->filmographyIndex:[I

    aget v3, v3, v2

    invoke-virtual {p1, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeInt32NoTag(I)V

    .line 1113
    add-int/lit8 v2, v2, 0x1

    goto :goto_4

    .line 1117
    .end local v0    # "dataSize":I
    .end local v2    # "i":I
    :cond_e
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_f

    .line 1118
    const/16 v3, 0xd

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Person;->appearance:[B

    invoke-virtual {p1, v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1120
    :cond_f
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1121
    return-void
.end method
