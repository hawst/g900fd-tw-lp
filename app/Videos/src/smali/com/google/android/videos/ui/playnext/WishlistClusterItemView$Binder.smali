.class public Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "WishlistClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;",
        "Lcom/google/android/videos/adapter/WishlistDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final moviePosterRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final showPosterRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

.field private final unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 3
    .param p2, "unpurchasedMoviesHelper"    # Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .param p4, "unpurchasedShowsHelper"    # Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .param p5, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .local p1, "moviePosterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p3, "showPosterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 163
    const/4 v0, 0x2

    invoke-direct {p0, p5, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 164
    if-eqz p2, :cond_0

    if-eqz p1, :cond_3

    :cond_0
    move v0, v2

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 165
    if-eqz p4, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    move v1, v2

    :cond_2
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 166
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->moviePosterRequester:Lcom/google/android/videos/async/Requester;

    .line 167
    iput-object p3, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->showPosterRequester:Lcom/google/android/videos/async/Requester;

    .line 168
    iput-object p2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 169
    iput-object p4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 170
    return-void

    :cond_3
    move v0, v1

    .line 164
    goto :goto_0
.end method


# virtual methods
.method protected bridge synthetic onBind(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/android/videos/adapter/DataSource;I)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;
    .param p2, "x1"    # Lcom/google/android/videos/adapter/DataSource;
    .param p3, "x2"    # I

    .prologue
    .line 149
    check-cast p1, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;

    .end local p1    # "x0":Lcom/google/android/videos/ui/playnext/ClusterItemView;
    check-cast p2, Lcom/google/android/videos/adapter/WishlistDataSource;

    .end local p2    # "x1":Lcom/google/android/videos/adapter/DataSource;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->onBind(Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;Lcom/google/android/videos/adapter/WishlistDataSource;I)V

    return-void
.end method

.method protected onBind(Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;Lcom/google/android/videos/adapter/WishlistDataSource;I)V
    .locals 5
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/WishlistDataSource;
    .param p3, "index"    # I

    .prologue
    const/4 v0, 0x0

    .line 174
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    .line 176
    .local v1, "item":Landroid/database/Cursor;
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getItemType(Landroid/database/Cursor;)I

    move-result v4

    sparse-switch v4, :sswitch_data_0

    .line 200
    :cond_0
    :goto_0
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->isRemoving(Landroid/database/Cursor;)Z

    move-result v4

    if-nez v4, :cond_1

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/WishlistDataSource;->isNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_4

    :cond_1
    const/4 v4, 0x1

    :goto_1
    invoke-virtual {p1, v4}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->setDimmedStyle(Z)V

    .line 201
    return-void

    .line 178
    :sswitch_0
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 179
    .local v3, "videoId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    if-nez v4, :cond_2

    .line 181
    .local v0, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_2
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindCommon(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 182
    invoke-virtual {p1, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindMovieSpecific(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 183
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->moviePosterRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p1, v3, v4}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindPoster(Ljava/lang/String;Lcom/google/android/videos/async/Requester;)V

    .line 184
    if-eqz v0, :cond_0

    .line 185
    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_0

    .line 179
    .end local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedMoviesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v4, v3}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    goto :goto_2

    .line 189
    .end local v3    # "videoId":Ljava/lang/String;
    :sswitch_1
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    .line 190
    .local v2, "showId":Ljava/lang/String;
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    if-nez v4, :cond_3

    .line 192
    .restart local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_3
    invoke-virtual {p2, v1}, Lcom/google/android/videos/adapter/WishlistDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {p1, v4, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindCommon(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 193
    invoke-virtual {p1, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindShowSpecific(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 194
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->showPosterRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p1, v2, v4}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bindPoster(Ljava/lang/String;Lcom/google/android/videos/async/Requester;)V

    .line 195
    if-eqz v0, :cond_0

    .line 196
    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_0

    .line 190
    .end local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;->unpurchasedShowsHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {v4, v2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    goto :goto_3

    .line 200
    .end local v2    # "showId":Ljava/lang/String;
    :cond_4
    const/4 v4, 0x0

    goto :goto_1

    .line 176
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method
