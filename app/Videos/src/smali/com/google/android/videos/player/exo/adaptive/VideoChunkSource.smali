.class public abstract Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;
.super Ljava/lang/Object;
.source "VideoChunkSource.java"

# interfaces
.implements Lcom/google/android/exoplayer/ExoPlayer$ExoPlayerComponent;
.implements Lcom/google/android/exoplayer/chunk/ChunkSource;
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;
    }
.end annotation


# instance fields
.field private final bandwidthBucketHistorySelectionPercentile:F

.field private final bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

.field private final bandwidthMeter:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

.field private final dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

.field private final disableHdOnMobileNetwork:Z

.field private final evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

.field private exitingTrickPlay:Z

.field private final extractors:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;",
            ">;"
        }
    .end annotation
.end field

.field private fastNetwork:Z

.field protected final formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

.field private final formats:[Lcom/google/android/exoplayer/chunk/Format;

.field private lastChunkWasInitialization:Z

.field private final maxHeight:I

.field private final maxWidth:I

.field private mobileNetwork:Z

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final numSegmentsPerChunk:I

.field private final representations:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;",
            ">;"
        }
    .end annotation
.end field

.field private final trackInfo:Lcom/google/android/exoplayer/TrackInfo;

.field private trickPlayEnabled:Z


# direct methods
.method public varargs constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;ILandroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V
    .locals 12
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "numSegmentsPerChunk"    # I
    .param p3, "context"    # Landroid/content/Context;
    .param p4, "config"    # Lcom/google/android/videos/Config;
    .param p5, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .param p6, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p7, "disableHdOnMobileNetwork"    # Z
    .param p8, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 113
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 114
    iput-object p1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    .line 115
    iput p2, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->numSegmentsPerChunk:I

    .line 116
    move-object/from16 v0, p8

    array-length v6, v0

    new-array v6, v6, [Lcom/google/android/exoplayer/chunk/Format;

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    .line 117
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->representations:Ljava/util/HashMap;

    .line 118
    new-instance v6, Ljava/util/HashMap;

    invoke-direct {v6}, Ljava/util/HashMap;-><init>()V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    .line 119
    move/from16 v0, p7

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->disableHdOnMobileNetwork:Z

    .line 120
    new-instance v6, Lcom/google/android/exoplayer/TrackInfo;

    const/4 v7, 0x0

    aget-object v7, p8, v7

    iget-object v7, v7, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->mimeType:Ljava/lang/String;

    const/4 v8, 0x0

    aget-object v8, p8, v8

    iget-wide v8, v8, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->periodDurationMs:J

    const-wide/16 v10, 0x3e8

    mul-long/2addr v8, v10

    invoke-direct {v6, v7, v8, v9}, Lcom/google/android/exoplayer/TrackInfo;-><init>(Ljava/lang/String;J)V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    .line 122
    new-instance v6, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    invoke-direct {v6}, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;-><init>()V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    .line 123
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 124
    new-instance v6, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    move-object/from16 v0, p4

    invoke-direct {v6, v0}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;-><init>(Lcom/google/android/videos/Config;)V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    .line 125
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthMeter:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    .line 126
    new-instance v6, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    invoke-virtual {p3}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v7

    invoke-interface/range {p4 .. p4}, Lcom/google/android/videos/Config;->exoBandwidthBucketHistoryMinCount()I

    move-result v8

    invoke-direct {v6, v7, v8}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;-><init>(Landroid/content/Context;I)V

    iput-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    .line 128
    invoke-interface/range {p4 .. p4}, Lcom/google/android/videos/Config;->exoBandwidthBucketHistorySelectionPercentile()F

    move-result v6

    iput v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthBucketHistorySelectionPercentile:F

    .line 130
    const/4 v5, 0x0

    .line 131
    .local v5, "maxWidth":I
    const/4 v4, 0x0

    .line 132
    .local v4, "maxHeight":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    move-object/from16 v0, p8

    array-length v6, v0

    if-ge v3, v6, :cond_0

    .line 133
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v7, p8, v3

    iget-object v7, v7, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    aput-object v7, v6, v3

    .line 134
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, v6, v3

    iget v6, v6, Lcom/google/android/exoplayer/chunk/Format;->width:I

    invoke-static {v6, v5}, Ljava/lang/Math;->max(II)I

    move-result v5

    .line 135
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v6, v6, v3

    iget v6, v6, Lcom/google/android/exoplayer/chunk/Format;->height:I

    invoke-static {v6, v4}, Ljava/lang/Math;->max(II)I

    move-result v4

    .line 136
    new-instance v2, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-direct {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;-><init>()V

    .line 137
    .local v2, "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v6, v7, v2}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 138
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->representations:Ljava/util/HashMap;

    iget-object v7, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    aget-object v7, v7, v3

    iget-object v7, v7, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    aget-object v8, p8, v3

    invoke-virtual {v6, v7, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 132
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 140
    .end local v2    # "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :cond_0
    iput v5, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->maxWidth:I

    .line 141
    iput v4, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->maxHeight:I

    .line 142
    iget-object v6, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    new-instance v7, Lcom/google/android/exoplayer/chunk/Format$DecreasingBandwidthComparator;

    invoke-direct {v7}, Lcom/google/android/exoplayer/chunk/Format$DecreasingBandwidthComparator;-><init>()V

    invoke-static {v6, v7}, Ljava/util/Arrays;->sort([Ljava/lang/Object;Ljava/util/Comparator;)V

    .line 143
    return-void
.end method

.method public varargs constructor <init>(Lcom/google/android/exoplayer/upstream/DataSource;Landroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V
    .locals 9
    .param p1, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "config"    # Lcom/google/android/videos/Config;
    .param p4, "bandwidthMeter"    # Lcom/google/android/exoplayer/upstream/BandwidthMeter;
    .param p5, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p6, "disableHdOnMobileNetwork"    # Z
    .param p7, "representations"    # [Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 107
    const/4 v2, 0x1

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    move-object/from16 v8, p7

    invoke-direct/range {v0 .. v8}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;ILandroid/content/Context;Lcom/google/android/videos/Config;Lcom/google/android/exoplayer/upstream/BandwidthMeter;Lcom/google/android/videos/utils/NetworkStatus;Z[Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    .line 109
    return-void
.end method

.method private static getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J
    .locals 6
    .param p0, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .prologue
    .line 351
    invoke-virtual {p0}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getIndexUri()Lcom/google/android/exoplayer/dash/mpd/RangedUri;

    move-result-object v0

    .line 352
    .local v0, "rangedUri":Lcom/google/android/exoplayer/dash/mpd/RangedUri;
    iget-wide v2, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->start:J

    iget-wide v4, v0, Lcom/google/android/exoplayer/dash/mpd/RangedUri;->length:J

    add-long/2addr v2, v4

    return-wide v2
.end method

.method private newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 8
    .param p1, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p2, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p3, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p4, "trigger"    # I

    .prologue
    .line 316
    new-instance v0, Lcom/google/android/exoplayer/upstream/DataSpec;

    iget-object v1, p1, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    const-wide/16 v2, 0x0

    invoke-static {p1}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v4

    invoke-virtual {p1}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 318
    .local v0, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v1, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;

    move-object v2, p0

    move-object v3, p3

    move-object v4, v0

    move v5, p4

    move-object v6, p2

    move-object v7, p1

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource$InitializationMp4Loadable;-><init>(Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;ILcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)V

    return-object v1
.end method

.method private static newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/parser/SegmentIndex;III)Lcom/google/android/exoplayer/chunk/Chunk;
    .locals 26
    .param p0, "representation"    # Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    .param p1, "extractor"    # Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    .param p2, "dataSource"    # Lcom/google/android/exoplayer/upstream/DataSource;
    .param p3, "sidx"    # Lcom/google/android/exoplayer/parser/SegmentIndex;
    .param p4, "index"    # I
    .param p5, "trigger"    # I
    .param p6, "numSegmentsPerChunk"    # I

    .prologue
    .line 325
    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    sub-int v3, v3, p4

    move/from16 v0, p6

    invoke-static {v0, v3}, Ljava/lang/Math;->min(II)I

    move-result v25

    .line 326
    .local v25, "numSegmentsToFetch":I
    add-int v3, p4, v25

    add-int/lit8 v24, v3, -0x1

    .line 327
    .local v24, "lastSegmentInChunk":I
    move-object/from16 v0, p3

    iget v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->length:I

    add-int/lit8 v3, v3, -0x1

    move/from16 v0, v24

    if-ne v0, v3, :cond_0

    const/16 v18, -0x1

    .line 329
    .local v18, "nextIndex":I
    :goto_0
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v14, v3, p4

    .line 332
    .local v14, "startTimeUs":J
    const/4 v3, -0x1

    move/from16 v0, v18

    if-ne v0, v3, :cond_1

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v8, v3, v24

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    aget-wide v10, v3, v24

    add-long v16, v8, v10

    .line 336
    .local v16, "endTimeUs":J
    :goto_1
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->getIndexAnchor(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;)J

    move-result-wide v8

    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->offsets:[J

    aget-wide v10, v3, p4

    add-long v4, v8, v10

    .line 339
    .local v4, "offset":J
    const-wide/16 v6, 0x0

    .line 340
    .local v6, "size":J
    move/from16 v23, p4

    .local v23, "i":I
    :goto_2
    move/from16 v0, v23

    move/from16 v1, v24

    if-gt v0, v1, :cond_2

    .line 341
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    aget v3, v3, v23

    int-to-long v8, v3

    add-long/2addr v6, v8

    .line 340
    add-int/lit8 v23, v23, 0x1

    goto :goto_2

    .line 327
    .end local v4    # "offset":J
    .end local v6    # "size":J
    .end local v14    # "startTimeUs":J
    .end local v16    # "endTimeUs":J
    .end local v18    # "nextIndex":I
    .end local v23    # "i":I
    :cond_0
    add-int/lit8 v18, v24, 0x1

    goto :goto_0

    .line 332
    .restart local v14    # "startTimeUs":J
    .restart local v18    # "nextIndex":I
    :cond_1
    move-object/from16 v0, p3

    iget-object v3, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    aget-wide v16, v3, v18

    goto :goto_1

    .line 344
    .restart local v4    # "offset":J
    .restart local v6    # "size":J
    .restart local v16    # "endTimeUs":J
    .restart local v23    # "i":I
    :cond_2
    new-instance v2, Lcom/google/android/exoplayer/upstream/DataSpec;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->uri:Landroid/net/Uri;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->getCacheKey()Ljava/lang/String;

    move-result-object v8

    invoke-direct/range {v2 .. v8}, Lcom/google/android/exoplayer/upstream/DataSpec;-><init>(Landroid/net/Uri;JJLjava/lang/String;)V

    .line 346
    .local v2, "dataSpec":Lcom/google/android/exoplayer/upstream/DataSpec;
    new-instance v9, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    const/16 v20, 0x0

    const-wide/16 v21, 0x0

    move-object/from16 v10, p2

    move-object v11, v2

    move/from16 v13, p5

    move-object/from16 v19, p1

    invoke-direct/range {v9 .. v22}, Lcom/google/android/exoplayer/chunk/Mp4MediaChunk;-><init>(Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/upstream/DataSpec;Lcom/google/android/exoplayer/chunk/Format;IJJILcom/google/android/exoplayer/parser/Extractor;ZJ)V

    return-object v9
.end method


# virtual methods
.method public continueBuffering(J)V
    .locals 0
    .param p1, "playbackPositionUs"    # J

    .prologue
    .line 187
    return-void
.end method

.method public disable(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 155
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    invoke-virtual {v0}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->saveHistory()V

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 157
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->exitingTrickPlay:Z

    .line 158
    return-void
.end method

.method public enable()V
    .locals 1

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    invoke-virtual {v0}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->loadHistory()V

    .line 150
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 151
    return-void
.end method

.method public abstract evaluate(Ljava/util/List;JJ[Lcom/google/android/exoplayer/chunk/Format;JZZZZLcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ[",
            "Lcom/google/android/exoplayer/chunk/Format;",
            "JZZZZ",
            "Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;",
            ")Z"
        }
    .end annotation
.end method

.method protected final getChunkDurationUs(Lcom/google/android/exoplayer/chunk/Format;I)J
    .locals 5
    .param p1, "format"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "chunkIndex"    # I

    .prologue
    const-wide/16 v2, -0x1

    .line 281
    if-nez p1, :cond_1

    .line 285
    :cond_0
    :goto_0
    return-wide v2

    .line 284
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    iget-object v4, p1, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v1, v4}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v0

    .line 285
    .local v0, "segmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    if-eqz v0, :cond_0

    iget-object v1, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->durationsUs:[J

    aget-wide v2, v1, p2

    goto :goto_0
.end method

.method protected final getChunkIndex(J)I
    .locals 5
    .param p1, "seekPositionUs"    # J

    .prologue
    .line 294
    const/4 v1, 0x0

    .line 296
    .local v1, "chunkIndex":I
    const/4 v2, 0x0

    .line 297
    .local v2, "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    iget-object v4, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    invoke-virtual {v4}, Ljava/util/HashMap;->values()Ljava/util/Collection;

    move-result-object v4

    invoke-interface {v4}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v3

    .local v3, "i$":Ljava/util/Iterator;
    :cond_0
    invoke-interface {v3}, Ljava/util/Iterator;->hasNext()Z

    move-result v4

    if-eqz v4, :cond_1

    invoke-interface {v3}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    .line 298
    .local v0, "candidateExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    invoke-virtual {v0}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v4

    if-eqz v4, :cond_0

    .line 299
    move-object v2, v0

    .line 303
    .end local v0    # "candidateExtractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    :cond_1
    if-eqz v2, :cond_2

    .line 304
    invoke-virtual {v2}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    invoke-static {v4, p1, p2}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v1

    .line 305
    if-gez v1, :cond_2

    neg-int v4, v1

    add-int/lit8 v1, v4, -0x2

    .line 307
    :cond_2
    return v1
.end method

.method public getChunkOperation(Ljava/util/List;JJLcom/google/android/exoplayer/chunk/ChunkOperationHolder;)V
    .locals 20
    .param p2, "seekPositionUs"    # J
    .param p4, "playbackPositionUs"    # J
    .param p6, "out"    # Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<+",
            "Lcom/google/android/exoplayer/chunk/MediaChunk;",
            ">;JJ",
            "Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 192
    .local p1, "queue":Ljava/util/List;, "Ljava/util/List<+Lcom/google/android/exoplayer/chunk/MediaChunk;>;"
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthMeter:Lcom/google/android/exoplayer/upstream/BandwidthMeter;

    invoke-interface {v4}, Lcom/google/android/exoplayer/upstream/BandwidthMeter;->getBitrateEstimate()J

    move-result-wide v9

    .line 193
    .local v9, "bitrateEstimate":J
    const-wide/16 v4, -0x1

    cmp-long v4, v9, v4

    if-eqz v4, :cond_0

    .line 194
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    invoke-virtual {v4, v9, v10}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->addCount(J)V

    .line 197
    :cond_0
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    iput v5, v4, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    .line 198
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget-object v4, v4, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    if-eqz v4, :cond_1

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->lastChunkWasInitialization:Z

    if-nez v4, :cond_2

    .line 199
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->exitingTrickPlay:Z

    move/from16 v19, v0

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formats:[Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v0, p0

    iget-boolean v11, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->fastNetwork:Z

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->disableHdOnMobileNetwork:Z

    if-eqz v4, :cond_4

    move-object/from16 v0, p0

    iget-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->mobileNetwork:Z

    if-eqz v4, :cond_4

    const/4 v12, 0x1

    :goto_0
    move-object/from16 v0, p0

    iget-boolean v13, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trickPlayEnabled:Z

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->exitingTrickPlay:Z

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    move-object/from16 v2, p0

    move-object/from16 v3, p1

    move-wide/from16 v4, p2

    move-wide/from16 v6, p4

    invoke-virtual/range {v2 .. v15}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluate(Ljava/util/List;JJ[Lcom/google/android/exoplayer/chunk/Format;JZZZZLcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;)Z

    move-result v4

    and-int v4, v4, v19

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->exitingTrickPlay:Z

    .line 203
    :cond_2
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget-object v0, v4, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->format:Lcom/google/android/exoplayer/chunk/Format;

    move-object/from16 v18, v0

    .line 204
    .local v18, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v4, v4, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->queueSize:I

    move-object/from16 v0, p6

    iput v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    .line 206
    if-nez v18, :cond_5

    .line 207
    const/4 v4, 0x0

    move-object/from16 v0, p6

    iput-object v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    .line 243
    :cond_3
    :goto_1
    return-void

    .line 199
    .end local v18    # "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    :cond_4
    const/4 v12, 0x0

    goto :goto_0

    .line 209
    .restart local v18    # "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    :cond_5
    move-object/from16 v0, p6

    iget v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    invoke-interface/range {p1 .. p1}, Ljava/util/List;->size()I

    move-result v5

    if-ne v4, v5, :cond_6

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    if-eqz v4, :cond_6

    move-object/from16 v0, p6

    iget-object v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    iget-object v4, v4, Lcom/google/android/exoplayer/chunk/Chunk;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v4, v4, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    if-eq v4, v5, :cond_3

    .line 216
    :cond_6
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->representations:Ljava/util/HashMap;

    move-object/from16 v0, v18

    iget-object v5, v0, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;

    .line 217
    .local v2, "selectedRepresentation":Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    iget-object v5, v2, Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;->format:Lcom/google/android/exoplayer/chunk/Format;

    iget-object v5, v5, Lcom/google/android/exoplayer/chunk/Format;->id:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    .line 218
    .local v3, "extractor":Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;
    invoke-virtual {v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getFormat()Lcom/google/android/exoplayer/MediaFormat;

    move-result-object v4

    if-nez v4, :cond_7

    .line 219
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v5, v5, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->newInitializationChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;I)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v16

    .line 221
    .local v16, "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    const/4 v4, 0x1

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->lastChunkWasInitialization:Z

    .line 222
    move-object/from16 v0, v16

    move-object/from16 v1, p6

    iput-object v0, v1, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto :goto_1

    .line 227
    .end local v16    # "initializationChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    :cond_7
    invoke-interface/range {p1 .. p1}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-eqz v4, :cond_9

    .line 228
    invoke-virtual {v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v4

    iget-object v4, v4, Lcom/google/android/exoplayer/parser/SegmentIndex;->timesUs:[J

    move-wide/from16 v0, p2

    invoke-static {v4, v0, v1}, Ljava/util/Arrays;->binarySearch([JJ)I

    move-result v6

    .line 229
    .local v6, "nextIndex":I
    if-gez v6, :cond_8

    neg-int v4, v6

    add-int/lit8 v6, v4, -0x2

    .line 234
    :cond_8
    :goto_2
    const/4 v4, -0x1

    if-ne v6, v4, :cond_a

    .line 235
    const/4 v4, 0x0

    move-object/from16 v0, p6

    iput-object v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto :goto_1

    .line 231
    .end local v6    # "nextIndex":I
    :cond_9
    move-object/from16 v0, p6

    iget v4, v0, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->queueSize:I

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v4}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/exoplayer/chunk/MediaChunk;

    iget v6, v4, Lcom/google/android/exoplayer/chunk/MediaChunk;->nextChunkIndex:I

    .restart local v6    # "nextIndex":I
    goto :goto_2

    .line 239
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->dataSource:Lcom/google/android/exoplayer/upstream/DataSource;

    invoke-virtual {v3}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->evaluation:Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;

    iget v7, v7, Lcom/google/android/exoplayer/chunk/FormatEvaluator$Evaluation;->trigger:I

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->numSegmentsPerChunk:I

    invoke-static/range {v2 .. v8}, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->newMediaChunk(Lcom/google/android/exoplayer/dash/mpd/Representation$SingleSegmentRepresentation;Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;Lcom/google/android/exoplayer/upstream/DataSource;Lcom/google/android/exoplayer/parser/SegmentIndex;III)Lcom/google/android/exoplayer/chunk/Chunk;

    move-result-object v17

    .line 241
    .local v17, "nextMediaChunk":Lcom/google/android/exoplayer/chunk/Chunk;
    const/4 v4, 0x0

    move-object/from16 v0, p0

    iput-boolean v4, v0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->lastChunkWasInitialization:Z

    .line 242
    move-object/from16 v0, v17

    move-object/from16 v1, p6

    iput-object v0, v1, Lcom/google/android/exoplayer/chunk/ChunkOperationHolder;->chunk:Lcom/google/android/exoplayer/chunk/Chunk;

    goto/16 :goto_1
.end method

.method protected final getChunkSizes(Ljava/lang/String;)[I
    .locals 2
    .param p1, "formatId"    # Ljava/lang/String;

    .prologue
    .line 276
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->extractors:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;

    invoke-virtual {v1}, Lcom/google/android/exoplayer/parser/mp4/FragmentedMp4Extractor;->getIndex()Lcom/google/android/exoplayer/parser/SegmentIndex;

    move-result-object v0

    .line 277
    .local v0, "segmentIndex":Lcom/google/android/exoplayer/parser/SegmentIndex;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, v0, Lcom/google/android/exoplayer/parser/SegmentIndex;->sizes:[I

    goto :goto_0
.end method

.method public getError()Ljava/io/IOException;
    .locals 1

    .prologue
    .line 169
    const/4 v0, 0x0

    return-object v0
.end method

.method protected final getHistoricalBitrateEstimate()J
    .locals 2

    .prologue
    .line 311
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthBucketHistorySelectionPercentile:F

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->getBitrate(F)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getMaxVideoDimensions(Lcom/google/android/exoplayer/MediaFormat;)V
    .locals 2
    .param p1, "out"    # Lcom/google/android/exoplayer/MediaFormat;

    .prologue
    .line 174
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    iget-object v0, v0, Lcom/google/android/exoplayer/TrackInfo;->mimeType:Ljava/lang/String;

    const-string v1, "video"

    invoke-virtual {v0, v1}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 175
    iget v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->maxWidth:I

    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->maxHeight:I

    invoke-virtual {p1, v0, v1}, Lcom/google/android/exoplayer/MediaFormat;->setMaxVideoDimensions(II)V

    .line 177
    :cond_0
    return-void
.end method

.method public final getTrackInfo()Lcom/google/android/exoplayer/TrackInfo;
    .locals 1

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trackInfo:Lcom/google/android/exoplayer/TrackInfo;

    return-object v0
.end method

.method public handleMessage(ILjava/lang/Object;)V
    .locals 3
    .param p1, "what"    # I
    .param p2, "msg"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ExoPlaybackException;
        }
    .end annotation

    .prologue
    const/4 v1, 0x1

    .line 247
    if-nez p1, :cond_2

    .line 248
    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "msg":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 249
    .local v0, "newTrickPlayEnabled":Z
    iget-boolean v2, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trickPlayEnabled:Z

    if-eqz v2, :cond_1

    if-nez v0, :cond_1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->exitingTrickPlay:Z

    .line 250
    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->trickPlayEnabled:Z

    .line 254
    .end local v0    # "newTrickPlayEnabled":Z
    :cond_0
    :goto_1
    return-void

    .line 249
    .restart local v0    # "newTrickPlayEnabled":Z
    :cond_1
    const/4 v1, 0x0

    goto :goto_0

    .line 251
    .end local v0    # "newTrickPlayEnabled":Z
    .restart local p2    # "msg":Ljava/lang/Object;
    :cond_2
    if-ne p1, v1, :cond_0

    .line 252
    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->formatSelectionHelper:Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "msg":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->setAudioBitrate(I)V

    goto :goto_1
.end method

.method public onChunkLoadError(Lcom/google/android/exoplayer/chunk/Chunk;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "chunk"    # Lcom/google/android/exoplayer/chunk/Chunk;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 259
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isFastNetwork()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->fastNetwork:Z

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetwork()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->mobileNetwork:Z

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->bandwidthHistoryManager:Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;

    iget-object v1, p0, Lcom/google/android/videos/player/exo/adaptive/VideoChunkSource;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->getNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/exo/adaptive/BandwidthBucketHistoryManager;->updateCurrentHistory(Landroid/net/NetworkInfo;)V

    .line 165
    return-void
.end method
