.class public final Lcom/google/android/videos/pano/model/LibraryItem;
.super Ljava/lang/Object;
.source "LibraryItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# instance fields
.field private final entityId:Ljava/lang/String;

.field private hashCode:I

.field private final imageResource:I

.field private final intent:Landroid/content/Intent;

.field private final title:I


# direct methods
.method public constructor <init>(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "imageResource"    # I
    .param p2, "title"    # I
    .param p3, "intent"    # Landroid/content/Intent;

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    .line 23
    iput p1, p0, Lcom/google/android/videos/pano/model/LibraryItem;->imageResource:I

    .line 24
    iput p2, p0, Lcom/google/android/videos/pano/model/LibraryItem;->title:I

    .line 25
    iput-object p3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->intent:Landroid/content/Intent;

    .line 26
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->entityId:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    if-ne p0, p1, :cond_1

    .line 56
    :cond_0
    :goto_0
    return v1

    .line 47
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/pano/model/LibraryItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 48
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 50
    check-cast v0, Lcom/google/android/videos/pano/model/LibraryItem;

    .line 52
    .local v0, "i":Lcom/google/android/videos/pano/model/LibraryItem;
    iget v3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    if-eqz v3, :cond_3

    iget v3, v0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    iget v4, v0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 53
    goto :goto_0

    .line 56
    :cond_3
    iget v3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->imageResource:I

    iget v4, v0, Lcom/google/android/videos/pano/model/LibraryItem;->imageResource:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->title:I

    iget v4, v0, Lcom/google/android/videos/pano/model/LibraryItem;->title:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/pano/model/LibraryItem;->intent:Landroid/content/Intent;

    iget-object v4, v0, Lcom/google/android/videos/pano/model/LibraryItem;->intent:Landroid/content/Intent;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Landroid/content/Intent;Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->entityId:Ljava/lang/String;

    return-object v0
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 60
    iget v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->imageResource:I

    return v0
.end method

.method public getIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->intent:Landroid/content/Intent;

    return-object v0
.end method

.method public getTitle()I
    .locals 1

    .prologue
    .line 64
    iget v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->title:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 31
    iget v1, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    if-nez v1, :cond_0

    .line 32
    const/16 v0, 0x11

    .line 33
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/pano/model/LibraryItem;->imageResource:I

    add-int/lit16 v0, v1, 0x20f

    .line 34
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/pano/model/LibraryItem;->title:I

    add-int v0, v1, v2

    .line 35
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/pano/model/LibraryItem;->intent:Landroid/content/Intent;

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Landroid/content/Intent;)I

    move-result v2

    add-int v0, v1, v2

    .line 36
    iput v0, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    .line 38
    .end local v0    # "result":I
    :cond_0
    iget v1, p0, Lcom/google/android/videos/pano/model/LibraryItem;->hashCode:I

    return v1
.end method
