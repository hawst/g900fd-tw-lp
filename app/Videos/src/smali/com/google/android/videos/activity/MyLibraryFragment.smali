.class public Lcom/google/android/videos/activity/MyLibraryFragment;
.super Lcom/google/android/videos/activity/HomeFragment;
.source "MyLibraryFragment.java"

# interfaces
.implements Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;,
        Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
    }
.end annotation


# instance fields
.field private final currentVerticals:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

.field private signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

.field private viewPager:Landroid/support/v4/view/ViewPager;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 65
    invoke-direct {p0}, Lcom/google/android/videos/activity/HomeFragment;-><init>()V

    .line 66
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/MyLibraryFragment;)Landroid/support/v4/view/ViewPager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/support/v4/view/ViewPager;)Landroid/support/v4/view/ViewPager;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;
    .param p1, "x1"    # Landroid/support/v4/view/ViewPager;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/MyLibraryFragment;)Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    return-object v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/activity/MyLibraryFragment;Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;)Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;
    .param p1, "x1"    # Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    .prologue
    .line 30
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/activity/MyLibraryFragment;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/activity/MyLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->setupPager()V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/activity/MyLibraryFragment;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->tryRestorePager()Z

    move-result v0

    return v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/activity/MyLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->selectInitialVertical()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/activity/MyLibraryFragment;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/activity/MyLibraryFragment;

    .prologue
    .line 30
    invoke-direct {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->savePager()V

    return-void
.end method

.method private addVertical(I)V
    .locals 2
    .param p1, "vertical"    # I

    .prologue
    .line 255
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 257
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->notifyDataSetChanged()V

    .line 259
    :cond_0
    return-void
.end method

.method public static getExtendedActionBarHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 57
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    mul-int/lit8 v0, v0, 0x3

    return v0
.end method

.method public static newInstance()Lcom/google/android/videos/activity/MyLibraryFragment;
    .locals 2

    .prologue
    .line 51
    new-instance v0, Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-direct {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;-><init>()V

    .line 52
    .local v0, "fragment":Lcom/google/android/videos/activity/MyLibraryFragment;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    invoke-virtual {v0, v1}, Lcom/google/android/videos/activity/MyLibraryFragment;->setArguments(Landroid/os/Bundle;)V

    .line 53
    return-object v0
.end method

.method private removeVertical(I)V
    .locals 2
    .param p1, "vertical"    # I

    .prologue
    .line 262
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 263
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->pagerAdapter:Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/MyLibraryFragment$LibraryPagerAdapter;->notifyDataSetChanged()V

    .line 265
    :cond_0
    return-void
.end method

.method private savePager()V
    .locals 4

    .prologue
    .line 216
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 217
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    const-string v2, "vertical"

    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v3}, Landroid/support/v4/view/ViewPager;->getCurrentItem()I

    move-result v3

    invoke-virtual {v0, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-virtual {v1, v2, v0}, Landroid/os/Bundle;->putInt(Ljava/lang/String;I)V

    .line 219
    :cond_0
    return-void
.end method

.method private selectInitialVertical()V
    .locals 5

    .prologue
    .line 232
    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v4}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 233
    .local v0, "account":Ljava/lang/String;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v2

    .line 234
    .local v2, "downloadedOnly":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v4

    if-ge v3, v4, :cond_0

    .line 235
    const/4 v1, -0x1

    .line 236
    .local v1, "contentType":I
    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/Integer;

    invoke-virtual {v4}, Ljava/lang/Integer;->intValue()I

    move-result v4

    packed-switch v4, :pswitch_data_0

    .line 246
    :goto_1
    const/4 v4, -0x1

    if-eq v1, v4, :cond_1

    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v4, v0, v1, v2}, Lcom/google/android/videos/ui/VerticalsHelper;->hasContent(Ljava/lang/String;IZ)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 248
    iget-object v4, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v4, v3}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 252
    .end local v1    # "contentType":I
    :cond_0
    return-void

    .line 238
    .restart local v1    # "contentType":I
    :pswitch_0
    const/4 v1, 0x1

    .line 239
    goto :goto_1

    .line 242
    :pswitch_1
    const/4 v1, 0x2

    goto :goto_1

    .line 234
    :cond_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 236
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method private setupPager()V
    .locals 5

    .prologue
    const/4 v4, 0x2

    const/4 v3, 0x1

    .line 200
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v2, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v0

    .line 202
    .local v0, "verticals":I
    and-int/lit8 v1, v0, 0x1

    if-eqz v1, :cond_0

    .line 203
    invoke-direct {p0, v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->addVertical(I)V

    .line 208
    :goto_0
    and-int/lit8 v1, v0, 0x2

    if-eqz v1, :cond_1

    .line 209
    invoke-direct {p0, v4}, Lcom/google/android/videos/activity/MyLibraryFragment;->addVertical(I)V

    .line 213
    :goto_1
    return-void

    .line 205
    :cond_0
    invoke-direct {p0, v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->removeVertical(I)V

    goto :goto_0

    .line 211
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/videos/activity/MyLibraryFragment;->removeVertical(I)V

    goto :goto_1
.end method

.method private tryRestorePager()Z
    .locals 5

    .prologue
    const/4 v1, 0x0

    .line 222
    iget-object v2, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v3

    const-string v4, "vertical"

    invoke-virtual {v3, v4, v1}, Landroid/os/Bundle;->getInt(Ljava/lang/String;I)I

    move-result v3

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 224
    .local v0, "verticalIndex":I
    const/4 v2, -0x1

    if-eq v0, v2, :cond_0

    .line 225
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 226
    const/4 v1, 0x1

    .line 228
    :cond_0
    return v1
.end method


# virtual methods
.method protected createHeaderListLayoutConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Lcom/google/android/videos/activity/MyLibraryFragment$1;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/activity/MyLibraryFragment$1;-><init>(Lcom/google/android/videos/activity/MyLibraryFragment;Landroid/content/Context;)V

    return-object v0
.end method

.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 97
    const v0, 0x7f0b009a

    return v0
.end method

.method protected getUiElementType()I
    .locals 1

    .prologue
    .line 80
    const/4 v0, -0x1

    return v0
.end method

.method protected onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    .locals 2
    .param p1, "homeActivity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    .line 72
    invoke-virtual {p2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 73
    invoke-virtual {p2}, Lcom/google/android/videos/VideosGlobals;->getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    .line 74
    invoke-virtual {p2}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 75
    new-instance v0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;

    invoke-virtual {p1}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;-><init>(Lcom/google/android/videos/activity/MyLibraryFragment;Lcom/google/android/videos/ui/SyncHelper;)V

    return-object v0
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 3
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    .line 187
    invoke-super {p0, p1, p2, p3}, Lcom/google/android/videos/activity/HomeFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;

    move-result-object v0

    .line 188
    .local v0, "view":Landroid/view/View;
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    new-instance v2, Lcom/google/android/videos/activity/MyLibraryFragment$2;

    invoke-direct {v2, p0}, Lcom/google/android/videos/activity/MyLibraryFragment$2;-><init>(Lcom/google/android/videos/activity/MyLibraryFragment;)V

    invoke-virtual {v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setOnPageChangeListener(Landroid/support/v4/view/ViewPager$OnPageChangeListener;)V

    .line 196
    return-object v0
.end method

.method public onEnabledVerticalsChanged(Ljava/lang/String;I)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 107
    invoke-direct {p0}, Lcom/google/android/videos/activity/MyLibraryFragment;->setupPager()V

    .line 108
    return-void
.end method

.method public onHasContentChanged(Ljava/lang/String;Z)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContent"    # Z

    .prologue
    .line 103
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 85
    invoke-super {p0}, Lcom/google/android/videos/activity/HomeFragment;->onStart()V

    .line 86
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 87
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 91
    invoke-super {p0}, Lcom/google/android/videos/activity/HomeFragment;->onStop()V

    .line 92
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 93
    return-void
.end method

.method public selectVertical(I)V
    .locals 3
    .param p1, "vertical"    # I

    .prologue
    .line 268
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    .line 269
    .local v0, "verticalIndex":I
    const/4 v1, -0x1

    if-eq v0, v1, :cond_0

    .line 270
    iget-object v1, p0, Lcom/google/android/videos/activity/MyLibraryFragment;->viewPager:Landroid/support/v4/view/ViewPager;

    invoke-virtual {v1, v0}, Landroid/support/v4/view/ViewPager;->setCurrentItem(I)V

    .line 272
    :cond_0
    return-void
.end method
