.class final Lcom/google/android/videos/utils/ByteArrayPool$1;
.super Ljava/lang/Object;
.source "ByteArrayPool.java"

# interfaces
.implements Ljava/util/Comparator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/utils/ByteArrayPool;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Ljava/util/Comparator",
        "<",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public compare(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)I
    .locals 2
    .param p1, "lhs"    # Lcom/google/android/videos/utils/ByteArray;
    .param p2, "rhs"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 62
    iget v0, p1, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    iget v1, p2, Lcom/google/android/videos/utils/ByteArray;->capacity:I

    sub-int/2addr v0, v1

    return v0
.end method

.method public bridge synthetic compare(Ljava/lang/Object;Ljava/lang/Object;)I
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 59
    check-cast p1, Lcom/google/android/videos/utils/ByteArray;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/utils/ByteArrayPool$1;->compare(Lcom/google/android/videos/utils/ByteArray;Lcom/google/android/videos/utils/ByteArray;)I

    move-result v0

    return v0
.end method
