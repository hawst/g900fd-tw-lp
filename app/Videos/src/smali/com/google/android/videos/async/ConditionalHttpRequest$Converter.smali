.class final Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;
.super Ljava/lang/Object;
.source "ConditionalHttpRequest.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/ConditionalHttpRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "Converter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/async/ConditionalHttpRequest",
        "<TR;>;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final sourceConverter:Lcom/google/android/videos/converter/RequestConverter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/converter/RequestConverter;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/converter/RequestConverter",
            "<TR;",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;, "Lcom/google/android/videos/async/ConditionalHttpRequest$Converter<TR;>;"
    .local p1, "sourceConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<TR;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 74
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/converter/RequestConverter;

    iput-object v0, p0, Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;->sourceConverter:Lcom/google/android/videos/converter/RequestConverter;

    .line 75
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;, "Lcom/google/android/videos/async/ConditionalHttpRequest$Converter<TR;>;"
    check-cast p1, Lcom/google/android/videos/async/ConditionalHttpRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;->convertRequest(Lcom/google/android/videos/async/ConditionalHttpRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/async/ConditionalHttpRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ConditionalHttpRequest",
            "<TR;>;)",
            "Lorg/apache/http/client/methods/HttpUriRequest;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 80
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;, "Lcom/google/android/videos/async/ConditionalHttpRequest$Converter<TR;>;"
    .local p1, "request":Lcom/google/android/videos/async/ConditionalHttpRequest;, "Lcom/google/android/videos/async/ConditionalHttpRequest<TR;>;"
    iget-object v1, p0, Lcom/google/android/videos/async/ConditionalHttpRequest$Converter;->sourceConverter:Lcom/google/android/videos/converter/RequestConverter;

    iget-object v2, p1, Lcom/google/android/videos/async/ConditionalHttpRequest;->targetRequest:Ljava/lang/Object;

    invoke-interface {v1, v2}, Lcom/google/android/videos/converter/RequestConverter;->convertRequest(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lorg/apache/http/client/methods/HttpUriRequest;

    .line 81
    .local v0, "httpRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    iget-object v1, p1, Lcom/google/android/videos/async/ConditionalHttpRequest;->eTag:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 82
    const-string v1, "If-None-Match"

    iget-object v2, p1, Lcom/google/android/videos/async/ConditionalHttpRequest;->eTag:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 84
    :cond_0
    iget-object v1, p1, Lcom/google/android/videos/async/ConditionalHttpRequest;->lastModified:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 85
    const-string v1, "If-Modified-Since"

    iget-object v2, p1, Lcom/google/android/videos/async/ConditionalHttpRequest;->lastModified:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 87
    :cond_1
    return-object v0
.end method
