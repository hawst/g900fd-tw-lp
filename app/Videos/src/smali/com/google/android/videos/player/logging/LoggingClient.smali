.class public interface abstract Lcom/google/android/videos/player/logging/LoggingClient;
.super Ljava/lang/Object;
.source "LoggingClient.java"


# virtual methods
.method public abstract endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V
.end method

.method public abstract onBandwidthSample(IIJJ)V
.end method

.method public abstract onDroppedFrames(II)V
.end method

.method public abstract onError(IIIILjava/lang/Exception;)V
.end method

.method public abstract onFailed(II)V
.end method

.method public abstract onFormatEnabled(III)V
.end method

.method public abstract onFormatSelected(III)V
.end method

.method public abstract onHttpDataSourceOpened(IJ)V
.end method

.method public abstract onLoadingChanged(IZ)V
.end method

.method public abstract onNetworkType(III)V
.end method

.method public abstract onStateChanged(IIZI)V
.end method

.method public abstract onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
.end method

.method public abstract onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
.end method

.method public abstract onUserSeekingChanged(IIZZ)V
.end method

.method public abstract startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
.end method

.method public abstract supportsOfflinePlaybacks()Z
.end method
