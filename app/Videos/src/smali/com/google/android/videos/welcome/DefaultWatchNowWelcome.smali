.class public Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;
.super Lcom/google/android/videos/welcome/Welcome;
.source "DefaultWatchNowWelcome.java"


# instance fields
.field private account:Ljava/lang/String;

.field private actionResIds:[I

.field private final activity:Landroid/app/Activity;

.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private detailMessage:Ljava/lang/String;

.field private freeMoviesBrowseUri:Landroid/net/Uri;

.field private freeShowsBrowseUri:Landroid/net/Uri;

.field private showsLaunched:Z

.field private title:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 32
    const-string v0, "defaultWatchNow"

    invoke-direct {p0, v0}, Lcom/google/android/videos/welcome/Welcome;-><init>(Ljava/lang/String;)V

    .line 33
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    .line 34
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->config:Lcom/google/android/videos/Config;

    .line 35
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 36
    return-void
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 91
    const/4 v0, 0x0

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 86
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->detailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 96
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->title:Ljava/lang/String;

    return-object v0
.end method

.method public onAction(I)V
    .locals 4
    .param p1, "index"    # I

    .prologue
    const/16 v3, 0x8

    .line 106
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    array-length v0, v0

    const/4 v1, 0x2

    if-ne v0, v1, :cond_1

    .line 107
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    if-eqz v0, :cond_0

    .line 108
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesAndShows(Landroid/app/Activity;Ljava/lang/String;I)V

    .line 126
    :goto_0
    return-void

    .line 110
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0

    .line 112
    :cond_1
    if-nez p1, :cond_3

    .line 114
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->freeMoviesBrowseUri:Landroid/net/Uri;

    if-eqz v0, :cond_2

    .line 115
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->freeMoviesBrowseUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_0

    .line 117
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0

    .line 120
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->freeShowsBrowseUri:Landroid/net/Uri;

    if-eqz v0, :cond_4

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->freeShowsBrowseUri:Landroid/net/Uri;

    iget-object v2, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_0

    .line 123
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowsVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public prepareIfEligible(Ljava/lang/String;ZZ)Z
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    .line 41
    if-nez p2, :cond_0

    if-eqz p3, :cond_1

    .line 42
    :cond_0
    const/4 v5, 0x0

    .line 76
    :goto_0
    return v5

    .line 44
    :cond_1
    iput-object p1, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->account:Ljava/lang/String;

    .line 45
    iget-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v5

    iput-boolean v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    .line 46
    iget-boolean v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    if-eqz v5, :cond_2

    iget-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->showsWelcomeFreeBrowseUri()Landroid/net/Uri;

    move-result-object v2

    .line 47
    .local v2, "freeShowsBrowseUri":Landroid/net/Uri;
    :goto_1
    if-eqz v2, :cond_3

    const/4 v4, 0x1

    .line 48
    .local v4, "showsFree":Z
    :goto_2
    iget-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->moviesWelcomeFreeBrowseUri()Landroid/net/Uri;

    move-result-object v1

    .line 49
    .local v1, "freeMoviesBrowseUri":Landroid/net/Uri;
    if-eqz v1, :cond_4

    const/4 v3, 0x1

    .line 50
    .local v3, "moviesFree":Z
    :goto_3
    iget-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    const v6, 0x7f0b00e8

    invoke-virtual {v5, v6}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->title:Ljava/lang/String;

    .line 51
    iget-boolean v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    if-eqz v5, :cond_5

    const v0, 0x7f0b00ea

    .line 53
    .local v0, "detailResId":I
    :goto_4
    iget-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->activity:Landroid/app/Activity;

    const v9, 0x7f0b007f

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    invoke-virtual {v5, v0, v6}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->detailMessage:Ljava/lang/String;

    .line 54
    if-eqz v3, :cond_6

    if-eqz v4, :cond_6

    .line 55
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_0

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    .line 76
    :goto_5
    const/4 v5, 0x1

    goto :goto_0

    .line 46
    .end local v0    # "detailResId":I
    .end local v1    # "freeMoviesBrowseUri":Landroid/net/Uri;
    .end local v2    # "freeShowsBrowseUri":Landroid/net/Uri;
    .end local v3    # "moviesFree":Z
    .end local v4    # "showsFree":Z
    :cond_2
    const/4 v2, 0x0

    goto :goto_1

    .line 47
    .restart local v2    # "freeShowsBrowseUri":Landroid/net/Uri;
    :cond_3
    const/4 v4, 0x0

    goto :goto_2

    .line 49
    .restart local v1    # "freeMoviesBrowseUri":Landroid/net/Uri;
    .restart local v4    # "showsFree":Z
    :cond_4
    const/4 v3, 0x0

    goto :goto_3

    .line 51
    .restart local v3    # "moviesFree":Z
    :cond_5
    const v0, 0x7f0b00e9

    goto :goto_4

    .line 58
    .restart local v0    # "detailResId":I
    :cond_6
    if-eqz v3, :cond_7

    iget-boolean v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    if-eqz v5, :cond_7

    .line 59
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_1

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    goto :goto_5

    .line 62
    :cond_7
    if-eqz v4, :cond_8

    .line 63
    const/4 v5, 0x4

    new-array v5, v5, [I

    fill-array-data v5, :array_2

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    goto :goto_5

    .line 66
    :cond_8
    if-eqz v3, :cond_9

    .line 67
    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_3

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    goto :goto_5

    .line 69
    :cond_9
    iget-boolean v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->showsLaunched:Z

    if-eqz v5, :cond_a

    .line 70
    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_4

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    goto :goto_5

    .line 73
    :cond_a
    const/4 v5, 0x2

    new-array v5, v5, [I

    fill-array-data v5, :array_5

    iput-object v5, p0, Lcom/google/android/videos/welcome/DefaultWatchNowWelcome;->actionResIds:[I

    goto :goto_5

    .line 55
    :array_0
    .array-data 4
        0x7f0201c2
        0x7f0b00f5
        0x7f0201c2
        0x7f0b00f7
    .end array-data

    .line 59
    :array_1
    .array-data 4
        0x7f0201c2
        0x7f0b00f5
        0x7f0201c2
        0x7f0b00f6
    .end array-data

    .line 63
    :array_2
    .array-data 4
        0x7f0201c2
        0x7f0b00f4
        0x7f0201c2
        0x7f0b00f7
    .end array-data

    .line 67
    :array_3
    .array-data 4
        0x7f0201c2
        0x7f0b00f5
    .end array-data

    .line 70
    :array_4
    .array-data 4
        0x7f0201c2
        0x7f0b00f3
    .end array-data

    .line 73
    :array_5
    .array-data 4
        0x7f0201c2
        0x7f0b00f4
    .end array-data
.end method
