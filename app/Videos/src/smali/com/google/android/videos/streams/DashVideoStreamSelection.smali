.class public Lcom/google/android/videos/streams/DashVideoStreamSelection;
.super Ljava/lang/Object;
.source "DashVideoStreamSelection.java"


# instance fields
.field public final hi:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final isHiHd:Z

.field public final lo:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;"
        }
    .end annotation
.end field

.field public final supportsQualityToggle:Z


# direct methods
.method public constructor <init>(Ljava/util/List;Ljava/util/List;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .local p2, "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    if-nez p1, :cond_1

    .line 22
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p1

    .end local p1    # "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    check-cast p1, Ljava/util/List;

    .line 26
    .restart local p1    # "hi":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    :cond_0
    :goto_0
    iput-object p1, p0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->hi:Ljava/util/List;

    .line 27
    iput-object p2, p0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->lo:Ljava/util/List;

    .line 28
    if-eq p1, p2, :cond_2

    move v0, v1

    :goto_1
    iput-boolean v0, p0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->supportsQualityToggle:Z

    .line 29
    invoke-interface {p1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    iget-object v0, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v0, v0, Lcom/google/android/videos/streams/ItagInfo;->height:I

    const/16 v3, 0x2d0

    if-lt v0, v3, :cond_3

    :goto_2
    iput-boolean v1, p0, Lcom/google/android/videos/streams/DashVideoStreamSelection;->isHiHd:Z

    .line 30
    return-void

    .line 23
    :cond_1
    if-nez p2, :cond_0

    .line 24
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object p2

    .end local p2    # "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    check-cast p2, Ljava/util/List;

    .restart local p2    # "lo":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    goto :goto_0

    :cond_2
    move v0, v2

    .line 28
    goto :goto_1

    :cond_3
    move v1, v2

    .line 29
    goto :goto_2
.end method
