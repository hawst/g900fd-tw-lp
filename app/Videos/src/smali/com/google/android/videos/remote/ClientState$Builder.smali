.class public Lcom/google/android/videos/remote/ClientState$Builder;
.super Ljava/lang/Object;
.source "ClientState.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/ClientState;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private seasonId:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 107
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public account(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 130
    iput-object p1, p0, Lcom/google/android/videos/remote/ClientState$Builder;->account:Ljava/lang/String;

    .line 131
    return-object p0
.end method

.method public build()Lcom/google/android/videos/remote/ClientState;
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 135
    iget-object v0, p0, Lcom/google/android/videos/remote/ClientState$Builder;->account:Ljava/lang/String;

    if-nez v0, :cond_0

    move-object v4, v5

    .line 136
    .local v4, "obfuscatedAccount":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/videos/remote/ClientState;

    iget-object v1, p0, Lcom/google/android/videos/remote/ClientState$Builder;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/remote/ClientState$Builder;->seasonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/remote/ClientState$Builder;->showId:Ljava/lang/String;

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/remote/ClientState;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/remote/ClientState$1;)V

    return-object v0

    .line 135
    .end local v4    # "obfuscatedAccount":Ljava/lang/String;
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/remote/ClientState$Builder;->account:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/remote/ClientState;->obfuscate(Ljava/lang/String;)Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/remote/ClientState;->access$000(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public seasonId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;
    .locals 0
    .param p1, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 120
    iput-object p1, p0, Lcom/google/android/videos/remote/ClientState$Builder;->seasonId:Ljava/lang/String;

    .line 121
    return-object p0
.end method

.method public showId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 125
    iput-object p1, p0, Lcom/google/android/videos/remote/ClientState$Builder;->showId:Ljava/lang/String;

    .line 126
    return-object p0
.end method

.method public videoId(Ljava/lang/String;)Lcom/google/android/videos/remote/ClientState$Builder;
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 115
    iput-object p1, p0, Lcom/google/android/videos/remote/ClientState$Builder;->videoId:Ljava/lang/String;

    .line 116
    return-object p0
.end method
