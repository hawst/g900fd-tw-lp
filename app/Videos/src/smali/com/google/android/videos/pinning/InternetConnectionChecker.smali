.class public final Lcom/google/android/videos/pinning/InternetConnectionChecker;
.super Ljava/lang/Object;
.source "InternetConnectionChecker.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Lcom/google/android/repolib/observers/Updatable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;
    }
.end annotation


# instance fields
.field private final blockingCheckConnectionRunnable:Ljava/lang/Runnable;

.field private checkPending:Z

.field private final generateHttp204url:Ljava/net/URL;

.field private final handler:Landroid/os/Handler;

.field private volatile haveInternetConnection:Z

.field private volatile haveNetworkConnection:Z

.field private final listener:Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;

.field private volatile networkConnectionIsWifi:Z

.field private final networkExecutor:Ljava/util/concurrent/Executor;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "listener"    # Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;
    .param p5, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 55
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 56
    iput-object p4, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->listener:Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;

    .line 57
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/util/concurrent/Executor;

    iput-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkExecutor:Ljava/util/concurrent/Executor;

    .line 59
    invoke-interface {p2}, Lcom/google/android/videos/Config;->generateHttp204Url()Ljava/lang/String;

    move-result-object v1

    .line 60
    .local v1, "url":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 61
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->generateHttp204url:Ljava/net/URL;

    .line 70
    :goto_0
    new-instance v2, Lcom/google/android/videos/pinning/InternetConnectionChecker$1;

    invoke-direct {v2, p0}, Lcom/google/android/videos/pinning/InternetConnectionChecker$1;-><init>(Lcom/google/android/videos/pinning/InternetConnectionChecker;)V

    iput-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->blockingCheckConnectionRunnable:Ljava/lang/Runnable;

    .line 77
    new-instance v2, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v2, v3, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    .line 78
    iput-object p5, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 79
    iget-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 80
    iget-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveNetworkConnection:Z

    .line 81
    iget-boolean v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveNetworkConnection:Z

    iput-boolean v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection:Z

    .line 82
    iget-boolean v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveNetworkConnection:Z

    if-eqz v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isWiFiNetwork()Z

    move-result v2

    if-eqz v2, :cond_1

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkConnectionIsWifi:Z

    .line 84
    return-void

    .line 64
    :cond_0
    :try_start_0
    new-instance v2, Ljava/net/URL;

    invoke-direct {v2, v1}, Ljava/net/URL;-><init>(Ljava/lang/String;)V

    iput-object v2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->generateHttp204url:Ljava/net/URL;
    :try_end_0
    .catch Ljava/net/MalformedURLException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 65
    :catch_0
    move-exception v0

    .line 66
    .local v0, "e":Ljava/net/MalformedURLException;
    new-instance v2, Ljava/lang/RuntimeException;

    const-string v3, "Invalid generate http 204 URL"

    invoke-direct {v2, v3, v0}, Ljava/lang/RuntimeException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v2

    .line 82
    .end local v0    # "e":Ljava/net/MalformedURLException;
    :cond_1
    const/4 v2, 0x0

    goto :goto_1
.end method

.method static synthetic access$000(Lcom/google/android/videos/pinning/InternetConnectionChecker;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/InternetConnectionChecker;

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->blockingCheckInternetConnection()Z

    move-result v0

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pinning/InternetConnectionChecker;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/InternetConnectionChecker;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method private blockingCheckInternetConnection()Z
    .locals 6

    .prologue
    const/4 v4, 0x0

    .line 196
    const/4 v2, 0x0

    .line 198
    .local v2, "urlConnection":Ljava/net/HttpURLConnection;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->generateHttp204url:Ljava/net/URL;

    invoke-virtual {v3}, Ljava/net/URL;->openConnection()Ljava/net/URLConnection;

    move-result-object v3

    move-object v0, v3

    check-cast v0, Ljava/net/HttpURLConnection;

    move-object v2, v0

    .line 199
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setInstanceFollowRedirects(Z)V

    .line 200
    const/16 v3, 0x2710

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setConnectTimeout(I)V

    .line 201
    const/16 v3, 0x2710

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setReadTimeout(I)V

    .line 202
    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Ljava/net/HttpURLConnection;->setUseCaches(Z)V

    .line 203
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->getResponseCode()I
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v3

    const/16 v5, 0xcc

    if-ne v3, v5, :cond_1

    const/4 v3, 0x1

    .line 208
    :goto_0
    if-eqz v2, :cond_0

    .line 209
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_0
    :goto_1
    return v3

    :cond_1
    move v3, v4

    .line 203
    goto :goto_0

    .line 204
    :catch_0
    move-exception v1

    .line 205
    .local v1, "e":Ljava/io/IOException;
    :try_start_1
    const-string v3, "Connection to 204 server failed"

    invoke-static {v3, v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 208
    if-eqz v2, :cond_2

    .line 209
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_2
    move v3, v4

    goto :goto_1

    .line 208
    .end local v1    # "e":Ljava/io/IOException;
    :catchall_0
    move-exception v3

    if-eqz v2, :cond_3

    .line 209
    invoke-virtual {v2}, Ljava/net/HttpURLConnection;->disconnect()V

    :cond_3
    throw v3
.end method

.method private handleConnectivityManagerState(ZZ)V
    .locals 0
    .param p1, "haveConnection"    # Z
    .param p2, "isWifi"    # Z

    .prologue
    .line 164
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->updateConnectivityState(ZZ)V

    .line 165
    return-void
.end method

.method private handleInternetCheckCompleted(Z)V
    .locals 1
    .param p1, "connected"    # Z

    .prologue
    .line 158
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->checkPending:Z

    .line 159
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->setHaveInternetConnection(Z)V

    .line 160
    return-void
.end method

.method private maybeCheckInternetConnection()V
    .locals 2

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->generateHttp204url:Ljava/net/URL;

    if-nez v0, :cond_1

    .line 155
    :cond_0
    :goto_0
    return-void

    .line 143
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->checkPending:Z

    if-nez v0, :cond_0

    .line 147
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveNetworkConnection:Z

    if-nez v0, :cond_2

    .line 149
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->setHaveInternetConnection(Z)V

    goto :goto_0

    .line 152
    :cond_2
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->checkPending:Z

    .line 153
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkExecutor:Ljava/util/concurrent/Executor;

    iget-object v1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->blockingCheckConnectionRunnable:Ljava/lang/Runnable;

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 154
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    const/4 v1, 0x2

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method

.method private setHaveInternetConnection(Z)V
    .locals 1
    .param p1, "haveInternetConnection"    # Z

    .prologue
    .line 168
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkConnectionIsWifi:Z

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->updateConnectivityState(ZZ)V

    .line 169
    return-void
.end method

.method private updateConnectivityState(ZZ)V
    .locals 4
    .param p1, "haveInternetConnection"    # Z
    .param p2, "networkConnectionIsWifi"    # Z

    .prologue
    const/4 v2, 0x2

    .line 173
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkConnectionIsWifi:Z

    if-ne v0, p2, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection:Z

    if-eq v0, p1, :cond_1

    .line 175
    :cond_0
    iput-boolean p2, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkConnectionIsWifi:Z

    .line 176
    iput-boolean p1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection:Z

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->listener:Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;

    if-eqz v0, :cond_1

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->listener:Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;->onConnectivityChanged()V

    .line 181
    :cond_1
    if-nez p1, :cond_2

    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveNetworkConnection:Z

    if-eqz v0, :cond_2

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->obtainMessage(I)Landroid/os/Message;

    move-result-object v1

    const-wide/32 v2, 0xea60

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v2}, Landroid/os/Handler;->removeMessages(I)V

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)Z
    .locals 4
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v0, 0x0

    const/4 v1, 0x1

    .line 114
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    .line 133
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected message: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v2, p1, Landroid/os/Message;->what:I

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 134
    :cond_0
    :goto_0
    return v1

    .line 116
    :pswitch_0
    invoke-direct {p0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->maybeCheckInternetConnection()V

    goto :goto_0

    .line 119
    :pswitch_1
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_1

    move v0, v1

    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handleInternetCheckCompleted(Z)V

    goto :goto_0

    .line 122
    :pswitch_2
    iget v2, p1, Landroid/os/Message;->arg1:I

    if-eqz v2, :cond_3

    move v2, v1

    :goto_1
    iget v3, p1, Landroid/os/Message;->arg2:I

    if-eqz v3, :cond_2

    move v0, v1

    :cond_2
    invoke-direct {p0, v2, v0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handleConnectivityManagerState(ZZ)V

    goto :goto_0

    :cond_3
    move v2, v0

    goto :goto_1

    .line 125
    :pswitch_3
    invoke-direct {p0, v1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->setHaveInternetConnection(Z)V

    goto :goto_0

    .line 128
    :pswitch_4
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection:Z

    if-eqz v0, :cond_0

    .line 129
    invoke-direct {p0}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->maybeCheckInternetConnection()V

    goto :goto_0

    .line 114
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_3
        :pswitch_4
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public haveInternetConnection()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection:Z

    return v0
.end method

.method public onError()V
    .locals 2

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 96
    return-void
.end method

.method public onSuccess()V
    .locals 2

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 103
    return-void
.end method

.method public quit()V
    .locals 2

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 88
    iget-object v0, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 89
    return-void
.end method

.method public update()V
    .locals 7

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 216
    iget-object v4, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    .line 217
    .local v0, "connected":Z
    if-eqz v0, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isWiFiNetwork()Z

    move-result v4

    if-eqz v4, :cond_0

    move v1, v2

    .line 218
    .local v1, "isWifi":Z
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;

    const/4 v6, 0x4

    if-eqz v0, :cond_1

    move v4, v2

    :goto_1
    if-eqz v1, :cond_2

    :goto_2
    invoke-virtual {v5, v6, v4, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v2

    invoke-virtual {v2}, Landroid/os/Message;->sendToTarget()V

    .line 220
    return-void

    .end local v1    # "isWifi":Z
    :cond_0
    move v1, v3

    .line 217
    goto :goto_0

    .restart local v1    # "isWifi":Z
    :cond_1
    move v4, v3

    .line 218
    goto :goto_1

    :cond_2
    move v2, v3

    goto :goto_2
.end method
