.class public abstract Lcom/google/android/videos/ui/playnext/ClusterItemView;
.super Lcom/google/android/videos/ui/ThemeSwitcher;
.source "ClusterItemView.java"

# interfaces
.implements Lcom/google/android/videos/flow/SelfRecycledListener;
.implements Lcom/google/android/videos/flow/SelfRenderedListener;
.implements Lcom/google/android/videos/logging/UiElementNode;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;,
        Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
    }
.end annotation


# static fields
.field protected static final clickableSubviewIds:[I


# instance fields
.field private final bitmapLoaderRunners:Landroid/util/SparseArray;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner",
            "<*>;>;"
        }
    .end annotation
.end field

.field private needSendImpression:Z

.field private parentNode:Lcom/google/android/videos/logging/UiElementNode;

.field private stateOverlay:Landroid/widget/FrameLayout;

.field private thumbnailAspectRatio:F

.field private final thumbnailMeasureMode:I

.field protected thumbnailView:Landroid/widget/ImageView;

.field private uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 141
    const/4 v0, 0x7

    new-array v0, v0, [I

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->clickableSubviewIds:[I

    return-void

    nop

    :array_0
    .array-data 4
        0x7f0f00d1
        0x7f0f00d2
        0x7f0f00d5
        0x7f0f00d0
        0x7f0f00d6
        0x7f0f00ca
        0x7f0f00dc
    .end array-data
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 161
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 162
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 165
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 166
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 169
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/ThemeSwitcher;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 171
    sget-object v1, Lcom/google/android/videos/R$styleable;->ClusterItemView:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 172
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v1, 0x0

    const v2, 0x3f31a787

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getFloat(IF)F

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->setThumbnailAspectRatio(F)V

    .line 174
    const/4 v1, 0x1

    const/4 v2, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailMeasureMode:I

    .line 177
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 179
    new-instance v1, Landroid/util/SparseArray;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/util/SparseArray;-><init>(I)V

    iput-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    .line 180
    return-void
.end method

.method public static findThumbnailView(Landroid/view/View;)Landroid/view/View;
    .locals 1
    .param p0, "child"    # Landroid/view/View;

    .prologue
    .line 297
    :goto_0
    if-eqz p0, :cond_0

    instance-of v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;

    if-nez v0, :cond_0

    .line 298
    invoke-virtual {p0}, Landroid/view/View;->getParent()Landroid/view/ViewParent;

    move-result-object p0

    .end local p0    # "child":Landroid/view/View;
    check-cast p0, Landroid/view/View;

    .restart local p0    # "child":Landroid/view/View;
    goto :goto_0

    .line 300
    :cond_0
    if-nez p0, :cond_1

    const/4 v0, 0x0

    .end local p0    # "child":Landroid/view/View;
    :goto_1
    return-object v0

    .restart local p0    # "child":Landroid/view/View;
    :cond_1
    check-cast p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;

    .end local p0    # "child":Landroid/view/View;
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    goto :goto_1
.end method

.method private measureThumbnailSpanningHeight(I)V
    .locals 6
    .param p1, "heightMeasureSpec"    # I

    .prologue
    .line 285
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 286
    .local v0, "availableHeight":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingTop()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingBottom()I

    move-result v5

    add-int v3, v4, v5

    .line 287
    .local v3, "verticalPadding":I
    sub-int/2addr v0, v3

    .line 289
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 290
    .local v2, "thumbnailLp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    sub-int v4, v0, v4

    iget v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->bottomMargin:I

    sub-int v1, v4, v5

    .line 292
    .local v1, "thumbnailHeight":I
    iput v1, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 293
    int-to-float v4, v1

    iget v5, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailAspectRatio:F

    mul-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 294
    return-void
.end method

.method private measureThumbnailSpanningWidth(I)V
    .locals 6
    .param p1, "widthMeasureSpec"    # I

    .prologue
    .line 273
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v0

    .line 274
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingLeft()I

    move-result v4

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingRight()I

    move-result v5

    add-int v1, v4, v5

    .line 275
    .local v1, "horizontalPadding":I
    sub-int/2addr v0, v1

    .line 277
    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v4}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v2

    check-cast v2, Landroid/widget/RelativeLayout$LayoutParams;

    .line 278
    .local v2, "thumbnailLp":Landroid/widget/RelativeLayout$LayoutParams;
    iget v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    sub-int v4, v0, v4

    iget v5, v2, Landroid/widget/RelativeLayout$LayoutParams;->rightMargin:I

    sub-int v3, v4, v5

    .line 280
    .local v3, "thumbnailWidth":I
    iput v3, v2, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 281
    int-to-float v4, v3

    iget v5, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailAspectRatio:F

    div-float/2addr v4, v5

    float-to-int v4, v4

    iput v4, v2, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 282
    return-void
.end method

.method private registerBitmapViewInternal(ILcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V
    .locals 8
    .param p1, "id"    # I
    .param p4, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "missingBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Class",
            "<TQ;>;)V"
        }
    .end annotation

    .prologue
    .line 374
    .local p2, "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<*>;"
    .local p3, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TQ;Landroid/graphics/Bitmap;>;"
    .local p6, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;

    .line 375
    .local v0, "existing":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<*>;"
    if-eqz v0, :cond_0

    move-object v1, p2

    move-object v2, p3

    move-object v3, p4

    move-object v4, p5

    move-object v5, p6

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->rebindAndDisableIfDifferent(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 377
    :cond_0
    iget-object v7, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    new-instance v1, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;-><init>(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    invoke-virtual {v7, p1, v1}, Landroid/util/SparseArray;->put(ILjava/lang/Object;)V

    .line 380
    :cond_1
    return-void
.end method


# virtual methods
.method public childImpression(Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 2
    .param p1, "childNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 335
    new-instance v0, Ljava/lang/IllegalStateException;

    const-string v1, "unwanted child"

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 3
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 202
    const/4 v1, 0x0

    aput-object p0, p1, v1

    .line 203
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    sget-object v1, Lcom/google/android/videos/ui/playnext/ClusterItemView;->clickableSubviewIds:[I

    array-length v1, v1

    if-ge v0, v1, :cond_0

    .line 204
    add-int/lit8 v1, v0, 0x1

    sget-object v2, Lcom/google/android/videos/ui/playnext/ClusterItemView;->clickableSubviewIds:[I

    aget v2, v2, v0

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    aput-object v2, p1, v1

    .line 203
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 206
    :cond_0
    return-void
.end method

.method public getParentNode()Lcom/google/android/videos/logging/UiElementNode;
    .locals 1

    .prologue
    .line 330
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->parentNode:Lcom/google/android/videos/logging/UiElementNode;

    return-object v0
.end method

.method public getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;
    .locals 1

    .prologue
    .line 325
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    return-object v0
.end method

.method protected initForLogging(Lcom/google/android/videos/logging/UiElementNode;ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;)V
    .locals 1
    .param p1, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p2, "cardType"    # I
    .param p3, "clientLogsCookie"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .prologue
    .line 308
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->needSendImpression:Z

    .line 309
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->parentNode:Lcom/google/android/videos/logging/UiElementNode;

    .line 310
    new-instance v0, Lcom/google/android/videos/logging/UiElementWrapper;

    invoke-direct {v0}, Lcom/google/android/videos/logging/UiElementWrapper;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    .line 311
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    iget-object v0, v0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iput p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 312
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    iget-object v0, v0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iput-object p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clientCookie:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElementInfo;

    .line 313
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 1

    .prologue
    .line 317
    invoke-super {p0}, Lcom/google/android/videos/ui/ThemeSwitcher;->onDetachedFromWindow()V

    .line 318
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->needSendImpression:Z

    .line 319
    return-void
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 210
    invoke-super {p0}, Lcom/google/android/videos/ui/ThemeSwitcher;->onFinishInflate()V

    .line 211
    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    .line 214
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f04002c

    const/4 v2, 0x0

    invoke-virtual {v0, v1, p0, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    .line 216
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->addView(Landroid/view/View;)V

    .line 217
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 422
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const/4 v0, 0x0

    return-object v0
.end method

.method protected onLayout(ZIIII)V
    .locals 6
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 261
    invoke-super/range {p0 .. p5}, Lcom/google/android/videos/ui/ThemeSwitcher;->onLayout(ZIIII)V

    .line 265
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->getHeight()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {v1}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v1

    if-eq v0, v1, :cond_0

    .line 266
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingLeft()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingLeft()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {v4}, Landroid/widget/FrameLayout;->getMeasuredWidth()I

    move-result v4

    add-int/2addr v3, v4

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingTop()I

    move-result v4

    iget-object v5, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {v5}, Landroid/widget/FrameLayout;->getMeasuredHeight()I

    move-result v5

    add-int/2addr v4, v5

    invoke-virtual {v0, v1, v2, v3, v4}, Landroid/widget/FrameLayout;->layout(IIII)V

    .line 270
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 232
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    if-eqz v0, :cond_0

    .line 233
    iget v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailMeasureMode:I

    packed-switch v0, :pswitch_data_0

    .line 246
    :cond_0
    :goto_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/ThemeSwitcher;->onMeasure(II)V

    .line 248
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v0

    if-eq v0, v4, :cond_1

    .line 251
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getMeasuredWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingLeft()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingTop()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->getPaddingBottom()I

    move-result v3

    sub-int/2addr v2, v3

    invoke-static {v2, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v2

    invoke-virtual {v0, v1, v2}, Landroid/widget/FrameLayout;->measure(II)V

    .line 257
    :cond_1
    return-void

    .line 235
    :pswitch_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->measureThumbnailSpanningWidth(I)V

    goto :goto_0

    .line 238
    :pswitch_1
    invoke-direct {p0, p2}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->measureThumbnailSpanningHeight(I)V

    goto :goto_0

    .line 233
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public onViewRecycled()V
    .locals 2

    .prologue
    .line 408
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 409
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->reset()V

    .line 408
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 411
    :cond_0
    return-void
.end method

.method public onViewRendered()V
    .locals 3

    .prologue
    .line 396
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1}, Landroid/util/SparseArray;->size()I

    move-result v1

    add-int/lit8 v0, v1, -0x1

    .local v0, "i":I
    :goto_0
    if-ltz v0, :cond_0

    .line 397
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1, v0}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v2, v0}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v2

    invoke-virtual {v1, v2, p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->kick(ILcom/google/android/videos/ui/playnext/ClusterItemView;)V

    .line 396
    add-int/lit8 v0, v0, -0x1

    goto :goto_0

    .line 400
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->needSendImpression:Z

    if-eqz v1, :cond_1

    .line 401
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->needSendImpression:Z

    .line 402
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->parentNode:Lcom/google/android/videos/logging/UiElementNode;

    invoke-interface {v1, p0}, Lcom/google/android/videos/logging/UiElementNode;->childImpression(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 404
    :cond_1
    return-void
.end method

.method protected final registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "bitmapView"    # Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;
    .param p4, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "missingBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;",
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Class",
            "<TQ;>;)V"
        }
    .end annotation

    .prologue
    .line 366
    .local p3, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TQ;Landroid/graphics/Bitmap;>;"
    .local p6, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/async/Requester;

    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/util/concurrent/Executor;

    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Class;

    move-object v0, p0

    move v1, p1

    move-object v5, p5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->registerBitmapViewInternal(ILcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 369
    return-void
.end method

.method protected final registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V
    .locals 7
    .param p1, "id"    # I
    .param p2, "bitmapView"    # Lcom/google/android/videos/ui/BitmapLoader$BitmapView;
    .param p4, "missingBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Lcom/google/android/videos/ui/BitmapLoader$BitmapView;",
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Class",
            "<TQ;>;)V"
        }
    .end annotation

    .prologue
    .line 354
    .local p3, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TQ;Landroid/graphics/Bitmap;>;"
    .local p5, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/async/Requester;

    const/4 v4, 0x0

    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Ljava/lang/Class;

    move-object v0, p0

    move v1, p1

    move-object v5, p4

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->registerBitmapViewInternal(ILcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 357
    return-void
.end method

.method public setDimmedStyle(Z)V
    .locals 2
    .param p1, "dimmed"    # Z

    .prologue
    .line 227
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->stateOverlay:Landroid/widget/FrameLayout;

    if-eqz p1, :cond_0

    const v0, 0x7f0a00d5

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/FrameLayout;->setBackgroundResource(I)V

    .line 228
    return-void

    .line 227
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setThumbnailAspectRatio(F)V
    .locals 1
    .param p1, "ratio"    # F

    .prologue
    .line 220
    iget v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailAspectRatio:F

    cmpl-float v0, p1, v0

    if-eqz v0, :cond_0

    .line 221
    iput p1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->thumbnailAspectRatio:F

    .line 222
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->requestLayout()V

    .line 224
    :cond_0
    return-void
.end method

.method protected final unregisterBitmapView(I)V
    .locals 2
    .param p1, "id"    # I

    .prologue
    .line 387
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;

    .line 388
    .local v0, "existing":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<*>;"
    if-eqz v0, :cond_0

    .line 389
    invoke-virtual {v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->reset()V

    .line 390
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView;->bitmapLoaderRunners:Landroid/util/SparseArray;

    invoke-virtual {v1, p1}, Landroid/util/SparseArray;->remove(I)V

    .line 392
    :cond_0
    return-void
.end method
