.class public Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
.super Ljava/lang/Object;
.source "SubtitleWindow.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/SubtitleWindow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final id:I

.field private final settingsTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

.field private final textTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;


# direct methods
.method public constructor <init>(I)V
    .locals 1
    .param p1, "id"    # I

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    iput p1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->id:I

    .line 69
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;

    invoke-direct {v0}, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;

    .line 70
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    invoke-direct {v0}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    .line 71
    return-void
.end method


# virtual methods
.method public addLine(Ljava/lang/String;IIZ)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    .locals 1
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "startTimeMillis"    # I
    .param p3, "endTimeMillis"    # I
    .param p4, "append"    # Z

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;

    invoke-virtual {v0, p1, p2, p3, p4}, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->addLine(Ljava/lang/String;IIZ)Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;

    .line 75
    return-object p0
.end method

.method public addSettings(ILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    .locals 1
    .param p1, "startTimeMillis"    # I
    .param p2, "windowSettings"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->addSettings(ILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    .line 80
    return-object p0
.end method

.method public build()Lcom/google/android/videos/subtitles/SubtitleWindow;
    .locals 5

    .prologue
    .line 84
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindow;

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->id:I

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->textTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;

    invoke-virtual {v2}, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->build()Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;->settingsTimelineBuilder:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    invoke-virtual {v3}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->build()Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    move-result-object v3

    const/4 v4, 0x0

    invoke-direct {v0, v1, v2, v3, v4}, Lcom/google/android/videos/subtitles/SubtitleWindow;-><init>(ILcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;Lcom/google/android/videos/subtitles/SubtitleWindow$1;)V

    return-object v0
.end method
