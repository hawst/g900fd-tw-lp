.class public Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
.super Landroid/widget/FrameLayout;
.source "DefaultControllerOverlay.java"

# interfaces
.implements Landroid/os/Handler$Callback;
.implements Landroid/view/GestureDetector$OnDoubleTapListener;
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/videos/player/overlay/ControllerOverlay;
.implements Lcom/google/android/videos/player/overlay/ScrubPad$Listener;
.implements Lcom/google/android/videos/player/overlay/TimeBar$Listener;
.implements Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/support/v4/app/FragmentActivity;

.field private audioTracks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;"
        }
    .end annotation
.end field

.field private ccButton:Landroid/widget/ImageButton;

.field private controlBar:Landroid/view/View;

.field private final controlBarHitRect:Landroid/graphics/Rect;

.field private controlsVisibility:I

.field private currentDialog:Landroid/content/DialogInterface;

.field private currentZone:I

.field private final errorBackgroundColor:I

.field private errorCanTroubleshoot:Z

.field private errorMessage:Ljava/lang/String;

.field private errorOverlay:Landroid/view/View;

.field private errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

.field private errorRetryButton:Landroid/widget/Button;

.field private errorText:Landroid/widget/TextView;

.field private errorTroubleshootButton:Landroid/widget/Button;

.field private errorVisible:Z

.field private final fadeDurationFast:I

.field private final fadeDurationSlow:I

.field private fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

.field private fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

.field private fineGrainedScrubbingHappened:Z

.field private fineScrubberOverlay:Landroid/view/View;

.field private firstEventTimeMillis:J

.field private final gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

.field private gestureDetectorState:I

.field private final handler:Landroid/os/Handler;

.field private hasKnowledge:Z

.field private hasSpinner:Z

.field private hideAnimationCanceled:Z

.field private final hideControlsAnimation:Landroid/view/animation/Animation;

.field private hideable:Z

.field private hqButton:Landroid/widget/ImageButton;

.field private hqEnabled:Z

.field private isFineGrainedScrubbing:Z

.field private isHqHd:Z

.field private isSeeking:Z

.field private lastIndicatedScrubbingTimeMillis:I

.field private lastScrubDirection:I

.field private lastScrubDirectionChangeTimeMillis:I

.field private lastScrubbingEvent:Landroid/view/MotionEvent;

.field private listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

.field private loading:Z

.field private final mediaKeyHelper:Lcom/google/android/videos/player/overlay/MediaKeyHelper;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private persistHideAtStateChange:Z

.field private playPauseButton:Landroid/widget/ImageButton;

.field private rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

.field private rewindIndicatorAnimation:Landroid/view/animation/Animation;

.field private scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

.field private scrubPadInitialExpandStartTime:J

.field private scrubPadState:I

.field private scrubPadToggleButton:Landroid/widget/ImageButton;

.field private final scrubberRect:Landroid/graphics/Rect;

.field private scrubbingStartedAtTimeMillis:I

.field private scrubbingTimeOffsetMillis:I

.field private seekingProgressText:Landroid/widget/TextView;

.field private seekingProgressTextAnimation:Landroid/view/animation/Animation;

.field private selectedAudioTrackIndex:I

.field private selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

.field private speedUp:F

.field private spinner:Landroid/widget/ProgressBar;

.field private state:I

.field private final storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

.field private subtitleTracks:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private supportsQualityToggle:Z

.field private thumbnailView:Landroid/widget/ImageView;

.field private timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

.field private totalSkipMillis:I

.field private touchExplorationEnabled:Z

.field private useScrubPad:Z

.field private wasPlayingWhenSeekingStarted:Z


# direct methods
.method public constructor <init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/StoryboardHelper;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 4
    .param p1, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p2, "storyboardHelper"    # Lcom/google/android/videos/player/overlay/StoryboardHelper;
    .param p3, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    const/4 v3, 0x0

    .line 195
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 196
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubberRect:Landroid/graphics/Rect;

    .line 197
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    .line 198
    iput-object p2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    .line 199
    iput-object p3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 200
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p0}, Landroid/os/Handler;-><init>(Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    .line 201
    invoke-virtual {p1}, Landroid/support/v4/app/FragmentActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0a00bf

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorBackgroundColor:I

    .line 202
    new-instance v0, Lcom/google/android/videos/player/overlay/MediaKeyHelper;

    new-instance v1, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;

    const/4 v2, 0x0

    invoke-direct {v1, p0, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$MediaKeyHelperListener;-><init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$1;)V

    invoke-direct {v0, v1}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;-><init>(Lcom/google/android/videos/player/overlay/MediaKeyHelper$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/videos/player/overlay/MediaKeyHelper;

    .line 203
    const v0, 0x7f05000f

    invoke-static {p1, v0}, Landroid/view/animation/AnimationUtils;->loadAnimation(Landroid/content/Context;I)Landroid/view/animation/Animation;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControlsAnimation:Landroid/view/animation/Animation;

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControlsAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 205
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002d

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationFast:I

    .line 206
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0c002b

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    .line 207
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBarHitRect:Landroid/graphics/Rect;

    .line 208
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    .line 209
    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    .line 210
    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    .line 211
    iput-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->loading:Z

    .line 212
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->inflate()V

    .line 213
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControls(Z)V

    .line 214
    new-instance v0, Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-direct {v0, p1, p0}, Lcom/google/android/videos/ui/GestureDetectorPlus;-><init>(Landroid/content/Context;Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    .line 215
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-virtual {v0, v3}, Lcom/google/android/videos/ui/GestureDetectorPlus;->setIsLongpressEnabled(Z)V

    .line 216
    return-void
.end method

.method static synthetic access$102(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;Landroid/content/DialogInterface;)Landroid/content/DialogInterface;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;
    .param p1, "x1"    # Landroid/content/DialogInterface;

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    return-object p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)Lcom/google/android/videos/player/overlay/TimeBar;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .prologue
    .line 56
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .prologue
    .line 56
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    return v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;

    .prologue
    .line 56
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onCcToggled()V

    return-void
.end method

.method private cancelHiding()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 498
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 499
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideAnimationCanceled:Z

    .line 500
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->clearAnimation()V

    .line 501
    return-void
.end method

.method private endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;
    .param p2, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1454
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Landroid/view/animation/Animation;->hasStarted()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v0, 0x1

    .line 1455
    .local v0, "shouldCallAnimationEnd":Z
    :goto_0
    invoke-virtual {p1}, Landroid/view/View;->clearAnimation()V

    .line 1456
    if-eqz v0, :cond_0

    .line 1457
    invoke-virtual {p0, p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onAnimationEnd(Landroid/view/animation/Animation;)V

    .line 1459
    :cond_0
    return-void

    .line 1454
    .end local v0    # "shouldCallAnimationEnd":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private fineGrainedScrubBy(I)V
    .locals 3
    .param p1, "timeMillis"    # I

    .prologue
    .line 1363
    const/4 v0, 0x0

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v2

    add-int/2addr v2, p1

    invoke-static {v1, v2}, Ljava/lang/Math;->min(II)I

    move-result v1

    invoke-static {v0, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubTo(I)V

    .line 1365
    return-void
.end method

.method private fineGrainedScrubTo(I)V
    .locals 10
    .param p1, "timeMillis"    # I

    .prologue
    const/4 v7, 0x4

    const/4 v3, 0x1

    const/4 v4, 0x0

    .line 1368
    iput-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubbingHappened:Z

    .line 1369
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v5, v7}, Landroid/os/Handler;->removeMessages(I)V

    .line 1370
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v5}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v5

    iget v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    sub-int/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Integer;->signum(I)I

    move-result v1

    .line 1372
    .local v1, "previousTimeDirection":I
    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    sub-int v5, p1, v5

    invoke-static {v5}, Ljava/lang/Integer;->signum(I)I

    move-result v0

    .line 1373
    .local v0, "currentTimeDirection":I
    if-eqz v1, :cond_0

    if-eq v1, v0, :cond_0

    .line 1374
    const/4 v5, 0x3

    const/4 v6, 0x2

    invoke-virtual {p0, v5, v6}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->performHapticFeedback(II)Z

    .line 1377
    :cond_0
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->loading:Z

    if-nez v5, :cond_1

    .line 1378
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v5, v3, p1, v4}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 1382
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v5}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v5

    sub-int v5, p1, v5

    invoke-static {v5}, Ljava/lang/Integer;->signum(I)I

    move-result v2

    .line 1383
    .local v2, "scrubDirection":I
    if-eqz v2, :cond_2

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirection:I

    if-eq v2, v5, :cond_2

    .line 1384
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirectionChangeTimeMillis:I

    .line 1385
    iput v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirection:I

    .line 1389
    :cond_2
    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    if-eq v5, v7, :cond_3

    .line 1390
    if-lez v2, :cond_5

    .line 1391
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirectionChangeTimeMillis:I

    sub-int v6, p1, v6

    invoke-virtual {v5, v6}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setScrubTime(I)V

    .line 1397
    :cond_3
    :goto_0
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->setScrubberTime(I)V

    .line 1398
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    invoke-virtual {v5, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 1399
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v6}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v6

    int-to-long v6, v6

    const-wide/32 v8, 0x36ee80

    cmp-long v6, v6, v8

    if-ltz v6, :cond_6

    :goto_1
    invoke-static {p1, v3}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v5, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1402
    add-int/lit8 v3, p1, -0x2a

    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastIndicatedScrubbingTimeMillis:I

    if-le v3, v4, :cond_7

    .line 1404
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1405
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    .line 1407
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastIndicatedScrubbingTimeMillis:I

    .line 1415
    :cond_4
    :goto_2
    return-void

    .line 1392
    :cond_5
    if-gez v2, :cond_3

    .line 1393
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirectionChangeTimeMillis:I

    sub-int/2addr v6, p1

    invoke-virtual {v5, v6}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setScrubTime(I)V

    goto :goto_0

    :cond_6
    move v3, v4

    .line 1399
    goto :goto_1

    .line 1408
    :cond_7
    add-int/lit8 v3, p1, 0x2a

    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastIndicatedScrubbingTimeMillis:I

    if-ge v3, v4, :cond_4

    .line 1410
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1411
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    .line 1413
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastIndicatedScrubbingTimeMillis:I

    goto :goto_2
.end method

.method private static getScreenZone(Landroid/view/MotionEvent;)I
    .locals 6
    .param p0, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x0

    .line 1310
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v3

    invoke-virtual {v3, v2}, Landroid/view/InputDevice;->getMotionRange(I)Landroid/view/InputDevice$MotionRange;

    move-result-object v0

    .line 1311
    .local v0, "range":Landroid/view/InputDevice$MotionRange;
    invoke-virtual {p0}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getMin()F

    move-result v4

    sub-float/2addr v3, v4

    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getMax()F

    move-result v4

    invoke-virtual {v0}, Landroid/view/InputDevice$MotionRange;->getMin()F

    move-result v5

    sub-float/2addr v4, v5

    div-float v1, v3, v4

    .line 1313
    .local v1, "relativeOnScreenPosition":F
    const v3, 0x3e19999a    # 0.15f

    cmpg-float v3, v1, v3

    if-gtz v3, :cond_1

    const/4 v2, -0x1

    :cond_0
    :goto_0
    return v2

    :cond_1
    const v3, 0x3f59999a    # 0.85f

    cmpg-float v3, v1, v3

    if-ltz v3, :cond_0

    const/4 v2, 0x1

    goto :goto_0
.end method

.method private getTimeOffsetMillis(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)F
    .locals 5
    .param p1, "e2"    # Landroid/view/MotionEvent;
    .param p2, "e3"    # Landroid/view/MotionEvent;

    .prologue
    .line 1326
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getDevice()Landroid/view/InputDevice;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v4}, Landroid/view/InputDevice;->getMotionRange(I)Landroid/view/InputDevice$MotionRange;

    move-result-object v1

    .line 1327
    .local v1, "range":Landroid/view/InputDevice$MotionRange;
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v3

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v4

    sub-float v0, v3, v4

    .line 1328
    .local v0, "dx":F
    invoke-virtual {v1}, Landroid/view/InputDevice$MotionRange;->getMax()F

    move-result v3

    invoke-virtual {v1}, Landroid/view/InputDevice$MotionRange;->getMin()F

    move-result v4

    sub-float/2addr v3, v4

    div-float v2, v0, v3

    .line 1329
    .local v2, "relativePosition":F
    iget-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->touchExplorationEnabled:Z

    if-nez v3, :cond_0

    .line 1330
    const v3, 0x476a6000    # 60000.0f

    mul-float/2addr v3, v2

    .line 1332
    :goto_0
    return v3

    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v2

    goto :goto_0
.end method

.method private inflate()V
    .locals 6

    .prologue
    const/4 v4, 0x1

    const/4 v5, -0x1

    .line 433
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->removeAllViews()V

    .line 435
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    .line 436
    .local v0, "context":Landroid/content/Context;
    new-instance v3, Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-direct {v3, v0}, Lcom/google/android/videos/player/overlay/ScrubPad;-><init>(Landroid/content/Context;)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    .line 437
    new-instance v2, Landroid/widget/FrameLayout$LayoutParams;

    const/16 v3, 0x11

    invoke-direct {v2, v5, v5, v3}, Landroid/widget/FrameLayout$LayoutParams;-><init>(III)V

    .line 439
    .local v2, "params":Landroid/widget/FrameLayout$LayoutParams;
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {p0, v3, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 441
    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    .line 442
    .local v1, "inflater":Landroid/view/LayoutInflater;
    const v3, 0x7f040030

    invoke-virtual {v1, v3, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 444
    const v3, 0x7f0f00f0

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    .line 445
    const v3, 0x7f0f01f2

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->thumbnailView:Landroid/widget/ImageView;

    .line 446
    const v3, 0x7f0f00f3

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/overlay/TimeBar;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    .line 447
    const v3, 0x7f0f00ef

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ProgressBar;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    .line 448
    const v3, 0x7f0f00f5

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    .line 449
    const v3, 0x7f0f00f6

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    .line 450
    const v3, 0x7f0f00f7

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    .line 451
    const v3, 0x7f0f00f4

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageButton;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    .line 452
    const v3, 0x7f0f0103

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    .line 453
    const v3, 0x7f0f0104

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorText:Landroid/widget/TextView;

    .line 454
    const v3, 0x7f0f0105

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryButton:Landroid/widget/Button;

    .line 455
    const v3, 0x7f0f0106

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/Button;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorTroubleshootButton:Landroid/widget/Button;

    .line 456
    const v3, 0x7f0f01ee

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    .line 457
    const v3, 0x7f0f01f0

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/TextView;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    .line 458
    const v3, 0x7f0f01ef

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    .line 459
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3, v4}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setRewinding(Z)V

    .line 460
    const v3, 0x7f0f01f1

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    .line 462
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    iget-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isHqHd:Z

    if-eqz v3, :cond_0

    const v3, 0x7f0201c5

    :goto_0
    invoke-virtual {v5, v3}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 463
    iget-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqEnabled:Z

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setHq(Z)V

    .line 464
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqEnabled:Z

    invoke-virtual {v3, v5}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 465
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 467
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-eqz v3, :cond_1

    move v3, v4

    :goto_1
    invoke-virtual {v5, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 468
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 470
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 471
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v3, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 472
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 473
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorTroubleshootButton:Landroid/widget/Button;

    invoke-virtual {v3, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 474
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3, p0}, Lcom/google/android/videos/player/overlay/TimeBar;->setListener(Lcom/google/android/videos/player/overlay/TimeBar$Listener;)V

    .line 475
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v3, p0}, Lcom/google/android/videos/player/overlay/ScrubPad;->setListener(Lcom/google/android/videos/player/overlay/ScrubPad$Listener;)V

    .line 477
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorMessage:Ljava/lang/String;

    invoke-virtual {v3, v4}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 478
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->thumbnailView:Landroid/widget/ImageView;

    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3, p0, v4, v5}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onInflated(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;Landroid/widget/ImageView;Lcom/google/android/videos/player/overlay/TimeBar;)V

    .line 479
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onStateChanged()V

    .line 480
    return-void

    .line 462
    :cond_0
    const v3, 0x7f0201c6

    goto :goto_0

    .line 467
    :cond_1
    const/4 v3, 0x0

    goto :goto_1
.end method

.method private isInToddlerMode()Z
    .locals 1

    .prologue
    .line 1294
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    invoke-static {v0}, Lcom/google/android/videos/utils/LockTaskModeCompat;->isInLockTaskMode(Landroid/content/Context;)Z

    move-result v0

    return v0
.end method

.method private maybeStartHiding()V
    .locals 5

    .prologue
    const/4 v4, 0x1

    .line 483
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v0, v4, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v4}, Landroid/os/Handler;->hasMessages(I)Z

    move-result v0

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    if-nez v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v2, 0x9c4

    invoke-virtual {v0, v4, v2, v3}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 488
    :cond_0
    return-void
.end method

.method private onCcToggled()V
    .locals 5

    .prologue
    .line 688
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->audioTracks:Ljava/util/ArrayList;

    if-nez v1, :cond_0

    .line 698
    :goto_0
    return-void

    .line 691
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    if-eqz v1, :cond_1

    .line 692
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    invoke-interface {v1}, Landroid/content/DialogInterface;->dismiss()V

    .line 694
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-nez v1, :cond_2

    const/4 v0, 0x0

    .line 696
    .local v0, "selectedSubtitleIndex":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->audioTracks:Ljava/util/ArrayList;

    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedAudioTrackIndex:I

    invoke-static {v1, v2, v0, v3, v4}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/util/ArrayList;ILjava/util/ArrayList;I)Landroid/content/DialogInterface;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    goto :goto_0

    .line 694
    .end local v0    # "selectedSubtitleIndex":I
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->indexOf(Ljava/lang/Object;)I

    move-result v0

    goto :goto_1
.end method

.method private onStateChanged()V
    .locals 2

    .prologue
    .line 513
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    const/4 v1, 0x2

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    if-nez v0, :cond_1

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->persistHideAtStateChange:Z

    if-nez v0, :cond_1

    .line 515
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    .line 517
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 518
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    .line 519
    return-void
.end method

.method private prepareToHideControls()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 348
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    .line 349
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eq v0, v1, :cond_0

    .line 350
    iput v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    .line 351
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    .line 352
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onHidePending()V

    .line 355
    :cond_0
    return-void
.end method

.method private repositionThumbnail()V
    .locals 6

    .prologue
    .line 847
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout$LayoutParams;

    .line 848
    .local v0, "layoutParams":Landroid/widget/FrameLayout$LayoutParams;
    iget v1, v0, Landroid/widget/FrameLayout$LayoutParams;->width:I

    .line 849
    .local v1, "thumbnailWidth":I
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingLeft()I

    move-result v2

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getWidth()I

    move-result v3

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getPaddingRight()I

    move-result v4

    sub-int/2addr v3, v4

    sub-int/2addr v3, v1

    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubberPositionInOverlay()I

    move-result v4

    div-int/lit8 v5, v1, 0x2

    sub-int/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->min(II)I

    move-result v3

    invoke-static {v2, v3}, Ljava/lang/Math;->max(II)I

    move-result v2

    iput v2, v0, Landroid/widget/FrameLayout$LayoutParams;->leftMargin:I

    .line 852
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 853
    return-void
.end method

.method private resetGesture()V
    .locals 2

    .prologue
    .line 1298
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 1300
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->stopFineGrainedScrubbing()V

    .line 1302
    :cond_1
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    const/4 v1, 0x5

    if-ne v0, v1, :cond_2

    .line 1303
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->cancelGesture()V

    .line 1305
    :cond_2
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    .line 1306
    return-void
.end method

.method private scrubberPositionInOverlay()I
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 856
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubberRect:Landroid/graphics/Rect;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterX()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberCenterX()I

    move-result v2

    invoke-virtual {v0, v1, v3, v2, v3}, Landroid/graphics/Rect;->set(IIII)V

    .line 857
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubberRect:Landroid/graphics/Rect;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->offsetDescendantRectToMyCoords(Landroid/view/View;Landroid/graphics/Rect;)V

    .line 858
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubberRect:Landroid/graphics/Rect;

    iget v0, v0, Landroid/graphics/Rect;->left:I

    return v0
.end method

.method private setStateInternal(IZ)V
    .locals 1
    .param p1, "state"    # I
    .param p2, "loading"    # Z

    .prologue
    .line 504
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v0, p1, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->loading:Z

    if-ne v0, p2, :cond_0

    .line 510
    :goto_0
    return-void

    .line 507
    :cond_0
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    .line 508
    iput-boolean p2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->loading:Z

    .line 509
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onStateChanged()V

    goto :goto_0
.end method

.method private static setVisible(Landroid/view/View;Z)V
    .locals 1
    .param p0, "view"    # Landroid/view/View;
    .param p1, "visible"    # Z

    .prologue
    .line 1462
    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p0, v0}, Landroid/view/View;->setVisibility(I)V

    .line 1463
    return-void

    .line 1462
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "ongoingAnimation"    # Landroid/view/animation/Animation;

    .prologue
    .line 1439
    if-eqz p2, :cond_0

    invoke-virtual {p2}, Landroid/view/animation/Animation;->hasEnded()Z

    move-result v1

    if-nez v1, :cond_0

    .line 1440
    invoke-virtual {p1, p2}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 1448
    .end local p2    # "ongoingAnimation":Landroid/view/animation/Animation;
    :goto_0
    return-object p2

    .line 1443
    .restart local p2    # "ongoingAnimation":Landroid/view/animation/Animation;
    :cond_0
    new-instance v0, Landroid/view/animation/AlphaAnimation;

    const/high16 v1, 0x3f800000    # 1.0f

    const/4 v2, 0x0

    invoke-direct {v0, v1, v2}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    .line 1444
    .local v0, "newAnimation":Landroid/view/animation/Animation;
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 1445
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/animation/Animation;->setStartOffset(J)V

    .line 1446
    invoke-virtual {v0, p0}, Landroid/view/animation/Animation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 1447
    invoke-virtual {p1, v0}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    move-object p2, v0

    .line 1448
    goto :goto_0
.end method

.method private startFineGrainedScrubbing()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const/4 v1, 0x1

    .line 1336
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1337
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1338
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1339
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1340
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1342
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 1343
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isFineGrainedScrubbing:Z

    .line 1344
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v0, v1, :cond_1

    move v0, v1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    .line 1346
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/TimeBar;->getProgress()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    .line 1347
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastIndicatedScrubbingTimeMillis:I

    .line 1348
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubDirectionChangeTimeMillis:I

    .line 1349
    iput v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingTimeOffsetMillis:I

    .line 1351
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onScrubbingStart(Z)V

    .line 1352
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/TimeBar;->startScrubbing()V

    .line 1354
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v0, :cond_0

    .line 1355
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->prepareToHideControls()V

    .line 1356
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 1359
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateBackground()V

    .line 1360
    return-void

    :cond_1
    move v0, v2

    .line 1344
    goto :goto_0
.end method

.method private startHiding(Z)V
    .locals 3
    .param p1, "fast"    # Z

    .prologue
    .line 491
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->prepareToHideControls()V

    .line 492
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControlsAnimation:Landroid/view/animation/Animation;

    if-eqz p1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationFast:I

    int-to-long v0, v0

    :goto_0
    invoke-virtual {v2, v0, v1}, Landroid/view/animation/Animation;->setDuration(J)V

    .line 493
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideAnimationCanceled:Z

    .line 494
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControlsAnimation:Landroid/view/animation/Animation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 495
    return-void

    .line 492
    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fadeDurationSlow:I

    int-to-long v0, v0

    goto :goto_0
.end method

.method private stopFineGrainedScrubbing()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 1418
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v2, 0x3

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 1419
    iput-boolean v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 1420
    iput-boolean v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isFineGrainedScrubbing:Z

    .line 1421
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    if-eqz v1, :cond_0

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v1, :cond_0

    .line 1422
    invoke-direct {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    .line 1424
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v0

    .line 1425
    .local v0, "currentScrubbingTimeMillis":I
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v1, v3, v0, v3}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 1426
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 1427
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1428
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1429
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1430
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1431
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateBackground()V

    .line 1432
    return-void
.end method

.method private updateBackground()V
    .locals 3

    .prologue
    .line 1470
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    const/4 v2, 0x2

    if-eq v1, v2, :cond_0

    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    const/4 v2, 0x3

    if-ne v1, v2, :cond_1

    .line 1472
    :cond_0
    const v0, 0x106000c

    .line 1478
    .local v0, "backgroundResource":I
    :goto_0
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundResource(I)V

    .line 1479
    return-void

    .line 1473
    .end local v0    # "backgroundResource":I
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isFineGrainedScrubbing:Z

    if-eqz v1, :cond_2

    .line 1474
    const v0, 0x7f0a00e6

    .restart local v0    # "backgroundResource":I
    goto :goto_0

    .line 1476
    .end local v0    # "backgroundResource":I
    :cond_2
    const/4 v0, 0x0

    .restart local v0    # "backgroundResource":I
    goto :goto_0
.end method

.method private updateLastScrubbingEvent(Landroid/view/MotionEvent;)V
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 1318
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubbingEvent:Landroid/view/MotionEvent;

    if-eqz v0, :cond_0

    .line 1319
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubbingEvent:Landroid/view/MotionEvent;

    invoke-virtual {v0}, Landroid/view/MotionEvent;->recycle()V

    .line 1321
    :cond_0
    invoke-static {p1}, Landroid/view/MotionEvent;->obtainNoHistory(Landroid/view/MotionEvent;)Landroid/view/MotionEvent;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->lastScrubbingEvent:Landroid/view/MotionEvent;

    .line 1322
    return-void
.end method

.method private updateScrubPad()V
    .locals 14

    .prologue
    const-wide/16 v12, 0xbb8

    const/4 v5, 0x3

    const/4 v3, 0x2

    const/4 v10, 0x1

    const/4 v4, 0x0

    .line 379
    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasKnowledge:Z

    if-eqz v2, :cond_4

    .line 380
    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->useScrubPad:Z

    if-eqz v2, :cond_3

    move v2, v3

    :goto_0
    iput v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    .line 385
    :goto_1
    iget-wide v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadInitialExpandStartTime:J

    const-wide/16 v8, 0x0

    cmp-long v2, v6, v8

    if-nez v2, :cond_1

    iget v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eq v2, v5, :cond_0

    iget v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-ne v2, v3, :cond_1

    .line 387
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iput-wide v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadInitialExpandStartTime:J

    .line 389
    :cond_1
    invoke-direct {p0, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateSeekingRelatedViews(Z)V

    .line 390
    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasKnowledge:Z

    if-eqz v2, :cond_2

    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->useScrubPad:Z

    if-eqz v2, :cond_2

    .line 391
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadInitialExpandStartTime:J

    sub-long v0, v2, v4

    .line 393
    .local v0, "scrubPadExpandDurationMillis":J
    cmp-long v2, v0, v12

    if-gez v2, :cond_6

    .line 396
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v3, 0x4

    sub-long v4, v12, v0

    invoke-virtual {v2, v3, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 404
    .end local v0    # "scrubPadExpandDurationMillis":J
    :cond_2
    :goto_2
    return-void

    :cond_3
    move v2, v4

    .line 380
    goto :goto_0

    .line 382
    :cond_4
    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->useScrubPad:Z

    if-eqz v2, :cond_5

    move v2, v5

    :goto_3
    iput v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    goto :goto_1

    :cond_5
    move v2, v4

    goto :goto_3

    .line 398
    .restart local v0    # "scrubPadExpandDurationMillis":J
    :cond_6
    iget-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubbingHappened:Z

    if-nez v2, :cond_2

    .line 400
    iput v10, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    .line 401
    invoke-direct {p0, v10}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateSeekingRelatedViews(Z)V

    goto :goto_2
.end method

.method private updateSeekingRelatedViews(Z)V
    .locals 7
    .param p1, "smoothTransition"    # Z

    .prologue
    const v6, 0x7f0b01c3

    const/high16 v5, 0x43340000    # 180.0f

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1235
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    if-eqz v1, :cond_0

    .line 1236
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-static {v1, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1237
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->setExpandingState(I)V

    .line 1238
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/TimeBar;->setEnabled(Z)V

    .line 1288
    :goto_0
    return-void

    .line 1241
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isInToddlerMode()Z

    move-result v1

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    invoke-virtual {v4, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->setEnabled(Z)V

    .line 1242
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v4, 0x7f0d0008

    invoke-virtual {v1, v4}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v0

    .line 1244
    .local v0, "ignoreGesturesAroundTimeBar":Z
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    packed-switch v1, :pswitch_data_0

    goto :goto_0

    .line 1246
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-static {v1, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1247
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->setExpandingState(I)V

    .line 1248
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-static {v1, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1249
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1250
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateBackground()V

    goto :goto_0

    .end local v0    # "ignoreGesturesAroundTimeBar":Z
    :cond_1
    move v1, v3

    .line 1241
    goto :goto_1

    .line 1254
    .restart local v0    # "ignoreGesturesAroundTimeBar":Z
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    const/4 v4, 0x4

    invoke-virtual {v1, v4}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 1255
    if-eqz p1, :cond_2

    .line 1256
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->smoothExpandToState(I)V

    .line 1261
    :goto_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-static {v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1262
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setRotation(F)V

    .line 1263
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    const v4, 0x7f0b01c2

    invoke-virtual {v2, v4}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1266
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setClickable(Z)V

    goto :goto_0

    .line 1258
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->setExpandingState(I)V

    .line 1259
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateBackground()V

    goto :goto_2

    .line 1269
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 1270
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setExpandingState(I)V

    .line 1271
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-static {v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1272
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setRotation(F)V

    .line 1273
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1275
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    goto/16 :goto_0

    .line 1278
    :pswitch_3
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/overlay/ScrubPad;->setVisibility(I)V

    .line 1279
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->setExpandingState(I)V

    .line 1280
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-static {v1, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 1281
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v5}, Landroid/widget/ImageButton;->setRotation(F)V

    .line 1282
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v6}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 1284
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setClickable(Z)V

    .line 1285
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateBackground()V

    goto/16 :goto_0

    .line 1244
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method private updateViews()V
    .locals 7

    .prologue
    const/4 v6, 0x2

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 593
    const/4 v0, 0x0

    .line 594
    .local v0, "spinnerVisible":Z
    iput-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    .line 595
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    packed-switch v1, :pswitch_data_0

    .line 614
    :cond_0
    :goto_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    if-eqz v1, :cond_3

    .line 615
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorBackgroundColor:I

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundColor(I)V

    .line 621
    :cond_1
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    iget-boolean v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    invoke-static {v1, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 622
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v1, :cond_4

    move v1, v2

    :goto_2
    invoke-static {v4, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 623
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorTroubleshootButton:Landroid/widget/Button;

    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isInToddlerMode()Z

    move-result v1

    if-nez v1, :cond_5

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorCanTroubleshoot:Z

    if-eqz v1, :cond_5

    move v1, v2

    :goto_3
    invoke-static {v4, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 624
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    if-nez v1, :cond_6

    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eq v1, v6, :cond_6

    move v1, v2

    :goto_4
    invoke-static {v4, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 625
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    if-nez v1, :cond_7

    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eq v1, v6, :cond_7

    move v1, v2

    :goto_5
    invoke-static {v4, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 626
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    iget-boolean v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->supportsQualityToggle:Z

    invoke-static {v1, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 627
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isInToddlerMode()Z

    move-result v4

    if-nez v4, :cond_8

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    if-nez v4, :cond_2

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->audioTracks:Ljava/util/ArrayList;

    if-eqz v4, :cond_8

    :cond_2
    :goto_6
    invoke-static {v1, v2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    .line 628
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasSpinner:Z

    if-eqz v1, :cond_9

    if-eqz v0, :cond_9

    .line 630
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v4, 0xc8

    invoke-virtual {v1, v6, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 635
    :goto_7
    invoke-direct {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateSeekingRelatedViews(Z)V

    .line 636
    return-void

    .line 597
    :pswitch_0
    iput-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    goto :goto_0

    .line 600
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v3}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 601
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b01c1

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 602
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->loading:Z

    .line 603
    goto/16 :goto_0

    .line 607
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing()Z

    move-result v1

    if-nez v1, :cond_0

    .line 608
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 609
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v4

    const v5, 0x7f0b01c0

    invoke-virtual {v4, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v4

    invoke-virtual {v1, v4}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    goto/16 :goto_0

    .line 616
    :cond_3
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eq v1, v6, :cond_1

    .line 619
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundResource(I)V

    goto/16 :goto_1

    :cond_4
    move v1, v3

    .line 622
    goto/16 :goto_2

    :cond_5
    move v1, v3

    .line 623
    goto/16 :goto_3

    :cond_6
    move v1, v3

    .line 624
    goto/16 :goto_4

    :cond_7
    move v1, v3

    .line 625
    goto :goto_5

    :cond_8
    move v2, v3

    .line 627
    goto :goto_6

    .line 632
    :cond_9
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v1, v6}, Landroid/os/Handler;->removeMessages(I)V

    .line 633
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-static {v1, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    goto :goto_7

    .line 595
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_2
        :pswitch_1
        :pswitch_0
    .end packed-switch
.end method


# virtual methods
.method public clearAudioTracks()V
    .locals 1

    .prologue
    .line 652
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setSelectedAudioTrackIndex(I)V

    .line 653
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setAudioTracks(Ljava/util/List;)V

    .line 654
    return-void
.end method

.method public clearSubtitles()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 646
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 647
    const/4 v0, 0x0

    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setSubtitles(Ljava/util/List;Z)V

    .line 648
    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 230
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 225
    return-object p0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 6
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 408
    iget v2, p1, Landroid/os/Message;->what:I

    packed-switch v2, :pswitch_data_0

    move v0, v1

    .line 428
    :cond_0
    :goto_0
    return v0

    .line 410
    :pswitch_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    goto :goto_0

    .line 413
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeMessages(I)V

    .line 414
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-static {v1, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setVisible(Landroid/view/View;Z)V

    goto :goto_0

    .line 417
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v2, 0x3

    const-wide/16 v4, 0x64

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 418
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->speedUp:F

    float-to-int v1, v1

    mul-int/lit8 v1, v1, 0x64

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubBy(I)V

    goto :goto_0

    .line 421
    :pswitch_3
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubbingHappened:Z

    if-nez v1, :cond_0

    .line 423
    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    .line 424
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateSeekingRelatedViews(Z)V

    goto :goto_0

    .line 408
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
    .end packed-switch
.end method

.method public hideControls(Z)V
    .locals 2
    .param p1, "persistHideAtStateChange"    # Z

    .prologue
    const/4 v1, 0x2

    .line 359
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->persistHideAtStateChange:Z

    or-int/2addr v0, p1

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->persistHideAtStateChange:Z

    .line 360
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    .line 361
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eq v0, v1, :cond_1

    .line 362
    iput v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    .line 363
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    .line 364
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onHidden()V

    .line 366
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 368
    :cond_1
    return-void
.end method

.method public init(Z)V
    .locals 0
    .param p1, "playWhenInitialized"    # Z

    .prologue
    .line 330
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    .line 331
    return-void
.end method

.method public onActivityResult(IILandroid/content/Intent;)Z
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 776
    const/4 v0, 0x0

    return v0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x4

    const/4 v1, 0x0

    .line 801
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControlsAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 802
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideAnimationCanceled:Z

    if-nez v0, :cond_0

    .line 803
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControls(Z)V

    .line 819
    :cond_0
    :goto_0
    return-void

    .line 805
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_2

    .line 806
    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    .line 807
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setVisibility(I)V

    .line 808
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInGraphic()V

    goto :goto_0

    .line 809
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_3

    .line 810
    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    .line 811
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setVisibility(I)V

    .line 812
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInGraphic()V

    goto :goto_0

    .line 813
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 814
    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    .line 815
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 816
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 817
    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    goto :goto_0
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 797
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 3
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 783
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_1

    .line 784
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setVisibility(I)V

    .line 785
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutGraphic(Z)V

    .line 792
    :cond_0
    :goto_0
    return-void

    .line 786
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_2

    .line 787
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->setVisibility(I)V

    .line 788
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutGraphic(Z)V

    goto :goto_0

    .line 789
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    if-ne p1, v0, :cond_0

    .line 790
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 534
    const/4 v0, 0x0

    return v0
.end method

.method public onCancel(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1192
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->resetGesture()V

    .line 1193
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    .line 1194
    const/4 v0, 0x1

    return v0
.end method

.method public onClick(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 887
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    .line 888
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_1

    .line 889
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onHQ()V

    .line 909
    :cond_0
    :goto_0
    return-void

    .line 890
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_2

    .line 891
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->onCcToggled()V

    .line 892
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    goto :goto_0

    .line 893
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->playPauseButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_4

    .line 894
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_3

    .line 895
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPause()V

    goto :goto_0

    .line 896
    :cond_3
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-nez v0, :cond_0

    .line 897
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_0

    .line 899
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    if-ne p1, v0, :cond_5

    .line 900
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->toggle()V

    goto :goto_0

    .line 901
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_6

    .line 902
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v0, :cond_0

    .line 903
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-interface {v0}, Lcom/google/android/videos/utils/RetryAction;->onRetry()V

    goto :goto_0

    .line 905
    :cond_6
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorTroubleshootButton:Landroid/widget/Button;

    if-ne p1, v0, :cond_0

    .line 906
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const-string v1, "mobile_movie_troubleshoot"

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/HelpHelper;->startContextualHelp(Landroid/app/Activity;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onDoubleTap(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v2, 0x2

    const/4 v0, 0x1

    .line 986
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v1, v2, :cond_0

    .line 988
    const/4 v0, 0x0

    .line 1009
    :goto_0
    return v0

    .line 991
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    if-eqz v1, :cond_1

    .line 992
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->stopFineGrainedScrubbing()V

    goto :goto_0

    .line 996
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v1, v0, :cond_3

    .line 997
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eqz v1, :cond_2

    .line 998
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    .line 1000
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPause()V

    goto :goto_0

    .line 1005
    :cond_3
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eq v1, v2, :cond_4

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v1, :cond_4

    .line 1006
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    .line 1008
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onPlay()V

    goto :goto_0
.end method

.method public onDoubleTapEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1014
    const/4 v0, 0x1

    return v0
.end method

.method public onDown(Landroid/view/MotionEvent;)Z
    .locals 4
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 1031
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isInToddlerMode()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 1050
    :goto_0
    return v0

    .line 1034
    :cond_0
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    if-nez v1, :cond_2

    .line 1035
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eq v1, v0, :cond_1

    .line 1037
    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    goto :goto_0

    .line 1039
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-nez v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBarHitRect:Landroid/graphics/Rect;

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v2

    float-to-int v2, v2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v3

    float-to-int v3, v3

    invoke-virtual {v1, v2, v3}, Landroid/graphics/Rect;->contains(II)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 1043
    const/4 v1, 0x5

    iput v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    goto :goto_0

    .line 1050
    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onExpandingStateChanged(I)V
    .locals 3
    .param p1, "newState"    # I

    .prologue
    const/4 v1, 0x1

    .line 1201
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eqz v0, :cond_0

    .line 1202
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasKnowledge:Z

    if-eqz v0, :cond_2

    .line 1203
    if-ne p1, v1, :cond_1

    const/4 v0, 0x2

    :goto_0
    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    .line 1209
    :goto_1
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateSeekingRelatedViews(Z)V

    .line 1210
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    if-ne p1, v1, :cond_4

    const/high16 v0, 0x43340000    # 180.0f

    :goto_2
    invoke-virtual {v2, v0}, Landroid/widget/ImageButton;->setRotation(F)V

    .line 1212
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 1203
    goto :goto_0

    .line 1206
    :cond_2
    if-ne p1, v1, :cond_3

    const/4 v0, 0x3

    :goto_3
    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_3

    .line 1210
    :cond_4
    const/4 v0, 0x0

    goto :goto_2
.end method

.method public onFling(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 9
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "velocityX"    # F
    .param p4, "velocityY"    # F

    .prologue
    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 1132
    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    const/4 v5, 0x5

    if-ne v4, v5, :cond_0

    .line 1133
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v3, p4}, Lcom/google/android/videos/player/overlay/ScrubPad;->onFling(F)V

    .line 1180
    :goto_0
    return v2

    .line 1136
    :cond_0
    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    const/4 v5, 0x2

    if-eq v4, v5, :cond_1

    move v2, v3

    .line 1137
    goto :goto_0

    .line 1139
    :cond_1
    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    invoke-static {p4}, Ljava/lang/Math;->abs(F)F

    move-result v5

    cmpg-float v4, v4, v5

    if-ltz v4, :cond_2

    invoke-static {p3}, Ljava/lang/Math;->abs(F)F

    move-result v4

    const/high16 v5, 0x447a0000    # 1000.0f

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    invoke-virtual {v6}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v6

    iget v6, v6, Landroid/util/DisplayMetrics;->density:F

    mul-float/2addr v5, v6

    cmpg-float v4, v4, v5

    if-gez v4, :cond_3

    .line 1142
    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    move v2, v3

    .line 1143
    goto :goto_0

    .line 1146
    :cond_3
    invoke-static {p3}, Ljava/lang/Math;->signum(F)F

    move-result v4

    const v5, 0x461c4000    # 10000.0f

    mul-float/2addr v4, v5

    float-to-int v0, v4

    .line 1147
    .local v0, "skipMillis":I
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/videos/player/overlay/TimeBar;->getProgress()I

    move-result v4

    add-int v1, v4, v0

    .line 1148
    .local v1, "timeMillis":I
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v4

    invoke-static {v1, v4}, Ljava/lang/Math;->min(II)I

    move-result v4

    invoke-static {v3, v4}, Ljava/lang/Math;->max(II)I

    move-result v1

    .line 1149
    iput-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 1150
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v4, v3}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onScrubbingStart(Z)V

    .line 1151
    iput-boolean v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 1152
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v4, v3, v1, v2}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 1153
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v4}, Lcom/google/android/videos/player/overlay/TimeBar;->startScrubbing()V

    .line 1154
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v4, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 1156
    invoke-static {v0}, Ljava/lang/Integer;->signum(I)I

    move-result v4

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    invoke-static {v5}, Ljava/lang/Integer;->signum(I)I

    move-result v5

    if-eq v4, v5, :cond_4

    .line 1157
    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    .line 1159
    :cond_4
    iget v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    add-int/2addr v4, v0

    iput v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    .line 1161
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v5

    const v6, 0x7f0b01f7

    new-array v7, v2, [Ljava/lang/Object;

    iget v8, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->totalSkipMillis:I

    div-int/lit16 v8, v8, 0x3e8

    invoke-static {v8}, Ljava/lang/Math;->abs(I)I

    move-result v8

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    aput-object v8, v7, v3

    invoke-virtual {v5, v6, v7}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v4, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 1163
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressText:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->seekingProgressTextAnimation:Landroid/view/animation/Animation;

    .line 1166
    const/4 v3, 0x0

    cmpl-float v3, p3, v3

    if-lez v3, :cond_5

    .line 1167
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1168
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1169
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    .line 1171
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->startFlingAnimation()V

    goto/16 :goto_0

    .line 1173
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1174
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastForwardIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->endDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)V

    .line 1175
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startDelayedFadeAnimation(Landroid/view/View;Landroid/view/animation/Animation;)Landroid/view/animation/Animation;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicatorAnimation:Landroid/view/animation/Animation;

    .line 1177
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->startFlingAnimation()V

    goto/16 :goto_0
.end method

.method public onKeyDown(ILandroid/view/KeyEvent;)Z
    .locals 4
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 539
    invoke-virtual {p2}, Landroid/view/KeyEvent;->isSystem()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {p1}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->isMediaKey(I)Z

    move-result v3

    if-eqz v3, :cond_4

    :cond_0
    move v0, v2

    .line 540
    .local v0, "mediaOrNonSystemKey":Z
    :goto_0
    if-eqz v0, :cond_1

    .line 541
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    .line 543
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/videos/player/overlay/MediaKeyHelper;

    invoke-virtual {v3, p1, p2}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-nez v3, :cond_2

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyDown(ILandroid/view/KeyEvent;)Z

    move-result v3

    if-eqz v3, :cond_3

    :cond_2
    move v1, v2

    :cond_3
    return v1

    .end local v0    # "mediaOrNonSystemKey":Z
    :cond_4
    move v0, v1

    .line 539
    goto :goto_0
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 548
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/videos/player/overlay/MediaKeyHelper;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0, p1, p2}, Landroid/widget/FrameLayout;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected onLayout(ZIIII)V
    .locals 17
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    .line 563
    invoke-super/range {p0 .. p5}, Landroid/widget/FrameLayout;->onLayout(ZIIII)V

    .line 564
    sub-int v11, p4, p2

    .line 565
    .local v11, "w":I
    sub-int v6, p5, p3

    .line 566
    .local v6, "h":I
    div-int/lit8 v1, v11, 0x2

    .line 567
    .local v1, "cx":I
    div-int/lit8 v2, v6, 0x2

    .line 570
    .local v2, "cy":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v4, v2, v12

    .line 571
    .local v4, "errorOverlayTop":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredHeight()I

    move-result v12

    add-int v3, v4, v12

    .line 572
    .local v3, "errorOverlayBottom":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getLeft()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorOverlay:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getRight()I

    move-result v14

    invoke-virtual {v12, v13, v4, v14, v3}, Landroid/view/View;->layout(IIII)V

    .line 576
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v12}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v10, v2, v12

    .line 577
    .local v10, "spinnerTop":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v12}, Landroid/widget/ProgressBar;->getMeasuredHeight()I

    move-result v12

    add-int v7, v10, v12

    .line 578
    .local v7, "spinnerBottom":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v12}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v8, v1, v12

    .line 579
    .local v8, "spinnerLeft":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v12}, Landroid/widget/ProgressBar;->getMeasuredWidth()I

    move-result v12

    add-int v9, v8, v12

    .line 580
    .local v9, "spinnerRight":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->spinner:Landroid/widget/ProgressBar;

    invoke-virtual {v12, v8, v10, v9, v7}, Landroid/widget/ProgressBar;->layout(IIII)V

    .line 583
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    invoke-virtual {v12}, Landroid/view/View;->getMeasuredWidth()I

    move-result v12

    div-int/lit8 v12, v12, 0x2

    sub-int v5, v1, v12

    .line 584
    .local v5, "fineScrubberOverlayLeft":I
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    invoke-virtual {v13}, Landroid/view/View;->getTop()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    invoke-virtual {v14}, Landroid/view/View;->getMeasuredWidth()I

    move-result v14

    add-int/2addr v14, v5

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineScrubberOverlay:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getBottom()I

    move-result v15

    invoke-virtual {v12, v5, v13, v14, v15}, Landroid/view/View;->layout(IIII)V

    .line 587
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBarHitRect:Landroid/graphics/Rect;

    invoke-virtual {v12, v13}, Landroid/view/View;->getHitRect(Landroid/graphics/Rect;)V

    .line 588
    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-static {v13}, Landroid/support/v4/view/ViewCompat;->getX(Landroid/view/View;)F

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-static {v14}, Landroid/support/v4/view/ViewCompat;->getY(Landroid/view/View;)F

    move-result v14

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    invoke-virtual {v15}, Landroid/view/View;->getWidth()I

    move-result v15

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlBar:Landroid/view/View;

    move-object/from16 v16, v0

    invoke-virtual/range {v16 .. v16}, Landroid/view/View;->getHeight()I

    move-result v16

    invoke-virtual/range {v12 .. v16}, Lcom/google/android/videos/player/overlay/ScrubPad;->setCollapsedPositionAndSize(FFII)V

    .line 590
    return-void
.end method

.method public onLongPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1027
    return-void
.end method

.method public onOrientationChanged()V
    .locals 4

    .prologue
    .line 553
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getProgress()I

    move-result v2

    .line 554
    .local v2, "progress":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v1

    .line 555
    .local v1, "duration":I
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getBufferedPercent()I

    move-result v0

    .line 556
    .local v0, "bufferedPercent":I
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->inflate()V

    .line 557
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 558
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3, v2, v1, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->setTime(III)V

    .line 559
    return-void
.end method

.method public onScaling(F)V
    .locals 4
    .param p1, "scaleRatio"    # F

    .prologue
    const/4 v3, 0x0

    .line 1216
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eqz v1, :cond_0

    .line 1217
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadToggleButton:Landroid/widget/ImageButton;

    const/high16 v2, 0x43340000    # 180.0f

    mul-float/2addr v2, p1

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setRotation(F)V

    .line 1221
    :cond_0
    const/high16 v1, 0x437f0000    # 255.0f

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 1222
    .local v0, "alpha":I
    if-nez v0, :cond_1

    .line 1223
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundResource(I)V

    .line 1227
    :goto_0
    return-void

    .line 1225
    :cond_1
    invoke-static {v0, v3, v3, v3}, Landroid/graphics/Color;->argb(IIII)I

    move-result v1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundColor(I)V

    goto :goto_0
.end method

.method public onScroll(Landroid/view/MotionEvent;Landroid/view/MotionEvent;FF)Z
    .locals 11
    .param p1, "e1"    # Landroid/view/MotionEvent;
    .param p2, "e2"    # Landroid/view/MotionEvent;
    .param p3, "distanceX"    # F
    .param p4, "distanceY"    # F

    .prologue
    const/4 v9, 0x0

    const/4 v8, 0x2

    const/4 v10, 0x3

    const/4 v4, 0x1

    const/4 v3, 0x0

    .line 1060
    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v5, v8, :cond_1

    .line 1126
    :cond_0
    :goto_0
    return v3

    .line 1063
    :cond_1
    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    packed-switch v5, :pswitch_data_0

    goto :goto_0

    .line 1065
    :pswitch_0
    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eqz v5, :cond_2

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eq v5, v10, :cond_2

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getY()F

    move-result v6

    sub-float/2addr v5, v6

    invoke-static {v5}, Ljava/lang/Math;->abs(F)F

    move-result v5

    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v6

    invoke-virtual {p2}, Landroid/view/MotionEvent;->getX()F

    move-result v7

    sub-float/2addr v6, v7

    invoke-static {v6}, Ljava/lang/Math;->abs(F)F

    move-result v6

    cmpl-float v5, v5, v6

    if-lez v5, :cond_2

    .line 1068
    const/4 v3, 0x5

    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    .line 1069
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v3, p4}, Lcom/google/android/videos/player/overlay/ScrubPad;->onScroll(F)V

    move v3, v4

    .line 1070
    goto :goto_0

    .line 1072
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iput-wide v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->firstEventTimeMillis:J

    .line 1073
    iput v8, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    goto :goto_0

    .line 1076
    :pswitch_1
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    if-nez v5, :cond_0

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    if-eq v5, v4, :cond_0

    .line 1079
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    iget-wide v8, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->firstEventTimeMillis:J

    sub-long v0, v6, v8

    .line 1080
    .local v0, "elapsedTimeFromFirstEventMillis":J
    const-wide/16 v6, 0xc8

    cmp-long v5, v0, v6

    if-ltz v5, :cond_0

    .line 1081
    iput v10, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    .line 1082
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateLastScrubbingEvent(Landroid/view/MotionEvent;)V

    .line 1083
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startFineGrainedScrubbing()V

    move v3, v4

    .line 1084
    goto :goto_0

    .line 1088
    .end local v0    # "elapsedTimeFromFirstEventMillis":J
    :pswitch_2
    invoke-static {p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getScreenZone(Landroid/view/MotionEvent;)I

    move-result v5

    iput v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentZone:I

    .line 1089
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->touchExplorationEnabled:Z

    if-nez v5, :cond_4

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentZone:I

    int-to-float v5, v5

    mul-float/2addr v5, p3

    cmpg-float v5, v5, v9

    if-gez v5, :cond_4

    .line 1090
    const/4 v3, 0x4

    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    .line 1091
    iget v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentZone:I

    int-to-float v3, v3

    const/high16 v5, 0x40800000    # 4.0f

    mul-float/2addr v3, v5

    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->speedUp:F

    .line 1092
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const-wide/16 v6, 0x64

    invoke-virtual {v3, v10, v6, v7}, Landroid/os/Handler;->sendEmptyMessageDelayed(IJ)Z

    .line 1093
    iget v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->speedUp:F

    cmpl-float v3, v3, v9

    if-lez v3, :cond_3

    .line 1094
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->startAutoScrubAnimation()V

    .line 1099
    :goto_1
    invoke-virtual {p0, v10, v8}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->performHapticFeedback(II)Z

    :goto_2
    move v3, v4

    .line 1107
    goto/16 :goto_0

    .line 1096
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->startAutoScrubAnimation()V

    goto :goto_1

    .line 1102
    :cond_4
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateLastScrubbingEvent(Landroid/view/MotionEvent;)V

    .line 1103
    invoke-direct {p0, p2, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getTimeOffsetMillis(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)F

    move-result v5

    float-to-int v5, v5

    iget v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    add-int/2addr v5, v6

    iget v6, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingTimeOffsetMillis:I

    add-int v2, v5, v6

    .line 1105
    .local v2, "timeScrubTo":I
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v5}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v5

    invoke-static {v2, v5}, Ljava/lang/Math;->min(II)I

    move-result v5

    invoke-static {v3, v5}, Ljava/lang/Math;->max(II)I

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubTo(I)V

    goto :goto_2

    .line 1110
    .end local v2    # "timeScrubTo":I
    :pswitch_3
    invoke-static {p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getScreenZone(Landroid/view/MotionEvent;)I

    move-result v3

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentZone:I

    if-eq v3, v5, :cond_5

    .line 1111
    iput v10, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetectorState:I

    .line 1112
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    invoke-virtual {v3, v10}, Landroid/os/Handler;->removeMessages(I)V

    .line 1113
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1114
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 1115
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateLastScrubbingEvent(Landroid/view/MotionEvent;)V

    .line 1116
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v3}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v3

    iget v5, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingStartedAtTimeMillis:I

    sub-int/2addr v3, v5

    invoke-direct {p0, p2, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getTimeOffsetMillis(Landroid/view/MotionEvent;Landroid/view/MotionEvent;)F

    move-result v5

    float-to-int v5, v5

    sub-int/2addr v3, v5

    iput v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubbingTimeOffsetMillis:I

    :cond_5
    move v3, v4

    .line 1119
    goto/16 :goto_0

    .line 1122
    :pswitch_4
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v3, p4}, Lcom/google/android/videos/player/overlay/ScrubPad;->onScroll(F)V

    move v3, v4

    .line 1123
    goto/16 :goto_0

    .line 1063
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
    .end packed-switch
.end method

.method public onScrubbingCanceled()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 874
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 875
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onScrubbingEnd()V

    .line 876
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInGraphic()V

    .line 877
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v0, :cond_0

    .line 878
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideControls(Z)V

    .line 880
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onScrubbingCanceled()V

    .line 881
    return-void
.end method

.method public onScrubbingContinue(II)V
    .locals 3
    .param p1, "scrubberPositionInTimeBar"    # I
    .param p2, "scrubberMillis"    # I

    .prologue
    .line 837
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/TimeBar;->getDuration()I

    move-result v0

    .line 838
    .local v0, "durationMillis":I
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v2}, Lcom/google/android/videos/player/overlay/TimeBar;->getProgress()I

    move-result v1

    .line 839
    .local v1, "progressMillis":I
    if-gtz v0, :cond_0

    .line 844
    :goto_0
    return-void

    .line 842
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    invoke-virtual {v2, p2, v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onScrubbingContinue(II)V

    .line 843
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->repositionThumbnail()V

    goto :goto_0
.end method

.method public onScrubbingEnd(I)V
    .locals 3
    .param p1, "scrubberTimeMillis"    # I

    .prologue
    const/4 v2, 0x0

    .line 863
    iput-boolean v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 864
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->onScrubbingEnd()V

    .line 865
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeInGraphic()V

    .line 866
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    if-nez v0, :cond_0

    .line 867
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    .line 869
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    const/4 v1, 0x1

    invoke-interface {v0, v2, p1, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onSeekTo(ZIZ)V

    .line 870
    return-void
.end method

.method public onScrubbingStart()V
    .locals 3

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 825
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    .line 826
    iget v2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    if-ne v2, v0, :cond_1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    .line 827
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onScrubbingStart(Z)V

    .line 828
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPad:Lcom/google/android/videos/player/overlay/ScrubPad;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/overlay/ScrubPad;->fadeOutGraphic(Z)V

    .line 829
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->wasPlayingWhenSeekingStarted:Z

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v0, :cond_0

    .line 830
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->prepareToHideControls()V

    .line 831
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 833
    :cond_0
    return-void

    :cond_1
    move v0, v1

    .line 826
    goto :goto_0
.end method

.method public onShowPress(Landroid/view/MotionEvent;)V
    .locals 0
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1022
    return-void
.end method

.method public onSingleTapConfirmed(Landroid/view/MotionEvent;)Z
    .locals 3
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v0, 0x1

    .line 965
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_1

    .line 967
    const/4 v0, 0x0

    .line 981
    :cond_0
    :goto_0
    return v0

    .line 970
    :cond_1
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isSeeking:Z

    if-eqz v1, :cond_2

    .line 971
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->stopFineGrainedScrubbing()V

    goto :goto_0

    .line 975
    :cond_2
    iget v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eqz v1, :cond_3

    .line 976
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showControls()V

    goto :goto_0

    .line 977
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-eqz v1, :cond_0

    .line 979
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->startHiding(Z)V

    goto :goto_0
.end method

.method public onSingleTapUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1055
    const/4 v0, 0x0

    return v0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 523
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 524
    const/4 v0, 0x1

    .line 529
    :goto_0
    return v0

    .line 526
    :cond_0
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getSource()I

    move-result v0

    and-int/lit8 v0, v0, 0x2

    if-nez v0, :cond_1

    .line 527
    const/4 v0, 0x0

    goto :goto_0

    .line 529
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->gestureDetector:Lcom/google/android/videos/ui/GestureDetectorPlus;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/GestureDetectorPlus;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    goto :goto_0
.end method

.method public onUp(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "e"    # Landroid/view/MotionEvent;

    .prologue
    .line 1185
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->resetGesture()V

    .line 1186
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    .line 1187
    const/4 v0, 0x1

    return v0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v0, 0x0

    .line 702
    iput v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadState:I

    .line 703
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setBackgroundResource(I)V

    .line 704
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorVisible:Z

    .line 705
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasKnowledge:Z

    .line 706
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->useScrubPad:Z

    .line 707
    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fineGrainedScrubbingHappened:Z

    .line 708
    const-wide/16 v0, 0x0

    iput-wide v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->scrubPadInitialExpandStartTime:J

    .line 709
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->handler:Landroid/os/Handler;

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeMessages(I)V

    .line 710
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->rewindIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 711
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->fastforwardIndicator:Lcom/google/android/videos/player/overlay/ScrubbingIndicator;

    invoke-virtual {v0}, Lcom/google/android/videos/player/overlay/ScrubbingIndicator;->endAnimation()V

    .line 712
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    if-eqz v0, :cond_0

    .line 713
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    invoke-interface {v0}, Landroid/content/DialogInterface;->dismiss()V

    .line 714
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    .line 716
    :cond_0
    return-void
.end method

.method public setAudioTracks(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 678
    .local p1, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    if-eqz p1, :cond_0

    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    const/4 v1, 0x1

    if-le v0, v1, :cond_0

    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, p1}, Ljava/util/ArrayList;-><init>(Ljava/util/Collection;)V

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->audioTracks:Ljava/util/ArrayList;

    .line 679
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 680
    return-void

    .line 678
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 254
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->resetGesture()V

    .line 255
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorMessage:Ljava/lang/String;

    .line 256
    iput-object p2, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 257
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorCanTroubleshoot:Z

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorMessage:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 259
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setStateInternal(IZ)V

    .line 260
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    if-eqz v0, :cond_0

    .line 261
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryButton:Landroid/widget/Button;

    invoke-virtual {v0}, Landroid/widget/Button;->requestFocus()Z

    .line 263
    :cond_0
    return-void
.end method

.method public setHasKnowledge(Z)V
    .locals 0
    .param p1, "hasKnowledge"    # Z

    .prologue
    .line 640
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasKnowledge:Z

    .line 641
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateScrubPad()V

    .line 642
    return-void
.end method

.method public setHasSpinner(Z)V
    .locals 1
    .param p1, "hasSpinner"    # Z

    .prologue
    .line 297
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasSpinner:Z

    if-eq v0, p1, :cond_0

    .line 298
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hasSpinner:Z

    .line 299
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 301
    :cond_0
    return-void
.end method

.method public setHideable(Z)V
    .locals 1
    .param p1, "hideable"    # Z

    .prologue
    .line 311
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    if-ne v0, p1, :cond_0

    .line 320
    :goto_0
    return-void

    .line 314
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hideable:Z

    .line 315
    if-eqz p1, :cond_1

    .line 316
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    goto :goto_0

    .line 318
    :cond_1
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    goto :goto_0
.end method

.method public setHq(Z)V
    .locals 3
    .param p1, "on"    # Z

    .prologue
    .line 282
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqEnabled:Z

    .line 283
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {v1, p1}, Landroid/widget/ImageButton;->setSelected(Z)V

    .line 285
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqEnabled:Z

    if-eqz v1, :cond_0

    .line 286
    const v0, 0x7f0b01c6

    .line 292
    .local v0, "action":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v0}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 293
    return-void

    .line 287
    .end local v0    # "action":I
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isHqHd:Z

    if-eqz v1, :cond_1

    .line 288
    const v0, 0x7f0b01c4

    .restart local v0    # "action":I
    goto :goto_0

    .line 290
    .end local v0    # "action":I
    :cond_1
    const v0, 0x7f0b01c5

    .restart local v0    # "action":I
    goto :goto_0
.end method

.method public setHqIsHd(Z)V
    .locals 2
    .param p1, "hqIsHd"    # Z

    .prologue
    .line 273
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isHqHd:Z

    if-eq v0, p1, :cond_0

    .line 274
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->isHqHd:Z

    .line 275
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqButton:Landroid/widget/ImageButton;

    if-eqz p1, :cond_1

    const v0, 0x7f0201c5

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setImageResource(I)V

    .line 276
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->hqEnabled:Z

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setHq(Z)V

    .line 278
    :cond_0
    return-void

    .line 275
    :cond_1
    const v0, 0x7f0201c6

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    .line 221
    return-void
.end method

.method public setSelectedAudioTrackIndex(I)V
    .locals 0
    .param p1, "trackIndex"    # I

    .prologue
    .line 684
    iput p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedAudioTrackIndex:I

    .line 685
    return-void
.end method

.method public setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 669
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-ne v0, p1, :cond_0

    .line 674
    :goto_0
    return-void

    .line 672
    :cond_0
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 673
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->ccButton:Landroid/widget/ImageButton;

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-eqz v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {v1, v0}, Landroid/widget/ImageButton;->setSelected(Z)V

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method public setState(ZZ)V
    .locals 2
    .param p1, "playing"    # Z
    .param p2, "loading"    # Z

    .prologue
    const/4 v1, 0x0

    .line 246
    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 247
    iput-object v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorMessage:Ljava/lang/String;

    .line 248
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->errorText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 249
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, v0, p2}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->setStateInternal(IZ)V

    .line 250
    return-void

    .line 249
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    .locals 1
    .param p1, "storyboards"    # [Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .prologue
    .line 241
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->storyboardHelper:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->setStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 242
    return-void
.end method

.method public setSubtitles(Ljava/util/List;Z)V
    .locals 2
    .param p2, "includeDisableOption"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v0, 0x0

    .line 658
    if-nez p1, :cond_0

    .line 659
    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    .line 664
    :goto_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 665
    return-void

    .line 661
    :cond_0
    if-eqz p2, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->activity:Landroid/support/v4/app/FragmentActivity;

    const v1, 0x7f0b01ed

    invoke-virtual {v0, v1}, Landroid/support/v4/app/FragmentActivity;->getString(I)Ljava/lang/String;

    move-result-object v0

    :cond_1
    invoke-static {p1, v0}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->getSelectableTracks(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->subtitleTracks:Ljava/util/ArrayList;

    goto :goto_0
.end method

.method public setSupportsQualityToggle(Z)V
    .locals 0
    .param p1, "supportsQualityToggle"    # Z

    .prologue
    .line 267
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->supportsQualityToggle:Z

    .line 268
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 269
    return-void
.end method

.method public setTimes(III)V
    .locals 1
    .param p1, "currentTimeMillis"    # I
    .param p2, "totalTimeMillis"    # I
    .param p3, "bufferedPercent"    # I

    .prologue
    .line 305
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->timeBar:Lcom/google/android/videos/player/overlay/TimeBar;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/player/overlay/TimeBar;->setTime(III)V

    .line 306
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->mediaKeyHelper:Lcom/google/android/videos/player/overlay/MediaKeyHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/player/overlay/MediaKeyHelper;->setTimes(III)V

    .line 307
    return-void
.end method

.method public setTouchExplorationEnabled(Z)V
    .locals 0
    .param p1, "touchExplorationEnabled"    # Z

    .prologue
    .line 324
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->touchExplorationEnabled:Z

    .line 325
    return-void
.end method

.method public setUseScrubPad(Z)V
    .locals 0
    .param p1, "useScrubPad"    # Z

    .prologue
    .line 372
    iput-boolean p1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->useScrubPad:Z

    .line 373
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateScrubPad()V

    .line 374
    return-void
.end method

.method public setVideoInfo(Ljava/lang/String;IILjava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "durationMillis"    # I
    .param p3, "resumeTimeMillis"    # I
    .param p4, "videoTitle"    # Ljava/lang/String;

    .prologue
    .line 237
    return-void
.end method

.method public showControls()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 335
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->persistHideAtStateChange:Z

    .line 336
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->cancelHiding()V

    .line 337
    iget v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    if-eqz v0, :cond_0

    .line 338
    iput v1, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->controlsVisibility:I

    .line 339
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->updateViews()V

    .line 340
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    if-eqz v0, :cond_0

    .line 341
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->listener:Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay$Listener;->onShown()V

    .line 344
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->maybeStartHiding()V

    .line 345
    return-void
.end method

.method public showDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;II)V
    .locals 6
    .param p1, "dialogId"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "message"    # Ljava/lang/CharSequence;
    .param p4, "positiveButtonStringId"    # I
    .param p5, "negativeButtonStringId"    # I

    .prologue
    .line 726
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    if-eqz v4, :cond_0

    .line 727
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    invoke-interface {v4}, Landroid/content/DialogInterface;->dismiss()V

    .line 730
    :cond_0
    new-instance v2, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$1;

    invoke-direct {v2, p0, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$1;-><init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;I)V

    .line 737
    .local v2, "confirmClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$2;-><init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;I)V

    .line 744
    .local v0, "cancelClickListener":Landroid/content/DialogInterface$OnClickListener;
    new-instance v1, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$3;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay$3;-><init>(Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;I)V

    .line 752
    .local v1, "cancelListener":Landroid/content/DialogInterface$OnCancelListener;
    new-instance v4, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->getContext()Landroid/content/Context;

    move-result-object v5

    invoke-direct {v4, v5}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v4, p2}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, p3}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, p4, v2}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, p5, v0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4, v1}, Landroid/app/AlertDialog$Builder;->setOnCancelListener(Landroid/content/DialogInterface$OnCancelListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v4

    invoke-virtual {v4}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v3

    .line 759
    .local v3, "dialog":Landroid/app/Dialog;
    invoke-virtual {v3}, Landroid/app/Dialog;->show()V

    .line 760
    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->currentDialog:Landroid/content/DialogInterface;

    .line 761
    return-void
.end method

.method public showShortClockConfirmationDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;Ljava/lang/String;)V
    .locals 6
    .param p1, "dialogId"    # I
    .param p2, "title"    # Ljava/lang/CharSequence;
    .param p3, "message"    # Ljava/lang/CharSequence;
    .param p4, "posterUri"    # Ljava/lang/String;

    .prologue
    .line 766
    const/4 v2, 0x0

    const v4, 0x1040013

    const v5, 0x1040009

    move-object v0, p0

    move v1, p1

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/player/overlay/DefaultControllerOverlay;->showDialog(ILjava/lang/CharSequence;Ljava/lang/CharSequence;II)V

    .line 767
    return-void
.end method

.method public showingChildActivity()Z
    .locals 1

    .prologue
    .line 771
    const/4 v0, 0x0

    return v0
.end method

.method public stopSeek()V
    .locals 0

    .prologue
    .line 721
    return-void
.end method
