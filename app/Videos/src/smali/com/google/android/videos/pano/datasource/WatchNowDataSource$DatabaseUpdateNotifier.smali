.class Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "WatchNowDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DatabaseUpdateNotifier"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V
    .locals 0

    .prologue
    .line 372
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource;
    .param p2, "x1"    # Lcom/google/android/videos/pano/datasource/WatchNowDataSource$1;

    .prologue
    .line 372
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;-><init>(Lcom/google/android/videos/pano/datasource/WatchNowDataSource;)V

    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 386
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->scheduleUpdate(Z)V

    .line 387
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 376
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->scheduleUpdate(Z)V

    .line 377
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 391
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->scheduleUpdate(Z)V

    .line 392
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 381
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/WatchNowDataSource$DatabaseUpdateNotifier;->this$0:Lcom/google/android/videos/pano/datasource/WatchNowDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/WatchNowDataSource;->scheduleUpdate(Z)V

    .line 382
    return-void
.end method
