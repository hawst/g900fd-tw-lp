.class public Lcom/google/android/videos/utils/TimeUtil;
.super Ljava/lang/Object;
.source "TimeUtil.java"


# static fields
.field private static calendar:Ljava/util/Calendar;

.field private static dateFormat:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Locale;",
            "Ljava/text/DateFormat;",
            ">;"
        }
    .end annotation
.end field

.field private static standaloneYearFormat:Landroid/util/Pair;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/util/Pair",
            "<",
            "Ljava/util/Locale;",
            "Ljava/text/SimpleDateFormat;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public static getAccessibilityString(Landroid/content/Context;I)Ljava/lang/String;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "progressMillis"    # I

    .prologue
    .line 104
    div-int/lit16 v3, p1, 0x3e8

    .line 105
    .local v3, "progressTotalSeconds":I
    rem-int/lit8 v2, v3, 0x3c

    .line 106
    .local v2, "progressSeconds":I
    div-int/lit8 v4, v3, 0x3c

    rem-int/lit8 v1, v4, 0x3c

    .line 107
    .local v1, "progressMinutes":I
    div-int/lit16 v0, v3, 0xe10

    .line 109
    .local v0, "progressHours":I
    const v4, 0x7f0b01ca

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x2

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    invoke-virtual {p0, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    return-object v4
.end method

.method public static getAccessibilityString(Landroid/content/Context;II)Ljava/lang/String;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "progressMillis"    # I
    .param p2, "durationMillis"    # I

    .prologue
    .line 115
    const v0, 0x7f0b01cb

    const/4 v1, 0x2

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-static {p0, p1}, Lcom/google/android/videos/utils/TimeUtil;->getAccessibilityString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    const/4 v2, 0x1

    invoke-static {p0, p2}, Lcom/google/android/videos/utils/TimeUtil;->getAccessibilityString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v3

    aput-object v3, v1, v2

    invoke-virtual {p0, v0, v1}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getBestYearPatternV18(Ljava/util/Locale;)Ljava/lang/String;
    .locals 1
    .param p0, "locale"    # Ljava/util/Locale;

    .prologue
    .line 172
    const-string v0, "y"

    invoke-static {p0, v0}, Landroid/text/format/DateFormat;->getBestDateTimePattern(Ljava/util/Locale;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private static getCalendar()Ljava/util/Calendar;
    .locals 1

    .prologue
    .line 164
    sget-object v0, Lcom/google/android/videos/utils/TimeUtil;->calendar:Ljava/util/Calendar;

    if-nez v0, :cond_0

    .line 165
    const-string v0, "UTC"

    invoke-static {v0}, Ljava/util/TimeZone;->getTimeZone(Ljava/lang/String;)Ljava/util/TimeZone;

    move-result-object v0

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/TimeZone;)Ljava/util/Calendar;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/utils/TimeUtil;->calendar:Ljava/util/Calendar;

    .line 167
    :cond_0
    sget-object v0, Lcom/google/android/videos/utils/TimeUtil;->calendar:Ljava/util/Calendar;

    return-object v0
.end method

.method public static declared-synchronized getDateString(J)Ljava/lang/String;
    .locals 8
    .param p0, "dateTimestampSeconds"    # J

    .prologue
    .line 154
    const-class v3, Lcom/google/android/videos/utils/TimeUtil;

    monitor-enter v3

    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 155
    .local v1, "locale":Ljava/util/Locale;
    sget-object v2, Lcom/google/android/videos/utils/TimeUtil;->dateFormat:Landroid/util/Pair;

    if-eqz v2, :cond_0

    sget-object v2, Lcom/google/android/videos/utils/TimeUtil;->dateFormat:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v2, v1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 156
    :cond_0
    const/4 v2, 0x2

    invoke-static {v2, v1}, Ljava/text/DateFormat;->getDateInstance(ILjava/util/Locale;)Ljava/text/DateFormat;

    move-result-object v0

    .line 158
    .local v0, "format":Ljava/text/DateFormat;
    invoke-static {v1, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v2

    sput-object v2, Lcom/google/android/videos/utils/TimeUtil;->dateFormat:Landroid/util/Pair;

    .line 160
    .end local v0    # "format":Ljava/text/DateFormat;
    :cond_1
    sget-object v2, Lcom/google/android/videos/utils/TimeUtil;->dateFormat:Landroid/util/Pair;

    iget-object v2, v2, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v2, Ljava/text/DateFormat;

    new-instance v4, Ljava/util/Date;

    const-wide/16 v6, 0x3e8

    mul-long/2addr v6, p0

    invoke-direct {v4, v6, v7}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v2, v4}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v2

    monitor-exit v3

    return-object v2

    .line 154
    .end local v1    # "locale":Ljava/util/Locale;
    :catchall_0
    move-exception v2

    monitor-exit v3

    throw v2
.end method

.method public static getDurationString(IZ)Ljava/lang/String;
    .locals 9
    .param p0, "millis"    # I
    .param p1, "showHours"    # Z

    .prologue
    const/16 v8, 0x3a

    const/16 v7, 0x30

    const/16 v6, 0xa

    .line 80
    div-int/lit16 v4, p0, 0x3e8

    .line 81
    .local v4, "totalSeconds":I
    rem-int/lit8 v2, v4, 0x3c

    .line 82
    .local v2, "seconds":I
    div-int/lit8 v5, v4, 0x3c

    rem-int/lit8 v1, v5, 0x3c

    .line 83
    .local v1, "minutes":I
    div-int/lit16 v0, v4, 0xe10

    .line 85
    .local v0, "hours":I
    new-instance v3, Ljava/lang/StringBuilder;

    const/16 v5, 0x8

    invoke-direct {v3, v5}, Ljava/lang/StringBuilder;-><init>(I)V

    .line 86
    .local v3, "stringBuilder":Ljava/lang/StringBuilder;
    if-gtz v0, :cond_0

    if-eqz p1, :cond_1

    .line 87
    :cond_0
    invoke-virtual {v3, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 88
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 90
    :cond_1
    if-ge v1, v6, :cond_2

    .line 91
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 93
    :cond_2
    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 94
    invoke-virtual {v3, v8}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 95
    if-ge v2, v6, :cond_3

    .line 96
    invoke-virtual {v3, v7}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 98
    :cond_3
    invoke-virtual {v3, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 99
    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    return-object v5
.end method

.method public static getRemainingDays(JJ)I
    .locals 6
    .param p0, "expirationTimestamp"    # J
    .param p2, "nowTimestamp"    # J

    .prologue
    .line 43
    sub-long v2, p0, p2

    const-wide/32 v4, 0x5265c00

    div-long v0, v2, v4

    .line 44
    .local v0, "remainingDays":J
    const-wide/16 v2, 0x0

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    const/4 v2, -0x1

    :goto_0
    return v2

    :cond_0
    const-wide/32 v2, 0x7fffffff

    cmp-long v2, v0, v2

    if-lez v2, :cond_1

    const v2, 0x7fffffff

    goto :goto_0

    :cond_1
    long-to-int v2, v0

    goto :goto_0
.end method

.method public static getShortClockTimeString(JLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 8
    .param p0, "millis"    # J
    .param p2, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/4 v6, 0x1

    const/4 v7, 0x0

    .line 121
    const-wide/32 v4, 0x5265c00

    div-long v0, p0, v4

    .line 122
    .local v0, "days":J
    const-wide/32 v4, 0x36ee80

    div-long v2, p0, v4

    .line 123
    .local v2, "hours":J
    const-wide/16 v4, 0x2

    cmp-long v4, v0, v4

    if-lez v4, :cond_0

    const v4, 0x7f0b018d

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v0, v1}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    :goto_0
    return-object v4

    :cond_0
    const v4, 0x7f0b018c

    new-array v5, v6, [Ljava/lang/Object;

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {p2, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0
.end method

.method public static declared-synchronized getStandaloneYearString(I)Ljava/lang/String;
    .locals 6
    .param p0, "year"    # I

    .prologue
    .line 143
    const-class v4, Lcom/google/android/videos/utils/TimeUtil;

    monitor-enter v4

    :try_start_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    .line 144
    .local v1, "locale":Ljava/util/Locale;
    sget-object v3, Lcom/google/android/videos/utils/TimeUtil;->standaloneYearFormat:Landroid/util/Pair;

    if-eqz v3, :cond_0

    sget-object v3, Lcom/google/android/videos/utils/TimeUtil;->standaloneYearFormat:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->first:Ljava/lang/Object;

    invoke-static {v3, v1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 145
    :cond_0
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v5, 0x12

    if-lt v3, v5, :cond_2

    invoke-static {v1}, Lcom/google/android/videos/utils/TimeUtil;->getBestYearPatternV18(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v2

    .line 146
    .local v2, "pattern":Ljava/lang/String;
    :goto_0
    new-instance v3, Ljava/text/SimpleDateFormat;

    invoke-direct {v3, v2, v1}, Ljava/text/SimpleDateFormat;-><init>(Ljava/lang/String;Ljava/util/Locale;)V

    invoke-static {v1, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    sput-object v3, Lcom/google/android/videos/utils/TimeUtil;->standaloneYearFormat:Landroid/util/Pair;

    .line 148
    .end local v2    # "pattern":Ljava/lang/String;
    :cond_1
    invoke-static {}, Lcom/google/android/videos/utils/TimeUtil;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 149
    .local v0, "calendar":Ljava/util/Calendar;
    const/4 v3, 0x1

    const/4 v5, 0x1

    invoke-virtual {v0, p0, v3, v5}, Ljava/util/Calendar;->set(III)V

    .line 150
    sget-object v3, Lcom/google/android/videos/utils/TimeUtil;->standaloneYearFormat:Landroid/util/Pair;

    iget-object v3, v3, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v3, Ljava/text/SimpleDateFormat;

    invoke-virtual {v0}, Ljava/util/Calendar;->getTime()Ljava/util/Date;

    move-result-object v5

    invoke-virtual {v3, v5}, Ljava/text/SimpleDateFormat;->format(Ljava/util/Date;)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v3

    monitor-exit v4

    return-object v3

    .line 145
    .end local v0    # "calendar":Ljava/util/Calendar;
    :cond_2
    :try_start_1
    const-string v2, "y"
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 143
    .end local v1    # "locale":Ljava/util/Locale;
    :catchall_0
    move-exception v3

    monitor-exit v4

    throw v3
.end method

.method public static getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;
    .locals 10
    .param p0, "expirationTimestamp"    # J
    .param p2, "nowTimestamp"    # J
    .param p4, "resources"    # Landroid/content/res/Resources;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 54
    cmp-long v3, p2, p0

    if-ltz v3, :cond_0

    .line 55
    const v3, 0x7f0b01fa

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 75
    :goto_0
    return-object v3

    .line 58
    :cond_0
    sub-long v6, p0, p2

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    const-wide/16 v8, 0x3c

    div-long/2addr v6, v8

    long-to-int v2, v6

    .line 59
    .local v2, "remainingMinutes":I
    div-int/lit8 v1, v2, 0x3c

    .line 62
    .local v1, "remainingHours":I
    if-lez v1, :cond_2

    rem-int/lit8 v3, v2, 0x3c

    if-lez v3, :cond_1

    move v3, v4

    :goto_1
    add-int/2addr v1, v3

    .line 63
    div-int/lit8 v0, v1, 0x18

    .line 66
    .local v0, "remainingDays":I
    if-lez v0, :cond_4

    rem-int/lit8 v3, v1, 0x18

    if-lez v3, :cond_3

    move v3, v4

    :goto_2
    add-int/2addr v0, v3

    .line 68
    if-lez v0, :cond_5

    .line 69
    const v3, 0x7f130002

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p4, v3, v0, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .end local v0    # "remainingDays":I
    :cond_1
    move v3, v5

    .line 62
    goto :goto_1

    :cond_2
    move v3, v5

    goto :goto_1

    .restart local v0    # "remainingDays":I
    :cond_3
    move v3, v5

    .line 66
    goto :goto_2

    :cond_4
    move v3, v5

    goto :goto_2

    .line 70
    :cond_5
    if-lez v1, :cond_6

    .line 71
    const v3, 0x7f130003

    new-array v4, v4, [Ljava/lang/Object;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p4, v3, v1, v4}, Landroid/content/res/Resources;->getQuantityString(II[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 72
    :cond_6
    const/16 v3, 0xa

    if-le v2, v3, :cond_7

    .line 73
    const v3, 0x7f0b01f8

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0

    .line 75
    :cond_7
    const v3, 0x7f0b01f9

    invoke-virtual {p4, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method public static declared-synchronized getYear(J)I
    .locals 6
    .param p0, "dateTimestampSec"    # J

    .prologue
    .line 129
    const-class v2, Lcom/google/android/videos/utils/TimeUtil;

    monitor-enter v2

    :try_start_0
    invoke-static {}, Lcom/google/android/videos/utils/TimeUtil;->getCalendar()Ljava/util/Calendar;

    move-result-object v0

    .line 130
    .local v0, "calendar":Ljava/util/Calendar;
    const-wide/16 v4, 0x3e8

    mul-long/2addr v4, p0

    invoke-virtual {v0, v4, v5}, Ljava/util/Calendar;->setTimeInMillis(J)V

    .line 131
    const/4 v1, 0x1

    invoke-virtual {v0, v1}, Ljava/util/Calendar;->get(I)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v1

    monitor-exit v2

    return v1

    .line 129
    .end local v0    # "calendar":Ljava/util/Calendar;
    :catchall_0
    move-exception v1

    monitor-exit v2

    throw v1
.end method

.method public static toDaysFromSeconds(I)I
    .locals 1
    .param p0, "seconds"    # I

    .prologue
    .line 135
    div-int/lit8 v0, p0, 0x3c

    div-int/lit8 v0, v0, 0x3c

    div-int/lit8 v0, v0, 0x18

    return v0
.end method

.method public static toHoursFromSeconds(I)I
    .locals 1
    .param p0, "seconds"    # I

    .prologue
    .line 139
    div-int/lit8 v0, p0, 0x3c

    div-int/lit8 v0, v0, 0x3c

    return v0
.end method
