.class final Lcom/google/android/videos/api/RecommendationsConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "RecommendationsConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/RecommendationsRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 40
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "recommendation"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/RecommendationsConverter;->baseUri:Ljava/lang/String;

    .line 41
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/RecommendationsConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 42
    return-void
.end method

.method private buildCategoryFromType(I)Ljava/lang/String;
    .locals 3
    .param p1, "type"    # I

    .prologue
    .line 70
    sparse-switch p1, :sswitch_data_0

    .line 80
    new-instance v0, Ljava/lang/IllegalArgumentException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unknown type "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 72
    :sswitch_0
    const-string v0, "type=movie"

    .line 78
    :goto_0
    return-object v0

    .line 74
    :sswitch_1
    const-string v0, "type=episode"

    goto :goto_0

    .line 76
    :sswitch_2
    const-string v0, "type=show"

    goto :goto_0

    .line 78
    :sswitch_3
    const-string v0, "type=season"

    goto :goto_0

    .line 70
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_2
        0x13 -> :sswitch_3
        0x14 -> :sswitch_1
    .end sparse-switch
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 25
    check-cast p1, Lcom/google/android/videos/api/RecommendationsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/RecommendationsConverter;->convertRequest(Lcom/google/android/videos/api/RecommendationsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/RecommendationsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/RecommendationsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsConverter;->baseUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/ApiUriBuilder;->create(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "max"

    iget v3, p1, Lcom/google/android/videos/api/RecommendationsRequest;->max:I

    invoke-static {v3}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "cat"

    iget v3, p1, Lcom/google/android/videos/api/RecommendationsRequest;->type:I

    invoke-direct {p0, v3}, Lcom/google/android/videos/api/RecommendationsConverter;->buildCategoryFromType(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/api/RecommendationsRequest;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictCountry(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/api/RecommendationsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictLocale(Ljava/util/Locale;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget v2, p1, Lcom/google/android/videos/api/RecommendationsRequest;->flags:I

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->addFlags(I)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v0

    .line 53
    .local v0, "uriBuilder":Lcom/google/android/videos/api/ApiUriBuilder;
    iget-object v1, p1, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    if-eqz v1, :cond_0

    .line 54
    const-string v1, "q"

    iget-object v2, p1, Lcom/google/android/videos/api/RecommendationsRequest;->queryAssetId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    .line 57
    :cond_0
    iget-object v1, p1, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    .line 58
    const-string v1, "mcc_mnc"

    iget-object v2, p1, Lcom/google/android/videos/api/RecommendationsRequest;->mccMnc:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    .line 60
    :cond_1
    invoke-virtual {v0}, Lcom/google/android/videos/api/ApiUriBuilder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 66
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/RecommendationsConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 25
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/RecommendationsConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    move-result-object v0

    return-object v0
.end method
