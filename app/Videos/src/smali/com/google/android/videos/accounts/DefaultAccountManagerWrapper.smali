.class public Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;
.super Lcom/google/android/videos/accounts/AccountManagerWrapper;
.source "DefaultAccountManagerWrapper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/accounts/AccountManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "accountManager"    # Landroid/accounts/AccountManager;
    .param p2, "scope"    # Ljava/lang/String;
    .param p3, "accountType"    # Ljava/lang/String;

    .prologue
    .line 29
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;-><init>(Landroid/accounts/AccountManager;Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-void
.end method


# virtual methods
.method protected blockingGetAuthTokenInternal(Landroid/accounts/Account;)Ljava/lang/String;
    .locals 5
    .param p1, "account"    # Landroid/accounts/Account;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;
        }
    .end annotation

    .prologue
    .line 40
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v3, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->scope:Ljava/lang/String;

    const/4 v4, 0x0

    invoke-virtual {v2, p1, v3, v4}, Landroid/accounts/AccountManager;->blockingGetAuthToken(Landroid/accounts/Account;Ljava/lang/String;Z)Ljava/lang/String;

    move-result-object v0

    .line 41
    .local v0, "authToken":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 42
    new-instance v2, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    const-string v3, "Null auth token"

    invoke-direct {v2, v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/String;)V

    throw v2
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_2

    .line 45
    .end local v0    # "authToken":Ljava/lang/String;
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Landroid/accounts/OperationCanceledException;
    new-instance v2, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v2, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 47
    .end local v1    # "e":Landroid/accounts/OperationCanceledException;
    :catch_1
    move-exception v1

    .line 48
    .local v1, "e":Landroid/accounts/AuthenticatorException;
    const-string v2, "blockingGetUserAuth failed with AuthenticatorException"

    invoke-static {v2, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 49
    new-instance v2, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v2, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 50
    .end local v1    # "e":Landroid/accounts/AuthenticatorException;
    :catch_2
    move-exception v1

    .line 51
    .local v1, "e":Ljava/io/IOException;
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "blockingGetUserAuth failed with IOException: "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v1}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 52
    new-instance v2, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;

    invoke-direct {v2, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException;-><init>(Ljava/lang/Throwable;)V

    throw v2

    .line 44
    .end local v1    # "e":Ljava/io/IOException;
    .restart local v0    # "authToken":Ljava/lang/String;
    :cond_0
    return-object v0
.end method

.method protected getAuthTokenInternal(Landroid/accounts/Account;Landroid/app/Activity;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 7
    .param p1, "account"    # Landroid/accounts/Account;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "callback"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    const/4 v3, 0x0

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v2, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->scope:Ljava/lang/String;

    new-instance v5, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;

    iget-object v1, p1, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-direct {v5, p0, v1, p3}, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;-><init>(Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V

    move-object v1, p1

    move-object v4, p2

    move-object v6, v3

    invoke-virtual/range {v0 .. v6}, Landroid/accounts/AccountManager;->getAuthToken(Landroid/accounts/Account;Ljava/lang/String;Landroid/os/Bundle;Landroid/app/Activity;Landroid/accounts/AccountManagerCallback;Landroid/os/Handler;)Landroid/accounts/AccountManagerFuture;

    .line 60
    return-void
.end method

.method public invalidateAuthToken(Ljava/lang/String;)V
    .locals 2
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 34
    iget-object v0, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->accountManager:Landroid/accounts/AccountManager;

    iget-object v1, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;->accountType:Ljava/lang/String;

    invoke-virtual {v0, v1, p1}, Landroid/accounts/AccountManager;->invalidateAuthToken(Ljava/lang/String;Ljava/lang/String;)V

    .line 35
    return-void
.end method
