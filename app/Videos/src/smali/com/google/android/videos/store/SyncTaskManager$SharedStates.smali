.class final Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
.super Ljava/lang/Object;
.source "SyncTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SyncTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x18
    name = "SharedStates"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<-TR;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private completed:Z

.field private maxErrorLevel:I

.field private maxException:Ljava/lang/Exception;

.field private pendingTaskCount:I

.field private final request:Lcom/google/android/videos/async/ControllableRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;"
        }
    .end annotation
.end field

.field private final scheduledAssets:Ljava/util/Set;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Set",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;",
            "Lcom/google/android/videos/async/NewCallback",
            "<-TR;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TR;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<-TR;Ljava/lang/Void;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 194
    iput-object p1, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    .line 195
    iput-object p2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->callback:Lcom/google/android/videos/async/NewCallback;

    .line 196
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->scheduledAssets:Ljava/util/Set;

    .line 197
    return-void
.end method

.method public static create()Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v1, 0x0

    .line 206
    new-instance v0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-direct {v0, v1, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;-><init>(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    return-object v0
.end method

.method public static create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<TR;>;",
            "Lcom/google/android/videos/async/NewCallback",
            "<-TR;",
            "Ljava/lang/Void;",
            ">;)",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<TR;>;"
        }
    .end annotation

    .prologue
    .line 201
    .local p0, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<TR;>;"
    .local p1, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<-TR;Ljava/lang/Void;>;"
    new-instance v2, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/ControllableRequest;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/async/NewCallback;

    invoke-direct {v2, v0, v1}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;-><init>(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    return-object v2
.end method


# virtual methods
.method public declared-synchronized addScheduledAsset(Ljava/lang/String;)Z
    .locals 2
    .param p1, "assetTag"    # Ljava/lang/String;

    .prologue
    .line 275
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->completed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "addScheduledAsset called after sync process was completed"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 277
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->scheduledAssets:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->add(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    .line 275
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized decrement()V
    .locals 5

    .prologue
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    const/4 v1, 0x0

    const/4 v0, 0x1

    .line 224
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->completed:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    const-string v3, "decrement called after sync process was completed"

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 225
    iget v2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I

    if-lez v2, :cond_2

    :goto_1
    const-string v1, "decrement called when no task was pending"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 226
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I

    add-int/lit8 v0, v0, -0x1

    iput v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I

    if-nez v0, :cond_0

    .line 227
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->completed:Z

    .line 228
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->callback:Lcom/google/android/videos/async/NewCallback;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    if-nez v0, :cond_3

    .line 239
    :cond_0
    :goto_2
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 224
    goto :goto_0

    :cond_2
    move v0, v1

    .line 225
    goto :goto_1

    .line 231
    :cond_3
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    invoke-virtual {v0}, Lcom/google/android/videos/async/ControllableRequest;->isCancelled()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 232
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->callback:Lcom/google/android/videos/async/NewCallback;

    iget-object v1, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    iget-object v1, v1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    invoke-interface {v0, v1}, Lcom/google/android/videos/async/NewCallback;->onCancelled(Ljava/lang/Object;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_2

    .line 224
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0

    .line 233
    :cond_4
    :try_start_2
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxErrorLevel:I

    if-nez v0, :cond_5

    .line 234
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->callback:Lcom/google/android/videos/async/NewCallback;

    iget-object v1, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    iget-object v1, v1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/NewCallback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_2

    .line 236
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->callback:Lcom/google/android/videos/async/NewCallback;

    iget-object v1, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    iget-object v1, v1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    new-instance v2, Lcom/google/android/videos/store/SyncTaskManager$SyncException;

    iget v3, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxErrorLevel:I

    iget-object v4, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxException:Ljava/lang/Exception;

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/store/SyncTaskManager$SyncException;-><init>(ILjava/lang/Exception;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/NewCallback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    goto :goto_2
.end method

.method public declared-synchronized increment()V
    .locals 2

    .prologue
    .line 213
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    monitor-enter p0

    :try_start_0
    iget-boolean v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->completed:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "increment called after sync process was completed"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 214
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 215
    monitor-exit p0

    return-void

    .line 213
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized isAssetScheduled(Ljava/lang/String;)Z
    .locals 1
    .param p1, "assetTag"    # Ljava/lang/String;

    .prologue
    .line 264
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->scheduledAssets:Ljava/util/Set;

    invoke-interface {v0, p1}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v0

    monitor-exit p0

    return v0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public isCanceled()Z
    .locals 1

    .prologue
    .line 281
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->request:Lcom/google/android/videos/async/ControllableRequest;

    iget-object v0, v0, Lcom/google/android/videos/async/ControllableRequest;->taskStatus:Lcom/google/android/videos/async/TaskStatus;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskStatus;->isCancelled(Lcom/google/android/videos/async/TaskStatus;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public declared-synchronized onError(ILjava/lang/Exception;)V
    .locals 4
    .param p1, "errorLevel"    # I
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .local p0, "this":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<TR;>;"
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 246
    monitor-enter p0

    :try_start_0
    iget-boolean v2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->completed:Z

    if-nez v2, :cond_1

    move v2, v0

    :goto_0
    const-string v3, "onError called after sync process was completed"

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 247
    iget v2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->pendingTaskCount:I

    if-lez v2, :cond_2

    :goto_1
    const-string v1, "onError called when no task was pending"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 248
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 249
    iget v0, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxErrorLevel:I

    if-le p1, v0, :cond_0

    .line 250
    iput p1, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxErrorLevel:I

    .line 251
    iput-object p2, p0, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->maxException:Ljava/lang/Exception;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 253
    :cond_0
    monitor-exit p0

    return-void

    :cond_1
    move v2, v1

    .line 246
    goto :goto_0

    :cond_2
    move v0, v1

    .line 247
    goto :goto_1

    .line 246
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
