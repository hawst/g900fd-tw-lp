.class public Lcom/google/android/videos/store/VideoUserContentService;
.super Landroid/app/Service;
.source "VideoUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;,
        Lcom/google/android/videos/store/VideoUserContentService$UpdateNotifier;
    }
.end annotation


# instance fields
.field private binder:Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 57
    invoke-direct {p0}, Landroid/app/Service;-><init>()V

    .line 121
    return-void
.end method


# virtual methods
.method public onBind(Landroid/content/Intent;)Landroid/os/IBinder;
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService;->binder:Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;

    return-object v0
.end method

.method public onCreate()V
    .locals 1

    .prologue
    .line 62
    invoke-super {p0}, Landroid/app/Service;->onCreate()V

    .line 63
    new-instance v0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;

    invoke-direct {v0, p0}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService;->binder:Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;

    .line 64
    return-void
.end method
