.class Lcom/google/android/videos/cache/PersistentCache$5;
.super Ljava/lang/Object;
.source "PersistentCache.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cache/PersistentCache;->init(Ljava/util/concurrent/Executor;)Lcom/google/android/videos/cache/PersistentCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cache/PersistentCache;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cache/PersistentCache;)V
    .locals 0

    .prologue
    .line 200
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache$5;, "Lcom/google/android/videos/cache/PersistentCache.5;"
    iput-object p1, p0, Lcom/google/android/videos/cache/PersistentCache$5;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 8

    .prologue
    .line 203
    .local p0, "this":Lcom/google/android/videos/cache/PersistentCache$5;, "Lcom/google/android/videos/cache/PersistentCache.5;"
    new-instance v5, Ljava/io/File;

    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache$5;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    # getter for: Lcom/google/android/videos/cache/PersistentCache;->cachePath:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/videos/cache/PersistentCache;->access$100(Lcom/google/android/videos/cache/PersistentCache;)Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iget-object v6, p0, Lcom/google/android/videos/cache/PersistentCache$5;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    # getter for: Lcom/google/android/videos/cache/PersistentCache;->filter:Ljava/io/FilenameFilter;
    invoke-static {v6}, Lcom/google/android/videos/cache/PersistentCache;->access$000(Lcom/google/android/videos/cache/PersistentCache;)Ljava/io/FilenameFilter;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/io/File;->listFiles(Ljava/io/FilenameFilter;)[Ljava/io/File;

    move-result-object v2

    .line 204
    .local v2, "files":[Ljava/io/File;
    if-eqz v2, :cond_0

    .line 205
    move-object v0, v2

    .local v0, "arr$":[Ljava/io/File;
    array-length v4, v0

    .local v4, "len$":I
    const/4 v3, 0x0

    .local v3, "i$":I
    :goto_0
    if-ge v3, v4, :cond_0

    aget-object v1, v0, v3

    .line 206
    .local v1, "file":Ljava/io/File;
    iget-object v5, p0, Lcom/google/android/videos/cache/PersistentCache$5;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    # getter for: Lcom/google/android/videos/cache/PersistentCache;->filenames:Ljava/util/concurrent/ConcurrentHashMap;
    invoke-static {v5}, Lcom/google/android/videos/cache/PersistentCache;->access$200(Lcom/google/android/videos/cache/PersistentCache;)Ljava/util/concurrent/ConcurrentHashMap;

    move-result-object v5

    invoke-virtual {v1}, Ljava/io/File;->getName()Ljava/lang/String;

    move-result-object v6

    const-string v7, ""

    invoke-virtual {v5, v6, v7}, Ljava/util/concurrent/ConcurrentHashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 205
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 209
    .end local v0    # "arr$":[Ljava/io/File;
    .end local v1    # "file":Ljava/io/File;
    .end local v3    # "i$":I
    .end local v4    # "len$":I
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/cache/PersistentCache$5;->this$0:Lcom/google/android/videos/cache/PersistentCache;

    # getter for: Lcom/google/android/videos/cache/PersistentCache;->initialized:Landroid/os/ConditionVariable;
    invoke-static {v5}, Lcom/google/android/videos/cache/PersistentCache;->access$300(Lcom/google/android/videos/cache/PersistentCache;)Landroid/os/ConditionVariable;

    move-result-object v5

    invoke-virtual {v5}, Landroid/os/ConditionVariable;->open()V

    .line 210
    return-void
.end method
