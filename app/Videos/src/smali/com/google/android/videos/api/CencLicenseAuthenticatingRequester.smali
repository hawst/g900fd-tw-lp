.class final Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;
.super Lcom/google/android/videos/async/AuthenticatingRequester;
.source "CencLicenseAuthenticatingRequester.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/async/AuthenticatingRequester",
        "<",
        "Lcom/google/android/videos/api/CencLicenseRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        "[B>;"
    }
.end annotation


# instance fields
.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lorg/apache/http/client/methods/HttpUriRequest;[B>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/AuthenticatingRequester;-><init>(Lcom/google/android/videos/accounts/AccountManagerWrapper;)V

    .line 34
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 35
    return-void
.end method


# virtual methods
.method protected canRetry(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)Z
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 73
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/async/AuthenticatingRequester;->canRetry(Lcom/google/android/videos/async/Request;Ljava/lang/Exception;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 74
    const/4 v0, 0x1

    .line 79
    .end local p2    # "exception":Ljava/lang/Exception;
    :goto_0
    return v0

    .line 76
    .restart local p2    # "exception":Ljava/lang/Exception;
    :cond_0
    instance-of v0, p2, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v0, :cond_1

    .line 77
    check-cast p2, Lcom/google/android/videos/api/CencLicenseException;

    .end local p2    # "exception":Ljava/lang/Exception;
    invoke-virtual {p2}, Lcom/google/android/videos/api/CencLicenseException;->isAuthRetryable()Z

    move-result v0

    goto :goto_0

    .line 79
    .restart local p2    # "exception":Ljava/lang/Exception;
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic canRetry(Lcom/google/android/videos/async/Request;Ljava/lang/Exception;)Z
    .locals 1
    .param p1, "x0"    # Lcom/google/android/videos/async/Request;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/videos/api/CencLicenseRequest;

    .end local p1    # "x0":Lcom/google/android/videos/async/Request;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;->canRetry(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/Exception;)Z

    move-result v0

    return v0
.end method

.method protected makeAuthenticatedRequest(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 10
    .param p1, "request"    # Lcom/google/android/videos/api/CencLicenseRequest;
    .param p2, "authToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/api/CencLicenseRequest;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lorg/apache/http/client/methods/HttpUriRequest;",
            "[B>;)V"
        }
    .end annotation

    .prologue
    .line 40
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lorg/apache/http/client/methods/HttpUriRequest;[B>;"
    new-instance v3, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->url:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    .line 43
    .local v3, "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    const-string v4, "oauth"

    invoke-virtual {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 44
    const-string v4, "offline"

    invoke-virtual {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 45
    const-string v4, "release"

    invoke-virtual {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 46
    const-string v4, "alreadypinned"

    invoke-virtual {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 48
    const-string v4, "oauth"

    invoke-virtual {v3, v4, p2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 49
    iget v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->keyRequestType:I

    const/4 v5, 0x2

    if-ne v4, v5, :cond_2

    .line 50
    const-string v4, "offline"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 51
    iget-boolean v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->isAlreadyPinned:Z

    if-eqz v4, :cond_0

    .line 52
    const-string v4, "alreadypinned"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 59
    :cond_0
    :goto_0
    iget v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->keyRequestType:I

    const/4 v5, 0x1

    if-eq v4, v5, :cond_1

    iget-wide v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->timeSinceStartedMillis:J

    const-wide/16 v6, 0x0

    cmp-long v4, v4, v6

    if-lez v4, :cond_1

    .line 61
    const-wide/16 v4, 0x1

    iget-wide v6, p1, Lcom/google/android/videos/api/CencLicenseRequest;->timeSinceStartedMillis:J

    const-wide/16 v8, 0x3e8

    div-long/2addr v6, v8

    invoke-static {v4, v5, v6, v7}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    .line 62
    .local v0, "elapsedSeconds":J
    const-string v4, "time_since_started"

    invoke-static {v0, v1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 65
    .end local v0    # "elapsedSeconds":J
    :cond_1
    invoke-virtual {v3}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v4

    iget-object v5, p1, Lcom/google/android/videos/api/CencLicenseRequest;->data:[B

    invoke-static {v4, v5}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v2

    .line 67
    .local v2, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    const-string v4, "Authorization"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Bearer "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-interface {v2, v4, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 68
    iget-object v4, p0, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;->target:Lcom/google/android/videos/async/Requester;

    invoke-interface {v4, v2, p3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 69
    return-void

    .line 54
    .end local v2    # "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_2
    iget v4, p1, Lcom/google/android/videos/api/CencLicenseRequest;->keyRequestType:I

    const/4 v5, 0x3

    if-ne v4, v5, :cond_0

    .line 55
    const-string v4, "offline"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 56
    const-string v4, "release"

    const-string v5, "true"

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    goto :goto_0
.end method

.method protected bridge synthetic makeAuthenticatedRequest(Lcom/google/android/videos/async/Request;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/async/Request;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 18
    check-cast p1, Lcom/google/android/videos/api/CencLicenseRequest;

    .end local p1    # "x0":Lcom/google/android/videos/async/Request;
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/api/CencLicenseAuthenticatingRequester;->makeAuthenticatedRequest(Lcom/google/android/videos/api/CencLicenseRequest;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
