.class public Lcom/google/android/videos/search/SuggestionsProvider;
.super Landroid/content/ContentProvider;
.source "SuggestionsProvider.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/content/ContentProvider;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Landroid/net/Uri;",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# static fields
.field private static final SUGGEST_COLUMNS:[Ljava/lang/String;


# instance fields
.field private purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private suggestionsClient:Lcom/google/android/videos/search/JsonSuggestionsClient;

.field private syncBitmapCacheRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private syncBitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field

.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 48
    const/16 v0, 0xb

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "suggest_text_1"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "suggest_text_2"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "suggest_icon_1"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "suggest_icon_2"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "suggest_intent_action"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "suggest_intent_data"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "suggest_intent_extra_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "suggest_shortcut_id"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "suggest_last_access_hint"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "suggest_flags"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/search/SuggestionsProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    return-void
.end method

.method public constructor <init>()V
    .locals 0

    .prologue
    .line 36
    invoke-direct {p0}, Landroid/content/ContentProvider;-><init>()V

    .line 167
    return-void
.end method

.method private static createShowSearchProjection(Z)[Ljava/lang/String;
    .locals 7
    .param p0, "linkToStore"    # Z

    .prologue
    .line 262
    new-instance v3, Landroid/net/Uri$Builder;

    invoke-direct {v3}, Landroid/net/Uri$Builder;-><init>()V

    const-string v4, "content"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "com.google.android.videos"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    const-string v4, "thumbnail/show"

    invoke-virtual {v3, v4}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 268
    .local v1, "baseThumbnailUriString":Ljava/lang/String;
    if-eqz p0, :cond_0

    const-string v2, "http://play.google.com/store/tv/show/?id="

    .line 272
    .local v2, "baseWatchUriString":Ljava/lang/String;
    :goto_0
    if-eqz p0, :cond_1

    const-string v0, "android.intent.action.SEARCH"

    .line 276
    .local v0, "action":Ljava/lang/String;
    :goto_1
    const/4 v3, 0x5

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v5, "shows_title AS suggest_text_1"

    aput-object v5, v3, v4

    const/4 v4, 0x1

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "/\' || "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "shows_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AS "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "suggest_icon_1"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x2

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' AS "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "suggest_intent_action"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x3

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "\'"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "\' || "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "shows_id"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, " AS "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    const-string v6, "suggest_intent_data"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x4

    const-string v5, "account AS suggest_intent_extra_data"

    aput-object v5, v3, v4

    return-object v3

    .line 268
    .end local v0    # "action":Ljava/lang/String;
    .end local v2    # "baseWatchUriString":Ljava/lang/String;
    :cond_0
    const-string v2, "http://www.youtube.com/show/?p="

    goto/16 :goto_0

    .line 272
    .restart local v2    # "baseWatchUriString":Ljava/lang/String;
    :cond_1
    const-string v0, "android.intent.action.VIEW"

    goto/16 :goto_1
.end method

.method private fetchSuggestionBitmaps(Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;)V
    .locals 4
    .param p1, "suggestions"    # Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;

    .prologue
    .line 136
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->moveToNext()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    const/4 v2, 0x3

    invoke-virtual {p1, v2}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 138
    .local v0, "icon1":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 139
    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 140
    .local v1, "iconUri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v2

    const-string v3, "thumbnail/suggest"

    invoke-virtual {v2, v3}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 141
    invoke-virtual {v1}, Landroid/net/Uri;->getLastPathSegment()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 143
    iget-object v2, p0, Lcom/google/android/videos/search/SuggestionsProvider;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v2, v1, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 145
    iget-object v2, p0, Lcom/google/android/videos/search/SuggestionsProvider;->syncBitmapCacheRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v2, v1, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0

    .line 149
    .end local v0    # "icon1":Ljava/lang/String;
    .end local v1    # "iconUri":Landroid/net/Uri;
    :cond_1
    const/4 v2, -0x1

    invoke-virtual {p1, v2}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->moveToPosition(I)Z

    .line 150
    return-void
.end method

.method private declared-synchronized init()V
    .locals 4

    .prologue
    .line 122
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    if-nez v0, :cond_0

    .line 123
    invoke-virtual {p0}, Lcom/google/android/videos/search/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 124
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 125
    new-instance v0, Lcom/google/android/videos/search/JsonSuggestionsClient;

    invoke-virtual {p0}, Lcom/google/android/videos/search/SuggestionsProvider;->getContext()Landroid/content/Context;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getHttpClient()Lorg/apache/http/client/HttpClient;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/search/JsonSuggestionsClient;-><init>(Landroid/content/Context;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/async/Requester;)V

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->suggestionsClient:Lcom/google/android/videos/search/JsonSuggestionsClient;

    .line 127
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 128
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesMemoryCacheRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->syncBitmapCacheRequester:Lcom/google/android/videos/async/Requester;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 131
    :cond_0
    monitor-exit p0

    return-void

    .line 122
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method private requestMoviePurchasesCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/android/videos/async/SyncCallback",
            "<*",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 235
    sget-object v1, Lcom/google/android/videos/search/VideoSearchProvider;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v0, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseRequests;->createMoviesSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v8

    .line 237
    .local v8, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v7

    .line 238
    .local v7, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v0, v8, v7}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 239
    return-object v7
.end method

.method private requestShowPurchasesCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/android/videos/async/SyncCallback",
            "<*",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 244
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/search/SuggestionsProvider;->createShowSearchProjection(Z)[Ljava/lang/String;

    move-result-object v1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const/4 v6, 0x0

    move-object v0, p1

    move-object v2, p2

    move v3, p3

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseRequests;->createShowsSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v8

    .line 247
    .local v8, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v7

    .line 248
    .local v7, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v0, p0, Lcom/google/android/videos/search/SuggestionsProvider;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v0, v8, v7}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 249
    return-object v7
.end method

.method private requestSuggestionsCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "I)",
            "Lcom/google/android/videos/async/SyncCallback",
            "<*",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 254
    new-instance v0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v4

    iget-object v1, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v1

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;)V

    .line 256
    .local v0, "request":Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v6

    .line 257
    .local v6, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Landroid/database/Cursor;>;"
    iget-object v1, p0, Lcom/google/android/videos/search/SuggestionsProvider;->suggestionsClient:Lcom/google/android/videos/search/JsonSuggestionsClient;

    invoke-virtual {v1, v0, v6}, Lcom/google/android/videos/search/JsonSuggestionsClient;->request(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Lcom/google/android/videos/async/Callback;)V

    .line 258
    return-object v6
.end method


# virtual methods
.method public delete(Landroid/net/Uri;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "selection"    # Ljava/lang/String;
    .param p3, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 215
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method

.method public getType(Landroid/net/Uri;)Ljava/lang/String;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 220
    const-string v0, "vnd.android.cursor.dir/vnd.android.search.suggest"

    return-object v0
.end method

.method public insert(Landroid/net/Uri;Landroid/content/ContentValues;)Landroid/net/Uri;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;

    .prologue
    .line 225
    const/4 v0, 0x0

    return-object v0
.end method

.method public onCreate()Z
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x1

    return v0
.end method

.method public onError(Landroid/net/Uri;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 160
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 36
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/search/SuggestionsProvider;->onError(Landroid/net/Uri;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;)V
    .locals 0
    .param p1, "request"    # Landroid/net/Uri;
    .param p2, "response"    # Lcom/google/android/videos/utils/ByteArray;

    .prologue
    .line 155
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 36
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/utils/ByteArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/search/SuggestionsProvider;->onResponse(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;)V

    return-void
.end method

.method public query(Landroid/net/Uri;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 12
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "projection"    # [Ljava/lang/String;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;
    .param p5, "sortOrder"    # Ljava/lang/String;

    .prologue
    .line 75
    invoke-direct {p0}, Lcom/google/android/videos/search/SuggestionsProvider;->init()V

    .line 76
    new-instance v8, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;

    sget-object v10, Lcom/google/android/videos/search/SuggestionsProvider;->SUGGEST_COLUMNS:[Ljava/lang/String;

    invoke-direct {v8, v10}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;-><init>([Ljava/lang/String;)V

    .line 78
    .local v8, "results":Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;
    invoke-virtual {p1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v6

    .line 79
    .local v6, "pathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface {v6}, Ljava/util/List;->size()I

    move-result v9

    .line 80
    .local v9, "size":I
    const/4 v10, 0x2

    if-ge v9, v10, :cond_1

    .line 118
    :cond_0
    :goto_0
    return-object v8

    .line 83
    :cond_1
    add-int/lit8 v10, v9, -0x1

    invoke-interface {v6, v10}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Ljava/lang/String;

    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v11

    invoke-virtual {v10, v11}, Ljava/lang/String;->toLowerCase(Ljava/util/Locale;)Ljava/lang/String;

    move-result-object v7

    .line 84
    .local v7, "query":Ljava/lang/String;
    const-string v10, "^\\s+"

    const-string v11, ""

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 85
    const-string v10, "\\s+"

    const-string v11, " "

    invoke-virtual {v7, v10, v11}, Ljava/lang/String;->replaceAll(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    .line 86
    const-string v10, "limit"

    invoke-virtual {p1, v10}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v10

    const/16 v11, 0xa

    invoke-static {v10, v11}, Lcom/google/android/videos/utils/Util;->parseInt(Ljava/lang/String;I)I

    move-result v5

    .line 89
    .local v5, "limit":I
    iget-object v10, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v10}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v10

    invoke-virtual {v10}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 90
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v10

    if-nez v10, :cond_0

    .line 94
    iget-object v10, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v10}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/android/videos/store/ConfigurationStore;->isPlayCountryKnown(Ljava/lang/String;)Z

    move-result v10

    if-nez v10, :cond_2

    .line 96
    :try_start_0
    iget-object v10, p0, Lcom/google/android/videos/search/SuggestionsProvider;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v10}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v10

    invoke-virtual {v10, v0}, Lcom/google/android/videos/store/ConfigurationStore;->blockingSyncUserConfiguration(Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/Throwable; {:try_start_0 .. :try_end_0} :catch_1

    .line 102
    :cond_2
    :goto_1
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 103
    .local v2, "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<*Landroid/database/Cursor;>;>;"
    invoke-direct {p0, v0, v7, v5}, Lcom/google/android/videos/search/SuggestionsProvider;->requestMoviePurchasesCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 104
    invoke-direct {p0, v0, v7, v5}, Lcom/google/android/videos/search/SuggestionsProvider;->requestShowPurchasesCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 105
    invoke-direct {p0, v0, v7, v5}, Lcom/google/android/videos/search/SuggestionsProvider;->requestSuggestionsCursor(Ljava/lang/String;Ljava/lang/String;I)Lcom/google/android/videos/async/SyncCallback;

    move-result-object v10

    invoke-virtual {v2, v10}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 107
    invoke-virtual {v2}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v4

    .local v4, "i$":Ljava/util/Iterator;
    :goto_2
    invoke-interface {v4}, Ljava/util/Iterator;->hasNext()Z

    move-result v10

    if-eqz v10, :cond_3

    invoke-interface {v4}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/async/SyncCallback;

    .line 111
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<*Landroid/database/Cursor;>;"
    const-wide/16 v10, 0x1388

    :try_start_1
    invoke-virtual {v1, v10, v11}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/database/Cursor;

    invoke-virtual {v8, v10}, Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;->add(Landroid/database/Cursor;)V
    :try_end_1
    .catch Ljava/lang/Exception; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_2

    .line 112
    :catch_0
    move-exception v3

    .line 113
    .local v3, "e":Ljava/lang/Exception;
    const-string v10, "suggestion query failed"

    invoke-static {v10, v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2

    .line 97
    .end local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<*Landroid/database/Cursor;>;"
    .end local v2    # "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<*Landroid/database/Cursor;>;>;"
    .end local v3    # "e":Ljava/lang/Exception;
    .end local v4    # "i$":Ljava/util/Iterator;
    :catch_1
    move-exception v3

    .line 98
    .local v3, "e":Ljava/lang/Throwable;
    const-string v10, "Could not sync user config, will use device country for suggestions"

    invoke-static {v10, v3}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1

    .line 117
    .end local v3    # "e":Ljava/lang/Throwable;
    .restart local v2    # "callbacks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/async/SyncCallback<*Landroid/database/Cursor;>;>;"
    .restart local v4    # "i$":Ljava/util/Iterator;
    :cond_3
    invoke-direct {p0, v8}, Lcom/google/android/videos/search/SuggestionsProvider;->fetchSuggestionBitmaps(Lcom/google/android/videos/search/SuggestionsProvider$SuggestionsCursor;)V

    goto/16 :goto_0
.end method

.method public update(Landroid/net/Uri;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "values"    # Landroid/content/ContentValues;
    .param p3, "selection"    # Ljava/lang/String;
    .param p4, "selectionArgs"    # [Ljava/lang/String;

    .prologue
    .line 230
    new-instance v0, Ljava/lang/UnsupportedOperationException;

    invoke-direct {v0}, Ljava/lang/UnsupportedOperationException;-><init>()V

    throw v0
.end method
