.class Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;
.super Landroid/os/Handler;
.source "LegacyPlayerSubtitleRenderer.java"


# instance fields
.field private currentSubtitleEventTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final player:Lcom/google/android/videos/player/legacy/LegacyPlayer;

.field private started:Z

.field private subtitles:Lcom/google/android/videos/subtitles/Subtitles;

.field private final subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/LegacyPlayer;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "overlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .prologue
    .line 28
    invoke-direct {p0}, Landroid/os/Handler;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->player:Lcom/google/android/videos/player/legacy/LegacyPlayer;

    .line 30
    iput-object p2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 31
    return-void
.end method

.method private processEventInternal(I)V
    .locals 3
    .param p1, "eventIndex"    # I

    .prologue
    .line 99
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 100
    .local v0, "eventTime":I
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/subtitles/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-interface {v1, v2}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->update(Ljava/util/List;)V

    .line 101
    return-void
.end method

.method private scheduleEventInternal(II)V
    .locals 4
    .param p1, "eventIndex"    # I
    .param p2, "currentPosition"    # I

    .prologue
    const/4 v2, 0x0

    .line 104
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge p1, v1, :cond_0

    .line 105
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-interface {v1, p1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    sub-int/2addr v1, p2

    invoke-static {v2, v1}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 106
    .local v0, "delayMillis":I
    invoke-virtual {p0, v2, p1, v2}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {p0, v1, v2, v3}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->sendMessageDelayed(Landroid/os/Message;J)Z

    .line 108
    .end local v0    # "delayMillis":I
    :cond_0
    return-void
.end method

.method private syncSubtitles()V
    .locals 5

    .prologue
    .line 89
    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->player:Lcom/google/android/videos/player/legacy/LegacyPlayer;

    invoke-virtual {v3}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v1

    .line 90
    .local v1, "currentPosition":I
    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v4

    invoke-static {v3, v4}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v2

    .line 91
    .local v2, "searchIndex":I
    if-ltz v2, :cond_1

    move v0, v2

    .line 92
    .local v0, "currentEventIndex":I
    :goto_0
    if-ltz v0, :cond_0

    .line 93
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->processEventInternal(I)V

    .line 95
    :cond_0
    add-int/lit8 v3, v0, 0x1

    invoke-direct {p0, v3, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->scheduleEventInternal(II)V

    .line 96
    return-void

    .line 91
    .end local v0    # "currentEventIndex":I
    :cond_1
    xor-int/lit8 v3, v2, -0x1

    add-int/lit8 v0, v3, -0x1

    goto :goto_0
.end method


# virtual methods
.method public handleMessage(Landroid/os/Message;)V
    .locals 2
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    if-eqz v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->started:Z

    if-nez v0, :cond_1

    .line 86
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    goto :goto_0

    .line 82
    :pswitch_0
    iget v0, p1, Landroid/os/Message;->arg1:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->processEventInternal(I)V

    .line 83
    iget v0, p1, Landroid/os/Message;->arg1:I

    add-int/lit8 v0, v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->player:Lcom/google/android/videos/player/legacy/LegacyPlayer;

    invoke-virtual {v1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v1

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->scheduleEventInternal(II)V

    goto :goto_0

    .line 80
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method

.method public release()V
    .locals 1

    .prologue
    .line 69
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->clear()V

    .line 71
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->hide()V

    .line 72
    return-void
.end method

.method public setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V
    .locals 2
    .param p1, "subtitles"    # Lcom/google/android/videos/subtitles/Subtitles;

    .prologue
    const/4 v1, 0x0

    .line 34
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    if-eq v0, p1, :cond_0

    .line 36
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 37
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->clear()V

    .line 38
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->hide()V

    .line 40
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    .line 41
    if-nez p1, :cond_1

    .line 42
    iput-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    .line 50
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/subtitles/Subtitles;->getEventTimes()Ljava/util/List;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->currentSubtitleEventTimes:Ljava/util/List;

    .line 45
    iget-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->started:Z

    if-eqz v0, :cond_0

    .line 46
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->syncSubtitles()V

    goto :goto_0
.end method

.method public start()V
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->started:Z

    .line 54
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    if-eqz v0, :cond_0

    .line 55
    invoke-direct {p0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->syncSubtitles()V

    .line 57
    :cond_0
    return-void
.end method

.method public stop(Z)V
    .locals 1
    .param p1, "hide"    # Z

    .prologue
    .line 60
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->started:Z

    .line 61
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->removeCallbacksAndMessages(Ljava/lang/Object;)V

    .line 62
    if-eqz p1, :cond_0

    .line 63
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->clear()V

    .line 64
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerSubtitleRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->hide()V

    .line 66
    :cond_0
    return-void
.end method
