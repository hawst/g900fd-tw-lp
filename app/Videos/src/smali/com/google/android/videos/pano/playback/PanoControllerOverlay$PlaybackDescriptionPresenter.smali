.class Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "PanoControllerOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackDescriptionPresenter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V
    .locals 0

    .prologue
    .line 787
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay;
    .param p2, "x1"    # Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;

    .prologue
    .line 787
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;-><init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 791
    iget-object v0, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 792
    .local v0, "textView":Landroid/widget/TextView;
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackDescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    iget-object v1, v1, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->videoTitle:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 793
    return-void
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 797
    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f04007c

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 799
    .local v0, "view":Landroid/view/View;
    new-instance v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-direct {v1, v0}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 804
    return-void
.end method
