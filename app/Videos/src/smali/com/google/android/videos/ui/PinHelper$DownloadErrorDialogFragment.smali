.class public Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadErrorDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 593
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;
    .param p4, "pinningFailedReason"    # I
    .param p5, "downloadSize"    # Ljava/lang/Long;
    .param p6, "drmErrorCode"    # Ljava/lang/Integer;
    .param p7, "isRental"    # Z

    .prologue
    .line 602
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 603
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 604
    const-string v2, "video_id"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 605
    const-string v2, "message"

    invoke-static {p0, p4, p5, p6, p7}, Lcom/google/android/videos/pinning/PinningStatusHelper;->humanizeFailedReason(Landroid/content/Context;ILjava/lang/Long;Ljava/lang/Integer;Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 608
    new-instance v1, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;-><init>()V

    .line 609
    .local v1, "instance":Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 610
    const-string v2, "DownloadErrorDialog"

    invoke-virtual {v1, p1, v2}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 612
    # invokes: Lcom/google/android/videos/ui/PinHelper;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    invoke-static {p0}, Lcom/google/android/videos/ui/PinHelper;->access$400(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p4, p5, p6}, Lcom/google/android/videos/logging/EventLogger;->onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V

    .line 614
    return-void
.end method


# virtual methods
.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 631
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 632
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-nez v0, :cond_0

    .line 637
    :goto_0
    return-void

    .line 635
    :cond_0
    # invokes: Lcom/google/android/videos/ui/PinHelper;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$400(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/logging/EventLogger;->onDismissDownloadErrorDialog()V

    .line 636
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->videoId:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/pinning/PinService;->requestClearError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 618
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 619
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->account:Ljava/lang/String;

    .line 620
    const-string v1, "video_id"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->videoId:Ljava/lang/String;

    .line 622
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$DownloadErrorDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b0124

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const-string v2, "message"

    invoke-virtual {v0, v2}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b0111

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
