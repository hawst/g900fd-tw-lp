.class public abstract Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HomeCursorHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/ui/CursorHelper",
        "<TS;>;",
        "Lcom/google/android/videos/ui/SyncHelper$Listener;"
    }
.end annotation


# instance fields
.field protected final syncHelper:Lcom/google/android/videos/ui/SyncHelper;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/SyncHelper;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/Config;",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/async/Requester",
            "<TS;",
            "Landroid/database/Cursor;",
            ">;",
            "Lcom/google/android/videos/ui/SyncHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 308
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;, "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper<TS;>;"
    .local p5, "store":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TS;Landroid/database/Cursor;>;"
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/CursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;)V

    .line 309
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/SyncHelper;

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 310
    return-void
.end method


# virtual methods
.method public onStart()V
    .locals 1

    .prologue
    .line 314
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;, "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper<TS;>;"
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 315
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 316
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->onAccount(Ljava/lang/String;)V

    .line 317
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 321
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;, "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 322
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 323
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 327
    .local p0, "this":Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;, "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper<TS;>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->onAccount(Ljava/lang/String;)V

    .line 328
    return-void
.end method
