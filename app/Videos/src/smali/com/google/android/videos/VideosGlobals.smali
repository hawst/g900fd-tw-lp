.class public abstract Lcom/google/android/videos/VideosGlobals;
.super Ljava/lang/Object;
.source "VideosGlobals.java"


# static fields
.field private static sIsInitializing:Z

.field private static sVideosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 58
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method public static declared-synchronized from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 69
    const-class v1, Lcom/google/android/videos/VideosGlobals;

    monitor-enter v1

    :try_start_0
    sget-object v0, Lcom/google/android/videos/VideosGlobals;->sVideosGlobals:Lcom/google/android/videos/VideosGlobals;

    if-nez v0, :cond_1

    .line 70
    sget-boolean v0, Lcom/google/android/videos/VideosGlobals;->sIsInitializing:Z

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Ljava/lang/IllegalStateException;

    invoke-direct {v0}, Ljava/lang/IllegalStateException;-><init>()V

    throw v0
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 69
    :catchall_0
    move-exception v0

    monitor-exit v1

    throw v0

    .line 73
    :cond_0
    const/4 v0, 0x1

    :try_start_1
    sput-boolean v0, Lcom/google/android/videos/VideosGlobals;->sIsInitializing:Z

    .line 74
    new-instance v0, Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-virtual {p0}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v2

    invoke-direct {v0, v2}, Lcom/google/android/videos/VideosGlobalsImpl;-><init>(Landroid/content/Context;)V

    sput-object v0, Lcom/google/android/videos/VideosGlobals;->sVideosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 76
    :cond_1
    sget-object v0, Lcom/google/android/videos/VideosGlobals;->sVideosGlobals:Lcom/google/android/videos/VideosGlobals;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    monitor-exit v1

    return-object v0
.end method


# virtual methods
.method public abstract getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;
.end method

.method public abstract getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;
.end method

.method public abstract getApplicationContext()Landroid/content/Context;
.end method

.method public abstract getApplicationVersion()Ljava/lang/String;
.end method

.method public abstract getApplicationVersionCode()I
.end method

.method public abstract getAssetImageUriCreator()Lcom/google/android/videos/ui/AssetImageUriCreator;
.end method

.method public abstract getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;
.end method

.method public abstract getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;
.end method

.method public abstract getConfig()Lcom/google/android/videos/Config;
.end method

.method public abstract getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;
.end method

.method public abstract getContentNotificationManager()Lcom/google/android/videos/ContentNotificationManager;
.end method

.method public abstract getCpuExecutor()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract getDashStreamsSelector()Lcom/google/android/videos/streams/DashStreamsSelector;
.end method

.method public abstract getDataSources()Lcom/google/android/videos/pano/datasource/DataSources;
.end method

.method public abstract getDatabase()Lcom/google/android/videos/store/Database;
.end method

.method public abstract getDownloadNotificationManager()Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end method

.method public abstract getDrmManager()Lcom/google/android/videos/drm/DrmManager;
.end method

.method public abstract getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;
.end method

.method public abstract getEventLogger()Lcom/google/android/videos/logging/EventLogger;
.end method

.method public abstract getExoCacheProvider()Lcom/google/android/videos/pinning/ExoCacheProvider;
.end method

.method public abstract getFreeMoviePromoWelcome()Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;
.end method

.method public abstract getGcmRegistrationManager()Lcom/google/android/videos/gcm/GcmRegistrationManager;
.end method

.method public abstract getHttpClient()Lorg/apache/http/client/HttpClient;
.end method

.method public abstract getItagInfoStore()Lcom/google/android/videos/store/ItagInfoStore;
.end method

.method public abstract getKnowledgeClient()Lcom/google/android/videos/tagging/KnowledgeClient;
.end method

.method public abstract getLegacyStreamsSelector()Lcom/google/android/videos/streams/LegacyStreamsSelector;
.end method

.method public abstract getLocalExecutor()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract getLocaleObservable()Lcom/google/android/repolib/observers/Observable;
.end method

.method public abstract getMediaRouteManager()Lcom/google/android/videos/remote/MediaRouteManager;
.end method

.method public abstract getNetworkExecutor()Ljava/util/concurrent/ExecutorService;
.end method

.method public abstract getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;
.end method

.method public abstract getNowtificationHandler()Lcom/google/android/videos/pano/nowtifications/NowtificationHandler;
.end method

.method public abstract getPinHelper()Lcom/google/android/videos/ui/PinHelper;
.end method

.method public abstract getPlayCommonNetworkStackWrapper()Lcom/google/android/videos/ui/PlayCommonNetworkStackWrapper;
.end method

.method public abstract getPlaybackStatusNotifier()Lcom/google/android/videos/player/PlaybackStatusNotifier;
.end method

.method public abstract getPosterStore()Lcom/google/android/videos/store/PosterStore;
.end method

.method public abstract getPreferences()Landroid/content/SharedPreferences;
.end method

.method public abstract getPremiumErrorMessage()Ljava/lang/CharSequence;
.end method

.method public abstract getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;
.end method

.method public abstract getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;
.end method

.method public abstract getRecommendationsRequestFactory()Lcom/google/android/videos/api/RecommendationsRequest$Factory;
.end method

.method public abstract getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;
.end method

.method public abstract getSignInManager()Lcom/google/android/videos/accounts/SignInManager;
.end method

.method public abstract getStoryboardClient()Lcom/google/android/videos/store/StoryboardClient;
.end method

.method public abstract getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;
.end method

.method public abstract getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;
.end method

.method public abstract getUserAgent()Ljava/lang/String;
.end method

.method public abstract getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;
.end method

.method public abstract getWishlistStore()Lcom/google/android/videos/store/WishlistStore;
.end method

.method public abstract getWishlistStoreSync()Lcom/google/android/videos/store/WishlistStoreSync;
.end method

.method public abstract getYouTubeStatsPingSender()Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;
.end method

.method public abstract hasPremiumError()Z
.end method

.method public abstract legacyDownloadsHaveAppLevelDrm()Z
.end method

.method public abstract onTrimMemory(I)V
.end method

.method public abstract refreshContentRestrictions()Z
.end method

.method public abstract updatePremiumStatus()I
.end method
