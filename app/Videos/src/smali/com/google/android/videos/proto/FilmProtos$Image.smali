.class public final Lcom/google/android/videos/proto/FilmProtos$Image;
.super Lcom/google/protobuf/nano/MessageNano;
.source "FilmProtos.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/proto/FilmProtos;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Image"
.end annotation


# instance fields
.field public cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

.field public data:[B

.field public mimeType:Ljava/lang/String;

.field public referrerUrl:Ljava/lang/String;

.field public url:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 1754
    invoke-direct {p0}, Lcom/google/protobuf/nano/MessageNano;-><init>()V

    .line 1755
    invoke-virtual {p0}, Lcom/google/android/videos/proto/FilmProtos$Image;->clear()Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 1756
    return-void
.end method


# virtual methods
.method public clear()Lcom/google/android/videos/proto/FilmProtos$Image;
    .locals 1

    .prologue
    .line 1759
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    .line 1760
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    .line 1761
    const-string v0, ""

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    .line 1762
    sget-object v0, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    .line 1763
    invoke-static {}, Lcom/google/android/videos/proto/FilmProtos$Rectangle;->emptyArray()[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    .line 1764
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cachedSize:I

    .line 1765
    return-object p0
.end method

.method protected computeSerializedSize()I
    .locals 5

    .prologue
    .line 1851
    invoke-super {p0}, Lcom/google/protobuf/nano/MessageNano;->computeSerializedSize()I

    move-result v2

    .line 1852
    .local v2, "size":I
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 1853
    const/4 v3, 0x1

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1856
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_1

    .line 1857
    const/4 v3, 0x2

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1860
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    const-string v4, ""

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_2

    .line 1861
    const/4 v3, 0x3

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeStringSize(ILjava/lang/String;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1864
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    sget-object v4, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_3

    .line 1865
    const/4 v3, 0x4

    iget-object v4, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeBytesSize(I[B)I

    move-result v3

    add-int/2addr v2, v3

    .line 1868
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-eqz v3, :cond_5

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v3, v3

    if-lez v3, :cond_5

    .line 1869
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v3, v3

    if-ge v1, v3, :cond_5

    .line 1870
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    aget-object v0, v3, v1

    .line 1871
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    if-eqz v0, :cond_4

    .line 1872
    const/4 v3, 0x6

    invoke-static {v3, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->computeMessageSize(ILcom/google/protobuf/nano/MessageNano;)I

    move-result v3

    add-int/2addr v2, v3

    .line 1869
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1877
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    .end local v1    # "i":I
    :cond_5
    return v2
.end method

.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 1770
    if-ne p1, p0, :cond_1

    .line 1805
    :cond_0
    :goto_0
    return v1

    .line 1773
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/proto/FilmProtos$Image;

    if-nez v3, :cond_2

    move v1, v2

    .line 1774
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 1776
    check-cast v0, Lcom/google/android/videos/proto/FilmProtos$Image;

    .line 1777
    .local v0, "other":Lcom/google/android/videos/proto/FilmProtos$Image;
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    if-nez v3, :cond_3

    .line 1778
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    if-eqz v3, :cond_4

    move v1, v2

    .line 1779
    goto :goto_0

    .line 1781
    :cond_3
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_4

    move v1, v2

    .line 1782
    goto :goto_0

    .line 1784
    :cond_4
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    if-nez v3, :cond_5

    .line 1785
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    if-eqz v3, :cond_6

    move v1, v2

    .line 1786
    goto :goto_0

    .line 1788
    :cond_5
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_6

    move v1, v2

    .line 1789
    goto :goto_0

    .line 1791
    :cond_6
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    if-nez v3, :cond_7

    .line 1792
    iget-object v3, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    if-eqz v3, :cond_8

    move v1, v2

    .line 1793
    goto :goto_0

    .line 1795
    :cond_7
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_8

    move v1, v2

    .line 1796
    goto :goto_0

    .line 1798
    :cond_8
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    invoke-static {v3, v4}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v3

    if-nez v3, :cond_9

    move v1, v2

    .line 1799
    goto :goto_0

    .line 1801
    :cond_9
    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    iget-object v4, v0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    invoke-static {v3, v4}, Lcom/google/protobuf/nano/InternalNano;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    move v1, v2

    .line 1803
    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v2, 0x0

    .line 1810
    const/16 v0, 0x11

    .line 1811
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    if-nez v1, :cond_0

    move v1, v2

    :goto_0
    add-int/lit16 v0, v1, 0x20f

    .line 1813
    mul-int/lit8 v3, v0, 0x1f

    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    if-nez v1, :cond_1

    move v1, v2

    :goto_1
    add-int v0, v3, v1

    .line 1815
    mul-int/lit8 v1, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    if-nez v3, :cond_2

    :goto_2
    add-int v0, v1, v2

    .line 1817
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    invoke-static {v2}, Ljava/util/Arrays;->hashCode([B)I

    move-result v2

    add-int v0, v1, v2

    .line 1818
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    invoke-static {v2}, Lcom/google/protobuf/nano/InternalNano;->hashCode([Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 1820
    return v0

    .line 1811
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_0

    .line 1813
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_1

    .line 1815
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2
.end method

.method public mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Image;
    .locals 6
    .param p1, "input"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x0

    .line 1885
    :cond_0
    :goto_0
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    move-result v3

    .line 1886
    .local v3, "tag":I
    sparse-switch v3, :sswitch_data_0

    .line 1890
    invoke-static {p1, v3}, Lcom/google/protobuf/nano/WireFormatNano;->parseUnknownField(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)Z

    move-result v5

    if-nez v5, :cond_0

    .line 1891
    :sswitch_0
    return-object p0

    .line 1896
    :sswitch_1
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    goto :goto_0

    .line 1900
    :sswitch_2
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    goto :goto_0

    .line 1904
    :sswitch_3
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readString()Ljava/lang/String;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    goto :goto_0

    .line 1908
    :sswitch_4
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readBytes()[B

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    goto :goto_0

    .line 1912
    :sswitch_5
    const/16 v5, 0x32

    invoke-static {p1, v5}, Lcom/google/protobuf/nano/WireFormatNano;->getRepeatedFieldArrayLength(Lcom/google/protobuf/nano/CodedInputByteBufferNano;I)I

    move-result v0

    .line 1914
    .local v0, "arrayLength":I
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-nez v5, :cond_2

    move v1, v4

    .line 1915
    .local v1, "i":I
    :goto_1
    add-int v5, v1, v0

    new-array v2, v5, [Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    .line 1917
    .local v2, "newArray":[Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    if-eqz v1, :cond_1

    .line 1918
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    invoke-static {v5, v4, v2, v4, v1}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 1920
    :cond_1
    :goto_2
    array-length v5, v2

    add-int/lit8 v5, v5, -0x1

    if-ge v1, v5, :cond_3

    .line 1921
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Rectangle;-><init>()V

    aput-object v5, v2, v1

    .line 1922
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1923
    invoke-virtual {p1}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readTag()I

    .line 1920
    add-int/lit8 v1, v1, 0x1

    goto :goto_2

    .line 1914
    .end local v1    # "i":I
    .end local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v1, v5

    goto :goto_1

    .line 1926
    .restart local v1    # "i":I
    .restart local v2    # "newArray":[Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    :cond_3
    new-instance v5, Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    invoke-direct {v5}, Lcom/google/android/videos/proto/FilmProtos$Rectangle;-><init>()V

    aput-object v5, v2, v1

    .line 1927
    aget-object v5, v2, v1

    invoke-virtual {p1, v5}, Lcom/google/protobuf/nano/CodedInputByteBufferNano;->readMessage(Lcom/google/protobuf/nano/MessageNano;)V

    .line 1928
    iput-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    goto :goto_0

    .line 1886
    nop

    :sswitch_data_0
    .sparse-switch
        0x0 -> :sswitch_0
        0xa -> :sswitch_1
        0x12 -> :sswitch_2
        0x1a -> :sswitch_3
        0x22 -> :sswitch_4
        0x32 -> :sswitch_5
    .end sparse-switch
.end method

.method public bridge synthetic mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/protobuf/nano/MessageNano;
    .locals 1
    .param p1, "x0"    # Lcom/google/protobuf/nano/CodedInputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1722
    invoke-virtual {p0, p1}, Lcom/google/android/videos/proto/FilmProtos$Image;->mergeFrom(Lcom/google/protobuf/nano/CodedInputByteBufferNano;)Lcom/google/android/videos/proto/FilmProtos$Image;

    move-result-object v0

    return-object v0
.end method

.method public writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V
    .locals 4
    .param p1, "output"    # Lcom/google/protobuf/nano/CodedOutputByteBufferNano;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 1826
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_0

    .line 1827
    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->mimeType:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1829
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 1830
    const/4 v2, 0x2

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->url:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1832
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    const-string v3, ""

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-nez v2, :cond_2

    .line 1833
    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->referrerUrl:Ljava/lang/String;

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeString(ILjava/lang/String;)V

    .line 1835
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    sget-object v3, Lcom/google/protobuf/nano/WireFormatNano;->EMPTY_BYTES:[B

    invoke-static {v2, v3}, Ljava/util/Arrays;->equals([B[B)Z

    move-result v2

    if-nez v2, :cond_3

    .line 1836
    const/4 v2, 0x4

    iget-object v3, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->data:[B

    invoke-virtual {p1, v2, v3}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeBytes(I[B)V

    .line 1838
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    if-eqz v2, :cond_5

    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v2, v2

    if-lez v2, :cond_5

    .line 1839
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    array-length v2, v2

    if-ge v1, v2, :cond_5

    .line 1840
    iget-object v2, p0, Lcom/google/android/videos/proto/FilmProtos$Image;->cropRegion:[Lcom/google/android/videos/proto/FilmProtos$Rectangle;

    aget-object v0, v2, v1

    .line 1841
    .local v0, "element":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    if-eqz v0, :cond_4

    .line 1842
    const/4 v2, 0x6

    invoke-virtual {p1, v2, v0}, Lcom/google/protobuf/nano/CodedOutputByteBufferNano;->writeMessage(ILcom/google/protobuf/nano/MessageNano;)V

    .line 1839
    :cond_4
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 1846
    .end local v0    # "element":Lcom/google/android/videos/proto/FilmProtos$Rectangle;
    .end local v1    # "i":I
    :cond_5
    invoke-super {p0, p1}, Lcom/google/protobuf/nano/MessageNano;->writeTo(Lcom/google/protobuf/nano/CodedOutputByteBufferNano;)V

    .line 1847
    return-void
.end method
