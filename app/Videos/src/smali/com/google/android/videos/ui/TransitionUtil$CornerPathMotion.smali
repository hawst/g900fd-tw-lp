.class public Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;
.super Landroid/transition/PathMotion;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "CornerPathMotion"
.end annotation


# instance fields
.field private moveVerticalFirst:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 296
    invoke-direct {p0}, Landroid/transition/PathMotion;-><init>()V

    return-void
.end method


# virtual methods
.method public getPath(FFFF)Landroid/graphics/Path;
    .locals 7
    .param p1, "startX"    # F
    .param p2, "startY"    # F
    .param p3, "endX"    # F
    .param p4, "endY"    # F

    .prologue
    .line 306
    new-instance v0, Landroid/graphics/Path;

    invoke-direct {v0}, Landroid/graphics/Path;-><init>()V

    .line 308
    .local v0, "path":Landroid/graphics/Path;
    iget-boolean v5, p0, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;->moveVerticalFirst:Z

    if-eqz v5, :cond_0

    move v3, p1

    .local v3, "controlX2":F
    :goto_0
    move v1, v3

    .line 309
    .local v1, "controlX1":F
    iget-boolean v5, p0, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;->moveVerticalFirst:Z

    if-eqz v5, :cond_1

    move v4, p4

    .local v4, "controlY2":F
    :goto_1
    move v2, v4

    .line 310
    .local v2, "controlY1":F
    invoke-virtual {v0, p1, p2}, Landroid/graphics/Path;->moveTo(FF)V

    move v5, p3

    move v6, p4

    .line 311
    invoke-virtual/range {v0 .. v6}, Landroid/graphics/Path;->cubicTo(FFFFFF)V

    .line 312
    return-object v0

    .end local v1    # "controlX1":F
    .end local v2    # "controlY1":F
    .end local v3    # "controlX2":F
    .end local v4    # "controlY2":F
    :cond_0
    move v3, p3

    .line 308
    goto :goto_0

    .restart local v1    # "controlX1":F
    .restart local v3    # "controlX2":F
    :cond_1
    move v4, p2

    .line 309
    goto :goto_1
.end method

.method public setMoveVerticalFirst(Z)V
    .locals 0
    .param p1, "moveVerticalFirst"    # Z

    .prologue
    .line 301
    iput-boolean p1, p0, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;->moveVerticalFirst:Z

    .line 302
    return-void
.end method
