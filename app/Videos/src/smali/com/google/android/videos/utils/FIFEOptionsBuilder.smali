.class public final Lcom/google/android/videos/utils/FIFEOptionsBuilder;
.super Ljava/lang/Object;
.source "FIFEOptionsBuilder.java"


# instance fields
.field final stringBuilder:Ljava/lang/StringBuilder;


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    .line 14
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    new-instance v1, Ljava/lang/StringBuilder;

    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x13

    if-lt v0, v2, :cond_0

    const-string v0, "k-rwu"

    :goto_0
    invoke-direct {v1, v0}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    iput-object v1, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    .line 20
    return-void

    .line 19
    :cond_0
    const-string v0, "k-nw"

    goto :goto_0
.end method


# virtual methods
.method public build()Ljava/lang/String;
    .locals 1

    .prologue
    .line 47
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public setCrop()Lcom/google/android/videos/utils/FIFEOptionsBuilder;
    .locals 2

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "-p-nu"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 43
    return-object p0
.end method

.method public setHeight(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;
    .locals 2
    .param p1, "height"    # I

    .prologue
    .line 29
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "-h"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 31
    return-object p0
.end method

.method public setSize(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;
    .locals 2
    .param p1, "size"    # I

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "-s"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 36
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 37
    return-object p0
.end method

.method public setWidth(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;
    .locals 2
    .param p1, "width"    # I

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    const-string v1, "-w"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 24
    iget-object v0, p0, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->stringBuilder:Ljava/lang/StringBuilder;

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    .line 25
    return-object p0
.end method
