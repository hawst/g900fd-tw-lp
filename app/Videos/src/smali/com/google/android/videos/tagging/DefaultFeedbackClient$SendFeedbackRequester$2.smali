.class Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;->onGotAutoLoginHeaderOrToken(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;ZLcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$report:Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;)V
    .locals 0

    .prologue
    .line 132
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->this$0:Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester;

    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "subRequest"    # Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 140
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 132
    check-cast p1, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->onError(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Void;)V
    .locals 2
    .param p1, "subRequest"    # Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;
    .param p2, "subResponse"    # Ljava/lang/Void;

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->val$report:Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    invoke-interface {v0, v1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 136
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 132
    check-cast p1, Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultFeedbackClient$SendFeedbackRequester$2;->onResponse(Lcom/google/android/videos/tagging/DefaultFeedbackClient$FeedbackSubmitRequest;Ljava/lang/Void;)V

    return-void
.end method
