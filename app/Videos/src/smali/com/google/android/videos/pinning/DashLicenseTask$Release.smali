.class Lcom/google/android/videos/pinning/DashLicenseTask$Release;
.super Lcom/google/android/videos/pinning/DashLicenseTask;
.source "DashLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DashLicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Release"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;)V
    .locals 10
    .param p1, "videosApp"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p3, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p4, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p5, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 196
    const/4 v2, 0x1

    const/4 v8, 0x1

    const/4 v9, 0x0

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/pinning/DashLicenseTask;-><init>(Lcom/google/android/videos/VideosGlobals;ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/logging/EventLogger;ZLcom/google/android/videos/pinning/DashLicenseTask$1;)V

    .line 197
    return-void
.end method

.method private clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V
    .locals 6
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    .prologue
    const-wide/16 v4, 0x0

    .line 219
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v2

    .line 220
    .local v2, "values":Landroid/content/ContentValues;
    if-nez p1, :cond_1

    move-wide v0, v4

    .line 221
    .local v0, "mergedExpirationTimestamp":J
    :goto_0
    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v3, v0, v4

    if-eqz v3, :cond_0

    .line 226
    const-string v3, "expiration_timestamp_seconds"

    const-wide/16 v4, 0x3e8

    div-long v4, v0, v4

    invoke-static {v4, v5}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v4

    invoke-virtual {v2, v3, v4}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 229
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->database:Lcom/google/android/videos/store/Database;

    iget-object v3, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v3, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v4, v3, v2}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 230
    return-void

    .line 220
    .end local v0    # "mergedExpirationTimestamp":J
    :cond_1
    iget-wide v0, p1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->mergedExpirationTimestamp:J

    goto :goto_0
.end method

.method private logError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 233
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->getThrowableToLog(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/logging/EventLogger;->onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 234
    return-void
.end method


# virtual methods
.method protected onCompleted()V
    .locals 2

    .prologue
    .line 240
    iget-object v1, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onLicenseReleaseCompleted(Ljava/lang/String;)V

    .line 241
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 245
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->logError(Ljava/lang/Throwable;)V

    .line 246
    return-void
.end method

.method protected onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;[B)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "response"    # [B

    .prologue
    .line 201
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V

    .line 202
    return-void
.end method

.method protected onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 208
    new-instance v0, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unable to release "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v0
.end method

.method protected onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 214
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V

    .line 215
    invoke-direct {p0, p2}, Lcom/google/android/videos/pinning/DashLicenseTask$Release;->logError(Ljava/lang/Throwable;)V

    .line 216
    return-void
.end method
