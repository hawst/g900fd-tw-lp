.class final Lcom/google/android/videos/store/StoryboardClient$8;
.super Ljava/lang/Object;
.source "StoryboardClient.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->createImageStorer(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/store/StoryboardImageRequest;",
        "Landroid/net/Uri;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 198
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public convertRequest(Lcom/google/android/videos/store/StoryboardImageRequest;)Landroid/net/Uri;
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 201
    iget-object v0, p1, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/Storyboard;->urls:[Ljava/lang/String;

    iget v1, p1, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    aget-object v0, v0, v1

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 198
    check-cast p1, Lcom/google/android/videos/store/StoryboardImageRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/store/StoryboardClient$8;->convertRequest(Lcom/google/android/videos/store/StoryboardImageRequest;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method
