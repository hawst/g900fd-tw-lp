.class final Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MovieDetailsFlowHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/view/View$OnKeyListener;
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "TitlePlayButtonViewHolder"
.end annotation


# instance fields
.field private final playButtonView:Landroid/widget/ImageButton;

.field final synthetic this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V
    .locals 2
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 424
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 425
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 427
    const v0, 0x7f0f00a8

    invoke-static {p2, v0}, Lcom/google/android/videos/utils/ViewUtil;->findViewById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->titleView:Landroid/widget/TextView;

    .line 428
    const v0, 0x7f0f00e1

    invoke-static {p2, v0}, Lcom/google/android/videos/utils/ViewUtil;->findViewById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageButton;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->playButtonView:Landroid/widget/ImageButton;

    .line 430
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->playButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 431
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->playButtonView:Landroid/widget/ImageButton;

    invoke-virtual {v0, p0}, Landroid/widget/ImageButton;->setOnKeyListener(Landroid/view/View$OnKeyListener;)V

    .line 432
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 433
    invoke-direct {p0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->setOutlineProviderV21()V

    .line 435
    :cond_0
    return-void
.end method

.method private setOutlineProviderV21()V
    .locals 2

    .prologue
    .line 439
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->playButtonView:Landroid/widget/ImageButton;

    new-instance v1, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder$1;-><init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;)V

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 449
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 2

    .prologue
    .line 454
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->movieTitle:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$500(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 457
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 461
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;->onPlayClick()V

    .line 462
    return-void
.end method

.method public onKey(Landroid/view/View;ILandroid/view/KeyEvent;)Z
    .locals 1
    .param p1, "v"    # Landroid/view/View;
    .param p2, "keyCode"    # I
    .param p3, "event"    # Landroid/view/KeyEvent;

    .prologue
    .line 466
    const/16 v0, 0x55

    if-eq p2, v0, :cond_0

    const/16 v0, 0x7e

    if-ne p2, v0, :cond_2

    .line 468
    :cond_0
    invoke-virtual {p3}, Landroid/view/KeyEvent;->getAction()I

    move-result v0

    if-nez v0, :cond_1

    invoke-virtual {p3}, Landroid/view/KeyEvent;->getRepeatCount()I

    move-result v0

    if-nez v0, :cond_1

    .line 469
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$TitlePlayButtonViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;->onPlayClick()V

    .line 471
    :cond_1
    const/4 v0, 0x1

    .line 473
    :goto_0
    return v0

    :cond_2
    const/4 v0, 0x0

    goto :goto_0
.end method
