.class public Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "DownloadsOngoing"
.end annotation


# instance fields
.field public final bytesTotal:J

.field public final bytesTotalIsIndeterminate:Z

.field public final bytesTransferred:J

.field public final count:I

.field public final firstAccount:Ljava/lang/String;

.field public final poster:Landroid/graphics/Bitmap;

.field public final singleSeasonId:Ljava/lang/String;

.field public final singleShowId:Ljava/lang/String;

.field public final singleTitle:Ljava/lang/String;

.field public final singleVideoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJZ)V
    .locals 0
    .param p1, "count"    # I
    .param p2, "singleAccount"    # Ljava/lang/String;
    .param p3, "singleVideoId"    # Ljava/lang/String;
    .param p4, "singleSeasonId"    # Ljava/lang/String;
    .param p5, "singleShowId"    # Ljava/lang/String;
    .param p6, "singleTitle"    # Ljava/lang/String;
    .param p7, "poster"    # Landroid/graphics/Bitmap;
    .param p8, "bytesTransferred"    # J
    .param p10, "bytesTotal"    # J
    .param p12, "bytesTotalIsIndeterminate"    # Z

    .prologue
    .line 140
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 141
    iput p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->count:I

    .line 142
    iput-object p2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->firstAccount:Ljava/lang/String;

    .line 143
    iput-object p3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleVideoId:Ljava/lang/String;

    .line 144
    iput-object p4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleSeasonId:Ljava/lang/String;

    .line 145
    iput-object p5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleShowId:Ljava/lang/String;

    .line 146
    iput-object p6, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->singleTitle:Ljava/lang/String;

    .line 147
    iput-object p7, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->poster:Landroid/graphics/Bitmap;

    .line 148
    iput-wide p8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransferred:J

    .line 149
    iput-wide p10, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    .line 150
    iput-boolean p12, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    .line 151
    return-void
.end method

.method static synthetic access$3000(Ljava/util/List;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .locals 1
    .param p0, "x0"    # Ljava/util/List;

    .prologue
    .line 126
    invoke-static {p0}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->create(Ljava/util/List;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    move-result-object v0

    return-object v0
.end method

.method private static create(Ljava/util/List;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .locals 24
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;",
            ">;)",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;"
        }
    .end annotation

    .prologue
    .line 154
    .local p0, "videoData":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;>;"
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_1

    const/4 v2, 0x1

    :goto_0
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 155
    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v16

    check-cast v16, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .line 156
    .local v16, "firstData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    const/4 v3, 0x1

    if-ne v2, v3, :cond_2

    const/16 v20, 0x1

    .line 158
    .local v20, "single":Z
    :goto_1
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->account:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$000(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v4

    .line 159
    .local v4, "firstAccount":Ljava/lang/String;
    if-eqz v20, :cond_3

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->videoId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v5

    .line 160
    .local v5, "singleVideoId":Ljava/lang/String;
    :goto_2
    if-eqz v20, :cond_4

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v6

    .line 161
    .local v6, "singleSeasonId":Ljava/lang/String;
    :goto_3
    if-eqz v20, :cond_5

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v7

    .line 162
    .local v7, "singleShowId":Ljava/lang/String;
    :goto_4
    if-eqz v20, :cond_6

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->title:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$400(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v8

    .line 164
    .local v8, "singleTitle":Ljava/lang/String;
    :goto_5
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v10

    .line 165
    .local v10, "bytesTransferred":J
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v12

    .line 166
    .local v12, "bytesTotal":J
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    const-wide/16 v22, -0x1

    cmp-long v2, v2, v22

    if-nez v2, :cond_7

    const/4 v14, 0x1

    .line 167
    .local v14, "bytesTotalIsIndeterminate":Z
    :goto_6
    const/16 v19, 0x1

    .line 168
    .local v19, "sameShowId":Z
    const/16 v18, 0x1

    .line 169
    .local v18, "sameSeasonId":Z
    const/16 v17, 0x1

    .local v17, "i":I
    :goto_7
    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v17

    if-ge v0, v2, :cond_b

    .line 170
    move-object/from16 v0, p0

    move/from16 v1, v17

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v15

    check-cast v15, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .line 171
    .local v15, "data":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->bytesDownloaded:J
    invoke-static {v15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$1200(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    add-long/2addr v10, v2

    .line 172
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static {v15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    add-long/2addr v12, v2

    .line 173
    if-nez v14, :cond_0

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->downloadSize:J
    invoke-static {v15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$900(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)J

    move-result-wide v2

    const-wide/16 v22, -0x1

    cmp-long v2, v2, v22

    if-nez v2, :cond_8

    :cond_0
    const/4 v14, 0x1

    .line 174
    :goto_8
    if-eqz v19, :cond_9

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static {v15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_9

    const/16 v19, 0x1

    .line 175
    :goto_9
    if-eqz v18, :cond_a

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;
    invoke-static {v15}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_a

    const/16 v18, 0x1

    .line 169
    :goto_a
    add-int/lit8 v17, v17, 0x1

    goto :goto_7

    .line 154
    .end local v4    # "firstAccount":Ljava/lang/String;
    .end local v5    # "singleVideoId":Ljava/lang/String;
    .end local v6    # "singleSeasonId":Ljava/lang/String;
    .end local v7    # "singleShowId":Ljava/lang/String;
    .end local v8    # "singleTitle":Ljava/lang/String;
    .end local v10    # "bytesTransferred":J
    .end local v12    # "bytesTotal":J
    .end local v14    # "bytesTotalIsIndeterminate":Z
    .end local v15    # "data":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .end local v16    # "firstData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .end local v17    # "i":I
    .end local v18    # "sameSeasonId":Z
    .end local v19    # "sameShowId":Z
    .end local v20    # "single":Z
    :cond_1
    const/4 v2, 0x0

    goto/16 :goto_0

    .line 156
    .restart local v16    # "firstData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_2
    const/16 v20, 0x0

    goto/16 :goto_1

    .line 159
    .restart local v4    # "firstAccount":Ljava/lang/String;
    .restart local v20    # "single":Z
    :cond_3
    const/4 v5, 0x0

    goto/16 :goto_2

    .line 160
    .restart local v5    # "singleVideoId":Ljava/lang/String;
    :cond_4
    const/4 v6, 0x0

    goto :goto_3

    .line 161
    .restart local v6    # "singleSeasonId":Ljava/lang/String;
    :cond_5
    const/4 v7, 0x0

    goto :goto_4

    .line 162
    .restart local v7    # "singleShowId":Ljava/lang/String;
    :cond_6
    const/4 v8, 0x0

    goto :goto_5

    .line 166
    .restart local v8    # "singleTitle":Ljava/lang/String;
    .restart local v10    # "bytesTransferred":J
    .restart local v12    # "bytesTotal":J
    :cond_7
    const/4 v14, 0x0

    goto :goto_6

    .line 173
    .restart local v14    # "bytesTotalIsIndeterminate":Z
    .restart local v15    # "data":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    .restart local v17    # "i":I
    .restart local v18    # "sameSeasonId":Z
    .restart local v19    # "sameShowId":Z
    :cond_8
    const/4 v14, 0x0

    goto :goto_8

    .line 174
    :cond_9
    const/16 v19, 0x0

    goto :goto_9

    .line 175
    :cond_a
    const/16 v18, 0x0

    goto :goto_a

    .line 177
    .end local v15    # "data":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_b
    if-nez v20, :cond_c

    if-eqz v19, :cond_f

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v2

    if-eqz v2, :cond_f

    :cond_c
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->poster:Landroid/graphics/Bitmap;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$600(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Landroid/graphics/Bitmap;

    move-result-object v9

    .line 179
    .local v9, "poster":Landroid/graphics/Bitmap;
    :goto_b
    if-eqz v19, :cond_d

    .line 180
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->showId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$300(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v7

    .line 182
    :cond_d
    if-eqz v18, :cond_e

    .line 183
    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->seasonId:Ljava/lang/String;
    invoke-static/range {v16 .. v16}, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;->access$100(Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)Ljava/lang/String;

    move-result-object v6

    .line 186
    :cond_e
    new-instance v2, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    invoke-interface/range {p0 .. p0}, Ljava/util/List;->size()I

    move-result v3

    invoke-direct/range {v2 .. v14}, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;-><init>(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;JJZ)V

    return-object v2

    .line 177
    .end local v9    # "poster":Landroid/graphics/Bitmap;
    :cond_f
    const/4 v9, 0x0

    goto :goto_b
.end method
