.class public interface abstract Lcom/google/android/videos/ui/EidrIdConverterHelper$EidrIdConverterListener;
.super Ljava/lang/Object;
.source "EidrIdConverterHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/EidrIdConverterHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "EidrIdConverterListener"
.end annotation


# virtual methods
.method public abstract onEidrIdConversionFailed()V
.end method

.method public abstract onEidrIdConversionFinished(Ljava/lang/String;)V
.end method

.method public abstract onNetworkRequest()V
.end method
