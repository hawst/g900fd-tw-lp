.class public Lcom/google/android/videos/drm/DrmRequest;
.super Lcom/google/android/videos/async/Request;
.source "DrmRequest.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/drm/DrmRequest$RequestType;
    }
.end annotation


# instance fields
.field final authToken:Ljava/lang/String;

.field public final ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

.field public final playbackId:Ljava/lang/String;

.field public final stream:Lcom/google/android/videos/streams/MediaStream;

.field public final type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p3, "type"    # Lcom/google/android/videos/drm/DrmRequest$RequestType;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "playbackId"    # Ljava/lang/String;
    .param p6, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p7, "authToken"    # Ljava/lang/String;

    .prologue
    .line 54
    if-eqz p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;Z)V

    .line 55
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/MediaStream;

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    .line 56
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    .line 57
    iget-boolean v0, p2, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    if-eqz v0, :cond_1

    .line 58
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/drm/DrmManager$Identifiers;

    iput-object v0, p0, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 62
    :goto_1
    iput-object p3, p0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    .line 63
    iput-object p5, p0, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    .line 64
    iput-object p7, p0, Lcom/google/android/videos/drm/DrmRequest;->authToken:Ljava/lang/String;

    .line 65
    return-void

    .line 54
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 60
    :cond_1
    iput-object p6, p0, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    goto :goto_1
.end method

.method public static createOfflineRequest(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    const/4 v1, 0x0

    .line 74
    iget-boolean v0, p0, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    const-string v2, "stream must be offline"

    invoke-static {v0, v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 75
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v2, p0

    move-object v4, p1

    move-object v5, v1

    move-object v6, p2

    move-object v7, v1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createOfflineRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    const/4 v5, 0x0

    .line 80
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 81
    iget-boolean v0, p1, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    const-string v1, "stream must be offline"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 82
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createOfflineSyncRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    const/4 v5, 0x0

    .line 87
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createPinRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    .line 91
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v6, v5

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createStreamingRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "playbackId"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 69
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->STREAMING:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v5, p3

    move-object v7, v6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createUnpinRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    const/4 v5, 0x0

    .line 96
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    sget-object v3, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    move-object v1, p0

    move-object v2, p1

    move-object v4, p2

    move-object v6, p3

    move-object v7, v5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method


# virtual methods
.method public copyWithToken(Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 8
    .param p1, "authToken"    # Ljava/lang/String;

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest;

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmRequest;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    iget-object v3, p0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    iget-object v4, p0, Lcom/google/android/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/drm/DrmRequest;->playbackId:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/drm/DrmRequest;->ids:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    move-object v7, p1

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/drm/DrmRequest;-><init>(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmRequest$RequestType;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;Ljava/lang/String;)V

    return-object v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 101
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "["

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmRequest;->stream:Lcom/google/android/videos/streams/MediaStream;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmRequest;->type:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
