.class Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;
.super Lcom/google/android/videos/pano/ui/ClickableAction;
.source "MovieDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addDetailsSectionActions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

.field final synthetic val$addedToWishlist:Z


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # Ljava/lang/String;
    .param p5, "x2"    # Ljava/lang/String;

    .prologue
    .line 151
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    iput-boolean p6, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->val$addedToWishlist:Z

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    iget-object v1, v1, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    iget-object v2, v2, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v3, 0x6

    iget-boolean v4, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->val$addedToWishlist:Z

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    # getter for: Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->contentView:Landroid/view/View;
    invoke-static {v6}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->access$000(Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;)Landroid/view/View;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    .line 156
    return-void

    .line 154
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
