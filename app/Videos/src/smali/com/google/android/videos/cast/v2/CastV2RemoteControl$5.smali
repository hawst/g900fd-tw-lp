.class Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;
.super Ljava/lang/Object;
.source "CastV2RemoteControl.java"

# interfaces
.implements Lcom/google/android/gms/common/api/ResultCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onGmsConnected()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/gms/common/api/ResultCallback",
        "<",
        "Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V
    .locals 0

    .prologue
    .line 566
    iput-object p1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onResult(Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;)V
    .locals 2
    .param p1, "result"    # Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;

    .prologue
    .line 569
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sessionRestore result: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    if-nez p1, :cond_1

    const-string v0, "null result"

    :goto_0
    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 570
    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/gms/common/api/Status;->isSuccess()Z

    move-result v0

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v0

    if-eqz v0, :cond_2

    .line 572
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sessionRestore restored select route: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->routeInfo:Landroid/support/v7/media/MediaRouter$RouteInfo;
    invoke-static {v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1500(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Landroid/support/v7/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v7/media/MediaRouter$RouteInfo;->getId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 573
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getSessionId()Ljava/lang/String;

    move-result-object v1

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->onWaitForStatus(Ljava/lang/String;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1600(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Ljava/lang/String;)V

    .line 580
    :cond_0
    :goto_1
    return-void

    .line 569
    :cond_1
    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getStatus()Lcom/google/android/gms/common/api/Status;

    move-result-object v0

    goto :goto_0

    .line 574
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # getter for: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->apiClient:Lcom/google/android/gms/common/api/GoogleApiClient;
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)Lcom/google/android/gms/common/api/GoogleApiClient;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/gms/common/api/GoogleApiClient;->isConnected()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 576
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "sessionRestore different session disconnect:"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-interface {p1}, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;->getSessionId()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 577
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->clearSessionData()V
    invoke-static {v0}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1700(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;)V

    .line 578
    iget-object v0, p0, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->this$0:Lcom/google/android/videos/cast/v2/CastV2RemoteControl;

    const/4 v1, 0x0

    # invokes: Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->disconnect(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl;->access$1800(Lcom/google/android/videos/cast/v2/CastV2RemoteControl;Z)V

    goto :goto_1
.end method

.method public bridge synthetic onResult(Lcom/google/android/gms/common/api/Result;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/gms/common/api/Result;

    .prologue
    .line 566
    check-cast p1, Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;

    .end local p1    # "x0":Lcom/google/android/gms/common/api/Result;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/cast/v2/CastV2RemoteControl$5;->onResult(Lcom/google/android/gms/cast/Cast$ApplicationConnectionResult;)V

    return-void
.end method
