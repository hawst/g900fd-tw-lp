.class Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;
.super Ljava/lang/Object;
.source "DefaultAccountManagerWrapper.java"

# interfaces
.implements Landroid/accounts/AccountManagerCallback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AuthTokenCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/accounts/AccountManagerCallback",
        "<",
        "Landroid/os/Bundle;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

.field final synthetic this$0:Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;Ljava/lang/String;Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;)V
    .locals 0
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "callback"    # Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .prologue
    .line 74
    iput-object p1, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->this$0:Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    iput-object p2, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    .line 76
    iput-object p3, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .line 77
    return-void
.end method


# virtual methods
.method public run(Landroid/accounts/AccountManagerFuture;)V
    .locals 8
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/accounts/AccountManagerFuture",
            "<",
            "Landroid/os/Bundle;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "future":Landroid/accounts/AccountManagerFuture;, "Landroid/accounts/AccountManagerFuture<Landroid/os/Bundle;>;"
    const/4 v7, 0x0

    .line 82
    :try_start_0
    invoke-interface {p1}, Landroid/accounts/AccountManagerFuture;->getResult()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/os/Bundle;

    .line 83
    .local v3, "extras":Landroid/os/Bundle;
    const-string v4, "intent"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->containsKey(Ljava/lang/String;)Z

    move-result v4

    if-eqz v4, :cond_0

    .line 86
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V
    :try_end_0
    .catch Landroid/accounts/OperationCanceledException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_0 .. :try_end_0} :catch_2
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 107
    :goto_0
    iput-object v7, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    .line 109
    .end local v3    # "extras":Landroid/os/Bundle;
    :goto_1
    return-void

    .line 88
    .restart local v3    # "extras":Landroid/os/Bundle;
    :cond_0
    :try_start_1
    const-string v4, "authAccount"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 89
    .local v0, "accountName":Ljava/lang/String;
    const-string v4, "authtoken"

    invoke-virtual {v3, v4}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 90
    .local v1, "authToken":Ljava/lang/String;
    if-eqz v1, :cond_1

    .line 91
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_1
    .catch Landroid/accounts/OperationCanceledException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_1 .. :try_end_1} :catch_2
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 97
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v3    # "extras":Landroid/os/Bundle;
    :catch_0
    move-exception v2

    .line 98
    .local v2, "e":Landroid/accounts/OperationCanceledException;
    :try_start_2
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onNotAuthenticated(Ljava/lang/String;)V
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_0

    .line 107
    iput-object v7, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    .line 93
    .end local v2    # "e":Landroid/accounts/OperationCanceledException;
    .restart local v0    # "accountName":Ljava/lang/String;
    .restart local v1    # "authToken":Ljava/lang/String;
    .restart local v3    # "extras":Landroid/os/Bundle;
    :cond_1
    :try_start_3
    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "got null authToken for "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-static {v0}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 94
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    new-instance v6, Landroid/accounts/AuthenticatorException;

    invoke-direct {v6}, Landroid/accounts/AuthenticatorException;-><init>()V

    invoke-interface {v4, v5, v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_3
    .catch Landroid/accounts/OperationCanceledException; {:try_start_3 .. :try_end_3} :catch_0
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_1
    .catch Landroid/accounts/AuthenticatorException; {:try_start_3 .. :try_end_3} :catch_2
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 99
    .end local v0    # "accountName":Ljava/lang/String;
    .end local v1    # "authToken":Ljava/lang/String;
    .end local v3    # "extras":Landroid/os/Bundle;
    :catch_1
    move-exception v2

    .line 100
    .local v2, "e":Ljava/io/IOException;
    :try_start_4
    const-string v4, "login IOException"

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 101
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_4
    .catchall {:try_start_4 .. :try_end_4} :catchall_0

    .line 107
    iput-object v7, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    .line 102
    .end local v2    # "e":Ljava/io/IOException;
    :catch_2
    move-exception v2

    .line 103
    .local v2, "e":Landroid/accounts/AuthenticatorException;
    :try_start_5
    const-string v4, "login AuthenticatorException"

    invoke-static {v4}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 104
    iget-object v4, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    iget-object v5, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->account:Ljava/lang/String;

    invoke-interface {v4, v5, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;->onAuthenticationError(Ljava/lang/String;Ljava/lang/Exception;)V
    :try_end_5
    .catchall {:try_start_5 .. :try_end_5} :catchall_0

    .line 107
    iput-object v7, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    goto :goto_1

    .end local v2    # "e":Landroid/accounts/AuthenticatorException;
    :catchall_0
    move-exception v4

    iput-object v7, p0, Lcom/google/android/videos/accounts/DefaultAccountManagerWrapper$AuthTokenCallback;->callback:Lcom/google/android/videos/accounts/AccountManagerWrapper$Authenticatee;

    throw v4
.end method
