.class final Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;
.super Lcom/google/android/videos/async/TimestampedCachingRequester;
.source "TimestampedCachingRequester.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/TimestampedCachingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "DefaultTimestampedCachingRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/async/TimestampedCachingRequester",
        "<TR;TR;TE;>;"
    }
.end annotation


# direct methods
.method protected constructor <init>(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)V
    .locals 1
    .param p3, "timeToLive"    # J
    .param p5, "readOnly"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/cache/Cache",
            "<TR;",
            "Lcom/google/android/videos/async/Timestamped",
            "<TE;>;>;",
            "Lcom/google/android/videos/async/Requester",
            "<TR;TE;>;JZ)V"
        }
    .end annotation

    .prologue
    .line 139
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;, "Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester<TR;TE;>;"
    .local p1, "cache":Lcom/google/android/videos/cache/Cache;, "Lcom/google/android/videos/cache/Cache<TR;Lcom/google/android/videos/async/Timestamped<TE;>;>;"
    .local p2, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;TE;>;"
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/async/TimestampedCachingRequester;-><init>(Lcom/google/android/videos/cache/Cache;Lcom/google/android/videos/async/Requester;JZ)V

    .line 140
    return-void
.end method


# virtual methods
.method protected toKey(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)TR;"
        }
    .end annotation

    .prologue
    .line 144
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester;, "Lcom/google/android/videos/async/TimestampedCachingRequester$DefaultTimestampedCachingRequester<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    return-object p1
.end method
