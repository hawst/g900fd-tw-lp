.class public abstract Lcom/google/android/videos/bitmap/BitmapCachingRequester;
.super Ljava/lang/Object;
.source "BitmapCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<TR;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final cache:Lcom/google/android/videos/bitmap/BitmapLruCache;

.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V
    .locals 1
    .param p2, "cache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester<TR;>;"
    .local p1, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TR;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/bitmap/BitmapLruCache;

    iput-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->cache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    .line 23
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 24
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/bitmap/BitmapCachingRequester;)Lcom/google/android/videos/bitmap/BitmapLruCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/bitmap/BitmapCachingRequester;

    .prologue
    .line 16
    iget-object v0, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->cache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    return-object v0
.end method


# virtual methods
.method public request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Lcom/google/android/videos/async/Callback",
            "<TR;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/videos/bitmap/BitmapCachingRequester;, "Lcom/google/android/videos/bitmap/BitmapCachingRequester<TR;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;Landroid/graphics/Bitmap;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->toCacheKey(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 29
    .local v0, "cacheKey":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->cache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/bitmap/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/graphics/Bitmap;

    .line 30
    .local v1, "cached":Landroid/graphics/Bitmap;
    if-eqz v1, :cond_0

    .line 31
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 46
    :goto_0
    return-void

    .line 35
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/bitmap/BitmapCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    new-instance v3, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;

    invoke-direct {v3, p0, v0, p2, p1}, Lcom/google/android/videos/bitmap/BitmapCachingRequester$1;-><init>(Lcom/google/android/videos/bitmap/BitmapCachingRequester;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Ljava/lang/Object;)V

    invoke-interface {v2, p1, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method public abstract toCacheKey(Ljava/lang/Object;)Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;)",
            "Ljava/lang/String;"
        }
    .end annotation
.end method
