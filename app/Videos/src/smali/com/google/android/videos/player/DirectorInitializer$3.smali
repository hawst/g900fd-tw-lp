.class Lcom/google/android/videos/player/DirectorInitializer$3;
.super Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;
.source "DirectorInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->onNoOfflineStream()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;

.field final synthetic val$decoratedSyncPurchasesCallback:Lcom/google/android/videos/async/NewCallback;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/NewCallback;)V
    .locals 1

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    iput-object p2, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->val$decoratedSyncPurchasesCallback:Lcom/google/android/videos/async/NewCallback;

    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/DirectorInitializer$DirectorAuthenticatee;-><init>(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/player/DirectorInitializer$1;)V

    return-void
.end method


# virtual methods
.method public onAuthenticated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "authToken"    # Ljava/lang/String;

    .prologue
    .line 559
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Syncing purchases of "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$100(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 560
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    new-instance v2, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v2}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    # setter for: Lcom/google/android/videos/player/DirectorInitializer;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;
    invoke-static {v1, v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$802(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/async/TaskControl;)Lcom/google/android/videos/async/TaskControl;

    .line 561
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->videoId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/player/DirectorInitializer;->access$100(Lcom/google/android/videos/player/DirectorInitializer;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->syncTaskControl:Lcom/google/android/videos/async/TaskControl;
    invoke-static {v2}, Lcom/google/android/videos/player/DirectorInitializer;->access$800(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/async/TaskControl;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v0

    .line 563
    .local v0, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;>;"
    iget-object v1, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;
    invoke-static {v1}, Lcom/google/android/videos/player/DirectorInitializer;->access$900(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/DirectorInitializer$3;->val$decoratedSyncPurchasesCallback:Lcom/google/android/videos/async/NewCallback;

    invoke-virtual {v1, v0, v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V

    .line 564
    return-void
.end method
