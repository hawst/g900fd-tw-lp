.class public final Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
.super Ljava/lang/Object;
.source "WatchNowRecommendationDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "Item"
.end annotation


# instance fields
.field public final asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field public final isWishlisted:Z


# direct methods
.method public constructor <init>(Lcom/google/wireless/android/video/magma/proto/AssetResource;Z)V
    .locals 1
    .param p1, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p2, "isWishlisted"    # Z

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iput-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 23
    iput-boolean p2, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    .line 24
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 28
    if-ne p0, p1, :cond_1

    .line 35
    :cond_0
    :goto_0
    return v1

    .line 31
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    if-nez v3, :cond_2

    move v1, v2

    .line 32
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 34
    check-cast v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;

    .line 35
    .local v0, "that":Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    iget-boolean v3, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    iget-boolean v4, v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    if-ne v3, v4, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v4, v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v3, v4}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_3
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    invoke-virtual {v0}, Lcom/google/wireless/android/video/magma/proto/AssetResource;->hashCode()I

    move-result v0

    mul-int/lit8 v1, v0, 0x61

    iget-boolean v0, p0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    if-eqz v0, :cond_0

    const/16 v0, 0x4cf

    :goto_0
    add-int/2addr v0, v1

    return v0

    :cond_0
    const/16 v0, 0x4d5

    goto :goto_0
.end method
