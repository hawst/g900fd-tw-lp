.class public Lcom/google/android/videos/store/ItagInfoStore;
.super Ljava/lang/Object;
.source "ItagInfoStore.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/ItagInfoStore$LoadQuery;,
        Lcom/google/android/videos/store/ItagInfoStore$LoadTask;,
        Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final itagsData:Ljava/util/concurrent/ConcurrentMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentMap",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/android/videos/streams/ItagInfo;",
            ">;"
        }
    .end annotation
.end field

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private videoFormatsGetRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;>;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/videos/store/Database;)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;

    .prologue
    .line 47
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 48
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->config:Lcom/google/android/videos/Config;

    .line 49
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/concurrent/Executor;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->localExecutor:Ljava/util/concurrent/Executor;

    .line 50
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->database:Lcom/google/android/videos/store/Database;

    .line 51
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;

    .line 52
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/ItagInfoStore;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/ItagInfoStore;)Ljava/util/concurrent/ConcurrentMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;

    return-object v0
.end method


# virtual methods
.method public getAllowedDownloadQualities()Ljava/util/Set;
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation

    .prologue
    .line 88
    new-instance v1, Ljava/util/HashSet;

    invoke-direct {v1}, Ljava/util/HashSet;-><init>()V

    .line 89
    .local v1, "downloadQualities":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    iget-object v5, p0, Lcom/google/android/videos/store/ItagInfoStore;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->useDashForDownloads()Z

    move-result v5

    if-eqz v5, :cond_1

    iget-object v5, p0, Lcom/google/android/videos/store/ItagInfoStore;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->orderedDashDownloadFormats()Ljava/util/List;

    move-result-object v0

    .line 91
    .local v0, "downloadFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    :goto_0
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v5

    if-ge v2, v5, :cond_3

    .line 92
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/Integer;

    .line 93
    .local v3, "itag":Ljava/lang/Integer;
    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-virtual {p0, v5}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v4

    .line 94
    .local v4, "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    if-nez v4, :cond_2

    .line 95
    const/4 v5, 0x0

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v6

    invoke-virtual {p0, v5, v6}, Lcom/google/android/videos/store/ItagInfoStore;->refreshItagData(ILcom/google/android/videos/async/Callback;)V

    .line 96
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Requested itags refresh due to missing data for itag: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 91
    :cond_0
    :goto_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 89
    .end local v0    # "downloadFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .end local v2    # "i":I
    .end local v3    # "itag":Ljava/lang/Integer;
    .end local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/store/ItagInfoStore;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->orderedDownloadFormats()Ljava/util/List;

    move-result-object v0

    goto :goto_0

    .line 97
    .restart local v0    # "downloadFormats":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    .restart local v2    # "i":I
    .restart local v3    # "itag":Ljava/lang/Integer;
    .restart local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    :cond_2
    iget v5, v4, Lcom/google/android/videos/streams/ItagInfo;->height:I

    iget v6, v4, Lcom/google/android/videos/streams/ItagInfo;->width:I

    mul-int/2addr v5, v6

    if-lez v5, :cond_0

    .line 98
    invoke-virtual {v4}, Lcom/google/android/videos/streams/ItagInfo;->getQuality()I

    move-result v5

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {v1, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    goto :goto_2

    .line 103
    .end local v3    # "itag":Ljava/lang/Integer;
    .end local v4    # "itagInfo":Lcom/google/android/videos/streams/ItagInfo;
    :cond_3
    return-object v1
.end method

.method public getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;
    .locals 2
    .param p1, "itag"    # I

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/concurrent/ConcurrentMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/ItagInfo;

    return-object v0
.end method

.method public init(Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/EmptyRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/Integer;",
            "Lcom/google/wireless/android/video/magma/proto/VideoFormat;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 55
    .local p1, "videoFormatsGetRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/EmptyRequest;Ljava/util/Map<Ljava/lang/Integer;Lcom/google/wireless/android/video/magma/proto/VideoFormat;>;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->videoFormatsGetRequester:Lcom/google/android/videos/async/Requester;

    .line 56
    return-void
.end method

.method public isEmpty()Z
    .locals 1

    .prologue
    .line 79
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;

    invoke-interface {v0}, Ljava/util/concurrent/ConcurrentMap;->isEmpty()Z

    move-result v0

    return v0
.end method

.method public load(ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 75
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;

    invoke-direct {v1, p0, p1, p2}, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;-><init>(Lcom/google/android/videos/store/ItagInfoStore;ILcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 76
    return-void
.end method

.method public refreshItagData(ILcom/google/android/videos/async/Callback;)V
    .locals 3
    .param p1, "requestId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 68
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->videoFormatsGetRequester:Lcom/google/android/videos/async/Requester;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "videoFormatsGetRequester is not initialized"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore;->videoFormatsGetRequester:Lcom/google/android/videos/async/Requester;

    sget-object v1, Lcom/google/android/videos/async/EmptyRequest;->INSTANCE:Lcom/google/android/videos/async/EmptyRequest;

    new-instance v2, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;

    invoke-direct {v2, p0, p1, p2}, Lcom/google/android/videos/store/ItagInfoStore$VideoFormatsCallback;-><init>(Lcom/google/android/videos/store/ItagInfoStore;ILcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 72
    return-void

    .line 68
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
