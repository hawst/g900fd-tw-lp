.class Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;
.super Ljava/lang/Object;
.source "PanoHomeActivity.java"

# interfaces
.implements Lcom/google/android/repolib/common/Action;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/activity/PanoHomeActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Action",
        "<",
        "Lcom/google/android/videos/pano/model/UriItem;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/PanoHomeActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/activity/PanoHomeActivity;)V
    .locals 0

    .prologue
    .line 164
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;->this$0:Lcom/google/android/videos/pano/activity/PanoHomeActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public apply(Lcom/google/android/videos/pano/model/UriItem;)V
    .locals 4
    .param p1, "uriItem"    # Lcom/google/android/videos/pano/model/UriItem;

    .prologue
    .line 168
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;->this$0:Lcom/google/android/videos/pano/activity/PanoHomeActivity;

    .line 169
    .local v0, "activity":Landroid/app/Activity;
    new-instance v1, Landroid/webkit/WebView;

    invoke-direct {v1, v0}, Landroid/webkit/WebView;-><init>(Landroid/content/Context;)V

    .line 170
    .local v1, "webView":Landroid/webkit/WebView;
    new-instance v2, Landroid/webkit/WebViewClient;

    invoke-direct {v2}, Landroid/webkit/WebViewClient;-><init>()V

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->setWebViewClient(Landroid/webkit/WebViewClient;)V

    .line 171
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/UriItem;->getUri()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/webkit/WebView;->loadUrl(Ljava/lang/String;)V

    .line 172
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/UriItem;->getViewTitle()I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/app/AlertDialog$Builder;->setView(Landroid/view/View;)Landroid/app/AlertDialog$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->show()Landroid/app/AlertDialog;

    .line 176
    return-void
.end method

.method public bridge synthetic apply(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 164
    check-cast p1, Lcom/google/android/videos/pano/model/UriItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/PanoHomeActivity$2;->apply(Lcom/google/android/videos/pano/model/UriItem;)V

    return-void
.end method
