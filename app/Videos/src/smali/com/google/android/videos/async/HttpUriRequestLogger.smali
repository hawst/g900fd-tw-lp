.class public Lcom/google/android/videos/async/HttpUriRequestLogger;
.super Ljava/lang/Object;
.source "HttpUriRequestLogger.java"


# direct methods
.method public static toLogString(Lorg/apache/http/client/methods/HttpUriRequest;)Ljava/lang/String;
    .locals 15
    .param p0, "httpRequest"    # Lorg/apache/http/client/methods/HttpUriRequest;

    .prologue
    const/16 v14, 0x10

    .line 25
    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    .line 26
    .local v7, "sb":Ljava/lang/StringBuilder;
    const-string v11, "curl"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getAllHeaders()[Lorg/apache/http/Header;

    move-result-object v0

    .local v0, "arr$":[Lorg/apache/http/Header;
    array-length v6, v0

    .local v6, "len$":I
    const/4 v5, 0x0

    .local v5, "i$":I
    :goto_0
    if-ge v5, v6, :cond_0

    aget-object v4, v0, v5

    .line 28
    .local v4, "header":Lorg/apache/http/Header;
    const-string v11, " -H \'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 29
    invoke-interface {v4}, Lorg/apache/http/Header;->getName()Ljava/lang/String;

    move-result-object v11

    const-string v12, "\'"

    const-string v13, "\\\'"

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 30
    const-string v11, ": "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 31
    invoke-interface {v4}, Lorg/apache/http/Header;->getValue()Ljava/lang/String;

    move-result-object v11

    const-string v12, "\'"

    const-string v13, "\\\'"

    invoke-virtual {v11, v12, v13}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 32
    const-string v11, "\'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 27
    add-int/lit8 v5, v5, 0x1

    goto :goto_0

    .line 34
    .end local v4    # "header":Lorg/apache/http/Header;
    :cond_0
    const-string v11, " \'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 35
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getURI()Ljava/net/URI;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    .line 36
    const-string v11, "\'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 37
    instance-of v11, p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    if-eqz v11, :cond_7

    .line 38
    new-instance v3, Ljava/io/ByteArrayOutputStream;

    invoke-direct {v3}, Ljava/io/ByteArrayOutputStream;-><init>()V

    .line 39
    .local v3, "buffer":Ljava/io/ByteArrayOutputStream;
    check-cast p0, Lorg/apache/http/HttpEntityEnclosingRequest;

    .end local p0    # "httpRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    invoke-interface {p0}, Lorg/apache/http/HttpEntityEnclosingRequest;->getEntity()Lorg/apache/http/HttpEntity;

    move-result-object v2

    .line 40
    .local v2, "body":Lorg/apache/http/HttpEntity;
    if-eqz v2, :cond_1

    .line 42
    :try_start_0
    invoke-interface {v2, v3}, Lorg/apache/http/HttpEntity;->writeTo(Ljava/io/OutputStream;)V
    :try_end_0
    .catch Ljava/io/IOException; {:try_start_0 .. :try_end_0} :catch_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 47
    :try_start_1
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_1
    .catch Ljava/io/IOException; {:try_start_1 .. :try_end_1} :catch_2

    .line 53
    :cond_1
    :goto_1
    new-instance v8, Ljava/lang/StringBuilder;

    invoke-direct {v8}, Ljava/lang/StringBuilder;-><init>()V

    .line 54
    .local v8, "textBody":Ljava/lang/StringBuilder;
    const/4 v9, 0x0

    .line 55
    .local v9, "usePrintf":Z
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->toByteArray()[B

    move-result-object v0

    .local v0, "arr$":[B
    array-length v6, v0

    const/4 v5, 0x0

    :goto_2
    if-ge v5, v6, :cond_4

    aget-byte v1, v0, v5

    .line 56
    .local v1, "b":B
    const/16 v11, 0x20

    if-lt v1, v11, :cond_2

    int-to-char v11, v1

    const/16 v12, 0x27

    if-eq v11, v12, :cond_2

    .line 57
    int-to-char v11, v1

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 55
    :goto_3
    add-int/lit8 v5, v5, 0x1

    goto :goto_2

    .line 43
    .end local v1    # "b":B
    .end local v8    # "textBody":Ljava/lang/StringBuilder;
    .end local v9    # "usePrintf":Z
    .local v0, "arr$":[Lorg/apache/http/Header;
    :catch_0
    move-exception v11

    .line 47
    :try_start_2
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_2
    .catch Ljava/io/IOException; {:try_start_2 .. :try_end_2} :catch_1

    goto :goto_1

    .line 48
    :catch_1
    move-exception v11

    goto :goto_1

    .line 46
    :catchall_0
    move-exception v11

    .line 47
    :try_start_3
    invoke-virtual {v3}, Ljava/io/ByteArrayOutputStream;->close()V
    :try_end_3
    .catch Ljava/io/IOException; {:try_start_3 .. :try_end_3} :catch_3

    .line 50
    :goto_4
    throw v11

    .line 61
    .local v0, "arr$":[B
    .restart local v1    # "b":B
    .restart local v8    # "textBody":Ljava/lang/StringBuilder;
    .restart local v9    # "usePrintf":Z
    :cond_2
    move v10, v1

    .line 62
    .local v10, "value":I
    add-int/lit16 v11, v10, 0x100

    rem-int/lit16 v10, v11, 0x100

    .line 63
    const-string v11, "\\x"

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 64
    if-ge v10, v14, :cond_3

    .line 65
    const/16 v11, 0x30

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    .line 67
    :cond_3
    invoke-static {v10, v14}, Ljava/lang/Integer;->toString(II)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v8, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 68
    const/4 v9, 0x1

    goto :goto_3

    .line 71
    .end local v1    # "b":B
    .end local v10    # "value":I
    :cond_4
    const-string v11, " -d \'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 72
    if-eqz v9, :cond_6

    .line 73
    const-string v11, "$(printf \'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 74
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 75
    const-string v11, "\')"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 79
    :goto_5
    const-string v11, "\'"

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 85
    .end local v0    # "arr$":[B
    .end local v2    # "body":Lorg/apache/http/HttpEntity;
    .end local v3    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "textBody":Ljava/lang/StringBuilder;
    .end local v9    # "usePrintf":Z
    :cond_5
    :goto_6
    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    return-object v11

    .line 77
    .restart local v0    # "arr$":[B
    .restart local v2    # "body":Lorg/apache/http/HttpEntity;
    .restart local v3    # "buffer":Ljava/io/ByteArrayOutputStream;
    .restart local v8    # "textBody":Ljava/lang/StringBuilder;
    .restart local v9    # "usePrintf":Z
    :cond_6
    invoke-virtual {v8}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_5

    .line 80
    .end local v2    # "body":Lorg/apache/http/HttpEntity;
    .end local v3    # "buffer":Ljava/io/ByteArrayOutputStream;
    .end local v8    # "textBody":Ljava/lang/StringBuilder;
    .end local v9    # "usePrintf":Z
    .local v0, "arr$":[Lorg/apache/http/Header;
    .restart local p0    # "httpRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    :cond_7
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v11

    const-string v12, "GET"

    invoke-virtual {v11, v12}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v11

    if-nez v11, :cond_5

    .line 81
    const-string v11, " -X "

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 82
    invoke-interface {p0}, Lorg/apache/http/client/methods/HttpUriRequest;->getMethod()Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    goto :goto_6

    .line 48
    .end local p0    # "httpRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    .restart local v2    # "body":Lorg/apache/http/HttpEntity;
    .restart local v3    # "buffer":Ljava/io/ByteArrayOutputStream;
    :catch_2
    move-exception v11

    goto/16 :goto_1

    :catch_3
    move-exception v12

    goto :goto_4
.end method
