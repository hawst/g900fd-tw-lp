.class public Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
.super Landroid/view/ViewGroup$MarginLayoutParams;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "LayoutParams"
.end annotation


# instance fields
.field public canDismiss:Z

.field public canDrag:Z

.field public column:I

.field public noPadding:Z

.field public removeOnDismiss:Z


# direct methods
.method public constructor <init>(III)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "column"    # I

    .prologue
    const/4 v1, 0x1

    .line 472
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 455
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 456
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 457
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 458
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    .line 473
    iput p3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    .line 474
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 4
    .param p1, "c"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 477
    invoke-direct {p0, p1, p2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 455
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 456
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 457
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 458
    iput-boolean v2, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    .line 479
    sget-object v1, Lcom/google/android/videos/R$styleable;->SuggestionGridLayout_Layout:[I

    invoke-virtual {p1, p2, v1}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 480
    .local v0, "a":Landroid/content/res/TypedArray;
    invoke-virtual {v0, v3, v3}, Landroid/content/res/TypedArray;->getInteger(II)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    .line 481
    const/4 v1, 0x2

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 482
    invoke-virtual {v0, v2, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 483
    const/4 v1, 0x4

    invoke-virtual {v0, v1, v3}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 484
    const/4 v1, 0x3

    invoke-virtual {v0, v1, v2}, Landroid/content/res/TypedArray;->getBoolean(IZ)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    .line 486
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 487
    return-void
.end method

.method public constructor <init>(Landroid/view/ViewGroup$LayoutParams;)V
    .locals 4
    .param p1, "other"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    const/4 v3, 0x1

    .line 461
    const/4 v1, -0x1

    iget v2, p1, Landroid/view/ViewGroup$LayoutParams;->height:I

    invoke-direct {p0, v1, v2}, Landroid/view/ViewGroup$MarginLayoutParams;-><init>(II)V

    .line 455
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 456
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 457
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 458
    iput-boolean v3, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->removeOnDismiss:Z

    .line 462
    instance-of v1, p1, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    if-eqz v1, :cond_0

    move-object v0, p1

    .line 463
    check-cast v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;

    .line 464
    .local v0, "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    iget v1, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    iput v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->column:I

    .line 465
    iget-boolean v1, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDismiss:Z

    .line 466
    iget-boolean v1, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->canDrag:Z

    .line 467
    iget-boolean v1, v0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    iput-boolean v1, p0, Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;->noPadding:Z

    .line 469
    .end local v0    # "lp":Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
    :cond_0
    return-void
.end method
