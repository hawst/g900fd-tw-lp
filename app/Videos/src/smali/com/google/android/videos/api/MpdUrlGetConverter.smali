.class final Lcom/google/android/videos/api/MpdUrlGetConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "MpdUrlGetConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/MpdUrlGetRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

.field private final multiAudio:Z


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;Z)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;
    .param p3, "multiAudio"    # Z

    .prologue
    .line 35
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 36
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "mpdurl"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->baseUri:Ljava/lang/String;

    .line 37
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 38
    iput-boolean p3, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->multiAudio:Z

    .line 39
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 23
    check-cast p1, Lcom/google/android/videos/api/MpdUrlGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/MpdUrlGetConverter;->convertRequest(Lcom/google/android/videos/api/MpdUrlGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/MpdUrlGetRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/MpdUrlGetRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 43
    new-instance v2, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v1, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->baseUri:Ljava/lang/String;

    invoke-direct {v2, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    iget-boolean v1, p1, Lcom/google/android/videos/api/MpdUrlGetRequest;->isEpisode:Z

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromEpisodeId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "youtube_com"

    iget-boolean v3, p1, Lcom/google/android/videos/api/MpdUrlGetRequest;->requireYouTubeUrl:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "lr"

    iget-object v3, p1, Lcom/google/android/videos/api/MpdUrlGetRequest;->locale:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    const-string v2, "ma"

    iget-boolean v3, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->multiAudio:Z

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 51
    .local v0, "uri":Ljava/lang/String;
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1

    .line 43
    .end local v0    # "uri":Ljava/lang/String;
    :cond_0
    iget-object v1, p1, Lcom/google/android/videos/api/MpdUrlGetRequest;->videoId:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 57
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/MpdUrlGetConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/MpdUrlGetConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/MpdUrlGetResponse;

    move-result-object v0

    return-object v0
.end method
