.class Lcom/google/android/videos/player/DirectorInitializer$9;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/DirectorInitializer;->initStreamsCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/MpdGetRequest;",
        "Lcom/google/android/videos/streams/Streams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/DirectorInitializer;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/DirectorInitializer;)V
    .locals 0

    .prologue
    .line 842
    iput-object p1, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 851
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x3

    const/4 v2, 0x0

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(IZ)V

    .line 852
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error loading video streams [request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 853
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onStreamsError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/player/DirectorInitializer;->access$3600(Lcom/google/android/videos/player/DirectorInitializer;Ljava/lang/Exception;)V

    .line 854
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 842
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$9;->onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "streams"    # Lcom/google/android/videos/streams/Streams;

    .prologue
    .line 845
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # getter for: Lcom/google/android/videos/player/DirectorInitializer;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$200(Lcom/google/android/videos/player/DirectorInitializer;)Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-result-object v0

    const/4 v1, 0x3

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(I)V

    .line 846
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # setter for: Lcom/google/android/videos/player/DirectorInitializer;->streams:Lcom/google/android/videos/streams/Streams;
    invoke-static {v0, p2}, Lcom/google/android/videos/player/DirectorInitializer;->access$1902(Lcom/google/android/videos/player/DirectorInitializer;Lcom/google/android/videos/streams/Streams;)Lcom/google/android/videos/streams/Streams;

    .line 847
    iget-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$9;->this$0:Lcom/google/android/videos/player/DirectorInitializer;

    # invokes: Lcom/google/android/videos/player/DirectorInitializer;->onTaskCompleted()V
    invoke-static {v0}, Lcom/google/android/videos/player/DirectorInitializer;->access$3300(Lcom/google/android/videos/player/DirectorInitializer;)V

    .line 848
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 842
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/streams/Streams;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/DirectorInitializer$9;->onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;)V

    return-void
.end method
