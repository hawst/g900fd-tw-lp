.class public Lcom/google/android/videos/ui/WatchNowFlowHelper;
.super Landroid/database/DataSetObserver;
.source "WatchNowFlowHelper.java"

# interfaces
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# instance fields
.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final suggestionsDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

.field private final suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/ItemsWithHeadingFlow",
            "<",
            "Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;",
            "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;",
            ">;"
        }
    .end annotation
.end field

.field private final suggestionsHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

.field private final watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

.field private final watchNowFlow:Lcom/google/android/videos/flow/Flow;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/WatchNowDataSource;Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Landroid/view/View$OnClickListener;Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p3, "welcomeFlow"    # Lcom/google/android/videos/welcome/WelcomeFlow;
    .param p4, "watchNowDataSource"    # Lcom/google/android/videos/adapter/WatchNowDataSource;
    .param p5, "watchNowRecommendationDataSource"    # Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;
    .param p6, "libraryItemClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p7, "suggestionItemClickListener"    # Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;
    .param p8, "seeMoreOnClickListener"    # Landroid/view/View$OnClickListener;
    .param p9, "watchNowBinder"    # Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;
    .param p10, "watchNowRecommendationBinder"    # Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;

    .prologue
    .line 56
    invoke-direct {p0}, Landroid/database/DataSetObserver;-><init>()V

    .line 57
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 58
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/WelcomeFlow;

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 59
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/adapter/WatchNowDataSource;

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    .line 60
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    .line 61
    iput-object p2, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 63
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 64
    invoke-static/range {p7 .. p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 65
    invoke-static/range {p8 .. p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 66
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 67
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 69
    new-instance v8, Lcom/google/android/videos/ui/WatchNowFlowHelper$1;

    const v0, 0x7f04009d

    invoke-direct {v8, p0, v0, p1}, Lcom/google/android/videos/ui/WatchNowFlowHelper$1;-><init>(Lcom/google/android/videos/ui/WatchNowFlowHelper;ILandroid/app/Activity;)V

    .line 79
    .local v8, "spacerFlow":Lcom/google/android/videos/flow/Flow;
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    const/4 v1, 0x0

    const v3, 0x7f04002d

    const/4 v6, -0x1

    move-object v2, p4

    move-object/from16 v4, p9

    move-object v5, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowFlow:Lcom/google/android/videos/flow/Flow;

    .line 87
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v2, 0x7f0b0132

    const/4 v3, 0x0

    const v4, 0x7f0b00d1

    const/4 v6, 0x0

    const/4 v7, 0x0

    move-object v1, p1

    move-object/from16 v5, p8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    .line 89
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v3, 0x7f04002e

    const/4 v6, -0x1

    move-object v2, p5

    move-object/from16 v4, p10

    move-object/from16 v5, p7

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    .line 98
    new-instance v0, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v1, 0x4

    new-array v1, v1, [Lcom/google/android/videos/flow/Flow;

    const/4 v2, 0x0

    aput-object v8, v1, v2

    const/4 v2, 0x1

    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowFlow:Lcom/google/android/videos/flow/Flow;

    aput-object v3, v1, v2

    const/4 v2, 0x2

    aput-object p3, v1, v2

    const/4 v2, 0x3

    iget-object v3, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 104
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->updateVisibilities()V

    .line 105
    return-void
.end method

.method public static getHeaderBottomMargin(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 119
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01cd

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    neg-int v0, v0

    return v0
.end method

.method public static getHeaderHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const/4 v0, 0x2

    const/4 v1, 0x0

    invoke-static {p0, v0, v1}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    invoke-static {p0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v1

    add-int/2addr v0, v1

    return v0
.end method

.method private updateVisibilities()V
    .locals 2

    .prologue
    .line 141
    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setContentInVertical(Z)V

    .line 142
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    iget-object v1, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v1}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDownloadedOnly(Z)V

    .line 143
    return-void

    .line 141
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getFlow()Lcom/google/android/videos/flow/Flow;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->flow:Lcom/google/android/videos/flow/Flow;

    return-object v0
.end method

.method public onChanged()V
    .locals 0

    .prologue
    .line 124
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->updateVisibilities()V

    .line 125
    return-void
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 156
    invoke-direct {p0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->updateVisibilities()V

    .line 157
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->registerObserver(Ljava/lang/Object;)V

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->onDownloadedOnlyChanged(Z)V

    .line 132
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 135
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->watchNowDataSource:Lcom/google/android/videos/adapter/WatchNowDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/WatchNowDataSource;->unregisterObserver(Ljava/lang/Object;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsDataSource:Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource;->unregisterObserver(Ljava/lang/Object;)V

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 138
    return-void
.end method

.method public setSuggestionHeaderDimmed(Z)V
    .locals 1
    .param p1, "dimmed"    # Z

    .prologue
    .line 150
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsHeadingBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;->setDimmed(Z)Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/ui/WatchNowFlowHelper;->suggestionsFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->notifyItemStatesChanged()V

    .line 152
    return-void
.end method
