.class Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;
.super Ljava/lang/Object;
.source "FallbackRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$1:Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;

.field final synthetic val$firstException:Ljava/lang/Exception;


# direct methods
.method constructor <init>(Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;Ljava/lang/Exception;)V
    .locals 0

    .prologue
    .line 68
    .local p0, "this":Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;, "Lcom/google/android/videos/async/FallbackRequester$FallbackCallback.1;"
    iput-object p1, p0, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;

    iput-object p2, p0, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;->val$firstException:Ljava/lang/Exception;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 75
    .local p0, "this":Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;, "Lcom/google/android/videos/async/FallbackRequester$FallbackCallback.1;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;

    # getter for: Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;->originalCallback:Lcom/google/android/videos/async/Callback;
    invoke-static {v0}, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;->access$100(Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;->val$firstException:Ljava/lang/Exception;

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 76
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p0, "this":Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;, "Lcom/google/android/videos/async/FallbackRequester$FallbackCallback.1;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback$1;->this$1:Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;

    # getter for: Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;->originalCallback:Lcom/google/android/videos/async/Callback;
    invoke-static {v0}, Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;->access$100(Lcom/google/android/videos/async/FallbackRequester$FallbackCallback;)Lcom/google/android/videos/async/Callback;

    move-result-object v0

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 72
    return-void
.end method
