.class Lcom/google/android/videos/player/Director$4;
.super Ljava/lang/Object;
.source "Director.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/Director;->initStoryboardCallback()Lcom/google/android/videos/async/Callback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/MpdGetRequest;",
        "Lcom/google/android/videos/streams/Streams;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/Director;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/Director;)V
    .locals 0

    .prologue
    .line 973
    iput-object p1, p0, Lcom/google/android/videos/player/Director$4;->this$0:Lcom/google/android/videos/player/Director;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 980
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error loading video streams [request="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/api/MpdGetRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 981
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 973
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/Director$4;->onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "streams"    # Lcom/google/android/videos/streams/Streams;

    .prologue
    .line 976
    iget-object v0, p0, Lcom/google/android/videos/player/Director$4;->this$0:Lcom/google/android/videos/player/Director;

    iget-object v1, p2, Lcom/google/android/videos/streams/Streams;->storyboards:[Lcom/google/wireless/android/video/magma/proto/Storyboard;

    # invokes: Lcom/google/android/videos/player/Director;->onStoryboards([Lcom/google/wireless/android/video/magma/proto/Storyboard;)V
    invoke-static {v0, v1}, Lcom/google/android/videos/player/Director;->access$300(Lcom/google/android/videos/player/Director;[Lcom/google/wireless/android/video/magma/proto/Storyboard;)V

    .line 977
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 973
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/streams/Streams;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/Director$4;->onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;)V

    return-void
.end method
