.class Lcom/google/android/videos/pinning/InternetConnectionChecker$1;
.super Ljava/lang/Object;
.source "InternetConnectionChecker.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pinning/InternetConnectionChecker;-><init>(Landroid/content/Context;Lcom/google/android/videos/Config;Ljava/util/concurrent/Executor;Lcom/google/android/videos/pinning/InternetConnectionChecker$Listener;Lcom/google/android/videos/utils/NetworkStatus;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pinning/InternetConnectionChecker;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pinning/InternetConnectionChecker;)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker$1;->this$0:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    const/4 v2, 0x0

    .line 73
    iget-object v1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker$1;->this$0:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    # invokes: Lcom/google/android/videos/pinning/InternetConnectionChecker;->blockingCheckInternetConnection()Z
    invoke-static {v1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->access$000(Lcom/google/android/videos/pinning/InternetConnectionChecker;)Z

    move-result v0

    .line 74
    .local v0, "connected":Z
    iget-object v1, p0, Lcom/google/android/videos/pinning/InternetConnectionChecker$1;->this$0:Lcom/google/android/videos/pinning/InternetConnectionChecker;

    # getter for: Lcom/google/android/videos/pinning/InternetConnectionChecker;->handler:Landroid/os/Handler;
    invoke-static {v1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->access$100(Lcom/google/android/videos/pinning/InternetConnectionChecker;)Landroid/os/Handler;

    move-result-object v3

    const/4 v4, 0x3

    if-eqz v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v3, v4, v1, v2}, Landroid/os/Handler;->obtainMessage(III)Landroid/os/Message;

    move-result-object v1

    invoke-virtual {v1}, Landroid/os/Message;->sendToTarget()V

    .line 75
    return-void

    :cond_0
    move v1, v2

    .line 74
    goto :goto_0
.end method
