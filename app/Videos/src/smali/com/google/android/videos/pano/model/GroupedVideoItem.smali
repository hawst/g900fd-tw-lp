.class public final Lcom/google/android/videos/pano/model/GroupedVideoItem;
.super Ljava/lang/Object;
.source "GroupedVideoItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/model/GroupedVideoItem$1;,
        Lcom/google/android/videos/pano/model/GroupedVideoItem$GroupedVideoItemFactory;
    }
.end annotation


# instance fields
.field private final group:Ljava/lang/String;

.field private final videoItem:Lcom/google/android/videos/pano/model/VideoItem;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/model/VideoItem;Ljava/lang/String;)V
    .locals 1
    .param p1, "videoItem"    # Lcom/google/android/videos/pano/model/VideoItem;
    .param p2, "group"    # Ljava/lang/String;

    .prologue
    .line 17
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 18
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/model/VideoItem;

    iput-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem;->videoItem:Lcom/google/android/videos/pano/model/VideoItem;

    .line 19
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem;->group:Ljava/lang/String;

    .line 20
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/model/VideoItem;Ljava/lang/String;Lcom/google/android/videos/pano/model/GroupedVideoItem$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/model/VideoItem;
    .param p2, "x1"    # Ljava/lang/String;
    .param p3, "x2"    # Lcom/google/android/videos/pano/model/GroupedVideoItem$1;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/model/GroupedVideoItem;-><init>(Lcom/google/android/videos/pano/model/VideoItem;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem;->videoItem:Lcom/google/android/videos/pano/model/VideoItem;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/model/VideoItem;->getEntityId()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getGroup()Ljava/lang/String;
    .locals 1

    .prologue
    .line 27
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem;->group:Ljava/lang/String;

    return-object v0
.end method

.method public getVideoItem()Lcom/google/android/videos/pano/model/VideoItem;
    .locals 1

    .prologue
    .line 23
    iget-object v0, p0, Lcom/google/android/videos/pano/model/GroupedVideoItem;->videoItem:Lcom/google/android/videos/pano/model/VideoItem;

    return-object v0
.end method
