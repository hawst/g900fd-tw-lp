.class public Lcom/google/android/videos/ui/DownloadView;
.super Lcom/google/android/play/widget/DownloadStatusView;
.source "DownloadView.java"


# static fields
.field public static final STATE_COMPLETED:I = 0x2

.field public static final STATE_ERROR:I = 0x3

.field public static final STATE_NEW:I = 0x4

.field public static final STATE_NOT_DOWNLOADED:I = 0x0

.field public static final STATE_ONGOING:I = 0x1

.field public static final STATE_PENDING:I = 0x5


# instance fields
.field private contentDescription:Ljava/lang/String;

.field private downloadFraction:F

.field private lastVideoAccount:Ljava/lang/String;

.field private lastVideoId:Ljava/lang/String;

.field private state:I

.field private title:Ljava/lang/String;

.field private unpinAnimator:Landroid/animation/ObjectAnimator;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 47
    invoke-direct {p0, p1}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;)V

    .line 48
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 52
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 55
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/play/widget/DownloadStatusView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 56
    return-void
.end method

.method private animateOrCancelUnpinAnimation(ZFZZ)V
    .locals 1
    .param p1, "pinned"    # Z
    .param p2, "prevDownloadFraction"    # F
    .param p3, "prevStateForSameVideo"    # Z
    .param p4, "failedOrOffline"    # Z

    .prologue
    .line 146
    if-nez p1, :cond_0

    if-nez p3, :cond_3

    .line 148
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/DownloadView;->cancelUnpinAnimation()V

    .line 156
    :cond_1
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_2

    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_2

    .line 157
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    .line 159
    :cond_2
    return-void

    .line 149
    :cond_3
    const/4 v0, 0x0

    cmpl-float v0, p2, v0

    if-lez v0, :cond_1

    if-nez p4, :cond_1

    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_4

    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->isRunning()Z

    move-result v0

    if-nez v0, :cond_1

    .line 152
    :cond_4
    invoke-direct {p0, p2}, Lcom/google/android/videos/ui/DownloadView;->animateUnwind(F)V

    goto :goto_0
.end method

.method private animateUnwind(F)V
    .locals 5
    .param p1, "fraction"    # F

    .prologue
    .line 162
    const/high16 v1, 0x447a0000    # 1000.0f

    mul-float/2addr v1, p1

    float-to-int v0, v1

    .line 163
    .local v0, "millis":I
    const-string v1, "downloadFraction"

    const/4 v2, 0x2

    new-array v2, v2, [F

    const/4 v3, 0x0

    aput p1, v2, v3

    const/4 v3, 0x1

    const/4 v4, 0x0

    aput v4, v2, v3

    invoke-static {p0, v1, v2}, Landroid/animation/ObjectAnimator;->ofFloat(Ljava/lang/Object;Ljava/lang/String;[F)Landroid/animation/ObjectAnimator;

    move-result-object v1

    int-to-long v2, v0

    invoke-virtual {v1, v2, v3}, Landroid/animation/ObjectAnimator;->setDuration(J)Landroid/animation/ObjectAnimator;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    .line 165
    iget-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    new-instance v2, Landroid/view/animation/LinearInterpolator;

    invoke-direct {v2}, Landroid/view/animation/LinearInterpolator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/animation/ObjectAnimator;->setInterpolator(Landroid/animation/TimeInterpolator;)V

    .line 166
    iget-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v1}, Landroid/animation/ObjectAnimator;->start()V

    .line 167
    return-void
.end method

.method private cancelUnpinAnimation()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    invoke-virtual {v0}, Landroid/animation/ObjectAnimator;->cancel()V

    .line 172
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    .line 174
    :cond_0
    return-void
.end method

.method private setState(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 177
    iget v0, p0, Lcom/google/android/videos/ui/DownloadView;->state:I

    if-eq v0, p1, :cond_0

    .line 178
    iput p1, p0, Lcom/google/android/videos/ui/DownloadView;->state:I

    .line 179
    invoke-direct {p0}, Lcom/google/android/videos/ui/DownloadView;->updateContentDescription()V

    .line 181
    :cond_0
    return-void
.end method

.method private updateContentDescription()V
    .locals 5

    .prologue
    .line 194
    const/4 v0, 0x0

    .line 195
    .local v0, "resId":I
    iget v1, p0, Lcom/google/android/videos/ui/DownloadView;->state:I

    packed-switch v1, :pswitch_data_0

    .line 218
    :goto_0
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->title:Ljava/lang/String;

    if-nez v1, :cond_1

    .line 219
    :cond_0
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->contentDescription:Ljava/lang/String;

    .line 223
    :goto_1
    return-void

    .line 197
    :pswitch_0
    const v0, 0x7f0b01b9

    .line 198
    goto :goto_0

    .line 200
    :pswitch_1
    const v0, 0x7f0b01bb

    .line 201
    goto :goto_0

    .line 203
    :pswitch_2
    const v0, 0x7f0b01bc

    .line 204
    goto :goto_0

    .line 206
    :pswitch_3
    const v0, 0x7f0b01bb

    .line 207
    goto :goto_0

    .line 209
    :pswitch_4
    const v0, 0x7f0b01ba

    .line 210
    goto :goto_0

    .line 212
    :pswitch_5
    const v0, 0x7f0b01bd

    .line 213
    goto :goto_0

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/DownloadView;->getContext()Landroid/content/Context;

    move-result-object v1

    const/4 v2, 0x1

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/ui/DownloadView;->title:Ljava/lang/String;

    aput-object v4, v2, v3

    invoke-virtual {v1, v0, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/DownloadView;->contentDescription:Ljava/lang/String;

    goto :goto_1

    .line 195
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method


# virtual methods
.method public getContentDescription()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 227
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->contentDescription:Ljava/lang/String;

    return-object v0
.end method

.method public setTitle(Ljava/lang/String;)V
    .locals 1
    .param p1, "title"    # Ljava/lang/String;

    .prologue
    .line 184
    iget-object v0, p0, Lcom/google/android/videos/ui/DownloadView;->title:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 185
    iput-object p1, p0, Lcom/google/android/videos/ui/DownloadView;->title:Ljava/lang/String;

    .line 186
    invoke-direct {p0}, Lcom/google/android/videos/ui/DownloadView;->updateContentDescription()V

    .line 188
    :cond_0
    return-void
.end method

.method public update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "pinned"    # Z
    .param p4, "pinningStatus"    # Ljava/lang/Integer;
    .param p5, "downloadFraction"    # F

    .prologue
    const/4 v8, 0x3

    const v1, 0x3c23d70a    # 0.01f

    const/4 v7, 0x4

    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 62
    cmpg-float v6, p5, v1

    if-gez v6, :cond_1

    .line 64
    .local v1, "fractionIfError":F
    :goto_0
    const/4 v0, 0x0

    .line 65
    .local v0, "failedOrPending":Z
    if-nez p3, :cond_3

    .line 66
    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {p4, v6}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 67
    invoke-direct {p0, v8}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 68
    invoke-virtual {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 69
    move p5, v1

    .line 70
    const/4 v0, 0x1

    .line 114
    :goto_1
    iget v2, p0, Lcom/google/android/videos/ui/DownloadView;->downloadFraction:F

    .line 115
    .local v2, "prevDownloadFraction":F
    iput p5, p0, Lcom/google/android/videos/ui/DownloadView;->downloadFraction:F

    .line 117
    iget-object v6, p0, Lcom/google/android/videos/ui/DownloadView;->lastVideoAccount:Ljava/lang/String;

    invoke-static {v6, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    iget-object v6, p0, Lcom/google/android/videos/ui/DownloadView;->lastVideoId:Ljava/lang/String;

    invoke-static {v6, p2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v6

    if-eqz v6, :cond_5

    move v3, v4

    .line 119
    .local v3, "prevStateForSameVideo":Z
    :goto_2
    iput-object p1, p0, Lcom/google/android/videos/ui/DownloadView;->lastVideoAccount:Ljava/lang/String;

    .line 120
    iput-object p2, p0, Lcom/google/android/videos/ui/DownloadView;->lastVideoId:Ljava/lang/String;

    .line 123
    invoke-direct {p0, p3, v2, v3, v0}, Lcom/google/android/videos/ui/DownloadView;->animateOrCancelUnpinAnimation(ZFZZ)V

    .line 126
    iget-object v6, p0, Lcom/google/android/videos/ui/DownloadView;->unpinAnimator:Landroid/animation/ObjectAnimator;

    if-nez v6, :cond_0

    .line 127
    invoke-virtual {p0, p5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadFraction(F)V

    .line 130
    if-nez v0, :cond_6

    :goto_3
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/DownloadView;->setOnline(Z)V

    .line 132
    :cond_0
    return-void

    .end local v0    # "failedOrPending":Z
    .end local v1    # "fractionIfError":F
    .end local v2    # "prevDownloadFraction":F
    .end local v3    # "prevStateForSameVideo":Z
    :cond_1
    move v1, p5

    .line 62
    goto :goto_0

    .line 72
    .restart local v0    # "failedOrPending":Z
    .restart local v1    # "fractionIfError":F
    :cond_2
    invoke-direct {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 73
    invoke-virtual {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 74
    const/4 p5, 0x0

    goto :goto_1

    .line 77
    :cond_3
    if-nez p4, :cond_4

    .line 79
    invoke-direct {p0, v7}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 80
    invoke-virtual {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 81
    const/4 p5, 0x0

    goto :goto_1

    .line 83
    :cond_4
    invoke-virtual {p4}, Ljava/lang/Integer;->intValue()I

    move-result v6

    packed-switch v6, :pswitch_data_0

    goto :goto_1

    .line 95
    :pswitch_0
    const/4 v6, 0x5

    invoke-direct {p0, v6}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 96
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 97
    const/4 v0, 0x1

    .line 98
    goto :goto_1

    .line 85
    :pswitch_1
    invoke-direct {p0, v7}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 86
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 87
    const/4 p5, 0x0

    .line 88
    goto :goto_1

    .line 90
    :pswitch_2
    const/4 v6, 0x2

    invoke-direct {p0, v6}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 91
    invoke-virtual {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 92
    const/high16 p5, 0x3f800000    # 1.0f

    .line 93
    goto :goto_1

    .line 100
    :pswitch_3
    invoke-direct {p0, v4}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 101
    invoke-virtual {p0, v4}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    goto :goto_1

    .line 105
    :pswitch_4
    invoke-direct {p0, v8}, Lcom/google/android/videos/ui/DownloadView;->setState(I)V

    .line 106
    invoke-virtual {p0, v5}, Lcom/google/android/videos/ui/DownloadView;->setDownloadRequested(Z)V

    .line 107
    move p5, v1

    .line 108
    const/4 v0, 0x1

    goto :goto_1

    .restart local v2    # "prevDownloadFraction":F
    :cond_5
    move v3, v5

    .line 117
    goto :goto_2

    .restart local v3    # "prevStateForSameVideo":Z
    :cond_6
    move v4, v5

    .line 130
    goto :goto_3

    .line 83
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_3
        :pswitch_2
        :pswitch_4
        :pswitch_1
    .end packed-switch
.end method
