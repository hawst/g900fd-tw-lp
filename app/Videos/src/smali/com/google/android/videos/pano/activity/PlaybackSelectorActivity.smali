.class public Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;
.super Landroid/app/Activity;
.source "PlaybackSelectorActivity.java"

# interfaces
.implements Lcom/google/android/recline/app/DialogFragment$Action$Listener;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method public static createIntent(Landroid/content/Context;ILjava/util/ArrayList;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "titleResourceId"    # I
    .param p3, "selectedIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "I",
            "Ljava/util/ArrayList",
            "<",
            "Ljava/lang/String;",
            ">;I)",
            "Landroid/content/Intent;"
        }
    .end annotation

    .prologue
    .line 28
    .local p2, "optionsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    const-string v1, "options_list"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putStringArrayListExtra(Ljava/lang/String;Ljava/util/ArrayList;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "selected_index"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "title_resource"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static getSelected(Landroid/content/Intent;)I
    .locals 2
    .param p0, "data"    # Landroid/content/Intent;

    .prologue
    .line 35
    if-nez p0, :cond_0

    .line 36
    const/4 v0, -0x1

    .line 38
    :goto_0
    return v0

    :cond_0
    const-string v0, "selected_index"

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v0

    goto :goto_0
.end method


# virtual methods
.method public onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 4
    .param p1, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    .line 74
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getKey()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    .line 75
    .local v1, "selected":I
    new-instance v2, Landroid/content/Intent;

    invoke-direct {v2}, Landroid/content/Intent;-><init>()V

    const-string v3, "selected_index"

    invoke-virtual {v2, v3, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v0

    .line 76
    .local v0, "data":Landroid/content/Intent;
    const/4 v2, -0x1

    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 77
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->finish()V

    .line 78
    return-void
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 9
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 43
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 45
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getIntent()Landroid/content/Intent;

    move-result-object v3

    .line 46
    .local v3, "intent":Landroid/content/Intent;
    invoke-static {v3}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getSelected(Landroid/content/Intent;)I

    move-result v5

    .line 47
    .local v5, "selected":I
    const-string v7, "title_resource"

    const/4 v8, -0x1

    invoke-virtual {v3, v7, v8}, Landroid/content/Intent;->getIntExtra(Ljava/lang/String;I)I

    move-result v7

    invoke-virtual {p0, v7}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 49
    .local v6, "title":Ljava/lang/String;
    const-string v7, "options_list"

    invoke-virtual {v3, v7}, Landroid/content/Intent;->getStringArrayListExtra(Ljava/lang/String;)Ljava/util/ArrayList;

    move-result-object v4

    .line 50
    .local v4, "options":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/String;>;"
    new-instance v0, Ljava/util/ArrayList;

    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    invoke-direct {v0, v7}, Ljava/util/ArrayList;-><init>(I)V

    .line 51
    .local v0, "actions":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/recline/app/DialogFragment$Action;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v7

    if-ge v2, v7, :cond_0

    .line 52
    new-instance v8, Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    invoke-direct {v8}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;-><init>()V

    invoke-virtual {v4, v2}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v7

    check-cast v7, Ljava/lang/String;

    invoke-virtual {v8, v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v7

    invoke-static {v2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v8}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->key(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Action$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/recline/app/DialogFragment$Action$Builder;->build()Lcom/google/android/recline/app/DialogFragment$Action;

    move-result-object v7

    invoke-virtual {v0, v7}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 51
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 55
    :cond_0
    new-instance v7, Lcom/google/android/recline/app/DialogFragment$Builder;

    invoke-direct {v7}, Lcom/google/android/recline/app/DialogFragment$Builder;-><init>()V

    invoke-virtual {v7, v6}, Lcom/google/android/recline/app/DialogFragment$Builder;->title(Ljava/lang/String;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7, v0}, Lcom/google/android/recline/app/DialogFragment$Builder;->actions(Ljava/util/ArrayList;)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7, v5}, Lcom/google/android/recline/app/DialogFragment$Builder;->selectedIndex(I)Lcom/google/android/recline/app/DialogFragment$Builder;

    move-result-object v7

    invoke-virtual {v7}, Lcom/google/android/recline/app/DialogFragment$Builder;->build()Lcom/google/android/recline/app/DialogFragment;

    move-result-object v1

    .line 61
    .local v1, "dialogFragment":Lcom/google/android/recline/app/DialogFragment;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v7

    invoke-static {v7, v1}, Lcom/google/android/recline/app/DialogFragment;->add(Landroid/app/FragmentManager;Lcom/google/android/recline/app/DialogFragment;)V

    .line 62
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 66
    invoke-super {p0}, Landroid/app/Activity;->onPause()V

    .line 68
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->setResult(ILandroid/content/Intent;)V

    .line 69
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/PlaybackSelectorActivity;->finish()V

    .line 70
    return-void
.end method
