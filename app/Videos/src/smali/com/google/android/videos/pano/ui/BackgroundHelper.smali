.class public final Lcom/google/android/videos/pano/ui/BackgroundHelper;
.super Ljava/lang/Object;
.source "BackgroundHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/async/ControllableRequest",
        "<",
        "Landroid/net/Uri;",
        ">;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

.field private backgroundUri:Landroid/net/Uri;

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private released:Z

.field private requestTaskControl:Lcom/google/android/videos/async/TaskControl;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 26
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 28
    invoke-static {p1}, Landroid/support/v17/leanback/app/BackgroundManager;->getInstance(Landroid/app/Activity;)Landroid/support/v17/leanback/app/BackgroundManager;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    .line 29
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {p1}, Landroid/app/Activity;->getWindow()Landroid/view/Window;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager;->attach(Landroid/view/Window;)V

    .line 30
    invoke-static {p1, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->callback:Lcom/google/android/videos/async/Callback;

    .line 31
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 70
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v17/leanback/app/BackgroundManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 71
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "response"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 65
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0, p2}, Landroid/support/v17/leanback/app/BackgroundManager;->setBitmap(Landroid/graphics/Bitmap;)V

    .line 66
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 14
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->released:Z

    .line 35
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 36
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v0}, Landroid/support/v17/leanback/app/BackgroundManager;->release()V

    .line 37
    return-void
.end method

.method public setBackgroundUri(Ljava/lang/String;)V
    .locals 4
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 40
    iget-boolean v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->released:Z

    if-eqz v1, :cond_1

    .line 61
    :cond_0
    :goto_0
    return-void

    .line 44
    :cond_1
    if-nez p1, :cond_2

    .line 45
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v1}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 46
    iput-object v2, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundUri:Landroid/net/Uri;

    .line 47
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundManager:Landroid/support/v17/leanback/app/BackgroundManager;

    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/app/BackgroundManager;->setBitmap(Landroid/graphics/Bitmap;)V

    goto :goto_0

    .line 51
    :cond_2
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 52
    .local v0, "newBackgroundUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundUri:Landroid/net/Uri;

    invoke-virtual {v0, v1}, Landroid/net/Uri;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 55
    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundUri:Landroid/net/Uri;

    .line 57
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v1}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 58
    new-instance v1, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v1}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    .line 59
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->backgroundUri:Landroid/net/Uri;

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v2, v3}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BackgroundHelper;->callback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method
