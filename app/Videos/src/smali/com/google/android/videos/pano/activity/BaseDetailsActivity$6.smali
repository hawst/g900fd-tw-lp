.class Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;
.super Landroid/os/AsyncTask;
.source "BaseDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapter(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/os/AsyncTask",
        "<",
        "Ljava/lang/Void;",
        "Ljava/lang/Void;",
        "Landroid/util/Pair",
        "<",
        "Ljava/util/ArrayList",
        "<",
        "Landroid/support/v17/leanback/widget/Row;",
        ">;",
        "Ljava/lang/String;",
        ">;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

.field final synthetic val$updateType:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;I)V
    .locals 0

    .prologue
    .line 401
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iput p2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    invoke-direct {p0}, Landroid/os/AsyncTask;-><init>()V

    return-void
.end method


# virtual methods
.method protected varargs doInBackground([Ljava/lang/Void;)Landroid/util/Pair;
    .locals 8
    .param p1, "params"    # [Ljava/lang/Void;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Ljava/lang/Void;",
            ")",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v7, 0x2

    const/4 v5, 0x1

    .line 405
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    invoke-static {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->onAdapterUpdateStart()V

    .line 407
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 409
    .local v2, "rows":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Row;>;"
    const/4 v1, 0x0

    .line 410
    .local v1, "detailsOverviewRow":Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    iget v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    if-eqz v4, :cond_0

    iget v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    if-ne v4, v7, :cond_1

    .line 411
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    invoke-static {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v6

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v4, v4, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-nez v4, :cond_6

    move v4, v5

    :goto_0
    invoke-virtual {v6, v4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getDetailsOverviewRow(Z)Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    move-result-object v1

    .line 413
    :cond_1
    if-eqz v1, :cond_2

    .line 414
    invoke-virtual {v2, v1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 417
    :cond_2
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    invoke-static {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    .line 418
    .local v0, "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v0, :cond_3

    .line 419
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v6, v6, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v4, v6}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 422
    :cond_3
    iget v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    if-eq v4, v5, :cond_4

    iget v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    if-ne v4, v7, :cond_5

    .line 423
    :cond_4
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v5, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    invoke-static {v5}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v5

    invoke-virtual {v4, v2, v5}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->addRows(Ljava/util/ArrayList;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;)V

    .line 425
    :cond_5
    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    invoke-static {v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getWallpaperUri()Ljava/lang/String;

    move-result-object v3

    .line 426
    .local v3, "screenshotUri":Ljava/lang/String;
    new-instance v4, Landroid/util/Pair;

    invoke-direct {v4, v2, v3}, Landroid/util/Pair;-><init>(Ljava/lang/Object;Ljava/lang/Object;)V

    return-object v4

    .line 411
    .end local v0    # "assetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v3    # "screenshotUri":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x0

    goto :goto_0
.end method

.method protected bridge synthetic doInBackground([Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # [Ljava/lang/Object;

    .prologue
    .line 401
    check-cast p1, [Ljava/lang/Void;

    .end local p1    # "x0":[Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->doInBackground([Ljava/lang/Void;)Landroid/util/Pair;

    move-result-object v0

    return-object v0
.end method

.method protected onPostExecute(Landroid/util/Pair;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "result":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/util/ArrayList<Landroid/support/v17/leanback/widget/Row;>;Ljava/lang/String;>;"
    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 431
    iget v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->val$updateType:I

    packed-switch v0, :pswitch_data_0

    .line 465
    :cond_0
    :goto_0
    return-void

    .line 433
    :pswitch_0
    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    invoke-virtual {v0}, Ljava/util/ArrayList;->isEmpty()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 438
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finish()V

    goto :goto_0

    .line 441
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v3, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    .line 442
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setBackground(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$600(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Ljava/lang/String;)V

    .line 443
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-nez v0, :cond_0

    .line 444
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # invokes: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapter(I)V
    invoke-static {v0, v2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$700(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;I)V

    goto :goto_0

    .line 448
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->isTransitioning:Z

    if-eqz v0, :cond_2

    .line 449
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->rowAdapterContents:Ljava/util/ArrayList;

    .line 453
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # setter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishedInitialAdapterLoad:Z
    invoke-static {v0, v2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$802(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Z)Z

    goto :goto_0

    .line 451
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v2, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    goto :goto_1

    .line 456
    :pswitch_2
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    # invokes: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setBackground(Ljava/lang/String;)V
    invoke-static {v1, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$600(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Ljava/lang/String;)V

    .line 457
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v0, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->isTransitioning:Z

    if-eqz v0, :cond_3

    .line 458
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/ArrayList;

    iput-object v0, v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->fullAdapterContents:Ljava/util/ArrayList;

    goto :goto_0

    .line 460
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v0

    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->clear()V

    .line 461
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    invoke-static {v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    move-result-object v1

    iget-object v0, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v0, Ljava/util/Collection;

    invoke-virtual {v1, v3, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->addAll(ILjava/util/Collection;)V

    goto/16 :goto_0

    .line 431
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method protected bridge synthetic onPostExecute(Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 401
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->onPostExecute(Landroid/util/Pair;)V

    return-void
.end method
