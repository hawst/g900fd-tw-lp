.class public final Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;
.super Ljava/lang/Object;
.source "BytesToBitmapResponseConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/ResponseConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/ResponseConverter",
        "<",
        "Lcom/google/android/videos/utils/ByteArray;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final desiredWidth:I

.field private final purgeable:Z


# direct methods
.method public constructor <init>(IZ)V
    .locals 2
    .param p1, "desiredWidth"    # I
    .param p2, "purgeable"    # Z

    .prologue
    .line 39
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 40
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "desiredWidth must be > 0"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 41
    iput p1, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->desiredWidth:I

    .line 42
    iput-boolean p2, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->purgeable:Z

    .line 43
    return-void

    .line 40
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public constructor <init>(Z)V
    .locals 1
    .param p1, "purgeable"    # Z

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->desiredWidth:I

    .line 28
    iput-boolean p1, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->purgeable:Z

    .line 29
    return-void
.end method

.method private static calculateScale(II)I
    .locals 1
    .param p0, "desiredWidth"    # I
    .param p1, "actualWidth"    # I

    .prologue
    .line 78
    const/4 v0, 0x1

    .line 79
    .local v0, "scale":I
    :goto_0
    shr-int/lit8 p1, p1, 0x1

    if-lt p1, p0, :cond_0

    .line 80
    shl-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 82
    :cond_0
    return v0
.end method

.method private decode([BII)Landroid/graphics/Bitmap;
    .locals 7
    .param p1, "buffer"    # [B
    .param p2, "offset"    # I
    .param p3, "length"    # I

    .prologue
    const/4 v3, 0x0

    const/4 v6, 0x0

    const/4 v2, 0x1

    .line 56
    new-instance v0, Landroid/graphics/BitmapFactory$Options;

    invoke-direct {v0}, Landroid/graphics/BitmapFactory$Options;-><init>()V

    .line 58
    .local v0, "options":Landroid/graphics/BitmapFactory$Options;
    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 59
    invoke-static {p1, p2, p3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    .line 60
    iget v4, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    if-gez v4, :cond_0

    move-object v2, v3

    .line 73
    :goto_0
    return-object v2

    .line 65
    :cond_0
    iget v4, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->desiredWidth:I

    if-nez v4, :cond_1

    move v1, v2

    .line 66
    .local v1, "scale":I
    :goto_1
    iget-boolean v4, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->purgeable:Z

    if-nez v4, :cond_2

    if-ne v1, v2, :cond_2

    .line 67
    invoke-static {p1, p2, p3, v3}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0

    .line 65
    .end local v1    # "scale":I
    :cond_1
    iget v4, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->desiredWidth:I

    iget v5, v0, Landroid/graphics/BitmapFactory$Options;->outWidth:I

    invoke-static {v4, v5}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->calculateScale(II)I

    move-result v1

    goto :goto_1

    .line 69
    .restart local v1    # "scale":I
    :cond_2
    iput v1, v0, Landroid/graphics/BitmapFactory$Options;->inSampleSize:I

    .line 70
    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inJustDecodeBounds:Z

    .line 71
    iget-boolean v2, p0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->purgeable:Z

    iput-boolean v2, v0, Landroid/graphics/BitmapFactory$Options;->inPurgeable:Z

    .line 72
    iput-boolean v6, v0, Landroid/graphics/BitmapFactory$Options;->inInputShareable:Z

    .line 73
    invoke-static {p1, p2, p3, v0}, Landroid/graphics/BitmapFactory;->decodeByteArray([BIILandroid/graphics/BitmapFactory$Options;)Landroid/graphics/Bitmap;

    move-result-object v2

    goto :goto_0
.end method


# virtual methods
.method public convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;
    .locals 4
    .param p1, "response"    # Lcom/google/android/videos/utils/ByteArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p1, Lcom/google/android/videos/utils/ByteArray;->data:[B

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v3

    invoke-direct {p0, v1, v2, v3}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->decode([BII)Landroid/graphics/Bitmap;

    move-result-object v0

    .line 48
    .local v0, "bitmap":Landroid/graphics/Bitmap;
    if-nez v0, :cond_0

    .line 49
    new-instance v1, Lcom/google/android/videos/converter/ConverterException;

    const-string v2, "failed to decode bitmap"

    invoke-direct {v1, v2}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 51
    :cond_0
    return-object v0
.end method

.method public bridge synthetic convertResponse(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    check-cast p1, Lcom/google/android/videos/utils/ByteArray;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;->convertResponse(Lcom/google/android/videos/utils/ByteArray;)Landroid/graphics/Bitmap;

    move-result-object v0

    return-object v0
.end method
