.class Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;
.super Ljava/lang/Object;
.source "ItagStreamRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/api/ItagStreamRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MpdCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/MpdGetRequest;",
        "Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/api/ItagStreamRequester;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p2, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    iput-object p1, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 124
    iput-object p2, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 125
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/ItagStreamRequester$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/api/ItagStreamRequester;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;
    .param p3, "x2"    # Lcom/google/android/videos/api/ItagStreamRequester$1;

    .prologue
    .line 119
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;-><init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 143
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 144
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 119
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->onError(Lcom/google/android/videos/api/MpdGetRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)V
    .locals 12
    .param p1, "request"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    .prologue
    .line 129
    iget-object v0, p2, Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;->resource:Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;

    iget-object v6, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription;->representations:[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;

    .local v6, "arr$":[Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_1

    aget-object v9, v6, v7

    .line 130
    .local v9, "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    # getter for: Lcom/google/android/videos/api/ItagStreamRequester;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;
    invoke-static {v0}, Lcom/google/android/videos/api/ItagStreamRequester;->access$100(Lcom/google/android/videos/api/ItagStreamRequester;)Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v0

    iget v1, v9, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/ItagInfoStore;->getItagInfo(I)Lcom/google/android/videos/streams/ItagInfo;

    move-result-object v0

    if-nez v0, :cond_0

    .line 131
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Requested itag data refresh due to missing itag: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, v9, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;->format:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    # getter for: Lcom/google/android/videos/api/ItagStreamRequester;->itagInfoStore:Lcom/google/android/videos/store/ItagInfoStore;
    invoke-static {v0}, Lcom/google/android/videos/api/ItagStreamRequester;->access$100(Lcom/google/android/videos/api/ItagStreamRequester;)Lcom/google/android/videos/store/ItagInfoStore;

    move-result-object v10

    const/4 v11, 0x0

    new-instance v0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;

    iget-object v1, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    iget-object v2, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    const/4 v5, 0x0

    move-object v3, p1

    move-object v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;-><init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;Lcom/google/android/videos/api/ItagStreamRequester$1;)V

    invoke-virtual {v10, v11, v0}, Lcom/google/android/videos/store/ItagInfoStore;->refreshItagData(ILcom/google/android/videos/async/Callback;)V

    .line 139
    .end local v9    # "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :goto_1
    return-void

    .line 129
    .restart local v9    # "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :cond_0
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 138
    .end local v9    # "r":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Representation;
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    # invokes: Lcom/google/android/videos/api/ItagStreamRequester;->convertToStreams(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;
    invoke-static {v1, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester;->access$300(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 119
    check-cast p1, Lcom/google/android/videos/api/MpdGetRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester$MpdCallback;->onResponse(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)V

    return-void
.end method
