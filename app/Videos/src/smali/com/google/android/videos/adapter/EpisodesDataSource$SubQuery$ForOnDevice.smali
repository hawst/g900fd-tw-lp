.class public abstract Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForOnDevice;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "ForOnDevice"
.end annotation


# direct methods
.method public static createRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "seasonId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 825
    const/4 v0, 0x3

    new-array v6, v0, [Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v0

    aput-object v0, v6, v1

    const/4 v0, 0x1

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    const/4 v0, 0x2

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v6, v0

    .line 830
    .local v6, "whereArgs":[Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "assets, videos ON assets_type = 20 AND assets_id = video_id, seasons ON episode_season_id = season_id, purchased_assets ON asset_type = 20 AND asset_id = assets_id AND purchase_status = 2 AND NOT (hidden IN (1, 3)), user_assets ON user_assets_account = account AND user_assets_type = 20 AND user_assets_id = assets_id"

    sget-object v3, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "account = ? AND (pinned IS NOT NULL AND pinned > 0) AND season_id = ?"

    const/4 v7, 0x0

    const-string v8, "episode_seqno"

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method
