.class public interface abstract Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;
.super Ljava/lang/Object;
.source "LegacyPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/legacy/LegacyPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onLegacyHqToggled(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZZ)V
.end method

.method public abstract onLegacyInitializationError(Lcom/google/android/videos/player/legacy/LegacyPlayer;Ljava/lang/Exception;)V
.end method

.method public abstract onLegacyInitialized(Lcom/google/android/videos/player/legacy/LegacyPlayer;Lcom/google/android/videos/streams/LegacyStreamSelection;)V
.end method

.method public abstract onLegacyPausedFrameTimestamp(Lcom/google/android/videos/player/legacy/LegacyPlayer;I)V
.end method

.method public abstract onLegacyPlayerError(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V
.end method

.method public abstract onLegacyPlayerStateChanged(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZI)V
.end method

.method public abstract onLegacyReleased(I)V
.end method
