.class public final Lcom/google/android/videos/pano/repositories/MovieCursorCreator;
.super Ljava/lang/Object;
.source "MovieCursorCreator.java"

# interfaces
.implements Lcom/google/android/repolib/common/Creator;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Creator",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;)V
    .locals 1
    .param p1, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p2, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/SignInManager;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 23
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 24
    return-void
.end method

.method public static movieCursorCreator(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;)Lcom/google/android/repolib/common/Creator;
    .locals 1
    .param p0, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p1, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/accounts/SignInManager;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            ")",
            "Lcom/google/android/repolib/common/Creator",
            "<",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 29
    new-instance v0, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;-><init>(Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/PurchaseStore;)V

    return-object v0
.end method


# virtual methods
.method public create()Landroid/database/Cursor;
    .locals 11

    .prologue
    const/4 v1, 0x0

    .line 35
    iget-object v10, p0, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, user_assets, videos ON asset_type = 6 AND user_assets_type = 6 AND account = user_assets_account AND asset_id = user_assets_id AND asset_id = video_id"

    sget-object v3, Lcom/google/android/videos/pano/repositories/MoviesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "NOT (hidden IN (1, 3)) AND account = ?  AND purchase_status = 2 AND purchase_type IN(1,2) AND merged_expiration_timestamp > ?"

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v7}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v1

    const/4 v7, 0x1

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const-string v8, "merged_expiration_timestamp, title"

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v10, v0}, Lcom/google/android/videos/store/PurchaseStore;->createFrom(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic create()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 14
    invoke-virtual {p0}, Lcom/google/android/videos/pano/repositories/MovieCursorCreator;->create()Landroid/database/Cursor;

    move-result-object v0

    return-object v0
.end method
