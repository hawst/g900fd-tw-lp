.class final Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow$1;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"

# interfaces
.implements Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 399
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createSubRequest(Landroid/database/Cursor;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 4
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 402
    const/4 v3, 0x0

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 403
    .local v0, "account":Ljava/lang/String;
    const/16 v3, 0xb

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 404
    .local v2, "seasonId":Ljava/lang/String;
    const/4 v3, 0x4

    invoke-interface {p1, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 405
    .local v1, "nextEpisodeId":Ljava/lang/String;
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 406
    invoke-static {v0, v2}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForSingleShow;->createRequestNoUnpurchased(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v3

    .line 408
    :goto_0
    return-object v3

    :cond_0
    invoke-static {v0, v2, v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForSingleShow;->createRequest(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v3

    goto :goto_0
.end method
