.class public Lcom/google/android/videos/utils/BrowserUtil;
.super Ljava/lang/Object;
.source "BrowserUtil.java"


# direct methods
.method public static startWebSearch(Landroid/app/Activity;Ljava/lang/String;)I
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "searchQuery"    # Ljava/lang/String;

    .prologue
    const/16 v2, 0xf

    .line 79
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 81
    new-instance v3, Landroid/content/Intent;

    const-string v4, "android.intent.action.WEB_SEARCH"

    invoke-direct {v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-string v4, "query"

    invoke-virtual {v3, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 82
    .local v1, "intent":Landroid/content/Intent;
    invoke-static {p0, v1}, Lcom/google/android/videos/utils/Util;->canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 83
    const v3, 0x7f0b0140

    const/4 v4, 0x1

    invoke-static {p0, v3, v4}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 91
    :goto_0
    return v2

    .line 87
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 88
    const/4 v2, -0x1

    goto :goto_0

    .line 89
    :catch_0
    move-exception v0

    .line 90
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Activity not found to search the web"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method public static viewUri(Landroid/app/Activity;Landroid/net/Uri;)Z
    .locals 2
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/videos/utils/UriRewriter;

    invoke-virtual {p0}, Landroid/app/Activity;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriRewriter;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v0

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/BrowserUtil;->viewUriNoRewrite(Landroid/app/Activity;Landroid/net/Uri;)Z

    move-result v0

    return v0
.end method

.method public static viewUriNoRewrite(Landroid/app/Activity;Landroid/net/Uri;)Z
    .locals 5
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 49
    new-instance v1, Landroid/content/Intent;

    const-string v4, "android.intent.action.VIEW"

    invoke-direct {v1, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 50
    .local v1, "viewIntent":Landroid/content/Intent;
    invoke-virtual {v1, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 51
    invoke-static {p0, v1}, Lcom/google/android/videos/utils/Util;->canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 52
    const v4, 0x7f0b013d

    invoke-static {p0, v4, v3}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 60
    :goto_0
    return v2

    .line 56
    :cond_0
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    move v2, v3

    .line 57
    goto :goto_0

    .line 58
    :catch_0
    move-exception v0

    .line 59
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const-string v3, "Activity not found to view uri"

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method
