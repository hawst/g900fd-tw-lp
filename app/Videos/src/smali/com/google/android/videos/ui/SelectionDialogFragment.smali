.class public abstract Lcom/google/android/videos/ui/SelectionDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "SelectionDialogFragment.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# instance fields
.field private defaultValueIndex:I

.field private values:[Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 18
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static buildArguments(Ljava/lang/String;[Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Landroid/os/Bundle;
    .locals 2
    .param p0, "title"    # Ljava/lang/String;
    .param p1, "texts"    # [Ljava/lang/String;
    .param p2, "values"    # [Ljava/lang/String;
    .param p3, "initialValue"    # Ljava/lang/String;

    .prologue
    .line 25
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 26
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v1, "dialog_title"

    invoke-virtual {v0, v1, p0}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 27
    const-string v1, "dialog_list_texts"

    invoke-virtual {v0, v1, p1}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 28
    const-string v1, "dialog_list_values"

    invoke-virtual {v0, v1, p2}, Landroid/os/Bundle;->putStringArray(Ljava/lang/String;[Ljava/lang/String;)V

    .line 29
    const-string v1, "dialog_list_initial_value"

    invoke-virtual {v0, v1, p3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 30
    return-object v0
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 62
    invoke-virtual {p0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->onSelectionCanceled()V

    .line 63
    return-void
.end method

.method public final onClick(Landroid/content/DialogInterface;I)V
    .locals 2
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    .line 67
    const/4 v0, -0x1

    if-ne p2, v0, :cond_0

    .line 68
    iget-object v0, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    iget v1, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->defaultValueIndex:I

    aget-object v0, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->onSelection(Ljava/lang/String;)V

    .line 73
    :goto_0
    return-void

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    aget-object v0, v0, p2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->onSelection(Ljava/lang/String;)V

    .line 72
    invoke-virtual {p0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->dismiss()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 8
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 35
    invoke-virtual {p0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 36
    .local v0, "args":Landroid/os/Bundle;
    const-string v6, "dialog_title"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 37
    .local v5, "title":Ljava/lang/String;
    const-string v6, "dialog_list_initial_value"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 38
    .local v3, "initialValue":Ljava/lang/String;
    const-string v6, "dialog_list_texts"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v4

    .line 39
    .local v4, "texts":[Ljava/lang/String;
    const-string v6, "dialog_list_values"

    invoke-virtual {v0, v6}, Landroid/os/Bundle;->getStringArray(Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    .line 41
    iget-object v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    array-length v6, v6

    array-length v7, v4

    if-eq v6, v7, :cond_0

    .line 42
    new-instance v6, Ljava/lang/IllegalArgumentException;

    invoke-direct {v6}, Ljava/lang/IllegalArgumentException;-><init>()V

    throw v6

    .line 45
    :cond_0
    const/4 v6, 0x0

    iput v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->defaultValueIndex:I

    .line 46
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    array-length v6, v6

    if-ge v2, v6, :cond_1

    .line 47
    iget-object v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->values:[Ljava/lang/String;

    aget-object v6, v6, v2

    invoke-virtual {v6, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_2

    .line 48
    iput v2, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->defaultValueIndex:I

    .line 53
    :cond_1
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/SelectionDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v6

    invoke-direct {v1, v6}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 54
    .local v1, "builder":Landroid/app/AlertDialog$Builder;
    invoke-virtual {v1, v5}, Landroid/app/AlertDialog$Builder;->setTitle(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    .line 55
    const v6, 0x104000a

    invoke-virtual {v1, v6, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 56
    iget v6, p0, Lcom/google/android/videos/ui/SelectionDialogFragment;->defaultValueIndex:I

    invoke-virtual {v1, v4, v6, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems([Ljava/lang/CharSequence;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 57
    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v6

    return-object v6

    .line 46
    .end local v1    # "builder":Landroid/app/AlertDialog$Builder;
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method protected abstract onSelection(Ljava/lang/String;)V
.end method

.method protected abstract onSelectionCanceled()V
.end method
