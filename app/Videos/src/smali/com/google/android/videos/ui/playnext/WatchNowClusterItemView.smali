.class public Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView;
.source "WatchNowClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;
    }
.end annotation


# static fields
.field private static final GOLDEN_RATIO_HEIGHT_COMPOUND:I

.field private static drawPaint:Landroid/graphics/Paint;

.field private static drawRect:Landroid/graphics/Rect;


# instance fields
.field private backgroundProtectionView:Landroid/view/View;

.field private downloadView:Lcom/google/android/videos/ui/DownloadView;

.field private nowPlayingOverlayView:Landroid/view/View;

.field private originalColumnCount:F

.field private originalHeightCompound:I

.field private statusView:Landroid/widget/TextView;

.field private subtitleView:Landroid/widget/TextView;

.field private textBox:Landroid/view/View;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;

.field private videoId:Ljava/lang/String;

.field private watchProgress:Landroid/widget/ProgressBar;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 42
    const v0, 0x3f1e353f    # 0.618f

    invoke-static {v0}, Lcom/google/android/play/utils/Compound;->floatLengthToCompound(F)I

    move-result v0

    sput v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->GOLDEN_RATIO_HEIGHT_COMPOUND:I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 62
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 63
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 66
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 67
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 70
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/ClusterItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 71
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->setWillNotDraw(Z)V

    .line 72
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    if-nez v0, :cond_0

    .line 73
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0}, Landroid/graphics/Rect;-><init>()V

    sput-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    .line 74
    new-instance v0, Landroid/graphics/Paint;

    invoke-direct {v0}, Landroid/graphics/Paint;-><init>()V

    sput-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawPaint:Landroid/graphics/Paint;

    .line 75
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawPaint:Landroid/graphics/Paint;

    const/high16 v1, -0x1000000

    invoke-virtual {v0, v1}, Landroid/graphics/Paint;->setColor(I)V

    .line 77
    :cond_0
    return-void
.end method


# virtual methods
.method bind(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/adapter/WatchNowDataSource;IZLcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;)V
    .locals 32
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/WatchNowDataSource;
    .param p3, "position"    # I
    .param p4, "allowDownloads"    # Z
    .param p5, "binder"    # Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/adapter/WatchNowDataSource;",
            "IZ",
            "Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 129
    .local p1, "screenshotRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v16

    .line 131
    .local v16, "cursor":Landroid/database/Cursor;
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalColumnCount:F

    const/high16 v5, 0x3f800000    # 1.0f

    cmpl-float v4, v4, v5

    if-lez v4, :cond_0

    .line 136
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getCount()I

    move-result v4

    add-int/lit8 v4, v4, -0x1

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalColumnCount:F

    float-to-int v5, v5

    rem-int/2addr v4, v5

    add-int/lit8 v23, v4, 0x1

    .line 137
    .local v23, "largeItemEndIndex":I
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v24

    check-cast v24, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 139
    .local v24, "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    move/from16 v0, p3

    move/from16 v1, v23

    if-ge v0, v1, :cond_4

    .line 140
    const/high16 v4, 0x3f800000    # 1.0f

    move-object/from16 v0, v24

    iput v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 141
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalHeightCompound:I

    move-object/from16 v0, v24

    iput v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    .line 149
    .end local v23    # "largeItemEndIndex":I
    .end local v24    # "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    :cond_0
    :goto_0
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v31

    .line 150
    .local v31, "title":Ljava/lang/String;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isMovie(Landroid/database/Cursor;)Z

    move-result v22

    .line 151
    .local v22, "isMovie":Z
    if-eqz v22, :cond_5

    .line 152
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 153
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->subtitleView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 154
    move-object/from16 v0, p0

    move-object/from16 v1, v31

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 155
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromMovieId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v4

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v4}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    .line 175
    :goto_1
    const/16 v17, 0x0

    .line 176
    .local v17, "expireStatus":Ljava/lang/String;
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getExpirationTimestamp(Landroid/database/Cursor;)J

    move-result-wide v18

    .line 177
    .local v18, "expirationTimestamp":J
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v4

    if-nez v4, :cond_7

    const/16 v20, 0x1

    .line 178
    .local v20, "expired":Z
    :goto_2
    if-eqz v20, :cond_8

    .line 179
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b01fa

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v17

    .line 188
    :cond_1
    :goto_3
    if-eqz v17, :cond_9

    .line 189
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->statusView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 190
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->statusView:Landroid/widget/TextView;

    move-object/from16 v0, v17

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 196
    :goto_4
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    iput-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->videoId:Ljava/lang/String;

    .line 197
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v8

    .line 198
    .local v8, "pinningStatus":Ljava/lang/Integer;
    if-eqz p4, :cond_a

    if-nez v20, :cond_a

    .line 199
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    .line 200
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v0, v31

    invoke-virtual {v4, v0}, Lcom/google/android/videos/ui/DownloadView;->setTitle(Ljava/lang/String;)V

    .line 201
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v4

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v5

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    invoke-static {v4, v5, v6}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F

    move-result v9

    .line 204
    .local v9, "progressFraction":F
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getAccount(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->videoId:Ljava/lang/String;

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v7

    invoke-virtual/range {v4 .. v9}, Lcom/google/android/videos/ui/DownloadView;->update(Ljava/lang/String;Ljava/lang/String;ZLjava/lang/Integer;F)V

    .line 211
    .end local v9    # "progressFraction":F
    :goto_5
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v5

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setMax(I)V

    .line 212
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v5

    div-int/lit16 v5, v5, 0x3e8

    invoke-virtual {v4, v5}, Landroid/widget/ProgressBar;->setProgress(I)V

    .line 215
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isNowPlaying(Landroid/database/Cursor;)Z

    move-result v4

    if-eqz v4, :cond_b

    .line 216
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->backgroundProtectionView:Landroid/view/View;

    const v5, 0x7f0a00e5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 218
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->nowPlayingOverlayView:Landroid/view/View;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    .line 226
    :goto_6
    const v11, 0x7f0f00c9

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v14, 0x0

    const-class v15, Ljava/lang/String;

    move-object/from16 v10, p0

    move-object/from16 v13, p1

    invoke-virtual/range {v10 .. v15}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 230
    if-eqz v8, :cond_c

    invoke-virtual {v8}, Ljava/lang/Integer;->intValue()I

    move-result v4

    const/4 v5, 0x3

    if-ne v4, v5, :cond_c

    const/16 v21, 0x1

    .line 232
    .local v21, "isDownloaded":Z
    :goto_7
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/adapter/WatchNowDataSource;->isNetworkConnected()Z

    move-result v4

    if-nez v4, :cond_2

    if-eqz v21, :cond_3

    :cond_2
    if-eqz v20, :cond_d

    :cond_3
    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->setDimmedStyle(Z)V

    .line 235
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    if-eqz v22, :cond_e

    const v4, 0x7f0b0078

    :goto_9
    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->videoId:Ljava/lang/String;

    invoke-static {v5, v4, v6}, Lcom/google/android/videos/ui/TransitionUtil;->encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V

    .line 237
    return-void

    .line 143
    .end local v8    # "pinningStatus":Ljava/lang/Integer;
    .end local v17    # "expireStatus":Ljava/lang/String;
    .end local v18    # "expirationTimestamp":J
    .end local v20    # "expired":Z
    .end local v21    # "isDownloaded":Z
    .end local v22    # "isMovie":Z
    .end local v31    # "title":Ljava/lang/String;
    .restart local v23    # "largeItemEndIndex":I
    .restart local v24    # "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    :cond_4
    move-object/from16 v0, p0

    iget v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalColumnCount:F

    move-object/from16 v0, v24

    iput v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    .line 144
    sget v4, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->GOLDEN_RATIO_HEIGHT_COMPOUND:I

    move-object/from16 v0, v24

    iput v4, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    goto/16 :goto_0

    .line 158
    .end local v23    # "largeItemEndIndex":I
    .end local v24    # "layoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    .restart local v22    # "isMovie":Z
    .restart local v31    # "title":Ljava/lang/String;
    :cond_5
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v29

    .line 159
    .local v29, "showTitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->titleView:Landroid/widget/TextView;

    move-object/from16 v0, v29

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 160
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getSeasonTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v28

    .line 161
    .local v28, "seasonTitle":Ljava/lang/String;
    invoke-static/range {v28 .. v28}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_6

    .line 162
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00c7

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getSeasonNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v28

    .line 165
    :cond_6
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0b00c8

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/Object;

    const/4 v7, 0x0

    aput-object v28, v6, v7

    const/4 v7, 0x1

    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    aput-object v10, v6, v7

    invoke-virtual {v4, v5, v6}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v30

    .line 167
    .local v30, "subtitle":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->subtitleView:Landroid/widget/TextView;

    const/4 v5, 0x0

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 168
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->subtitleView:Landroid/widget/TextView;

    move-object/from16 v0, v30

    invoke-virtual {v4, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 169
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const/4 v5, 0x1

    const/4 v6, 0x2

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    aput-object v29, v6, v7

    const/4 v7, 0x1

    aput-object v30, v6, v7

    invoke-static {v4, v5, v6}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p0

    invoke-virtual {v0, v4}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 170
    move-object/from16 v0, p2

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/WatchNowDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromEpisodeId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v4

    move-object/from16 v0, p5

    move-object/from16 v1, p0

    invoke-virtual {v0, v1, v4}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    goto/16 :goto_1

    .line 177
    .end local v28    # "seasonTitle":Ljava/lang/String;
    .end local v29    # "showTitle":Ljava/lang/String;
    .end local v30    # "subtitle":Ljava/lang/String;
    .restart local v17    # "expireStatus":Ljava/lang/String;
    .restart local v18    # "expirationTimestamp":J
    :cond_7
    const/16 v20, 0x0

    goto/16 :goto_2

    .line 180
    .restart local v20    # "expired":Z
    :cond_8
    const-wide v4, 0x7fffffffffffffffL

    cmp-long v4, v18, v4

    if-eqz v4, :cond_1

    .line 181
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v26

    .line 182
    .local v26, "nowTimestamp":J
    move-wide/from16 v0, v18

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/TimeUtil;->getRemainingDays(JJ)I

    move-result v25

    .line 183
    .local v25, "remainingDays":I
    const/16 v4, 0x3c

    move/from16 v0, v25

    if-gt v0, v4, :cond_1

    .line 184
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    move-wide/from16 v0, v18

    move-wide/from16 v2, v26

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/utils/TimeUtil;->getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v17

    goto/16 :goto_3

    .line 192
    .end local v25    # "remainingDays":I
    .end local v26    # "nowTimestamp":J
    :cond_9
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->statusView:Landroid/widget/TextView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/widget/TextView;->setVisibility(I)V

    goto/16 :goto_4

    .line 207
    .restart local v8    # "pinningStatus":Ljava/lang/Integer;
    :cond_a
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Lcom/google/android/videos/ui/DownloadView;->setVisibility(I)V

    goto/16 :goto_5

    .line 220
    :cond_b
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->backgroundProtectionView:Landroid/view/View;

    const v5, 0x7f0201e5

    invoke-virtual {v4, v5}, Landroid/view/View;->setBackgroundResource(I)V

    .line 222
    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->nowPlayingOverlayView:Landroid/view/View;

    const/16 v5, 0x8

    invoke-virtual {v4, v5}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_6

    .line 230
    :cond_c
    const/16 v21, 0x0

    goto/16 :goto_7

    .line 232
    .restart local v21    # "isDownloaded":Z
    :cond_d
    const/4 v4, 0x0

    goto/16 :goto_8

    .line 235
    :cond_e
    const v4, 0x7f0b0077

    goto/16 :goto_9
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 251
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 252
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->textBox:Landroid/view/View;

    aput-object v1, p1, v0

    .line 253
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    aput-object v1, p1, v0

    .line 254
    return-void
.end method

.method public isOpaque()Z
    .locals 1

    .prologue
    .line 105
    const/4 v0, 0x1

    return v0
.end method

.method protected onDraw(Landroid/graphics/Canvas;)V
    .locals 4
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    const/4 v3, 0x0

    .line 110
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onDraw(Landroid/graphics/Canvas;)V

    .line 113
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getWidth()I

    move-result v1

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getPaddingTop()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 114
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 116
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->right:I

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getPaddingRight()I

    move-result v2

    sub-int/2addr v1, v2

    iput v1, v0, Landroid/graphics/Rect;->left:I

    .line 117
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->bottom:I

    iput v1, v0, Landroid/graphics/Rect;->top:I

    .line 118
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getHeight()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->bottom:I

    .line 119
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 121
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    iput v3, v0, Landroid/graphics/Rect;->left:I

    .line 122
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getPaddingLeft()I

    move-result v1

    iput v1, v0, Landroid/graphics/Rect;->right:I

    .line 123
    sget-object v0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawRect:Landroid/graphics/Rect;

    sget-object v1, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->drawPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v0, v1}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 125
    return-void
.end method

.method protected onFinishInflate()V
    .locals 5

    .prologue
    .line 81
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onFinishInflate()V

    .line 82
    const v2, 0x7f0f00dc

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->textBox:Landroid/view/View;

    .line 83
    const v2, 0x7f0f00a8

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->titleView:Landroid/widget/TextView;

    .line 84
    const v2, 0x7f0f00de

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->subtitleView:Landroid/widget/TextView;

    .line 85
    const v2, 0x7f0f00d8

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/TextView;

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->statusView:Landroid/widget/TextView;

    .line 86
    const v2, 0x7f0f00d1

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/DownloadView;

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->downloadView:Lcom/google/android/videos/ui/DownloadView;

    .line 87
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const/4 v3, 0x0

    const v4, 0x7f020131

    invoke-static {v2, v3, v4}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;ZI)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 89
    const v2, 0x7f0f00ce

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/widget/ProgressBar;

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->watchProgress:Landroid/widget/ProgressBar;

    .line 90
    const v2, 0x7f0f00dd

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->backgroundProtectionView:Landroid/view/View;

    .line 91
    const v2, 0x7f0f00df

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->nowPlayingOverlayView:Landroid/view/View;

    .line 92
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->nowPlayingOverlayView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    const/4 v4, 0x2

    new-array v4, v4, [I

    fill-array-data v4, :array_0

    invoke-static {v3, v4}, Lcom/google/android/videos/ui/FlickeringDrawable;->createFromDrawableResources(Landroid/content/res/Resources;[I)Lcom/google/android/videos/ui/FlickeringDrawable;

    move-result-object v3

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 96
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v1

    .line 97
    .local v1, "layoutParams":Landroid/view/ViewGroup$LayoutParams;
    instance-of v2, v1, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    move-object v0, v1

    .line 98
    check-cast v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;

    .line 99
    .local v0, "flowLayoutParams":Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;
    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->gridColumnCount:F

    iput v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalColumnCount:F

    .line 100
    iget v2, v0, Lcom/google/android/play/layout/FlowLayoutManager$LayoutParams;->heightCompound:I

    iput v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->originalHeightCompound:I

    .line 101
    return-void

    .line 92
    nop

    :array_0
    .array-data 4
        0x7f020177
        0x7f020176
    .end array-data
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 241
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowClusterItemView;->videoId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 244
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
