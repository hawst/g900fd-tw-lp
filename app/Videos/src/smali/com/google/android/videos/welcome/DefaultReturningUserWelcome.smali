.class public Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;
.super Lcom/google/android/videos/welcome/Welcome;
.source "DefaultReturningUserWelcome.java"

# interfaces
.implements Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;


# instance fields
.field private account:Ljava/lang/String;

.field private actionResIds:[I

.field private final activity:Lcom/google/android/videos/activity/HomeActivity;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private detailMessage:Ljava/lang/String;

.field private hasContent:Z

.field private showsLaunched:Z

.field private title:Ljava/lang/String;

.field private final verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/store/ConfigurationStore;)V
    .locals 1
    .param p1, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;

    .prologue
    .line 34
    const-string v0, "returningUser"

    invoke-direct {p0, v0}, Lcom/google/android/videos/welcome/Welcome;-><init>(Ljava/lang/String;)V

    .line 35
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/HomeActivity;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    .line 36
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 37
    invoke-virtual {p1}, Lcom/google/android/videos/activity/HomeActivity;->getVerticalsHelper()Lcom/google/android/videos/ui/VerticalsHelper;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    .line 38
    return-void
.end method

.method private setHasContent(Z)V
    .locals 1
    .param p1, "hasContent"    # Z

    .prologue
    .line 125
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->hasContent:Z

    if-eq v0, p1, :cond_0

    .line 126
    iput-boolean p1, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->hasContent:Z

    .line 127
    invoke-virtual {p0}, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->notifyEligibilityChanged()V

    .line 129
    :cond_0
    return-void
.end method


# virtual methods
.method public getActionResIds()[I
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->actionResIds:[I

    return-object v0
.end method

.method public getDefaultBitmapResId()I
    .locals 1

    .prologue
    .line 73
    const/4 v0, 0x0

    return v0
.end method

.method public getDetailMessage()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->detailMessage:Ljava/lang/String;

    return-object v0
.end method

.method public getNetworkBitmapUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 78
    const/4 v0, 0x0

    return-object v0
.end method

.method public getTitle()Ljava/lang/CharSequence;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->title:Ljava/lang/String;

    return-object v0
.end method

.method public onAction(I)V
    .locals 3
    .param p1, "index"    # I

    .prologue
    const/16 v2, 0xb

    .line 88
    if-nez p1, :cond_1

    .line 89
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isDrawerOpen()Z

    move-result v0

    if-nez v0, :cond_0

    .line 90
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->toggleDrawer()V

    .line 99
    :cond_0
    :goto_0
    return-void

    .line 93
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->showsLaunched:Z

    if-eqz v0, :cond_2

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesAndShows(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0

    .line 96
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMoviesVertical(Landroid/app/Activity;Ljava/lang/String;I)V

    goto :goto_0
.end method

.method public onEnabledVerticalsChanged(Ljava/lang/String;I)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    .line 115
    return-void
.end method

.method public onHasContentChanged(Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContent"    # Z

    .prologue
    .line 119
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    invoke-static {v0, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 120
    invoke-direct {p0, p2}, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->setHasContent(Z)V

    .line 122
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    iget-object v1, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/VerticalsHelper;->hasContent(Ljava/lang/String;)Z

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->setHasContent(Z)V

    .line 105
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/VerticalsHelper;->removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V

    .line 110
    return-void
.end method

.method public final prepareIfEligible(Ljava/lang/String;ZZ)Z
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "hasContentInVertical"    # Z
    .param p3, "downloadedOnly"    # Z

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 43
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    invoke-static {p1, v3}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 44
    iput-object p1, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->account:Ljava/lang/String;

    .line 45
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->verticalsHelper:Lcom/google/android/videos/ui/VerticalsHelper;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/ui/VerticalsHelper;->hasContent(Ljava/lang/String;)Z

    move-result v3

    invoke-direct {p0, v3}, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->setHasContent(Z)V

    .line 47
    :cond_0
    if-nez p2, :cond_1

    iget-boolean v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->hasContent:Z

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/HomeActivity;->hasDrawer()Z

    move-result v3

    if-eqz v3, :cond_1

    if-eqz p3, :cond_2

    :cond_1
    move v1, v2

    .line 58
    :goto_0
    return v1

    .line 50
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v3, p1}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v3

    iput-boolean v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->showsLaunched:Z

    .line 51
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    const v4, 0x7f0b00df

    invoke-virtual {v3, v4}, Lcom/google/android/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->title:Ljava/lang/String;

    .line 52
    iget-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->activity:Lcom/google/android/videos/activity/HomeActivity;

    const v4, 0x7f0b00e0

    invoke-virtual {v3, v4}, Lcom/google/android/videos/activity/HomeActivity;->getString(I)Ljava/lang/String;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->detailMessage:Ljava/lang/String;

    .line 53
    iget-boolean v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->showsLaunched:Z

    if-eqz v3, :cond_3

    const v0, 0x7f0b00f3

    .line 55
    .local v0, "playStoreButtonLabel":I
    :goto_1
    const/4 v3, 0x4

    new-array v3, v3, [I

    const v4, 0x7f020161

    aput v4, v3, v2

    const v2, 0x7f0b00e1

    aput v2, v3, v1

    const/4 v2, 0x2

    const v4, 0x7f0201c2

    aput v4, v3, v2

    const/4 v2, 0x3

    aput v0, v3, v2

    iput-object v3, p0, Lcom/google/android/videos/welcome/DefaultReturningUserWelcome;->actionResIds:[I

    goto :goto_0

    .line 53
    .end local v0    # "playStoreButtonLabel":I
    :cond_3
    const v0, 0x7f0b00f4

    goto :goto_1
.end method
