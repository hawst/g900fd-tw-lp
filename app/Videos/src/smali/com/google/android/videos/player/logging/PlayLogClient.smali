.class public Lcom/google/android/videos/player/logging/PlayLogClient;
.super Ljava/lang/Object;
.source "PlayLogClient.java"

# interfaces
.implements Lcom/google/android/videos/player/logging/LoggingClient;


# instance fields
.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private isOffline:Z

.field private isTrailer:Z

.field private lastErrorCode:I

.field private lastErrorType:I

.field private lastException:Ljava/lang/Exception;

.field private mediaTimeAtStartOfSeekMs:I

.field private playerType:I

.field private seasonId:Ljava/lang/String;

.field private showId:Ljava/lang/String;

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/logging/EventLogger;)V
    .locals 0
    .param p1, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 28
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 29
    iput-object p1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 30
    return-void
.end method


# virtual methods
.method public endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V
    .locals 8
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "derivedStats"    # Lcom/google/android/videos/player/logging/DerivedStats;

    .prologue
    .line 140
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->playerType:I

    iget-object v2, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->showId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->seasonId:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isTrailer:Z

    iget-boolean v6, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isOffline:Z

    move-object v7, p3

    invoke-interface/range {v0 .. v7}, Lcom/google/android/videos/logging/EventLogger;->onPlayerEnded(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/player/logging/DerivedStats;)V

    .line 142
    return-void
.end method

.method public onBandwidthSample(IIJJ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # I
    .param p3, "bytes"    # J
    .param p5, "bitrateEstimate"    # J

    .prologue
    .line 131
    return-void
.end method

.method public onDroppedFrames(II)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "count"    # I

    .prologue
    .line 89
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2}, Lcom/google/android/videos/logging/EventLogger;->onPlayerDroppedFrames(I)V

    .line 90
    return-void
.end method

.method public onError(IIIILjava/lang/Exception;)V
    .locals 12
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "errorType"    # I
    .param p4, "errorCode"    # I
    .param p5, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 74
    iput p3, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorType:I

    .line 75
    move/from16 v0, p4

    iput v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorCode:I

    .line 76
    move-object/from16 v0, p5

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastException:Ljava/lang/Exception;

    .line 77
    iget-object v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v2, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->playerType:I

    iget-object v3, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->videoId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->showId:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->seasonId:Ljava/lang/String;

    iget-boolean v6, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isTrailer:Z

    iget-boolean v7, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isOffline:Z

    move v8, p2

    move v9, p3

    move/from16 v10, p4

    move-object/from16 v11, p5

    invoke-interface/range {v1 .. v11}, Lcom/google/android/videos/logging/EventLogger;->onPlayerError(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V

    .line 79
    return-void
.end method

.method public onFailed(II)V
    .locals 11
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->playerType:I

    iget-object v2, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->showId:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->seasonId:Ljava/lang/String;

    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isTrailer:Z

    iget-boolean v6, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isOffline:Z

    iget v8, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorType:I

    iget v9, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorCode:I

    iget-object v10, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastException:Ljava/lang/Exception;

    move v7, p2

    invoke-interface/range {v0 .. v10}, Lcom/google/android/videos/logging/EventLogger;->onPlayerFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V

    .line 85
    return-void
.end method

.method public onFormatEnabled(III)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2, p3}, Lcom/google/android/videos/logging/EventLogger;->onPlayerFormatEnabled(II)V

    .line 100
    return-void
.end method

.method public onFormatSelected(III)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 94
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2, p3}, Lcom/google/android/videos/logging/EventLogger;->onPlayerFormatSelected(II)V

    .line 95
    return-void
.end method

.method public onHttpDataSourceOpened(IJ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "elapsedMs"    # J

    .prologue
    .line 136
    return-void
.end method

.method public onLoadingChanged(IZ)V
    .locals 0
    .param p1, "sessionTimeMs"    # I
    .param p2, "loading"    # Z

    .prologue
    .line 120
    return-void
.end method

.method public onNetworkType(III)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "type"    # I
    .param p3, "subType"    # I

    .prologue
    .line 124
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0}, Lcom/google/android/videos/logging/EventLogger;->onPlayerNetworkType()V

    .line 125
    return-void
.end method

.method public onStateChanged(IIZI)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "playWhenReady"    # Z
    .param p4, "playbackState"    # I

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2, p3, p4}, Lcom/google/android/videos/logging/EventLogger;->onPlayerStateChanged(IZI)V

    .line 69
    return-void
.end method

.method public onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2}, Lcom/google/android/videos/logging/EventLogger;->onPlayerSubtitleEnabled(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 110
    return-void
.end method

.method public onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 114
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2}, Lcom/google/android/videos/logging/EventLogger;->onPlayerSubtitleError(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 115
    return-void
.end method

.method public onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 1
    .param p1, "sessionTimeMs"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 104
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p2}, Lcom/google/android/videos/logging/EventLogger;->onPlayerSubtitleSelected(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 105
    return-void
.end method

.method public onUserSeekingChanged(IIZZ)V
    .locals 2
    .param p1, "sessionTimeMs"    # I
    .param p2, "mediaTimeMs"    # I
    .param p3, "seeking"    # Z
    .param p4, "isFineScrubbing"    # Z

    .prologue
    .line 57
    if-eqz p3, :cond_0

    .line 58
    iput p2, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->mediaTimeAtStartOfSeekMs:I

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-interface {v0, p4, p2}, Lcom/google/android/videos/logging/EventLogger;->onPlayerSeekingStart(ZI)V

    .line 63
    :goto_0
    return-void

    .line 61
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->mediaTimeAtStartOfSeekMs:I

    invoke-interface {v0, p4, p2, v1}, Lcom/google/android/videos/logging/EventLogger;->onPlayerSeekingEnd(ZII)V

    goto :goto_0
.end method

.method public startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 10
    .param p1, "sessionTimeProvider"    # Lcom/google/android/videos/player/logging/SessionTimeProvider;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "isOffline"    # Z

    .prologue
    .line 41
    iput p3, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->playerType:I

    .line 42
    iput-object p5, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->videoId:Ljava/lang/String;

    .line 43
    move-object/from16 v0, p6

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->showId:Ljava/lang/String;

    .line 44
    move-object/from16 v0, p7

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->seasonId:Ljava/lang/String;

    .line 45
    move/from16 v0, p8

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isTrailer:Z

    .line 46
    move/from16 v0, p9

    iput-boolean v0, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->isOffline:Z

    .line 47
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorType:I

    .line 48
    const/4 v1, 0x0

    iput v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastErrorCode:I

    .line 49
    const/4 v1, 0x0

    iput-object v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->lastException:Ljava/lang/Exception;

    .line 50
    iget-object v1, p0, Lcom/google/android/videos/player/logging/PlayLogClient;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p5

    move-object/from16 v6, p6

    move-object/from16 v7, p7

    move/from16 v8, p8

    move/from16 v9, p9

    invoke-interface/range {v1 .. v9}, Lcom/google/android/videos/logging/EventLogger;->onPlayerStarted(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 52
    return-void
.end method

.method public supportsOfflinePlaybacks()Z
    .locals 1

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method
