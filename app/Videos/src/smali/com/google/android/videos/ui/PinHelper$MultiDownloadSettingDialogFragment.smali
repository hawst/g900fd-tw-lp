.class public Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;
.super Landroid/support/v4/app/DialogFragment;
.source "PinHelper.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;
.implements Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/PinHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "MultiDownloadSettingDialogFragment"
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private preferences:Landroid/content/SharedPreferences;

.field private settingAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 303
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 3
    .param p0, "manager"    # Landroid/support/v4/app/FragmentManager;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "hdAvailable"    # Z
    .param p4, "multipleAudioLanguages"    # Z

    .prologue
    .line 313
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 314
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 315
    const-string v2, "video_id"

    invoke-virtual {v0, v2, p2}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 316
    const-string v2, "hd_available"

    invoke-virtual {v0, v2, p3}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 317
    const-string v2, "multiple_audio_languages"

    invoke-virtual {v0, v2, p4}, Landroid/os/Bundle;->putBoolean(Ljava/lang/String;Z)V

    .line 319
    new-instance v1, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;

    invoke-direct {v1}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;-><init>()V

    .line 320
    .local v1, "instance":Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->setArguments(Landroid/os/Bundle;)V

    .line 321
    const-string v2, "DownloadMultiSettingDialog"

    invoke-virtual {v1, p0, v2}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 322
    return-void
.end method


# virtual methods
.method public final onCancel(Landroid/content/DialogInterface;)V
    .locals 1
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 404
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 405
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    # invokes: Lcom/google/android/videos/ui/PinHelper;->onPinCanceled(Landroid/app/Activity;)V
    invoke-static {v0}, Lcom/google/android/videos/ui/PinHelper;->access$300(Landroid/app/Activity;)V

    .line 406
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 10
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "position"    # I

    .prologue
    const/4 v5, 0x0

    .line 372
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 373
    .local v0, "activity":Landroid/support/v4/app/FragmentActivity;
    if-nez v0, :cond_1

    .line 400
    :cond_0
    :goto_0
    return-void

    .line 378
    :cond_1
    const/4 v1, -0x1

    if-ne p2, v1, :cond_3

    .line 379
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    const-string v2, "download_setting_shown_flags"

    invoke-interface {v1, v2, v5}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v6

    .line 380
    .local v6, "alreadyShownFlags":I
    move v7, v6

    .line 381
    .local v7, "combinedShownFlags":I
    const/4 v8, 0x0

    .local v8, "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1}, Landroid/widget/ArrayAdapter;->getCount()I

    move-result v1

    if-ge v8, v1, :cond_2

    .line 382
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, v8}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v1

    or-int/2addr v7, v1

    .line 381
    add-int/lit8 v8, v8, 0x1

    goto :goto_1

    .line 384
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    const-string v2, "download_setting_shown_flags"

    invoke-interface {v1, v2, v7}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 385
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/ui/PinHelper;->requestPin(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/ui/PinHelper;->access$200(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 389
    .end local v6    # "alreadyShownFlags":I
    .end local v7    # "combinedShownFlags":I
    .end local v8    # "i":I
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v1, p2}, Landroid/widget/ArrayAdapter;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v9

    .line 390
    .local v9, "settingFlag":I
    const/4 v1, 0x1

    if-ne v9, v1, :cond_4

    .line 391
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->videoId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showNetworkPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 393
    :cond_4
    const/4 v1, 0x2

    if-ne v9, v1, :cond_5

    .line 394
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->videoId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showQualityPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 396
    :cond_5
    const/4 v1, 0x4

    if-ne v9, v1, :cond_0

    .line 397
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    iget-object v3, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->videoId:Ljava/lang/String;

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/PinHelper$SingleDownloadSettingDialogFragment;->showAudioPreferenceInstance(Landroid/content/Context;Landroid/support/v4/app/FragmentManager;Landroid/content/SharedPreferences;Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 10
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 326
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getArguments()Landroid/os/Bundle;

    move-result-object v1

    .line 327
    .local v1, "args":Landroid/os/Bundle;
    const-string v8, "authAccount"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->account:Ljava/lang/String;

    .line 328
    const-string v8, "video_id"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->videoId:Ljava/lang/String;

    .line 329
    const-string v8, "hd_available"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v3

    .line 330
    .local v3, "hdAvailable":Z
    const-string v8, "multiple_audio_languages"

    invoke-virtual {v1, v8}, Landroid/os/Bundle;->getBoolean(Ljava/lang/String;)Z

    move-result v4

    .line 332
    .local v4, "multipleAudioLanguages":Z
    invoke-virtual {p0}, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 333
    .local v0, "activity":Landroid/app/Activity;
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v7

    .line 334
    .local v7, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    .line 335
    .local v5, "networkStatus":Lcom/google/android/videos/utils/NetworkStatus;
    invoke-virtual {v7}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v8

    iput-object v8, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    .line 337
    new-instance v2, Landroid/app/AlertDialog$Builder;

    invoke-direct {v2, v0}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    .line 338
    .local v2, "builder":Landroid/app/AlertDialog$Builder;
    const v8, 0x7f0b012b

    invoke-virtual {v2, v8}, Landroid/app/AlertDialog$Builder;->setTitle(I)Landroid/app/AlertDialog$Builder;

    .line 339
    const v8, 0x104000a

    invoke-virtual {v2, v8, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 341
    new-instance v6, Ljava/util/ArrayList;

    invoke-direct {v6}, Ljava/util/ArrayList;-><init>()V

    .line 342
    .local v6, "settingFlags":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Ljava/lang/Integer;>;"
    invoke-interface {v5}, Lcom/google/android/videos/utils/NetworkStatus;->isMobileNetworkCapable()Z

    move-result v8

    if-eqz v8, :cond_0

    .line 343
    const/4 v8, 0x1

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 345
    :cond_0
    if-eqz v3, :cond_1

    .line 346
    const/4 v8, 0x2

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 348
    :cond_1
    if-eqz v4, :cond_2

    .line 349
    const/4 v8, 0x4

    invoke-static {v8}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v8

    invoke-virtual {v6, v8}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 352
    :cond_2
    new-instance v8, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;

    iget-object v9, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-direct {v8, v0, v9, v6}, Lcom/google/android/videos/ui/PinHelper$SettingAdapter;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Ljava/util/ArrayList;)V

    iput-object v8, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    .line 353
    iget-object v8, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    const/4 v9, -0x1

    invoke-virtual {v2, v8, v9, p0}, Landroid/app/AlertDialog$Builder;->setSingleChoiceItems(Landroid/widget/ListAdapter;ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    .line 354
    invoke-virtual {v2}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v8

    return-object v8
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 366
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onPause()V

    .line 367
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->unregisterOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 368
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 359
    invoke-super {p0}, Landroid/support/v4/app/DialogFragment;->onResume()V

    .line 360
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0, p0}, Landroid/content/SharedPreferences;->registerOnSharedPreferenceChangeListener(Landroid/content/SharedPreferences$OnSharedPreferenceChangeListener;)V

    .line 361
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 362
    return-void
.end method

.method public onSharedPreferenceChanged(Landroid/content/SharedPreferences;Ljava/lang/String;)V
    .locals 1
    .param p1, "preferences"    # Landroid/content/SharedPreferences;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 410
    iget-object v0, p0, Lcom/google/android/videos/ui/PinHelper$MultiDownloadSettingDialogFragment;->settingAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0}, Landroid/widget/ArrayAdapter;->notifyDataSetChanged()V

    .line 411
    return-void
.end method
