.class Lcom/google/android/videos/ui/EidrIdConverterHelper$1;
.super Ljava/lang/Object;
.source "EidrIdConverterHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/EidrIdConverterHelper;->convert(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/String;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

.field final synthetic val$eidrIdSource:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/EidrIdConverterHelper;I)V
    .locals 0

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    iput p2, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->val$eidrIdSource:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 70
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "eidrId"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 79
    instance-of v0, p2, Lcom/google/android/videos/async/NotFoundException;

    if-eqz v0, :cond_0

    .line 80
    iget v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->val$eidrIdSource:I

    packed-switch v0, :pswitch_data_0

    .line 90
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "eidrId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " not found"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 91
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    .line 98
    :goto_0
    return-void

    .line 82
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->syncPurchasesForEidrId()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$100(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    goto :goto_0

    .line 86
    :pswitch_1
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->convertThroughAssetApi()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$200(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    goto :goto_0

    .line 95
    :cond_0
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unable to convert eidrId "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 96
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onFailed()V
    invoke-static {v0}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$300(Lcom/google/android/videos/ui/EidrIdConverterHelper;)V

    goto :goto_0

    .line 80
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 70
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->onResponse(Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "eidrId"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/ui/EidrIdConverterHelper$1;->this$0:Lcom/google/android/videos/ui/EidrIdConverterHelper;

    # invokes: Lcom/google/android/videos/ui/EidrIdConverterHelper;->onEidrIdConverted(Ljava/lang/String;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/ui/EidrIdConverterHelper;->access$000(Lcom/google/android/videos/ui/EidrIdConverterHelper;Ljava/lang/String;)V

    .line 75
    return-void
.end method
