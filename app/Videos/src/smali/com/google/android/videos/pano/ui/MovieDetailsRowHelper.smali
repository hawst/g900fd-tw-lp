.class public Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;
.super Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.source "MovieDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;,
        Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$Query;
    }
.end annotation


# instance fields
.field private final contentView:Landroid/view/View;

.field private ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;Landroid/view/View;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "onPurchaseActionListener"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
    .param p6, "relatedRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p10, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p11, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .param p12, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p14, "contentView"    # Landroid/view/View;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/WishlistStore;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/view/View;",
            ")V"
        }
    .end annotation

    .prologue
    .line 73
    .local p5, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p7, "relatedRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p8, "reviewsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetReviewListRequest;Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;>;"
    .local p9, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    .local p13, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct/range {p0 .. p13}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;)V

    .line 76
    iput-object p14, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->contentView:Landroid/view/View;

    .line 77
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->contentView:Landroid/view/View;

    return-object v0
.end method

.method private addPrice(ILcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Z
    .locals 19
    .param p1, "action"    # I
    .param p2, "resource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .param p3, "itemId"    # Ljava/lang/String;

    .prologue
    .line 186
    packed-switch p1, :pswitch_data_0

    .line 209
    const/4 v3, 0x0

    .line 226
    :goto_0
    return v3

    .line 188
    :pswitch_0
    const/16 v16, 0x4

    .line 189
    .local v16, "id":I
    const/4 v6, 0x1

    .line 190
    .local v6, "purchaseType":I
    const/16 v18, 0x0

    .line 191
    .local v18, "preorder":Z
    const/16 v17, 0x2

    .line 192
    .local v17, "offerType":I
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move/from16 v0, v17

    invoke-static {v3, v4, v0}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v15

    .line 211
    .local v15, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    :goto_1
    if-nez v15, :cond_0

    .line 212
    const/4 v3, 0x0

    goto :goto_0

    .line 195
    .end local v6    # "purchaseType":I
    .end local v15    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .end local v16    # "id":I
    .end local v17    # "offerType":I
    .end local v18    # "preorder":Z
    :pswitch_1
    const/16 v16, 0x3

    .line 196
    .restart local v16    # "id":I
    const/4 v6, 0x2

    .line 197
    .restart local v6    # "purchaseType":I
    const/16 v18, 0x0

    .line 198
    .restart local v18    # "preorder":Z
    const/16 v17, 0x1

    .line 199
    .restart local v17    # "offerType":I
    const/4 v3, 0x0

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move/from16 v0, v17

    invoke-static {v3, v4, v0}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v15

    .line 200
    .restart local v15    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    goto :goto_1

    .line 202
    .end local v6    # "purchaseType":I
    .end local v15    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .end local v16    # "id":I
    .end local v17    # "offerType":I
    .end local v18    # "preorder":Z
    :pswitch_2
    const/16 v16, 0x5

    .line 203
    .restart local v16    # "id":I
    const/4 v6, 0x0

    .line 204
    .restart local v6    # "purchaseType":I
    const/16 v18, 0x1

    .line 205
    .restart local v18    # "preorder":Z
    const/16 v17, -0x1

    .line 206
    .restart local v17    # "offerType":I
    const/4 v3, 0x1

    move-object/from16 v0, p2

    iget-object v4, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    move/from16 v0, v17

    invoke-static {v3, v4, v0}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v15

    .line 207
    .restart local v15    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    goto :goto_1

    .line 215
    :cond_0
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    move/from16 v0, v17

    move/from16 v1, v18

    invoke-static {v0, v15, v3, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->formatPriceTitle(ILcom/google/android/videos/utils/OfferUtil$CheapestOffer;Landroid/content/Context;Z)Ljava/lang/String;

    move-result-object v12

    .line 218
    .local v12, "title":Ljava/lang/String;
    new-instance v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    const/4 v4, 0x0

    const/4 v5, 0x0

    const/16 v7, 0xc

    move-object/from16 v3, p3

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;II)V

    .line 220
    .local v2, "purchase":Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;
    new-instance v8, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$2;

    move/from16 v0, v16

    int-to-long v10, v0

    const/4 v13, 0x0

    move-object/from16 v9, p0

    move-object v14, v2

    invoke-direct/range {v8 .. v14}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$2;-><init>(Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V

    move-object/from16 v0, p0

    invoke-virtual {v0, v8}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 226
    const/4 v3, 0x1

    goto :goto_0

    .line 186
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private getOwnershipInformation()Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;
    .locals 15

    .prologue
    .line 230
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    if-eqz v0, :cond_0

    .line 231
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    .line 259
    :goto_0
    return-object v0

    .line 234
    :cond_0
    sget-object v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->NOT_OWNED:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    .line 235
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v12

    .line 236
    .local v12, "purchaseCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    iget-object v14, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchased_assets, user_assets, videos ON asset_type = 6 AND user_assets_type = 6 AND account = user_assets_account AND asset_id = user_assets_id AND asset_id = video_id"

    sget-object v3, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$Query;->COLUMNS:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "account = ? AND video_id = ? AND purchase_status in (2, 6) AND merged_expiration_timestamp > ?"

    const/4 v6, 0x3

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x1

    iget-object v8, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    aput-object v8, v6, v7

    const/4 v7, 0x2

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {v8, v9}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    invoke-virtual {v14, v0, v12}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 243
    :try_start_0
    invoke-virtual {v12}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v10

    check-cast v10, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 245
    .local v10, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v10}, Landroid/database/Cursor;->moveToFirst()Z

    move-result v0

    if-eqz v0, :cond_1

    .line 246
    const/4 v0, 0x0

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 247
    .local v3, "resumeTimeMillis":I
    const/4 v0, 0x1

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 248
    .local v4, "expirationTimestamp":J
    const/4 v0, 0x2

    invoke-interface {v10, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v13

    .line 249
    .local v13, "purchaseStatus":I
    new-instance v1, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    const/4 v2, 0x1

    const/4 v0, 0x6

    if-ne v13, v0, :cond_2

    const/4 v6, 0x1

    :goto_1
    invoke-direct/range {v1 .. v6}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;-><init>(ZIJZ)V

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    .line 253
    .end local v3    # "resumeTimeMillis":I
    .end local v4    # "expirationTimestamp":J
    .end local v13    # "purchaseStatus":I
    :cond_1
    :try_start_2
    invoke-interface {v10}, Landroid/database/Cursor;->close()V
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    .line 259
    .end local v10    # "cursor":Landroid/database/Cursor;
    :goto_2
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    goto :goto_0

    .line 249
    .restart local v3    # "resumeTimeMillis":I
    .restart local v4    # "expirationTimestamp":J
    .restart local v10    # "cursor":Landroid/database/Cursor;
    .restart local v13    # "purchaseStatus":I
    :cond_2
    const/4 v6, 0x0

    goto :goto_1

    .line 253
    .end local v3    # "resumeTimeMillis":I
    .end local v4    # "expirationTimestamp":J
    .end local v13    # "purchaseStatus":I
    :catchall_0
    move-exception v0

    :try_start_3
    invoke-interface {v10}, Landroid/database/Cursor;->close()V

    throw v0
    :try_end_3
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_3 .. :try_end_3} :catch_0

    .line 255
    .end local v10    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v11

    .line 256
    .local v11, "e":Ljava/util/concurrent/ExecutionException;
    const-string v0, "Failed to fetch purchases"

    invoke-virtual {v11}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_2
.end method


# virtual methods
.method public addDetailsSectionActions()V
    .locals 33

    .prologue
    .line 99
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    .line 101
    .local v31, "resources":Landroid/content/res/Resources;
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v24

    .line 102
    .local v24, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v24, :cond_1

    .line 160
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->getOwnershipInformation()Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    move-result-object v28

    .line 108
    .local v28, "ownershipInfo":Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;
    move-object/from16 v0, v28

    iget-boolean v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->purchased:Z

    if-eqz v2, :cond_3

    move-object/from16 v0, v28

    iget-boolean v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->preorder:Z

    if-nez v2, :cond_3

    .line 109
    move-object/from16 v0, v28

    iget v8, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->resumeTimeMillis:I

    .line 110
    .local v8, "resumeTimeMillis":I
    move-object/from16 v0, v24

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    mul-int/lit16 v2, v2, 0x3e8

    move-object/from16 v0, v24

    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->startOfCreditSec:I

    invoke-static {v8, v2, v3}, Lcom/google/android/videos/utils/Util;->isAtEndOfMovie(III)Z

    move-result v2

    if-eqz v2, :cond_2

    .line 112
    const/4 v8, 0x0

    .line 116
    :cond_2
    if-nez v8, :cond_6

    .line 117
    const v2, 0x7f0b00ac

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 124
    .local v29, "playFromBeginningLabel":Ljava/lang/String;
    :goto_1
    const-wide/16 v2, 0x1

    const/4 v4, 0x0

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v14

    move-object/from16 v9, p0

    move-wide v10, v2

    move-object/from16 v12, v29

    move-object v13, v4

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 129
    .end local v8    # "resumeTimeMillis":I
    .end local v29    # "playFromBeginningLabel":Ljava/lang/String;
    :cond_3
    move-object/from16 v0, v24

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->trailerId:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    move-object/from16 v23, v0

    .local v23, "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    move-object/from16 v0, v23

    array-length v0, v0

    move/from16 v27, v0

    .local v27, "len$":I
    const/16 v26, 0x0

    .local v26, "i$":I
    :goto_2
    move/from16 v0, v26

    move/from16 v1, v27

    if-ge v0, v1, :cond_4

    aget-object v30, v23, v26

    .line 130
    .local v30, "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    move-object/from16 v0, v30

    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/4 v3, 0x6

    if-ne v2, v3, :cond_7

    .line 131
    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    move-object/from16 v0, v30

    iget-object v11, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x1

    const/4 v15, 0x0

    invoke-static/range {v9 .. v15}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v14

    .line 133
    .local v14, "actionIntent":Landroid/content/Intent;
    const-wide/16 v10, 0x2

    const v2, 0x7f0b00af

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    .line 139
    .end local v14    # "actionIntent":Landroid/content/Intent;
    .end local v30    # "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_4
    move-object/from16 v0, v28

    iget-boolean v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->purchased:Z

    if-nez v2, :cond_0

    .line 141
    const/4 v2, 0x0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addPrice(ILcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Z

    move-result v25

    .line 142
    .local v25, "hasBuyOrRentButton":Z
    const/4 v2, 0x1

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addPrice(ILcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Z

    move-result v2

    or-int v25, v25, v2

    .line 143
    if-nez v25, :cond_5

    .line 144
    const/4 v2, 0x2

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    move-object/from16 v0, p0

    move-object/from16 v1, v24

    invoke-direct {v0, v2, v1, v3}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addPrice(ILcom/google/wireless/android/video/magma/proto/AssetResource;Ljava/lang/String;)Z

    .line 147
    :cond_5
    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->isAddedToWishlist()Z

    move-result v22

    .line 149
    .local v22, "addedToWishlist":Z
    if-eqz v22, :cond_8

    const v32, 0x7f0b00b1

    .line 151
    .local v32, "wishlistTitleId":I
    :goto_3
    new-instance v16, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;

    const-wide/16 v18, 0x5

    invoke-virtual/range {v31 .. v32}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v20

    const/16 v21, 0x0

    move-object/from16 v17, p0

    invoke-direct/range {v16 .. v22}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$1;-><init>(Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Z)V

    .line 158
    .local v16, "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    move-object/from16 v0, p0

    move-object/from16 v1, v16

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    goto/16 :goto_0

    .line 119
    .end local v16    # "action":Lcom/google/android/videos/pano/ui/ClickableAction;
    .end local v22    # "addedToWishlist":Z
    .end local v23    # "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v25    # "hasBuyOrRentButton":Z
    .end local v26    # "i$":I
    .end local v27    # "len$":I
    .end local v32    # "wishlistTitleId":I
    .restart local v8    # "resumeTimeMillis":I
    :cond_6
    const v2, 0x7f0b00ae

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v29

    .line 120
    .restart local v29    # "playFromBeginningLabel":Ljava/lang/String;
    const-wide/16 v10, 0x0

    const v2, 0x7f0b00ad

    move-object/from16 v0, v31

    invoke-virtual {v0, v2}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v12

    const/4 v13, 0x0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->context:Landroid/content/Context;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    const/4 v5, 0x0

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-static/range {v2 .. v8}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZI)Landroid/content/Intent;

    move-result-object v14

    move-object/from16 v9, p0

    invoke-virtual/range {v9 .. v14}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V

    goto/16 :goto_1

    .line 129
    .end local v8    # "resumeTimeMillis":I
    .end local v29    # "playFromBeginningLabel":Ljava/lang/String;
    .restart local v23    # "arr$":[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .restart local v26    # "i$":I
    .restart local v27    # "len$":I
    .restart local v30    # "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    :cond_7
    add-int/lit8 v26, v26, 0x1

    goto/16 :goto_2

    .line 149
    .end local v30    # "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .restart local v22    # "addedToWishlist":Z
    .restart local v25    # "hasBuyOrRentButton":Z
    :cond_8
    const v32, 0x7f0b00b0

    goto :goto_3
.end method

.method protected addExtra(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;)V
    .locals 4
    .param p1, "overview"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;

    .prologue
    .line 91
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->getOwnershipInformation()Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    move-result-object v0

    .line 92
    .local v0, "ownershipInfo":Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;
    iget-wide v2, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->expirationTimestamp:J

    iput-wide v2, p1, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->expirationTimestamp:J

    .line 93
    iget-boolean v1, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->preorder:Z

    iput-boolean v1, p1, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->preorder:Z

    .line 94
    iget-boolean v1, v0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;->purchased:Z

    iput-boolean v1, p1, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->purchased:Z

    .line 95
    return-void
.end method

.method protected getAssetId(Ljava/lang/String;)Ljava/lang/String;
    .locals 1
    .param p1, "itemId"    # Ljava/lang/String;

    .prologue
    .line 177
    invoke-static {p1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method protected getRelatedRequest()Lcom/google/android/videos/api/RecommendationsRequest;
    .locals 5

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->relatedRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    const/16 v3, 0xf

    const/16 v4, 0x1b

    invoke-interface {v0, v1, v2, v3, v4}, Lcom/google/android/videos/api/RecommendationsRequest$Factory;->createForRelatedMovies(Ljava/lang/String;Ljava/lang/String;II)Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v0

    return-object v0
.end method

.method protected getWishlistRequest()Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 4

    .prologue
    .line 172
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->account:Ljava/lang/String;

    const/4 v2, 0x6

    iget-object v3, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;ILjava/lang/String;)V

    return-object v0
.end method

.method public onPurchasesUpdated()V
    .locals 1

    .prologue
    .line 86
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    .line 87
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 81
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper;->ownershipInfo:Lcom/google/android/videos/pano/ui/MovieDetailsRowHelper$OwnershipInfo;

    .line 82
    return-void
.end method
