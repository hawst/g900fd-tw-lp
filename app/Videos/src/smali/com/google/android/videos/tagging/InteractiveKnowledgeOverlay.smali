.class public Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
.super Landroid/widget/FrameLayout;
.source "InteractiveKnowledgeOverlay.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/player/PlayerView$PlayerOverlay;
.implements Lcom/google/android/videos/tagging/CardsView$Listener;
.implements Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;
.implements Lcom/google/android/videos/tagging/FeedbackViewHelper$Listener;
.implements Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
.implements Lcom/google/android/videos/tagging/KnowledgeView;
.implements Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;
.implements Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;,
        Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private activity:Landroid/app/Activity;

.field private final actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

.field private final cardsTransitionRoot:Landroid/widget/FrameLayout;

.field private final cardsView:Lcom/google/android/videos/tagging/CardsView;

.field private final cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

.field private configuredOrientation:I

.field private final entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

.field private final entitiesTransitionRoot:Landroid/widget/FrameLayout;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

.field private feedbackProductId:I

.field private final feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

.field private horizontalGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

.field private knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

.field private knowledgeFeedbackTypeId:I

.field private listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

.field private mode:I

.field private networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

.field private final secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

.field private showKnowledgeAtMillis:I

.field private final songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final taggedKnowledgeEntities:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;"
        }
    .end annotation
.end field

.field private final tagsView:Lcom/google/android/videos/tagging/TagsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 140
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 141
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 144
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 145
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 9
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 148
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 149
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040045

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 150
    const v0, 0x7f0f0123

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/TagsView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    .line 151
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/TagsView;->setOnTagClickListener(Lcom/google/android/videos/tagging/TagsView$OnTagClickListener;)V

    .line 153
    const v0, 0x7f0f0121

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    .line 154
    const v0, 0x7f0f0124

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    .line 155
    const v0, 0x7f0f0122

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/RecyclerView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    .line 156
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-direct {v0, p1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    .line 157
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->setOnEntityClickListener(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$OnEntityClickListener;)V

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v2, "IntKnowledgeOverlay"

    invoke-direct {v1, v2}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 160
    new-instance v6, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    invoke-direct {v6}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;-><init>()V

    .line 161
    .local v6, "animator":Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;
    invoke-virtual {v6, v8}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->setSupportsChangeAnimations(Z)V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v6}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v7}, Landroid/support/v7/widget/RecyclerView;->setClipToPadding(Z)V

    .line 164
    new-instance v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-direct {v0, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;-><init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    .line 166
    const v0, 0x7f0f0125

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/CardsView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    .line 167
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/CardsView;->setCardDismissListener(Lcom/google/android/videos/tagging/CardsView$Listener;)V

    .line 168
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0d0007

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getBoolean(I)Z

    move-result v4

    .line 170
    .local v4, "hideTagsWhenObscured":Z
    new-instance v0, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    new-instance v2, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;

    invoke-direct {v2, p0, v4}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$CardsViewAnimationHelperListener;-><init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Z)V

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;-><init>(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;Lcom/google/android/videos/tagging/CardsView;Lcom/google/android/videos/tagging/CardsViewAnimationHelper$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    .line 172
    new-instance v0, Lcom/google/android/videos/tagging/FeedbackViewHelper;

    const v1, 0x7f0f0126

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    move-object v5, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/FeedbackViewHelper;-><init>(Landroid/view/View;Lcom/google/android/videos/tagging/TagsView;Lcom/google/android/videos/tagging/CardsView;ZLcom/google/android/videos/tagging/FeedbackViewHelper$Listener;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    .line 174
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    .line 176
    new-instance v0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-direct {v0, p1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->setListener(Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;)V

    .line 179
    new-instance v0, Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-direct {v0, p1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->setListener(Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;)V

    .line 186
    iput v7, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->configuredOrientation:I

    .line 188
    invoke-virtual {p0, v7}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setClipChildren(Z)V

    .line 189
    invoke-virtual {p0, v7}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setClipToPadding(Z)V

    .line 190
    invoke-virtual {p0, v8}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setMeasureAllChildren(Z)V

    .line 191
    invoke-direct {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->clearKnowledge()V

    .line 192
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;)Lcom/google/android/videos/tagging/TagsView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    return-object v0
.end method

.method private clearKnowledge()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 365
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->clear()V

    .line 366
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(I)V

    .line 367
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    .line 368
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hide(Z)V

    .line 369
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hide(Z)V

    .line 370
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 371
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 372
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 373
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 374
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 375
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setVisibility(I)V

    .line 376
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 377
    const/4 v0, -0x1

    iput v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->showKnowledgeAtMillis:I

    .line 378
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clear()V

    .line 382
    :cond_0
    :goto_0
    return-void

    .line 379
    :cond_1
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 380
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->clearKnowledge()V

    goto :goto_0
.end method


# virtual methods
.method public dispatchTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 564
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onStartDispatchTouchEvent()V

    .line 565
    invoke-super {p0, p1}, Landroid/widget/FrameLayout;->dispatchTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    return v0
.end method

.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 201
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getMode()I
    .locals 1

    .prologue
    .line 361
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    return v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 196
    return-object p0
.end method

.method public hasContent()Z
    .locals 4

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 247
    iget v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_2

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesAdapter:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter;->getItemCount()I

    move-result v2

    if-eqz v2, :cond_1

    :cond_0
    :goto_0
    return v0

    :cond_1
    move v0, v1

    goto :goto_0

    :cond_2
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    goto :goto_0
.end method

.method public hideKnowledge()V
    .locals 0

    .prologue
    .line 343
    invoke-direct {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->clearKnowledge()V

    .line 344
    return-void
.end method

.method public init(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/tagging/FeedbackClient;IILcom/google/android/videos/logging/EventLogger;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p5, "feedbackClient"    # Lcom/google/android/videos/tagging/FeedbackClient;
    .param p6, "feedbackProductId"    # I
    .param p7, "knowledgeFeedbackTypeId"    # I
    .param p8, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;

    .prologue
    .line 231
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/Activity;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    .line 232
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->account:Ljava/lang/String;

    .line 233
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/StoreStatusMonitor;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 234
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 235
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/FeedbackClient;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

    .line 236
    iput p6, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackProductId:I

    .line 237
    iput p7, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeFeedbackTypeId:I

    .line 238
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 239
    invoke-direct {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->clearKnowledge()V

    .line 240
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->onInit(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;)V

    .line 241
    return-void
.end method

.method public initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V
    .locals 9
    .param p1, "knowledgeBundle"    # Lcom/google/android/videos/tagging/KnowledgeBundle;
    .param p2, "timeSupplier"    # Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
    .param p3, "mode"    # I

    .prologue
    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 253
    iput-object p1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 254
    iput p3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    .line 255
    if-eqz p1, :cond_0

    .line 256
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    iget-object v5, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v6, p1, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->onInitKnowledge(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/async/Requester;)V

    .line 259
    :cond_0
    const/4 v0, 0x1

    if-ne p3, v0, :cond_2

    .line 260
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->horizontalGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setFallbackGestureListener(Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V

    .line 261
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0, v7}, Lcom/google/android/videos/tagging/TagsView;->setVisibility(I)V

    .line 262
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v8}, Lcom/google/android/videos/utils/ViewUtil;->setRippleViewParentVisibility(Landroid/view/ViewGroup;I)V

    .line 274
    :cond_1
    :goto_0
    return-void

    .line 263
    :cond_2
    const/4 v0, 0x2

    if-ne p3, v0, :cond_1

    .line 264
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0, v8}, Lcom/google/android/videos/tagging/TagsView;->setVisibility(I)V

    .line 265
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-static {v0, v7}, Lcom/google/android/videos/utils/ViewUtil;->setRippleViewParentVisibility(Landroid/view/ViewGroup;I)V

    .line 266
    invoke-virtual {p0, v7}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setVisibility(I)V

    .line 267
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->setFallbackGestureListener(Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V

    .line 268
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    invoke-virtual {v0}, Landroid/content/res/Resources;->getConfiguration()Landroid/content/res/Configuration;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->onConfigurationChanged(Landroid/content/res/Configuration;)V

    .line 269
    if-eqz p1, :cond_1

    .line 270
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v1

    invoke-virtual {v0, p1, p2, v1}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->init(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;Lcom/google/android/videos/Config;)V

    goto :goto_0
.end method

.method public isCardListExpanded()Z
    .locals 1

    .prologue
    .line 497
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->isExpanded()Z

    move-result v0

    return v0
.end method

.method public isInteracting()Z
    .locals 1

    .prologue
    .line 502
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->isCardListExpanded()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isVisible()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onActorClick(Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/view/View;)V
    .locals 7
    .param p1, "actor"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .param p2, "profileImageView"    # Landroid/view/View;

    .prologue
    .line 424
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    .line 425
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onExpandRecentActors()V

    .line 427
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    const/4 v3, 0x0

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v6, v4, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    move-object v4, p2

    move-object v5, p1

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->show(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 429
    return-void
.end method

.method public onAllCardsDismissed()V
    .locals 2

    .prologue
    .line 444
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(I)V

    .line 445
    return-void
.end method

.method public onBackPressed()Z
    .locals 3

    .prologue
    const/4 v0, 0x1

    .line 508
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 509
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hide(Z)V

    .line 524
    :cond_0
    :goto_0
    return v0

    .line 511
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->isVisible()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 512
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hide(Z)V

    goto :goto_0

    .line 514
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 515
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    .line 517
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->isExpanded()Z

    move-result v1

    if-eqz v1, :cond_4

    .line 518
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    const/4 v2, 0x4

    invoke-virtual {v1, v2}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->collapse(I)V

    .line 519
    iget v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 520
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsView;->clear()V

    goto :goto_0

    .line 524
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onCardDismissed(Landroid/view/View;)V
    .locals 2
    .param p1, "card"    # Landroid/view/View;

    .prologue
    .line 433
    invoke-virtual {p1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    .line 434
    .local v0, "viewTag":Ljava/lang/Object;
    instance-of v1, v0, Lcom/google/android/videos/tagging/CardTag;

    if-nez v1, :cond_1

    .line 440
    .end local v0    # "viewTag":Ljava/lang/Object;
    :cond_0
    :goto_0
    return-void

    .line 437
    .restart local v0    # "viewTag":Ljava/lang/Object;
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v1, :cond_0

    .line 438
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    check-cast v0, Lcom/google/android/videos/tagging/CardTag;

    .end local v0    # "viewTag":Ljava/lang/Object;
    invoke-interface {v1, v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardDismissed(Lcom/google/android/videos/tagging/CardTag;)V

    goto :goto_0
.end method

.method public onCardsViewScrollChanged(I)V
    .locals 1
    .param p1, "verticalScrollOrigin"    # I

    .prologue
    .line 449
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    .line 450
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardsViewScrollChanged(I)V

    .line 452
    :cond_0
    return-void
.end method

.method protected onConfigurationChanged(Landroid/content/res/Configuration;)V
    .locals 4
    .param p1, "newConfig"    # Landroid/content/res/Configuration;

    .prologue
    .line 206
    iget v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->configuredOrientation:I

    iget v3, p1, Landroid/content/res/Configuration;->orientation:I

    if-eq v2, v3, :cond_0

    .line 207
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f3

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    .line 209
    .local v0, "paddingBottom":I
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0e01f4

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v1

    .line 211
    .local v1, "paddingLeftRight":I
    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingTop()I

    move-result v3

    invoke-virtual {v2, v1, v3, v1, v0}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 213
    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->onOrientationChange()V

    .line 214
    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->onOrientationChange()V

    .line 215
    iget v2, p1, Landroid/content/res/Configuration;->orientation:I

    iput v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->configuredOrientation:I

    .line 217
    .end local v0    # "paddingBottom":I
    .end local v1    # "paddingLeftRight":I
    :cond_0
    return-void
.end method

.method public onEntityClick(Lcom/google/android/videos/tagging/KnowledgeEntity;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 7
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p2, "frame"    # Landroid/view/ViewGroup;
    .param p3, "image"    # Landroid/view/View;

    .prologue
    .line 473
    if-eqz p1, :cond_0

    .line 474
    instance-of v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v0, :cond_1

    .line 475
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    move-object v5, p1

    check-cast v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v6, v3, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->show(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 482
    :cond_0
    :goto_0
    return-void

    .line 478
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    move-object v5, p1

    check-cast v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v6, v3, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    move-object v3, p2

    move-object v4, p3

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->show(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/async/Requester;)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 418
    const-string v0, "Error sending knowledge feedback"

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 419
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardFeedbackReport(Z)V

    .line 420
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->onError(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Exception;)V

    return-void
.end method

.method public onExtendedProfileVisibilityChanged(Landroid/view/View;Z)V
    .locals 4
    .param p1, "extendedProfileView"    # Landroid/view/View;
    .param p2, "visible"    # Z

    .prologue
    const/16 v1, 0x8

    const/4 v2, 0x0

    .line 624
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v3, 0x1

    if-ne v0, v3, :cond_0

    .line 626
    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    if-eqz p2, :cond_2

    move v0, v1

    :goto_0
    invoke-static {v3, v0}, Lcom/google/android/videos/utils/ViewUtil;->setRippleViewParentVisibility(Landroid/view/ViewGroup;I)V

    .line 627
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    if-eqz p2, :cond_3

    :goto_1
    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/TagsView;->setVisibility(I)V

    .line 629
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v0, :cond_1

    .line 630
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v0, p2}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onEntityExpandingStateChanged(Z)V

    .line 632
    :cond_1
    return-void

    :cond_2
    move v0, v2

    .line 626
    goto :goto_0

    :cond_3
    move v1, v2

    .line 627
    goto :goto_1
.end method

.method public onFeedbackButtonClick(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 4
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 386
    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 399
    :cond_0
    :goto_0
    return-void

    .line 389
    :cond_1
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 390
    .local v1, "taggedKnowledgeEntitiesForFeedback":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v3

    if-ge v0, v3, :cond_3

    .line 391
    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .line 392
    .local v2, "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    iget-object v3, v2, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    invoke-virtual {v3, p1}, Ljava/lang/Object;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 393
    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 390
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .line 396
    .end local v2    # "taggedKnowledgeEntity":Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
    :cond_3
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_0

    .line 397
    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v3, v1}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->collectFeedback(Ljava/util/List;)V

    goto :goto_0
.end method

.method public onFeedbackCollected(Ljava/util/List;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 403
    .local p1, "incorrectlyTaggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;

    iget v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackProductId:I

    iget v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeFeedbackTypeId:I

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v4, v4, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    iget v5, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->showKnowledgeAtMillis:I

    move-object v6, p1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;-><init>(IILjava/lang/String;Lcom/google/android/videos/tagging/TagStream;ILjava/util/List;)V

    .line 406
    .local v0, "report":Lcom/google/android/videos/tagging/KnowledgeFeedbackReport;
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackClient:Lcom/google/android/videos/tagging/FeedbackClient;

    invoke-interface {v1, v0, p0}, Lcom/google/android/videos/tagging/FeedbackClient;->sendFeedback(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Lcom/google/android/videos/async/Callback;)V

    .line 407
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    const v2, 0x7f0b01e9

    const/4 v3, 0x0

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 408
    return-void
.end method

.method public onInterceptTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 570
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onInterceptTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onResponse(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Void;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 412
    const-string v0, "Knowledge feedback sent."

    invoke-static {v0}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 413
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const/4 v1, 0x1

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onInfoCardFeedbackReport(Z)V

    .line 414
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 49
    check-cast p1, Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->onResponse(Lcom/google/android/videos/tagging/FeedbackClient$FeedbackReport;Ljava/lang/Void;)V

    return-void
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "w"    # I
    .param p2, "h"    # I
    .param p3, "oldw"    # I
    .param p4, "oldh"    # I

    .prologue
    .line 558
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/FrameLayout;->onSizeChanged(IIII)V

    .line 559
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->updateMaxTranslationY()V

    .line 560
    return-void
.end method

.method public onTagClick(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;)V
    .locals 3
    .param p1, "taggedKnowledgeEntity"    # Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    .prologue
    .line 456
    if-eqz p1, :cond_2

    .line 457
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->contains(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 458
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    iget-object v0, p1, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v1, p0, v0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->show(Landroid/view/ViewGroup;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 468
    :cond_0
    :goto_0
    return-void

    .line 460
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    .line 461
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onClickOutsideTags()V

    goto :goto_0

    .line 463
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->isCollectingFeedback()Z

    move-result v0

    if-eqz v0, :cond_3

    .line 464
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->reset()V

    goto :goto_0

    .line 465
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v0, :cond_0

    .line 466
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onClickOutsideTags()V

    goto :goto_0
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 576
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->onTouchEvent(Landroid/view/MotionEvent;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public reset()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 348
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hide(Z)V

    .line 349
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->hide(Z)V

    .line 350
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 351
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->entitiesTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 352
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    invoke-virtual {v0}, Landroid/widget/FrameLayout;->removeAllViews()V

    .line 353
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsTransitionRoot:Landroid/widget/FrameLayout;

    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0, v1}, Landroid/widget/FrameLayout;->addView(Landroid/view/View;)V

    .line 354
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->reset()V

    .line 355
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/CardsView;->clear()V

    .line 356
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clear()V

    .line 357
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 358
    return-void
.end method

.method public setContentVisible(Z)V
    .locals 3
    .param p1, "visible"    # Z

    .prologue
    .line 487
    if-eqz p1, :cond_1

    const/4 v0, 0x0

    .line 488
    .local v0, "visibility":I
    :goto_0
    iget v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_2

    .line 489
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/TagsView;->setVisibility(I)V

    .line 490
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-static {v1, v0}, Lcom/google/android/videos/utils/ViewUtil;->setRippleViewParentVisibility(Landroid/view/ViewGroup;I)V

    .line 494
    :cond_0
    :goto_1
    return-void

    .line 487
    .end local v0    # "visibility":I
    :cond_1
    const/16 v0, 0x8

    goto :goto_0

    .line 491
    .restart local v0    # "visibility":I
    :cond_2
    iget v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    .line 492
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v0}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_1
.end method

.method public setFallbackGestureListener(Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;)V
    .locals 0
    .param p1, "horizontalGestureListener"    # Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .prologue
    .line 225
    iput-object p1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->horizontalGestureListener:Lcom/google/android/videos/ui/GestureDetectorPlus$OnGestureListener;

    .line 226
    return-void
.end method

.method public setListener(Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    .prologue
    .line 220
    iput-object p1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    .line 221
    return-void
.end method

.method public setPadding(IIII)V
    .locals 5
    .param p1, "left"    # I
    .param p2, "top"    # I
    .param p3, "right"    # I
    .param p4, "bottom"    # I

    .prologue
    .line 529
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    if-nez v1, :cond_1

    .line 554
    :cond_0
    :goto_0
    return-void

    .line 536
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    const/4 v2, 0x0

    invoke-virtual {v1, p1, v2, p3, p4}, Lcom/google/android/videos/tagging/CardsView;->setPadding(IIII)V

    .line 539
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v1, p2}, Lcom/google/android/videos/tagging/CardsView;->setTopInset(I)V

    .line 540
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsViewAnimationHelper:Lcom/google/android/videos/tagging/CardsViewAnimationHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/CardsViewAnimationHelper;->updateMaxTranslationY()V

    .line 541
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->feedbackViewHelper:Lcom/google/android/videos/tagging/FeedbackViewHelper;

    invoke-virtual {v1, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/FeedbackViewHelper;->setPadding(IIII)V

    .line 542
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2}, Landroid/support/v7/widget/RecyclerView;->getPaddingLeft()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v3}, Landroid/support/v7/widget/RecyclerView;->getPaddingRight()I

    move-result v3

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v4}, Landroid/support/v7/widget/RecyclerView;->getPaddingBottom()I

    move-result v4

    invoke-virtual {v1, v2, p2, v3, v4}, Landroid/support/v7/widget/RecyclerView;->setPadding(IIII)V

    .line 544
    if-eqz p2, :cond_0

    .line 547
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e017a

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v0

    .line 549
    .local v0, "actionBarHeight":I
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getPaddingLeft()I

    move-result v2

    sub-int v3, p2, v0

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, p4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->setPadding(IIII)V

    .line 551
    iget-object v1, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->songProfileView:Lcom/google/android/videos/tagging/ExtendedSongProfileView;

    iget-object v2, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getPaddingLeft()I

    move-result v2

    sub-int v3, p2, v0

    iget-object v4, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->actorProfileView:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getPaddingRight()I

    move-result v4

    invoke-virtual {v1, v2, v3, v4, p4}, Lcom/google/android/videos/tagging/ExtendedSongProfileView;->setPadding(IIII)V

    goto :goto_0
.end method

.method public showPausedKnowledge(IIILjava/util/List;I)V
    .locals 18
    .param p1, "timeMillis"    # I
    .param p2, "videoDisplayWidth"    # I
    .param p3, "videoDisplayHeight"    # I
    .param p5, "showRecentActorsWithinMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 279
    .local p4, "taggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v4, 0x1

    if-ne v3, v4, :cond_8

    .line 280
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    invoke-interface {v3}, Ljava/util/List;->clear()V

    .line 281
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->taggedKnowledgeEntities:Ljava/util/List;

    move-object/from16 v0, p4

    invoke-interface {v3, v0}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    .line 282
    move/from16 v0, p1

    move-object/from16 v1, p0

    iput v0, v1, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->showKnowledgeAtMillis:I

    .line 284
    if-lez p2, :cond_1

    if-lez p3, :cond_1

    const/16 v17, 0x1

    .line 285
    .local v17, "showTags":Z
    :goto_0
    if-eqz v17, :cond_2

    invoke-interface/range {p4 .. p4}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-nez v3, :cond_2

    .line 286
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    move-object/from16 v0, p4

    move/from16 v1, p2

    move/from16 v2, p3

    invoke-virtual {v3, v0, v1, v2}, Lcom/google/android/videos/tagging/TagsView;->show(Ljava/util/List;II)V

    .line 291
    :goto_1
    new-instance v10, Ljava/util/ArrayList;

    invoke-direct {v10}, Ljava/util/ArrayList;-><init>()V

    .line 292
    .local v10, "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    new-instance v15, Ljava/util/HashSet;

    invoke-direct {v15}, Ljava/util/HashSet;-><init>()V

    .line 293
    .local v15, "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    const/4 v13, 0x0

    .local v13, "i":I
    :goto_2
    invoke-interface/range {p4 .. p4}, Ljava/util/List;->size()I

    move-result v3

    if-ge v13, v3, :cond_3

    .line 294
    move-object/from16 v0, p4

    invoke-interface {v0, v13}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;

    iget-object v14, v3, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 295
    .local v14, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    instance-of v3, v14, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    if-eqz v3, :cond_0

    iget v3, v14, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v3

    invoke-interface {v15, v3}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_0

    .line 297
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    move-object v4, v14

    check-cast v4, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    invoke-virtual/range {v3 .. v10}, Lcom/google/android/videos/tagging/KnowledgeBundle;->inflateSongCards(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;Ljava/util/Collection;)V

    .line 293
    :cond_0
    add-int/lit8 v13, v13, 0x1

    goto :goto_2

    .line 284
    .end local v10    # "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v13    # "i":I
    .end local v14    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    .end local v15    # "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v17    # "showTags":Z
    :cond_1
    const/16 v17, 0x0

    goto :goto_0

    .line 288
    .restart local v17    # "showTags":Z
    :cond_2
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/TagsView;->clear()V

    goto :goto_1

    .line 303
    .restart local v10    # "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .restart local v13    # "i":I
    .restart local v15    # "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    :cond_3
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    move/from16 v4, p1

    move-object v5, v15

    move-object/from16 v7, p0

    invoke-virtual/range {v3 .. v8}, Lcom/google/android/videos/tagging/KnowledgeBundle;->inflateCurrentActorsCard(ILjava/util/Set;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Landroid/app/Activity;)Landroid/view/View;

    move-result-object v11

    .line 305
    .local v11, "currentActorsCard":Landroid/view/View;
    if-eqz v11, :cond_4

    .line 306
    invoke-interface {v10, v11}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 308
    :cond_4
    const/4 v12, 0x0

    .line 309
    .local v12, "hasRecentActorsCard":Z
    if-lez p5, :cond_5

    .line 310
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    const/4 v4, 0x0

    sub-int v5, p1, p5

    invoke-static {v4, v5}, Ljava/lang/Math;->max(II)I

    move-result v4

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->activity:Landroid/app/Activity;

    move/from16 v5, p1

    move-object v6, v15

    move-object/from16 v8, p0

    invoke-virtual/range {v3 .. v9}, Lcom/google/android/videos/tagging/KnowledgeBundle;->inflateRecentActorsCard(IILjava/util/Set;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Landroid/app/Activity;)Landroid/view/View;

    move-result-object v16

    .line 313
    .local v16, "recentActorsCard":Landroid/view/View;
    if-eqz v16, :cond_5

    .line 314
    const/4 v12, 0x1

    .line 315
    move-object/from16 v0, v16

    invoke-interface {v10, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 318
    .end local v16    # "recentActorsCard":Landroid/view/View;
    :cond_5
    invoke-interface {v10}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_7

    .line 319
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->clearKnowledge()V

    .line 330
    .end local v10    # "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v11    # "currentActorsCard":Landroid/view/View;
    .end local v12    # "hasRecentActorsCard":Z
    .end local v13    # "i":I
    .end local v15    # "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v17    # "showTags":Z
    :cond_6
    :goto_3
    return-void

    .line 321
    .restart local v10    # "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .restart local v11    # "currentActorsCard":Landroid/view/View;
    .restart local v12    # "hasRecentActorsCard":Z
    .restart local v13    # "i":I
    .restart local v15    # "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .restart local v17    # "showTags":Z
    :cond_7
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->cardsView:Lcom/google/android/videos/tagging/CardsView;

    invoke-virtual {v3, v10}, Lcom/google/android/videos/tagging/CardsView;->show(Ljava/util/List;)V

    .line 322
    const/4 v3, 0x0

    move-object/from16 v0, p0

    invoke-virtual {v0, v3}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->setVisibility(I)V

    .line 323
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    if-eqz v3, :cond_6

    .line 324
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->listener:Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;

    invoke-interface {v3, v12}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;->onCardsShown(Z)V

    goto :goto_3

    .line 327
    .end local v10    # "cards":Ljava/util/List;, "Ljava/util/List<Landroid/view/View;>;"
    .end local v11    # "currentActorsCard":Landroid/view/View;
    .end local v12    # "hasRecentActorsCard":Z
    .end local v13    # "i":I
    .end local v15    # "localIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .end local v17    # "showTags":Z
    :cond_8
    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v4, 0x2

    if-ne v3, v4, :cond_6

    .line 328
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    invoke-virtual {v3}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->pauseKnowledge()V

    goto :goto_3
.end method

.method public showPlayingKnowledge()V
    .locals 2

    .prologue
    .line 334
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_1

    .line 335
    invoke-direct {p0}, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->clearKnowledge()V

    .line 339
    :cond_0
    :goto_0
    return-void

    .line 336
    :cond_1
    iget v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->mode:I

    const/4 v1, 0x2

    if-ne v0, v1, :cond_0

    .line 337
    iget-object v0, p0, Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;->secondScreenEntitiesPlayer:Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/SecondScreenEntitiesPlayer;->maybeStartOrResume()V

    goto :goto_0
.end method
