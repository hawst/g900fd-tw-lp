.class Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "AssetStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/AssetStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DeleteOrphanedMetadataTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/AssetStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p2, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 1555
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    .line 1556
    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/AssetStoreSync;->access$200(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 1557
    return-void
.end method


# virtual methods
.method protected doSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 1561
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 1562
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 1564
    .local v0, "success":Z
    :try_start_0
    const-string v2, "shows"

    const-string v3, "(NOT EXISTS (SELECT NULL FROM purchased_assets WHERE purchase_status != -1 AND asset_type IN (19, 20) AND root_asset_id IS shows_id)) AND (NOT EXISTS (SELECT NULL FROM wishlist WHERE wishlist_item_type = 18 AND wishlist_item_state != 3 AND wishlist_item_id = shows_id))"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1565
    const-string v2, "seasons"

    const-string v3, "NOT EXISTS (SELECT NULL FROM shows WHERE shows_id = show_id)"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1566
    const-string v2, "videos"

    const-string v3, "(NOT EXISTS (SELECT NULL FROM seasons WHERE episode_season_id IS season_id)) AND (NOT EXISTS (SELECT NULL FROM purchased_assets WHERE purchase_status != -1 AND asset_type IN (6,20) AND asset_id = video_id)) AND (NOT EXISTS (SELECT NULL FROM wishlist WHERE wishlist_item_type = 6 AND wishlist_item_state != 3 AND wishlist_item_id = video_id) )"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 1567
    const-string v2, "assets"

    const-string v3, "CASE assets_type WHEN 6 THEN NOT EXISTS (SELECT NULL FROM videos WHERE video_id = assets_id) WHEN 20 THEN NOT EXISTS (SELECT NULL FROM videos WHERE video_id = assets_id) WHEN 19 THEN NOT EXISTS (SELECT NULL FROM seasons WHERE season_id = assets_id) WHEN 18 THEN NOT EXISTS (SELECT NULL FROM shows WHERE shows_id = assets_id) ELSE 1 END"

    const/4 v4, 0x0

    invoke-virtual {v1, v2, v3, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 1568
    const/4 v0, 0x1

    .line 1570
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1571
    if-eqz v0, :cond_0

    .line 1572
    iget-object v2, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    iget v3, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v4, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->removeOrphanedBitmaps(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    invoke-static {v2, v3, v4}, Lcom/google/android/videos/store/AssetStoreSync;->access$2000(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 1575
    :cond_0
    return-void

    .line 1570
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    # getter for: Lcom/google/android/videos/store/AssetStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/AssetStoreSync;->access$900(Lcom/google/android/videos/store/AssetStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v5, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 1571
    if-eqz v0, :cond_1

    .line 1572
    iget-object v3, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->this$0:Lcom/google/android/videos/store/AssetStoreSync;

    iget v4, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->priority:I

    iget-object v5, p0, Lcom/google/android/videos/store/AssetStoreSync$DeleteOrphanedMetadataTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    # invokes: Lcom/google/android/videos/store/AssetStoreSync;->removeOrphanedBitmaps(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    invoke-static {v3, v4, v5}, Lcom/google/android/videos/store/AssetStoreSync;->access$2000(Lcom/google/android/videos/store/AssetStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    :cond_1
    throw v2
.end method
