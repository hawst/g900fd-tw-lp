.class public Lcom/google/android/videos/streams/ItagInfo;
.super Ljava/lang/Object;
.source "ItagInfo.java"


# instance fields
.field public final audioChannels:I

.field public final drmType:I

.field public final height:I

.field public final isDash:Z

.field public final isMulti:Z

.field public final width:I


# direct methods
.method public constructor <init>(IIIZIZ)V
    .locals 0
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "audioChannels"    # I
    .param p4, "isDash"    # Z
    .param p5, "drmType"    # I
    .param p6, "isMulti"    # Z

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 25
    iput p1, p0, Lcom/google/android/videos/streams/ItagInfo;->width:I

    .line 26
    iput p2, p0, Lcom/google/android/videos/streams/ItagInfo;->height:I

    .line 27
    iput p3, p0, Lcom/google/android/videos/streams/ItagInfo;->audioChannels:I

    .line 28
    iput-boolean p4, p0, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    .line 29
    iput p5, p0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    .line 30
    iput-boolean p6, p0, Lcom/google/android/videos/streams/ItagInfo;->isMulti:Z

    .line 31
    return-void
.end method


# virtual methods
.method public getQuality()I
    .locals 2

    .prologue
    .line 41
    iget v0, p0, Lcom/google/android/videos/streams/ItagInfo;->height:I

    const/16 v1, 0x2d0

    if-ge v0, v1, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public isSurroundSound()Z
    .locals 2

    .prologue
    .line 34
    iget v0, p0, Lcom/google/android/videos/streams/ItagInfo;->audioChannels:I

    const/4 v1, 0x3

    if-le v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 46
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[width="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/streams/ItagInfo;->width:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", height="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/streams/ItagInfo;->height:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", audioChannels="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/streams/ItagInfo;->audioChannels:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isDash="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", drmType="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", isMulti="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/streams/ItagInfo;->isMulti:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    const/16 v1, 0x5d

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(C)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
