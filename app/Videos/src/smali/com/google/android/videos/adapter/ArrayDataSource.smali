.class public Lcom/google/android/videos/adapter/ArrayDataSource;
.super Lcom/google/android/videos/adapter/AbstractDataSource;
.source "ArrayDataSource.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/adapter/AbstractDataSource",
        "<TT;>;"
    }
.end annotation


# instance fields
.field private data:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field private doneInitialNotification:Z


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    invoke-direct {p0}, Lcom/google/android/videos/adapter/AbstractDataSource;-><init>()V

    return-void
.end method


# virtual methods
.method public getAll()[Ljava/lang/Object;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()[TT;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 28
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    array-length v0, v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getItem(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)TT;"
        }
    .end annotation

    .prologue
    .line 20
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    if-eqz v0, :cond_0

    const/4 v0, -0x1

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    array-length v0, v0

    if-ge p1, v0, :cond_0

    .line 21
    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    aget-object v0, v0, p1

    return-object v0

    .line 23
    :cond_0
    new-instance v0, Ljava/lang/IndexOutOfBoundsException;

    invoke-direct {v0}, Ljava/lang/IndexOutOfBoundsException;-><init>()V

    throw v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 53
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/ArrayDataSource;->getItem(I)Ljava/lang/Object;

    move-result-object v0

    .line 54
    .local v0, "item":Ljava/lang/Object;, "TT;"
    instance-of v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-eqz v1, :cond_0

    .line 55
    check-cast v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .end local v0    # "item":Ljava/lang/Object;, "TT;"
    iget-object v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetResourceId(Lcom/google/wireless/android/video/magma/proto/AssetResourceId;)Ljava/lang/String;

    move-result-object v0

    .line 57
    :cond_0
    return-object v0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 36
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/ArrayDataSource;->updateArray([Ljava/lang/Object;)V

    .line 37
    return-void
.end method

.method public updateArray([Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([TT;)V"
        }
    .end annotation

    .prologue
    .line 44
    .local p0, "this":Lcom/google/android/videos/adapter/ArrayDataSource;, "Lcom/google/android/videos/adapter/ArrayDataSource<TT;>;"
    .local p1, "newData":[Ljava/lang/Object;, "[TT;"
    iget-boolean v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->doneInitialNotification:Z

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    invoke-static {v0, p1}, Ljava/util/Arrays;->equals([Ljava/lang/Object;[Ljava/lang/Object;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 45
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->doneInitialNotification:Z

    .line 46
    iput-object p1, p0, Lcom/google/android/videos/adapter/ArrayDataSource;->data:[Ljava/lang/Object;

    .line 47
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/ArrayDataSource;->notifyChanged()V

    .line 49
    :cond_1
    return-void
.end method
