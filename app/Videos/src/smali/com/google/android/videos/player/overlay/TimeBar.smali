.class public Lcom/google/android/videos/player/overlay/TimeBar;
.super Landroid/view/View;
.source "TimeBar.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/TimeBar$Listener;
    }
.end annotation


# static fields
.field private static final STATE_DISABLED:[I

.field private static final STATE_ENABLED:[I

.field private static final STATE_PRESSED:[I


# instance fields
.field private buffered:I

.field private final bufferedBar:Landroid/graphics/Rect;

.field private final bufferedPaint:Landroid/graphics/Paint;

.field private currentScrubberRadius:F

.field private durationMillis:I

.field private durationTextView:Landroid/widget/TextView;

.field private final durationTextViewId:I

.field private final enableSnapDistance:I

.field private isScrubbing:Z

.field private listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

.field private final pressedScrubberRadius:I

.field private previousSnapDirection:F

.field private previousTouchX:I

.field private final progressBar:Landroid/graphics/Rect;

.field private progressMillis:I

.field private final progressPaint:Landroid/graphics/Paint;

.field private progressTextView:Landroid/widget/TextView;

.field private final progressTextViewId:I

.field private final rail:Landroid/graphics/Rect;

.field private final railHeight:I

.field private final railPaint:Landroid/graphics/Paint;

.field private screenDensity:F

.field private final scrubber:Landroid/graphics/drawable/StateListDrawable;

.field private scrubberAnimationStartUptimeMillis:J

.field private scrubberAnimationState:I

.field private scrubberCenterX:I

.field private scrubberCenterY:I

.field private final scrubberPadding:I

.field private final scrubberRadius:I

.field private final scrubberRegion:Landroid/graphics/Rect;

.field private scrubberTimeMillis:I

.field private snapEnabled:Z


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 51
    new-array v0, v3, [I

    const v1, 0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_ENABLED:[I

    .line 52
    new-array v0, v3, [I

    const v1, -0x101009e

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_DISABLED:[I

    .line 53
    new-array v0, v3, [I

    const v1, 0x10100a7

    aput v1, v0, v2

    sput-object v0, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_PRESSED:[I

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 112
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 113
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 116
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/player/overlay/TimeBar;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 117
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 10
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v4, 0x0

    const-wide/high16 v8, 0x3fe0000000000000L    # 0.5

    const/4 v6, -0x1

    .line 120
    invoke-direct {p0, p1, p2, p3}, Landroid/view/View;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 94
    iput v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 107
    iput v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    .line 108
    iput v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    .line 109
    iput v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->buffered:I

    .line 122
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railPaint:Landroid/graphics/Paint;

    .line 123
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    .line 124
    new-instance v2, Landroid/graphics/Paint;

    invoke-direct {v2}, Landroid/graphics/Paint;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    .line 126
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    invoke-virtual {v2}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v1

    .line 127
    .local v1, "metrics":Landroid/util/DisplayMetrics;
    sget-object v2, Lcom/google/android/videos/R$styleable;->TimeBar:[I

    invoke-virtual {p1, p2, v2}, Landroid/content/Context;->obtainStyledAttributes(Landroid/util/AttributeSet;[I)Landroid/content/res/TypedArray;

    move-result-object v0

    .line 128
    .local v0, "a":Landroid/content/res/TypedArray;
    const/4 v2, 0x2

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextViewId:I

    .line 129
    const/4 v2, 0x1

    invoke-virtual {v0, v2, v6}, Landroid/content/res/TypedArray;->getResourceId(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextViewId:I

    .line 130
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40a00000    # 5.0f

    mul-float/2addr v2, v3

    float-to-double v2, v2

    add-double/2addr v2, v8

    double-to-int v2, v2

    invoke-virtual {v0, v4, v2}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railHeight:I

    .line 132
    const/4 v2, 0x6

    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x41400000    # 12.0f

    mul-float/2addr v3, v4

    float-to-double v4, v3

    add-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    .line 134
    const/4 v2, 0x7

    iget v3, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v4, 0x41c00000    # 24.0f

    mul-float/2addr v3, v4

    float-to-double v4, v3

    add-double/2addr v4, v8

    double-to-int v3, v4

    invoke-virtual {v0, v2, v3}, Landroid/content/res/TypedArray;->getDimensionPixelSize(II)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    .line 136
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railPaint:Landroid/graphics/Paint;

    const/4 v3, 0x3

    const v4, -0x7f7f80

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 137
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    const/4 v3, 0x4

    invoke-virtual {v0, v3, v6}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 138
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    const/4 v3, 0x5

    const v4, -0xbfc0

    invoke-virtual {v0, v3, v4}, Landroid/content/res/TypedArray;->getColor(II)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/graphics/Paint;->setColor(I)V

    .line 139
    invoke-virtual {v0}, Landroid/content/res/TypedArray;->recycle()V

    .line 141
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    .line 142
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    .line 143
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    .line 144
    new-instance v2, Landroid/graphics/Rect;

    invoke-direct {v2}, Landroid/graphics/Rect;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRegion:Landroid/graphics/Rect;

    .line 146
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0201d2

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    check-cast v2, Landroid/graphics/drawable/StateListDrawable;

    iput-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    .line 147
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    sget-object v3, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_DISABLED:[I

    invoke-virtual {v2, v3}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 148
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x40c00000    # 6.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberPadding:I

    .line 149
    iget v2, v1, Landroid/util/DisplayMetrics;->density:F

    const/high16 v3, 0x41600000    # 14.0f

    mul-float/2addr v2, v3

    float-to-int v2, v2

    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->enableSnapDistance:I

    .line 150
    return-void
.end method

.method private accessibilityAnnounceScrubberTime(Z)V
    .locals 3
    .param p1, "immediately"    # Z

    .prologue
    .line 674
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/AccessibilityUtils;->isAccessibilityEnabled(Landroid/content/Context;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 686
    :goto_0
    return-void

    .line 678
    :cond_0
    if-nez p1, :cond_1

    .line 679
    const-wide/16 v0, 0xfa0

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0

    .line 681
    :cond_1
    invoke-virtual {p0, p0}, Lcom/google/android/videos/player/overlay/TimeBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 683
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v2

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/TimeUtil;->getAccessibilityString(Landroid/content/Context;I)Ljava/lang/String;

    move-result-object v1

    invoke-static {v0, v1, p0}, Lcom/google/android/videos/utils/AccessibilityUtils;->sendAccessibilityEventWithText(Landroid/content/Context;Ljava/lang/String;Landroid/view/View;)V

    goto :goto_0
.end method

.method private findTextView(I)Landroid/widget/TextView;
    .locals 4
    .param p1, "id"    # I

    .prologue
    const/4 v2, 0x0

    .line 187
    const/4 v3, -0x1

    if-ne p1, v3, :cond_0

    move-object v1, v2

    .line 200
    :goto_0
    return-object v1

    .line 191
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getRootView()Landroid/view/View;

    move-result-object v0

    .line 192
    .local v0, "root":Landroid/view/View;
    if-nez v0, :cond_1

    move-object v1, v2

    .line 193
    goto :goto_0

    .line 196
    :cond_1
    invoke-virtual {v0, p1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 197
    .local v1, "view":Landroid/view/View;
    instance-of v3, v1, Landroid/widget/TextView;

    if-eqz v3, :cond_2

    .line 198
    check-cast v1, Landroid/widget/TextView;

    goto :goto_0

    :cond_2
    move-object v1, v2

    .line 200
    goto :goto_0
.end method

.method private inScrubberRegion(FFI)Z
    .locals 6
    .param p1, "x"    # F
    .param p2, "y"    # F
    .param p3, "slop"    # I

    .prologue
    .line 563
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRegion:Landroid/graphics/Rect;

    new-instance v1, Landroid/graphics/Rect;

    float-to-int v2, p1

    sub-int/2addr v2, p3

    float-to-int v3, p2

    sub-int/2addr v3, p3

    float-to-int v4, p1

    add-int/2addr v4, p3

    float-to-int v5, p2

    add-int/2addr v5, p3

    invoke-direct {v1, v2, v3, v4, v5}, Landroid/graphics/Rect;-><init>(IIII)V

    invoke-static {v0, v1}, Landroid/graphics/Rect;->intersects(Landroid/graphics/Rect;Landroid/graphics/Rect;)Z

    move-result v0

    return v0
.end method

.method private isScrubbingEnabled()Z
    .locals 1

    .prologue
    .line 572
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setBartimeEnabled(Z)V
    .locals 1
    .param p1, "enabled"    # Z

    .prologue
    .line 639
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 640
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 642
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 643
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setEnabled(Z)V

    .line 645
    :cond_1
    return-void
.end method

.method private setScrubberPosition(I)V
    .locals 13
    .param p1, "touchX"    # I

    .prologue
    const/4 v12, 0x0

    const/4 v5, 0x1

    const/4 v6, 0x0

    .line 579
    iget-boolean v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v7, :cond_2

    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v7, :cond_2

    .line 580
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v7, v7, Landroid/graphics/Rect;->left:I

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v8}, Landroid/graphics/Rect;->width()I

    move-result v8

    int-to-long v8, v8

    iget v10, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    int-to-long v10, v10

    mul-long/2addr v8, v10

    iget v10, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-long v10, v10

    div-long/2addr v8, v10

    long-to-int v8, v8

    add-int v4, v7, v8

    .line 581
    .local v4, "scrubberX":I
    sub-int v7, p1, v4

    invoke-static {v7}, Ljava/lang/Math;->abs(I)I

    move-result v1

    .line 582
    .local v1, "distanceFromScrubberX":I
    sub-int v7, p1, v4

    int-to-float v7, v7

    invoke-static {v7}, Ljava/lang/Math;->signum(F)F

    move-result v0

    .line 583
    .local v0, "directionFromScrubberX":F
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousTouchX:I

    sub-int v7, p1, v7

    int-to-float v7, v7

    invoke-static {v7}, Ljava/lang/Math;->signum(F)F

    move-result v7

    cmpl-float v7, v0, v7

    if-nez v7, :cond_3

    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousSnapDirection:F

    cmpl-float v7, v0, v7

    if-eqz v7, :cond_3

    move v2, v5

    .line 585
    .local v2, "movingAwayFromStart":Z
    :goto_0
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberPadding:I

    if-gt v1, v7, :cond_0

    if-nez v2, :cond_0

    iget-boolean v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->snapEnabled:Z

    if-nez v7, :cond_5

    .line 588
    :cond_0
    const/high16 v7, 0x3f800000    # 1.0f

    iget-object v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v8, v8, Landroid/graphics/Rect;->left:I

    sub-int v8, p1, v8

    int-to-float v8, v8

    iget-object v9, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v9}, Landroid/graphics/Rect;->width()I

    move-result v9

    int-to-float v9, v9

    div-float/2addr v8, v9

    invoke-static {v7, v8}, Ljava/lang/Math;->min(FF)F

    move-result v7

    invoke-static {v12, v7}, Ljava/lang/Math;->max(FF)F

    move-result v3

    .line 590
    .local v3, "scrubberFraction":F
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-float v7, v7

    mul-float/2addr v7, v3

    float-to-int v7, v7

    iput v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 591
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->enableSnapDistance:I

    if-le v1, v7, :cond_4

    .line 592
    iput-boolean v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->snapEnabled:Z

    .line 604
    .end local v3    # "scrubberFraction":F
    :cond_1
    :goto_1
    invoke-direct {p0, v5}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 605
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousTouchX:I

    .line 606
    invoke-direct {p0, v6}, Lcom/google/android/videos/player/overlay/TimeBar;->accessibilityAnnounceScrubberTime(Z)V

    .line 608
    .end local v0    # "directionFromScrubberX":F
    .end local v1    # "distanceFromScrubberX":I
    .end local v2    # "movingAwayFromStart":Z
    .end local v4    # "scrubberX":I
    :cond_2
    return-void

    .restart local v0    # "directionFromScrubberX":F
    .restart local v1    # "distanceFromScrubberX":I
    .restart local v4    # "scrubberX":I
    :cond_3
    move v2, v6

    .line 583
    goto :goto_0

    .line 593
    .restart local v2    # "movingAwayFromStart":Z
    .restart local v3    # "scrubberFraction":F
    :cond_4
    if-eqz v2, :cond_1

    .line 594
    iput-boolean v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->snapEnabled:Z

    .line 595
    iput v12, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousSnapDirection:F

    goto :goto_1

    .line 597
    .end local v3    # "scrubberFraction":F
    :cond_5
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    if-eq v7, v8, :cond_1

    .line 599
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    sub-int/2addr v7, v8

    int-to-float v7, v7

    invoke-static {v7}, Ljava/lang/Math;->signum(F)F

    move-result v7

    iput v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousSnapDirection:F

    .line 600
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    iput v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 601
    const/4 v7, 0x3

    const/4 v8, 0x2

    invoke-virtual {p0, v7, v8}, Lcom/google/android/videos/player/overlay/TimeBar;->performHapticFeedback(II)Z

    goto :goto_1
.end method

.method private startContractingScrubber()V
    .locals 2

    .prologue
    .line 447
    const/4 v0, 0x2

    iput v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    .line 448
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationStartUptimeMillis:J

    .line 449
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->invalidate()V

    .line 450
    return-void
.end method

.method private startExpandingScrubber()V
    .locals 2

    .prologue
    .line 441
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    .line 442
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationStartUptimeMillis:J

    .line 443
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->invalidate()V

    .line 444
    return-void
.end method

.method private stringForTime(I)Ljava/lang/String;
    .locals 4
    .param p1, "millis"    # I

    .prologue
    .line 669
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-long v0, v0

    const-wide/32 v2, 0x36ee80

    cmp-long v0, v0, v2

    if-ltz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {p1, v0}, Lcom/google/android/videos/utils/TimeUtil;->getDurationString(IZ)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private update(I)V
    .locals 4
    .param p1, "flags"    # I

    .prologue
    .line 517
    if-nez p1, :cond_0

    .line 556
    :goto_0
    return-void

    .line 521
    :cond_0
    and-int/lit8 v1, p1, 0x1

    if-eqz v1, :cond_1

    .line 522
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->updateProgressText()V

    .line 523
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v1, :cond_7

    sget-object v1, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_PRESSED:[I

    :goto_1
    invoke-virtual {v2, v1}, Landroid/graphics/drawable/StateListDrawable;->setState([I)Z

    .line 527
    :cond_1
    and-int/lit8 v1, p1, 0x8

    if-eqz v1, :cond_2

    .line 528
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->updateDurationText()V

    .line 531
    :cond_2
    and-int/lit8 v1, p1, 0x9

    if-eqz v1, :cond_3

    .line 532
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->updateAccessibilityText()V

    .line 535
    :cond_3
    and-int/lit8 v1, p1, 0xa

    if-eqz v1, :cond_4

    .line 536
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 537
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v1, :cond_4

    .line 538
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 539
    .local v0, "fraction":F
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 543
    .end local v0    # "fraction":F
    :cond_4
    and-int/lit8 v1, p1, 0x4

    if-eqz v1, :cond_5

    .line 544
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v1, v2}, Landroid/graphics/Rect;->set(Landroid/graphics/Rect;)V

    .line 545
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v1, :cond_5

    .line 546
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->buffered:I

    int-to-float v1, v1

    iget v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-float v2, v2

    div-float v0, v1, v2

    .line 547
    .restart local v0    # "fraction":F
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->left:I

    iget-object v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v3}, Landroid/graphics/Rect;->width()I

    move-result v3

    int-to-float v3, v3

    mul-float/2addr v3, v0

    float-to-int v3, v3

    add-int/2addr v2, v3

    iput v2, v1, Landroid/graphics/Rect;->right:I

    .line 551
    .end local v0    # "fraction":F
    :cond_5
    and-int/lit8 v1, p1, 0x10

    if-eqz v1, :cond_6

    .line 552
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v1, :cond_9

    const/4 v1, 0x1

    :goto_2
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->setBartimeEnabled(Z)V

    .line 555
    :cond_6
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->invalidate()V

    goto/16 :goto_0

    .line 523
    :cond_7
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbingEnabled()Z

    move-result v1

    if-eqz v1, :cond_8

    sget-object v1, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_ENABLED:[I

    goto :goto_1

    :cond_8
    sget-object v1, Lcom/google/android/videos/player/overlay/TimeBar;->STATE_DISABLED:[I

    goto :goto_1

    .line 552
    :cond_9
    const/4 v1, 0x0

    goto :goto_2
.end method

.method private updateAccessibilityText()V
    .locals 4

    .prologue
    .line 629
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 630
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/utils/TimeUtil;->getAccessibilityString(Landroid/content/Context;II)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 633
    :cond_0
    return-void
.end method

.method private updateBufferedPercentage(I)I
    .locals 3
    .param p1, "bufferedPercent"    # I

    .prologue
    .line 498
    const/4 v1, 0x0

    .line 499
    .local v1, "updateFlags":I
    iget v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    mul-int/2addr v2, p1

    div-int/lit8 v0, v2, 0x64

    .line 500
    .local v0, "buffered":I
    iget v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->buffered:I

    if-eq v2, v0, :cond_0

    .line 501
    iput v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->buffered:I

    .line 502
    or-int/lit8 v1, v1, 0x4

    .line 504
    :cond_0
    return v1
.end method

.method private updateDuration(I)I
    .locals 4
    .param p1, "durationMillis"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 458
    const/4 v0, 0x0

    .line 459
    .local v0, "updateFlags":I
    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-eq v3, p1, :cond_1

    .line 460
    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-gtz v3, :cond_2

    move v3, v1

    :goto_0
    if-gtz p1, :cond_3

    :goto_1
    if-eq v3, v1, :cond_0

    .line 461
    or-int/lit8 v0, v0, 0x10

    .line 463
    :cond_0
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    .line 464
    or-int/lit8 v0, v0, 0x8

    .line 465
    if-nez p1, :cond_1

    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v1, :cond_1

    .line 466
    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 469
    :cond_1
    return v0

    :cond_2
    move v3, v2

    .line 460
    goto :goto_0

    :cond_3
    move v1, v2

    goto :goto_1
.end method

.method private updateDurationText()V
    .locals 2

    .prologue
    .line 623
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 624
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextView:Landroid/widget/TextView;

    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 626
    :cond_0
    return-void
.end method

.method private updateProgress(I)I
    .locals 2
    .param p1, "progressMillis"    # I

    .prologue
    .line 479
    const/4 v0, 0x0

    .line 480
    .local v0, "updateFlags":I
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    if-eq v1, p1, :cond_0

    .line 481
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-nez v1, :cond_0

    .line 482
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    .line 483
    or-int/lit8 v0, v0, 0x2

    .line 484
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 485
    or-int/lit8 v0, v0, 0x1

    .line 488
    :cond_0
    return v0
.end method

.method private updateProgressText()V
    .locals 2

    .prologue
    .line 614
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 615
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->stringForTime(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 617
    :cond_0
    return-void
.end method

.method private updateScrubberRadius()V
    .locals 9

    .prologue
    const/4 v8, 0x0

    .line 368
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    if-nez v1, :cond_2

    .line 369
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v1, :cond_1

    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    int-to-float v1, v1

    :goto_0
    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    .line 388
    :cond_0
    :goto_1
    return-void

    .line 369
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    int-to-float v1, v1

    goto :goto_0

    .line 372
    :cond_2
    invoke-static {}, Landroid/os/SystemClock;->uptimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationStartUptimeMillis:J

    sub-long v2, v4, v6

    .line 373
    .local v2, "timeElapsed":J
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    sub-int/2addr v1, v4

    int-to-long v4, v1

    mul-long/2addr v4, v2

    const-wide/16 v6, 0x12c

    div-long/2addr v4, v6

    long-to-float v0, v4

    .line 375
    .local v0, "scrubberRadiusUpdate":F
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    const/4 v4, 0x1

    if-ne v1, v4, :cond_3

    .line 376
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    add-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    .line 377
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    int-to-float v4, v4

    cmpl-float v1, v1, v4

    if-ltz v1, :cond_0

    .line 378
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    .line 379
    iput v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    goto :goto_1

    .line 382
    :cond_3
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    sub-float/2addr v1, v0

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    .line 383
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    int-to-float v4, v4

    cmpg-float v1, v1, v4

    if-gtz v1, :cond_0

    .line 384
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    int-to-float v1, v1

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    .line 385
    iput v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    goto :goto_1
.end method


# virtual methods
.method public draw(Landroid/graphics/Canvas;)V
    .locals 7
    .param p1, "canvas"    # Landroid/graphics/Canvas;

    .prologue
    .line 335
    invoke-super {p0, p1}, Landroid/view/View;->draw(Landroid/graphics/Canvas;)V

    .line 336
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 337
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-gtz v1, :cond_1

    .line 365
    :cond_0
    :goto_0
    return-void

    .line 340
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-eq v1, v2, :cond_2

    .line 341
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 343
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->right:I

    if-eq v1, v2, :cond_3

    .line 345
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressBar:Landroid/graphics/Rect;

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2}, Landroid/graphics/Canvas;->drawRect(Landroid/graphics/Rect;Landroid/graphics/Paint;)V

    .line 348
    :cond_3
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v1, :cond_4

    .line 349
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 350
    .local v0, "x":I
    int-to-float v1, v0

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->exactCenterY()F

    move-result v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRadius:I

    div-int/lit8 v3, v3, 0x4

    int-to-float v3, v3

    iget-object v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->bufferedPaint:Landroid/graphics/Paint;

    invoke-virtual {p1, v1, v2, v3, v4}, Landroid/graphics/Canvas;->drawCircle(FFFLandroid/graphics/Paint;)V

    .line 354
    .end local v0    # "x":I
    :cond_4
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->updateScrubberRadius()V

    .line 355
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->left:I

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    .line 356
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v2}, Landroid/graphics/Rect;->width()I

    move-result v2

    int-to-long v2, v2

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    int-to-long v4, v4

    mul-long/2addr v2, v4

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    int-to-long v4, v4

    div-long/2addr v2, v4

    long-to-int v2, v2

    add-int/2addr v1, v2

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    .line 357
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRegion:Landroid/graphics/Rect;

    iget v1, v1, Landroid/graphics/Rect;->top:I

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRegion:Landroid/graphics/Rect;

    iget v2, v2, Landroid/graphics/Rect;->bottom:I

    add-int/2addr v1, v2

    div-int/lit8 v1, v1, 0x2

    iput v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterY:I

    .line 358
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    iget v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    int-to-float v2, v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    sub-float/2addr v2, v3

    float-to-int v2, v2

    iget v3, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterY:I

    int-to-float v3, v3

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    sub-float/2addr v3, v4

    float-to-int v3, v3

    iget v4, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    int-to-float v4, v4

    iget v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    add-float/2addr v4, v5

    float-to-int v4, v4

    iget v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterY:I

    int-to-float v5, v5

    iget v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->currentScrubberRadius:F

    add-float/2addr v5, v6

    float-to-int v5, v5

    invoke-virtual {v1, v2, v3, v4, v5}, Landroid/graphics/drawable/StateListDrawable;->setBounds(IIII)V

    .line 361
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubber:Landroid/graphics/drawable/StateListDrawable;

    invoke-virtual {v1, p1}, Landroid/graphics/drawable/StateListDrawable;->draw(Landroid/graphics/Canvas;)V

    .line 362
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberAnimationState:I

    if-eqz v1, :cond_0

    .line 363
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->invalidate()V

    goto/16 :goto_0
.end method

.method public endScrubbingAt(I)V
    .locals 2
    .param p1, "timeMillis"    # I

    .prologue
    .line 234
    iget-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-nez v1, :cond_0

    .line 247
    :goto_0
    return-void

    .line 237
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->startContractingScrubber()V

    .line 238
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    .line 239
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 240
    const/4 v0, 0x1

    .line 241
    .local v0, "updateFlags":I
    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    if-eq v1, p1, :cond_1

    .line 242
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    .line 243
    or-int/lit8 v0, v0, 0x2

    .line 245
    :cond_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 246
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->accessibilityAnnounceScrubberTime(Z)V

    goto :goto_0
.end method

.method public getBufferedPercent()I
    .locals 2

    .prologue
    .line 315
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    if-lez v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->buffered:I

    mul-int/lit8 v0, v0, 0x64

    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    div-int/2addr v0, v1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 308
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationMillis:I

    return v0
.end method

.method public getProgress()I
    .locals 1

    .prologue
    .line 301
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    return v0
.end method

.method public getScrubberCenterX()I
    .locals 1

    .prologue
    .line 330
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterX:I

    return v0
.end method

.method public getScrubberCenterY()I
    .locals 1

    .prologue
    .line 326
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberCenterY:I

    return v0
.end method

.method public getScrubberTime()I
    .locals 1

    .prologue
    .line 322
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    return v0
.end method

.method public isScrubbing()Z
    .locals 1

    .prologue
    .line 211
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 3

    .prologue
    .line 161
    invoke-super {p0}, Landroid/view/View;->onAttachedToWindow()V

    .line 164
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextViewId:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->findTextView(I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressTextView:Landroid/widget/TextView;

    .line 165
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextViewId:I

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->findTextView(I)Landroid/widget/TextView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->durationTextView:Landroid/widget/TextView;

    .line 167
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->isInEditMode()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 169
    const/16 v0, 0x53

    const/16 v1, 0x128

    const/16 v2, 0x32

    invoke-virtual {p0, v0, v1, v2}, Lcom/google/android/videos/player/overlay/TimeBar;->setTime(III)V

    .line 172
    :cond_0
    const/16 v0, 0x19

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 173
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 177
    invoke-virtual {p0, p0}, Lcom/google/android/videos/player/overlay/TimeBar;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 178
    invoke-super {p0}, Landroid/view/View;->onDetachedFromWindow()V

    .line 179
    return-void
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 649
    const/4 v7, 0x0

    invoke-static {v7, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->getDefaultSize(II)I

    move-result v6

    .line 650
    .local v6, "w":I
    const/high16 v7, 0x42400000    # 48.0f

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->screenDensity:F

    mul-float/2addr v7, v8

    float-to-int v7, v7

    invoke-static {v7, p2}, Lcom/google/android/videos/player/overlay/TimeBar;->getDefaultSize(II)I

    move-result v0

    .line 651
    .local v0, "h":I
    invoke-virtual {p0, v6, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->setMeasuredDimension(II)V

    .line 653
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getPaddingTop()I

    move-result v7

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getPaddingTop()I

    move-result v8

    sub-int v8, v0, v8

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getPaddingBottom()I

    move-result v9

    sub-int/2addr v8, v9

    div-int/lit8 v8, v8, 0x2

    add-int v1, v7, v8

    .line 654
    .local v1, "midY":I
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getPaddingLeft()I

    move-result v7

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    add-int v3, v7, v8

    .line 655
    .local v3, "railL":I
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getPaddingRight()I

    move-result v7

    sub-int v7, v6, v7

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    sub-int v4, v7, v8

    .line 656
    .local v4, "railR":I
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railHeight:I

    div-int/lit8 v7, v7, 0x2

    sub-int v5, v1, v7

    .line 657
    .local v5, "railT":I
    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->railHeight:I

    div-int/lit8 v7, v7, 0x2

    add-int v2, v1, v7

    .line 659
    .local v2, "railB":I
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    invoke-virtual {v7, v3, v5, v4, v2}, Landroid/graphics/Rect;->set(IIII)V

    .line 661
    iget-object v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberRegion:Landroid/graphics/Rect;

    iget v8, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    sub-int v8, v3, v8

    iget v9, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    sub-int v9, v1, v9

    iget v10, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    add-int/2addr v10, v4

    iget v11, p0, Lcom/google/android/videos/player/overlay/TimeBar;->pressedScrubberRadius:I

    add-int/2addr v11, v1

    invoke-virtual {v7, v8, v9, v10, v11}, Landroid/graphics/Rect;->set(IIII)V

    .line 665
    const/4 v7, 0x6

    invoke-direct {p0, v7}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 666
    return-void
.end method

.method public onTouchEvent(Landroid/view/MotionEvent;)Z
    .locals 8
    .param p1, "event"    # Landroid/view/MotionEvent;

    .prologue
    const/4 v1, 0x0

    const/4 v4, 0x1

    .line 392
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbingEnabled()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 393
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getX()F

    move-result v5

    float-to-int v2, v5

    .line 394
    .local v2, "x":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getY()F

    move-result v5

    float-to-int v3, v5

    .line 396
    .local v3, "y":I
    invoke-virtual {p1}, Landroid/view/MotionEvent;->getAction()I

    move-result v5

    packed-switch v5, :pswitch_data_0

    .end local v2    # "x":I
    .end local v3    # "y":I
    :cond_0
    move v4, v1

    .line 437
    :cond_1
    :goto_0
    return v4

    .line 398
    .restart local v2    # "x":I
    .restart local v3    # "y":I
    :pswitch_0
    int-to-float v5, v2

    int-to-float v6, v3

    iget v7, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberPadding:I

    invoke-direct {p0, v5, v6, v7}, Lcom/google/android/videos/player/overlay/TimeBar;->inScrubberRegion(FFI)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 399
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->snapEnabled:Z

    .line 400
    iput v2, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousTouchX:I

    .line 401
    const/4 v5, 0x0

    iput v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->previousSnapDirection:F

    .line 402
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->startScrubbing()V

    .line 403
    invoke-direct {p0, v2}, Lcom/google/android/videos/player/overlay/TimeBar;->setScrubberPosition(I)V

    .line 404
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    if-eqz v5, :cond_1

    .line 405
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    invoke-interface {v5}, Lcom/google/android/videos/player/overlay/TimeBar$Listener;->onScrubbingStart()V

    goto :goto_0

    .line 411
    :pswitch_1
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v5, :cond_0

    .line 412
    invoke-direct {p0, v2}, Lcom/google/android/videos/player/overlay/TimeBar;->setScrubberPosition(I)V

    .line 413
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    if-eqz v5, :cond_1

    .line 414
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v5, v5, Landroid/graphics/Rect;->left:I

    iget-object v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->rail:Landroid/graphics/Rect;

    iget v6, v6, Landroid/graphics/Rect;->right:I

    invoke-static {v6, v2}, Ljava/lang/Math;->min(II)I

    move-result v6

    invoke-static {v5, v6}, Ljava/lang/Math;->max(II)I

    move-result v0

    .line 415
    .local v0, "boundedX":I
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->getScrubberTime()I

    move-result v6

    invoke-interface {v5, v0, v6}, Lcom/google/android/videos/player/overlay/TimeBar$Listener;->onScrubbingContinue(II)V

    goto :goto_0

    .line 422
    .end local v0    # "boundedX":I
    :pswitch_2
    iget-boolean v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v5, :cond_0

    .line 423
    iget v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    iget v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->progressMillis:I

    if-eq v5, v6, :cond_2

    move v1, v4

    .line 424
    .local v1, "progressWillChange":Z
    :cond_2
    iget v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    invoke-virtual {p0, v5}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 425
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    if-eqz v5, :cond_1

    .line 426
    if-eqz v1, :cond_3

    .line 427
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    iget v6, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    invoke-interface {v5, v6}, Lcom/google/android/videos/player/overlay/TimeBar$Listener;->onScrubbingEnd(I)V

    goto :goto_0

    .line 429
    :cond_3
    iget-object v5, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    invoke-interface {v5}, Lcom/google/android/videos/player/overlay/TimeBar$Listener;->onScrubbingCanceled()V

    goto :goto_0

    .line 396
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_2
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method public run()V
    .locals 1

    .prologue
    .line 156
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->accessibilityAnnounceScrubberTime(Z)V

    .line 157
    return-void
.end method

.method public setEnabled(Z)V
    .locals 2
    .param p1, "enabled"    # Z

    .prologue
    .line 262
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->isEnabled()Z

    move-result v0

    if-ne v0, p1, :cond_1

    .line 272
    :cond_0
    :goto_0
    return-void

    .line 265
    :cond_1
    invoke-super {p0, p1}, Landroid/view/View;->setEnabled(Z)V

    .line 266
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v0, :cond_0

    .line 267
    iget v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->endScrubbingAt(I)V

    .line 268
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    if-eqz v0, :cond_0

    .line 269
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    iget v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/TimeBar$Listener;->onScrubbingEnd(I)V

    goto :goto_0
.end method

.method public setListener(Lcom/google/android/videos/player/overlay/TimeBar$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    .prologue
    .line 204
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->listener:Lcom/google/android/videos/player/overlay/TimeBar$Listener;

    .line 205
    return-void
.end method

.method public setProgress(I)V
    .locals 1
    .param p1, "progressMillis"    # I

    .prologue
    .line 279
    invoke-direct {p0, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->updateProgress(I)I

    move-result v0

    .line 280
    .local v0, "updateFlags":I
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 281
    return-void
.end method

.method public setScrubberTime(I)V
    .locals 1
    .param p1, "scrubberTimeMillis"    # I

    .prologue
    .line 253
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v0, :cond_0

    .line 254
    iput p1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->scrubberTimeMillis:I

    .line 255
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 256
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->accessibilityAnnounceScrubberTime(Z)V

    .line 258
    :cond_0
    return-void
.end method

.method public setTime(III)V
    .locals 3
    .param p1, "progressMillis"    # I
    .param p2, "durationMillis"    # I
    .param p3, "bufferedPercent"    # I

    .prologue
    .line 290
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/overlay/TimeBar;->updateDuration(I)I

    move-result v1

    invoke-direct {p0, p1}, Lcom/google/android/videos/player/overlay/TimeBar;->updateProgress(I)I

    move-result v2

    or-int/2addr v1, v2

    invoke-direct {p0, p3}, Lcom/google/android/videos/player/overlay/TimeBar;->updateBufferedPercentage(I)I

    move-result v2

    or-int v0, v1, v2

    .line 294
    .local v0, "updateFlags":I
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    .line 295
    return-void
.end method

.method public startScrubbing()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 220
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    if-eqz v0, :cond_0

    .line 226
    :goto_0
    return-void

    .line 223
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/TimeBar;->startExpandingScrubber()V

    .line 224
    iput-boolean v1, p0, Lcom/google/android/videos/player/overlay/TimeBar;->isScrubbing:Z

    .line 225
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/overlay/TimeBar;->update(I)V

    goto :goto_0
.end method
