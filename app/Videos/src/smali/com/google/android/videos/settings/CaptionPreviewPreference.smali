.class public Lcom/google/android/videos/settings/CaptionPreviewPreference;
.super Landroid/preference/Preference;
.source "CaptionPreviewPreference.java"


# instance fields
.field private final captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

.field private final fontSize:F

.field private final previewTextTranslations:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/util/Locale;",
            "Ljava/lang/CharSequence;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 39
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 40
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 43
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 44
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 47
    invoke-direct {p0, p1, p2, p3}, Landroid/preference/Preference;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 49
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v4

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;

    move-result-object v4

    iput-object v4, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    .line 50
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e01d7

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v3

    .line 53
    .local v3, "minReadableFontSize":F
    const-string v4, "window"

    invoke-virtual {p1, v4}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/view/WindowManager;

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 55
    .local v0, "display":Landroid/view/Display;
    new-instance v2, Landroid/graphics/Point;

    invoke-direct {v2}, Landroid/graphics/Point;-><init>()V

    .line 56
    .local v2, "displaySize":Landroid/graphics/Point;
    invoke-virtual {v0, v2}, Landroid/view/Display;->getSize(Landroid/graphics/Point;)V

    .line 57
    iget v4, v2, Landroid/graphics/Point;->x:I

    iget v5, v2, Landroid/graphics/Point;->y:I

    invoke-static {v4, v5}, Ljava/lang/Math;->min(II)I

    move-result v1

    .line 58
    .local v1, "displayHeight":I
    const v4, 0x3d5a511a    # 0.0533f

    int-to-float v5, v1

    mul-float/2addr v4, v5

    invoke-static {v3, v4}, Ljava/lang/Math;->max(FF)F

    move-result v4

    iput v4, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->fontSize:F

    .line 60
    sget v4, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v5, 0x11

    if-lt v4, v5, :cond_0

    new-instance v4, Ljava/util/HashMap;

    invoke-direct {v4}, Ljava/util/HashMap;-><init>()V

    :goto_0
    iput-object v4, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->previewTextTranslations:Ljava/util/Map;

    .line 61
    const v4, 0x7f040020

    invoke-virtual {p0, v4}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->setLayoutResource(I)V

    .line 62
    const/4 v4, 0x0

    invoke-virtual {p0, v4}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->setPersistent(Z)V

    .line 63
    return-void

    .line 60
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method


# virtual methods
.method protected onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 67
    invoke-super {p0, p1}, Landroid/preference/Preference;->onCreateView(Landroid/view/ViewGroup;)Landroid/view/View;

    move-result-object v0

    .line 68
    .local v0, "view":Landroid/view/View;
    const v1, 0x7f0f00c7

    invoke-virtual {v0, v1}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/exoplayer/text/SubtitleView;

    iput-object v1, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    .line 69
    invoke-virtual {p0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->refresh()V

    .line 70
    return-object v0
.end method

.method public refresh()V
    .locals 6

    .prologue
    const v5, 0x7f0b024f

    .line 74
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    if-nez v2, :cond_1

    .line 101
    :cond_0
    :goto_0
    return-void

    .line 78
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v2}, Lcom/google/android/videos/player/CaptionPreferences;->isEnabled()Z

    move-result v2

    if-nez v2, :cond_2

    .line 79
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/text/SubtitleView;->setVisibility(I)V

    goto :goto_0

    .line 83
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/text/SubtitleView;->setVisibility(I)V

    .line 84
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    iget-object v3, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v3}, Lcom/google/android/videos/player/CaptionPreferences;->getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/text/SubtitleView;->setStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 85
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    iget v3, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->fontSize:F

    iget-object v4, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v4}, Lcom/google/android/videos/player/CaptionPreferences;->getFontScale()F

    move-result v4

    mul-float/2addr v3, v4

    invoke-virtual {v2, v3}, Lcom/google/android/exoplayer/text/SubtitleView;->setTextSize(F)V

    .line 86
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x11

    if-lt v2, v3, :cond_0

    .line 90
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v2}, Lcom/google/android/videos/player/CaptionPreferences;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 91
    .local v0, "locale":Ljava/util/Locale;
    if-nez v0, :cond_4

    invoke-virtual {p0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-virtual {v2, v5}, Landroid/content/Context;->getText(I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 94
    .local v1, "previewText":Ljava/lang/CharSequence;
    :goto_1
    if-nez v1, :cond_3

    .line 95
    invoke-virtual {p0}, Lcom/google/android/videos/settings/CaptionPreviewPreference;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {v2, v0, v5}, Lcom/google/android/videos/utils/Util;->getTextForLocale(Landroid/content/Context;Ljava/util/Locale;I)Ljava/lang/CharSequence;

    move-result-object v1

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->previewTextTranslations:Ljava/util/Map;

    invoke-interface {v2, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 99
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->subtitleView:Lcom/google/android/exoplayer/text/SubtitleView;

    invoke-virtual {v2, v1}, Lcom/google/android/exoplayer/text/SubtitleView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 91
    .end local v1    # "previewText":Ljava/lang/CharSequence;
    :cond_4
    iget-object v2, p0, Lcom/google/android/videos/settings/CaptionPreviewPreference;->previewTextTranslations:Ljava/util/Map;

    invoke-interface {v2, v0}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/CharSequence;

    move-object v1, v2

    goto :goto_1
.end method
