.class public abstract Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;
.super Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;
.source "EpisodeClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "Binder"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder",
        "<",
        "Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;",
        "Lcom/google/android/videos/adapter/EpisodesDataSource;",
        ">;"
    }
.end annotation


# instance fields
.field private final allowDownloads:Z

.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final screenshotRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final showBannerRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;ZLcom/google/android/videos/logging/UiElementNode;)V
    .locals 1
    .param p4, "unpurchasedEpisodesHelper"    # Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;
    .param p5, "allowDownloads"    # Z
    .param p6, "clusterUiElementNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;",
            "Z",
            "Lcom/google/android/videos/logging/UiElementNode;",
            ")V"
        }
    .end annotation

    .prologue
    .line 238
    .local p1, "screenshotRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p2, "showBannerRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    .local p3, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    const/4 v0, 0x5

    invoke-direct {p0, p6, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$Binder;-><init>(Lcom/google/android/videos/logging/UiElementNode;I)V

    .line 239
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->screenshotRequester:Lcom/google/android/videos/async/Requester;

    .line 240
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->showBannerRequester:Lcom/google/android/videos/async/Requester;

    .line 241
    iput-object p4, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    .line 242
    iput-object p3, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 243
    iput-boolean p5, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->allowDownloads:Z

    .line 244
    return-void
.end method


# virtual methods
.method protected final bindPurchasedEpisodeCard(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;Landroid/database/Cursor;)V
    .locals 32
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 248
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v31

    .line 249
    .local v31, "resources":Landroid/content/res/Resources;
    const v7, 0x7f0b00c6

    const/4 v9, 0x2

    new-array v9, v9, [Ljava/lang/Object;

    const/4 v12, 0x0

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v12

    const/4 v12, 0x1

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v15

    aput-object v15, v9, v12

    move-object/from16 v0, v31

    invoke-virtual {v0, v7, v9}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v8

    .line 251
    .local v8, "title":Ljava/lang/String;
    const/4 v10, 0x0

    .line 252
    .local v10, "statusText":Ljava/lang/String;
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v7

    if-nez v7, :cond_2

    const/16 v23, 0x1

    .line 253
    .local v23, "expired":Z
    :goto_0
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getExpirationTimestamp(Landroid/database/Cursor;)J

    move-result-wide v24

    .line 254
    .local v24, "expirationTimestamp":J
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isLastWatched(Landroid/database/Cursor;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 255
    const v7, 0x7f0b0122

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 268
    :cond_0
    :goto_1
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v26

    .line 269
    .local v26, "haveLicense":Z
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v22

    .line 270
    .local v22, "downloadSize":Ljava/lang/Long;
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    .line 271
    .local v6, "bytesDownloaded":Ljava/lang/Long;
    move/from16 v0, v26

    move-object/from16 v1, v22

    invoke-static {v0, v1, v6}, Lcom/google/android/videos/pinning/PinningStatusHelper;->getProgressFraction(ZLjava/lang/Long;Ljava/lang/Long;)F

    move-result v14

    .line 273
    .local v14, "progress":F
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v13

    .line 274
    .local v13, "pinningStatus":Ljava/lang/Integer;
    if-eqz v13, :cond_6

    invoke-virtual {v13}, Ljava/lang/Integer;->intValue()I

    move-result v7

    const/4 v9, 0x3

    if-ne v7, v9, :cond_6

    const/16 v27, 0x1

    .line 276
    .local v27, "isDownloaded":Z
    :goto_2
    if-nez v23, :cond_7

    move-object/from16 v0, p0

    iget-boolean v7, v0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->allowDownloads:Z

    if-eqz v7, :cond_7

    const/4 v11, 0x1

    .line 277
    .local v11, "showPin":Z
    :goto_3
    if-nez v23, :cond_1

    if-nez v27, :cond_8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isNetworkConnected()Z

    move-result v7

    if-nez v7, :cond_8

    :cond_1
    const/16 v17, 0x1

    .line 278
    .local v17, "dimmed":Z
    :goto_4
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v19

    .line 279
    .local v19, "videoId":Ljava/lang/String;
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getAccount(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v18

    .line 280
    .local v18, "account":Ljava/lang/String;
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v9

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v12

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v15

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getResumeTimestamp(Landroid/database/Cursor;)I

    move-result v16

    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getPublishTimestampSeconds(Landroid/database/Cursor;)J

    move-result-wide v20

    move-object/from16 v7, p1

    invoke-virtual/range {v7 .. v21}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->updateViewForPurchasedEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLjava/lang/Integer;FIIZLjava/lang/String;Ljava/lang/String;J)V

    .line 285
    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->screenshotRequester:Lcom/google/android/videos/async/Requester;

    const-class v9, Ljava/lang/String;

    move-object/from16 v0, p1

    move-object/from16 v1, v19

    invoke-virtual {v0, v7, v1, v9}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->prepareThumbnailBitmapView(Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 287
    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromEpisodeId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v7

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    invoke-virtual {v0, v1, v7}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    .line 288
    return-void

    .line 252
    .end local v6    # "bytesDownloaded":Ljava/lang/Long;
    .end local v11    # "showPin":Z
    .end local v13    # "pinningStatus":Ljava/lang/Integer;
    .end local v14    # "progress":F
    .end local v17    # "dimmed":Z
    .end local v18    # "account":Ljava/lang/String;
    .end local v19    # "videoId":Ljava/lang/String;
    .end local v22    # "downloadSize":Ljava/lang/Long;
    .end local v23    # "expired":Z
    .end local v24    # "expirationTimestamp":J
    .end local v26    # "haveLicense":Z
    .end local v27    # "isDownloaded":Z
    :cond_2
    const/16 v23, 0x0

    goto/16 :goto_0

    .line 256
    .restart local v23    # "expired":Z
    .restart local v24    # "expirationTimestamp":J
    :cond_3
    invoke-virtual/range {p2 .. p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isNextEpisode(Landroid/database/Cursor;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 257
    const v7, 0x7f0b0123

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 258
    :cond_4
    if-eqz v23, :cond_5

    .line 259
    const v7, 0x7f0b01fa

    move-object/from16 v0, v31

    invoke-virtual {v0, v7}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 260
    :cond_5
    const-wide v20, 0x7fffffffffffffffL

    cmp-long v7, v24, v20

    if-eqz v7, :cond_0

    .line 261
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v28

    .line 262
    .local v28, "nowTimestamp":J
    move-wide/from16 v0, v24

    move-wide/from16 v2, v28

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/TimeUtil;->getRemainingDays(JJ)I

    move-result v30

    .line 263
    .local v30, "remainingDays":I
    const/16 v7, 0x3c

    move/from16 v0, v30

    if-gt v0, v7, :cond_0

    .line 264
    move-wide/from16 v0, v24

    move-wide/from16 v2, v28

    move-object/from16 v4, v31

    invoke-static {v0, v1, v2, v3, v4}, Lcom/google/android/videos/utils/TimeUtil;->getTimeToExpirationString(JJLandroid/content/res/Resources;)Ljava/lang/String;

    move-result-object v10

    goto/16 :goto_1

    .line 274
    .end local v28    # "nowTimestamp":J
    .end local v30    # "remainingDays":I
    .restart local v6    # "bytesDownloaded":Ljava/lang/Long;
    .restart local v13    # "pinningStatus":Ljava/lang/Integer;
    .restart local v14    # "progress":F
    .restart local v22    # "downloadSize":Ljava/lang/Long;
    .restart local v26    # "haveLicense":Z
    :cond_6
    const/16 v27, 0x0

    goto/16 :goto_2

    .line 276
    .restart local v27    # "isDownloaded":Z
    :cond_7
    const/4 v11, 0x0

    goto/16 :goto_3

    .line 277
    .restart local v11    # "showPin":Z
    :cond_8
    const/16 v17, 0x0

    goto/16 :goto_4
.end method

.method protected final bindUnpurchasedEpisodeCard(Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;Lcom/google/android/videos/adapter/EpisodesDataSource;Landroid/database/Cursor;)V
    .locals 12
    .param p1, "view"    # Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;
    .param p2, "dataSource"    # Lcom/google/android/videos/adapter/EpisodesDataSource;
    .param p3, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 292
    invoke-virtual {p1}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    .line 293
    .local v9, "resources":Landroid/content/res/Resources;
    const v0, 0x7f0b00c6

    const/4 v2, 0x2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getEpisodeNumber(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    const/4 v5, 0x1

    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v2, v5

    invoke-virtual {v9, v0, v2}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 295
    .local v1, "title":Ljava/lang/String;
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isNextEpisode(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_2

    const v0, 0x7f0b0123

    invoke-virtual {v9, v0}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 297
    .local v3, "statusText":Ljava/lang/String;
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    if-nez v0, :cond_3

    const/4 v8, 0x0

    .line 299
    .local v8, "episodeAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_1
    if-nez v8, :cond_4

    const/4 v4, 0x0

    .line 301
    .local v4, "offers":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :goto_2
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p2}, Lcom/google/android/videos/adapter/EpisodesDataSource;->isNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_5

    const/4 v5, 0x1

    :goto_3
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getPublishTimestampSeconds(Landroid/database/Cursor;)J

    move-result-wide v6

    move-object v0, p1

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->updateViewForUnpurchasedEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;ZJ)V

    .line 303
    const/4 v11, 0x0

    .line 304
    .local v11, "screenshotUri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    if-eqz v0, :cond_0

    .line 305
    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getScreenshotUri(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v10

    .line 306
    .local v10, "screenUriString":Ljava/lang/String;
    if-nez v10, :cond_6

    const/4 v11, 0x0

    .line 308
    .end local v10    # "screenUriString":Ljava/lang/String;
    :cond_0
    :goto_4
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    const-class v2, Landroid/net/Uri;

    invoke-virtual {p1, v0, v11, v2}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView;->prepareThumbnailBitmapView(Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Class;)V

    .line 310
    if-eqz v8, :cond_1

    .line 311
    invoke-virtual {p0, p1, v8}, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 313
    :cond_1
    return-void

    .line 295
    .end local v3    # "statusText":Ljava/lang/String;
    .end local v4    # "offers":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .end local v8    # "episodeAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v11    # "screenshotUri":Landroid/net/Uri;
    :cond_2
    const/4 v3, 0x0

    goto :goto_0

    .line 297
    .restart local v3    # "statusText":Ljava/lang/String;
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/EpisodeClusterItemView$Binder;->unpurchasedEpisodesHelper:Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;

    invoke-virtual {p2, p3}, Lcom/google/android/videos/adapter/EpisodesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Lcom/google/android/videos/ui/UnpurchasedAssetsHelper;->getAssetResource(Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v8

    goto :goto_1

    .line 299
    .restart local v8    # "episodeAssetResource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_4
    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    goto :goto_2

    .line 301
    .restart local v4    # "offers":[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    :cond_5
    const/4 v5, 0x0

    goto :goto_3

    .line 306
    .restart local v10    # "screenUriString":Ljava/lang/String;
    .restart local v11    # "screenshotUri":Landroid/net/Uri;
    :cond_6
    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v11

    goto :goto_4
.end method
