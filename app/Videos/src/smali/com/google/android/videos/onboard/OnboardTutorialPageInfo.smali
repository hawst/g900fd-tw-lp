.class public Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;
.super Ljava/lang/Object;
.source "OnboardTutorialPageInfo.java"

# interfaces
.implements Lcom/google/android/play/onboard/OnboardPageInfo;


# instance fields
.field private final context:Landroid/content/Context;

.field private final pageCount:I

.field private final pageIndex:I


# direct methods
.method public constructor <init>(Landroid/content/Context;II)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "pageIndex"    # I
    .param p3, "pageCount"    # I

    .prologue
    .line 21
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    iput-object p1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->context:Landroid/content/Context;

    .line 23
    iput p2, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I

    .line 24
    iput p3, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageCount:I

    .line 25
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I

    return v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;

    .prologue
    .line 15
    iget v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageCount:I

    return v0
.end method


# virtual methods
.method public allowSwipeToNext(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 101
    const/4 v0, 0x1

    return v0
.end method

.method public allowSwipeToPrevious(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 96
    const/4 v0, 0x1

    return v0
.end method

.method public getEndButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 4
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 71
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    .line 72
    .local v0, "buttonInfo":Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    const v1, 0x7f02007d

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 73
    iget-object v2, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->context:Landroid/content/Context;

    iget v1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I

    iget v3, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageCount:I

    add-int/lit8 v3, v3, -0x1

    if-ne v1, v3, :cond_0

    const v1, 0x7f0b0106

    :goto_0
    invoke-virtual {v0, v2, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 75
    new-instance v1, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$2;-><init>(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 85
    return-object v0

    .line 73
    :cond_0
    const v1, 0x7f0b005a

    goto :goto_0
.end method

.method public getGroupPageCount(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 39
    iget v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageCount:I

    return v0
.end method

.method public getGroupPageIndex(Lcom/google/android/play/onboard/OnboardHostControl;)I
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 44
    iget v0, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I

    return v0
.end method

.method public getStartButtonInfo(Lcom/google/android/play/onboard/OnboardHostControl;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    .locals 3
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    invoke-direct {v0}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;-><init>()V

    .line 50
    .local v0, "buttonInfo":Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;
    iget v1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->pageIndex:I

    if-nez v1, :cond_0

    .line 51
    iget-object v1, p0, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;->context:Landroid/content/Context;

    const v2, 0x7f0b0107

    invoke-virtual {v0, v1, v2}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setLabel(Landroid/content/Context;I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 55
    :goto_0
    new-instance v1, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/onboard/OnboardTutorialPageInfo$1;-><init>(Lcom/google/android/videos/onboard/OnboardTutorialPageInfo;Lcom/google/android/play/onboard/OnboardHostControl;)V

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setClickRunnable(Ljava/lang/Runnable;)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 65
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setVisible(Z)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    .line 66
    return-object v0

    .line 53
    :cond_0
    const v1, 0x7f02007e

    invoke-virtual {v0, v1}, Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;->setIconResId(I)Lcom/google/android/play/onboard/OnboardNavFooterButtonInfo;

    goto :goto_0
.end method

.method public isNavFooterVisible(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 29
    const/4 v0, 0x1

    return v0
.end method

.method public shouldAdjustPagePaddingToFitNavFooter(Lcom/google/android/play/onboard/OnboardHostControl;)Z
    .locals 1
    .param p1, "hostControl"    # Lcom/google/android/play/onboard/OnboardHostControl;

    .prologue
    .line 34
    const/4 v0, 0x1

    return v0
.end method
