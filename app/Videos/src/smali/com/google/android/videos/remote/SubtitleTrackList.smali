.class public Lcom/google/android/videos/remote/SubtitleTrackList;
.super Ljava/lang/Object;
.source "SubtitleTrackList.java"


# instance fields
.field public final tracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 19
    .local p2, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-object p1, p0, Lcom/google/android/videos/remote/SubtitleTrackList;->videoId:Ljava/lang/String;

    .line 21
    iput-object p2, p0, Lcom/google/android/videos/remote/SubtitleTrackList;->tracks:Ljava/util/List;

    .line 22
    return-void
.end method
