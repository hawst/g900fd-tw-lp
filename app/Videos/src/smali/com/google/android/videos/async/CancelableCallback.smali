.class public final Lcom/google/android/videos/async/CancelableCallback;
.super Ljava/lang/Object;
.source "CancelableCallback.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<R:",
        "Ljava/lang/Object;",
        "E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private volatile canceled:Z

.field private final target:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    .local p1, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 17
    iput-object p1, p0, Lcom/google/android/videos/async/CancelableCallback;->target:Lcom/google/android/videos/async/Callback;

    .line 18
    return-void
.end method

.method public static create(Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/CancelableCallback;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<R:",
            "Ljava/lang/Object;",
            "E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)",
            "Lcom/google/android/videos/async/CancelableCallback",
            "<TR;TE;>;"
        }
    .end annotation

    .prologue
    .line 21
    .local p0, "target":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    new-instance v0, Lcom/google/android/videos/async/CancelableCallback;

    invoke-direct {v0, p0}, Lcom/google/android/videos/async/CancelableCallback;-><init>(Lcom/google/android/videos/async/Callback;)V

    return-object v0
.end method


# virtual methods
.method public cancel()V
    .locals 1

    .prologue
    .line 25
    .local p0, "this":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/async/CancelableCallback;->canceled:Z

    .line 26
    return-void
.end method

.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 41
    .local p0, "this":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableCallback;->canceled:Z

    if-nez v0, :cond_0

    .line 42
    iget-object v0, p0, Lcom/google/android/videos/async/CancelableCallback;->target:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 44
    :cond_0
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 34
    .local p0, "this":Lcom/google/android/videos/async/CancelableCallback;, "Lcom/google/android/videos/async/CancelableCallback<TR;TE;>;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iget-boolean v0, p0, Lcom/google/android/videos/async/CancelableCallback;->canceled:Z

    if-nez v0, :cond_0

    .line 35
    iget-object v0, p0, Lcom/google/android/videos/async/CancelableCallback;->target:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 37
    :cond_0
    return-void
.end method
