.class public Lcom/google/android/videos/store/UserAssetsUtil;
.super Ljava/lang/Object;
.source "UserAssetsUtil.java"


# direct methods
.method static createAllSeasonRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 47
    const-string v0, "INSERT INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp) SELECT user_assets_account,19,episode_season_id,ifnull(season_synced_timestamp, 0),max(last_activity_timestamp) FROM user_assets, videos ON user_assets_type = 20 AND user_assets_id = video_id AND episode_season_id IS NOT NULL LEFT JOIN seasons ON season_id = episode_season_id GROUP BY user_assets_account, episode_season_id"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method static createAllShowRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 69
    const-string v0, "INSERT OR IGNORE INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp,season_id_of_last_activity,season_seqno_of_last_activity) SELECT user_assets_account,18,root_id,ifnull(shows_synced_timestamp, 0),last_activity_timestamp,assets_id,season_seqno FROM user_assets, assets ON user_assets_type = 19 AND assets_type = 19 AND user_assets_id = assets_id LEFT JOIN shows ON root_id = shows_id ORDER BY last_activity_timestamp DESC"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 70
    const-string v0, "UPDATE user_assets SET last_watched_episode_id = (SELECT asset_id FROM purchased_assets, assets WHERE account = user_assets_account AND asset_type = 20 AND assets_type = 20 AND asset_id = assets_id AND root_asset_id IS user_assets_id AND last_watched_timestamp > 0 AND NOT is_bonus_content ORDER BY last_watched_timestamp DESC) WHERE user_assets_type = 18"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 71
    const-string v0, "UPDATE user_assets SET watched_to_end_credit = 0,last_watched_episode_seqno = (SELECT episode_seqno FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),last_watched_season_seqno = (SELECT season_seqno FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),next_show_episode_id = (SELECT next_episode_id FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),next_show_episode_in_same_season = (SELECT next_episode_in_same_season FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id) WHERE user_assets_type = 18 AND last_watched_episode_id IS NOT NULL"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 72
    const-string v0, "UPDATE user_assets SET owned_episode_count = (SELECT COUNT(*) FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id) WHERE user_assets_type = 18"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 73
    const-string v0, "UPDATE user_assets SET owned_complete_show = 0 WHERE user_assets_type = 18"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 74
    return-void
.end method

.method static createAllVideoRowsForUpgrade(Landroid/database/sqlite/SQLiteDatabase;)V
    .locals 1
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 26
    const-string v0, "INSERT INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp,ppm_remaining,watched_to_end_credit,merged_expiration_timestamp) SELECT account,asset_type,asset_id,ifnull(video_synced_timestamp, 0),max(purchase_timestamp_seconds * 1000, last_watched_timestamp, ifnull(publish_timestamp * 1000, 0)),min(1000000, max(0, 1000000 - ifnull(resume_timestamp * 1000 / duration_seconds, 0))),0,ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) FROM purchased_assets LEFT JOIN videos ON asset_id = video_id WHERE asset_type IN (6,20)"

    invoke-virtual {p0, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;)V

    .line 27
    return-void
.end method

.method public static purgeForAccount(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;)V
    .locals 7
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 110
    const/4 v5, 0x1

    new-array v0, v5, [Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v6

    .line 115
    .local v0, "bindArgs":[Ljava/lang/String;
    const-string v5, "user_assets"

    const-string v6, "user_assets_account = ? AND user_assets_type = 6 AND NOT EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_assets_account AND asset_type = 6 AND asset_id = user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 118
    const-string v5, "SELECT DISTINCT episode_season_id FROM user_assets, videos ON user_assets_type = 20 AND user_assets_id = video_id AND episode_season_id IS NOT NULL WHERE user_assets_account = ? AND user_assets_type = 20 AND NOT EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND asset_id = user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 121
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    new-array v3, v5, [Ljava/lang/String;

    .line 122
    .local v3, "seasonIds":[Ljava/lang/String;
    :goto_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_0

    .line 123
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v3, v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 126
    .end local v3    # "seasonIds":[Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v5

    .restart local v3    # "seasonIds":[Ljava/lang/String;
    :cond_0
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 128
    const-string v5, "user_assets"

    const-string v6, "user_assets_account = ? AND user_assets_type = 20 AND NOT EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND asset_id = user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 129
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    array-length v5, v3

    if-ge v2, v5, :cond_1

    .line 130
    aget-object v5, v3, v2

    invoke-static {p0, p1, v5}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 129
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 134
    :cond_1
    const-string v5, "SELECT DISTINCT root_id FROM user_assets, assets ON user_assets_type = 19 AND user_assets_id = assets_id WHERE user_assets_account = ? AND user_assets_type = 19 AND NOT EXISTS (SELECT NULL FROM purchased_assets, videos ON asset_type = 20 AND asset_id = video_id WHERE account = user_assets_account AND episode_season_id IS user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v0}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 137
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->getCount()I

    move-result v5

    new-array v4, v5, [Ljava/lang/String;

    .line 138
    .local v4, "showIds":[Ljava/lang/String;
    :goto_2
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 139
    invoke-interface {v1}, Landroid/database/Cursor;->getPosition()I

    move-result v5

    const/4 v6, 0x0

    invoke-interface {v1, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v4, v5
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    goto :goto_2

    .line 142
    .end local v4    # "showIds":[Ljava/lang/String;
    :catchall_1
    move-exception v5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v5

    .restart local v4    # "showIds":[Ljava/lang/String;
    :cond_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 144
    const-string v5, "user_assets"

    const-string v6, "user_assets_account = ? AND user_assets_type = 19 AND NOT EXISTS (SELECT NULL FROM purchased_assets, videos ON asset_type = 20 AND asset_id = video_id WHERE account = user_assets_account AND episode_season_id IS user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 145
    const/4 v2, 0x0

    :goto_3
    array-length v5, v4

    if-ge v2, v5, :cond_3

    .line 146
    aget-object v5, v4, v2

    invoke-static {p0, p1, v5}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 145
    add-int/lit8 v2, v2, 0x1

    goto :goto_3

    .line 150
    :cond_3
    const-string v5, "user_assets"

    const-string v6, "user_assets_account = ? AND user_assets_type = 18 AND NOT EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    .line 151
    return-void
.end method

.method public static purgeForAccountAndVideo(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 9
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x1

    const/4 v7, 0x0

    .line 160
    const/4 v5, 0x2

    new-array v0, v5, [Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v7

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v0, v8

    .line 166
    .local v0, "bindArgs":[Ljava/lang/String;
    const-string v5, "user_assets"

    const-string v6, "user_assets_account = ? AND user_assets_id = ? AND user_assets_type IN (6, 20) AND NOT EXISTS (SELECT NULL FROM purchased_assets WHERE account = user_assets_account AND asset_type = user_assets_type AND asset_id = user_assets_id AND purchase_status != -1)"

    invoke-virtual {p0, v5, v6, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v2

    .line 168
    .local v2, "rowsDeleted":I
    if-nez v2, :cond_0

    .line 188
    :goto_0
    return-void

    .line 173
    :cond_0
    const-string v5, "SELECT episode_season_id,root_id FROM videos, assets WHERE assets_type = 20 AND video_id = assets_id AND video_id = ?"

    new-array v6, v8, [Ljava/lang/String;

    aput-object p2, v6, v7

    invoke-virtual {p0, v5, v6}, Landroid/database/sqlite/SQLiteDatabase;->rawQuery(Ljava/lang/String;[Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v1

    .line 178
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v5

    if-nez v5, :cond_1

    .line 184
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    goto :goto_0

    .line 181
    :cond_1
    const/4 v5, 0x0

    :try_start_1
    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 182
    .local v3, "seasonId":Ljava/lang/String;
    const/4 v5, 0x1

    invoke-interface {v1, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result-object v4

    .line 184
    .local v4, "showId":Ljava/lang/String;
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 186
    invoke-static {p0, p1, v3}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    .line 187
    invoke-static {p0, p1, v4}, Lcom/google/android/videos/store/UserAssetsUtil;->refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 184
    .end local v3    # "seasonId":Ljava/lang/String;
    .end local v4    # "showId":Ljava/lang/String;
    :catchall_0
    move-exception v5

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v5
.end method

.method public static refreshSeasonRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 58
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 62
    .local v0, "bindArgs":[Ljava/lang/String;
    const-string v1, "REPLACE INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp) SELECT user_assets_account,19,episode_season_id,ifnull(season_synced_timestamp, 0),max(last_activity_timestamp) FROM user_assets, videos ON user_assets_type = 20 AND user_assets_id = video_id AND episode_season_id IS NOT NULL LEFT JOIN seasons ON season_id = episode_season_id GROUP BY user_assets_account, episode_season_id HAVING user_assets_account = ? AND episode_season_id IS ?"

    invoke-virtual {p0, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 63
    return-void
.end method

.method public static refreshShowRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 6
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x2

    const/4 v4, 0x0

    const/4 v3, 0x1

    .line 85
    new-array v0, v5, [Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v4

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v3

    .line 92
    .local v0, "bindArgs":[Ljava/lang/String;
    const-string v2, "REPLACE INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp,season_id_of_last_activity,season_seqno_of_last_activity) SELECT user_assets_account,18,root_id,ifnull(shows_synced_timestamp, 0),last_activity_timestamp,assets_id,season_seqno FROM user_assets, assets ON user_assets_type = 19 AND assets_type = 19 AND user_assets_id = assets_id LEFT JOIN shows ON root_id = shows_id WHERE user_assets_account = ? AND root_id = ? ORDER BY last_activity_timestamp DESC LIMIT 1"

    invoke-virtual {p0, v2}, Landroid/database/sqlite/SQLiteDatabase;->compileStatement(Ljava/lang/String;)Landroid/database/sqlite/SQLiteStatement;

    move-result-object v1

    .line 93
    .local v1, "step1":Landroid/database/sqlite/SQLiteStatement;
    aget-object v2, v0, v4

    invoke-virtual {v1, v3, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 94
    aget-object v2, v0, v3

    invoke-virtual {v1, v5, v2}, Landroid/database/sqlite/SQLiteStatement;->bindString(ILjava/lang/String;)V

    .line 95
    invoke-virtual {v1}, Landroid/database/sqlite/SQLiteStatement;->executeInsert()J

    move-result-wide v2

    const-wide/16 v4, -0x1

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    .line 102
    :goto_0
    return-void

    .line 98
    :cond_0
    const-string v2, "UPDATE user_assets SET last_watched_episode_id = (SELECT asset_id FROM purchased_assets, assets WHERE account = user_assets_account AND asset_type = 20 AND assets_type = 20 AND asset_id = assets_id AND root_asset_id IS user_assets_id AND last_watched_timestamp > 0 AND NOT is_bonus_content ORDER BY last_watched_timestamp DESC) WHERE user_assets_type = 18 AND user_assets_account = ? AND user_assets_id = ?"

    invoke-virtual {p0, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 99
    const-string v2, "UPDATE user_assets SET watched_to_end_credit = (SELECT user_episodes.watched_to_end_credit FROM user_assets AS user_episodes WHERE user_episodes.user_assets_account = user_assets.user_assets_account AND user_episodes.user_assets_type = 20 AND user_episodes.user_assets_id = user_assets.last_watched_episode_id),last_watched_episode_seqno = (SELECT episode_seqno FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),last_watched_season_seqno = (SELECT season_seqno FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),next_show_episode_id = (SELECT next_episode_id FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id),next_show_episode_in_same_season = (SELECT next_episode_in_same_season FROM assets WHERE assets_type = 20 AND assets_id = last_watched_episode_id) WHERE user_assets_account = ? AND user_assets_type = 18 AND user_assets_id = ? AND last_watched_episode_id IS NOT NULL"

    invoke-virtual {p0, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 100
    const-string v2, "UPDATE user_assets SET owned_episode_count = (SELECT COUNT(*) FROM purchased_assets WHERE account = user_assets_account AND asset_type = 20 AND root_asset_id = user_assets_id) WHERE user_assets_type = 18 AND user_assets_account = ? AND user_assets_id = ?"

    invoke-virtual {p0, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 101
    const-string v2, "UPDATE user_assets SET owned_complete_show = EXISTS(SELECT NULL FROM shows WHERE shows_id = user_assets_id AND shows_full_sync_timestamp > 0) AND owned_episode_count = (SELECT COUNT(*) FROM assets WHERE assets_type = 20 AND root_id = user_assets_id) WHERE user_assets_account = ? AND user_assets_type = 18 AND user_assets_id = ?"

    invoke-virtual {p0, v2, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    goto :goto_0
.end method

.method public static refreshVideoRow(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p0, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 36
    const/4 v1, 0x2

    new-array v0, v1, [Ljava/lang/String;

    const/4 v1, 0x0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    aput-object v2, v0, v1

    .line 40
    .local v0, "bindArgs":[Ljava/lang/String;
    const-string v1, "REPLACE INTO user_assets (user_assets_account,user_assets_type,user_assets_id,metadata_timestamp_at_last_sync,last_activity_timestamp,ppm_remaining,watched_to_end_credit,merged_expiration_timestamp) SELECT account,asset_type,asset_id,ifnull(video_synced_timestamp, 0),max(purchase_timestamp_seconds * 1000, last_watched_timestamp, ifnull(publish_timestamp * 1000, 0)),min(1000000, max(0, 1000000 - ifnull(resume_timestamp * 1000 / duration_seconds, 0))),end_credit_start_seconds IS NOT NULL AND end_credit_start_seconds != 0 AND resume_timestamp >= end_credit_start_seconds * 1000,ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) FROM purchased_assets LEFT JOIN videos ON asset_id = video_id LEFT JOIN assets ON asset_type = assets_type AND asset_id = assets_id WHERE account = ? AND asset_type IN (6, 20) AND asset_id = ?"

    invoke-virtual {p0, v1, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V

    .line 41
    return-void
.end method
