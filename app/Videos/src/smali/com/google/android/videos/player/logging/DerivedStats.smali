.class public Lcom/google/android/videos/player/logging/DerivedStats;
.super Ljava/lang/Object;
.source "DerivedStats.java"


# instance fields
.field public aggregateFormatStatsPlayingTimeMs:J

.field public final connectionTypesUsed:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public droppedFrameCount:I

.field public earlyRebufferingCount:I

.field public earlyRebufferingTimeMs:I

.field public errorCount:I

.field public failureCount:I

.field public firstItag:I

.field public final itagsUsed:Ljava/util/HashSet;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashSet",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field public joiningTimeMs:I

.field public secondItag:I

.field public secondItagSelectionTimeMs:I

.field public totalPlayingTimeMs:I

.field public totalRebufferingCount:I

.field public totalRebufferingTimeMs:I

.field public videoBandwidthTimesPlayingTimeMs:J

.field public videoHeightTimesPlayingTimeMs:J


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 33
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStats;->connectionTypesUsed:Ljava/util/HashSet;

    .line 35
    new-instance v0, Ljava/util/HashSet;

    invoke-direct {v0}, Ljava/util/HashSet;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/DerivedStats;->itagsUsed:Ljava/util/HashSet;

    .line 36
    return-void
.end method
