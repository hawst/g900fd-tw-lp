.class Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CleanupTask"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/PurchaseStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V
    .locals 1
    .param p2, "priority"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 798
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 799
    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 800
    return-void
.end method

.method private removeOldPurchases(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 10
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 835
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v6

    const-wide/32 v8, 0x5265c00

    sub-long/2addr v6, v8

    const-wide/16 v8, 0x3e8

    div-long v2, v6, v8

    .line 837
    .local v2, "timestampSeconds":J
    new-instance v1, Landroid/content/ContentValues;

    invoke-direct {v1}, Landroid/content/ContentValues;-><init>()V

    .line 838
    .local v1, "values":Landroid/content/ContentValues;
    const-string v6, "purchase_status"

    const/4 v7, -0x1

    invoke-static {v7}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    invoke-virtual {v1, v6, v7}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 839
    const-string v6, "purchased_assets"

    const-string v7, "purchase_status NOT IN (2, 1, 6) AND (expiration_timestamp_seconds IS NULL OR expiration_timestamp_seconds < ?)"

    new-array v8, v4, [Ljava/lang/String;

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v9

    aput-object v9, v8, v5

    invoke-virtual {p1, v6, v1, v7, v8}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v0

    .line 841
    .local v0, "rowsMarked":I
    if-lez v0, :cond_0

    :goto_0
    return v4

    :cond_0
    move v4, v5

    goto :goto_0
.end method

.method private removePurchasesForUnknownAccounts(Landroid/database/sqlite/SQLiteDatabase;)Z
    .locals 9
    .param p1, "transaction"    # Landroid/database/sqlite/SQLiteDatabase;

    .prologue
    .line 819
    iget-object v6, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-static {v6}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$900(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 820
    .local v2, "knownAccounts":[Landroid/accounts/Account;
    array-length v6, v2

    new-array v0, v6, [Ljava/lang/String;

    .line 821
    .local v0, "accountNames":[Ljava/lang/String;
    new-instance v4, Ljava/lang/StringBuilder;

    const-string v6, " NOT IN ("

    invoke-direct {v4, v6}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 822
    .local v4, "notInClauseBuilder":Ljava/lang/StringBuilder;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v6, v2

    if-ge v1, v6, :cond_1

    .line 823
    aget-object v6, v2, v1

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    aput-object v6, v0, v1

    .line 824
    if-nez v1, :cond_0

    const-string v6, "?"

    :goto_1
    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 822
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 824
    :cond_0
    const-string v6, ",?"

    goto :goto_1

    .line 826
    :cond_1
    const-string v6, ")"

    invoke-virtual {v4, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    .line 827
    .local v3, "notInClause":Ljava/lang/String;
    const-string v6, "purchased_assets"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "account"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v5

    .line 829
    .local v5, "rowsDeleted":I
    const-string v6, "user_data"

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "user_account"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-virtual {p1, v6, v7, v0}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v6

    add-int/2addr v5, v6

    .line 831
    if-lez v5, :cond_2

    const/4 v6, 0x1

    :goto_2
    return v6

    :cond_2
    const/4 v6, 0x0

    goto :goto_2
.end method


# virtual methods
.method protected doSync()V
    .locals 6

    .prologue
    const/4 v5, 0x0

    .line 804
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v1

    .line 805
    .local v1, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v0, 0x0

    .line 807
    .local v0, "success":Z
    :try_start_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->removePurchasesForUnknownAccounts(Landroid/database/sqlite/SQLiteDatabase;)Z

    .line 808
    invoke-direct {p0, v1}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->removeOldPurchases(Landroid/database/sqlite/SQLiteDatabase;)Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 809
    const/4 v0, 0x1

    .line 811
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v2

    new-array v3, v5, [Ljava/lang/Object;

    invoke-virtual {v2, v1, v0, v5, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 813
    new-instance v2, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v2}, Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;->schedule()V

    .line 814
    new-instance v2, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    iget v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v5, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-direct {v2, v3, v4, v5}, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v2}, Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;->schedule()V

    .line 815
    iget-object v2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    invoke-static {v2}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$800(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->priority:I

    iget-object v4, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/store/AssetStoreSync;->deleteOrphanedMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 816
    return-void

    .line 811
    :catchall_0
    move-exception v2

    iget-object v3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->this$0:Lcom/google/android/videos/store/PurchaseStoreSync;

    # getter for: Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v3}, Lcom/google/android/videos/store/PurchaseStoreSync;->access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v3

    new-array v4, v5, [Ljava/lang/Object;

    invoke-virtual {v3, v1, v0, v5, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v2
.end method
