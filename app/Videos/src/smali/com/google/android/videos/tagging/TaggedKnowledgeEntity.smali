.class public final Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;
.super Ljava/lang/Object;
.source "TaggedKnowledgeEntity.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$1;,
        Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;
    }
.end annotation


# static fields
.field private static final TYPE_AND_HORIZONTAL_POSITION_COMPARATOR:Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;


# instance fields
.field public final knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

.field public final splitId:I

.field public final tagShape:Lcom/google/android/videos/tagging/Tag$TagShape;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 26
    new-instance v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;-><init>(Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$1;)V

    sput-object v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->TYPE_AND_HORIZONTAL_POSITION_COMPARATOR:Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;

    return-void
.end method

.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeEntity;ILcom/google/android/videos/tagging/Tag$TagShape;)V
    .locals 1
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p2, "splitId"    # I
    .param p3, "tagShape"    # Lcom/google/android/videos/tagging/Tag$TagShape;

    .prologue
    .line 45
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 46
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    iput-object v0, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->knowledgeEntity:Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 47
    iput p2, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->splitId:I

    .line 48
    iput-object p3, p0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->tagShape:Lcom/google/android/videos/tagging/Tag$TagShape;

    .line 49
    return-void
.end method

.method public static sortByTypeAndHorizontalPosition(Ljava/util/List;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 23
    .local p0, "list":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    sget-object v0, Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;->TYPE_AND_HORIZONTAL_POSITION_COMPARATOR:Lcom/google/android/videos/tagging/TaggedKnowledgeEntity$TypeAndHorizontalPositionComparator;

    invoke-static {p0, v0}, Ljava/util/Collections;->sort(Ljava/util/List;Ljava/util/Comparator;)V

    .line 24
    return-void
.end method
