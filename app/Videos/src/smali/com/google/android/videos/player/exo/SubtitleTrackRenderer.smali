.class public Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;
.super Lcom/google/android/exoplayer/TrackRenderer;
.source "SubtitleTrackRenderer.java"

# interfaces
.implements Landroid/os/Handler$Callback;


# instance fields
.field private currentPositionUs:J

.field private nextEventIndex:I

.field private nextEventTimeMs:I

.field private subtitleEventTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private subtitleNeedsSync:Z

.field private subtitles:Lcom/google/android/videos/subtitles/Subtitles;

.field private final subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private final uiHandler:Landroid/os/Handler;


# direct methods
.method public constructor <init>(Landroid/os/Looper;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V
    .locals 1
    .param p1, "uiLooper"    # Landroid/os/Looper;
    .param p2, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .prologue
    .line 42
    invoke-direct {p0}, Lcom/google/android/exoplayer/TrackRenderer;-><init>()V

    .line 43
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0, p1, p0}, Landroid/os/Handler;-><init>(Landroid/os/Looper;Landroid/os/Handler$Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->uiHandler:Landroid/os/Handler;

    .line 44
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iput-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 45
    return-void
.end method

.method private getEventTimeMs(Ljava/util/List;I)I
    .locals 1
    .param p2, "index"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;I)I"
        }
    .end annotation

    .prologue
    .line 145
    .local p1, "eventTimes":Ljava/util/List;, "Ljava/util/List<Ljava/lang/Integer;>;"
    invoke-interface {p1}, Ljava/util/List;->size()I

    move-result v0

    if-ge p2, v0, :cond_0

    invoke-interface {p1, p2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, -0x1

    goto :goto_0
.end method

.method private postClearSubtitles()V
    .locals 2

    .prologue
    .line 154
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->uiHandler:Landroid/os/Handler;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/os/Handler;->sendEmptyMessage(I)Z

    .line 155
    return-void
.end method

.method private postUpdateSubtitles(I)V
    .locals 3
    .param p1, "positionMs"    # I

    .prologue
    .line 149
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->uiHandler:Landroid/os/Handler;

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/subtitles/Subtitles;->getSubtitleWindowSnapshotsAt(I)Ljava/util/List;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/os/Handler;->obtainMessage(ILjava/lang/Object;)Landroid/os/Message;

    move-result-object v0

    invoke-virtual {v0}, Landroid/os/Message;->sendToTarget()V

    .line 151
    return-void
.end method

.method private syncSubtitles(J)V
    .locals 5
    .param p1, "positionUs"    # J

    .prologue
    const/4 v3, 0x0

    .line 114
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    if-nez v1, :cond_0

    .line 115
    iput v3, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    .line 116
    const/4 v1, -0x1

    iput v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventTimeMs:I

    .line 117
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->postClearSubtitles()V

    .line 125
    :goto_0
    iput-boolean v3, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleNeedsSync:Z

    .line 126
    return-void

    .line 119
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->usToMs(J)I

    move-result v0

    .line 120
    .local v0, "positionMs":I
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleEventTimes:Ljava/util/List;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    invoke-static {v1, v2}, Ljava/util/Collections;->binarySearch(Ljava/util/List;Ljava/lang/Object;)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    .line 121
    iget v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    if-ltz v1, :cond_1

    iget v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    add-int/lit8 v1, v1, 0x1

    :goto_1
    iput v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    .line 122
    iget-object v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleEventTimes:Ljava/util/List;

    iget v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->getEventTimeMs(Ljava/util/List;I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventTimeMs:I

    .line 123
    invoke-direct {p0, v0}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->postUpdateSubtitles(I)V

    goto :goto_0

    .line 121
    :cond_1
    iget v1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    xor-int/lit8 v1, v1, -0x1

    goto :goto_1
.end method

.method private updateSubtitles(J)V
    .locals 5
    .param p1, "positionUs"    # J

    .prologue
    .line 129
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->usToMs(J)I

    move-result v1

    .line 130
    .local v1, "timeMs":I
    const/4 v0, 0x0

    .line 131
    .local v0, "needsPost":Z
    :goto_0
    iget v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventTimeMs:I

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    iget v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventTimeMs:I

    if-gt v2, v1, :cond_0

    .line 132
    iget-object v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleEventTimes:Ljava/util/List;

    iget v3, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    add-int/lit8 v3, v3, 0x1

    iput v3, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventIndex:I

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->getEventTimeMs(Ljava/util/List;I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->nextEventTimeMs:I

    .line 133
    const/4 v0, 0x1

    goto :goto_0

    .line 135
    :cond_0
    if-eqz v0, :cond_1

    .line 136
    invoke-direct {p0, v1}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->postUpdateSubtitles(I)V

    .line 138
    :cond_1
    return-void
.end method

.method private usToMs(J)I
    .locals 3
    .param p1, "timeUs"    # J

    .prologue
    .line 141
    const-wide/16 v0, 0x3e8

    div-long v0, p1, v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method protected doPrepare()I
    .locals 1
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 58
    const/4 v0, 0x1

    return v0
.end method

.method protected doSomeWork(JJ)V
    .locals 1
    .param p1, "positionUs"    # J
    .param p3, "elapsedRealtimeUs"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 75
    iput-wide p1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->currentPositionUs:J

    .line 76
    iget-boolean v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleNeedsSync:Z

    if-eqz v0, :cond_0

    .line 77
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->syncSubtitles(J)V

    .line 81
    :goto_0
    return-void

    .line 79
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->updateSubtitles(J)V

    goto :goto_0
.end method

.method protected getBufferedPositionUs()J
    .locals 2

    .prologue
    .line 110
    const-wide/16 v0, -0x3

    return-wide v0
.end method

.method protected getCurrentPositionUs()J
    .locals 2

    .prologue
    .line 100
    iget-wide v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->currentPositionUs:J

    return-wide v0
.end method

.method protected getDurationUs()J
    .locals 2

    .prologue
    .line 105
    const-wide/16 v0, -0x2

    return-wide v0
.end method

.method public handleMessage(ILjava/lang/Object;)V
    .locals 1
    .param p1, "what"    # I
    .param p2, "object"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/exoplayer/ExoPlaybackException;
        }
    .end annotation

    .prologue
    .line 49
    if-nez p1, :cond_0

    .line 50
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleNeedsSync:Z

    .line 51
    check-cast p2, Lcom/google/android/videos/subtitles/Subtitles;

    .end local p2    # "object":Ljava/lang/Object;
    iput-object p2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    .line 52
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    if-nez v0, :cond_1

    const/4 v0, 0x0

    :goto_0
    iput-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitleEventTimes:Ljava/util/List;

    .line 54
    :cond_0
    return-void

    .line 52
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitles:Lcom/google/android/videos/subtitles/Subtitles;

    invoke-virtual {v0}, Lcom/google/android/videos/subtitles/Subtitles;->getEventTimes()Ljava/util/List;

    move-result-object v0

    goto :goto_0
.end method

.method public handleMessage(Landroid/os/Message;)Z
    .locals 3
    .param p1, "msg"    # Landroid/os/Message;

    .prologue
    const/4 v1, 0x1

    .line 160
    iget v0, p1, Landroid/os/Message;->what:I

    packed-switch v0, :pswitch_data_0

    .line 169
    const/4 v0, 0x0

    :goto_0
    return v0

    .line 162
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->clear()V

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->hide()V

    move v0, v1

    .line 164
    goto :goto_0

    .line 166
    :pswitch_1
    iget-object v2, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iget-object v0, p1, Landroid/os/Message;->obj:Ljava/lang/Object;

    check-cast v0, Ljava/util/List;

    invoke-interface {v2, v0}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->update(Ljava/util/List;)V

    move v0, v1

    .line 167
    goto :goto_0

    .line 160
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_1
    .end packed-switch
.end method

.method protected isEnded()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x1

    return v0
.end method

.method protected isReady()Z
    .locals 1

    .prologue
    .line 95
    const/4 v0, 0x1

    return v0
.end method

.method protected declared-synchronized onDisabled()V
    .locals 1

    .prologue
    .line 85
    monitor-enter p0

    :try_start_0
    invoke-direct {p0}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->postClearSubtitles()V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 86
    monitor-exit p0

    return-void

    .line 85
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method protected onEnabled(JZ)V
    .locals 1
    .param p1, "positionUs"    # J
    .param p3, "joining"    # Z

    .prologue
    .line 63
    iput-wide p1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->currentPositionUs:J

    .line 64
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->syncSubtitles(J)V

    .line 65
    return-void
.end method

.method protected seekTo(J)V
    .locals 1
    .param p1, "positionUs"    # J

    .prologue
    .line 69
    iput-wide p1, p0, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->currentPositionUs:J

    .line 70
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/exo/SubtitleTrackRenderer;->syncSubtitles(J)V

    .line 71
    return-void
.end method
