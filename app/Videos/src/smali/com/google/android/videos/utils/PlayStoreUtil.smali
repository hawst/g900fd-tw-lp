.class public Lcom/google/android/videos/utils/PlayStoreUtil;
.super Ljava/lang/Object;
.source "PlayStoreUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/utils/PlayStoreUtil$PurchaseStoreCallback;,
        Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;
    }
.end annotation


# direct methods
.method private static getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "starter"    # Landroid/content/Context;

    .prologue
    .line 409
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    return-object v0
.end method

.method public static getPlayStoreVideosAppUri()Landroid/net/Uri;
    .locals 1

    .prologue
    .line 330
    const-string v0, "market://details?id=com.google.android.videos"

    invoke-static {v0}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private static getStorePackage(Landroid/content/Context;)Ljava/lang/String;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 387
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->isFinskyInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 388
    const-string v0, "com.google.android.finsky"

    .line 392
    :goto_0
    return-object v0

    .line 389
    :cond_0
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->isVendingInstalled(Landroid/content/Context;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 390
    const-string v0, "com.android.vending"

    goto :goto_0

    .line 392
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private static getStoreVersionCode(Landroid/content/Context;)I
    .locals 5
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    .line 372
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getStorePackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    .line 373
    .local v1, "storePackage":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 379
    :goto_0
    return v2

    .line 377
    :cond_0
    :try_start_0
    invoke-virtual {p0}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    const/4 v4, 0x0

    invoke-virtual {v3, v1, v4}, Landroid/content/pm/PackageManager;->getPackageInfo(Ljava/lang/String;I)Landroid/content/pm/PackageInfo;

    move-result-object v3

    iget v2, v3, Landroid/content/pm/PackageInfo;->versionCode:I
    :try_end_0
    .catch Landroid/content/pm/PackageManager$NameNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 378
    :catch_0
    move-exception v0

    .line 379
    .local v0, "e":Landroid/content/pm/PackageManager$NameNotFoundException;
    goto :goto_0
.end method

.method private static isFinskyInstalled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 397
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/android/videos/utils/PlayStoreUriCreator;->PLAY_STORE_BASE_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 398
    .local v0, "finskyIntent":Landroid/content/Intent;
    const-string v1, "com.google.android.finsky"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 399
    invoke-static {p0, v0}, Lcom/google/android/videos/utils/Util;->canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private static isVendingInstalled(Landroid/content/Context;)Z
    .locals 3
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 403
    new-instance v0, Landroid/content/Intent;

    const-string v1, "android.intent.action.VIEW"

    sget-object v2, Lcom/google/android/videos/utils/PlayStoreUriCreator;->PLAY_STORE_BASE_URI:Landroid/net/Uri;

    invoke-direct {v0, v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 404
    .local v0, "vendingIntent":Landroid/content/Intent;
    const-string v1, "com.android.vending"

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 405
    invoke-static {p0, v0}, Lcom/google/android/videos/utils/Util;->canResolveIntent(Landroid/content/Context;Landroid/content/Intent;)Z

    move-result v1

    return v1
.end method

.method private static showError(Landroid/content/Context;I)V
    .locals 1
    .param p0, "starter"    # Landroid/content/Context;
    .param p1, "messageId"    # I

    .prologue
    .line 365
    const/4 v0, 0x1

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 366
    return-void
.end method

.method private static showPackageMissingError(Landroid/content/Context;)V
    .locals 1
    .param p0, "starter"    # Landroid/content/Context;

    .prologue
    .line 361
    const v0, 0x7f0b013f

    invoke-static {p0, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->showError(Landroid/content/Context;I)V

    .line 362
    return-void
.end method

.method public static startEpisodeDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "episodeId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "filteringType"    # I

    .prologue
    .line 236
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startEpisodeDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static startEpisodeDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I
    .locals 6
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "episodeId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "filteringType"    # I
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 227
    const/16 v2, 0x14

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startShopForDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILandroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V
    .locals 5
    .param p0, "starter"    # Landroid/content/Context;
    .param p1, "storeUri"    # Landroid/net/Uri;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "eventSource"    # I

    .prologue
    .line 72
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 73
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 74
    invoke-static {p3}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1, v1}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createFromUri(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object p1

    .line 76
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 77
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    const/16 v3, 0xe

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    const-string v4, "/tv/"

    invoke-virtual {v1, v4}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    const-string v1, "shows"

    :goto_0
    invoke-interface {v2, v3, v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onPlayStoreUriOpen(ILjava/lang/String;I)V

    .line 82
    return-void

    .line 77
    :cond_0
    const-string v1, "movies"

    goto :goto_0
.end method

.method private static startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I
    .locals 6
    .param p0, "starter"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "account"    # Ljava/lang/String;

    .prologue
    .line 334
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getStorePackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .line 335
    .local v2, "storePackage":Ljava/lang/String;
    if-nez v2, :cond_0

    .line 336
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->showPackageMissingError(Landroid/content/Context;)V

    .line 337
    const/16 v3, 0xc

    .line 356
    :goto_0
    return v3

    .line 340
    :cond_0
    new-instance v1, Landroid/content/Intent;

    const-string v3, "android.intent.action.VIEW"

    new-instance v4, Lcom/google/android/videos/utils/UriRewriter;

    invoke-virtual {p0}, Landroid/content/Context;->getContentResolver()Landroid/content/ContentResolver;

    move-result-object v5

    invoke-direct {v4, v5}, Lcom/google/android/videos/utils/UriRewriter;-><init>(Landroid/content/ContentResolver;)V

    invoke-virtual {v4, p1}, Lcom/google/android/videos/utils/UriRewriter;->rewrite(Landroid/net/Uri;)Landroid/net/Uri;

    move-result-object v4

    invoke-direct {v1, v3, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    .line 342
    .local v1, "intent":Landroid/content/Intent;
    const/high16 v3, 0x80000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    .line 343
    invoke-virtual {v1, v2}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 344
    instance-of v3, p0, Landroid/app/Service;

    if-eqz v3, :cond_1

    .line 345
    const/high16 v3, 0x10000000

    invoke-virtual {v1, v3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 347
    :cond_1
    if-eqz p2, :cond_2

    .line 348
    const-string v3, "authAccount"

    invoke-virtual {v1, v3, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 351
    :cond_2
    :try_start_0
    invoke-virtual {p0, v1}, Landroid/content/Context;->startActivity(Landroid/content/Intent;)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 352
    const/4 v3, -0x1

    goto :goto_0

    .line 353
    :catch_0
    move-exception v0

    .line 355
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v3, 0x7f0b013e

    invoke-static {p0, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->showError(Landroid/content/Context;I)V

    .line 356
    const/16 v3, 0xd

    goto :goto_0
.end method

.method public static startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)I
    .locals 1
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "filteringType"    # I

    .prologue
    .line 212
    const/4 v0, 0x0

    invoke-static {p0, p1, p2, p3, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I
    .locals 6
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "filteringType"    # I
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 203
    const/4 v2, 0x6

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startShopForDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILandroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method public static startPanoSearch(Landroid/app/Activity;)V
    .locals 4
    .param p0, "starter"    # Landroid/app/Activity;

    .prologue
    .line 106
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0}, Landroid/content/Intent;-><init>()V

    new-instance v1, Landroid/content/ComponentName;

    const-string v2, "com.android.vending"

    const-string v3, "com.google.android.finsky.activities.LeanbackPlayMoviesAndTvSearchActivity"

    invoke-direct {v1, v2, v3}, Landroid/content/ComponentName;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setComponent(Landroid/content/ComponentName;)Landroid/content/Intent;

    move-result-object v0

    const/high16 v1, 0x4080000

    invoke-virtual {v0, v1}, Landroid/content/Intent;->setFlags(I)Landroid/content/Intent;

    move-result-object v0

    const/16 v1, 0x3eb

    invoke-virtual {p0, v0, v1}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V

    .line 111
    return-void
.end method

.method public static startSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 3
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "searchQuery"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "eventSource"    # I

    .prologue
    .line 92
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 93
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 94
    invoke-static {p4}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {p1, p2, v2}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createSearchUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 96
    .local v1, "searchUri":Landroid/net/Uri;
    invoke-static {p0, v1, p3}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 97
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p4, p2, v0}, Lcom/google/android/videos/logging/EventLogger;->onPlayStoreSearch(ILjava/lang/String;I)V

    .line 98
    return-void
.end method

.method public static startSeasonDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I
    .locals 6
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "seasonId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "filteringType"    # I
    .param p4, "extra"    # Landroid/os/Bundle;

    .prologue
    .line 251
    const/16 v2, 0x13

    move-object v0, p0

    move-object v1, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startShopForDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILandroid/os/Bundle;)I

    move-result v0

    return v0
.end method

.method private static startShopForDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;ILjava/lang/String;ILandroid/os/Bundle;)I
    .locals 7
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "itemType"    # I
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "filteringType"    # I
    .param p5, "extra"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    .line 269
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getStorePackage(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v3

    .line 270
    .local v3, "storePackage":Ljava/lang/String;
    if-nez v3, :cond_0

    .line 271
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->showPackageMissingError(Landroid/content/Context;)V

    .line 272
    const/16 v4, 0xc

    .line 322
    :goto_0
    return v4

    .line 274
    :cond_0
    new-instance v2, Landroid/content/Intent;

    const-string v4, "com.android.vending.billing.PURCHASE"

    invoke-direct {v2, v4}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    .line 275
    .local v2, "intent":Landroid/content/Intent;
    const-string v4, "android.intent.category.DEFAULT"

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addCategory(Ljava/lang/String;)Landroid/content/Intent;

    .line 276
    const/high16 v4, 0x80000

    invoke-virtual {v2, v4}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    .line 277
    invoke-virtual {v2, v3}, Landroid/content/Intent;->setPackage(Ljava/lang/String;)Landroid/content/Intent;

    .line 279
    if-eqz p5, :cond_1

    .line 280
    invoke-virtual {v2, p5}, Landroid/content/Intent;->putExtras(Landroid/os/Bundle;)Landroid/content/Intent;

    .line 282
    :cond_1
    const-string v4, "authAccount"

    invoke-virtual {v2, v4, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 283
    const-string v4, "backend"

    const/4 v5, 0x4

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 284
    const-string v4, "document_type"

    invoke-virtual {v2, v4, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 285
    const-string v4, "backend_docid"

    invoke-virtual {v2, v4, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 289
    sparse-switch p2, :sswitch_data_0

    .line 303
    new-instance v4, Ljava/security/InvalidParameterException;

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "Invalid itemType:"

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-direct {v4, v5}, Ljava/security/InvalidParameterException;-><init>(Ljava/lang/String;)V

    throw v4

    .line 291
    :sswitch_0
    const-string v1, "movie-"

    .line 306
    .local v1, "fullDocidPrefix":Ljava/lang/String;
    :goto_1
    const-string v4, "full_docid"

    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v5, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 308
    const/4 v4, 0x1

    if-ne p4, v4, :cond_3

    .line 309
    const-string v4, "offer_type"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 310
    const-string v4, "offer_filter"

    const-string v5, "PURCHASE"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    .line 317
    :cond_2
    :goto_2
    const/16 v4, 0x389

    :try_start_0
    invoke-virtual {p0, v2, v4}, Landroid/app/Activity;->startActivityForResult(Landroid/content/Intent;I)V
    :try_end_0
    .catch Landroid/content/ActivityNotFoundException; {:try_start_0 .. :try_end_0} :catch_0

    .line 318
    const/4 v4, -0x1

    goto :goto_0

    .line 295
    .end local v1    # "fullDocidPrefix":Ljava/lang/String;
    :sswitch_1
    const-string v1, "tvseason-"

    .line 296
    .restart local v1    # "fullDocidPrefix":Ljava/lang/String;
    goto :goto_1

    .line 299
    .end local v1    # "fullDocidPrefix":Ljava/lang/String;
    :sswitch_2
    const-string v1, "tvepisode-"

    .line 300
    .restart local v1    # "fullDocidPrefix":Ljava/lang/String;
    goto :goto_1

    .line 311
    :cond_3
    const/4 v4, 0x2

    if-ne p4, v4, :cond_2

    .line 312
    const-string v4, "offer_type"

    invoke-virtual {v2, v4, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;I)Landroid/content/Intent;

    .line 313
    const-string v4, "offer_filter"

    const-string v5, "RENTAL"

    invoke-virtual {v2, v4, v5}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    goto :goto_2

    .line 319
    :catch_0
    move-exception v0

    .line 321
    .local v0, "e":Landroid/content/ActivityNotFoundException;
    const v4, 0x7f0b013e

    invoke-static {p0, v4}, Lcom/google/android/videos/utils/PlayStoreUtil;->showError(Landroid/content/Context;I)V

    .line 322
    const/16 v4, 0xd

    goto/16 :goto_0

    .line 289
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x13 -> :sswitch_1
        0x14 -> :sswitch_2
    .end sparse-switch
.end method

.method public static supportsDirectPurchases(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 188
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getStoreVersionCode(Landroid/content/Context;)I

    move-result v0

    const v1, 0x4c7c152

    if-lt v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static viewEpisodeDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "episodeId"    # Ljava/lang/String;
    .param p4, "account"    # Ljava/lang/String;
    .param p5, "eventSource"    # I

    .prologue
    .line 172
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 173
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 174
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 175
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 176
    invoke-static {p5}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, p2, p3, v0}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createEpisodeDetailsUri(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 178
    .local v6, "showDetailsUri":Landroid/net/Uri;
    invoke-static {p0, v6, p4}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v5

    .line 179
    .local v5, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    move v1, p5

    move-object v2, p3

    move-object v3, p1

    move-object v4, p2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForAsset(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 181
    return-void
.end method

.method public static viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "eventSource"    # I

    .prologue
    const/4 v3, 0x0

    .line 138
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    invoke-static {p3}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createMovieDetailsUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 141
    .local v6, "videoDetailsUri":Landroid/net/Uri;
    invoke-static {p0, v6, p2}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v5

    .line 142
    .local v5, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    move v1, p3

    move-object v2, p1

    move-object v4, v3

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForAsset(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 143
    return-void
.end method

.method public static viewMoviesAndShows(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 3
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "eventSource"    # I

    .prologue
    .line 127
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    invoke-static {p2}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createMoviesAndShowsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 129
    .local v1, "storeUri":Landroid/net/Uri;
    invoke-static {p0, v1, p1}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 130
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p2, v0}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForMoviesAndShows(II)V

    .line 131
    return-void
.end method

.method public static viewMoviesVertical(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 3
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "eventSource"    # I

    .prologue
    .line 117
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 118
    invoke-static {p2}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createMoviesUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 119
    .local v1, "storeUri":Landroid/net/Uri;
    invoke-static {p0, v1, p1}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 120
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p2, v0}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForMovies(II)V

    .line 121
    return-void
.end method

.method public static viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 7
    .param p0, "starter"    # Landroid/content/Context;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "eventSource"    # I

    .prologue
    const/4 v2, 0x0

    .line 160
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 161
    invoke-static {p3}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v0

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createShowDetailsUri(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v6

    .line 163
    .local v6, "showDetailsUri":Landroid/net/Uri;
    invoke-static {p0, v6, p2}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v5

    .line 164
    .local v5, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    move v1, p3

    move-object v3, p1

    move-object v4, v2

    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForAsset(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 165
    return-void
.end method

.method public static viewShowsVertical(Landroid/app/Activity;Ljava/lang/String;I)V
    .locals 3
    .param p0, "starter"    # Landroid/app/Activity;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "eventSource"    # I

    .prologue
    .line 149
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 150
    invoke-static {p2}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/utils/PlayStoreUriCreator;->createShowsUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    .line 151
    .local v1, "storeUri":Landroid/net/Uri;
    invoke-static {p0, v1, p1}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUriInternal(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;)I

    move-result v0

    .line 152
    .local v0, "result":I
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->getEventLogger(Landroid/content/Context;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    invoke-interface {v2, p2, v0}, Lcom/google/android/videos/logging/EventLogger;->onOpenedPlayStoreForShows(II)V

    .line 153
    return-void
.end method
