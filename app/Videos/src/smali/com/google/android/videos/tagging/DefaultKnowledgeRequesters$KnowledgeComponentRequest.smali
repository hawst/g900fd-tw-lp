.class public abstract Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "KnowledgeComponentRequest"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$BytesRequestHandler;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest$RequestHandler;
    }
.end annotation


# instance fields
.field public final knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

.field public final storage:I


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeRequest;I)V
    .locals 0
    .param p1, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "storage"    # I

    .prologue
    .line 349
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 351
    iput p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->storage:I

    .line 352
    return-void
.end method


# virtual methods
.method protected final baseFileName()Ljava/lang/String;
    .locals 2

    .prologue
    .line 357
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "1"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeRequest;->userCountry:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "."

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$KnowledgeComponentRequest;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public abstract toFileName()Ljava/lang/String;
.end method
