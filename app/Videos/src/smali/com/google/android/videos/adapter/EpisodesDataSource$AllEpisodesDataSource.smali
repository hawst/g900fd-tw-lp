.class public final Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;
.super Lcom/google/android/videos/adapter/EpisodesDataSource;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "AllEpisodesDataSource"
.end annotation


# instance fields
.field private cursor:Lcom/google/android/videos/store/MasterSubCursor;

.field private networkConnected:Z


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 245
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/adapter/EpisodesDataSource;-><init>(Lcom/google/android/videos/adapter/EpisodesDataSource$1;)V

    return-void
.end method


# virtual methods
.method public changeCursor(Landroid/database/Cursor;)V
    .locals 1
    .param p1, "newCursor"    # Landroid/database/Cursor;

    .prologue
    .line 265
    if-eqz p1, :cond_0

    instance-of v0, p1, Lcom/google/android/videos/store/MasterSubCursor;

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    move-object v0, p1

    .line 266
    check-cast v0, Lcom/google/android/videos/store/MasterSubCursor;

    iput-object v0, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->cursor:Lcom/google/android/videos/store/MasterSubCursor;

    .line 267
    invoke-super {p0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 268
    return-void

    .line 265
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public createSubRangeDataSource(I)Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;
    .locals 4
    .param p1, "position"    # I

    .prologue
    .line 281
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->getMasterSubCursor(Landroid/database/Cursor;)Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v0

    .line 282
    .local v0, "cursor":Lcom/google/android/videos/store/MasterSubCursor;
    new-instance v1, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    invoke-interface {v0}, Lcom/google/android/videos/store/MasterSubCursor;->getCurrentSubRange()Lcom/google/android/videos/store/MasterSubCursor;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v1, v2, p0, v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;-><init>(Landroid/database/Cursor;Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;Lcom/google/android/videos/adapter/EpisodesDataSource$1;)V

    return-object v1
.end method

.method public isNetworkConnected()Z
    .locals 1

    .prologue
    .line 260
    iget-boolean v0, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->networkConnected:Z

    return v0
.end method

.method public setNetworkConnected(Z)V
    .locals 1
    .param p1, "networkConnected"    # Z

    .prologue
    .line 252
    iget-boolean v0, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->networkConnected:Z

    if-eq v0, p1, :cond_0

    .line 253
    iput-boolean p1, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->networkConnected:Z

    .line 254
    invoke-virtual {p0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->notifyChanged()V

    .line 256
    :cond_0
    return-void
.end method
