.class public Lcom/google/android/videos/store/PurchaseStoreSync;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/PurchaseStoreSync$UnpinUnneededDownloadsTask;,
        Lcom/google/android/videos/store/PurchaseStoreSync$PurgePurchasesTask;,
        Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;,
        Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;,
        Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;,
        Lcom/google/android/videos/store/PurchaseStoreSync$ShouldUnpinQuery;,
        Lcom/google/android/videos/store/PurchaseStoreSync$MyLibraryQuery;,
        Lcom/google/android/videos/store/PurchaseStoreSync$SnapshotTokenQuery;,
        Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    }
.end annotation


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final context:Landroid/content/Context;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

.field private final syncUserLibraryRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final userConfigurationSyncTimes:Ljava/util/concurrent/ConcurrentHashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/ConcurrentHashMap",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Long;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/SyncTaskManager;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/AssetStoreSync;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "syncTaskManager"    # Lcom/google/android/videos/store/SyncTaskManager;
    .param p7, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p8, "assetStoreSync"    # Lcom/google/android/videos/store/AssetStoreSync;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/content/SharedPreferences;",
            "Lcom/google/android/videos/accounts/AccountManagerWrapper;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/UserLibraryRequest;",
            "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/SyncTaskManager;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/store/AssetStoreSync;",
            ")V"
        }
    .end annotation

    .prologue
    .line 243
    .local p5, "syncUserLibraryRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/UserLibraryRequest;Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 244
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;

    .line 245
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/SharedPreferences;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;

    .line 246
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/accounts/AccountManagerWrapper;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 247
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;

    .line 248
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->syncUserLibraryRequester:Lcom/google/android/videos/async/Requester;

    .line 249
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/SyncTaskManager;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    .line 250
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 251
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/AssetStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    .line 253
    new-instance v0, Ljava/util/concurrent/ConcurrentHashMap;

    invoke-direct {v0}, Ljava/util/concurrent/ConcurrentHashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->userConfigurationSyncTimes:Ljava/util/concurrent/ConcurrentHashMap;

    .line 254
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/SharedPreferences;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->preferences:Landroid/content/SharedPreferences;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/store/PurchaseStoreSync;)Ljava/util/concurrent/ConcurrentHashMap;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->userConfigurationSyncTimes:Ljava/util/concurrent/ConcurrentHashMap;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/ConfigurationStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->syncUserLibraryRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/store/PurchaseStoreSync;)Landroid/content/Context;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->context:Landroid/content/Context;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/store/PurchaseStoreSync;)Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/PurchaseStoreSync;

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    return-object v0
.end method


# virtual methods
.method public cleanup()V
    .locals 3

    .prologue
    .line 339
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;

    const/16 v1, 0xbb8

    invoke-static {}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create()Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->schedule()V

    .line 340
    return-void
.end method

.method public cleanup(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 3
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 334
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Ljava/lang/Integer;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Ljava/lang/Integer;Ljava/lang/Void;>;"
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;

    const/16 v1, 0xbb8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v2

    invoke-direct {v0, p0, v1, v2}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$CleanupTask;->schedule()V

    .line 336
    return-void
.end method

.method public setHiddenStateForMovie(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "hidden"    # Z

    .prologue
    .line 343
    if-nez p3, :cond_0

    const-string v6, "account = ? AND asset_type = 6 AND asset_id = ? AND hidden IN (1, 3)"

    .line 345
    .local v6, "whereClause":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create()Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->schedule()V

    .line 347
    return-void

    .line 343
    .end local v6    # "whereClause":Ljava/lang/String;
    :cond_0
    const-string v6, "account = ? AND asset_type = 6 AND asset_id = ? AND NOT (hidden IN (1, 3))"

    goto :goto_0
.end method

.method public setHiddenStateForShow(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "hidden"    # Z

    .prologue
    .line 350
    if-nez p3, :cond_0

    const-string v6, "account = ? AND CASE asset_type WHEN 19 THEN EXISTS (SELECT NULL FROM seasons WHERE season_id = asset_id AND show_id = :itemid) WHEN 20 THEN EXISTS (SELECT NULL FROM videos,seasons WHERE episode_season_id = season_id AND video_id = asset_id AND show_id = :itemid) END AND hidden IN (1, 3)"

    .line 352
    .local v6, "whereClause":Ljava/lang/String;
    :goto_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;

    const/4 v2, 0x0

    invoke-static {}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create()Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    move-object v1, p0

    move-object v4, p1

    move-object v5, p2

    move v7, p3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SetPurchaseVisibilityTask;->schedule()V

    .line 354
    return-void

    .line 350
    .end local v6    # "whereClause":Ljava/lang/String;
    :cond_0
    const-string v6, "account = ? AND CASE asset_type WHEN 19 THEN EXISTS (SELECT NULL FROM seasons WHERE season_id = asset_id AND show_id = :itemid) WHEN 20 THEN EXISTS (SELECT NULL FROM videos,seasons WHERE episode_season_id = season_id AND video_id = asset_id AND show_id = :itemid) END AND NOT (hidden IN (1, 3))"

    goto :goto_0
.end method

.method public syncPurchases(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<-",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 262
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Ljava/lang/String;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<-Ljava/lang/String;Ljava/lang/Void;>;"
    new-instance v1, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    const/16 v2, 0x7d0

    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$1;

    invoke-direct {v0, p0, p2}, Lcom/google/android/videos/store/PurchaseStoreSync$1;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/NewCallback;)V

    invoke-static {p1, v0}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    .line 294
    return-void
.end method

.method public syncPurchasesForSeason(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 324
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    new-instance v1, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    const/16 v2, 0x3e8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-object v0, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    invoke-direct {v1, p0, v2, v3, v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V

    invoke-virtual {v1}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    .line 326
    return-void
.end method

.method public syncPurchasesForVideo(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)V
    .locals 7
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            ">;",
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;>;"
    .local p2, "callback":Lcom/google/android/videos/async/NewCallback;, "Lcom/google/android/videos/async/NewCallback<Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;>;"
    const/4 v5, 0x0

    .line 305
    iget-object v0, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    iget-boolean v0, v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->isFullSync:Z

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 306
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;

    const/16 v2, 0x3e8

    invoke-static {p1, p2}, Lcom/google/android/videos/store/SyncTaskManager$SharedStates;->create(Lcom/google/android/videos/async/ControllableRequest;Lcom/google/android/videos/async/NewCallback;)Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    move-result-object v3

    iget-object v4, p1, Lcom/google/android/videos/async/ControllableRequest;->data:Ljava/lang/Object;

    check-cast v4, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    move-object v1, p0

    move-object v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Lcom/google/android/videos/api/UserLibraryRequest;Lcom/google/android/videos/store/PurchaseStoreSync$1;)V

    invoke-virtual {v0}, Lcom/google/android/videos/store/PurchaseStoreSync$SyncPurchasesTask;->schedule()V

    .line 308
    return-void

    .line 305
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
