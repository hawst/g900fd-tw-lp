.class public Lcom/google/android/videos/ui/ManageDownloadsHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "ManageDownloadsHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/ManageDownloadsHelper$1;,
        Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;,
        Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;
    }
.end annotation


# instance fields
.field private final activity:Landroid/support/v4/app/FragmentActivity;

.field private final cursorHelpers:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;",
            ">;"
        }
    .end annotation
.end field

.field private final database:Lcom/google/android/videos/store/Database;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final flow:Lcom/google/android/videos/flow/Flow;

.field private final listView:Landroid/support/v7/widget/RecyclerView;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingPosterUpdatedNotification:Z

.field private pendingScreenshotUpdatedNotification:Z

.field private pendingShowPosterUpdatedNotification:Z

.field private final pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private final pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

.field private final progressBar:Landroid/view/View;

.field private final storageHeaderFlow:Lcom/google/android/videos/ui/StorageHeaderFlow;

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/ManageDownloadsActivity;Lcom/google/android/videos/VideosGlobals;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 24
    .param p1, "activity"    # Lcom/google/android/videos/activity/ManageDownloadsActivity;
    .param p2, "globals"    # Lcom/google/android/videos/VideosGlobals;
    .param p3, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p4, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 85
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 86
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v4

    .line 87
    .local v4, "config":Lcom/google/android/videos/Config;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v22

    .line 88
    .local v22, "posterStore":Lcom/google/android/videos/store/PosterStore;
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v7

    .line 89
    .local v7, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/support/v4/app/FragmentActivity;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;

    .line 90
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->database:Lcom/google/android/videos/store/Database;

    .line 91
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 92
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 93
    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/SyncHelper;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 94
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 96
    const v3, 0x7f0f0120

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    .line 98
    const v3, 0x7f0f0029

    move-object/from16 v0, p1

    invoke-virtual {v0, v3}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 100
    const/4 v3, 0x0

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    new-instance v6, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v9, 0x9

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, p4

    invoke-direct {v6, v9, v0, v10}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-static {v3, v6}, Lcom/google/android/videos/ui/DownloadItemView;->newMoviesBinder(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)Lcom/google/android/videos/ui/DownloadItemView$Binder;

    move-result-object v20

    .line 106
    .local v20, "moviesBinder":Lcom/google/android/videos/ui/DownloadItemView$Binder;, "Lcom/google/android/videos/ui/DownloadItemView$Binder<Lcom/google/android/videos/adapter/MoviesDataSource;>;"
    const/4 v3, 0x1

    move-object/from16 v0, v22

    invoke-virtual {v0, v3}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v3

    new-instance v6, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v9, 0xa

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-object/from16 v0, p4

    invoke-direct {v6, v9, v0, v10}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-static {v3, v6}, Lcom/google/android/videos/ui/DownloadItemView;->newEpisodesBinder(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)Lcom/google/android/videos/ui/DownloadItemView$Binder;

    move-result-object v17

    .line 112
    .local v17, "episodesBinder":Lcom/google/android/videos/ui/DownloadItemView$Binder;, "Lcom/google/android/videos/ui/DownloadItemView$Binder<Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;>;"
    new-instance v3, Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-direct {v3}, Lcom/google/android/videos/adapter/MoviesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 113
    new-instance v3, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-direct {v3}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    .line 115
    new-instance v5, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v3

    invoke-direct {v5, v3}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    .line 117
    .local v5, "uiHandler":Landroid/os/Handler;
    new-instance v2, Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->database:Lcom/google/android/videos/store/Database;

    const/4 v9, 0x0

    move-object/from16 v3, p1

    move-object/from16 v8, p3

    invoke-direct/range {v2 .. v9}, Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 119
    .local v2, "moviesCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/CursorHelper;->setDownloadedOnly(Z)V

    .line 121
    new-instance v8, Lcom/google/android/videos/ui/CursorHelper$PinnedTvCursorHelper;

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->database:Lcom/google/android/videos/store/Database;

    move-object/from16 v9, p1

    move-object v10, v4

    move-object v11, v5

    move-object v13, v7

    move-object/from16 v14, p3

    invoke-direct/range {v8 .. v14}, Lcom/google/android/videos/ui/CursorHelper$PinnedTvCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 124
    .local v8, "tvCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    new-instance v3, Ljava/util/HashMap;

    invoke-direct {v3}, Ljava/util/HashMap;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    .line 125
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    new-instance v6, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    const/4 v10, 0x0

    invoke-direct {v6, v9, v10}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/ManageDownloadsHelper$1;)V

    invoke-interface {v3, v2, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    new-instance v6, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    move-object/from16 v0, p0

    iget-object v9, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    const/4 v10, 0x0

    invoke-direct {v6, v9, v10}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;-><init>(Lcom/google/android/videos/adapter/VideosDataSource;Lcom/google/android/videos/ui/ManageDownloadsHelper$1;)V

    invoke-interface {v3, v8, v6}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v19, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;

    const/4 v3, 0x0

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    invoke-direct {v0, v1, v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;-><init>(Lcom/google/android/videos/ui/ManageDownloadsHelper;Lcom/google/android/videos/ui/ManageDownloadsHelper$1;)V

    .line 130
    .local v19, "libraryItemClickListener":Lcom/google/android/videos/ui/ManageDownloadsHelper$LibraryItemClickListener;
    new-instance v3, Lcom/google/android/videos/ui/StorageHeaderFlow;

    invoke-direct {v3}, Lcom/google/android/videos/ui/StorageHeaderFlow;-><init>()V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->storageHeaderFlow:Lcom/google/android/videos/ui/StorageHeaderFlow;

    .line 132
    new-instance v21, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    new-instance v9, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v11, 0x7f0b00c2

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, p1

    invoke-direct/range {v9 .. v16}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    const v13, 0x7f040034

    const/16 v16, -0x1

    move-object/from16 v10, v21

    move-object v11, v9

    move-object/from16 v14, v20

    move-object/from16 v15, v19

    invoke-direct/range {v10 .. v16}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    .line 141
    .local v21, "moviesFlow":Lcom/google/android/videos/flow/Flow;
    new-instance v23, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    new-instance v9, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    const v11, 0x7f0b00c3

    const/4 v12, 0x0

    const/4 v13, 0x0

    const/4 v14, 0x0

    const/4 v15, 0x0

    const/16 v16, 0x0

    move-object/from16 v10, p1

    invoke-direct/range {v9 .. v16}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    move-object/from16 v0, p0

    iget-object v12, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    const v13, 0x7f040033

    const/16 v16, -0x1

    move-object/from16 v10, v23

    move-object v11, v9

    move-object/from16 v14, v17

    move-object/from16 v15, v19

    invoke-direct/range {v10 .. v16}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    .line 150
    .local v23, "tvFlow":Lcom/google/android/videos/flow/Flow;
    new-instance v3, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v6, 0x4

    new-array v6, v6, [Lcom/google/android/videos/flow/Flow;

    const/4 v9, 0x0

    new-instance v10, Lcom/google/android/videos/ui/PlayListSpacerFlow;

    const/4 v11, 0x2

    const/4 v12, -0x6

    invoke-direct {v10, v11, v12}, Lcom/google/android/videos/ui/PlayListSpacerFlow;-><init>(II)V

    aput-object v10, v6, v9

    const/4 v9, 0x1

    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->storageHeaderFlow:Lcom/google/android/videos/ui/StorageHeaderFlow;

    aput-object v10, v6, v9

    const/4 v9, 0x2

    aput-object v21, v6, v9

    const/4 v9, 0x3

    aput-object v23, v6, v9

    invoke-direct {v3, v6}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->flow:Lcom/google/android/videos/flow/Flow;

    .line 154
    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 156
    new-instance v18, Lcom/google/android/videos/flow/FlowAdapter;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->flow:Lcom/google/android/videos/flow/Flow;

    move-object/from16 v0, v18

    invoke-direct {v0, v3}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 157
    .local v18, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v3, 0x1

    move-object/from16 v0, v18

    invoke-virtual {v0, v3}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 158
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v6, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v9, "ManageDownloadsActivity"

    invoke-direct {v6, v9}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v3, v6}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 159
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, v18

    invoke-virtual {v3, v0}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 160
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/SyncHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Landroid/support/v4/app/FragmentActivity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->activity:Landroid/support/v4/app/FragmentActivity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/ui/PinHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/ui/ManageDownloadsHelper;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/ui/ManageDownloadsHelper;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method public static getHeaderBottomMargin(Landroid/content/Context;)I
    .locals 1
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 168
    const/4 v0, -0x6

    invoke-static {p0, v0}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getHeaderContentGap(Landroid/content/Context;I)I

    move-result v0

    return v0
.end method

.method public static getHeaderHeight(Landroid/content/Context;)I
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 163
    const/4 v0, 0x2

    const/4 v1, -0x6

    invoke-static {p0, v0, v1}, Lcom/google/android/videos/ui/PlayListSpacerFlow;->getSpacerHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method private isTransitioning()Z
    .locals 1

    .prologue
    .line 296
    const/4 v0, 0x0

    return v0
.end method

.method private updateVideosStorage()V
    .locals 8

    .prologue
    .line 223
    const-wide/16 v4, 0x0

    .line 224
    .local v4, "requiredBytes":J
    const-wide/16 v2, 0x0

    .line 226
    .local v2, "downloadedBytes":J
    iget-object v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v6}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {v6}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    .line 227
    .local v0, "cursorData":Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    iget-wide v6, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->requiredBytes:J

    add-long/2addr v4, v6

    .line 228
    iget-wide v6, v0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->downloadedBytes:J

    add-long/2addr v2, v6

    .line 229
    goto :goto_0

    .line 230
    .end local v0    # "cursorData":Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->storageHeaderFlow:Lcom/google/android/videos/ui/StorageHeaderFlow;

    invoke-virtual {v6, v4, v5, v2, v3}, Lcom/google/android/videos/ui/StorageHeaderFlow;->updateVideosStorage(JJ)V

    .line 231
    return-void
.end method

.method private updateVisibilities()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 258
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    .line 259
    .local v0, "syncState":I
    packed-switch v0, :pswitch_data_0

    .line 286
    :goto_0
    return-void

    .line 263
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 264
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 273
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->hasCursor()Z

    move-result v1

    if-nez v1, :cond_1

    .line 274
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 275
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 276
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_2

    .line 277
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 278
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 280
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 281
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 259
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 212
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    .line 213
    .local v0, "relatedData":Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->isTransitioning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 214
    const/4 v1, 0x1

    # setter for: Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->access$202(Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;Z)Z

    .line 220
    :goto_0
    return-void

    .line 216
    :cond_0
    invoke-virtual {p1}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->changeCursor(Landroid/database/Cursor;)V

    .line 217
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->updateVideosStorage()V

    .line 218
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->updateVisibilities()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 3

    .prologue
    .line 205
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 206
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    goto :goto_0

    .line 208
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;>;"
    :cond_0
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 332
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onPostersUpdated()V

    .line 333
    return-void
.end method

.method public onPostersUpdated()V
    .locals 1

    .prologue
    .line 234
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingPosterUpdatedNotification:Z

    .line 239
    :goto_0
    return-void

    .line 238
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedMoviesDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public onScreenshotUpdated()V
    .locals 1

    .prologue
    .line 250
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 251
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingScreenshotUpdatedNotification:Z

    .line 255
    :goto_0
    return-void

    .line 254
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 342
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onScreenshotUpdated()V

    .line 343
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 337
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onShowPostersUpdated()V

    .line 338
    return-void
.end method

.method public onShowPostersUpdated()V
    .locals 1

    .prologue
    .line 242
    invoke-direct {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 243
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingShowPosterUpdatedNotification:Z

    .line 247
    :goto_0
    return-void

    .line 246
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pinnedTvDataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/EpisodesDataSource$AllEpisodesDataSource;->notifyChanged()V

    goto :goto_0
.end method

.method public final onStart()V
    .locals 3

    .prologue
    .line 173
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 174
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 175
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->keySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/CursorHelper;

    .line 176
    .local v0, "cursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 177
    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    goto :goto_0

    .line 180
    .end local v0    # "cursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v2

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->onSyncStateChanged(I)V

    .line 181
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/videos/ui/ManageDownloadsHelper;->update()V

    .line 183
    return-void
.end method

.method public onStop()V
    .locals 5

    .prologue
    const/16 v3, 0x8

    const/4 v4, 0x0

    .line 187
    iput-boolean v4, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingPosterUpdatedNotification:Z

    .line 188
    iput-boolean v4, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingShowPosterUpdatedNotification:Z

    .line 189
    iput-boolean v4, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->pendingScreenshotUpdatedNotification:Z

    .line 190
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 191
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 192
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v2}, Ljava/util/Map;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 193
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    # setter for: Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->pendingCursor:Z
    invoke-static {v2, v4}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->access$202(Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;Z)Z

    .line 194
    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->changeCursor(Landroid/database/Cursor;)V

    .line 195
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 196
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    goto :goto_0

    .line 198
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Lcom/google/android/videos/ui/CursorHelper<*>;Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;>;"
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 199
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 200
    iget-object v2, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 201
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 0
    .param p1, "state"    # I

    .prologue
    .line 328
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 0
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 293
    return-void
.end method

.method public update()V
    .locals 4

    .prologue
    .line 347
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v3}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    .line 348
    .local v1, "isConnected":Z
    iget-object v3, p0, Lcom/google/android/videos/ui/ManageDownloadsHelper;->cursorHelpers:Ljava/util/Map;

    invoke-interface {v3}, Ljava/util/Map;->values()Ljava/util/Collection;

    move-result-object v3

    invoke-interface {v3}, Ljava/util/Collection;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;

    .line 349
    .local v2, "relatedData":Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    # getter for: Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->dataSource:Lcom/google/android/videos/adapter/VideosDataSource;
    invoke-static {v2}, Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;->access$300(Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;)Lcom/google/android/videos/adapter/VideosDataSource;

    move-result-object v3

    invoke-interface {v3, v1}, Lcom/google/android/videos/adapter/VideosDataSource;->setNetworkConnected(Z)V

    goto :goto_0

    .line 351
    .end local v2    # "relatedData":Lcom/google/android/videos/ui/ManageDownloadsHelper$CursorHelperRelatedData;
    :cond_0
    return-void
.end method
