.class interface abstract Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;
.super Ljava/lang/Object;
.source "MovieDetailsActivity.java"

# interfaces
.implements Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PurchaseQuery"
.end annotation


# static fields
.field public static final ACTORS:I

.field public static final BADGE_CAPTION:I

.field public static final BADGE_KNOWLEDGE:I

.field public static final BADGE_SURROUND_SOUND:I

.field public static final COLUMNS:[Ljava/lang/String;

.field public static final DESCRIPTION:I

.field public static final DIRECTORS:I

.field public static final FORMAT_TYPE:I

.field public static final PRODUCERS:I

.field public static final PURCHASE_TYPE:I

.field public static final RATING_NAME:I

.field public static final RELEASE_YEAR:I

.field public static final TITLE:I

.field public static final VIDEO_DURATION_SECONDS:I

.field public static final WRITERS:I


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    .line 1077
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    const/16 v1, 0xe

    new-array v1, v1, [Ljava/lang/String;

    const/4 v2, 0x0

    const-string v3, "title"

    aput-object v3, v1, v2

    const/4 v2, 0x1

    const-string v3, "description"

    aput-object v3, v1, v2

    const/4 v2, 0x2

    const-string v3, "writers"

    aput-object v3, v1, v2

    const/4 v2, 0x3

    const-string v3, "directors"

    aput-object v3, v1, v2

    const/4 v2, 0x4

    const-string v3, "actors"

    aput-object v3, v1, v2

    const/4 v2, 0x5

    const-string v3, "producers"

    aput-object v3, v1, v2

    const/4 v2, 0x6

    const-string v3, "release_year"

    aput-object v3, v1, v2

    const/4 v2, 0x7

    const-string v3, "duration_seconds"

    aput-object v3, v1, v2

    const/16 v2, 0x8

    const-string v3, "rating_name"

    aput-object v3, v1, v2

    const/16 v2, 0x9

    const-string v3, "format_type"

    aput-object v3, v1, v2

    const/16 v2, 0xa

    const-string v3, "badge_surround_sound"

    aput-object v3, v1, v2

    const/16 v2, 0xb

    const-string v3, "badge_knowledge"

    aput-object v3, v1, v2

    const/16 v2, 0xc

    const-string v3, "has_subtitles"

    aput-object v3, v1, v2

    const/16 v2, 0xd

    const-string v3, "purchase_type"

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->extend([Ljava/lang/String;[Ljava/lang/String;)[Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    .line 1094
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x0

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->TITLE:I

    .line 1095
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x1

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->DESCRIPTION:I

    .line 1096
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x2

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->WRITERS:I

    .line 1097
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x3

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->DIRECTORS:I

    .line 1098
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x4

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->ACTORS:I

    .line 1099
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x5

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->PRODUCERS:I

    .line 1100
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x6

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->RELEASE_YEAR:I

    .line 1101
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x7

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->VIDEO_DURATION_SECONDS:I

    .line 1102
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x8

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->RATING_NAME:I

    .line 1103
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0x9

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->FORMAT_TYPE:I

    .line 1104
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xa

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_SURROUND_SOUND:I

    .line 1105
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xb

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_KNOWLEDGE:I

    .line 1106
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xc

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->BADGE_CAPTION:I

    .line 1107
    sget-object v0, Lcom/google/android/videos/activity/MovieDetailsActivity$DownloadStatusQuery;->COLUMNS:[Ljava/lang/String;

    array-length v0, v0

    add-int/lit8 v0, v0, 0xd

    sput v0, Lcom/google/android/videos/activity/MovieDetailsActivity$PurchaseQuery;->PURCHASE_TYPE:I

    return-void
.end method
