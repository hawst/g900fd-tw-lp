.class public Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;
.super Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;
.source "DetailsDescriptionPresenter.java"

# interfaces
.implements Landroid/view/View$OnLayoutChangeListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;
    }
.end annotation


# instance fields
.field private body:Landroid/widget/TextView;

.field private final config:Lcom/google/android/videos/Config;

.field private final handler:Landroid/os/Handler;

.field private final listener:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;

.field private title:Landroid/widget/TextView;

.field private titleFontSizeChanged:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;Lcom/google/android/videos/Config;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 53
    invoke-direct {p0}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->listener:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;

    .line 55
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->config:Lcom/google/android/videos/Config;

    .line 56
    new-instance v0, Landroid/os/Handler;

    invoke-direct {v0}, Landroid/os/Handler;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->handler:Landroid/os/Handler;

    .line 57
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;)Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->listener:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;Landroid/widget/TextView;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;
    .param p1, "x1"    # Landroid/widget/TextView;
    .param p2, "x2"    # I

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextSize(Landroid/widget/TextView;I)V

    return-void
.end method

.method static synthetic access$302(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;
    .param p1, "x1"    # Z

    .prologue
    .line 37
    iput-boolean p1, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->titleFontSizeChanged:Z

    return p1
.end method

.method private bindRatings(Landroid/view/View;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;)V
    .locals 12
    .param p1, "view"    # Landroid/view/View;
    .param p2, "ratings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .prologue
    const/4 v7, 0x2

    const/4 v8, 0x1

    const/16 v11, 0x8

    const/4 v10, 0x0

    .line 219
    const v6, 0x7f0f00e8

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    check-cast v3, Landroid/widget/ImageView;

    .line 220
    .local v3, "tomatoView":Landroid/widget/ImageView;
    const v6, 0x7f0f0137

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/TextView;

    .line 221
    .local v5, "tomatoesView":Landroid/widget/TextView;
    const v6, 0x7f0f0138

    invoke-virtual {p1, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/StarRatingBar;

    .line 223
    .local v1, "starsView":Lcom/google/android/play/layout/StarRatingBar;
    const/4 v6, 0x3

    invoke-static {p2, v7, v6}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v4

    .line 225
    .local v4, "tomatoes":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    invoke-static {p2, v8, v7}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v0

    .line 228
    .local v0, "stars":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-eqz v4, :cond_1

    .line 229
    invoke-virtual {v3, v10}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 230
    invoke-virtual {v5, v10}, Landroid/widget/TextView;->setVisibility(I)V

    .line 232
    invoke-virtual {p1}, Landroid/view/View;->getResources()Landroid/content/res/Resources;

    move-result-object v7

    iget-object v6, v4, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    iget-boolean v6, v6, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eqz v6, :cond_0

    const v6, 0x7f0200ad

    :goto_0
    invoke-virtual {v7, v6}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v2

    .line 234
    .local v2, "tomato":Landroid/graphics/drawable/Drawable;
    invoke-virtual {v3, v2}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 235
    invoke-virtual {p1}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v6

    const v7, 0x7f0b0190

    new-array v8, v8, [Ljava/lang/Object;

    iget v9, v4, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    float-to-int v9, v9

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v9

    aput-object v9, v8, v10

    invoke-virtual {v6, v7, v8}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 242
    .end local v2    # "tomato":Landroid/graphics/drawable/Drawable;
    :goto_1
    if-eqz v0, :cond_2

    .line 243
    invoke-virtual {v1, v10}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 244
    iget v6, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-virtual {v1, v6}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 248
    :goto_2
    return-void

    .line 232
    :cond_0
    const v6, 0x7f020107

    goto :goto_0

    .line 238
    :cond_1
    invoke-virtual {v3, v11}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 239
    invoke-virtual {v5, v11}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_1

    .line 246
    :cond_2
    invoke-virtual {v1, v11}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_2
.end method

.method private static getRentalExpirationDescription(Landroid/content/Context;Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;)Ljava/lang/String;
    .locals 11
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "cheapestOfferOfType"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .prologue
    const/4 v5, 0x2

    const/4 v10, 0x0

    const/4 v7, 0x1

    .line 178
    if-eqz p1, :cond_0

    iget-object v4, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->offerType:I

    if-ne v4, v7, :cond_0

    .line 179
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    .line 180
    .local v3, "resources":Landroid/content/res/Resources;
    iget-object v2, p1, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    .line 181
    .local v2, "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    iget v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalPolicy:I

    packed-switch v4, :pswitch_data_0

    .line 210
    .end local v2    # "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .end local v3    # "resources":Landroid/content/res/Resources;
    :cond_0
    const/4 v4, 0x0

    :goto_0
    return-object v4

    .line 183
    .restart local v2    # "offer":Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .restart local v3    # "resources":Landroid/content/res/Resources;
    :pswitch_0
    iget v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-static {v4}, Lcom/google/android/videos/utils/TimeUtil;->toDaysFromSeconds(I)I

    move-result v0

    .line 184
    .local v0, "days":I
    if-lez v0, :cond_1

    .line 185
    const v4, 0x7f0b01fc

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 188
    :cond_1
    const v4, 0x7f0b01fb

    new-array v5, v7, [Ljava/lang/Object;

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-static {v6}, Lcom/google/android/videos/utils/TimeUtil;->toHoursFromSeconds(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 192
    .end local v0    # "days":I
    :pswitch_1
    iget v4, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    invoke-static {v4}, Lcom/google/android/videos/utils/TimeUtil;->toDaysFromSeconds(I)I

    move-result v1

    .line 193
    .local v1, "finishDays":I
    if-lez v1, :cond_2

    .line 194
    const v4, 0x7f0b01fe

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-static {v6}, Lcom/google/android/videos/utils/TimeUtil;->toDaysFromSeconds(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    invoke-static {v6}, Lcom/google/android/videos/utils/TimeUtil;->toDaysFromSeconds(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 199
    :cond_2
    const v4, 0x7f0b01fd

    new-array v5, v5, [Ljava/lang/Object;

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalLongTimerSec:I

    invoke-static {v6}, Lcom/google/android/videos/utils/TimeUtil;->toDaysFromSeconds(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v10

    iget v6, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalShortTimerSec:I

    invoke-static {v6}, Lcom/google/android/videos/utils/TimeUtil;->toHoursFromSeconds(I)I

    move-result v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v5, v7

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto :goto_0

    .line 205
    .end local v1    # "finishDays":I
    :pswitch_2
    const v4, 0x7f0b01ff

    new-array v5, v7, [Ljava/lang/Object;

    invoke-static {}, Ljava/text/DateFormat;->getDateTimeInstance()Ljava/text/DateFormat;

    move-result-object v6

    new-instance v7, Ljava/util/Date;

    iget-wide v8, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->rentalExpirationTimestampSec:J

    invoke-direct {v7, v8, v9}, Ljava/util/Date;-><init>(J)V

    invoke-virtual {v6, v7}, Ljava/text/DateFormat;->format(Ljava/util/Date;)Ljava/lang/String;

    move-result-object v6

    aput-object v6, v5, v10

    invoke-virtual {v3, v4, v5}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v4

    goto/16 :goto_0

    .line 181
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_0
        :pswitch_1
        :pswitch_2
    .end packed-switch
.end method

.method private setTextOrVisibilityGone(Landroid/widget/TextView;Ljava/lang/String;)V
    .locals 1
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "text"    # Ljava/lang/String;

    .prologue
    .line 214
    invoke-virtual {p1, p2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 215
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {p1, v0}, Landroid/widget/TextView;->setVisibility(I)V

    .line 216
    return-void

    .line 215
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method private setTextSize(Landroid/widget/TextView;I)V
    .locals 2
    .param p1, "textView"    # Landroid/widget/TextView;
    .param p2, "dimensionResourceId"    # I

    .prologue
    .line 171
    invoke-virtual {p1}, Landroid/widget/TextView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-virtual {v1, p2}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v0

    .line 172
    .local v0, "titleTextSize":F
    const/4 v1, 0x0

    invoke-virtual {p1, v1, v0}, Landroid/widget/TextView;->setTextSize(IF)V

    .line 173
    return-void
.end method


# virtual methods
.method protected onBindDescription(Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 12
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 61
    instance-of v7, p2, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;

    if-nez v7, :cond_0

    .line 120
    :goto_0
    return-void

    .line 65
    :cond_0
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    invoke-virtual {v7}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    .local v1, "context":Landroid/content/Context;
    move-object v4, p2

    .line 66
    check-cast v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;

    .line 67
    .local v4, "object":Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;
    iget-object v0, v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 68
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v3, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 69
    .local v3, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getTitle()Landroid/widget/TextView;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    .line 70
    invoke-virtual {p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->getBody()Landroid/widget/TextView;

    move-result-object v7

    iput-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    .line 71
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f00f7

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v5

    check-cast v5, Landroid/widget/ImageView;

    .line 72
    .local v5, "subtitlesBadgeView":Landroid/widget/ImageView;
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f0136

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    check-cast v6, Landroid/widget/ImageView;

    .line 74
    .local v6, "surroundBadgeView":Landroid/widget/ImageView;
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    new-instance v8, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$1;

    invoke-direct {v8, p0}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$1;-><init>(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;)V

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 83
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f0135

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/android/videos/pano/ui/PanoHelper;->getContentRating(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextOrVisibilityGone(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 86
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f0133

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-static {v3}, Lcom/google/android/videos/api/AssetResourceUtil;->getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I

    move-result v8

    invoke-static {v8}, Lcom/google/android/videos/pano/ui/PanoHelper;->formatReleaseYear(I)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextOrVisibilityGone(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 89
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f0134

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    iget v8, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    invoke-static {v8, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->formatVideoDuration(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v8

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextOrVisibilityGone(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 92
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    iget-object v8, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->description:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 94
    iget-object v7, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    if-eqz v7, :cond_1

    .line 95
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->bindRatings(Landroid/view/View;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;)V

    .line 98
    :cond_1
    iget-object v7, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-boolean v7, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->hasCaption:Z

    if-eqz v7, :cond_2

    const/4 v7, 0x0

    :goto_1
    invoke-virtual {v5, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 99
    iget-object v7, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    if-eqz v7, :cond_3

    iget-object v7, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->badge:Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;

    iget-boolean v7, v7, Lcom/google/wireless/android/video/magma/proto/AssetResource$Badge;->audio51:Z

    if-eqz v7, :cond_3

    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->config:Lcom/google/android/videos/Config;

    invoke-interface {v7}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v7

    if-eqz v7, :cond_3

    const/4 v7, 0x0

    :goto_2
    invoke-virtual {v6, v7}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 103
    iget-boolean v7, v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->preorder:Z

    if-eqz v7, :cond_4

    .line 104
    const v7, 0x7f0b0156

    invoke-virtual {v1, v7}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 111
    .local v2, "extraInformation":Ljava/lang/String;
    :goto_3
    iget-object v7, p1, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter$ViewHolder;->view:Landroid/view/View;

    const v8, 0x7f0f013b

    invoke-virtual {v7, v8}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v7

    check-cast v7, Landroid/widget/TextView;

    invoke-direct {p0, v7, v2}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextOrVisibilityGone(Landroid/widget/TextView;Ljava/lang/String;)V

    .line 115
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    const v8, 0x7f0e005c

    invoke-direct {p0, v7, v8}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->setTextSize(Landroid/widget/TextView;I)V

    .line 116
    const/4 v7, 0x0

    iput-boolean v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->titleFontSizeChanged:Z

    .line 117
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v8, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v7, v8}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 118
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 119
    iget-object v7, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    invoke-virtual {v7, p0}, Landroid/widget/TextView;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    goto/16 :goto_0

    .line 98
    .end local v2    # "extraInformation":Ljava/lang/String;
    :cond_2
    const/16 v7, 0x8

    goto :goto_1

    .line 99
    :cond_3
    const/16 v7, 0x8

    goto :goto_2

    .line 105
    :cond_4
    iget-wide v8, v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->expirationTimestamp:J

    const-wide v10, 0x7fffffffffffffffL

    cmp-long v7, v8, v10

    if-nez v7, :cond_6

    .line 106
    iget-boolean v7, v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->purchased:Z

    if-eqz v7, :cond_5

    const/4 v2, 0x0

    .restart local v2    # "extraInformation":Ljava/lang/String;
    :goto_4
    goto :goto_3

    .end local v2    # "extraInformation":Ljava/lang/String;
    :cond_5
    const/4 v7, 0x0

    iget-object v8, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v9, 0x1

    invoke-static {v7, v8, v9}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v7

    invoke-static {v1, v7}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->getRentalExpirationDescription(Landroid/content/Context;Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;)Ljava/lang/String;

    move-result-object v2

    goto :goto_4

    .line 109
    :cond_6
    iget-wide v8, v4, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->expirationTimestamp:J

    invoke-static {v8, v9, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getExpirationTitle(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    .restart local v2    # "extraInformation":Ljava/lang/String;
    goto :goto_3
.end method

.method public onLayoutChange(Landroid/view/View;IIIIIIII)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;
    .param p2, "left"    # I
    .param p3, "top"    # I
    .param p4, "right"    # I
    .param p5, "bottom"    # I
    .param p6, "oldLeft"    # I
    .param p7, "oldTop"    # I
    .param p8, "oldRight"    # I
    .param p9, "oldBottom"    # I

    .prologue
    .line 141
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    if-ne p1, v2, :cond_1

    .line 143
    sub-int v2, p4, p2

    if-lez v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLineCount()I

    move-result v2

    const/4 v3, 0x1

    if-le v2, v3, :cond_0

    iget-boolean v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->titleFontSizeChanged:Z

    if-nez v2, :cond_0

    .line 145
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->handler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$2;

    invoke-direct {v3, p0}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$2;-><init>(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 168
    :cond_0
    :goto_0
    return-void

    .line 155
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    if-ne p1, v2, :cond_0

    .line 160
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    invoke-virtual {v2}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v1

    .line 161
    .local v1, "layout":Landroid/text/Layout;
    sub-int v0, p5, p3

    .line 164
    .local v0, "height":I
    invoke-virtual {v1}, Landroid/text/Layout;->getLineCount()I

    move-result v2

    add-int/lit8 v2, v2, -0x1

    invoke-virtual {v1, v2}, Landroid/text/Layout;->getLineBottom(I)I

    move-result v2

    if-gt v2, v0, :cond_0

    .line 165
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setFocusable(Z)V

    goto :goto_0
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    const/4 v1, 0x0

    .line 124
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    if-eqz v0, :cond_0

    .line 125
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 126
    iput-object v1, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->title:Landroid/widget/TextView;

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    if-eqz v0, :cond_1

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    invoke-virtual {v0, p0}, Landroid/widget/TextView;->removeOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 130
    iput-object v1, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->body:Landroid/widget/TextView;

    .line 132
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;->handler:Landroid/os/Handler;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 133
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/AbstractDetailsDescriptionPresenter;->onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 134
    return-void
.end method
