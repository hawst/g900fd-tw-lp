.class public Lcom/google/android/videos/player/logging/YouTubeStatsHelper;
.super Ljava/lang/Object;
.source "YouTubeStatsHelper.java"


# static fields
.field private static final FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

.field private static final RANDOM:Ljava/util/Random;

.field private static final TIME_FORMAT:Ljava/text/NumberFormat;


# direct methods
.method static constructor <clinit>()V
    .locals 4

    .prologue
    const/4 v3, 0x3

    const/4 v2, 0x2

    const/4 v1, 0x0

    .line 47
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->TIME_FORMAT:Ljava/text/NumberFormat;

    .line 48
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->TIME_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v3}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 49
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->TIME_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v3}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 50
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->TIME_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 51
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/text/NumberFormat;->getInstance(Ljava/util/Locale;)Ljava/text/NumberFormat;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

    .line 52
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMinimumFractionDigits(I)V

    .line 53
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v2}, Ljava/text/NumberFormat;->setMaximumFractionDigits(I)V

    .line 54
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

    invoke-virtual {v0, v1}, Ljava/text/NumberFormat;->setGroupingUsed(Z)V

    .line 66
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    sput-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->RANDOM:Ljava/util/Random;

    return-void
.end method

.method public static appendSessionNonce(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p0, "uri"    # Landroid/net/Uri;
    .param p1, "sessionNonce"    # Ljava/lang/String;

    .prologue
    .line 84
    invoke-virtual {p0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "cpn"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method static clearClientParameters(Lcom/google/android/videos/utils/UriBuilder;)V
    .locals 2
    .param p0, "builder"    # Lcom/google/android/videos/utils/UriBuilder;

    .prologue
    .line 123
    const-string v0, "cplatform"

    invoke-virtual {p0, v0}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "c"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cos"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cosver"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cbrand"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cmodel"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cver"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cbr"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cbrver"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->deleteQueryParameters(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 132
    return-void
.end method

.method public static generateSessionNonce()Ljava/lang/String;
    .locals 2

    .prologue
    .line 78
    const/16 v1, 0xc

    new-array v0, v1, [B

    .line 79
    .local v0, "randomBytes":[B
    sget-object v1, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->RANDOM:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextBytes([B)V

    .line 80
    const/16 v1, 0xa

    invoke-static {v0, v1}, Landroid/util/Base64;->encodeToString([BI)Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method

.method static getFractionString(F)Ljava/lang/String;
    .locals 4
    .param p0, "fraction"    # F

    .prologue
    .line 171
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->FRACTIONAL_FORMAT:Ljava/text/NumberFormat;

    float-to-double v2, p0

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static getStatsUriBuilder(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;
    .locals 3
    .param p0, "baseUri"    # Ljava/lang/String;
    .param p1, "serviceName"    # Ljava/lang/String;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "videoId"    # Ljava/lang/String;

    .prologue
    .line 90
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-direct {v0, p0}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    invoke-virtual {v0, p1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "ns"

    const-string v2, "gp"

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "cpn"

    invoke-virtual {v0, v1, p2}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    const-string v1, "docid"

    invoke-virtual {v0, v1, p3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    return-object v0
.end method

.method static getTimeString(I)Ljava/lang/String;
    .locals 4
    .param p0, "timeMs"    # I

    .prologue
    .line 175
    sget-object v0, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->TIME_FORMAT:Ljava/text/NumberFormat;

    int-to-float v1, p0

    const/high16 v2, 0x447a0000    # 1000.0f

    div-float/2addr v1, v2

    float-to-double v2, v1

    invoke-virtual {v0, v2, v3}, Ljava/text/NumberFormat;->format(D)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static setClientParameters(Lcom/google/android/videos/utils/UriBuilder;IILjava/lang/String;Ljava/lang/String;)V
    .locals 5
    .param p0, "builder"    # Lcom/google/android/videos/utils/UriBuilder;
    .param p1, "deviceType"    # I
    .param p2, "playerType"    # I
    .param p3, "appVersion"    # Ljava/lang/String;
    .param p4, "packageName"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x2

    .line 99
    if-ne p1, v2, :cond_0

    const-string v0, "tv"

    .line 102
    .local v0, "platform":Ljava/lang/String;
    :goto_0
    if-ne p1, v2, :cond_2

    const-string v1, "tvandroid"

    .line 104
    .local v1, "softwareInterface":Ljava/lang/String;
    :goto_1
    if-ne p2, v3, :cond_3

    .line 105
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/E1.0.13"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    .line 111
    :goto_2
    const-string v2, "cplatform"

    invoke-virtual {p0, v2, v0}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "c"

    invoke-virtual {v2, v3, v1}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cos"

    const-string v4, "Android"

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cosver"

    sget-object v4, Landroid/os/Build$VERSION;->RELEASE:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cbrand"

    sget-object v4, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cmodel"

    sget-object v4, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v2, v3, v4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cver"

    invoke-virtual {v2, v3, p3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cbr"

    invoke-virtual {v2, v3, p4}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v2

    const-string v3, "cbrver"

    invoke-virtual {v2, v3, p3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 120
    return-void

    .line 99
    .end local v0    # "platform":Ljava/lang/String;
    .end local v1    # "softwareInterface":Ljava/lang/String;
    :cond_0
    if-ne p1, v3, :cond_1

    const-string v0, "tablet"

    goto :goto_0

    :cond_1
    const-string v0, "mobile"

    goto :goto_0

    .line 102
    .restart local v0    # "platform":Ljava/lang/String;
    :cond_2
    const-string v1, "android"

    goto :goto_1

    .line 106
    .restart local v1    # "softwareInterface":Ljava/lang/String;
    :cond_3
    if-ne p2, v2, :cond_4

    .line 107
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/F1.0.13"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_2

    .line 109
    :cond_4
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, "/L"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object p3

    goto :goto_2
.end method

.method static toStatsConnectionType(II)I
    .locals 1
    .param p0, "type"    # I
    .param p1, "subtype"    # I

    .prologue
    const/4 v0, 0x6

    .line 135
    packed-switch p0, :pswitch_data_0

    .line 166
    :pswitch_0
    const/4 v0, 0x1

    :goto_0
    :pswitch_1
    return v0

    .line 137
    :pswitch_2
    const/4 v0, 0x2

    goto :goto_0

    .line 139
    :pswitch_3
    const/4 v0, 0x3

    goto :goto_0

    .line 143
    :pswitch_4
    packed-switch p1, :pswitch_data_1

    .line 163
    const/4 v0, 0x7

    goto :goto_0

    .line 146
    :pswitch_5
    const/4 v0, 0x4

    goto :goto_0

    .line 159
    :pswitch_6
    const/4 v0, 0x5

    goto :goto_0

    .line 135
    nop

    :pswitch_data_0
    .packed-switch -0x1
        :pswitch_2
        :pswitch_4
        :pswitch_3
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_1
    .end packed-switch

    .line 143
    :pswitch_data_1
    .packed-switch 0x1
        :pswitch_5
        :pswitch_5
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_6
        :pswitch_1
        :pswitch_6
        :pswitch_6
    .end packed-switch
.end method
