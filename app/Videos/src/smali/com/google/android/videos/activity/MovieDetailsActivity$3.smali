.class Lcom/google/android/videos/activity/MovieDetailsActivity$3;
.super Ljava/lang/Object;
.source "MovieDetailsActivity.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V
    .locals 0

    .prologue
    .line 349
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method private processResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 360
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # getter for: Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$400(Lcom/google/android/videos/activity/MovieDetailsActivity;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    if-eq p1, v0, :cond_1

    .line 371
    :cond_0
    :goto_0
    return-void

    .line 363
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/activity/MovieDetailsActivity;->currentPurchaseRequest:Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-static {v0, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$402(Lcom/google/android/videos/activity/MovieDetailsActivity;Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .line 364
    if-eqz p2, :cond_2

    .line 365
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onPurchaseCursor(Landroid/database/Cursor;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$500(Lcom/google/android/videos/activity/MovieDetailsActivity;Landroid/database/Cursor;)V

    .line 367
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # getter for: Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z
    invoke-static {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$600(Lcom/google/android/videos/activity/MovieDetailsActivity;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 368
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    const/4 v1, 0x0

    # setter for: Lcom/google/android/videos/activity/MovieDetailsActivity;->pendingPurchaseRequest:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$602(Lcom/google/android/videos/activity/MovieDetailsActivity;Z)Z

    .line 369
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->refreshPurchase()V
    invoke-static {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$700(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    goto :goto_0
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 357
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->processResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    .line 358
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 349
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 352
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->processResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    .line 353
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 349
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity$3;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method
