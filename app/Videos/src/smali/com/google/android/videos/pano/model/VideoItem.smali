.class public Lcom/google/android/videos/pano/model/VideoItem;
.super Lcom/google/android/videos/pano/model/Item;
.source "VideoItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# static fields
.field static final NO_RATINGS:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;


# instance fields
.field private final aggregatedRatings:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

.field private final badgeResourceId:I

.field private final cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

.field private final durationSec:I

.field private final expirationTimestamp:J

.field private hashCode:I

.field private final height:I

.field public final id:Ljava/lang/String;

.field public final intent:Landroid/content/Intent;

.field private final isMovie:Z

.field public final posterUri:Ljava/lang/String;

.field private final progress:F

.field private final releaseYear:I

.field public final subtitle:Ljava/lang/String;

.field public final title:Ljava/lang/String;

.field public final wallpaperUri:Ljava/lang/String;

.field private final width:I


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 23
    const/4 v0, 0x0

    new-array v0, v0, [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    sput-object v0, Lcom/google/android/videos/pano/model/VideoItem;->NO_RATINGS:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;)V
    .locals 18
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "wallpaperUri"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "intent"    # Landroid/content/Intent;
    .param p9, "isMovie"    # Z
    .param p10, "expirationTimestamp"    # J
    .param p12, "releaseYear"    # I
    .param p13, "durationSec"    # I
    .param p14, "progress"    # F
    .param p15, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p16, "aggregatedRatings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .prologue
    .line 57
    const/16 v17, 0x0

    move-object/from16 v0, p0

    move-object/from16 v1, p1

    move-object/from16 v2, p2

    move-object/from16 v3, p3

    move-object/from16 v4, p4

    move-object/from16 v5, p5

    move/from16 v6, p6

    move/from16 v7, p7

    move-object/from16 v8, p8

    move/from16 v9, p9

    move-wide/from16 v10, p10

    move/from16 v12, p12

    move/from16 v13, p13

    move/from16 v14, p14

    move-object/from16 v15, p15

    move-object/from16 v16, p16

    invoke-direct/range {v0 .. v17}, Lcom/google/android/videos/pano/model/VideoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V

    .line 60
    return-void
.end method

.method constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;ZJIIFLcom/google/android/videos/utils/OfferUtil$CheapestOffer;[Lcom/google/wireless/android/video/magma/proto/ViewerRating;I)V
    .locals 2
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "wallpaperUri"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "intent"    # Landroid/content/Intent;
    .param p9, "isMovie"    # Z
    .param p10, "expirationTimestamp"    # J
    .param p12, "releaseYear"    # I
    .param p13, "durationSec"    # I
    .param p14, "progress"    # F
    .param p15, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p16, "aggregatedRatings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .param p17, "badgeResourceId"    # I

    .prologue
    .line 66
    invoke-direct {p0}, Lcom/google/android/videos/pano/model/Item;-><init>()V

    .line 67
    iput-object p1, p0, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    .line 68
    iput-object p2, p0, Lcom/google/android/videos/pano/model/VideoItem;->title:Ljava/lang/String;

    .line 69
    iput-object p3, p0, Lcom/google/android/videos/pano/model/VideoItem;->subtitle:Ljava/lang/String;

    .line 70
    iput-object p4, p0, Lcom/google/android/videos/pano/model/VideoItem;->posterUri:Ljava/lang/String;

    .line 71
    iput-object p5, p0, Lcom/google/android/videos/pano/model/VideoItem;->wallpaperUri:Ljava/lang/String;

    .line 72
    iput p6, p0, Lcom/google/android/videos/pano/model/VideoItem;->width:I

    .line 73
    iput p7, p0, Lcom/google/android/videos/pano/model/VideoItem;->height:I

    .line 74
    iput-object p8, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    .line 75
    iput-boolean p9, p0, Lcom/google/android/videos/pano/model/VideoItem;->isMovie:Z

    .line 76
    iput-wide p10, p0, Lcom/google/android/videos/pano/model/VideoItem;->expirationTimestamp:J

    .line 77
    iput p12, p0, Lcom/google/android/videos/pano/model/VideoItem;->releaseYear:I

    .line 78
    iput p13, p0, Lcom/google/android/videos/pano/model/VideoItem;->durationSec:I

    .line 79
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    .line 80
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->aggregatedRatings:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .line 81
    move/from16 v0, p14

    iput v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->progress:F

    .line 82
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->badgeResourceId:I

    .line 83
    return-void
.end method

.method public static getStarRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;)F
    .locals 3
    .param p0, "ratings"    # [Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    .prologue
    .line 86
    const/4 v1, 0x1

    const/4 v2, 0x2

    invoke-static {p0, v1, v2}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v0

    .line 88
    .local v0, "starRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return v1

    :cond_0
    iget v1, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    goto :goto_0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 6
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 163
    if-ne p1, p0, :cond_1

    .line 164
    const/4 v1, 0x1

    .line 186
    :cond_0
    :goto_0
    return v1

    .line 166
    :cond_1
    instance-of v2, p1, Lcom/google/android/videos/pano/model/VideoItem;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 169
    check-cast v0, Lcom/google/android/videos/pano/model/VideoItem;

    .line 171
    .local v0, "item":Lcom/google/android/videos/pano/model/VideoItem;
    iget v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    if-eqz v2, :cond_2

    iget v2, v0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    if-eqz v2, :cond_2

    iget v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    iget v3, v0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    if-ne v2, v3, :cond_0

    .line 175
    :cond_2
    iget-boolean v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->isMovie:Z

    iget-boolean v3, v0, Lcom/google/android/videos/pano/model/VideoItem;->isMovie:Z

    if-ne v2, v3, :cond_0

    iget-wide v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->expirationTimestamp:J

    iget-wide v4, v0, Lcom/google/android/videos/pano/model/VideoItem;->expirationTimestamp:J

    cmp-long v2, v2, v4

    if-nez v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->releaseYear:I

    iget v3, v0, Lcom/google/android/videos/pano/model/VideoItem;->releaseYear:I

    if-ne v2, v3, :cond_0

    .line 180
    iget-object v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    iget-object v3, v0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/Util;->areEqual(Landroid/content/Intent;Landroid/content/Intent;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 186
    invoke-super {p0, p1}, Lcom/google/android/videos/pano/model/Item;->equals(Ljava/lang/Object;)Z

    move-result v1

    goto :goto_0
.end method

.method public getBackgroundUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 108
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->wallpaperUri:Ljava/lang/String;

    return-object v0
.end method

.method public getBadgeResourceId()I
    .locals 1

    .prologue
    .line 143
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->badgeResourceId:I

    return v0
.end method

.method public getCheapestOffer()Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .locals 1

    .prologue
    .line 128
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->cheapestOffer:Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    return-object v0
.end method

.method public getDurationInSeconds()I
    .locals 1

    .prologue
    .line 133
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->durationSec:I

    return v0
.end method

.method public getEntityId()Ljava/lang/String;
    .locals 2

    .prologue
    .line 207
    iget-boolean v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->isMovie:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x6

    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0

    :cond_0
    const/16 v0, 0x12

    goto :goto_0
.end method

.method public getImageHeight()I
    .locals 1

    .prologue
    .line 118
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->height:I

    return v0
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->posterUri:Ljava/lang/String;

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 113
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->width:I

    return v0
.end method

.method public getProgress()F
    .locals 1

    .prologue
    .line 138
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->progress:F

    return v0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 123
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->aggregatedRatings:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    invoke-static {v0}, Lcom/google/android/videos/pano/model/VideoItem;->getStarRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;)F

    move-result v0

    return v0
.end method

.method public getSubtitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->subtitle:Ljava/lang/String;

    return-object v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 1

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->title:Ljava/lang/String;

    return-object v0
.end method

.method public hashCode()I
    .locals 7

    .prologue
    .line 191
    iget v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    .line 192
    .local v0, "result":I
    if-nez v0, :cond_0

    .line 193
    invoke-super {p0}, Lcom/google/android/videos/pano/model/Item;->hashCode()I

    move-result v0

    .line 194
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/videos/pano/model/VideoItem;->isMovie:Z

    if-eqz v1, :cond_1

    const/4 v1, 0x1

    :goto_0
    add-int v0, v2, v1

    .line 195
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->releaseYear:I

    add-int v0, v1, v2

    .line 196
    mul-int/lit8 v1, v0, 0x1f

    iget-wide v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->expirationTimestamp:J

    iget-wide v4, p0, Lcom/google/android/videos/pano/model/VideoItem;->expirationTimestamp:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 197
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->id:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Ljava/lang/Object;)I

    move-result v2

    add-int v0, v1, v2

    .line 198
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->hashCode(Landroid/content/Intent;)I

    move-result v2

    add-int v0, v1, v2

    .line 199
    iput v0, p0, Lcom/google/android/videos/pano/model/VideoItem;->hashCode:I

    .line 201
    :cond_0
    return v0

    .line 194
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 3
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    if-eqz v1, :cond_1

    .line 150
    const/4 v0, 0x0

    .line 151
    .local v0, "bundle":Landroid/os/Bundle;
    iget-object v1, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    instance-of v1, v1, Lcom/google/android/videos/pano/ui/VideoCardView;

    if-eqz v1, :cond_0

    .line 152
    iget-object v1, p2, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v1, Lcom/google/android/videos/pano/ui/VideoCardView;

    invoke-virtual {v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->getImageView()Landroid/widget/ImageView;

    move-result-object v1

    const-string v2, "hero"

    invoke-static {p1, v1, v2}, Landroid/support/v4/app/ActivityOptionsCompat;->makeSceneTransitionAnimation(Landroid/app/Activity;Landroid/view/View;Ljava/lang/String;)Landroid/support/v4/app/ActivityOptionsCompat;

    move-result-object v1

    invoke-virtual {v1}, Landroid/support/v4/app/ActivityOptionsCompat;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 155
    iget-object v1, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    invoke-static {v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->markAsRunningAnimation(Landroid/content/Intent;)V

    .line 157
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    invoke-virtual {p1, v1, v0}, Landroid/app/Activity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 159
    .end local v0    # "bundle":Landroid/os/Bundle;
    :cond_1
    return-void
.end method
