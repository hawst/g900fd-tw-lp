.class public Lcom/google/android/videos/pinning/PinningTask$PinningException;
.super Lcom/google/android/videos/pinning/Task$TaskException;
.source "PinningTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/PinningTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PinningException"
.end annotation


# instance fields
.field public final drmErrorCode:Ljava/lang/Integer;

.field public final failedReason:I

.field public final fatal:Z


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZI)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .param p3, "fatal"    # Z
    .param p4, "failedReason"    # I

    .prologue
    .line 59
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 60
    iput-boolean p3, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->fatal:Z

    .line 61
    iput p4, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->failedReason:I

    .line 62
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    .line 63
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;Ljava/lang/Throwable;ZII)V
    .locals 1
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .param p3, "fatal"    # Z
    .param p4, "failedReason"    # I
    .param p5, "drmErrorCode"    # I

    .prologue
    .line 67
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 68
    iput-boolean p3, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->fatal:Z

    .line 69
    iput p4, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->failedReason:I

    .line 70
    invoke-static {p5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    .line 71
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "detailMessage"    # Ljava/lang/String;
    .param p2, "fatal"    # Z
    .param p3, "failedReason"    # I

    .prologue
    .line 52
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;)V

    .line 53
    iput-boolean p2, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->fatal:Z

    .line 54
    iput p3, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->failedReason:I

    .line 55
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/pinning/PinningTask$PinningException;->drmErrorCode:Ljava/lang/Integer;

    .line 56
    return-void
.end method
