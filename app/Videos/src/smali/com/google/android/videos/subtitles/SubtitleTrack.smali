.class public final Lcom/google/android/videos/subtitles/SubtitleTrack;
.super Ljava/lang/Object;
.source "SubtitleTrack.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/subtitles/SubtitleTrack$1;,
        Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;
    }
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x5L


# instance fields
.field public final fileVersion:I

.field public final format:I

.field public final isForced:Z

.field public final languageCode:Ljava/lang/String;

.field public final trackName:Ljava/lang/String;

.field public final url:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V
    .locals 1
    .param p1, "languageCode"    # Ljava/lang/String;
    .param p2, "trackName"    # Ljava/lang/String;
    .param p3, "format"    # I
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "url"    # Ljava/lang/String;
    .param p6, "isForced"    # Z
    .param p7, "fileVersion"    # I

    .prologue
    .line 64
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 65
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    .line 66
    iput-object p2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    .line 67
    iput p3, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    .line 68
    iput-object p4, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    .line 69
    iput-object p5, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    .line 70
    iput-boolean p6, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    .line 71
    iput p7, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->fileVersion:I

    .line 72
    return-void
.end method

.method static synthetic access$100(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 1
    .param p0, "x0"    # Ljava/lang/String;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .param p2, "x2"    # I

    .prologue
    .line 22
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method

.method public static create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 1
    .param p0, "videoId"    # Ljava/lang/String;
    .param p1, "caption"    # Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    .prologue
    .line 104
    const/4 v0, 0x5

    invoke-static {p0, p1, v0}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method

.method private static create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 8
    .param p0, "videoId"    # Ljava/lang/String;
    .param p1, "caption"    # Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    .param p2, "version"    # I

    .prologue
    .line 108
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    iget-object v2, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    iget-wide v4, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    long-to-int v3, v4

    iget-object v5, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    iget-boolean v6, p1, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    move-object v4, p0

    move v7, p2

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/subtitles/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 8
    .param p0, "languageCode"    # Ljava/lang/String;
    .param p1, "languageName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "format"    # I
    .param p4, "url"    # Ljava/lang/String;
    .param p5, "isForced"    # Z

    .prologue
    .line 76
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 77
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const/4 v7, 0x5

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    move v6, p5

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/subtitles/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    return-object v0
.end method

.method public static create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZ)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 6
    .param p0, "languageCode"    # Ljava/lang/String;
    .param p1, "languageName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "format"    # I
    .param p4, "isForced"    # Z

    .prologue
    .line 84
    const-string v4, ""

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move v3, p3

    move v5, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    return-object v0
.end method

.method public static createDisableTrack(Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 8
    .param p0, "disableText"    # Ljava/lang/String;

    .prologue
    const/4 v3, 0x0

    .line 100
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const-string v1, "<disable>"

    const-string v4, ""

    const-string v5, ""

    move-object v2, p0

    move v6, v3

    move v7, v3

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/subtitles/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    return-object v0
.end method

.method public static createLegacy(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 8
    .param p0, "languageCode"    # Ljava/lang/String;
    .param p1, "languageName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "format"    # I
    .param p4, "url"    # Ljava/lang/String;

    .prologue
    .line 93
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const/4 v6, 0x0

    const/4 v7, 0x1

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/subtitles/SubtitleTrack;-><init>(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;ZI)V

    return-object v0
.end method

.method private writeReplace()Ljava/lang/Object;
    .locals 2
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 160
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;-><init>(Lcom/google/android/videos/subtitles/SubtitleTrack$1;)V

    invoke-virtual {v0, p0}, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->setSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 136
    instance-of v2, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-nez v2, :cond_1

    .line 140
    :cond_0
    :goto_0
    return v1

    :cond_1
    move-object v0, p1

    .line 139
    check-cast v0, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 140
    .local v0, "other":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    iget v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    if-ne v2, v3, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    iget-object v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-static {v2, v3}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    iget-boolean v3, v0, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-ne v2, v3, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method public hashCode()I
    .locals 4

    .prologue
    const/4 v1, 0x0

    .line 150
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    move v0, v1

    .line 151
    .local v0, "result":I
    :goto_0
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    move v2, v1

    :goto_1
    add-int v0, v3, v2

    .line 152
    mul-int/lit8 v2, v0, 0x1f

    iget v3, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    add-int v0, v2, v3

    .line 153
    mul-int/lit8 v3, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_2

    move v2, v1

    :goto_2
    add-int v0, v3, v2

    .line 154
    mul-int/lit8 v2, v0, 0x1f

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    :goto_3
    add-int v0, v2, v1

    .line 155
    mul-int/lit8 v2, v0, 0x1f

    iget-boolean v1, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-eqz v1, :cond_4

    const/16 v1, 0x4cf

    :goto_4
    add-int v0, v2, v1

    .line 156
    return v0

    .line 150
    .end local v0    # "result":I
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0

    .line 151
    .restart local v0    # "result":I
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 153
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_2

    .line 154
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    goto :goto_3

    .line 155
    :cond_4
    const/16 v1, 0x4d5

    goto :goto_4
.end method

.method public isDisableTrack()Z
    .locals 2

    .prologue
    .line 117
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    const-string v1, "<disable>"

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 4

    .prologue
    .line 122
    invoke-virtual {p0}, Lcom/google/android/videos/subtitles/SubtitleTrack;->isDisableTrack()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 123
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    .line 131
    :goto_0
    return-object v2

    .line 126
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/utils/Util;->getLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 127
    .local v1, "languageName":Ljava/lang/String;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0, v1}, Ljava/lang/StringBuilder;-><init>(Ljava/lang/String;)V

    .line 128
    .local v0, "builder":Ljava/lang/StringBuilder;
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-nez v2, :cond_1

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v2, v1}, Ljava/lang/String;->equalsIgnoreCase(Ljava/lang/String;)Z

    move-result v2

    if-nez v2, :cond_1

    .line 129
    const-string v2, " - "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    .line 131
    :cond_1
    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    goto :goto_0
.end method
