.class public Lcom/google/android/videos/player/PlayerUtil;
.super Ljava/lang/Object;
.source "PlayerUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/PlayerUtil$1;
    }
.end annotation


# static fields
.field private static final DIAGNOSTIC_INFO:Ljava/util/regex/Pattern;


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 24
    const-string v0, ".*?_(neg_|)(\\d+)"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/player/PlayerUtil;->DIAGNOSTIC_INFO:Ljava/util/regex/Pattern;

    return-void
.end method

.method public static computeCencDrmError(Landroid/content/Context;Lcom/google/android/videos/api/CencLicenseException;ZZZ)Landroid/util/Pair;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exception"    # Lcom/google/android/videos/api/CencLicenseException;
    .param p2, "isNetworkAvailable"    # Z
    .param p3, "isPinnedLocally"    # Z
    .param p4, "isRental"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/api/CencLicenseException;",
            "ZZZ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    .line 170
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 171
    const v3, 0x7f0b017b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    .line 215
    :goto_0
    return-object v3

    .line 174
    :cond_0
    const v1, 0x7f0b0160

    .line 175
    .local v1, "error":I
    const/4 v0, 0x1

    .line 176
    .local v0, "canRetry":Z
    iget v3, p1, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    sparse-switch v3, :sswitch_data_0

    .line 214
    :goto_1
    new-array v3, v4, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 215
    .local v2, "errorMessage":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    goto :goto_0

    .line 178
    .end local v2    # "errorMessage":Ljava/lang/String;
    :sswitch_0
    if-eqz p4, :cond_1

    const v1, 0x7f0b0151

    .line 180
    :goto_2
    goto :goto_1

    .line 178
    :cond_1
    const v1, 0x7f0b0150

    goto :goto_2

    .line 182
    :sswitch_1
    const v1, 0x7f0b01f6

    .line 183
    const/4 v0, 0x0

    .line 184
    goto :goto_1

    .line 186
    :sswitch_2
    const v1, 0x7f0b0178

    .line 187
    const/4 v0, 0x0

    .line 188
    goto :goto_1

    .line 191
    :sswitch_3
    const v1, 0x7f0b017a

    .line 192
    const/4 v0, 0x0

    .line 193
    goto :goto_1

    .line 195
    :sswitch_4
    const v1, 0x7f0b0148

    .line 196
    const/4 v0, 0x0

    .line 197
    goto :goto_1

    .line 200
    :sswitch_5
    const v1, 0x7f0b014a

    .line 201
    const/4 v0, 0x0

    .line 202
    goto :goto_1

    .line 204
    :sswitch_6
    const v1, 0x7f0b0154

    .line 205
    const/4 v0, 0x0

    .line 206
    goto :goto_1

    .line 208
    :sswitch_7
    const v1, 0x7f0b014e

    .line 209
    goto :goto_1

    .line 211
    :sswitch_8
    const v1, 0x7f0b014f

    goto :goto_1

    .line 176
    :sswitch_data_0
    .sparse-switch
        0x5 -> :sswitch_3
        0x8 -> :sswitch_6
        0x12c -> :sswitch_2
        0x12d -> :sswitch_0
        0x12f -> :sswitch_7
        0x130 -> :sswitch_8
        0x191 -> :sswitch_1
        0x193 -> :sswitch_3
        0x1f4 -> :sswitch_5
        0x1f5 -> :sswitch_4
        0x1f6 -> :sswitch_5
    .end sparse-switch
.end method

.method public static computeDrmError(Landroid/content/Context;Lcom/google/android/videos/drm/DrmException;ZZZ)Landroid/util/Pair;
    .locals 6
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exception"    # Lcom/google/android/videos/drm/DrmException;
    .param p2, "isNetworkAvailable"    # Z
    .param p3, "isPinnedLocally"    # Z
    .param p4, "isRental"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/drm/DrmException;",
            "ZZZ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 97
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 98
    const v3, 0x7f0b017b

    invoke-virtual {p0, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    .line 158
    :goto_0
    return-object v3

    .line 101
    :cond_0
    const v1, 0x7f0b0160

    .line 102
    .local v1, "error":I
    const/4 v0, 0x1

    .line 103
    .local v0, "canRetry":Z
    sget-object v3, Lcom/google/android/videos/player/PlayerUtil$1;->$SwitchMap$com$google$android$videos$drm$DrmException$DrmError:[I

    iget-object v4, p1, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    invoke-virtual {v4}, Lcom/google/android/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v4

    aget v3, v3, v4

    packed-switch v3, :pswitch_data_0

    .line 157
    :cond_1
    :goto_1
    :pswitch_0
    new-array v3, v5, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget v5, p1, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {p0, v1, v3}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    .line 158
    .local v2, "errorMessage":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-static {v2, v3}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    goto :goto_0

    .line 105
    .end local v2    # "errorMessage":Ljava/lang/String;
    :pswitch_1
    if-eqz p4, :cond_2

    const v1, 0x7f0b0151

    .line 107
    :goto_2
    goto :goto_1

    .line 105
    :cond_2
    const v1, 0x7f0b0150

    goto :goto_2

    .line 109
    :pswitch_2
    const v1, 0x7f0b01f6

    .line 110
    const/4 v0, 0x0

    .line 111
    goto :goto_1

    .line 114
    :pswitch_3
    const v1, 0x7f0b0179

    .line 115
    const/4 v0, 0x0

    .line 116
    goto :goto_1

    .line 118
    :pswitch_4
    const v1, 0x7f0b017a

    .line 119
    const/4 v0, 0x0

    .line 120
    goto :goto_1

    .line 122
    :pswitch_5
    const v1, 0x7f0b0145

    .line 123
    goto :goto_1

    .line 125
    :pswitch_6
    const v1, 0x7f0b0178

    .line 126
    const/4 v0, 0x0

    .line 127
    goto :goto_1

    .line 130
    :pswitch_7
    if-eqz p4, :cond_1

    .line 131
    const v1, 0x7f0b0148

    .line 132
    const/4 v0, 0x0

    goto :goto_1

    .line 138
    :pswitch_8
    const v1, 0x7f0b0161

    .line 139
    const/4 v0, 0x0

    .line 140
    goto :goto_1

    .line 142
    :pswitch_9
    const v1, 0x7f0b0143

    .line 143
    goto :goto_1

    .line 103
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method public static computePlayerError(Landroid/content/Context;Lcom/google/android/videos/player/legacy/MediaPlayerException;ZZZ)Landroid/util/Pair;
    .locals 7
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "exception"    # Lcom/google/android/videos/player/legacy/MediaPlayerException;
    .param p2, "isNetworkAvailable"    # Z
    .param p3, "isPinnedLocally"    # Z
    .param p4, "isRental"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/player/legacy/MediaPlayerException;",
            "ZZZ)",
            "Landroid/util/Pair",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation

    .prologue
    const/4 v5, 0x1

    .line 35
    if-nez p2, :cond_0

    if-nez p3, :cond_0

    .line 36
    const v4, 0x7f0b017b

    invoke-virtual {p0, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v5}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v5

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    .line 88
    :goto_0
    return-object v4

    .line 39
    :cond_0
    iget v2, p1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->extra:I

    .line 40
    .local v2, "extra":I
    const v3, 0x7f0b01f0

    .line 41
    .local v3, "messageId":I
    const/4 v0, 0x1

    .line 42
    .local v0, "canRetry":Z
    iget v4, p1, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    if-ne v4, v5, :cond_1

    .line 43
    const v3, 0x7f0b01f1

    .line 44
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_AUTHENTICATION_FAILURE:I

    if-ne v2, v4, :cond_2

    .line 45
    const v3, 0x7f0b0145

    .line 87
    :cond_1
    :goto_1
    new-array v4, v5, [Ljava/lang/Object;

    const/4 v5, 0x0

    invoke-static {v2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {p0, v3, v4}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    .line 88
    .local v1, "errorMessage":Ljava/lang/String;
    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v4

    invoke-static {v1, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    goto :goto_0

    .line 46
    .end local v1    # "errorMessage":Ljava/lang/String;
    :cond_2
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_STREAMING_UNAVAILABLE:I

    if-ne v2, v4, :cond_4

    .line 47
    if-eqz p4, :cond_3

    const v3, 0x7f0b0151

    .line 49
    :goto_2
    const/4 v0, 0x0

    goto :goto_1

    .line 47
    :cond_3
    const v3, 0x7f0b0150

    goto :goto_2

    .line 50
    :cond_4
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACK:I

    if-ne v2, v4, :cond_5

    .line 51
    const v3, 0x7f0b014e

    goto :goto_1

    .line 52
    :cond_5
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CONCURRENT_PLAYBACKS_BY_ACCOUNT:I

    if-ne v2, v4, :cond_6

    .line 53
    const v3, 0x7f0b014f

    goto :goto_1

    .line 54
    :cond_6
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_TERMINATE_REQUESTED:I

    if-ne v2, v4, :cond_7

    .line 55
    const v3, 0x7f0b01f3

    goto :goto_1

    .line 56
    :cond_7
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_CANNOT_ACTIVATE_RENTAL:I

    if-ne v2, v4, :cond_8

    .line 57
    const v3, 0x7f0b0155

    .line 58
    const/4 v0, 0x0

    goto :goto_1

    .line 59
    :cond_8
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_UNUSUAL_ACTIVITY:I

    if-ne v2, v4, :cond_9

    .line 60
    const v3, 0x7f0b0154

    .line 61
    const/4 v0, 0x0

    goto :goto_1

    .line 62
    :cond_9
    sget v4, Lcom/google/android/videos/player/legacy/MediaPlayerException;->EXTRA_HEARTBEAT_NO_ACTIVE_PURCHASE_AGREEMENT:I

    if-ne v2, v4, :cond_a

    .line 63
    const v3, 0x7f0b014d

    .line 64
    const/4 v0, 0x0

    goto :goto_1

    .line 65
    :cond_a
    const/16 v4, -0x7d2

    if-eq v2, v4, :cond_b

    const/16 v4, -0x7d1

    if-ne v2, v4, :cond_c

    .line 67
    :cond_b
    const v3, 0x7f0b0148

    .line 68
    const/4 v0, 0x0

    goto :goto_1

    .line 69
    :cond_c
    const/16 v4, -0x3ec

    if-ne v2, v4, :cond_d

    .line 70
    if-nez p3, :cond_1

    .line 71
    const v3, 0x7f0b01f3

    goto :goto_1

    .line 75
    :cond_d
    const/16 v4, -0x3e81

    if-ne v2, v4, :cond_e

    .line 78
    const v3, 0x7f0b0164

    goto :goto_1

    .line 79
    :cond_e
    const/16 v4, -0x3ea

    if-eq v2, v4, :cond_f

    const/16 v4, -0x3eb

    if-ne v2, v4, :cond_10

    .line 81
    :cond_f
    const v3, 0x7f0b01f2

    goto/16 :goto_1

    .line 82
    :cond_10
    const/16 v4, 0x20

    if-eq v2, v4, :cond_11

    const/16 v4, -0x3ed

    if-ne v2, v4, :cond_1

    .line 84
    :cond_11
    const v3, 0x7f0b01f3

    goto/16 :goto_1
.end method

.method public static createVirtualizerIfAvailableV18(I)Landroid/media/audiofx/Virtualizer;
    .locals 9
    .param p0, "audioSessionId"    # I

    .prologue
    const/4 v6, 0x0

    .line 227
    :try_start_0
    invoke-static {}, Landroid/media/audiofx/AudioEffect;->queryEffects()[Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v0

    .line 228
    .local v0, "audioEffects":[Landroid/media/audiofx/AudioEffect$Descriptor;
    const/4 v2, 0x0

    .line 229
    .local v2, "foundSupportedVirtualizer":Z
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v7, v0

    if-ge v3, v7, :cond_1

    .line 230
    sget-object v7, Landroid/media/audiofx/AudioEffect;->EFFECT_TYPE_VIRTUALIZER:Ljava/util/UUID;

    aget-object v8, v0, v3

    iget-object v8, v8, Landroid/media/audiofx/AudioEffect$Descriptor;->type:Ljava/util/UUID;

    invoke-virtual {v7, v8}, Ljava/util/UUID;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 231
    aget-object v7, v0, v3

    iget-object v7, v7, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 232
    .local v4, "uuid":Ljava/lang/String;
    const-string v7, "36103c52-8514-11e2-9e96-0800200c9a66"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_0

    const-string v7, "36103c50-8514-11e2-9e96-0800200c9a66"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 233
    :cond_0
    const/4 v2, 0x1

    .line 238
    .end local v4    # "uuid":Ljava/lang/String;
    :cond_1
    if-nez v2, :cond_4

    move-object v5, v6

    .line 252
    .end local v0    # "audioEffects":[Landroid/media/audiofx/AudioEffect$Descriptor;
    .end local v2    # "foundSupportedVirtualizer":Z
    .end local v3    # "i":I
    :cond_2
    :goto_1
    return-object v5

    .line 229
    .restart local v0    # "audioEffects":[Landroid/media/audiofx/AudioEffect$Descriptor;
    .restart local v2    # "foundSupportedVirtualizer":Z
    .restart local v3    # "i":I
    :cond_3
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 241
    :cond_4
    new-instance v5, Landroid/media/audiofx/Virtualizer;

    const/4 v7, 0x0

    invoke-direct {v5, v7, p0}, Landroid/media/audiofx/Virtualizer;-><init>(II)V

    .line 242
    .local v5, "virtualizer":Landroid/media/audiofx/Virtualizer;
    invoke-virtual {v5}, Landroid/media/audiofx/Virtualizer;->getDescriptor()Landroid/media/audiofx/AudioEffect$Descriptor;

    move-result-object v7

    iget-object v7, v7, Landroid/media/audiofx/AudioEffect$Descriptor;->uuid:Ljava/util/UUID;

    invoke-virtual {v7}, Ljava/util/UUID;->toString()Ljava/lang/String;

    move-result-object v4

    .line 243
    .restart local v4    # "uuid":Ljava/lang/String;
    const-string v7, "36103c52-8514-11e2-9e96-0800200c9a66"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    const-string v7, "36103c50-8514-11e2-9e96-0800200c9a66"

    invoke-virtual {v7, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-nez v7, :cond_2

    .line 247
    invoke-virtual {v5}, Landroid/media/audiofx/Virtualizer;->release()V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    move-object v5, v6

    .line 248
    goto :goto_1

    .line 250
    .end local v0    # "audioEffects":[Landroid/media/audiofx/AudioEffect$Descriptor;
    .end local v2    # "foundSupportedVirtualizer":Z
    .end local v3    # "i":I
    .end local v4    # "uuid":Ljava/lang/String;
    .end local v5    # "virtualizer":Landroid/media/audiofx/Virtualizer;
    :catch_0
    move-exception v1

    .line 251
    .local v1, "e":Ljava/lang/RuntimeException;
    const-string v7, "Failed to initialize audio virtualizer"

    invoke-static {v7, v1}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v5, v6

    .line 252
    goto :goto_1
.end method

.method public static hashErrorString(Ljava/lang/String;)I
    .locals 1
    .param p0, "message"    # Ljava/lang/String;

    .prologue
    .line 291
    if-nez p0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    invoke-virtual {p0}, Ljava/lang/String;->hashCode()I

    move-result v0

    goto :goto_0
.end method

.method public static parseErrorCode(Ljava/lang/String;)I
    .locals 3
    .param p0, "diagnosticInfo"    # Ljava/lang/String;

    .prologue
    .line 268
    if-nez p0, :cond_1

    .line 269
    const/4 v0, -0x1

    .line 281
    :cond_0
    :goto_0
    return v0

    .line 272
    :cond_1
    sget-object v2, Lcom/google/android/videos/player/PlayerUtil;->DIAGNOSTIC_INFO:Ljava/util/regex/Pattern;

    invoke-virtual {v2, p0}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v1

    .line 273
    .local v1, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v1}, Ljava/util/regex/Matcher;->matches()Z

    move-result v2

    if-nez v2, :cond_2

    .line 274
    invoke-static {p0}, Lcom/google/android/videos/player/PlayerUtil;->hashErrorString(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 277
    :cond_2
    const/4 v2, 0x2

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v0

    .line 278
    .local v0, "errorCode":I
    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/String;->isEmpty()Z

    move-result v2

    if-nez v2, :cond_0

    .line 279
    neg-int v0, v0

    goto :goto_0
.end method
