.class Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;
.super Ljava/lang/Object;
.source "RestrictionsActivity.java"

# interfaces
.implements Lcom/google/android/recline/app/DialogFragment$Action$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/RestrictionsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PreferenceEntryListener"
.end annotation


# instance fields
.field private final preference:Landroid/preference/Preference;

.field final synthetic this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pano/activity/RestrictionsActivity;Landroid/preference/Preference;)V
    .locals 0
    .param p2, "preference"    # Landroid/preference/Preference;

    .prologue
    .line 178
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 179
    iput-object p2, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    .line 180
    return-void
.end method

.method private onPreferenceEntrySelected(Ljava/lang/String;)V
    .locals 4
    .param p1, "value"    # Ljava/lang/String;

    .prologue
    .line 189
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    instance-of v1, v1, Landroid/preference/CheckBoxPreference;

    if-eqz v1, :cond_0

    .line 190
    const-string v1, "1"

    invoke-static {v1, p1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    .line 191
    .local v0, "isChecked":Z
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    check-cast v1, Landroid/preference/CheckBoxPreference;

    invoke-virtual {v1, v0}, Landroid/preference/CheckBoxPreference;->setChecked(Z)V

    .line 192
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->access$300(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Lcom/google/android/videos/ui/RestrictionsHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    invoke-static {v0}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/ui/RestrictionsHelper;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    .line 197
    .end local v0    # "isChecked":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    # invokes: Lcom/google/android/videos/pano/activity/RestrictionsActivity;->updateActions()V
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->access$400(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)V

    .line 198
    return-void

    .line 194
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    check-cast v1, Landroid/preference/ListPreference;

    invoke-virtual {v1, p1}, Landroid/preference/ListPreference;->setValue(Ljava/lang/String;)V

    .line 195
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->access$300(Lcom/google/android/videos/pano/activity/RestrictionsActivity;)Lcom/google/android/videos/ui/RestrictionsHelper;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->preference:Landroid/preference/Preference;

    invoke-virtual {v1, v2, p1}, Lcom/google/android/videos/ui/RestrictionsHelper;->onPreferenceChange(Landroid/preference/Preference;Ljava/lang/Object;)Z

    goto :goto_0
.end method


# virtual methods
.method public onActionClicked(Lcom/google/android/recline/app/DialogFragment$Action;)V
    .locals 1
    .param p1, "action"    # Lcom/google/android/recline/app/DialogFragment$Action;

    .prologue
    .line 184
    invoke-virtual {p1}, Lcom/google/android/recline/app/DialogFragment$Action;->getKey()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->onPreferenceEntrySelected(Ljava/lang/String;)V

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/RestrictionsActivity$PreferenceEntryListener;->this$0:Lcom/google/android/videos/pano/activity/RestrictionsActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v0

    invoke-virtual {v0}, Landroid/app/FragmentManager;->popBackStack()V

    .line 186
    return-void
.end method
