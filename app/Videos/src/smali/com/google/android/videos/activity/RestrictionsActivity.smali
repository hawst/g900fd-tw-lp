.class public Lcom/google/android/videos/activity/RestrictionsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "RestrictionsActivity.java"

# interfaces
.implements Lcom/google/android/videos/ui/RestrictionsHelper$Listener;


# instance fields
.field private helper:Lcom/google/android/videos/ui/RestrictionsHelper;

.field private restrictionsFragment:Landroid/preference/PreferenceFragment;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 21
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method

.method private getOrCreateRestrictionsFragment()Landroid/preference/PreferenceFragment;
    .locals 3

    .prologue
    const v2, 0x1020002

    .line 75
    invoke-virtual {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1, v2}, Landroid/app/FragmentManager;->findFragmentById(I)Landroid/app/Fragment;

    move-result-object v0

    check-cast v0, Landroid/preference/PreferenceFragment;

    .line 77
    .local v0, "restrictionsFragment":Landroid/preference/PreferenceFragment;
    if-nez v0, :cond_0

    .line 78
    new-instance v0, Lcom/google/android/videos/activity/RestrictionsActivity$1;

    .end local v0    # "restrictionsFragment":Landroid/preference/PreferenceFragment;
    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/RestrictionsActivity$1;-><init>(Lcom/google/android/videos/activity/RestrictionsActivity;)V

    .line 79
    .restart local v0    # "restrictionsFragment":Landroid/preference/PreferenceFragment;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->beginTransaction()Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1, v2, v0}, Landroid/app/FragmentTransaction;->replace(ILandroid/app/Fragment;)Landroid/app/FragmentTransaction;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentTransaction;->commit()I

    .line 82
    invoke-virtual {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->getFragmentManager()Landroid/app/FragmentManager;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/FragmentManager;->executePendingTransactions()Z

    .line 84
    const v1, 0x7f080003

    invoke-virtual {v0, v1}, Landroid/preference/PreferenceFragment;->addPreferencesFromResource(I)V

    .line 85
    invoke-virtual {v0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/RestrictionsHelper;->createAllowUnratedPreference()Landroid/preference/CheckBoxPreference;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 88
    :cond_0
    return-object v0
.end method


# virtual methods
.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 1
    .param p1, "requestCode"    # I
    .param p2, "resultCode"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 51
    iget-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0, p1, p2, p3}, Lcom/google/android/videos/ui/RestrictionsHelper;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 55
    :goto_0
    return-void

    .line 54
    :cond_0
    invoke-super {p0, p1, p2, p3}, Landroid/support/v4/app/FragmentActivity;->onActivityResult(IILandroid/content/Intent;)V

    goto :goto_0
.end method

.method public onCreate(Landroid/os/Bundle;)V
    .locals 3
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    const/16 v1, 0xc

    .line 28
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 29
    invoke-virtual {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->getActionBar()Landroid/app/ActionBar;

    move-result-object v0

    .line 30
    .local v0, "actionBar":Landroid/app/ActionBar;
    invoke-virtual {v0, v1, v1}, Landroid/app/ActionBar;->setDisplayOptions(II)V

    .line 32
    const v1, 0x7f0b0200

    invoke-virtual {v0, v1}, Landroid/app/ActionBar;->setTitle(I)V

    .line 33
    const v1, 0x7f0400b7

    invoke-virtual {p0, v1}, Lcom/google/android/videos/activity/RestrictionsActivity;->setContentView(I)V

    .line 34
    new-instance v1, Lcom/google/android/videos/ui/RestrictionsHelper;

    const v2, 0x7f0f00d8

    invoke-virtual {p0, v2}, Lcom/google/android/videos/activity/RestrictionsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    invoke-direct {v1, p0, p0, v2}, Lcom/google/android/videos/ui/RestrictionsHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/ui/RestrictionsHelper$Listener;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    .line 35
    return-void
.end method

.method public onOptionsItemSelected(Landroid/view/MenuItem;)Z
    .locals 2
    .param p1, "item"    # Landroid/view/MenuItem;

    .prologue
    .line 59
    invoke-interface {p1}, Landroid/view/MenuItem;->getItemId()I

    move-result v0

    const v1, 0x102002c

    if-ne v0, v1, :cond_0

    .line 60
    invoke-virtual {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->finish()V

    .line 61
    const/4 v0, 0x1

    .line 63
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 45
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onPause()V

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->onPause()V

    .line 47
    return-void
.end method

.method public onRestrictionPreferenceCreated(Landroid/preference/ListPreference;)V
    .locals 1
    .param p1, "preference"    # Landroid/preference/ListPreference;

    .prologue
    .line 68
    iget-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->restrictionsFragment:Landroid/preference/PreferenceFragment;

    if-nez v0, :cond_0

    .line 69
    invoke-direct {p0}, Lcom/google/android/videos/activity/RestrictionsActivity;->getOrCreateRestrictionsFragment()Landroid/preference/PreferenceFragment;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->restrictionsFragment:Landroid/preference/PreferenceFragment;

    .line 71
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->restrictionsFragment:Landroid/preference/PreferenceFragment;

    invoke-virtual {v0}, Landroid/preference/PreferenceFragment;->getPreferenceScreen()Landroid/preference/PreferenceScreen;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/preference/PreferenceScreen;->addPreference(Landroid/preference/Preference;)Z

    .line 72
    return-void
.end method

.method public onResume()V
    .locals 1

    .prologue
    .line 39
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onResume()V

    .line 40
    iget-object v0, p0, Lcom/google/android/videos/activity/RestrictionsActivity;->helper:Lcom/google/android/videos/ui/RestrictionsHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RestrictionsHelper;->onResume()V

    .line 41
    return-void
.end method
