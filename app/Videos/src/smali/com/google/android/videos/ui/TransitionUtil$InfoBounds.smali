.class public Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;
.super Landroid/transition/ChangeBounds;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/TransitionUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "InfoBounds"
.end annotation


# instance fields
.field private endTop:I

.field private pathMotion:Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;

.field private startTop:I

.field private final transitionName:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 1
    .param p1, "transitionName"    # Ljava/lang/String;

    .prologue
    .line 331
    invoke-direct {p0}, Landroid/transition/ChangeBounds;-><init>()V

    .line 332
    iput-object p1, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->transitionName:Ljava/lang/String;

    .line 333
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->addTarget(Ljava/lang/String;)Landroid/transition/Transition;

    .line 334
    new-instance v0, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;

    invoke-direct {v0}, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->pathMotion:Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;

    .line 335
    iget-object v0, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->pathMotion:Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->setPathMotion(Landroid/transition/PathMotion;)V

    .line 336
    return-void
.end method


# virtual methods
.method public captureEndValues(Landroid/transition/TransitionValues;)V
    .locals 3
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 348
    invoke-super {p0, p1}, Landroid/transition/ChangeBounds;->captureEndValues(Landroid/transition/TransitionValues;)V

    .line 349
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->transitionName:Ljava/lang/String;

    invoke-static {v1, v2}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 350
    iget-object v1, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getTop()I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->endTop:I

    .line 351
    iget v1, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->startTop:I

    iget v2, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->endTop:I

    if-ge v1, v2, :cond_1

    const/4 v0, 0x1

    .line 352
    .local v0, "isFallingCard":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->pathMotion:Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;->setMoveVerticalFirst(Z)V

    .line 354
    .end local v0    # "isFallingCard":Z
    :cond_0
    return-void

    .line 351
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public captureStartValues(Landroid/transition/TransitionValues;)V
    .locals 2
    .param p1, "transitionValues"    # Landroid/transition/TransitionValues;

    .prologue
    .line 340
    invoke-super {p0, p1}, Landroid/transition/ChangeBounds;->captureStartValues(Landroid/transition/TransitionValues;)V

    .line 341
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->transitionName:Ljava/lang/String;

    invoke-static {v0, v1}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 342
    iget-object v0, p1, Landroid/transition/TransitionValues;->view:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getTop()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;->startTop:I

    .line 344
    :cond_0
    return-void
.end method
