.class Lcom/google/android/videos/activity/HomeFragment$1;
.super Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.source "HomeFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/HomeFragment;->createHeaderListLayoutConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/HomeFragment;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/HomeFragment;Landroid/content/Context;)V
    .locals 0
    .param p2, "x0"    # Landroid/content/Context;

    .prologue
    .line 79
    iput-object p1, p0, Lcom/google/android/videos/activity/HomeFragment$1;->this$0:Lcom/google/android/videos/activity/HomeFragment;

    invoke-direct {p0, p2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;-><init>(Landroid/content/Context;)V

    return-void
.end method


# virtual methods
.method protected addContentView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;)V
    .locals 1
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "root"    # Landroid/view/ViewGroup;

    .prologue
    .line 87
    const v0, 0x7f0400b8

    invoke-virtual {p1, v0, p2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 88
    return-void
.end method

.method protected alwaysUseFloatingBackground()Z
    .locals 1

    .prologue
    .line 82
    const/4 v0, 0x1

    return v0
.end method

.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 97
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment$1;->this$0:Lcom/google/android/videos/activity/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeFragment;->getHeaderBottomMargin()I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 92
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment$1;->this$0:Lcom/google/android/videos/activity/HomeFragment;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeFragment;->getHeaderHeight()I

    move-result v0

    return v0
.end method

.method protected getTabMode()I
    .locals 1

    .prologue
    .line 107
    const/4 v0, 0x2

    return v0
.end method

.method protected getToolbarTitleMode()I
    .locals 1

    .prologue
    .line 117
    const/4 v0, 0x1

    return v0
.end method

.method protected hasViewPager()Z
    .locals 1

    .prologue
    .line 112
    const/4 v0, 0x0

    return v0
.end method

.method protected useBuiltInActionBar()Z
    .locals 1

    .prologue
    .line 102
    const/4 v0, 0x1

    return v0
.end method
