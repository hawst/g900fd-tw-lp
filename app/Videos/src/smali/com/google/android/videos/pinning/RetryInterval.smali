.class public Lcom/google/android/videos/pinning/RetryInterval;
.super Ljava/lang/Object;
.source "RetryInterval.java"


# instance fields
.field private final maxLength:I

.field private final minLength:I

.field private final random:Ljava/util/Random;


# direct methods
.method public constructor <init>(II)V
    .locals 5
    .param p1, "minLength"    # I
    .param p2, "maxLength"    # I

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 32
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 33
    if-lez p1, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "min: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 34
    if-gt p1, p2, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "minLength: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, ", maxLength: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 36
    iput p1, p0, Lcom/google/android/videos/pinning/RetryInterval;->minLength:I

    .line 37
    iput p2, p0, Lcom/google/android/videos/pinning/RetryInterval;->maxLength:I

    .line 39
    new-instance v0, Ljava/util/Random;

    invoke-direct {v0}, Ljava/util/Random;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pinning/RetryInterval;->random:Ljava/util/Random;

    .line 40
    return-void

    :cond_0
    move v0, v2

    .line 33
    goto :goto_0

    :cond_1
    move v1, v2

    .line 34
    goto :goto_1
.end method

.method private clamp(IIJ)I
    .locals 5
    .param p1, "min"    # I
    .param p2, "max"    # I
    .param p3, "value"    # J

    .prologue
    .line 58
    int-to-long v0, p1

    int-to-long v2, p2

    invoke-static {p3, p4, v2, v3}, Ljava/lang/Math;->min(JJ)J

    move-result-wide v2

    invoke-static {v0, v1, v2, v3}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v0

    long-to-int v0, v0

    return v0
.end method


# virtual methods
.method public getLength(I)J
    .locals 8
    .param p1, "retryCount"    # I

    .prologue
    .line 47
    if-lez p1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v2, "retryCount should be positive"

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 49
    const/16 v1, 0x20

    if-lt p1, v1, :cond_1

    .line 50
    iget v0, p0, Lcom/google/android/videos/pinning/RetryInterval;->maxLength:I

    .line 54
    .local v0, "length":I
    :goto_1
    int-to-long v2, v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/RetryInterval;->random:Ljava/util/Random;

    invoke-virtual {v1, v0}, Ljava/util/Random;->nextInt(I)I

    move-result v1

    int-to-long v4, v1

    add-long/2addr v2, v4

    div-int/lit8 v1, v0, 0x2

    int-to-long v4, v1

    sub-long/2addr v2, v4

    return-wide v2

    .line 47
    .end local v0    # "length":I
    :cond_0
    const/4 v1, 0x0

    goto :goto_0

    .line 52
    :cond_1
    iget v1, p0, Lcom/google/android/videos/pinning/RetryInterval;->minLength:I

    iget v2, p0, Lcom/google/android/videos/pinning/RetryInterval;->maxLength:I

    const-wide/16 v4, 0x1

    add-int/lit8 v3, p1, -0x1

    shl-long/2addr v4, v3

    iget v3, p0, Lcom/google/android/videos/pinning/RetryInterval;->minLength:I

    int-to-long v6, v3

    mul-long/2addr v4, v6

    invoke-direct {p0, v1, v2, v4, v5}, Lcom/google/android/videos/pinning/RetryInterval;->clamp(IIJ)I

    move-result v0

    .restart local v0    # "length":I
    goto :goto_1
.end method
