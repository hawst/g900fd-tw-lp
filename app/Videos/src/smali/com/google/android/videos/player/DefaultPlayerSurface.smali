.class public Lcom/google/android/videos/player/DefaultPlayerSurface;
.super Landroid/view/ViewGroup;
.source "DefaultPlayerSurface.java"

# interfaces
.implements Landroid/view/SurfaceHolder$Callback;
.implements Lcom/google/android/videos/player/PlayerSurface;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;
    }
.end annotation


# instance fields
.field private isShowing:Z

.field private listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

.field private onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

.field private final openShutterRunnable:Ljava/lang/Runnable;

.field private final shutterView:Landroid/view/View;

.field private surfaceCreated:Z

.field private final surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

.field private videoDisplayHeight:I

.field private videoDisplayWidth:I

.field private videoHeight:I

.field private videoWidth:I

.field private zoom:I

.field private zoomSupported:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v3, 0x1

    .line 45
    invoke-direct {p0, p1}, Landroid/view/ViewGroup;-><init>(Landroid/content/Context;)V

    .line 47
    new-instance v1, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-direct {v1, p0, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;-><init>(Lcom/google/android/videos/player/DefaultPlayerSurface;Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    .line 48
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x11

    if-lt v1, v2, :cond_0

    .line 49
    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1, v3}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->setSecureV17(Z)V

    .line 51
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    .line 52
    .local v0, "surfaceHolder":Landroid/view/SurfaceHolder;
    const/4 v1, 0x3

    invoke-interface {v0, v1}, Landroid/view/SurfaceHolder;->setType(I)V

    .line 53
    invoke-interface {v0, p0}, Landroid/view/SurfaceHolder;->addCallback(Landroid/view/SurfaceHolder$Callback;)V

    .line 56
    new-instance v1, Landroid/view/View;

    invoke-direct {v1, p1}, Landroid/view/View;-><init>(Landroid/content/Context;)V

    iput-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    .line 57
    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    const/high16 v2, -0x1000000

    invoke-virtual {v1, v2}, Landroid/view/View;->setBackgroundColor(I)V

    .line 59
    new-instance v1, Lcom/google/android/videos/player/DefaultPlayerSurface$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/DefaultPlayerSurface$1;-><init>(Lcom/google/android/videos/player/DefaultPlayerSurface;)V

    iput-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    .line 66
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/DefaultPlayerSurface;->setVisible(Z)V

    .line 67
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/DefaultPlayerSurface;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/player/DefaultPlayerSurface;)Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I

    return v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I

    return v0
.end method

.method static synthetic access$402(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayWidth:I

    return p1
.end method

.method static synthetic access$502(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;
    .param p1, "x1"    # I

    .prologue
    .line 21
    iput p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayHeight:I

    return p1
.end method

.method static synthetic access$600(Lcom/google/android/videos/player/DefaultPlayerSurface;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoomSupported:Z

    return v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/player/DefaultPlayerSurface;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/DefaultPlayerSurface;

    .prologue
    .line 21
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoom:I

    return v0
.end method


# virtual methods
.method public closeShutter()V
    .locals 2

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/view/View;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 105
    return-void
.end method

.method public getHorizontalLetterboxFraction()F
    .locals 1

    .prologue
    .line 164
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v0, v0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    return v0
.end method

.method public getSurfaceHolder()Landroid/view/SurfaceHolder;
    .locals 1

    .prologue
    .line 91
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v0

    return-object v0
.end method

.method public getVerticalLetterboxFraction()F
    .locals 1

    .prologue
    .line 169
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    iget v0, v0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    return v0
.end method

.method public getVideoDisplayHeight()I
    .locals 1

    .prologue
    .line 154
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayHeight:I

    return v0
.end method

.method public getVideoDisplayWidth()I
    .locals 1

    .prologue
    .line 149
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayWidth:I

    return v0
.end method

.method public isSurfaceCreated()Z
    .locals 1

    .prologue
    .line 109
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceCreated:Z

    return v0
.end method

.method protected onLayout(ZIIII)V
    .locals 4
    .param p1, "changed"    # Z
    .param p2, "l"    # I
    .param p3, "t"    # I
    .param p4, "r"    # I
    .param p5, "b"    # I

    .prologue
    const/4 v3, 0x0

    .line 225
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->layout(IIII)V

    .line 226
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getVisibility()I

    move-result v0

    const/16 v1, 0x8

    if-eq v0, v1, :cond_0

    .line 227
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getMeasuredWidth()I

    move-result v1

    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getMeasuredHeight()I

    move-result v2

    invoke-virtual {v0, v3, v3, v1, v2}, Landroid/view/View;->layout(IIII)V

    .line 229
    :cond_0
    return-void
.end method

.method protected onMeasure(II)V
    .locals 5
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v4, 0x40000000    # 2.0f

    .line 211
    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2, p1, p2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->measure(II)V

    .line 212
    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredWidth()I

    move-result v1

    .line 213
    .local v1, "width":I
    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getMeasuredHeight()I

    move-result v0

    .line 215
    .local v0, "height":I
    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getVisibility()I

    move-result v2

    const/16 v3, 0x8

    if-eq v2, v3, :cond_0

    .line 216
    iget-object v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-static {v1, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v3

    invoke-static {v0, v4}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v4

    invoke-virtual {v2, v3, v4}, Landroid/view/View;->measure(II)V

    .line 220
    :cond_0
    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->setMeasuredDimension(II)V

    .line 221
    return-void
.end method

.method public openShutter()V
    .locals 4

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->openShutterRunnable:Ljava/lang/Runnable;

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/view/View;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 99
    return-void
.end method

.method public recreateSurface()V
    .locals 2

    .prologue
    .line 114
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->isShowing:Z

    if-eqz v0, :cond_0

    .line 115
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    .line 116
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    const/4 v1, 0x0

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/player/DefaultPlayerSurface;->addView(Landroid/view/View;I)V

    .line 118
    :cond_0
    return-void
.end method

.method public release()V
    .locals 2

    .prologue
    .line 122
    iget-object v1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getHolder()Landroid/view/SurfaceHolder;

    move-result-object v1

    invoke-interface {v1}, Landroid/view/SurfaceHolder;->getSurface()Landroid/view/Surface;

    move-result-object v0

    .line 123
    .local v0, "surface":Landroid/view/Surface;
    if-eqz v0, :cond_0

    .line 124
    invoke-virtual {v0}, Landroid/view/Surface;->release()V

    .line 126
    :cond_0
    return-void
.end method

.method public setListener(Lcom/google/android/videos/player/PlayerSurface$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/PlayerSurface$Listener;

    .prologue
    .line 86
    iput-object p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    .line 87
    return-void
.end method

.method public setOnDisplayParametersChangedListener(Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    .prologue
    .line 159
    iput-object p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    .line 160
    return-void
.end method

.method public setVideoSize(II)V
    .locals 1
    .param p1, "videoWidth"    # I
    .param p2, "videoHeight"    # I

    .prologue
    .line 140
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I

    if-ne v0, p1, :cond_0

    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I

    if-eq v0, p2, :cond_1

    .line 141
    :cond_0
    iput p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I

    .line 142
    iput p2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I

    .line 143
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->requestLayout()V

    .line 145
    :cond_1
    return-void
.end method

.method public setVisible(Z)V
    .locals 1
    .param p1, "visible"    # Z

    .prologue
    .line 71
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->isShowing:Z

    if-ne p1, v0, :cond_0

    .line 82
    :goto_0
    return-void

    .line 74
    :cond_0
    iput-boolean p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->isShowing:Z

    .line 75
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->isShowing:Z

    if-eqz v0, :cond_1

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->addView(Landroid/view/View;)V

    goto :goto_0

    .line 79
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->shutterView:Landroid/view/View;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->removeView(Landroid/view/View;)V

    goto :goto_0
.end method

.method public setZoom(I)V
    .locals 1
    .param p1, "zoom"    # I

    .prologue
    .line 179
    iget v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoom:I

    if-eq v0, p1, :cond_0

    .line 180
    iput p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoom:I

    .line 181
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceView:Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;

    invoke-virtual {v0}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->requestLayout()V

    .line 183
    :cond_0
    return-void
.end method

.method public setZoomSupported(Z)V
    .locals 1
    .param p1, "zoomSupported"    # Z

    .prologue
    .line 130
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoomSupported:Z

    if-eq v0, p1, :cond_0

    .line 131
    iput-boolean p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoomSupported:Z

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    if-eqz v0, :cond_0

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;->onZoomSupportedChanged(Z)V

    .line 136
    :cond_0
    return-void
.end method

.method public surfaceChanged(Landroid/view/SurfaceHolder;III)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;
    .param p2, "format"    # I
    .param p3, "width"    # I
    .param p4, "height"    # I

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    .line 188
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface$Listener;->surfaceChanged()V

    .line 190
    :cond_0
    return-void
.end method

.method public surfaceCreated(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 194
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceCreated:Z

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    .line 196
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface$Listener;->surfaceCreated()V

    .line 198
    :cond_0
    return-void
.end method

.method public surfaceDestroyed(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 202
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->surfaceCreated:Z

    .line 203
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->listener:Lcom/google/android/videos/player/PlayerSurface$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface$Listener;->surfaceDestroyed()V

    .line 206
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/DefaultPlayerSurface;->closeShutter()V

    .line 207
    return-void
.end method

.method public zoomSupported()Z
    .locals 1

    .prologue
    .line 174
    iget-boolean v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface;->zoomSupported:Z

    return v0
.end method
