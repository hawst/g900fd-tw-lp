.class public final Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;
.super Ljava/lang/Object;
.source "MovieFromCursorFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/model/MovieItem;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final context:Landroid/content/Context;


# direct methods
.method private constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->context:Landroid/content/Context;

    .line 22
    return-void
.end method

.method public static movieFromCursorFactory(Landroid/content/Context;)Lcom/google/android/repolib/common/Factory;
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/repolib/common/Factory",
            "<",
            "Lcom/google/android/videos/pano/model/MovieItem;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;-><init>(Landroid/content/Context;)V

    return-object v0
.end method


# virtual methods
.method public createFrom(Landroid/database/Cursor;)Lcom/google/android/videos/pano/model/MovieItem;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 31
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->isNull(I)Z

    move-result v3

    if-eqz v3, :cond_0

    const-wide v12, 0x7fffffffffffffffL

    .line 33
    .local v12, "expirationTimestamp":J
    :goto_0
    const/4 v3, 0x0

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 34
    .local v4, "id":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->context:Landroid/content/Context;

    invoke-static {v3}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v10

    .line 35
    .local v10, "posterHeight":I
    const/4 v3, 0x5

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getInt(I)I

    move-result v15

    .line 36
    .local v15, "durationInSeconds":I
    const/4 v3, 0x7

    const/4 v5, 0x0

    move-object/from16 v0, p1

    invoke-static {v0, v3, v5}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v2

    .line 37
    .local v2, "resumeTimestamp":I
    new-instance v3, Lcom/google/android/videos/pano/model/MovieItem;

    const/4 v5, 0x2

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->context:Landroid/content/Context;

    invoke-static {v12, v13, v6}, Lcom/google/android/videos/pano/ui/PanoHelper;->getExpirationTitle(JLandroid/content/Context;)Ljava/lang/String;

    move-result-object v6

    const/4 v7, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    const/4 v8, 0x6

    move-object/from16 v0, p1

    invoke-interface {v0, v8}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    int-to-float v9, v10

    const v11, 0x3f31a787

    mul-float/2addr v9, v11

    float-to-int v9, v9

    move-object/from16 v0, p0

    iget-object v11, v0, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->context:Landroid/content/Context;

    invoke-static {v11, v4}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v11

    const/4 v14, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v14}, Landroid/database/Cursor;->getInt(I)I

    move-result v14

    if-nez v15, :cond_1

    const/16 v16, 0x0

    :goto_1
    invoke-direct/range {v3 .. v16}, Lcom/google/android/videos/pano/model/MovieItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V

    return-object v3

    .line 31
    .end local v2    # "resumeTimestamp":I
    .end local v4    # "id":Ljava/lang/String;
    .end local v10    # "posterHeight":I
    .end local v12    # "expirationTimestamp":J
    .end local v15    # "durationInSeconds":I
    :cond_0
    const/4 v3, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v3}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v12

    goto :goto_0

    .line 37
    .restart local v2    # "resumeTimestamp":I
    .restart local v4    # "id":Ljava/lang/String;
    .restart local v10    # "posterHeight":I
    .restart local v12    # "expirationTimestamp":J
    .restart local v15    # "durationInSeconds":I
    :cond_1
    div-int/lit16 v0, v2, 0x3e8

    move/from16 v16, v0

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    int-to-float v0, v15

    move/from16 v17, v0

    div-float v16, v16, v17

    goto :goto_1
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 16
    check-cast p1, Landroid/database/Cursor;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/repositories/MovieFromCursorFactory;->createFrom(Landroid/database/Cursor;)Lcom/google/android/videos/pano/model/MovieItem;

    move-result-object v0

    return-object v0
.end method
