.class public Lcom/google/android/videos/store/PurchaseRequests;
.super Ljava/lang/Object;
.source "PurchaseRequests.java"


# static fields
.field private static final WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

.field private static final WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_MATCHES_QUERY:Ljava/lang/String;

.field private static final WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_SHOW_TITLE_MATCHES_QUERY:Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 215
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

    .line 220
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_MATCHES_QUERY:Ljava/lang/String;

    .line 267
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "shows_title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " OR "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "shows_title"

    invoke-static {v1}, Lcom/google/android/videos/utils/DbUtils;->buildEscapingLikeClause(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ")"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_SHOW_TITLE_MATCHES_QUERY:Ljava/lang/String;

    return-void
.end method

.method public static createActivePurchaseRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "columns"    # [Ljava/lang/String;
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "timestamp"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 311
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    const-string v3, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND video_id = ?"

    const-string v5, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    aput-object p1, v6, v1

    const/4 v9, -0x1

    move-object v3, p0

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createActivePurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 11
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "timestamp"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 327
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    const-string v3, "account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND video_id = ?"

    const-string v5, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    aput-object p0, v6, v1

    const/4 v3, 0x1

    aput-object p2, v6, v3

    const/4 v9, -0x1

    move-object v3, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createActivePurchasesRequestForGlobalSearch([Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 13
    .param p0, "columns"    # [Ljava/lang/String;
    .param p1, "query"    # Ljava/lang/String;
    .param p2, "limit"    # I
    .param p3, "timestamp"    # J
    .param p5, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 301
    invoke-static {p1}, Lcom/google/android/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 302
    .local v11, "escapedQuery":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    sget-object v3, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_MATCHES_QUERY:Ljava/lang/String;

    const-string v5, "@NOW"

    invoke-static/range {p3 .. p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "% "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p0

    move v9, p2

    move-object/from16 v10, p5

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createActivePurchasesRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "timestamp"    # J

    .prologue
    const/4 v7, 0x0

    const/4 v1, 0x0

    .line 319
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    const-string v3, "account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW"

    const-string v5, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    aput-object p0, v6, v1

    const/4 v9, -0x1

    move-object v3, p1

    move-object v8, v7

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createContentTypesRequest(J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "nowTimestamp"    # J

    .prologue
    const/4 v6, 0x0

    const/4 v1, 0x1

    .line 498
    const-string v0, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW"

    const-string v2, "@NOW"

    invoke-static {p0, p1}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    .line 500
    .local v5, "where":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id"

    const/4 v3, 0x3

    new-array v3, v3, [Ljava/lang/String;

    const/4 v4, 0x0

    const-string v7, "account"

    aput-object v7, v3, v4

    const-string v4, "asset_type"

    aput-object v4, v3, v1

    const/4 v4, 0x2

    const-string v7, "(pinned IS NOT NULL AND pinned > 0)"

    aput-object v7, v3, v4

    const-string v4, "rating_id"

    const-string v8, "account"

    const/4 v9, -0x1

    move-object v7, v6

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMoviesSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .param p4, "timestamp"    # J
    .param p6, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 394
    invoke-static {p2}, Lcom/google/android/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 395
    .local v11, "escapedQuery":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchased_assets, videos ON asset_type = 6 AND asset_id = video_id"

    const-string v4, "rating_id"

    sget-object v3, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_MATCHES_QUERY:Ljava/lang/String;

    const-string v5, "@NOW"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v6, v3

    const/4 v3, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "% "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x0

    const/4 v8, 0x0

    move-object v3, p1

    move v9, p3

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createMyLastWatchedEpisodeRequest(Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "expireAfterTimestamp"    # J
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x1

    .line 475
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND show_id = ?"

    invoke-static {p2, p3, v2}, Lcom/google/android/videos/store/PurchaseRequests;->processVodWhereClause(JLjava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, "resume_timestamp"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " > 0"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    .line 479
    .local v5, "where":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    const-string v4, "rating_id"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v6, v3

    aput-object p4, v6, v1

    const/4 v7, 0x0

    const-string v8, "last_watched_timestamp DESC"

    move-object v3, p1

    move v9, v1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMyShowRequest(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 11
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "nowTimestamp"    # J

    .prologue
    const/4 v9, 0x1

    const/4 v1, 0x0

    .line 435
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    const-string v4, "rating_id"

    const-string v3, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW AND show_id = ?"

    const-string v5, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    aput-object p0, v6, v1

    aput-object p2, v6, v9

    const-string v7, "show_id"

    const/4 v8, 0x0

    move-object v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createMyShowsRequest(Ljava/lang/String;Z[Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 11
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "downloadedOnly"    # Z
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "nowTimestamp"    # J

    .prologue
    const/4 v1, 0x0

    .line 421
    const-string v10, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW"

    .line 422
    .local v10, "where":Ljava/lang/String;
    if-eqz p1, :cond_0

    .line 423
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v0, v10}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v2, " AND (pinned IS NOT NULL AND pinned > 0)"

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    .line 425
    :cond_0
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    const-string v4, "rating_id"

    const-string v3, "@NOW"

    invoke-static {p3, p4}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v10, v3, v5}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x1

    new-array v6, v3, [Ljava/lang/String;

    aput-object p0, v6, v1

    const-string v7, "show_id"

    const-string v8, "shows_title"

    const/4 v9, -0x1

    move-object v3, p2

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createNowPlayingRequestForGlobalSearch([Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "columns"    # [Ljava/lang/String;
    .param p1, "limit"    # I
    .param p2, "nowTimestamp"    # J
    .param p4, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    const/4 v6, 0x0

    .line 344
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x1

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    const-string v3, "NOT (hidden IN (1, 3)) AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW"

    const-string v5, "@NOW"

    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v7

    invoke-virtual {v3, v5, v7}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    move-object v3, p0

    move-object v7, v6

    move-object v8, v6

    move v9, p1

    move-object/from16 v10, p4

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method public static createNowPlayingRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "nowTimestamp"    # J

    .prologue
    const/4 v1, 0x1

    .line 352
    invoke-static {p2, p3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v10

    .line 353
    .local v10, "nowTimestampString":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id"

    const-string v4, "rating_id"

    const-string v3, "NOT (hidden IN (1, 3)) AND account = ? AND purchase_status = 2 AND ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807) > @NOW"

    const-string v5, "@NOW"

    invoke-virtual {v3, v5, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    new-array v6, v1, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v6, v3

    const/4 v7, 0x0

    const-string v3, "min( @NOW - purchase_timestamp_seconds * 1000, @NOW - last_watched_timestamp, ifnull(expiration_timestamp_seconds * 1000 - @NOW, 9223372036854775807))"

    const-string v8, "@NOW"

    invoke-virtual {v3, v8, v10}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v8

    const/4 v9, -0x1

    move-object v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createPurchaseRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 10
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 335
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const-string v2, "purchased_assets, videos ON asset_type IN (6,20) AND asset_id = video_id LEFT JOIN (seasons, shows ON show_id = shows_id) ON episode_season_id IS season_id, assets ON asset_id = assets_id AND asset_type = assets_type"

    const-string v4, "rating_id"

    const-string v5, "account = ? AND video_id = ?"

    const/4 v3, 0x2

    new-array v6, v3, [Ljava/lang/String;

    aput-object p0, v6, v1

    const/4 v3, 0x1

    aput-object p2, v6, v3

    const/4 v7, 0x0

    const-string v8, "purchase_status <> 2"

    const/4 v9, -0x1

    move-object v3, p1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    return-object v0
.end method

.method public static createShowsSearchRequestForUser(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;IJLcom/google/android/videos/store/CancellationSignalHolder;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 12
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .param p4, "timestamp"    # J
    .param p6, "cancellationSignalHolder"    # Lcom/google/android/videos/store/CancellationSignalHolder;

    .prologue
    .line 405
    invoke-static {p2}, Lcom/google/android/videos/utils/DbUtils;->escapeLikeArgument(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v11

    .line 406
    .local v11, "escapedQuery":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x1

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    const-string v4, "rating_id"

    sget-object v3, Lcom/google/android/videos/store/PurchaseRequests;->WHERE_NOT_HIDDEN_AND_ACTIVE_AND_SAME_ACCOUNT_AND_SHOW_TITLE_MATCHES_QUERY:Ljava/lang/String;

    const-string v5, "@NOW"

    invoke-static/range {p4 .. p5}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v3, v5, v6}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v5

    const/4 v3, 0x3

    new-array v6, v3, [Ljava/lang/String;

    const/4 v3, 0x0

    aput-object p0, v6, v3

    const/4 v3, 0x1

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v3, 0x2

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    const-string v8, "% "

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, "%"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    aput-object v7, v6, v3

    const/4 v7, 0x0

    const-string v8, "shows_title"

    move-object v3, p1

    move v9, p3

    move-object/from16 v10, p6

    invoke-direct/range {v0 .. v10}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILcom/google/android/videos/store/CancellationSignalHolder;)V

    return-object v0
.end method

.method private static processVodWhereClause(JLjava/lang/String;)Ljava/lang/String;
    .locals 4
    .param p0, "expireAfterTimestamp"    # J
    .param p2, "whereClause"    # Ljava/lang/String;

    .prologue
    .line 361
    const-string v0, "@NOW"

    const-wide/32 v2, 0x5265c00

    sub-long v2, p0, v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toString(J)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p2, v0, v1}, Ljava/lang/String;->replace(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
