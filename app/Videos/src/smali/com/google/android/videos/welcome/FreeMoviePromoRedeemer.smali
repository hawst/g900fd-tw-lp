.class public Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;
.super Ljava/lang/Object;
.source "FreeMoviePromoRedeemer.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;,
        Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;,
        Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;,
        Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    }
.end annotation


# instance fields
.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

.field private final purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

.field private final purchaseStoreSyncCallback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemCallback:Lcom/google/android/videos/async/NewCallback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/NewCallback",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final redeemPromotionRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Landroid/os/Handler;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;)V
    .locals 2
    .param p1, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p2, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p4, "handler"    # Landroid/os/Handler;
    .param p5, "listener"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RedeemPromotionRequest;",
            "Ljava/lang/Void;",
            ">;",
            "Landroid/os/Handler;",
            "Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "redeemPromotionRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RedeemPromotionRequest;Ljava/lang/Void;>;"
    const/4 v1, 0x0

    .line 37
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 38
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/EventLogger;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 40
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStoreSync;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    .line 41
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    .line 42
    new-instance v0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$RedeemCallback;-><init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;)V

    invoke-static {p4, v0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeemCallback:Lcom/google/android/videos/async/NewCallback;

    .line 43
    new-instance v0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$PurchaseStoreSyncCallback;-><init>(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$1;)V

    invoke-static {p4, v0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSyncCallback:Lcom/google/android/videos/async/NewCallback;

    .line 45
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/async/NewCallback;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSyncCallback:Lcom/google/android/videos/async/NewCallback;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/store/PurchaseStoreSync;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->purchaseStoreSync:Lcom/google/android/videos/store/PurchaseStoreSync;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;)Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;

    .prologue
    .line 20
    iget-object v0, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->listener:Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer$Listener;

    return-object v0
.end method


# virtual methods
.method public redeem(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "type"    # I
    .param p4, "promotionCode"    # Ljava/lang/String;

    .prologue
    .line 48
    new-instance v0, Lcom/google/android/videos/api/RedeemPromotionRequest;

    invoke-static {p3, p2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromAssetTypeAndId(ILjava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, p1, v1, p4}, Lcom/google/android/videos/api/RedeemPromotionRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 50
    .local v0, "redeemRequest":Lcom/google/android/videos/api/RedeemPromotionRequest;
    iget-object v1, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeemPromotionRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/welcome/FreeMoviePromoRedeemer;->redeemCallback:Lcom/google/android/videos/async/NewCallback;

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 51
    return-void
.end method
