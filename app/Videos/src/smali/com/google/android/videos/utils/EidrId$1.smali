.class final Lcom/google/android/videos/utils/EidrId$1;
.super Ljava/lang/Object;
.source "EidrId.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/utils/EidrId;->convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Ljava/util/concurrent/ExecutorService;Lcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$database:Lcom/google/android/videos/store/Database;

.field final synthetic val$eidr:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/async/Callback;)V
    .locals 0

    .prologue
    .line 90
    iput-object p1, p0, Lcom/google/android/videos/utils/EidrId$1;->val$database:Lcom/google/android/videos/store/Database;

    iput-object p2, p0, Lcom/google/android/videos/utils/EidrId$1;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/utils/EidrId$1;->val$eidr:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/utils/EidrId$1;->val$callback:Lcom/google/android/videos/async/Callback;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 93
    iget-object v1, p0, Lcom/google/android/videos/utils/EidrId$1;->val$database:Lcom/google/android/videos/store/Database;

    iget-object v2, p0, Lcom/google/android/videos/utils/EidrId$1;->val$account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/utils/EidrId$1;->val$eidr:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/utils/EidrId;->convertEidrIdToVideoId(Lcom/google/android/videos/store/Database;Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 94
    .local v0, "videoId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 95
    iget-object v1, p0, Lcom/google/android/videos/utils/EidrId$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/utils/EidrId$1;->val$eidr:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 99
    :goto_0
    return-void

    .line 97
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/utils/EidrId$1;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/utils/EidrId$1;->val$eidr:Ljava/lang/String;

    new-instance v3, Lcom/google/android/videos/async/NotFoundException;

    invoke-direct {v3}, Lcom/google/android/videos/async/NotFoundException;-><init>()V

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
