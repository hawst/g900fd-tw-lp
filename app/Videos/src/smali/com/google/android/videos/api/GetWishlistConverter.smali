.class final Lcom/google/android/videos/api/GetWishlistConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "GetWishlistConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/GetWishlistRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 34
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 35
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "wishlistitem"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/GetWishlistConverter;->baseUri:Ljava/lang/String;

    .line 36
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/GetWishlistConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 37
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/videos/api/GetWishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/GetWishlistConverter;->convertRequest(Lcom/google/android/videos/api/GetWishlistRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/GetWishlistRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/GetWishlistRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 41
    iget-object v0, p0, Lcom/google/android/videos/api/GetWishlistConverter;->baseUri:Ljava/lang/String;

    .line 42
    .local v0, "uri":Ljava/lang/String;
    iget-object v1, p1, Lcom/google/android/videos/api/GetWishlistRequest;->snapshotToken:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 43
    new-instance v1, Lcom/google/android/videos/utils/UriBuilder;

    invoke-direct {v1, v0}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v2, "snapshotToken"

    iget-object v3, p1, Lcom/google/android/videos/api/GetWishlistRequest;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    .line 47
    :cond_0
    invoke-static {v0}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 53
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/GetWishlistConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/GetWishlistConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    move-result-object v0

    return-object v0
.end method
