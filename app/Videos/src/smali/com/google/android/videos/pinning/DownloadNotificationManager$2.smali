.class Lcom/google/android/videos/pinning/DownloadNotificationManager$2;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;->onDownloadsStateChanged(Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 285
    iput-object p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    iput-object p2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$videoId:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 7

    .prologue
    .line 288
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$account:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$videoId:Ljava/lang/String;

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    .line 289
    .local v0, "key":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    iget-object v5, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$account:Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->val$videoId:Ljava/lang/String;

    # invokes: Lcom/google/android/videos/pinning/DownloadNotificationManager;->computeVideoData(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    invoke-static {v4, v5, v6}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1700(Lcom/google/android/videos/pinning/DownloadNotificationManager;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    move-result-object v1

    .line 291
    .local v1, "newVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;

    move-result-object v5

    monitor-enter v5

    .line 292
    if-nez v1, :cond_0

    :try_start_0
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v0}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    move-object v3, v4

    .line 294
    .local v3, "oldVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :goto_0
    monitor-exit v5

    .line 295
    if-nez v1, :cond_1

    if-nez v3, :cond_1

    .line 306
    :goto_1
    return-void

    .line 292
    .end local v3    # "oldVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;
    invoke-static {v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;

    move-result-object v4

    invoke-interface {v4, v0, v1}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    move-object v3, v4

    goto :goto_0

    .line 294
    :catchall_0
    move-exception v4

    monitor-exit v5
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    throw v4

    .line 299
    .restart local v3    # "oldVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_1
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # invokes: Lcom/google/android/videos/pinning/DownloadNotificationManager;->computeNotificationData()Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    invoke-static {v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1400(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    move-result-object v2

    .line 300
    .local v2, "notificationData":Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
    iget-object v4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$2;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;
    invoke-static {v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1600(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;

    move-result-object v4

    new-instance v5, Lcom/google/android/videos/pinning/DownloadNotificationManager$2$1;

    invoke-direct {v5, p0, v2, v1, v3}, Lcom/google/android/videos/pinning/DownloadNotificationManager$2$1;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$2;Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;)V

    invoke-virtual {v4, v5}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    goto :goto_1
.end method
