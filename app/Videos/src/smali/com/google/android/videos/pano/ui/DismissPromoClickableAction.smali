.class final Lcom/google/android/videos/pano/ui/DismissPromoClickableAction;
.super Lcom/google/android/videos/pano/ui/ClickableAction;
.source "DismissPromoClickableAction.java"


# instance fields
.field private final promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;


# direct methods
.method public constructor <init>(JLcom/google/android/videos/welcome/PromoDismisser;Ljava/lang/String;)V
    .locals 1
    .param p1, "id"    # J
    .param p3, "dismisser"    # Lcom/google/android/videos/welcome/PromoDismisser;
    .param p4, "title"    # Ljava/lang/String;

    .prologue
    .line 11
    const-string v0, ""

    invoke-direct {p0, p1, p2, p4, v0}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 12
    iput-object p3, p0, Lcom/google/android/videos/pano/ui/DismissPromoClickableAction;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    .line 13
    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 17
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/DismissPromoClickableAction;->promoDismisser:Lcom/google/android/videos/welcome/PromoDismisser;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/PromoDismisser;->dismissPromo()V

    .line 18
    invoke-virtual {p1}, Landroid/app/Activity;->finish()V

    .line 19
    return-void
.end method
