.class Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "NotificationData"
.end annotation


# instance fields
.field private final downloadsCompleted:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;",
            ">;"
        }
    .end annotation
.end field

.field private final downloadsError:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;",
            ">;"
        }
    .end annotation
.end field

.field private final downloadsOngoing:Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

.field private final downloadsPending:Ljava/util/Collection;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;)V
    .locals 0
    .param p1, "downloadsOngoing"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;",
            ">;",
            "Ljava/util/Collection",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;",
            ">;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 754
    .local p2, "downloadsPending":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;>;"
    .local p3, "downloadsCompleted":Ljava/util/Collection;, "Ljava/util/Collection<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;>;"
    .local p4, "downloadsError":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 755
    iput-object p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsOngoing:Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    .line 756
    iput-object p2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsPending:Ljava/util/Collection;

    .line 757
    iput-object p3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsCompleted:Ljava/util/Collection;

    .line 758
    iput-object p4, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsError:Ljava/util/Collection;

    .line 759
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;Lcom/google/android/videos/pinning/DownloadNotificationManager$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .param p2, "x1"    # Ljava/util/Collection;
    .param p3, "x2"    # Ljava/util/Collection;
    .param p4, "x3"    # Ljava/util/List;
    .param p5, "x4"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$1;

    .prologue
    .line 745
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Ljava/util/Collection;Ljava/util/Collection;Ljava/util/List;)V

    return-void
.end method

.method static synthetic access$2000(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsOngoing:Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    return-object v0
.end method

.method static synthetic access$2100(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsPending:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$2200(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsCompleted:Ljava/util/Collection;

    return-object v0
.end method

.method static synthetic access$2300(Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;)Ljava/util/Collection;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;

    .prologue
    .line 745
    iget-object v0, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$NotificationData;->downloadsError:Ljava/util/Collection;

    return-object v0
.end method
