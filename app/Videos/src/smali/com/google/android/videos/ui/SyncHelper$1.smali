.class Lcom/google/android/videos/ui/SyncHelper$1;
.super Ljava/lang/Object;
.source "SyncHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/SyncHelper;->createSyncWishlistcallback()Lcom/google/android/videos/async/CancelableCallback;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/SyncHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/SyncHelper;)V
    .locals 0

    .prologue
    .line 351
    iput-object p1, p0, Lcom/google/android/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/videos/ui/SyncHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 351
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/SyncHelper$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 361
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/videos/ui/SyncHelper;

    const/4 v1, 0x7

    # invokes: Lcom/google/android/videos/ui/SyncHelper;->setState(I)V
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->access$200(Lcom/google/android/videos/ui/SyncHelper;I)V

    .line 362
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 351
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/SyncHelper$1;->onResponse(Ljava/lang/String;Ljava/lang/Void;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Ljava/lang/Void;)V
    .locals 2
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 354
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/videos/ui/SyncHelper;

    const/4 v1, 0x1

    # setter for: Lcom/google/android/videos/ui/SyncHelper;->wishlistSynced:Z
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->access$002(Lcom/google/android/videos/ui/SyncHelper;Z)Z

    .line 355
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/videos/ui/SyncHelper;

    # getter for: Lcom/google/android/videos/ui/SyncHelper;->purchasesSynced:Z
    invoke-static {v0}, Lcom/google/android/videos/ui/SyncHelper;->access$100(Lcom/google/android/videos/ui/SyncHelper;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/videos/ui/SyncHelper$1;->this$0:Lcom/google/android/videos/ui/SyncHelper;

    const/4 v1, 0x4

    # invokes: Lcom/google/android/videos/ui/SyncHelper;->setState(I)V
    invoke-static {v0, v1}, Lcom/google/android/videos/ui/SyncHelper;->access$200(Lcom/google/android/videos/ui/SyncHelper;I)V

    .line 358
    :cond_0
    return-void
.end method
