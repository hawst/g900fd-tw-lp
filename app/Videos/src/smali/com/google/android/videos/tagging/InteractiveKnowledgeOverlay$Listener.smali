.class public interface abstract Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay$Listener;
.super Ljava/lang/Object;
.source "InteractiveKnowledgeOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/InteractiveKnowledgeOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCardDismissed(Lcom/google/android/videos/tagging/CardTag;)V
.end method

.method public abstract onCardListCollapseProgress(F)V
.end method

.method public abstract onCardListCollapsed(I)V
.end method

.method public abstract onCardListExpanded(I)V
.end method

.method public abstract onCardsShown(Z)V
.end method

.method public abstract onCardsViewScrollChanged(I)V
.end method

.method public abstract onClickOutsideTags()V
.end method

.method public abstract onEntityExpandingStateChanged(Z)V
.end method

.method public abstract onExpandRecentActors()V
.end method
