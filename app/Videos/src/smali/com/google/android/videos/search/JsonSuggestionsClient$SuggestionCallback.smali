.class Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SuggestionCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;",
        "Lorg/json/JSONArray;",
        ">;"
    }
.end annotation


# instance fields
.field private final assetsCachingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;

.field private final targetCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;",
            "Landroid/database/Cursor;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 152
    .local p2, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p3, "targetCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Landroid/database/Cursor;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 153
    iput-object p1, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    .line 154
    iput-object p2, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    .line 155
    iput-object p3, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    .line 156
    return-void
.end method

.method private convertSuggestion(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONObject;)[Ljava/lang/Object;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2, "suggestion"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 180
    const-string v3, "s"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 181
    .local v2, "title":Ljava/lang/String;
    const-string v3, "p"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 182
    .local v0, "finskyId":Ljava/lang/String;
    if-eqz v0, :cond_2

    .line 183
    const-string v3, "u"

    invoke-virtual {p2, v3}, Lorg/json/JSONObject;->optString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 184
    .local v1, "iconUri":Ljava/lang/String;
    if-eqz v1, :cond_0

    .line 185
    invoke-static {v1}, Lcom/google/android/videos/search/VideoSearchProvider;->getSuggestThumbnailPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v3}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    .line 187
    :cond_0
    const-string v3, "movie-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_1

    .line 188
    const/4 v3, 0x6

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v3, v2, v1}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newMovieSuggestion(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    .line 193
    .end local v1    # "iconUri":Ljava/lang/String;
    :goto_0
    return-object v3

    .line 189
    .restart local v1    # "iconUri":Ljava/lang/String;
    :cond_1
    const-string v3, "tvshow-"

    invoke-virtual {v0, v3}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 190
    const/4 v3, 0x7

    invoke-virtual {v0, v3}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, v3, v2, v1}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newShowSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    goto :goto_0

    .line 193
    .end local v1    # "iconUri":Ljava/lang/String;
    :cond_2
    invoke-direct {p0, v2}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newSearchSuggestion(Ljava/lang/String;)[Ljava/lang/Object;

    move-result-object v3

    goto :goto_0
.end method

.method private getPricingInfo(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/String;)Ljava/lang/String;
    .locals 13
    .param p1, "request"    # Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 240
    new-instance v8, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v9, p1, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->account:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v8

    iget-object v9, p1, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->country:Ljava/lang/String;

    invoke-virtual {v8, v9}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v8

    const/16 v9, 0x8

    invoke-virtual {v8, v9}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    .line 244
    .local v4, "requestBuilder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    invoke-static {p2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v4, v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v0

    .line 245
    .local v0, "addedId":Z
    if-nez v0, :cond_0

    .line 247
    iget-object v8, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v9, 0x7f0b019f

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 265
    :goto_0
    return-object v8

    .line 251
    :cond_0
    :try_start_0
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v7

    .line 252
    .local v7, "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    invoke-virtual {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v1

    .line 253
    .local v1, "assetRequest":Lcom/google/android/videos/api/AssetsRequest;
    iget-object v8, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    invoke-interface {v8, v1, v7}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 254
    invoke-virtual {v7}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .line 255
    .local v6, "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    iget-object v8, v6, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v8, v8

    const/4 v9, 0x1

    if-ne v8, v9, :cond_1

    .line 256
    iget-object v8, v6, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v9, 0x0

    aget-object v5, v8, v9

    .line 257
    .local v5, "resource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    const/4 v8, 0x1

    iget-object v9, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v8, v9}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v2

    .line 258
    .local v2, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-eqz v2, :cond_1

    .line 259
    iget-object v8, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v9, 0x7f0b0192

    const/4 v10, 0x1

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, v2, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v12, v12, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v12, v10, v11

    invoke-virtual {v8, v9, v10}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v8

    goto :goto_0

    .line 262
    .end local v1    # "assetRequest":Lcom/google/android/videos/api/AssetsRequest;
    .end local v2    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .end local v5    # "resource":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .end local v6    # "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    .end local v7    # "syncCallback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    :catch_0
    move-exception v3

    .line 263
    .local v3, "e":Ljava/lang/Exception;
    const-string v8, "can\'t fetch price"

    invoke-static {v8, v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 265
    .end local v3    # "e":Ljava/lang/Exception;
    :cond_1
    iget-object v8, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v9, 0x7f0b019f

    invoke-virtual {v8, v9}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v8

    goto :goto_0
.end method

.method private newMovieSuggestion(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2, "id"    # Ljava/lang/String;
    .param p3, "name"    # Ljava/lang/String;
    .param p4, "iconUri"    # Ljava/lang/String;

    .prologue
    .line 198
    # getter for: Lcom/google/android/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "movies"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "details"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 205
    .local v6, "uri":Landroid/net/Uri;
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->getPricingInfo(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 206
    .local v3, "detail":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p4

    move-object v2, p3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;
    .locals 3
    .param p1, "icon1"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "detail"    # Ljava/lang/String;
    .param p4, "flags"    # I
    .param p5, "data"    # Ljava/lang/String;

    .prologue
    .line 230
    # getter for: Lcom/google/android/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/search/JsonSuggestionsClient;->access$200()[Ljava/lang/String;

    move-result-object v1

    array-length v1, v1

    new-array v0, v1, [Ljava/lang/Object;

    .line 231
    .local v0, "row":[Ljava/lang/Object;
    const/4 v1, 0x2

    aput-object p1, v0, v1

    .line 232
    const/4 v1, 0x1

    aput-object p2, v0, v1

    .line 233
    const/4 v1, 0x5

    aput-object p3, v0, v1

    .line 234
    const/4 v1, 0x3

    aput-object p5, v0, v1

    .line 235
    const/4 v1, 0x4

    invoke-static {p4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v2

    aput-object v2, v0, v1

    .line 236
    return-object v0
.end method

.method private newSearchSuggestion(Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "query"    # Ljava/lang/String;

    .prologue
    .line 220
    # getter for: Lcom/google/android/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "search"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "q"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "c"

    const-string v2, "movies"

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 225
    .local v6, "uri":Landroid/net/Uri;
    const-string v1, "android.resource://android/17170445"

    const/4 v3, 0x0

    const/4 v4, 0x1

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v2, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method private newShowSuggestion(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)[Ljava/lang/Object;
    .locals 7
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "name"    # Ljava/lang/String;
    .param p3, "iconUri"    # Ljava/lang/String;

    .prologue
    .line 210
    # getter for: Lcom/google/android/videos/search/JsonSuggestionsClient;->PLAY_STORE_BASE_URI:Landroid/net/Uri;
    invoke-static {}, Lcom/google/android/videos/search/JsonSuggestionsClient;->access$300()Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "tv"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "show"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "id"

    invoke-virtual {v0, v1, p1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v6

    .line 215
    .local v6, "uri":Landroid/net/Uri;
    iget-object v0, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->context:Landroid/content/Context;

    const v1, 0x7f0b019f

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 216
    .local v3, "detail":Ljava/lang/String;
    const/4 v4, 0x0

    invoke-virtual {v6}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v5

    move-object v0, p0

    move-object v1, p3

    move-object v2, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->newRow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)[Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 160
    iget-object v0, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 161
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 141
    check-cast p1, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->onError(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONArray;)V
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
    .param p2, "response"    # Lorg/json/JSONArray;

    .prologue
    .line 166
    :try_start_0
    new-instance v2, Landroid/database/MatrixCursor;

    # getter for: Lcom/google/android/videos/search/JsonSuggestionsClient;->SUGGEST_COLUMNS:[Ljava/lang/String;
    invoke-static {}, Lcom/google/android/videos/search/JsonSuggestionsClient;->access$200()[Ljava/lang/String;

    move-result-object v4

    invoke-direct {v2, v4}, Landroid/database/MatrixCursor;-><init>([Ljava/lang/String;)V

    .line 167
    .local v2, "result":Landroid/database/MatrixCursor;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    invoke-virtual {p2}, Lorg/json/JSONArray;->length()I

    move-result v4

    if-ge v1, v4, :cond_0

    .line 168
    invoke-virtual {p2, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v4

    invoke-direct {p0, p1, v4}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->convertSuggestion(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONObject;)[Ljava/lang/Object;

    move-result-object v3

    .line 169
    .local v3, "row":[Ljava/lang/Object;
    const/4 v4, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v3, v4

    .line 170
    invoke-virtual {v2, v3}, Landroid/database/MatrixCursor;->addRow([Ljava/lang/Object;)V

    .line 167
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 172
    .end local v3    # "row":[Ljava/lang/Object;
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v4, p1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 176
    .end local v1    # "i":I
    .end local v2    # "result":Landroid/database/MatrixCursor;
    :goto_1
    return-void

    .line 173
    :catch_0
    move-exception v0

    .line 174
    .local v0, "exception":Lorg/json/JSONException;
    iget-object v4, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v4, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 141
    check-cast p1, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lorg/json/JSONArray;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionCallback;->onResponse(Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;Lorg/json/JSONArray;)V

    return-void
.end method
