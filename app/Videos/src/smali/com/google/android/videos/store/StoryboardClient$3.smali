.class Lcom/google/android/videos/store/StoryboardClient$3;
.super Ljava/lang/Object;
.source "StoryboardClient.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->requestOfflineStoryboard(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/store/StoryboardClient;

.field final synthetic val$callback:Lcom/google/android/videos/async/Callback;

.field final synthetic val$storage:I

.field final synthetic val$videoId:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/store/StoryboardClient;Lcom/google/android/videos/async/Callback;Ljava/lang/String;I)V
    .locals 0

    .prologue
    .line 107
    iput-object p1, p0, Lcom/google/android/videos/store/StoryboardClient$3;->this$0:Lcom/google/android/videos/store/StoryboardClient;

    iput-object p2, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    iput-object p3, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$videoId:Ljava/lang/String;

    iput p4, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$storage:I

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 6

    .prologue
    .line 111
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/store/StoryboardClient$3;->this$0:Lcom/google/android/videos/store/StoryboardClient;

    # getter for: Lcom/google/android/videos/store/StoryboardClient;->storyboardStore:Lcom/google/android/videos/store/AbstractFileStore;
    invoke-static {v3}, Lcom/google/android/videos/store/StoryboardClient;->access$000(Lcom/google/android/videos/store/StoryboardClient;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v3

    iget-object v4, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$videoId:Ljava/lang/String;

    iget v5, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$storage:I

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/store/AbstractFileStore;->get(Ljava/lang/Object;I)Ljava/lang/Object;

    move-result-object v3

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    :try_end_0
    .catch Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException; {:try_start_0 .. :try_end_0} :catch_0

    .line 115
    :goto_0
    return-void

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Lcom/google/android/videos/store/AbstractFileStore$StoreOperationException;
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$callback:Lcom/google/android/videos/async/Callback;

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardClient$3;->val$videoId:Ljava/lang/String;

    invoke-interface {v1, v2, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method
