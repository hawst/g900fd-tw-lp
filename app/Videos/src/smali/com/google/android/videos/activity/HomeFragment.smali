.class public abstract Lcom/google/android/videos/activity/HomeFragment;
.super Landroid/support/v4/app/Fragment;
.source "HomeFragment.java"

# interfaces
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    }
.end annotation


# instance fields
.field private bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

.field private downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field protected headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

.field protected helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

.field private helperStarted:Z

.field private final pullToRefreshOffsetMode:I

.field protected rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

.field private syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field protected uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/HomeFragment;-><init>(I)V

    .line 57
    return-void
.end method

.method public constructor <init>(I)V
    .locals 0
    .param p1, "pullToRefreshOffsetMode"    # I

    .prologue
    .line 59
    invoke-direct {p0}, Landroid/support/v4/app/Fragment;-><init>()V

    .line 60
    iput p1, p0, Lcom/google/android/videos/activity/HomeFragment;->pullToRefreshOffsetMode:I

    .line 61
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/activity/HomeFragment;)Lcom/google/android/videos/ui/SyncHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/HomeFragment;

    .prologue
    .line 25
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/activity/HomeFragment;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/activity/HomeFragment;

    .prologue
    .line 25
    iget v0, p0, Lcom/google/android/videos/activity/HomeFragment;->pullToRefreshOffsetMode:I

    return v0
.end method


# virtual methods
.method protected createHeaderListLayoutConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;
    .locals 2

    .prologue
    .line 79
    new-instance v0, Lcom/google/android/videos/activity/HomeFragment$1;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/activity/HomeFragment$1;-><init>(Lcom/google/android/videos/activity/HomeFragment;Landroid/content/Context;)V

    return-object v0
.end method

.method protected createPullToRefreshProvider()Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;
    .locals 1

    .prologue
    .line 126
    new-instance v0, Lcom/google/android/videos/activity/HomeFragment$2;

    invoke-direct {v0, p0}, Lcom/google/android/videos/activity/HomeFragment$2;-><init>(Lcom/google/android/videos/activity/HomeFragment;)V

    return-object v0
.end method

.method public ensureHelperStopped()V
    .locals 1

    .prologue
    .line 243
    iget-boolean v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helperStarted:Z

    if-eqz v0, :cond_0

    .line 244
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    invoke-interface {v0}, Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;->onStop()V

    .line 245
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helperStarted:Z

    .line 247
    :cond_0
    return-void
.end method

.method protected final getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .locals 1

    .prologue
    .line 260
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    return-object v0
.end method

.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 217
    const/4 v0, 0x0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 3

    .prologue
    .line 212
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    const/4 v1, 0x2

    const/4 v2, 0x0

    invoke-static {v0, v1, v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getMinimumHeaderHeight(Landroid/content/Context;II)I

    move-result v0

    return v0
.end method

.method protected getPlayHeaderListLayout()Lcom/google/android/play/headerlist/PlayHeaderListLayout;
    .locals 1

    .prologue
    .line 206
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    return-object v0
.end method

.method protected abstract getTitleResourceId()I
.end method

.method protected abstract getUiElementType()I
.end method

.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 143
    invoke-super {p0, p1}, Landroid/support/v4/app/Fragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 144
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/HomeActivity;

    .line 145
    .local v0, "homeActivity":Lcom/google/android/videos/activity/HomeActivity;
    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->syncDrawerState()V

    .line 146
    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->supportInvalidateOptionsMenu()V

    .line 148
    invoke-static {v0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    .line 149
    .local v2, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    new-instance v3, Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/DownloadedOnlyManager;-><init>(Landroid/content/SharedPreferences;)V

    iput-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 151
    iget-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v3, :cond_0

    .line 152
    new-instance v3, Lcom/google/android/videos/ui/BannerTextHelper;

    iget-object v4, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/activity/HomeFragment;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-direct {v3, v4, v5, v6}, Lcom/google/android/videos/ui/BannerTextHelper;-><init>(Lcom/google/android/play/headerlist/PlayHeaderListLayout;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    iput-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    .line 155
    :cond_0
    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 156
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getUiElementType()I

    move-result v1

    .line 157
    .local v1, "uiElementType":I
    if-ltz v1, :cond_1

    .line 158
    new-instance v3, Lcom/google/android/videos/logging/RootUiElementNodeImpl;

    iget-object v4, p0, Lcom/google/android/videos/activity/HomeFragment;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-direct {v3, v1, v4}, Lcom/google/android/videos/logging/RootUiElementNodeImpl;-><init>(ILcom/google/android/videos/logging/UiEventLoggingHelper;)V

    iput-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    .line 160
    :cond_1
    invoke-virtual {p0, v0, v2}, Lcom/google/android/videos/activity/HomeFragment;->onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    .line 161
    return-void
.end method

.method protected abstract onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.end method

.method public onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
    .locals 6
    .param p1, "inflater"    # Landroid/view/LayoutInflater;
    .param p2, "container"    # Landroid/view/ViewGroup;
    .param p3, "savedState"    # Landroid/os/Bundle;

    .prologue
    const v5, 0x7f0a0079

    const/4 v4, 0x0

    .line 65
    const v2, 0x7f040042

    invoke-virtual {p1, v2, p2, v4}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v1

    .line 66
    .local v1, "view":Landroid/view/View;
    const v2, 0x7f0f011f

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    iput-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 67
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->createHeaderListLayoutConfigurator()Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->configure(Lcom/google/android/play/headerlist/PlayHeaderListLayout$Configurator;)V

    .line 68
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/HomeActivity;

    .line 69
    .local v0, "homeActivity":Lcom/google/android/videos/activity/HomeActivity;
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-static {v0, v5}, Lcom/google/android/videos/ui/DogfoodHelper;->addPawsIfNeeded(Landroid/content/Context;I)Landroid/graphics/drawable/Drawable;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setFloatingControlsBackground(Landroid/graphics/drawable/Drawable;)V

    .line 71
    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 72
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 73
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->createPullToRefreshProvider()Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->setPullToRefreshProvider(Lcom/google/android/play/headerlist/PlayHeaderListLayout$PullToRefreshProvider;)V

    .line 74
    iget-object v2, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v2}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [I

    aput v5, v3, v4

    invoke-virtual {v2, v3}, Landroid/support/v4/widget/SwipeRefreshLayout;->setColorSchemeResources([I)V

    .line 75
    return-object v1
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 236
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    if-eqz v0, :cond_0

    .line 237
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    invoke-interface {v0}, Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;->onDestroy()V

    .line 239
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroy()V

    .line 240
    return-void
.end method

.method public onDestroyView()V
    .locals 1

    .prologue
    .line 251
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onDestroyView()V

    .line 252
    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->ensureHelperStopped()V

    .line 253
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    if-eqz v0, :cond_0

    .line 254
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 255
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 257
    :cond_0
    return-void
.end method

.method public onPause()V
    .locals 1

    .prologue
    .line 199
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onPause()V

    .line 200
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    if-eqz v0, :cond_0

    .line 201
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->flushImpression()V

    .line 203
    :cond_0
    return-void
.end method

.method public onResume()V
    .locals 2

    .prologue
    .line 187
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onResume()V

    .line 188
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    invoke-interface {v0}, Lcom/google/android/videos/logging/RootUiElementNode;->startNewImpression()V

    .line 191
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    if-eqz v0, :cond_1

    .line 192
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getActionBarView()Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/support/v7/widget/Toolbar;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/HomeFragment;->getTitleResourceId()I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/Toolbar;->setTitle(I)V

    .line 193
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->activateBuiltInToolbar()V

    .line 195
    :cond_1
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 177
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStart()V

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    invoke-interface {v0}, Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;->onStart()V

    .line 179
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helperStarted:Z

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    if-eqz v0, :cond_0

    .line 181
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->enable()V

    .line 183
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 228
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    if-eqz v0, :cond_0

    .line 229
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->bannerTextHelper:Lcom/google/android/videos/ui/BannerTextHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/BannerTextHelper;->disable()V

    .line 231
    :cond_0
    invoke-super {p0}, Landroid/support/v4/app/Fragment;->onStop()V

    .line 232
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 266
    packed-switch p1, :pswitch_data_0

    .line 274
    :goto_0
    return-void

    .line 271
    :pswitch_0
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->headerListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    invoke-virtual {v0}, Lcom/google/android/play/headerlist/PlayHeaderListLayout;->getSwipeRefreshLayout()Landroid/support/v4/widget/SwipeRefreshLayout;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/widget/SwipeRefreshLayout;->setRefreshing(Z)V

    goto :goto_0

    .line 266
    nop

    :pswitch_data_0
    .packed-switch 0x4
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_0
    .end packed-switch
.end method

.method protected onTransitioningChanged(Z)V
    .locals 1
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 221
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    if-eqz v0, :cond_0

    .line 222
    iget-object v0, p0, Lcom/google/android/videos/activity/HomeFragment;->helper:Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;

    invoke-interface {v0, p1}, Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;->onTransitioningChanged(Z)V

    .line 224
    :cond_0
    return-void
.end method
