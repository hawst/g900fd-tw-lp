.class Lcom/google/android/videos/store/SubtitlesClient$SubtitleTrackSerializingConverter;
.super Lcom/google/android/videos/cache/SerializingConverter;
.source "SubtitlesClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SubtitlesClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SubtitleTrackSerializingConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/cache/SerializingConverter",
        "<",
        "Ljava/util/List",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        ">;>;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/videos/cache/SerializingConverter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/store/SubtitlesClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/store/SubtitlesClient$1;

    .prologue
    .line 130
    invoke-direct {p0}, Lcom/google/android/videos/store/SubtitlesClient$SubtitleTrackSerializingConverter;-><init>()V

    return-void
.end method


# virtual methods
.method protected createObjectInputStream(Ljava/io/InputStream;)Ljava/io/ObjectInputStream;
    .locals 3
    .param p1, "source"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;

    invoke-direct {v0, p1}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 137
    .local v0, "legacyInputStream":Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;
    const-class v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const-class v2, Lcom/google/android/videos/store/V1SubtitleTrack;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 138
    const-class v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const-class v2, Lcom/google/android/videos/store/V2SubtitleTrack;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 139
    const-class v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const-class v2, Lcom/google/android/videos/store/V3SubtitleTrack;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 140
    const-class v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    const-class v2, Lcom/google/android/videos/store/V4SubtitleTrack;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/Class;Ljava/lang/Class;)V

    .line 141
    return-object v0
.end method
