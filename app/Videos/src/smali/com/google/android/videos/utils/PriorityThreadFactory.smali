.class public Lcom/google/android/videos/utils/PriorityThreadFactory;
.super Ljava/lang/Object;
.source "PriorityThreadFactory.java"

# interfaces
.implements Ljava/util/concurrent/ThreadFactory;


# instance fields
.field private final defaultFactory:Ljava/util/concurrent/ThreadFactory;

.field private final namePrefix:Ljava/lang/String;

.field private final threadPriority:I


# direct methods
.method public constructor <init>(Ljava/lang/String;I)V
    .locals 1
    .param p1, "namePrefix"    # Ljava/lang/String;
    .param p2, "threadPriority"    # I

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->namePrefix:Ljava/lang/String;

    .line 29
    iput p2, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->threadPriority:I

    .line 30
    invoke-static {}, Ljava/util/concurrent/Executors;->defaultThreadFactory()Ljava/util/concurrent/ThreadFactory;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->defaultFactory:Ljava/util/concurrent/ThreadFactory;

    .line 31
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/utils/PriorityThreadFactory;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/utils/PriorityThreadFactory;

    .prologue
    .line 16
    iget v0, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->threadPriority:I

    return v0
.end method


# virtual methods
.method public newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;
    .locals 6
    .param p1, "r"    # Ljava/lang/Runnable;

    .prologue
    const/4 v3, -0x1

    .line 37
    iget-object v4, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->defaultFactory:Ljava/util/concurrent/ThreadFactory;

    new-instance v5, Lcom/google/android/videos/utils/PriorityThreadFactory$1;

    invoke-direct {v5, p0, p1}, Lcom/google/android/videos/utils/PriorityThreadFactory$1;-><init>(Lcom/google/android/videos/utils/PriorityThreadFactory;Ljava/lang/Runnable;)V

    invoke-interface {v4, v5}, Ljava/util/concurrent/ThreadFactory;->newThread(Ljava/lang/Runnable;)Ljava/lang/Thread;

    move-result-object v2

    .line 48
    .local v2, "thread":Ljava/lang/Thread;
    invoke-virtual {v2}, Ljava/lang/Thread;->getName()Ljava/lang/String;

    move-result-object v1

    .line 49
    .local v1, "name":Ljava/lang/String;
    if-nez v1, :cond_0

    move v0, v3

    .line 50
    .local v0, "index":I
    :goto_0
    if-eq v0, v3, :cond_1

    .line 51
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    add-int/lit8 v4, v0, 0x7

    invoke-virtual {v1, v4}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    .line 55
    :goto_1
    return-object v2

    .line 49
    .end local v0    # "index":I
    :cond_0
    const-string v4, "-thread-"

    invoke-virtual {v1, v4}, Ljava/lang/String;->indexOf(Ljava/lang/String;)I

    move-result v0

    goto :goto_0

    .line 53
    .restart local v0    # "index":I
    :cond_1
    if-nez v1, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->namePrefix:Ljava/lang/String;

    :goto_2
    invoke-virtual {v2, v3}, Ljava/lang/Thread;->setName(Ljava/lang/String;)V

    goto :goto_1

    :cond_2
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v4, p0, Lcom/google/android/videos/utils/PriorityThreadFactory;->namePrefix:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    goto :goto_2
.end method
