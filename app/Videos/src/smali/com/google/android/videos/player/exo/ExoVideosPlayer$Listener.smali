.class public interface abstract Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;
.super Ljava/lang/Object;
.source "ExoVideosPlayer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/exo/ExoVideosPlayer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onExoDroppedFrames(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V
.end method

.method public abstract onExoHqToggled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZZ)V
.end method

.method public abstract onExoHttpDataSourceOpened(Lcom/google/android/videos/player/exo/ExoVideosPlayer;J)V
.end method

.method public abstract onExoInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V
.end method

.method public abstract onExoInitialized(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZ)V
.end method

.method public abstract onExoInternalAudioTrackInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
.end method

.method public abstract onExoInternalBandwidthSample(Lcom/google/android/videos/player/exo/ExoVideosPlayer;IJJ)V
.end method

.method public abstract onExoInternalConsumptionError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V
.end method

.method public abstract onExoInternalCryptoError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;I)V
.end method

.method public abstract onExoInternalDecoderInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
.end method

.method public abstract onExoInternalFormatEnabled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V
.end method

.method public abstract onExoInternalFormatSelected(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V
.end method

.method public abstract onExoInternalLoadingChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Z)V
.end method

.method public abstract onExoInternalRuntimeError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/RuntimeException;)V
.end method

.method public abstract onExoInternalUpstreamError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V
.end method

.method public abstract onExoInternalWidevineDrmError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V
.end method

.method public abstract onExoPausedFrameTimestamp(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V
.end method

.method public abstract onExoPlayerError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/ExoPlaybackException;)V
.end method

.method public abstract onExoPlayerStateChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZI)V
.end method

.method public abstract onExoReleased(I)V
.end method
