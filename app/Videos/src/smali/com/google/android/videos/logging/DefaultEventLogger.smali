.class public final Lcom/google/android/videos/logging/DefaultEventLogger;
.super Ljava/lang/Object;
.source "DefaultEventLogger.java"

# interfaces
.implements Lcom/google/android/videos/logging/EventLogger;
.implements Lcom/google/android/videos/logging/UiEventLogger;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/logging/DefaultEventLogger$Values;
    }
.end annotation


# instance fields
.field private final analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

.field private final appSessionNonce:Ljava/lang/String;

.field private final appSessionStartMillis:J

.field private currentPage:I

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private playbackSessionNonce:Ljava/lang/String;

.field private playbackSessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/logging/AnalyticsClient;Lcom/google/android/videos/utils/NetworkStatus;)V
    .locals 2
    .param p1, "analyticsClient"    # Lcom/google/android/videos/logging/AnalyticsClient;
    .param p2, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/AnalyticsClient;

    iput-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    .line 76
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/NetworkStatus;

    iput-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 77
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->currentPage:I

    .line 78
    invoke-static {}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->generateSessionNonce()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->appSessionNonce:Ljava/lang/String;

    .line 79
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->appSessionStartMillis:J

    .line 80
    return-void
.end method

.method private addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    .prologue
    .line 1049
    iget-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->getNetworkType()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->networkTypeToProto(I)I

    move-result v0

    iput v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkType:I

    .line 1050
    iget-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->getNetworkSubtype()I

    move-result v0

    iput v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkSubtype:I

    .line 1051
    iget-object v0, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    iput-boolean v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->networkIsConnected:Z

    .line 1052
    return-void
.end method

.method private static addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V
    .locals 2
    .param p0, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .param p1, "result"    # I

    .prologue
    const/4 v1, 0x0

    .line 1055
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    .line 1056
    const/4 v0, 0x0

    invoke-static {p1, v1, v1, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v0

    iput-object v0, p0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 1058
    :cond_0
    return-void
.end method

.method private static buildAssetInfo(ILjava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "assetType"    # I
    .param p1, "assetId"    # Ljava/lang/String;

    .prologue
    .line 970
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 971
    .local v0, "info":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    sparse-switch p0, :sswitch_data_0

    .line 996
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 999
    :cond_0
    :goto_0
    if-eqz p1, :cond_1

    .line 1000
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 1002
    :cond_1
    return-object v0

    .line 973
    :sswitch_0
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 976
    :sswitch_1
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 979
    :sswitch_2
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 980
    if-eqz p1, :cond_0

    .line 981
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    .line 982
    const/4 p1, 0x0

    goto :goto_0

    .line 986
    :sswitch_3
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 987
    if-eqz p1, :cond_0

    .line 988
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    .line 989
    const/4 p1, 0x0

    goto :goto_0

    .line 993
    :sswitch_4
    const/4 v1, 0x6

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 971
    :sswitch_data_0
    .sparse-switch
        0x2 -> :sswitch_4
        0x6 -> :sswitch_0
        0x12 -> :sswitch_2
        0x13 -> :sswitch_3
        0x14 -> :sswitch_1
    .end sparse-switch
.end method

.method private static buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 1
    .param p0, "assetId"    # Ljava/lang/String;

    .prologue
    .line 956
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 957
    .local v0, "info":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    if-eqz p0, :cond_0

    .line 958
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 960
    :cond_0
    return-object v0
.end method

.method private static buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "videoId"    # Ljava/lang/String;
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "isTrailer"    # Z

    .prologue
    .line 1007
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;-><init>()V

    .line 1008
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    if-eqz p0, :cond_0

    .line 1009
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 1011
    :cond_0
    if-eqz p1, :cond_1

    .line 1012
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->showId:Ljava/lang/String;

    .line 1014
    :cond_1
    if-eqz p2, :cond_2

    .line 1015
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->seasonId:Ljava/lang/String;

    .line 1018
    :cond_2
    if-eqz p0, :cond_6

    .line 1019
    if-eqz p3, :cond_3

    .line 1020
    const/4 v1, 0x3

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 1036
    :goto_0
    return-object v0

    .line 1021
    :cond_3
    if-nez p2, :cond_4

    if-eqz p1, :cond_5

    .line 1022
    :cond_4
    const/4 v1, 0x2

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 1024
    :cond_5
    const/4 v1, 0x1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 1026
    :cond_6
    if-eqz p2, :cond_7

    .line 1027
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 1028
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 1029
    :cond_7
    if-eqz p1, :cond_8

    .line 1030
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->assetId:Ljava/lang/String;

    .line 1031
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0

    .line 1033
    :cond_8
    const/4 v1, 0x0

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    goto :goto_0
.end method

.method private static buildAssetInfoForEidrID(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .locals 2
    .param p0, "eidrId"    # Ljava/lang/String;

    .prologue
    .line 964
    invoke-static {p0}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    .line 965
    .local v0, "info":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    const/4 v1, 0x7

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;->type:I

    .line 966
    return-object v0
.end method

.method private static buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
    .locals 2
    .param p0, "pingValues"    # [I
    .param p1, "taskType"    # I

    .prologue
    .line 1061
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;-><init>()V

    .line 1062
    .local v0, "task":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;
    add-int/lit8 v1, p1, 0x0

    aget v1, p0, v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->runningCount:I

    .line 1063
    add-int/lit8 v1, p1, 0x6

    aget v1, p0, v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->activeCount:I

    .line 1064
    add-int/lit8 v1, p1, 0xc

    aget v1, p0, v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->canceledCount:I

    .line 1065
    add-int/lit8 v1, p1, 0x12

    aget v1, p0, v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->failedCount:I

    .line 1066
    add-int/lit8 v1, p1, 0x18

    aget v1, p0, v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;->backedOffCount:I

    .line 1067
    return-object v0
.end method

.method private static buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;
    .locals 3
    .param p0, "type"    # I
    .param p1, "detail"    # I
    .param p2, "additional"    # I
    .param p3, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 922
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 924
    .local v0, "cause":Ljava/lang/Throwable;
    :goto_0
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;-><init>()V

    .line 925
    .local v1, "error":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;
    iput p0, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->type:I

    .line 926
    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->detail:I

    .line 927
    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->additional:I

    .line 928
    invoke-static {p3}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->objectToClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->exceptionClassname:Ljava/lang/String;

    .line 929
    invoke-static {p3}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->throwableToLocation(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->exceptionLocation:Ljava/lang/String;

    .line 930
    invoke-static {v0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->objectToClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->exceptionCauseClassname:Ljava/lang/String;

    .line 931
    invoke-static {v0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->throwableToLocation(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->exceptionCauseLocation:Ljava/lang/String;

    .line 932
    return-object v1

    .line 922
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "error":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Throwable;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0
.end method

.method private buildErrorEvent(II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 2
    .param p1, "type"    # I
    .param p2, "detail"    # I

    .prologue
    .line 913
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, p2, v0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    return-object v0
.end method

.method private buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 2
    .param p1, "type"    # I
    .param p2, "detail"    # I
    .param p3, "additional"    # I
    .param p4, "exception"    # Ljava/lang/Throwable;

    .prologue
    .line 937
    const/4 v1, 0x2

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 938
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 939
    return-object v0
.end method

.method private buildErrorEvent(ILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 1
    .param p1, "type"    # I
    .param p2, "exception"    # Ljava/lang/Throwable;

    .prologue
    const/4 v0, 0x0

    .line 917
    invoke-direct {p0, p1, v0, v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    return-object v0
.end method

.method private buildExternalPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 2
    .param p1, "page"    # I

    .prologue
    .line 896
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 897
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalPage:I

    .line 898
    return-object v0
.end method

.method private buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 2
    .param p1, "eventType"    # I

    .prologue
    .line 874
    invoke-direct {p0, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 875
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->currentPage:I

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pageType:I

    .line 876
    return-object v0
.end method

.method private buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 3
    .param p1, "page"    # I

    .prologue
    .line 888
    iget v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->currentPage:I

    .line 889
    .local v1, "previousPage":I
    iput p1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->currentPage:I

    .line 890
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 891
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->previousPage:I

    .line 892
    return-object v0
.end method

.method private buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 6
    .param p1, "eventType"    # I

    .prologue
    .line 880
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;-><init>()V

    .line 881
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->appSessionNonce:Ljava/lang/String;

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v2

    iget-wide v4, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->appSessionStartMillis:J

    sub-long/2addr v2, v4

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildSession(Ljava/lang/String;J)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    .line 883
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventType:I

    .line 884
    return-object v0
.end method

.method private buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 6
    .param p1, "eventType"    # I

    .prologue
    .line 902
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 903
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    .line 904
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->eventType:I

    .line 905
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionNonce:Ljava/lang/String;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    if-eqz v1, :cond_0

    .line 906
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionNonce:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    invoke-interface {v3}, Lcom/google/android/videos/player/logging/SessionTimeProvider;->getSessionTimeMs()I

    move-result v3

    int-to-long v4, v3

    invoke-static {v2, v4, v5}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildSession(Ljava/lang/String;J)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    .line 909
    :cond_0
    return-object v0
.end method

.method private static buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;
    .locals 2
    .param p0, "playerType"    # I
    .param p1, "assetInfo"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    .param p2, "isOffline"    # Z

    .prologue
    .line 1041
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;-><init>()V

    .line 1042
    .local v0, "playbackInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;
    invoke-static {p0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->playerTypeToProto(I)I

    move-result v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;->playerType:I

    .line 1043
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 1044
    if-eqz p2, :cond_0

    const/16 v1, 0x64

    :goto_0
    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;->downloadedPercent:I

    .line 1045
    return-object v0

    .line 1044
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private buildPlayerError(IILjava/lang/Exception;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;
    .locals 3
    .param p1, "errorType"    # I
    .param p2, "errorCode"    # I
    .param p3, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 943
    if-nez p3, :cond_0

    const/4 v0, 0x0

    .line 945
    .local v0, "cause":Ljava/lang/Throwable;
    :goto_0
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;-><init>()V

    .line 946
    .local v1, "error":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;
    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->type:I

    .line 947
    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->detailCode:I

    .line 948
    invoke-static {p3}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->objectToClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->exceptionClassname:Ljava/lang/String;

    .line 949
    invoke-static {p3}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->throwableToLocation(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->exceptionLocation:Ljava/lang/String;

    .line 950
    invoke-static {v0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->objectToClassName(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->exceptionCauseClassname:Ljava/lang/String;

    .line 951
    invoke-static {v0}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->throwableToLocation(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;->exceptionCauseLocation:Ljava/lang/String;

    .line 952
    return-object v1

    .line 943
    .end local v0    # "cause":Ljava/lang/Throwable;
    .end local v1    # "error":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;
    :cond_0
    invoke-virtual {p3}, Ljava/lang/Exception;->getCause()Ljava/lang/Throwable;

    move-result-object v0

    goto :goto_0
.end method

.method private static buildSession(Ljava/lang/String;J)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;
    .locals 1
    .param p0, "sessionNonce"    # Ljava/lang/String;
    .param p1, "sessionTimeMs"    # J

    .prologue
    .line 867
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;-><init>()V

    .line 868
    .local v0, "proto":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;
    iput-object p0, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;->sessionId:Ljava/lang/String;

    .line 869
    iput-wide p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;->sessionTimeMillis:J

    .line 870
    return-object v0
.end method

.method private buildSubtitleEvent(ILcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .locals 3
    .param p1, "eventType"    # I
    .param p2, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 739
    invoke-direct {p0, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 740
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    if-eqz p2, :cond_0

    .line 741
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->subtitleLanguageCode:Ljava/lang/String;

    .line 743
    :cond_0
    return-object v0
.end method

.method private makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;
    .locals 2
    .param p1, "timing"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;

    .prologue
    .line 437
    new-instance v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    invoke-direct {v0}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;-><init>()V

    .line 438
    .local v0, "taskTiming":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;
    iget v1, p1, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->start:I

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->startMillis:I

    .line 439
    iget v1, p1, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->end:I

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->endMillis:I

    .line 440
    iget-boolean v1, p1, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;->hasFailures:Z

    iput-boolean v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;->possiblyInaccurate:Z

    .line 441
    return-object v0
.end method


# virtual methods
.method public onAppDrmInit(II)V
    .locals 3
    .param p1, "frameworkDrmLevel"    # I
    .param p2, "frameworkDrmError"    # I

    .prologue
    .line 397
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 398
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 399
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->type:I

    .line 400
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->frameworkDrmError:I

    .line 401
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->frameworkDrmLevel:I

    .line 402
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 403
    return-void
.end method

.method public onAppDrmInitFailed(IIII)V
    .locals 3
    .param p1, "status"    # I
    .param p2, "wvError"    # I
    .param p3, "frameworkDrmLevel"    # I
    .param p4, "frameworkDrmError"    # I

    .prologue
    .line 408
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 409
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 410
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 411
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->type:I

    .line 412
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput p4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->frameworkDrmError:I

    .line 413
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput p3, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->frameworkDrmLevel:I

    .line 414
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 415
    return-void
.end method

.method public onCaptionSettingsPageOpened()V
    .locals 2

    .prologue
    .line 274
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 275
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 276
    return-void
.end method

.method public onCastDisplayDetected()V
    .locals 2

    .prologue
    .line 650
    const/16 v1, 0x18

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 651
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v1, 0x4

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    .line 652
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 653
    return-void
.end method

.method public onClickEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V
    .locals 2
    .param p1, "clickEvent"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .prologue
    .line 861
    const/16 v1, 0x25

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 862
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->click:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .line 863
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 864
    return-void
.end method

.method public onDatabaseUpgrade(II)V
    .locals 2
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I

    .prologue
    .line 657
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 658
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    .line 659
    iput p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    .line 660
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 661
    return-void
.end method

.method public onDatabaseUpgradeError(IILjava/lang/Exception;)V
    .locals 2
    .param p1, "oldVersion"    # I
    .param p2, "newVersion"    # I
    .param p3, "e"    # Ljava/lang/Exception;

    .prologue
    .line 665
    const/16 v1, 0x9

    invoke-direct {p0, v1, p3}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(ILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 666
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->oldVersion:I

    .line 667
    iput p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->newVersion:I

    .line 668
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 669
    return-void
.end method

.method public onDeviceCapabilitiesStreamFilter(Lcom/google/android/videos/streams/MediaStream;IZ)V
    .locals 3
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "maxH264DecodableFrameSize"    # I
    .param p3, "wasFiltered"    # Z

    .prologue
    .line 732
    const/16 v1, 0x15

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 733
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v2, p1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v2, v2, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->itag:I

    .line 734
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->maxH264DecodableFrameSize:I

    .line 735
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-boolean p3, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->deviceStreamFilterEnabled:Z

    .line 736
    return-void
.end method

.method public onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "result"    # I
    .param p3, "filteringType"    # I
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 212
    const/16 v1, 0x13

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 213
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 214
    invoke-static {v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 215
    const/4 v1, 0x0

    invoke-static {p4, p5, p6, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 216
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 217
    return-void
.end method

.method public onDismissDownloadDialog(Z)V
    .locals 2
    .param p1, "remove"    # Z

    .prologue
    .line 341
    if-eqz p1, :cond_0

    const/16 v1, 0x1f

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 344
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 345
    return-void

    .line 341
    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    :cond_0
    const/16 v1, 0x20

    goto :goto_0
.end method

.method public onDismissDownloadErrorDialog()V
    .locals 2

    .prologue
    .line 358
    const/16 v1, 0x21

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 359
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 360
    return-void
.end method

.method public onExpandRecentActors()V
    .locals 2

    .prologue
    .line 702
    const/16 v1, 0x13

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 704
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 705
    return-void
.end method

.method public onExternalApiCallFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "action"    # I
    .param p2, "eidrId"    # Ljava/lang/String;
    .param p3, "referer"    # Ljava/lang/String;
    .param p4, "throwable"    # Ljava/lang/Throwable;

    .prologue
    const/4 v2, 0x0

    .line 842
    const/16 v1, 0x23

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 843
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->apiActionToProto(I)I

    move-result v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    .line 844
    invoke-static {p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfoForEidrID(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 845
    const/16 v1, 0x12

    invoke-static {v1, v2, v2, p4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 846
    if-eqz p3, :cond_0

    .line 847
    iput-object p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 849
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 850
    return-void
.end method

.method public onExternalApiCallSuccess(ILjava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "action"    # I
    .param p2, "eidrId"    # Ljava/lang/String;
    .param p3, "referer"    # Ljava/lang/String;

    .prologue
    .line 821
    const/16 v1, 0x23

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 822
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->apiActionToProto(I)I

    move-result v1

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    .line 823
    invoke-static {p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfoForEidrID(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 824
    if-eqz p3, :cond_0

    .line 825
    iput-object p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 827
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 828
    return-void
.end method

.method public onExternalApiQuery(Ljava/lang/String;)V
    .locals 2
    .param p1, "referer"    # Ljava/lang/String;

    .prologue
    .line 832
    const/16 v1, 0x23

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 833
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v1, 0x5

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->externalApiAction:I

    .line 834
    if-eqz p1, :cond_0

    .line 835
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 837
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 838
    return-void
.end method

.method public onFrameworkDrmInit(J)V
    .locals 3
    .param p1, "drmInfoRequestStatus"    # J

    .prologue
    .line 379
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 380
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 381
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    const/4 v2, 0x2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->type:I

    .line 382
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput-wide p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->status:J

    .line 383
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 384
    return-void
.end method

.method public onFrameworkDrmInitFailed()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    .line 388
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 389
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/16 v1, 0x10

    const/4 v2, 0x0

    invoke-static {v1, v3, v3, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 390
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 391
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    const/4 v2, 0x2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->type:I

    .line 392
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 393
    return-void
.end method

.method public onHelpAndFeedbackOpened()V
    .locals 2

    .prologue
    .line 300
    const/4 v1, 0x1

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildExternalPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 301
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 302
    return-void
.end method

.method public onImpressionEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;)V
    .locals 2
    .param p1, "impressionEvent"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .prologue
    .line 854
    const/16 v1, 0x24

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 855
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput-object p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->impression:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 856
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 857
    return-void
.end method

.method public onInfoCardDismissed(I)V
    .locals 3
    .param p1, "cardType"    # I

    .prologue
    .line 695
    const/16 v1, 0x12

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 696
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->cardTypeToProto(I)I

    move-result v2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->infoCardType:I

    .line 697
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 698
    return-void
.end method

.method public onInfoCardFeedbackReport(Z)V
    .locals 2
    .param p1, "sentSuccessfully"    # Z

    .prologue
    .line 709
    if-eqz p1, :cond_0

    const/16 v1, 0x16

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 712
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 713
    return-void

    .line 709
    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    :cond_0
    const/16 v1, 0x17

    goto :goto_0
.end method

.method public onInfoCardsCollapsed(I)V
    .locals 2
    .param p1, "eventCause"    # I

    .prologue
    .line 688
    const/16 v1, 0x11

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 689
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->infoCardEventCause:I

    .line 690
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 691
    return-void
.end method

.method public onInfoCardsExpanded(IZ)V
    .locals 2
    .param p1, "eventCause"    # I
    .param p2, "isAfterSeek"    # Z

    .prologue
    .line 680
    const/16 v1, 0x10

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 681
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->infoCardEventCause:I

    .line 682
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-boolean p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->infoCardExpandAfterSeek:Z

    .line 683
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 684
    return-void
.end method

.method public onInfoCardsShown(Z)V
    .locals 2
    .param p1, "hasRecentActorsCard"    # Z

    .prologue
    .line 673
    const/16 v1, 0xf

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 674
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-boolean p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->hasRecentActorsCard:Z

    .line 675
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 676
    return-void
.end method

.method public onLicenseRefreshCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 156
    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 157
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 158
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 159
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 160
    return-void
.end method

.method public onLicenseRefreshError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 164
    const/4 v1, 0x5

    invoke-direct {p0, v1, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(ILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 166
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 167
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 168
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v1, :cond_0

    .line 169
    iget-object v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-object v1, p2

    check-cast v1, Lcom/google/android/videos/drm/DrmException;

    iget v1, v1, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    iput v1, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->detail:I

    .line 171
    :cond_0
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_1

    .line 172
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    check-cast p2, Lcom/google/android/videos/drm/DrmFallbackException;

    .end local p2    # "cause":Ljava/lang/Throwable;
    iget v2, p2, Lcom/google/android/videos/drm/DrmFallbackException;->fallbackDrmLevel:I

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;->additional:I

    .line 174
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 175
    return-void
.end method

.method public onLicenseReleaseCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 179
    const/16 v1, 0xf

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 180
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 181
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 182
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 183
    return-void
.end method

.method public onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V
    .locals 5
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 187
    const/4 v1, 0x0

    .line 188
    .local v1, "detail":I
    const/4 v0, 0x0

    .line 189
    .local v0, "additional":I
    instance-of v4, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v4, :cond_0

    move-object v3, p2

    .line 190
    check-cast v3, Lcom/google/android/videos/drm/DrmException;

    .line 191
    .local v3, "exception":Lcom/google/android/videos/drm/DrmException;
    iget-object v4, v3, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    invoke-virtual {v4}, Lcom/google/android/videos/drm/DrmException$DrmError;->ordinal()I

    move-result v1

    .line 192
    iget v0, v3, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    .line 195
    .end local v3    # "exception":Lcom/google/android/videos/drm/DrmException;
    :cond_0
    const/16 v4, 0x11

    invoke-direct {p0, v4, v1, v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v2

    .line 197
    .local v2, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v4

    iput-object v4, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 198
    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 199
    iget-object v4, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v4, v2}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 200
    return-void
.end method

.method public onModularDrmInit(I)V
    .locals 3
    .param p1, "securityLevel"    # I

    .prologue
    .line 370
    const/16 v1, 0x1b

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 371
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    .line 372
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    const/4 v2, 0x3

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->type:I

    .line 373
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->drmInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$DrmInfo;->modularDrmSecurityLevel:I

    .line 374
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 375
    return-void
.end method

.method public onMovieDetailsPageOpened(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 230
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 231
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v1, 0x6

    invoke-static {v1, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(ILjava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 232
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 233
    return-void
.end method

.method public onOpenedPlayStoreForAsset(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;
    .param p5, "result"    # I

    .prologue
    .line 749
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 750
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p5}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 751
    const/4 v1, 0x0

    invoke-static {p2, p3, p4, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 752
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 753
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 754
    return-void
.end method

.method public onOpenedPlayStoreForMovies(II)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "result"    # I

    .prologue
    .line 797
    const/4 v1, 0x4

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildExternalPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 798
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 799
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 800
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 801
    return-void
.end method

.method public onOpenedPlayStoreForMoviesAndShows(II)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "result"    # I

    .prologue
    .line 813
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildExternalPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 814
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 815
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 816
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 817
    return-void
.end method

.method public onOpenedPlayStoreForShows(II)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "result"    # I

    .prologue
    .line 805
    const/4 v1, 0x5

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildExternalPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 806
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 807
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 808
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 809
    return-void
.end method

.method public onPinAction(Ljava/lang/String;ZII)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "pin"    # Z
    .param p3, "pinQuality"    # I
    .param p4, "storage"    # I

    .prologue
    .line 106
    if-eqz p2, :cond_1

    const/16 v1, 0xa

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 108
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 109
    if-eqz p2, :cond_0

    .line 110
    iput p3, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinQuality:I

    .line 111
    iput p4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinStorage:I

    .line 113
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 114
    return-void

    .line 106
    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    :cond_1
    const/16 v1, 0xd

    goto :goto_0
.end method

.method public onPinClick(Z)V
    .locals 2
    .param p1, "invokedFromContextMenu"    # Z

    .prologue
    .line 306
    const/16 v1, 0x1c

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 307
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    if-eqz p1, :cond_0

    .line 308
    const/16 v1, 0x10

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 310
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 311
    return-void
.end method

.method public onPinningCompleted(Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 118
    const/16 v1, 0xb

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 119
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 120
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 121
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 122
    return-void
.end method

.method public onPinningError(Ljava/lang/String;Ljava/lang/Integer;Ljava/lang/Throwable;ZZII)V
    .locals 3
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "itag"    # Ljava/lang/Integer;
    .param p3, "cause"    # Ljava/lang/Throwable;
    .param p4, "fatal"    # Z
    .param p5, "exceededMaxRetries"    # Z
    .param p6, "drmErrorCode"    # I
    .param p7, "failedReason"    # I

    .prologue
    .line 127
    const/4 v1, 0x2

    invoke-direct {p0, v1, p7, p6, p3}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 129
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 130
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 131
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    .line 132
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    iput-boolean p4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;->fatal:Z

    .line 133
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    iput-boolean p5, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;->exceededMaxRetries:Z

    .line 134
    if-eqz p2, :cond_0

    .line 135
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningErrorInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;

    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PinningErrorInfo;->itag:I

    .line 137
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 138
    return-void
.end method

.method public onPlayStoreSearch(ILjava/lang/String;I)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "result"    # I

    .prologue
    .line 774
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 775
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p3}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 776
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 777
    if-eqz p2, :cond_0

    .line 778
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    .line 780
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 781
    return-void
.end method

.method public onPlayStoreUriOpen(ILjava/lang/String;I)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "category"    # Ljava/lang/String;
    .param p3, "result"    # I

    .prologue
    .line 785
    const/16 v1, 0x15

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 786
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {v0, p3}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 787
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 788
    if-eqz p2, :cond_0

    .line 789
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->searchCategory:Ljava/lang/String;

    .line 791
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 792
    return-void
.end method

.method public onPlaybackInit(Ljava/lang/String;I)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "displayType"    # I

    .prologue
    .line 419
    const/16 v1, 0x19

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 420
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 421
    iput p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->displayType:I

    .line 422
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 423
    return-void
.end method

.method public onPlaybackInitError(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZZILjava/lang/Exception;)V
    .locals 5
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "isTrailer"    # Z
    .param p5, "userVisible"    # Z
    .param p6, "canRetry"    # Z
    .param p7, "errorType"    # I
    .param p8, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v4, 0x1

    const/4 v2, 0x0

    .line 428
    if-eqz p5, :cond_1

    move v3, v4

    :goto_0
    if-eqz p6, :cond_0

    const/4 v2, 0x2

    :cond_0
    add-int v1, v3, v2

    .line 429
    .local v1, "flags":I
    invoke-direct {p0, v4, p7, v1, p8}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 431
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1, p2, p3, p4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v2

    iput-object v2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 432
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 433
    iget-object v2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v2, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 434
    return-void

    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .end local v1    # "flags":I
    :cond_1
    move v3, v2

    .line 428
    goto :goto_0
.end method

.method public onPlaybackPreparationStats(ZLjava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLandroid/util/SparseArray;)V
    .locals 9
    .param p1, "hasFailures"    # Z
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "displayType"    # I
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "isOffline"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z",
            "Ljava/lang/String;",
            "II",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZ",
            "Landroid/util/SparseArray",
            "<",
            "Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 452
    .local p10, "tasks":Landroid/util/SparseArray;, "Landroid/util/SparseArray<Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;>;"
    const/16 v7, 0x26

    invoke-direct {p0, v7}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v2

    .line 454
    .local v2, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-direct {v7}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;-><init>()V

    iput-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    .line 455
    new-instance v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {v7}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;-><init>()V

    iput-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    .line 456
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    iput-boolean p1, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->possiblyInaccurate:Z

    .line 457
    new-instance v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    invoke-direct {v7}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;-><init>()V

    iput-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    .line 458
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->session:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;

    iput-object p2, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Session;->sessionId:Ljava/lang/String;

    .line 460
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-virtual/range {p10 .. p10}, Landroid/util/SparseArray;->size()I

    move-result v4

    .local v4, "size":I
    :goto_0
    if-ge v3, v4, :cond_0

    .line 461
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->keyAt(I)I

    move-result v5

    .line 462
    .local v5, "task":I
    move-object/from16 v0, p10

    invoke-virtual {v0, v3}, Landroid/util/SparseArray;->valueAt(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;

    .line 463
    .local v6, "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    packed-switch v5, :pswitch_data_0

    .line 460
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 465
    :pswitch_0
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoGetRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 468
    :pswitch_1
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->videoStreamsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 471
    :pswitch_2
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->purchasesRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 474
    :pswitch_3
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->trailerAssetsRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 477
    :pswitch_4
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->localPlayerLoad:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 480
    :pswitch_5
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->buildRenderers:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 483
    :pswitch_6
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->getKeyRequest:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 486
    :pswitch_7
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackPreparationStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;

    invoke-direct {p0, v6}, Lcom/google/android/videos/logging/DefaultEventLogger;->makeTaskTiming(Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackPreparationStats;->provideKeyResponse:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$TaskTiming;

    goto :goto_1

    .line 491
    .end local v5    # "task":I
    .end local v6    # "timing":Lcom/google/android/videos/player/logging/PlaybackPreparationLogger$TaskTiming;
    :cond_0
    invoke-static/range {p5 .. p8}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    .line 492
    .local v1, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    move/from16 v0, p9

    invoke-static {p3, v1, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->info:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    .line 493
    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 494
    iget-object v7, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v7, v2}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 495
    return-void

    .line 463
    nop

    :pswitch_data_0
    .packed-switch 0x2
        :pswitch_0
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
    .end packed-switch
.end method

.method public onPlayerDroppedFrames(I)V
    .locals 2
    .param p1, "count"    # I

    .prologue
    .line 564
    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 565
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->droppedFrameCount:I

    .line 566
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 567
    return-void
.end method

.method public onPlayerEnded(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/player/logging/DerivedStats;)V
    .locals 12
    .param p1, "playerType"    # I
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z
    .param p6, "isOffline"    # Z
    .param p7, "stats"    # Lcom/google/android/videos/player/logging/DerivedStats;

    .prologue
    .line 612
    new-instance v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    invoke-direct {v6}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;-><init>()V

    .line 613
    .local v6, "playbackStats":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->joiningTimeMs:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->joiningTimeMillis:I

    .line 614
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalPlayingTimeMs:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->playingTimeMillis:I

    .line 615
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->droppedFrameCount:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalDroppedFrameCount:I

    .line 616
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->errorCount:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalErrorCount:I

    .line 617
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->failureCount:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalFailureCount:I

    .line 618
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingCount:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingCount:I

    .line 619
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->totalRebufferingTimeMs:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->totalRebufferingTimeMillis:I

    .line 620
    move-object/from16 v0, p7

    iget-object v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->itagsUsed:Ljava/util/HashSet;

    invoke-static {v7}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->integerSetToArray(Ljava/util/HashSet;)[I

    move-result-object v7

    iput-object v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->itagsUsed:[I

    .line 621
    move-object/from16 v0, p7

    iget-object v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->connectionTypesUsed:Ljava/util/HashSet;

    invoke-static {v7}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->integerSetToArray(Ljava/util/HashSet;)[I

    move-result-object v5

    .line 622
    .local v5, "networkTypesUsed":[I
    const/4 v4, 0x0

    .local v4, "i":I
    :goto_0
    array-length v7, v5

    if-ge v4, v7, :cond_0

    .line 623
    aget v7, v5, v4

    invoke-static {v7}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->networkTypeToProto(I)I

    move-result v7

    aput v7, v5, v4

    .line 622
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 625
    :cond_0
    iput-object v5, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->networkTypesUsed:[I

    .line 626
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingCount:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingCount:I

    .line 627
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->earlyRebufferingTimeMs:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->earlyRebufferingTimeMillis:I

    .line 628
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->firstItag:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->firstItag:I

    .line 629
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->secondItag:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItag:I

    .line 630
    move-object/from16 v0, p7

    iget v7, v0, Lcom/google/android/videos/player/logging/DerivedStats;->secondItagSelectionTimeMs:I

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->secondItagSelectionTimeMillis:I

    .line 631
    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/google/android/videos/player/logging/DerivedStats;->aggregateFormatStatsPlayingTimeMs:J

    const-wide/16 v10, 0x0

    cmp-long v7, v8, v10

    if-lez v7, :cond_1

    .line 632
    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/google/android/videos/player/logging/DerivedStats;->videoHeightTimesPlayingTimeMs:J

    move-object/from16 v0, p7

    iget-wide v10, v0, Lcom/google/android/videos/player/logging/DerivedStats;->aggregateFormatStatsPlayingTimeMs:J

    div-long/2addr v8, v10

    long-to-int v7, v8

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoResolution:I

    .line 634
    move-object/from16 v0, p7

    iget-wide v8, v0, Lcom/google/android/videos/player/logging/DerivedStats;->videoBandwidthTimesPlayingTimeMs:J

    move-object/from16 v0, p7

    iget-wide v10, v0, Lcom/google/android/videos/player/logging/DerivedStats;->aggregateFormatStatsPlayingTimeMs:J

    div-long/2addr v8, v10

    long-to-int v7, v8

    iput v7, v6, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;->averageVideoBandwidth:I

    .line 638
    :cond_1
    const/4 v7, 0x2

    invoke-direct {p0, v7}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v3

    .line 639
    .local v3, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static/range {p2 .. p5}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v2

    .line 640
    .local v2, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v7, v3, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    move/from16 v0, p6

    invoke-static {p1, v2, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    move-result-object v8

    iput-object v8, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->info:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    .line 641
    iget-object v7, v3, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-object v6, v7, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->playbackStats:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackStats;

    .line 642
    iget-object v7, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v7, v3}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 644
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionNonce:Ljava/lang/String;

    .line 645
    const/4 v7, 0x0

    iput-object v7, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    .line 646
    return-void
.end method

.method public onPlayerError(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V
    .locals 4
    .param p1, "playerType"    # I
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z
    .param p6, "isOffline"    # Z
    .param p7, "mediaTimeMs"    # I
    .param p8, "errorType"    # I
    .param p9, "errorCode"    # I
    .param p10, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 542
    const/4 v2, 0x4

    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v1

    .line 543
    .local v1, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p2, p3, p4, p5}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    .line 544
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-static {p1, v0, p6}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->info:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    .line 545
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 546
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-direct {p0, p8, p9, p10}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlayerError(IILjava/lang/Exception;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;

    move-result-object v3

    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;

    .line 547
    iget-object v2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v2, v1}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 548
    return-void
.end method

.method public onPlayerFailed(ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZIIILjava/lang/Exception;)V
    .locals 4
    .param p1, "playerType"    # I
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "seasonId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z
    .param p6, "isOffline"    # Z
    .param p7, "mediaTimeMs"    # I
    .param p8, "errorType"    # I
    .param p9, "errorCode"    # I
    .param p10, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 554
    const/4 v2, 0x5

    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v1

    .line 555
    .local v1, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p2, p3, p4, p5}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    .line 556
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-static {p1, v0, p6}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->info:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    .line 557
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p7, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 558
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-direct {p0, p8, p9, p10}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlayerError(IILjava/lang/Exception;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;

    move-result-object v3

    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerError;

    .line 559
    iget-object v2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v2, v1}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 560
    return-void
.end method

.method public onPlayerFormatEnabled(II)V
    .locals 2
    .param p1, "itag"    # I
    .param p2, "trigger"    # I

    .prologue
    .line 578
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 579
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->itag:I

    .line 580
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->trigger:I

    .line 581
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 582
    return-void
.end method

.method public onPlayerFormatSelected(II)V
    .locals 2
    .param p1, "itag"    # I
    .param p2, "trigger"    # I

    .prologue
    .line 571
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 572
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->itag:I

    .line 573
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 574
    return-void
.end method

.method public onPlayerNetworkType()V
    .locals 2

    .prologue
    .line 604
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 605
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 606
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 607
    return-void
.end method

.method public onPlayerSeekingEnd(ZII)V
    .locals 2
    .param p1, "isFineScrubbing"    # Z
    .param p2, "mediaTimeMs"    # I
    .param p3, "mediaTimeAtStartOfSeekMs"    # I

    .prologue
    .line 531
    const/4 v1, 0x7

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 532
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-boolean p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->isSeekFineGrained:Z

    .line 533
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 534
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p3, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeAtStartOfSeekMillis:I

    .line 535
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 536
    return-void
.end method

.method public onPlayerSeekingStart(ZI)V
    .locals 2
    .param p1, "isFineScrubbing"    # Z
    .param p2, "mediaTimeMs"    # I

    .prologue
    .line 522
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 523
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput-boolean p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->isSeekFineGrained:Z

    .line 524
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 525
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 526
    return-void
.end method

.method public onPlayerStarted(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "sessionTimeProvider"    # Lcom/google/android/videos/player/logging/SessionTimeProvider;
    .param p2, "sessionNonce"    # Ljava/lang/String;
    .param p3, "playerType"    # I
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;
    .param p7, "isTrailer"    # Z
    .param p8, "isOffline"    # Z

    .prologue
    .line 501
    iput-object p2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionNonce:Ljava/lang/String;

    .line 502
    iput-object p1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->playbackSessionTimeProvider:Lcom/google/android/videos/player/logging/SessionTimeProvider;

    .line 503
    const/4 v2, 0x1

    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v1

    .line 504
    .local v1, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    const/4 v3, 0x0

    iput v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 505
    invoke-static {p4, p5, p6, p7}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    .line 506
    .local v0, "assetInfo":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;
    iget-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    invoke-static {p3, v0, p8}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackInfo(ILcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    move-result-object v3

    iput-object v3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->info:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlaybackInfo;

    .line 507
    iget-object v2, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v2, v1}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 508
    return-void
.end method

.method public onPlayerStateChanged(IZI)V
    .locals 3
    .param p1, "mediaTimeMs"    # I
    .param p2, "playWhenReady"    # Z
    .param p3, "playbackState"    # I

    .prologue
    .line 512
    const/4 v1, 0x3

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPlaybackEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 513
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iput p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->mediaTimeMillis:I

    .line 514
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    new-instance v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;

    invoke-direct {v2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;-><init>()V

    iput-object v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->playerState:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;

    .line 515
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->playerState:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;

    iput p3, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;->internalState:I

    .line 516
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->playbackEvent:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;

    iget-object v1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent;->playerState:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;

    iput-boolean p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlaybackEvent$PlayerState;->playWhenReady:Z

    .line 517
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 518
    return-void
.end method

.method public onPlayerSubtitleEnabled(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 592
    const/16 v1, 0xb

    invoke-direct {p0, v1, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildSubtitleEvent(ILcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 593
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 594
    return-void
.end method

.method public onPlayerSubtitleError(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 598
    const/16 v1, 0xc

    invoke-direct {p0, v1, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildSubtitleEvent(ILcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 599
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 600
    return-void
.end method

.method public onPlayerSubtitleSelected(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 2
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 586
    const/16 v1, 0xa

    invoke-direct {p0, v1, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildSubtitleEvent(ILcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 587
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 588
    return-void
.end method

.method public onPostPurchaseDialogOpened(Ljava/lang/String;Ljava/lang/String;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "referer"    # Ljava/lang/String;

    .prologue
    .line 258
    const/16 v1, 0xe

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 259
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 260
    if-eqz p2, :cond_0

    .line 261
    iput-object p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 263
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 264
    return-void
.end method

.method public onPreferenceChange(Ljava/lang/String;Ljava/lang/Object;)V
    .locals 3
    .param p1, "preference"    # Ljava/lang/String;
    .param p2, "newValue"    # Ljava/lang/Object;

    .prologue
    .line 280
    const/16 v1, 0x9

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 281
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;-><init>()V

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    .line 282
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    iput-object p1, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->name:Ljava/lang/String;

    .line 284
    instance-of v1, p2, Ljava/lang/String;

    if-eqz v1, :cond_1

    .line 285
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    const/4 v2, 0x3

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    .line 286
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    check-cast p2, Ljava/lang/String;

    .end local p2    # "newValue":Ljava/lang/Object;
    iput-object p2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->stringValue:Ljava/lang/String;

    .line 295
    :cond_0
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 296
    return-void

    .line 287
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_1
    instance-of v1, p2, Ljava/lang/Integer;

    if-eqz v1, :cond_2

    .line 288
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    const/4 v2, 0x2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    .line 289
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    check-cast p2, Ljava/lang/Integer;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Integer;->intValue()I

    move-result v2

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->intValue:I

    goto :goto_0

    .line 290
    .restart local p2    # "newValue":Ljava/lang/Object;
    :cond_2
    instance-of v1, p2, Ljava/lang/Boolean;

    if-eqz v1, :cond_0

    .line 291
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    const/4 v2, 0x1

    iput v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->type:I

    .line 292
    iget-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->preferenceChange:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;

    check-cast p2, Ljava/lang/Boolean;

    .end local p2    # "newValue":Ljava/lang/Object;
    invoke-virtual {p2}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v2

    iput-boolean v2, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Preference;->boolValue:Z

    goto :goto_0
.end method

.method public onPremiumWatchPageOpened(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;
    .param p3, "showId"    # Ljava/lang/String;
    .param p4, "isTrailer"    # Z

    .prologue
    .line 245
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 246
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1, p3, p2, p4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 247
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 248
    return-void
.end method

.method public onRemoveItemDialogShown(Ljava/lang/String;)V
    .locals 2
    .param p1, "assetId"    # Ljava/lang/String;

    .prologue
    .line 315
    const/16 v1, 0xc

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 316
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 317
    const/16 v1, 0x10

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 318
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 319
    return-void
.end method

.method public onSettingsPageOpened()V
    .locals 2

    .prologue
    .line 268
    const/16 v1, 0x8

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 269
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 270
    return-void
.end method

.method public onShowDownloadDialog(Z)V
    .locals 2
    .param p1, "completed"    # Z

    .prologue
    .line 333
    if-eqz p1, :cond_0

    const/16 v1, 0x1d

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 336
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 337
    return-void

    .line 333
    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    :cond_0
    const/16 v1, 0x1e

    goto :goto_0
.end method

.method public onShowDownloadErrorDialog(ILjava/lang/Long;Ljava/lang/Integer;)V
    .locals 6
    .param p1, "failedReason"    # I
    .param p2, "downloadSize"    # Ljava/lang/Long;
    .param p3, "drmErrorCode"    # Ljava/lang/Integer;

    .prologue
    .line 349
    const/16 v1, 0x22

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 350
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v3, 0x2

    if-eqz p3, :cond_0

    invoke-virtual {p3}, Ljava/lang/Integer;->intValue()I

    move-result v1

    move v2, v1

    :goto_0
    if-eqz p2, :cond_1

    invoke-virtual {p2}, Ljava/lang/Long;->longValue()J

    move-result-wide v4

    long-to-int v1, v4

    :goto_1
    const/4 v4, 0x0

    invoke-static {v3, v2, v1, v4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildError(IIILjava/lang/Throwable;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->error:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$ApplicationError;

    .line 353
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 354
    return-void

    .line 350
    :cond_0
    const/4 v1, 0x0

    move v2, v1

    goto :goto_0

    :cond_1
    const/4 v1, -0x1

    goto :goto_1
.end method

.method public onShowPageOpened(Ljava/lang/String;Ljava/lang/String;)V
    .locals 3
    .param p1, "showId"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 237
    const/4 v1, 0x6

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 238
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v1, 0x0

    const/4 v2, 0x0

    invoke-static {v1, p1, p2, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 239
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 240
    return-void
.end method

.method public onSuggestionsError(I)V
    .locals 2
    .param p1, "errorType"    # I

    .prologue
    .line 364
    const/4 v1, 0x7

    invoke-direct {p0, v1, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildErrorEvent(II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 365
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 366
    return-void
.end method

.method public onTransfersPing([II)V
    .locals 2
    .param p1, "pingValues"    # [I
    .param p2, "pendingFlags"    # I

    .prologue
    .line 142
    const/16 v1, 0x1a

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPagelessEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 143
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    const/4 v1, 0x0

    invoke-static {p1, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pinningTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 144
    const/4 v1, 0x2

    invoke-static {p1, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseRefreshTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 145
    const/4 v1, 0x1

    invoke-static {p1, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->licenseReleaseTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 146
    const/4 v1, 0x4

    invoke-static {p1, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateWishlistTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 147
    const/4 v1, 0x3

    invoke-static {p1, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildBackgroundTaskStats([II)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->updateLastPlaybackTasks:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$BackgroundTaskCounts;

    .line 149
    iput p2, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->pendingFlags:I

    .line 150
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 151
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 152
    return-void
.end method

.method public onUnpinVideo(Ljava/lang/String;Z)V
    .locals 2
    .param p1, "assetId"    # Ljava/lang/String;
    .param p2, "invokedFromContextMenu"    # Z

    .prologue
    .line 323
    const/16 v1, 0xd

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 324
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 325
    if-eqz p2, :cond_0

    .line 326
    const/16 v1, 0x10

    iput v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 328
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 329
    return-void
.end method

.method public onVerticalOpened(I)V
    .locals 2
    .param p1, "vertical"    # I

    .prologue
    .line 252
    invoke-static {p1}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->verticalToPage(I)I

    move-result v1

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 253
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 254
    return-void
.end method

.method public onVideosStart(Landroid/app/Activity;ILjava/lang/String;)V
    .locals 5
    .param p1, "context"    # Landroid/app/Activity;
    .param p2, "premiumStatus"    # I
    .param p3, "referer"    # Ljava/lang/String;

    .prologue
    .line 84
    new-instance v3, Landroid/util/DisplayMetrics;

    invoke-direct {v3}, Landroid/util/DisplayMetrics;-><init>()V

    .line 85
    .local v3, "metrics":Landroid/util/DisplayMetrics;
    invoke-virtual {p1}, Landroid/app/Activity;->getWindowManager()Landroid/view/WindowManager;

    move-result-object v4

    invoke-interface {v4}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v0

    .line 86
    .local v0, "defaultDisplay":Landroid/view/Display;
    invoke-virtual {v0, v3}, Landroid/view/Display;->getMetrics(Landroid/util/DisplayMetrics;)V

    .line 88
    new-instance v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    invoke-direct {v1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;-><init>()V

    .line 89
    .local v1, "display":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;
    iget v4, v3, Landroid/util/DisplayMetrics;->widthPixels:I

    iput v4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->width:I

    .line 90
    iget v4, v3, Landroid/util/DisplayMetrics;->heightPixels:I

    iput v4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->height:I

    .line 91
    iget v4, v3, Landroid/util/DisplayMetrics;->densityDpi:I

    iput v4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->densityDpi:I

    .line 92
    invoke-virtual {v0}, Landroid/view/Display;->getRotation()I

    move-result v4

    iput v4, v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;->orientation:I

    .line 94
    const/4 v4, 0x1

    invoke-direct {p0, v4}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageOpenEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v2

    .line 95
    .local v2, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-direct {p0, v2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addNetworkInfo(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 96
    invoke-static {p2}, Lcom/google/android/videos/logging/DefaultEventLogger$Values;->premiumStatusToProto(I)I

    move-result v4

    iput v4, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->premiumStatus:I

    .line 97
    if-eqz p3, :cond_0

    .line 98
    iput-object p3, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->referer:Ljava/lang/String;

    .line 100
    :cond_0
    iput-object v1, v2, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->display:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$Display;

    .line 101
    iget-object v4, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v4, v2}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 102
    return-void
.end method

.method public onWebSearch(II)V
    .locals 2
    .param p1, "eventSource"    # I
    .param p2, "result"    # I

    .prologue
    .line 723
    const/16 v1, 0x14

    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 724
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 725
    invoke-static {v0, p2}, Lcom/google/android/videos/logging/DefaultEventLogger;->addResult(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;I)V

    .line 726
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 727
    return-void
.end method

.method public onWishlistAction(Ljava/lang/String;IZI)V
    .locals 2
    .param p1, "itemId"    # Ljava/lang/String;
    .param p2, "itemType"    # I
    .param p3, "isAdd"    # Z
    .param p4, "eventSource"    # I

    .prologue
    .line 221
    if-eqz p3, :cond_0

    const/16 v1, 0x16

    :goto_0
    invoke-direct {p0, v1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildPageEvent(I)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    move-result-object v0

    .line 223
    .local v0, "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    invoke-static {p2, p1}, Lcom/google/android/videos/logging/DefaultEventLogger;->buildAssetInfo(ILjava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v1

    iput-object v1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->assetInfo:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    .line 224
    iput p4, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->eventSource:I

    .line 225
    iget-object v1, p0, Lcom/google/android/videos/logging/DefaultEventLogger;->analyticsClient:Lcom/google/android/videos/logging/AnalyticsClient;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/AnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V

    .line 226
    return-void

    .line 221
    .end local v0    # "event":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    :cond_0
    const/16 v1, 0x17

    goto :goto_0
.end method
