.class public Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;
.super Ljava/lang/Object;
.source "SubtitleWindowTextTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final appends:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Boolean;",
            ">;"
        }
    .end annotation
.end field

.field private final endTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field

.field private final lines:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final startTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 118
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    .line 120
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    .line 121
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->appends:Ljava/util/List;

    .line 122
    return-void
.end method


# virtual methods
.method public addLine(Ljava/lang/String;IIZ)Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;
    .locals 2
    .param p1, "text"    # Ljava/lang/String;
    .param p2, "startTimeMillis"    # I
    .param p3, "endTimeMillis"    # I
    .param p4, "append"    # Z

    .prologue
    .line 125
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p2, v0, :cond_0

    .line 126
    const-string v0, "subtitles are not given in non-decreasing start time order"

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 128
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 129
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 130
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    invoke-interface {v0, p1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 131
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->appends:Ljava/util/List;

    invoke-static {p4}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 132
    return-object p0
.end method

.method public build()Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;
    .locals 6

    .prologue
    .line 136
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->endTimes:Ljava/util/List;

    iget-object v3, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->lines:Ljava/util/List;

    iget-object v4, p0, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$Builder;->appends:Ljava/util/List;

    const/4 v5, 0x0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;-><init>(Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline$1;)V

    return-object v0
.end method
