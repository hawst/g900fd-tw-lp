.class public Lcom/google/android/videos/player/LocalPlaybackHelper;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Landroid/media/AudioManager$OnAudioFocusChangeListener;
.implements Landroid/view/View$OnTouchListener;
.implements Lcom/google/android/videos/async/Callback;
.implements Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;
.implements Lcom/google/android/videos/player/PlaybackHelper;
.implements Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;
.implements Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;
.implements Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
.implements Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;,
        Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;,
        Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;,
        Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;,
        Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Landroid/media/AudioManager$OnAudioFocusChangeListener;",
        "Landroid/view/View$OnTouchListener;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        "Lcom/google/android/videos/subtitles/Subtitles;",
        ">;",
        "Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;",
        "Lcom/google/android/videos/player/PlaybackHelper;",
        "Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;",
        "Lcom/google/android/videos/player/legacy/LegacyPlayer$Listener;",
        "Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;",
        "Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final activity:Landroid/app/Activity;

.field private final adaptive:Z

.field private final adaptiveDisableHdOnMobileNetwork:Z

.field private final captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

.field private final config:Lcom/google/android/videos/Config;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

.field private final display:Landroid/view/Display;

.field private displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

.field private final displaySupportsProtectedBuffers:Z

.field private final enableVirtualizer:Z

.field private final errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

.field private final immersionHelper:Lcom/google/android/videos/utils/ImmersionHelper;

.field private initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

.field private final internalBroadcastReceiver:Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

.field private final isTrailer:Z

.field private final isTv:Z

.field private final lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

.field private final listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

.field private localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

.field private localResumeTimeMillis:I

.field private localSubtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private final mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingLocalShortClockActivation:Z

.field private playWhenScrubbingEnds:Z

.field private final playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

.field private final playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

.field private final playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

.field private player:Lcom/google/android/videos/player/VideosPlayer;

.field private playerView:Lcom/google/android/videos/player/PlayerView;

.field private final positionUpdateRunnable:Ljava/lang/Runnable;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

.field private remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

.field private remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

.field private scrubbing:Z

.field private final seasonId:Ljava/lang/String;

.field private selectedAudioTrackLanguage:Ljava/lang/String;

.field private selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

.field private final showId:Ljava/lang/String;

.field private state:I

.field private final streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

.field private subtitleTracks:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation
.end field

.field private final subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

.field private final surroundSound:Z

.field private final uiHandler:Landroid/os/Handler;

.field private final videoId:Ljava/lang/String;

.field private videoInfo:Lcom/google/android/videos/player/VideoInfo;

.field private final videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZLjava/lang/String;Lcom/google/android/videos/utils/RetryAction;Landroid/media/MediaRouter$RouteInfo;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V
    .locals 18
    .param p1, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;
    .param p2, "activity"    # Landroid/app/Activity;
    .param p3, "listener"    # Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;
    .param p4, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .param p5, "videoId"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "seasonId"    # Ljava/lang/String;
    .param p8, "isTrailer"    # Z
    .param p9, "account"    # Ljava/lang/String;
    .param p10, "directorRetryAction"    # Lcom/google/android/videos/utils/RetryAction;
    .param p11, "displayRoute"    # Landroid/media/MediaRouter$RouteInfo;
    .param p12, "preparationLogger"    # Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;
        }
    .end annotation

    .prologue
    .line 202
    invoke-direct/range {p0 .. p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/VideosGlobals;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 204
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/app/Activity;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    .line 205
    invoke-static/range {p3 .. p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    .line 206
    invoke-static/range {p4 .. p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/player/PlaybackResumeState;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    .line 207
    invoke-static/range {p5 .. p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    .line 208
    move-object/from16 v0, p6

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    .line 209
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->seasonId:Ljava/lang/String;

    .line 210
    move/from16 v0, p8

    move-object/from16 v1, p0

    iput-boolean v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTrailer:Z

    .line 211
    if-eqz p8, :cond_1

    move-object/from16 v3, p9

    :goto_0
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    .line 212
    invoke-static/range {p10 .. p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/utils/RetryAction;

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 213
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getSubtitlesClient()Lcom/google/android/videos/store/SubtitlesClient;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    .line 214
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    .line 215
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getPlaybackStatusNotifier()Lcom/google/android/videos/player/PlaybackStatusNotifier;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    .line 216
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    .line 217
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 218
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    .line 219
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/videos/store/Database;

    .line 220
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getLocalExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    .line 221
    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getCaptionPreferences()Lcom/google/android/videos/player/CaptionPreferences;

    move-result-object v3

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    .line 222
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    .line 223
    new-instance v3, Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual/range {p0 .. p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v4

    move-object/from16 v0, p2

    invoke-direct {v3, v0, v4}, Landroid/support/v4/media/session/MediaSessionCompat;-><init>(Landroid/content/Context;Ljava/lang/String;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    .line 224
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    const/4 v4, 0x3

    invoke-virtual {v3, v4}, Landroid/support/v4/media/session/MediaSessionCompat;->setFlags(I)V

    .line 226
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    new-instance v4, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;

    const/4 v5, 0x0

    move-object/from16 v0, p0

    invoke-direct {v4, v0, v5}, Lcom/google/android/videos/player/LocalPlaybackHelper$MediaSessionEventCallback;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Lcom/google/android/videos/player/LocalPlaybackHelper$1;)V

    invoke-virtual {v3, v4}, Landroid/support/v4/media/session/MediaSessionCompat;->setCallback(Landroid/support/v4/media/session/MediaSessionCompat$Callback;)V

    .line 227
    sget v3, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v4, 0x11

    if-lt v3, v4, :cond_3

    .line 228
    if-nez p11, :cond_2

    const/4 v3, 0x0

    :goto_1
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    .line 229
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-static {v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->getDisplaySupportsProtectedBuffersV17(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displaySupportsProtectedBuffers:Z

    .line 234
    :goto_2
    invoke-virtual/range {p2 .. p2}, Landroid/app/Activity;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/utils/Util;->isTv(Landroid/content/pm/PackageManager;)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTv:Z

    .line 235
    new-instance v3, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v4

    invoke-direct {v3, v4}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    .line 237
    new-instance v16, Lcom/google/android/videos/player/logging/LogcatClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->playbackDebugLoggingEnabled()Z

    move-result v3

    move-object/from16 v0, v16

    invoke-direct {v0, v3}, Lcom/google/android/videos/player/logging/LogcatClient;-><init>(Z)V

    .line 238
    .local v16, "logcatClient":Lcom/google/android/videos/player/logging/LogcatClient;
    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 239
    new-instance v3, Lcom/google/android/videos/player/logging/PlaybackLogger;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    const/4 v5, 0x1

    new-array v5, v5, [Lcom/google/android/videos/player/logging/LoggingClient;

    const/4 v6, 0x0

    aput-object v16, v5, v6

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;-><init>(Lcom/google/android/videos/Config;Landroid/content/Context;[Lcom/google/android/videos/player/logging/LoggingClient;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    .line 258
    :goto_3
    new-instance v3, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/videos/player/LocalPlaybackHelper$PositionUpdateRunnable;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Lcom/google/android/videos/player/LocalPlaybackHelper$1;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->positionUpdateRunnable:Ljava/lang/Runnable;

    .line 259
    new-instance v3, Lcom/google/android/videos/utils/ImmersionHelper;

    move-object/from16 v0, p2

    invoke-direct {v3, v0}, Lcom/google/android/videos/utils/ImmersionHelper;-><init>(Landroid/app/Activity;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->immersionHelper:Lcom/google/android/videos/utils/ImmersionHelper;

    .line 260
    if-nez p8, :cond_0

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->soundWelcomeVideoId()Ljava/lang/String;

    move-result-object v3

    move-object/from16 v0, p5

    invoke-virtual {v0, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_7

    :cond_0
    const/4 v3, 0x0

    :goto_4
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    .line 263
    new-instance v3, Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    const/4 v4, 0x0

    move-object/from16 v0, p0

    invoke-direct {v3, v0, v4}, Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Lcom/google/android/videos/player/LocalPlaybackHelper$1;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    .line 264
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 265
    new-instance v9, Lcom/google/android/videos/pinning/LastPlaybackSaver;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/videos/store/Database;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    move-object/from16 v10, p2

    move-object/from16 v11, p9

    move/from16 v12, p8

    invoke-direct/range {v9 .. v14}, Lcom/google/android/videos/pinning/LastPlaybackSaver;-><init>(Landroid/content/Context;Ljava/lang/String;ZLcom/google/android/videos/store/Database;Ljava/util/concurrent/Executor;)V

    move-object/from16 v0, p0

    iput-object v9, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    .line 268
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->supportsAdaptivePlayback()Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->adaptive:Z

    .line 269
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "adaptive_disable_hd_on_mobile_network"

    const/4 v5, 0x0

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->adaptiveDisableHdOnMobileNetwork:Z

    .line 271
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    const-string v4, "enable_surround_sound"

    const/4 v5, 0x1

    invoke-interface {v3, v4, v5}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v3

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->surroundSound:Z

    .line 272
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->surroundSound:Z

    if-eqz v3, :cond_8

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->audioVirtualizerEnabled()Z

    move-result v3

    if-eqz v3, :cond_8

    const/4 v3, 0x1

    :goto_5
    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->enableVirtualizer:Z

    .line 273
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-nez v3, :cond_9

    const-string v3, "window"

    move-object/from16 v0, p2

    invoke-virtual {v0, v3}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/view/WindowManager;

    invoke-interface {v3}, Landroid/view/WindowManager;->getDefaultDisplay()Landroid/view/Display;

    move-result-object v3

    :goto_6
    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->display:Landroid/view/Display;

    .line 276
    return-void

    .line 211
    .end local v16    # "logcatClient":Lcom/google/android/videos/player/logging/LogcatClient;
    :cond_1
    invoke-static/range {p9 .. p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Ljava/lang/String;

    goto/16 :goto_0

    .line 228
    :cond_2
    new-instance v3, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    move-object/from16 v0, p0

    move-object/from16 v1, p11

    invoke-direct {v3, v0, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;Landroid/media/MediaRouter$RouteInfo;)V

    goto/16 :goto_1

    .line 231
    :cond_3
    const/4 v3, 0x0

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    .line 232
    const/4 v3, 0x1

    move-object/from16 v0, p0

    iput-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displaySupportsProtectedBuffers:Z

    goto/16 :goto_2

    .line 241
    .restart local v16    # "logcatClient":Lcom/google/android/videos/player/logging/LogcatClient;
    :cond_4
    move-object/from16 v0, p0

    iget-boolean v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTv:Z

    if-eqz v3, :cond_5

    const/4 v7, 0x2

    .line 245
    .local v7, "deviceType":I
    :goto_7
    new-instance v2, Lcom/google/android/videos/player/logging/VssClient;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getYouTubeStatsPingSender()Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    move-result-object v3

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v4}, Lcom/google/android/videos/Config;->youtubeStatsUri()Landroid/net/Uri;

    move-result-object v4

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersion()Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p2 .. p2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v6

    invoke-direct/range {v2 .. v7}, Lcom/google/android/videos/player/logging/VssClient;-><init>(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;I)V

    .line 248
    .local v2, "vssLoggingClient":Lcom/google/android/videos/player/logging/VssClient;
    new-instance v8, Lcom/google/android/videos/player/logging/QoeClient;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getYouTubeStatsPingSender()Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;

    move-result-object v9

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->youtubeStatsUri()Landroid/net/Uri;

    move-result-object v10

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getApplicationVersion()Ljava/lang/String;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Landroid/app/Activity;->getPackageName()Ljava/lang/String;

    move-result-object v12

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->getExperimentId()Ljava/lang/String;

    move-result-object v14

    move v13, v7

    invoke-direct/range {v8 .. v14}, Lcom/google/android/videos/player/logging/QoeClient;-><init>(Lcom/google/android/videos/player/logging/YouTubeStatsPingSender;Landroid/net/Uri;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    .line 251
    .local v8, "qoeLoggingClient":Lcom/google/android/videos/player/logging/QoeClient;
    new-instance v17, Lcom/google/android/videos/player/logging/PlayLogClient;

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v3

    move-object/from16 v0, v17

    invoke-direct {v0, v3}, Lcom/google/android/videos/player/logging/PlayLogClient;-><init>(Lcom/google/android/videos/logging/EventLogger;)V

    .line 252
    .local v17, "videosLoggingClient":Lcom/google/android/videos/player/logging/PlayLogClient;
    new-instance v15, Lcom/google/android/videos/player/logging/HerrevadClient;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v3}, Lcom/google/android/videos/Config;->minIntervalBetweenHerrevadReportSeconds()I

    move-result v3

    move-object/from16 v0, p2

    invoke-direct {v15, v0, v3}, Lcom/google/android/videos/player/logging/HerrevadClient;-><init>(Landroid/content/Context;I)V

    .line 254
    .local v15, "herrevadClient":Lcom/google/android/videos/player/logging/HerrevadClient;
    new-instance v3, Lcom/google/android/videos/player/logging/PlaybackLogger;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    const/4 v5, 0x5

    new-array v5, v5, [Lcom/google/android/videos/player/logging/LoggingClient;

    const/4 v6, 0x0

    aput-object v16, v5, v6

    const/4 v6, 0x1

    aput-object v2, v5, v6

    const/4 v6, 0x2

    aput-object v8, v5, v6

    const/4 v6, 0x3

    aput-object v17, v5, v6

    const/4 v6, 0x4

    aput-object v15, v5, v6

    move-object/from16 v0, p2

    invoke-direct {v3, v4, v0, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;-><init>(Lcom/google/android/videos/Config;Landroid/content/Context;[Lcom/google/android/videos/player/logging/LoggingClient;)V

    move-object/from16 v0, p0

    iput-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    goto/16 :goto_3

    .line 241
    .end local v2    # "vssLoggingClient":Lcom/google/android/videos/player/logging/VssClient;
    .end local v7    # "deviceType":I
    .end local v8    # "qoeLoggingClient":Lcom/google/android/videos/player/logging/QoeClient;
    .end local v15    # "herrevadClient":Lcom/google/android/videos/player/logging/HerrevadClient;
    .end local v17    # "videosLoggingClient":Lcom/google/android/videos/player/logging/PlayLogClient;
    :cond_5
    invoke-static/range {p2 .. p2}, Lcom/google/android/videos/utils/DisplayUtil;->isTablet(Landroid/content/Context;)Z

    move-result v3

    if-eqz v3, :cond_6

    const/4 v7, 0x1

    goto :goto_7

    :cond_6
    const/4 v7, 0x0

    goto/16 :goto_7

    .line 260
    :cond_7
    new-instance v3, Lcom/google/android/videos/ui/StreamingWarningHelper;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v0, p2

    move-object/from16 v1, p0

    invoke-direct {v3, v0, v4, v1, v5}, Lcom/google/android/videos/ui/StreamingWarningHelper;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Lcom/google/android/videos/ui/StreamingWarningHelper$Listener;Lcom/google/android/videos/utils/NetworkStatus;)V

    goto/16 :goto_4

    .line 272
    :cond_8
    const/4 v3, 0x0

    goto/16 :goto_5

    .line 273
    :cond_9
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->display:Landroid/view/Display;
    invoke-static {v3}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->access$300(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Landroid/view/Display;

    move-result-object v3

    goto/16 :goto_6
.end method

.method static synthetic access$1000(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/VideosPlayer;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    return-object v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/player/LocalPlaybackHelper;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$500(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/store/Database;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/videos/store/Database;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$700(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    return-object v0
.end method

.method static synthetic access$800(Lcom/google/android/videos/player/LocalPlaybackHelper;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    return v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/player/LocalPlaybackHelper;

    .prologue
    .line 121
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    return-object v0
.end method

.method private appendYouTubeSessionNonce(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)Lcom/google/android/videos/streams/MediaStream;
    .locals 4
    .param p1, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p2, "sessionNonce"    # Ljava/lang/String;

    .prologue
    .line 532
    new-instance v0, Lcom/google/android/videos/streams/MediaStream;

    iget-object v1, p1, Lcom/google/android/videos/streams/MediaStream;->uri:Landroid/net/Uri;

    invoke-static {v1, p2}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->appendSessionNonce(Landroid/net/Uri;Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget-object v3, p1, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/streams/MediaStream;-><init>(Landroid/net/Uri;Lcom/google/android/videos/streams/ItagInfo;Lcom/google/android/videos/proto/StreamInfo;)V

    return-object v0
.end method

.method private getAudioManager()Landroid/media/AudioManager;
    .locals 2

    .prologue
    .line 1298
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const-string v1, "audio"

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/media/AudioManager;

    return-object v0
.end method

.method private static getDisplaySupportsProtectedBuffersV17(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Z
    .locals 3
    .param p0, "displayRouteHolder"    # Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    .prologue
    const/4 v1, 0x1

    .line 312
    if-eqz p0, :cond_0

    .line 313
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->getDisplayFlags()I

    move-result v0

    .line 314
    .local v0, "displayFlags":I
    and-int/lit8 v2, v0, 0x1

    if-ne v2, v1, :cond_1

    .line 317
    .end local v0    # "displayFlags":I
    :cond_0
    :goto_0
    return v1

    .line 314
    .restart local v0    # "displayFlags":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private getDisplayTypeV17()I
    .locals 2

    .prologue
    .line 332
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->displayRoute:Landroid/media/MediaRouter$RouteInfo;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->access$400(Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;)Landroid/media/MediaRouter$RouteInfo;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/String;->valueOf(Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 333
    .local v0, "displayRouteInfo":Ljava/lang/String;
    const-string v1, "HDMI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 334
    const/4 v1, 0x2

    .line 338
    :goto_0
    return v1

    .line 335
    :cond_0
    const-string v1, "WIFI"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 336
    const/4 v1, 0x3

    goto :goto_0

    .line 338
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method private initSessionMetadata(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
    .locals 4
    .param p1, "initData"    # Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .prologue
    .line 523
    new-instance v0, Landroid/support/v4/media/MediaMetadataCompat$Builder;

    invoke-direct {v0}, Landroid/support/v4/media/MediaMetadataCompat$Builder;-><init>()V

    .line 524
    .local v0, "bob":Landroid/support/v4/media/MediaMetadataCompat$Builder;
    const-string v1, "android.media.metadata.DURATION"

    iget v2, p1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    int-to-long v2, v2

    invoke-virtual {v0, v1, v2, v3}, Landroid/support/v4/media/MediaMetadataCompat$Builder;->putLong(Ljava/lang/String;J)Landroid/support/v4/media/MediaMetadataCompat$Builder;

    .line 525
    const-string v1, "android.media.metadata.TITLE"

    iget-object v2, p1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/media/MediaMetadataCompat$Builder;

    .line 526
    const-string v1, "android.media.metadata.DISPLAY_TITLE"

    iget-object v2, p1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    invoke-virtual {v0, v1, v2}, Landroid/support/v4/media/MediaMetadataCompat$Builder;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/support/v4/media/MediaMetadataCompat$Builder;

    .line 528
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual {v0}, Landroid/support/v4/media/MediaMetadataCompat$Builder;->build()Landroid/support/v4/media/MediaMetadataCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/media/session/MediaSessionCompat;->setMetadata(Landroid/support/v4/media/MediaMetadataCompat;)V

    .line 529
    return-void
.end method

.method private isTouchExplorationEnabled()Z
    .locals 3

    .prologue
    .line 1292
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const-string v2, "accessibility"

    invoke-virtual {v1, v2}, Landroid/app/Activity;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/AccessibilityManager;

    .line 1294
    .local v0, "accessibilityManager":Landroid/view/accessibility/AccessibilityManager;
    invoke-virtual {v0}, Landroid/view/accessibility/AccessibilityManager;->isTouchExplorationEnabled()Z

    move-result v1

    return v1
.end method

.method private maybeCreateExoVideosPlayer()Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .locals 13

    .prologue
    .line 298
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-eqz v1, :cond_0

    .line 299
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    check-cast v1, Lcom/google/android/videos/player/exo/ExoVideosPlayer;

    .line 306
    :goto_0
    return-object v1

    .line 301
    :cond_0
    new-instance v0, Lcom/google/android/videos/player/exo/ExoVideosPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->display:Landroid/view/Display;

    iget-boolean v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->surroundSound:Z

    iget-boolean v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->enableVirtualizer:Z

    iget-boolean v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->adaptive:Z

    iget-boolean v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->adaptiveDisableHdOnMobileNetwork:Z

    iget-boolean v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displaySupportsProtectedBuffers:Z

    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v9}, Lcom/google/android/videos/VideosGlobals;->getDashStreamsSelector()Lcom/google/android/videos/streams/DashStreamsSelector;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-object v12, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    move-object v10, p0

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;-><init>(Lcom/google/android/videos/VideosGlobals;Landroid/app/Activity;Landroid/view/Display;ZZZZZLcom/google/android/videos/streams/DashStreamsSelector;Lcom/google/android/videos/player/exo/ExoVideosPlayer$Listener;Lcom/google/android/videos/player/PlaybackResumeState;Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;)V

    .line 305
    .local v0, "player":Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    move-object v1, v0

    .line 306
    goto :goto_0
.end method

.method private onExoInternalRuntimeErrorV21(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/RuntimeException;)Z
    .locals 4
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/RuntimeException;

    .prologue
    .line 915
    instance-of v0, p2, Landroid/media/MediaCodec$CodecException;

    if-eqz v0, :cond_0

    .line 916
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v2, 0x19

    move-object v0, p2

    check-cast v0, Landroid/media/MediaCodec$CodecException;

    invoke-virtual {v0}, Landroid/media/MediaCodec$CodecException;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/player/PlayerUtil;->parseErrorCode(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v1, v2, v0, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 919
    const/4 v0, 0x1

    .line 921
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onExoInternalWidevineDrmErrorV21(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)Z
    .locals 4
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 1001
    instance-of v0, p2, Landroid/media/MediaDrm$MediaDrmStateException;

    if-eqz v0, :cond_0

    .line 1002
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v2, 0x18

    move-object v0, p2

    check-cast v0, Landroid/media/MediaDrm$MediaDrmStateException;

    invoke-virtual {v0}, Landroid/media/MediaDrm$MediaDrmStateException;->getDiagnosticInfo()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/player/PlayerUtil;->parseErrorCode(Ljava/lang/String;)I

    move-result v0

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v1, v2, v0, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1005
    const/4 v0, 0x1

    .line 1007
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
    .locals 3
    .param p1, "message"    # Ljava/lang/String;
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 1170
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v0

    .line 1171
    .local v0, "currentPosition":I
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFailed(I)V

    .line 1172
    iput v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    .line 1173
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 1174
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v1, :cond_0

    .line 1175
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    .line 1177
    :cond_0
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 1178
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setStreamingInProgress(Z)V

    .line 1179
    return-void
.end method

.method private onPlayerPause(I)V
    .locals 4
    .param p1, "frameTimestampMs"    # I

    .prologue
    .line 729
    iget-boolean v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->scrubbing:Z

    if-nez v3, :cond_0

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v3, :cond_1

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/StreamingWarningHelper;->isShowingDialog()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 736
    :cond_0
    :goto_0
    return-void

    .line 732
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v3}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getPlayerView()Lcom/google/android/videos/player/PlayerView;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v0

    .line 733
    .local v0, "playerSurface":Lcom/google/android/videos/player/PlayerSurface;
    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->getVideoDisplayWidth()I

    move-result v2

    .line 734
    .local v2, "videoDisplayWidth":I
    invoke-interface {v0}, Lcom/google/android/videos/player/PlayerSurface;->getVideoDisplayHeight()I

    move-result v1

    .line 735
    .local v1, "videoDisplayHeight":I
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v3, p1, v2, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onPlayerPause(III)V

    goto :goto_0
.end method

.method private onPlayerStateChanged(ZI)V
    .locals 6
    .param p1, "playWhenReady"    # Z
    .param p2, "playerState"    # I

    .prologue
    const/4 v4, 0x3

    const/4 v2, 0x1

    const/4 v3, 0x0

    .line 739
    const/4 v0, 0x0

    .line 740
    .local v0, "remotePlayerState":I
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->immersionHelper:Lcom/google/android/videos/utils/ImmersionHelper;

    if-eqz p1, :cond_2

    if-eq p2, v2, :cond_2

    const/4 v1, 0x5

    if-eq p2, v1, :cond_2

    move v1, v2

    :goto_0
    invoke-virtual {v5, v1}, Lcom/google/android/videos/utils/ImmersionHelper;->setImmersive(Z)V

    .line 742
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v5}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v1, p1, p2, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onStateChanged(ZII)V

    .line 743
    packed-switch p2, :pswitch_data_0

    .line 781
    :cond_0
    :goto_1
    packed-switch v0, :pswitch_data_1

    .line 790
    :goto_2
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    if-eqz v1, :cond_1

    .line 791
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->setPlayerState(I)V

    .line 793
    :cond_1
    return-void

    :cond_2
    move v1, v3

    .line 740
    goto :goto_0

    .line 745
    :pswitch_1
    iget v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    const/4 v2, 0x2

    if-ne v1, v2, :cond_3

    const/4 v0, 0x4

    .line 746
    :goto_3
    goto :goto_1

    :cond_3
    move v0, v3

    .line 745
    goto :goto_3

    .line 748
    :pswitch_2
    if-eqz p1, :cond_4

    move v0, v2

    .line 749
    :goto_4
    goto :goto_1

    :cond_4
    move v0, v4

    .line 748
    goto :goto_4

    .line 751
    :pswitch_3
    if-eqz p1, :cond_5

    move v0, v2

    .line 752
    :goto_5
    invoke-virtual {p0, p1, v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setState(ZZ)V

    goto :goto_1

    :cond_5
    move v0, v4

    .line 751
    goto :goto_5

    .line 755
    :pswitch_4
    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->maybeSchedulePositionUpdate(I)V

    .line 756
    invoke-virtual {p0, p1, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setState(ZZ)V

    .line 757
    if-eqz p1, :cond_6

    .line 758
    const/4 v0, 0x2

    .line 759
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    invoke-virtual {v1}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->onPlaying()V

    .line 760
    iget-boolean v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    if-eqz v1, :cond_0

    .line 761
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->persistLocalShortClockActivation()V

    .line 762
    iput-boolean v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    goto :goto_1

    .line 765
    :cond_6
    const/4 v0, 0x3

    .line 766
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    invoke-virtual {v1}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->onPaused()V

    .line 767
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v1, :cond_0

    .line 768
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    goto :goto_1

    .line 773
    :pswitch_5
    const/4 v0, 0x5

    .line 774
    iput v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 775
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v1, :cond_7

    .line 776
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/StreamingWarningHelper;->onPlaybackStopped()V

    .line 778
    :cond_7
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v1}, Landroid/app/Activity;->finish()V

    goto :goto_1

    .line 783
    :pswitch_6
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onPlayerPlay()V

    goto :goto_2

    .line 787
    :pswitch_7
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onPlayerStop()V

    goto :goto_2

    .line 743
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
    .end packed-switch

    .line 781
    :pswitch_data_1
    .packed-switch 0x0
        :pswitch_7
        :pswitch_0
        :pswitch_6
        :pswitch_0
        :pswitch_0
        :pswitch_7
    .end packed-switch
.end method

.method private persistLocalShortClockActivation()V
    .locals 8

    .prologue
    .line 660
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-wide v6, v3, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockMillis:J

    add-long v0, v4, v6

    .line 661
    .local v0, "expirationTimestamp":J
    new-instance v2, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    invoke-direct {v2, v3, v4}, Lcom/google/android/videos/pinning/DownloadKey;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    .line 662
    .local v2, "key":Lcom/google/android/videos/pinning/DownloadKey;
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v4, Lcom/google/android/videos/player/LocalPlaybackHelper$1;

    invoke-direct {v4, p0, v0, v1, v2}, Lcom/google/android/videos/player/LocalPlaybackHelper$1;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;JLcom/google/android/videos/pinning/DownloadKey;)V

    invoke-interface {v3, v4}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 685
    return-void
.end method

.method private playVideoInternal()V
    .locals 2

    .prologue
    const/4 v1, 0x1

    .line 569
    invoke-virtual {p0, v1, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setState(ZZ)V

    .line 570
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v0, v0, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v0, :cond_0

    .line 571
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setStreamingInProgress(Z)V

    .line 573
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->setPlayWhenReady(Z)V

    .line 574
    iget v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    if-eq v0, v1, :cond_1

    .line 575
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparePlayer()V

    .line 577
    :cond_1
    return-void
.end method

.method private preparePlayer()V
    .locals 2

    .prologue
    .line 580
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    iget v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->seekTo(I)V

    .line 581
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->prepare(Lcom/google/android/videos/player/VideoInfo;)V

    .line 582
    const/4 v0, 0x1

    iput v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 583
    return-void
.end method

.method private processTracks([Lcom/google/wireless/android/video/magma/proto/AudioInfo;ILjava/util/List;ILjava/lang/String;Z)V
    .locals 9
    .param p1, "audioTracks"    # [Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .param p2, "selectedAudioTrackIndex"    # I
    .param p4, "subtitleMode"    # I
    .param p5, "subtitleDefaultLanguage"    # Ljava/lang/String;
    .param p6, "haveAudioInDeviceLanguage"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            "I",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;I",
            "Ljava/lang/String;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 1186
    .local p3, "subtitleTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    .line 1188
    iput-object p3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->subtitleTracks:Ljava/util/List;

    .line 1190
    if-eqz p1, :cond_0

    .line 1191
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/player/PlaybackResumeState;->setSelectedAudioTrackIndex(I)V

    .line 1192
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSelectedAudioTrackIndex(I)V

    .line 1193
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-static {p1}, Ljava/util/Arrays;->asList([Ljava/lang/Object;)Ljava/util/List;

    move-result-object v1

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setAudioTracks(Ljava/util/List;)V

    .line 1195
    aget-object v7, p1, p2

    .line 1196
    .local v7, "selectedAudioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v0

    iget-object v1, v7, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->getLanguageMatchScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v0

    if-lez v0, :cond_3

    const/4 p6, 0x1

    .line 1198
    :goto_0
    iget-object v0, v7, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    .line 1200
    .end local v7    # "selectedAudioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_0
    if-eqz p3, :cond_2

    .line 1201
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    move-object v0, p3

    move v3, p4

    move-object v4, p5

    move v5, p6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->selectTrack(Ljava/util/List;Lcom/google/android/videos/player/CaptionPreferences;Landroid/content/SharedPreferences;ILjava/lang/String;ZLcom/google/android/videos/player/PlaybackResumeState;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v8

    .line 1204
    .local v8, "selectedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    if-nez v8, :cond_1

    .line 1206
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    const/4 v1, 0x1

    invoke-static {p3, v0, v1}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v8

    .line 1209
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    const/4 v0, 0x3

    if-eq p4, v0, :cond_4

    const/4 v0, 0x1

    :goto_1
    invoke-interface {v1, p3, v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSubtitles(Ljava/util/List;Z)V

    .line 1211
    const/4 v0, 0x1

    invoke-direct {p0, v8, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;Z)V

    .line 1213
    .end local v8    # "selectedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_2
    return-void

    .line 1196
    .restart local v7    # "selectedAudioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    :cond_3
    const/4 p6, 0x0

    goto :goto_0

    .line 1209
    .end local v7    # "selectedAudioInfo":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .restart local v8    # "selectedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_4
    const/4 v0, 0x0

    goto :goto_1
.end method

.method private setMediaSessionState(I)V
    .locals 4
    .param p1, "mediaSessionState"    # I

    .prologue
    .line 553
    new-instance v0, Landroid/support/v4/media/session/PlaybackStateCompat$Builder;

    invoke-direct {v0}, Landroid/support/v4/media/session/PlaybackStateCompat$Builder;-><init>()V

    .line 554
    .local v0, "builder":Landroid/support/v4/media/session/PlaybackStateCompat$Builder;
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v1

    int-to-long v2, v1

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, p1, v2, v3, v1}, Landroid/support/v4/media/session/PlaybackStateCompat$Builder;->setState(IJF)V

    .line 555
    const/4 v1, 0x3

    if-ne p1, v1, :cond_0

    const-wide/16 v2, 0x2

    :goto_0
    invoke-virtual {v0, v2, v3}, Landroid/support/v4/media/session/PlaybackStateCompat$Builder;->setActions(J)V

    .line 557
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual {v0}, Landroid/support/v4/media/session/PlaybackStateCompat$Builder;->build()Landroid/support/v4/media/session/PlaybackStateCompat;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/support/v4/media/session/MediaSessionCompat;->setPlaybackState(Landroid/support/v4/media/session/PlaybackStateCompat;)V

    .line 558
    return-void

    .line 555
    :cond_0
    const-wide/16 v2, 0x4

    goto :goto_0
.end method

.method private setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;Z)V
    .locals 3
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "logSelection"    # Z

    .prologue
    .line 1243
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-eq v1, p1, :cond_1

    .line 1244
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 1245
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, p1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 1246
    if-eqz p2, :cond_0

    .line 1247
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onSubtitleSelected(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 1250
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    const/4 v2, 0x0

    invoke-interface {v1, v2}, Lcom/google/android/videos/player/VideosPlayer;->setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V

    .line 1251
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-eqz v1, :cond_1

    .line 1253
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v1, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    const/4 v2, -0x1

    if-ne v1, v2, :cond_2

    const/4 v0, 0x0

    .line 1256
    .local v0, "storageToUse":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->subtitlesClient:Lcom/google/android/videos/store/SubtitlesClient;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-static {v2, p0}, Lcom/google/android/videos/async/ActivityCallback;->create(Landroid/app/Activity;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/ActivityCallback;

    move-result-object v2

    invoke-virtual {v1, p1, v0, v2}, Lcom/google/android/videos/store/SubtitlesClient;->requestSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V

    .line 1260
    .end local v0    # "storageToUse":I
    :cond_1
    return-void

    .line 1253
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v0, v1, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    goto :goto_0
.end method


# virtual methods
.method public getDisplayType()I
    .locals 1

    .prologue
    .line 323
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-nez v0, :cond_0

    .line 324
    const/4 v0, 0x1

    .line 326
    :goto_0
    return v0

    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->getDisplayTypeV17()I

    move-result v0

    goto :goto_0
.end method

.method public getPlayerTimeMillis()I
    .locals 1

    .prologue
    .line 1282
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-nez v0, :cond_0

    .line 1283
    const/4 v0, -0x1

    .line 1285
    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v0

    goto :goto_0
.end method

.method public init(Z)V
    .locals 11
    .param p1, "playWhenInitialized"    # Z
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotFoundException;,
            Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;
        }
    .end annotation

    .prologue
    const/4 v4, 0x1

    const/4 v5, 0x0

    .line 345
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v6, :cond_0

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v6}, Lcom/google/android/videos/Config;->forceMirrorMode()Z

    move-result v6

    if-nez v6, :cond_0

    .line 347
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-virtual {v6, v7}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->initLocalPlaybackViewHolder(Lcom/google/android/videos/tagging/KnowledgeView;)Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    .line 354
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v6, :cond_2

    .line 355
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    invoke-virtual {v6}, Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;->getDisplayFlags()I

    move-result v0

    .line 356
    .local v0, "displayFlags":I
    and-int/lit8 v6, v0, 0x2

    const/4 v7, 0x2

    if-ne v6, v7, :cond_1

    move v1, v4

    .line 357
    .local v1, "isSecure":Z
    :goto_1
    iget-boolean v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTrailer:Z

    if-nez v6, :cond_2

    if-nez v1, :cond_2

    .line 358
    new-instance v4, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;

    invoke-direct {v4}, Lcom/google/android/videos/player/PlaybackHelper$DisplayNotSecureException;-><init>()V

    throw v4

    .line 350
    .end local v0    # "displayFlags":I
    .end local v1    # "isSecure":Z
    :cond_0
    new-instance v6, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;

    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localSubtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    iget-object v10, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    invoke-direct {v6, v7, v8, v9, v10}, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;-><init>(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V

    iput-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    goto :goto_0

    .restart local v0    # "displayFlags":I
    :cond_1
    move v1, v5

    .line 356
    goto :goto_1

    .line 362
    .end local v0    # "displayFlags":I
    :cond_2
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->seasonId:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    invoke-virtual {v6, v7, v8, v9}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->init(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 364
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v6}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getSubtitlesOverlay()Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    move-result-object v2

    .line 365
    .local v2, "subtitlesOverlay":Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v6}, Lcom/google/android/videos/player/CaptionPreferences;->getFontScale()F

    move-result v6

    invoke-interface {v2, v6}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->setFontScale(F)V

    .line 366
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->captionPreferences:Lcom/google/android/videos/player/CaptionPreferences;

    invoke-virtual {v6}, Lcom/google/android/videos/player/CaptionPreferences;->getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v6

    invoke-interface {v2, v6}, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;->setCaptionStyle(Lcom/google/android/exoplayer/text/CaptionStyleCompat;)V

    .line 367
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v6}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getRemoteScreenInfoOverlay()Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    move-result-object v6

    iput-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    .line 375
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTouchExplorationEnabled()Z

    move-result v3

    .line 376
    .local v3, "touchExplorationEnabled":Z
    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-eqz v6, :cond_3

    move v6, v4

    :goto_2
    invoke-interface {v7, v6}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setUseScrubPad(Z)V

    .line 377
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v6, v3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setTouchExplorationEnabled(Z)V

    .line 378
    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    if-nez v3, :cond_4

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displayRouteHolder:Lcom/google/android/videos/player/LocalPlaybackHelper$DisplayRouteHolderV17;

    if-nez v6, :cond_4

    move v6, v4

    :goto_3
    invoke-interface {v7, v6}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHideable(Z)V

    .line 379
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v6, v5}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSupportsQualityToggle(Z)V

    .line 380
    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v6, v5}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHasKnowledge(Z)V

    .line 381
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v5}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->clearSubtitles()V

    .line 382
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v5}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->clearAudioTracks()V

    .line 383
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v5, p1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->init(Z)V

    .line 384
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual {v5, v4}, Landroid/support/v4/media/session/MediaSessionCompat;->setActive(Z)V

    .line 385
    return-void

    :cond_3
    move v6, v5

    .line 376
    goto :goto_2

    :cond_4
    move v6, v5

    .line 378
    goto :goto_3
.end method

.method public initKnowledgeViewHelper(Lcom/google/android/videos/tagging/KnowledgeViewHelper;)V
    .locals 6
    .param p1, "knowledgeViewHelper"    # Lcom/google/android/videos/tagging/KnowledgeViewHelper;

    .prologue
    .line 389
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v4}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getTaggingKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;

    move-result-object v2

    .line 390
    .local v2, "taggingKnowledgeView":Lcom/google/android/videos/tagging/KnowledgeView;
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v4}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getTaglessKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;

    move-result-object v3

    .line 392
    .local v3, "taglessKnowledgeView":Lcom/google/android/videos/tagging/KnowledgeView;
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v4}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getTaggingKnowledgeViewResources()Landroid/content/res/Resources;

    move-result-object v4

    const v5, 0x7f0e019b

    invoke-virtual {v4, v5}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 394
    .local v1, "sqrtMinTagArea":F
    mul-float v0, v1, v1

    .line 395
    .local v0, "minTagArea":F
    invoke-virtual {p1, v0, v2, v3, p0}, Lcom/google/android/videos/tagging/KnowledgeViewHelper;->init(FLcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;)V

    .line 396
    return-void
.end method

.method public load(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V
    .locals 37
    .param p1, "initData"    # Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .prologue
    .line 438
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskStart(I)V

    .line 439
    move-object/from16 v0, p1

    iget-object v2, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    iget-object v9, v2, Lcom/google/android/videos/streams/Streams;->mediaStreams:Ljava/util/List;

    .line 440
    .local v9, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v32

    check-cast v32, Lcom/google/android/videos/streams/MediaStream;

    .line 441
    .local v32, "firstStream":Lcom/google/android/videos/streams/MediaStream;
    move-object/from16 v0, v32

    iget-boolean v0, v0, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    move/from16 v18, v0

    .line 442
    .local v18, "isOffline":Z
    move-object/from16 v0, v32

    iget-object v2, v0, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v2, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    if-eqz v2, :cond_4

    const/4 v5, 0x1

    .line 443
    .local v5, "isEncrypted":Z
    :goto_0
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    .line 444
    move-object/from16 v0, p1

    iget v2, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->resumeTimeMillis:I

    move-object/from16 v0, p0

    iput v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    .line 445
    move-object/from16 v0, p1

    iget-boolean v2, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockNotActivated:Z

    if-eqz v2, :cond_5

    if-eqz v18, :cond_5

    const/4 v2, 0x1

    :goto_1
    move-object/from16 v0, p0

    iput-boolean v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->pendingLocalShortClockActivation:Z

    .line 448
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    move-object/from16 v0, p0

    iget v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    move-object/from16 v0, p1

    iget v4, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    const/4 v6, 0x0

    invoke-interface {v2, v3, v4, v6}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setTimes(III)V

    .line 449
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    if-eqz v2, :cond_0

    .line 450
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    move-object/from16 v0, p1

    iget-object v3, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setVideoTitle(Ljava/lang/String;)V

    .line 453
    :cond_0
    invoke-static {}, Lcom/google/android/videos/player/logging/YouTubeStatsHelper;->generateSessionNonce()Ljava/lang/String;

    move-result-object v11

    .line 454
    .local v11, "sessionNonce":Ljava/lang/String;
    if-eqz v18, :cond_7

    .line 455
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    if-eqz v4, :cond_6

    const/4 v4, 0x1

    :goto_2
    move-object/from16 v0, p1

    iget-object v6, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    move-object/from16 v0, p1

    iget-object v7, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->cencKeySetId:[B

    move-object/from16 v0, p1

    iget v8, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->cencSecurityLevel:I

    move-object/from16 v0, p1

    iget v10, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    invoke-static/range {v2 .. v10}, Lcom/google/android/videos/player/VideoInfo;->createOffline(Ljava/lang/String;Ljava/lang/String;ZZLcom/google/android/videos/drm/DrmManager$Identifiers;[BILjava/util/List;I)Lcom/google/android/videos/player/VideoInfo;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    .line 468
    :goto_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v2}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getPlayerView()Lcom/google/android/videos/player/PlayerView;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/player/PlayerView;->getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;

    move-result-object v35

    .line 469
    .local v35, "playerSurface":Lcom/google/android/videos/player/PlayerSurface;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v2}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->getSubtitlesOverlay()Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    move-result-object v36

    .line 472
    .local v36, "subtitlesOverlay":Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    const/4 v3, 0x1

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setSessionInProgress(Z)V

    .line 473
    const/4 v2, 0x0

    invoke-interface {v9, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget-boolean v2, v2, Lcom/google/android/videos/streams/ItagInfo;->isDash:Z

    if-eqz v2, :cond_b

    .line 474
    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->adaptive:Z

    if-eqz v2, :cond_a

    const/4 v12, 0x1

    .line 475
    .local v12, "playerType":I
    :goto_4
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-nez v2, :cond_1

    .line 476
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->maybeCreateExoVideosPlayer()Lcom/google/android/videos/player/exo/ExoVideosPlayer;

    .line 483
    :cond_1
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->seasonId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTrailer:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    move/from16 v19, v0

    move-object/from16 v20, v9

    invoke-virtual/range {v10 .. v20}, Lcom/google/android/videos/player/logging/PlaybackLogger;->startSession(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILjava/util/List;)V

    .line 495
    :goto_5
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    move-object/from16 v0, v35

    move-object/from16 v1, v36

    invoke-interface {v2, v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->setViews(Lcom/google/android/videos/player/PlayerSurface;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;)V

    .line 497
    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isFastNetwork()Z

    move-result v2

    if-eqz v2, :cond_d

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v2}, Lcom/google/android/videos/utils/NetworkStatus;->isChargeableNetwork()Z

    move-result v2

    if-nez v2, :cond_d

    const/4 v2, 0x1

    :goto_6
    invoke-virtual {v4, v2}, Lcom/google/android/videos/player/PlaybackResumeState;->getHqState(Z)Z

    move-result v2

    invoke-interface {v3, v2}, Lcom/google/android/videos/player/VideosPlayer;->setHq(Z)V

    .line 500
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v2, :cond_2

    .line 501
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/StreamingWarningHelper;->register()V

    .line 503
    :cond_2
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    invoke-virtual {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->register()V

    .line 504
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v2, v2, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v2, :cond_3

    .line 505
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparePlayer()V

    .line 507
    :cond_3
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    const/4 v3, 0x6

    invoke-virtual {v2, v3}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->recordTaskEnd(I)V

    .line 509
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preparationLogger:Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->getDisplayType()I

    move-result v13

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->seasonId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTrailer:Z

    move/from16 v17, v0

    invoke-virtual/range {v10 .. v18}, Lcom/google/android/videos/player/logging/PlaybackPreparationLogger;->addExtraInfo(Ljava/lang/String;IILjava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 511
    invoke-direct/range {p0 .. p1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->initSessionMetadata(Lcom/google/android/videos/player/DirectorInitializer$InitializationData;)V

    .line 512
    return-void

    .line 442
    .end local v5    # "isEncrypted":Z
    .end local v11    # "sessionNonce":Ljava/lang/String;
    .end local v12    # "playerType":I
    .end local v35    # "playerSurface":Lcom/google/android/videos/player/PlayerSurface;
    .end local v36    # "subtitlesOverlay":Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    :cond_4
    const/4 v5, 0x0

    goto/16 :goto_0

    .line 445
    .restart local v5    # "isEncrypted":Z
    :cond_5
    const/4 v2, 0x0

    goto/16 :goto_1

    .line 455
    .restart local v11    # "sessionNonce":Ljava/lang/String;
    :cond_6
    const/4 v4, 0x0

    goto/16 :goto_2

    .line 459
    :cond_7
    move-object/from16 v34, v9

    .line 460
    .local v34, "originalMediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    new-instance v9, Ljava/util/ArrayList;

    .end local v9    # "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    invoke-interface/range {v34 .. v34}, Ljava/util/List;->size()I

    move-result v2

    invoke-direct {v9, v2}, Ljava/util/ArrayList;-><init>(I)V

    .line 461
    .restart local v9    # "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    const/16 v33, 0x0

    .local v33, "i":I
    :goto_7
    invoke-interface/range {v34 .. v34}, Ljava/util/List;->size()I

    move-result v2

    move/from16 v0, v33

    if-ge v0, v2, :cond_8

    .line 462
    move-object/from16 v0, v34

    move/from16 v1, v33

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    move-object/from16 v0, p0

    invoke-direct {v0, v2, v11}, Lcom/google/android/videos/player/LocalPlaybackHelper;->appendYouTubeSessionNonce(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;)Lcom/google/android/videos/streams/MediaStream;

    move-result-object v2

    invoke-interface {v9, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 461
    add-int/lit8 v33, v33, 0x1

    goto :goto_7

    .line 464
    :cond_8
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v3, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v4, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    if-eqz v4, :cond_9

    const/4 v4, 0x1

    :goto_8
    move-object/from16 v0, p1

    iget v7, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    move-object v6, v9

    invoke-static/range {v2 .. v7}, Lcom/google/android/videos/player/VideoInfo;->createOnline(Ljava/lang/String;Ljava/lang/String;ZZLjava/util/List;I)Lcom/google/android/videos/player/VideoInfo;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    goto/16 :goto_3

    :cond_9
    const/4 v4, 0x0

    goto :goto_8

    .line 474
    .end local v33    # "i":I
    .end local v34    # "originalMediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    .restart local v35    # "playerSurface":Lcom/google/android/videos/player/PlayerSurface;
    .restart local v36    # "subtitlesOverlay":Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    :cond_a
    const/4 v12, 0x2

    goto/16 :goto_4

    .line 487
    :cond_b
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-nez v2, :cond_c

    const/4 v2, 0x1

    :goto_9
    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 488
    const/4 v12, 0x0

    .line 489
    .restart local v12    # "playerType":I
    new-instance v19, Lcom/google/android/videos/player/legacy/LegacyPlayer;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    move-object/from16 v20, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->preferences:Landroid/content/SharedPreferences;

    move-object/from16 v21, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->display:Landroid/view/Display;

    move-object/from16 v22, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    move-object/from16 v23, v0

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getDrmManager()Lcom/google/android/videos/drm/DrmManager;

    move-result-object v24

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getLegacyStreamsSelector()Lcom/google/android/videos/streams/LegacyStreamsSelector;

    move-result-object v25

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->surroundSound:Z

    move/from16 v26, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->enableVirtualizer:Z

    move/from16 v27, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->displaySupportsProtectedBuffers:Z

    move/from16 v28, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->config:Lcom/google/android/videos/Config;

    move-object/from16 v30, v0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    move-object/from16 v31, v0

    move-object/from16 v29, p0

    invoke-direct/range {v19 .. v31}, Lcom/google/android/videos/player/legacy/LegacyPlayer;-><init>(Landroid/app/Activity;Landroid/content/SharedPreferences;Landroid/view/Display;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/streams/LegacyStreamsSelector;ZZZLcom/google/android/videos/player/legacy/LegacyPlayer$Listener;Lcom/google/android/videos/Config;Lcom/google/android/videos/player/PlaybackResumeState;)V

    move-object/from16 v0, v19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    .line 492
    move-object/from16 v0, p0

    iget-object v10, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    move-object/from16 v0, p0

    iget-object v13, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->showId:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->seasonId:Ljava/lang/String;

    move-object/from16 v16, v0

    move-object/from16 v0, p0

    iget-boolean v0, v0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTrailer:Z

    move/from16 v17, v0

    move-object/from16 v0, p1

    iget v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    move/from16 v19, v0

    move-object/from16 v20, v9

    invoke-virtual/range {v10 .. v20}, Lcom/google/android/videos/player/logging/PlaybackLogger;->startSession(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILjava/util/List;)V

    goto/16 :goto_5

    .line 487
    .end local v12    # "playerType":I
    :cond_c
    const/4 v2, 0x0

    goto/16 :goto_9

    .line 497
    .restart local v12    # "playerType":I
    :cond_d
    const/4 v2, 0x0

    goto/16 :goto_6
.end method

.method public maybeJoinCurrentPlayback(Ljava/lang/String;)Z
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 433
    const/4 v0, 0x0

    return v0
.end method

.method maybeSchedulePositionUpdate(I)V
    .locals 6
    .param p1, "delayMillis"    # I

    .prologue
    .line 796
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->positionUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v1, v2}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 797
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getPlaybackState()I

    move-result v0

    .line 798
    .local v0, "playerState":I
    :goto_0
    const/4 v1, 0x3

    if-eq v0, v1, :cond_0

    const/4 v1, 0x4

    if-ne v0, v1, :cond_1

    .line 800
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->positionUpdateRunnable:Ljava/lang/Runnable;

    int-to-long v4, p1

    invoke-virtual {v1, v2, v4, v5}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 802
    :cond_1
    return-void

    .line 797
    .end local v0    # "playerState":I
    :cond_2
    const/4 v0, 0x1

    goto :goto_0
.end method

.method public onAudioFocusChange(I)V
    .locals 1
    .param p1, "focusChange"    # I

    .prologue
    .line 516
    const/4 v0, -0x1

    if-eq p1, v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->isTv:Z

    if-eqz v0, :cond_1

    const/4 v0, -0x2

    if-ne p1, v0, :cond_1

    .line 518
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->pause()V

    .line 520
    :cond_1
    return-void
.end method

.method public onDownloadProgress(JJI)V
    .locals 2
    .param p1, "currentFileSize"    # J
    .param p3, "totalFileSize"    # J
    .param p5, "percentDownloaded"    # I

    .prologue
    .line 690
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    new-instance v1, Lcom/google/android/videos/player/LocalPlaybackHelper$2;

    invoke-direct {v1, p0, p5}, Lcom/google/android/videos/player/LocalPlaybackHelper$2;-><init>(Lcom/google/android/videos/player/LocalPlaybackHelper;I)V

    invoke-virtual {v0, v1}, Landroid/app/Activity;->runOnUiThread(Ljava/lang/Runnable;)V

    .line 699
    return-void
.end method

.method public onError(Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 1272
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-ne p1, v0, :cond_0

    .line 1273
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;Z)V

    .line 1274
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onSubtitleError(Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V

    .line 1276
    :cond_0
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onError(Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V

    return-void
.end method

.method public onExoDroppedFrames(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "count"    # I

    .prologue
    .line 1038
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onDroppedFrames(I)V

    .line 1039
    return-void
.end method

.method public onExoHqToggled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZZ)V
    .locals 3
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "isHqHd"    # Z
    .param p3, "hq"    # Z
    .param p4, "wasAutomaticDrop"    # Z

    .prologue
    .line 809
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, p3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHq(Z)V

    .line 810
    if-nez p3, :cond_0

    if-eqz p4, :cond_0

    .line 811
    if-eqz p2, :cond_1

    const v0, 0x7f0b018e

    .line 812
    .local v0, "toastMessage":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const/4 v2, 0x1

    invoke-static {v1, v0, v2}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 814
    .end local v0    # "toastMessage":I
    :cond_0
    return-void

    .line 811
    :cond_1
    const v0, 0x7f0b018f

    goto :goto_0
.end method

.method public onExoHttpDataSourceOpened(Lcom/google/android/videos/player/exo/ExoVideosPlayer;J)V
    .locals 2
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "elapsedMs"    # J

    .prologue
    .line 1033
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onHttpDataSourceOpened(J)V

    .line 1034
    return-void
.end method

.method public onExoInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V
    .locals 5
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    const/4 v4, 0x0

    const/4 v3, 0x0

    .line 833
    instance-of v0, p2, Lcom/google/android/videos/streams/MissingStreamException;

    if-eqz v0, :cond_0

    .line 834
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v1, 0x2

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, v3, p2, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 836
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b01f4

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 837
    :cond_0
    instance-of v0, p2, Landroid/media/UnsupportedSchemeException;

    if-eqz v0, :cond_1

    .line 838
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v1, 0x1

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, v3, p2, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 839
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b017a

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 849
    :goto_0
    return-void

    .line 840
    :cond_1
    instance-of v0, p2, Lcom/google/android/videos/player/ProtectedBufferException;

    if-eqz v0, :cond_2

    .line 841
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v1, 0xd

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, v3, p2, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 843
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b0152

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0, v4}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    goto :goto_0

    .line 845
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v1, 0xf

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, v3, p2, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 846
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v1, 0x7f0b0147

    invoke-virtual {v0, v1}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    goto :goto_0
.end method

.method public onExoInitialized(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZZ)V
    .locals 7
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "hqToggleSupported"    # Z
    .param p3, "isHqHd"    # Z

    .prologue
    .line 824
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSupportsQualityToggle(Z)V

    .line 825
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p3}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHqIsHd(Z)V

    .line 826
    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getSelectedAudioTrack()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    iget-object v3, v0, Lcom/google/android/videos/streams/Streams;->captions:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v4, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleMode:I

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleDefaultLanguage:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v6, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->haveAudioInDeviceLanguage:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/player/LocalPlaybackHelper;->processTracks([Lcom/google/wireless/android/video/magma/proto/AudioInfo;ILjava/util/List;ILjava/lang/String;Z)V

    .line 829
    return-void
.end method

.method public onExoInternalAudioTrackInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;)V
    .locals 4
    .param p1, "exoVideosPlayer"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;

    .prologue
    .line 934
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v1, 0x12

    iget v2, p2, Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;->audioTrackState:I

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v3}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 936
    return-void
.end method

.method public onExoInternalBandwidthSample(Lcom/google/android/videos/player/exo/ExoVideosPlayer;IJJ)V
    .locals 7
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "elapsedMs"    # I
    .param p3, "bytes"    # J
    .param p5, "bitrateEstimate"    # J

    .prologue
    .line 1028
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    move v1, p2

    move-wide v2, p3

    move-wide v4, p5

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onBandwidthSample(IJJ)V

    .line 1029
    return-void
.end method

.method public onExoInternalConsumptionError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V
    .locals 4
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 964
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v1, 0x8

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/player/PlayerUtil;->hashErrorString(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 966
    return-void
.end method

.method public onExoInternalCryptoError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;I)V
    .locals 3
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/Exception;
    .param p3, "errorCode"    # I

    .prologue
    .line 940
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v1, 0x4

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v2

    invoke-virtual {v0, v1, p3, p2, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 942
    return-void
.end method

.method public onExoInternalDecoderInitializationError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;)V
    .locals 4
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;

    .prologue
    .line 927
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v1, 0x3

    iget-object v2, p2, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;->diagnosticInfo:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/player/PlayerUtil;->parseErrorCode(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 929
    return-void
.end method

.method public onExoInternalFormatEnabled(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 1017
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatEnabled(II)V

    .line 1018
    return-void
.end method

.method public onExoInternalFormatSelected(Lcom/google/android/videos/player/exo/ExoVideosPlayer;II)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "itag"    # I
    .param p3, "trigger"    # I

    .prologue
    .line 1012
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2, p3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatSelected(II)V

    .line 1013
    return-void
.end method

.method public onExoInternalLoadingChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Z)V
    .locals 1
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "loading"    # Z

    .prologue
    .line 1022
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onLoadingChanged(Z)V

    .line 1023
    return-void
.end method

.method public onExoInternalRuntimeError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/RuntimeException;)V
    .locals 4
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/RuntimeException;

    .prologue
    .line 904
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onExoInternalRuntimeErrorV21(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/RuntimeException;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 911
    :goto_0
    return-void

    .line 908
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v1, 0x9

    const/4 v2, 0x0

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v0, v1, v2, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    goto :goto_0
.end method

.method public onExoInternalUpstreamError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/io/IOException;)V
    .locals 6
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/io/IOException;

    .prologue
    .line 946
    instance-of v2, p2, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;

    if-eqz v2, :cond_0

    move-object v1, p2

    .line 947
    check-cast v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;

    .line 948
    .local v1, "invalidResponseCodeException":Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v3, 0x10

    iget v4, v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;->responseCode:I

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v2, v3, v4, v1, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 960
    .end local v1    # "invalidResponseCodeException":Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidResponseCodeException;
    :goto_0
    return-void

    .line 951
    :cond_0
    instance-of v2, p2, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;

    if-eqz v2, :cond_1

    move-object v0, p2

    .line 952
    check-cast v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;

    .line 953
    .local v0, "invalidContentTypeException":Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v3, 0x11

    iget-object v4, v0, Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;->contentType:Ljava/lang/String;

    invoke-static {v4}, Lcom/google/android/videos/player/PlayerUtil;->hashErrorString(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v2, v3, v4, v0, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    goto :goto_0

    .line 957
    .end local v0    # "invalidContentTypeException":Lcom/google/android/exoplayer/upstream/HttpDataSource$InvalidContentTypeException;
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v3, 0x7

    invoke-virtual {p2}, Ljava/io/IOException;->getMessage()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/player/PlayerUtil;->hashErrorString(Ljava/lang/String;)I

    move-result v4

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v5

    invoke-virtual {v2, v3, v4, p2, v5}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    goto :goto_0
.end method

.method public onExoInternalWidevineDrmError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)V
    .locals 5
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Ljava/lang/Exception;

    .prologue
    .line 970
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->isSessionStarted()Z

    move-result v1

    if-nez v1, :cond_1

    .line 997
    :cond_0
    :goto_0
    return-void

    .line 976
    :cond_1
    sget v1, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v2, 0x15

    if-lt v1, v2, :cond_2

    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onExoInternalWidevineDrmErrorV21(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Ljava/lang/Exception;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 978
    :cond_2
    instance-of v1, p2, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v1, :cond_3

    .line 979
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v3, 0x5

    move-object v1, p2

    check-cast v1, Lcom/google/android/videos/api/CencLicenseException;

    iget v1, v1, Lcom/google/android/videos/api/CencLicenseException;->statusCode:I

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v4

    invoke-virtual {v2, v3, v1, p2, v4}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    goto :goto_0

    .line 983
    :cond_3
    instance-of v1, p2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$NoWidevinePsshException;

    if-eqz v1, :cond_4

    .line 984
    const/16 v0, 0x14

    .line 994
    .local v0, "errorType":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {p2}, Ljava/lang/Exception;->getMessage()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/player/PlayerUtil;->hashErrorString(Ljava/lang/String;)I

    move-result v2

    invoke-virtual {p1}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->getCurrentPosition()I

    move-result v3

    invoke-virtual {v1, v0, v2, p2, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    goto :goto_0

    .line 985
    .end local v0    # "errorType":I
    :cond_4
    instance-of v1, p2, Landroid/media/NotProvisionedException;

    if-eqz v1, :cond_5

    .line 986
    const/16 v0, 0x15

    .restart local v0    # "errorType":I
    goto :goto_1

    .line 987
    .end local v0    # "errorType":I
    :cond_5
    instance-of v1, p2, Landroid/media/DeniedByServerException;

    if-eqz v1, :cond_6

    .line 988
    const/16 v0, 0x16

    .restart local v0    # "errorType":I
    goto :goto_1

    .line 989
    .end local v0    # "errorType":I
    :cond_6
    instance-of v1, p2, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$InvalidKeySetIdException;

    if-eqz v1, :cond_7

    .line 990
    const/16 v0, 0x17

    .restart local v0    # "errorType":I
    goto :goto_1

    .line 992
    .end local v0    # "errorType":I
    :cond_7
    const/16 v0, 0x13

    .restart local v0    # "errorType":I
    goto :goto_1
.end method

.method public onExoPausedFrameTimestamp(Lcom/google/android/videos/player/exo/ExoVideosPlayer;I)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "frameTimestampMs"    # I

    .prologue
    .line 1043
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerPause(I)V

    .line 1044
    return-void
.end method

.method public onExoPlayerError(Lcom/google/android/videos/player/exo/ExoVideosPlayer;Lcom/google/android/exoplayer/ExoPlaybackException;)V
    .locals 13
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "e"    # Lcom/google/android/exoplayer/ExoPlaybackException;

    .prologue
    const v8, 0x7f0b017b

    const v12, 0x7f0b0147

    const/4 v10, 0x0

    const/4 v9, 0x1

    .line 855
    const/4 v0, 0x1

    .line 856
    .local v0, "canRetry":Z
    iget-object v11, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v11}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v7

    .line 857
    .local v7, "networkAvailable":Z
    invoke-virtual {p2}, Lcom/google/android/exoplayer/ExoPlaybackException;->getCause()Ljava/lang/Throwable;

    move-result-object v1

    .line 858
    .local v1, "cause":Ljava/lang/Throwable;
    iget-object v11, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v11, v11, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v11, :cond_0

    if-nez v7, :cond_0

    .line 859
    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v9, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 899
    .local v6, "errorMessage":Ljava/lang/String;
    :goto_0
    if-eqz v0, :cond_e

    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    :goto_1
    invoke-direct {p0, v6, v8}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 900
    return-void

    .line 860
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_0
    instance-of v11, v1, Lcom/google/android/exoplayer/MediaCodecTrackRenderer$DecoderInitializationException;

    if-eqz v11, :cond_1

    .line 861
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v8, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 862
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_1
    instance-of v11, v1, Lcom/google/android/exoplayer/audio/AudioTrack$InitializationException;

    if-eqz v11, :cond_2

    .line 863
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-virtual {v8, v12}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 864
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_2
    instance-of v11, v1, Lcom/google/android/videos/api/CencLicenseException;

    if-eqz v11, :cond_3

    move-object v2, v1

    .line 865
    check-cast v2, Lcom/google/android/videos/api/CencLicenseException;

    .line 866
    .local v2, "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v9, v9, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    iget-object v10, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v10, v10, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->isRental:Z

    invoke-static {v8, v2, v7, v9, v10}, Lcom/google/android/videos/player/PlayerUtil;->computeCencDrmError(Landroid/content/Context;Lcom/google/android/videos/api/CencLicenseException;ZZZ)Landroid/util/Pair;

    move-result-object v4

    .line 868
    .local v4, "error":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v6, v4, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v6, Ljava/lang/String;

    .line 869
    .restart local v6    # "errorMessage":Ljava/lang/String;
    iget-object v8, v4, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v8, Ljava/lang/Boolean;

    invoke-virtual {v8}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v0

    .line 870
    goto :goto_0

    .end local v2    # "cencLicenseException":Lcom/google/android/videos/api/CencLicenseException;
    .end local v4    # "error":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_3
    instance-of v11, v1, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$NoWidevinePsshException;

    if-eqz v11, :cond_4

    .line 871
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v9, 0x7f0b01f4

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 872
    .restart local v6    # "errorMessage":Ljava/lang/String;
    const/4 v0, 0x0

    goto :goto_0

    .line 873
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_4
    instance-of v11, v1, Landroid/media/DeniedByServerException;

    if-eqz v11, :cond_5

    .line 874
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v9, 0x7f0b017a

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 875
    .restart local v6    # "errorMessage":Ljava/lang/String;
    const/4 v0, 0x0

    goto :goto_0

    .line 876
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_5
    instance-of v11, v1, Landroid/media/NotProvisionedException;

    if-eqz v11, :cond_6

    .line 877
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v9, 0x7f0b015f

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto :goto_0

    .line 878
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_6
    instance-of v11, v1, Landroid/media/MediaCodec$CryptoException;

    if-eqz v11, :cond_9

    move-object v3, v1

    .line 879
    check-cast v3, Landroid/media/MediaCodec$CryptoException;

    .line 880
    .local v3, "cryptoException":Landroid/media/MediaCodec$CryptoException;
    invoke-virtual {v3}, Landroid/media/MediaCodec$CryptoException;->getErrorCode()I

    move-result v5

    .line 881
    .local v5, "errorCode":I
    const/4 v8, 0x2

    if-eq v5, v8, :cond_7

    if-ne v5, v9, :cond_8

    .line 883
    :cond_7
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v11, 0x7f0b0160

    new-array v9, v9, [Ljava/lang/Object;

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v12

    aput-object v12, v9, v10

    invoke-virtual {v8, v11, v9}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_0

    .line 885
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_8
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v9, 0x7f0b0146

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_0

    .line 887
    .end local v3    # "cryptoException":Landroid/media/MediaCodec$CryptoException;
    .end local v5    # "errorCode":I
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_9
    instance-of v11, v1, Lcom/google/android/videos/drm/WidevineMediaDrmWrapper$InvalidKeySetIdException;

    if-eqz v11, :cond_a

    .line 888
    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v9, 0x7f0b0162

    invoke-virtual {v8, v9}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_0

    .line 889
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_a
    instance-of v11, v1, Lcom/google/android/exoplayer/upstream/HttpDataSource$HttpDataSourceException;

    if-eqz v11, :cond_c

    .line 890
    iget-object v9, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    if-eqz v7, :cond_b

    const v8, 0x7f0b0141

    :cond_b
    invoke-virtual {v9, v8}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_0

    .line 897
    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_c
    iget-object v11, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    iget-object v8, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v8, v8, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v8, :cond_d

    move v8, v9

    :goto_2
    invoke-virtual {v11, v1, v8}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;Z)Ljava/lang/String;

    move-result-object v6

    .restart local v6    # "errorMessage":Ljava/lang/String;
    goto/16 :goto_0

    .end local v6    # "errorMessage":Ljava/lang/String;
    :cond_d
    move v8, v10

    goto :goto_2

    .line 899
    .restart local v6    # "errorMessage":Ljava/lang/String;
    :cond_e
    const/4 v8, 0x0

    goto/16 :goto_1
.end method

.method public onExoPlayerStateChanged(Lcom/google/android/videos/player/exo/ExoVideosPlayer;ZI)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/exo/ExoVideosPlayer;
    .param p2, "playWhenReady"    # Z
    .param p3, "playerState"    # I

    .prologue
    .line 819
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerStateChanged(ZI)V

    .line 820
    return-void
.end method

.method public onExoReleased(I)V
    .locals 2
    .param p1, "finalPosition"    # I

    .prologue
    .line 1048
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->isSessionStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 1049
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->endSession(I)V

    .line 1051
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setSessionInProgress(Z)V

    .line 1052
    return-void
.end method

.method public onHQ()V
    .locals 2

    .prologue
    .line 654
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getHq()Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v0, 0x1

    .line 655
    .local v0, "hq":Z
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/PlaybackResumeState;->setHqState(Z)V

    .line 656
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/VideosPlayer;->setHq(Z)V

    .line 657
    return-void

    .line 654
    .end local v0    # "hq":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onLegacyHqToggled(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZZ)V
    .locals 7
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "hq"    # Z
    .param p3, "wasAutomaticDrop"    # Z

    .prologue
    const/4 v4, 0x1

    .line 1110
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v3, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHq(Z)V

    .line 1111
    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getStreamSelection()Lcom/google/android/videos/streams/LegacyStreamSelection;

    move-result-object v1

    .line 1112
    .local v1, "streamSelection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    if-nez v1, :cond_0

    .line 1127
    :goto_0
    return-void

    .line 1116
    :cond_0
    if-nez p2, :cond_1

    if-eqz p3, :cond_1

    .line 1117
    iget-boolean v3, v1, Lcom/google/android/videos/streams/LegacyStreamSelection;->isHiHd:Z

    if-eqz v3, :cond_2

    const v2, 0x7f0b018e

    .line 1119
    .local v2, "toastMessage":I
    :goto_1
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    invoke-static {v3, v2, v4}, Lcom/google/android/videos/utils/Util;->showToast(Landroid/content/Context;II)V

    .line 1121
    .end local v2    # "toastMessage":I
    :cond_1
    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getSelectedStream()Lcom/google/android/videos/streams/MediaStream;

    move-result-object v0

    .line 1122
    .local v0, "selectedStream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v3, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onLegacyStreamSelected(Lcom/google/android/videos/streams/MediaStream;)V

    .line 1124
    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v3, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v6, v3, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    if-eqz p3, :cond_3

    const/4 v3, 0x2

    :goto_2
    invoke-virtual {v5, v6, v3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatSelected(II)V

    .line 1126
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v5, v0, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v5, v5, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v3, v5, v4}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatEnabled(II)V

    goto :goto_0

    .line 1117
    .end local v0    # "selectedStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_2
    const v2, 0x7f0b018f

    goto :goto_1

    .restart local v0    # "selectedStream":Lcom/google/android/videos/streams/MediaStream;
    :cond_3
    move v3, v4

    .line 1124
    goto :goto_2
.end method

.method public onLegacyInitializationError(Lcom/google/android/videos/player/legacy/LegacyPlayer;Ljava/lang/Exception;)V
    .locals 8
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    const/4 v7, 0x0

    .line 1081
    instance-of v4, p2, Lcom/google/android/videos/streams/MissingStreamException;

    if-eqz v4, :cond_0

    .line 1082
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v5, 0x2

    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v6

    invoke-virtual {v4, v5, v7, p2, v6}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1084
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v5, 0x7f0b01f4

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1085
    .local v2, "errorMessage":Ljava/lang/String;
    const/4 v3, 0x0

    .line 1105
    .local v3, "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :goto_0
    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 1106
    return-void

    .line 1086
    .end local v2    # "errorMessage":Ljava/lang/String;
    .end local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_0
    instance-of v4, p2, Lcom/google/android/videos/player/ProtectedBufferException;

    if-eqz v4, :cond_1

    .line 1087
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v5, 0xd

    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v6

    invoke-virtual {v4, v5, v7, p2, v6}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1089
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    const v5, 0x7f0b0152

    invoke-virtual {v4, v5}, Landroid/app/Activity;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 1090
    .restart local v2    # "errorMessage":Ljava/lang/String;
    const/4 v3, 0x0

    .restart local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    goto :goto_0

    .line 1091
    .end local v2    # "errorMessage":Ljava/lang/String;
    .end local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_1
    instance-of v4, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v4, :cond_3

    move-object v0, p2

    .line 1092
    check-cast v0, Lcom/google/android/videos/drm/DrmException;

    .line 1093
    .local v0, "drmException":Lcom/google/android/videos/drm/DrmException;
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/4 v5, 0x6

    iget v6, v0, Lcom/google/android/videos/drm/DrmException;->errorCode:I

    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v7

    invoke-virtual {v4, v5, v6, p2, v7}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1095
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v5}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v5

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v6, v6, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    iget-object v7, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v7, v7, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->isRental:Z

    invoke-static {v4, v0, v5, v6, v7}, Lcom/google/android/videos/player/PlayerUtil;->computeDrmError(Landroid/content/Context;Lcom/google/android/videos/drm/DrmException;ZZZ)Landroid/util/Pair;

    move-result-object v1

    .line 1097
    .local v1, "error":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v2, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v2, Ljava/lang/String;

    .line 1098
    .restart local v2    # "errorMessage":Ljava/lang/String;
    iget-object v4, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v4, Ljava/lang/Boolean;

    invoke-virtual {v4}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v4

    if-eqz v4, :cond_2

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .line 1099
    .restart local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :goto_1
    goto :goto_0

    .line 1098
    .end local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    :cond_2
    const/4 v3, 0x0

    goto :goto_1

    .line 1100
    .end local v0    # "drmException":Lcom/google/android/videos/drm/DrmException;
    .end local v1    # "error":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    .end local v2    # "errorMessage":Ljava/lang/String;
    :cond_3
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    const/16 v5, 0xc

    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v6

    invoke-virtual {v4, v5, v7, p2, v6}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1102
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->errorHelper:Lcom/google/android/videos/utils/ErrorHelper;

    invoke-virtual {v4, p2}, Lcom/google/android/videos/utils/ErrorHelper;->humanize(Ljava/lang/Throwable;)Ljava/lang/String;

    move-result-object v2

    .line 1103
    .restart local v2    # "errorMessage":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    .restart local v3    # "retryAction":Lcom/google/android/videos/utils/RetryAction;
    goto :goto_0
.end method

.method public onLegacyInitialized(Lcom/google/android/videos/player/legacy/LegacyPlayer;Lcom/google/android/videos/streams/LegacyStreamSelection;)V
    .locals 8
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "selection"    # Lcom/google/android/videos/streams/LegacyStreamSelection;

    .prologue
    const/4 v2, 0x0

    .line 1064
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-boolean v1, p2, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSupportsQualityToggle(Z)V

    .line 1065
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iget-boolean v1, p2, Lcom/google/android/videos/streams/LegacyStreamSelection;->isHiHd:Z

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setHqIsHd(Z)V

    .line 1067
    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getSelectedStream()Lcom/google/android/videos/streams/MediaStream;

    move-result-object v7

    .line 1068
    .local v7, "selectedStream":Lcom/google/android/videos/streams/MediaStream;
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0, v7}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onLegacyStreamSelected(Lcom/google/android/videos/streams/MediaStream;)V

    .line 1070
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v1, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v1, v1, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatSelected(II)V

    .line 1071
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v1, v7, Lcom/google/android/videos/streams/MediaStream;->info:Lcom/google/android/videos/proto/StreamInfo;

    iget v1, v1, Lcom/google/android/videos/proto/StreamInfo;->itag:I

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onFormatEnabled(II)V

    .line 1072
    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-result-object v1

    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getSelectedAudioTrack()I

    move-result v2

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v0, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    iget-object v3, v0, Lcom/google/android/videos/streams/Streams;->captions:Ljava/util/List;

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget v4, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleMode:I

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-object v5, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleDefaultLanguage:Ljava/lang/String;

    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v6, v0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->haveAudioInDeviceLanguage:Z

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/player/LocalPlaybackHelper;->processTracks([Lcom/google/wireless/android/video/magma/proto/AudioInfo;ILjava/util/List;ILjava/lang/String;Z)V

    .line 1075
    return-void
.end method

.method public onLegacyPausedFrameTimestamp(Lcom/google/android/videos/player/legacy/LegacyPlayer;I)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "frameTimestampMs"    # I

    .prologue
    .line 1160
    invoke-direct {p0, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerPause(I)V

    .line 1161
    return-void
.end method

.method public onLegacyPlayerError(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZLcom/google/android/videos/player/legacy/MediaPlayerException;)V
    .locals 7
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "fatal"    # Z
    .param p3, "exception"    # Lcom/google/android/videos/player/legacy/MediaPlayerException;

    .prologue
    .line 1132
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v4}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v3

    .line 1133
    .local v3, "networkAvailable":Z
    invoke-virtual {p1}, Lcom/google/android/videos/player/legacy/LegacyPlayer;->getCurrentPosition()I

    move-result v0

    .line 1135
    .local v0, "currentPosition":I
    iget v4, p3, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    const/16 v5, 0x64

    if-ne v4, v5, :cond_0

    .line 1136
    const/16 v2, 0x9

    .line 1147
    .local v2, "logErrorType":I
    :goto_0
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget v5, p3, Lcom/google/android/videos/player/legacy/MediaPlayerException;->extra:I

    invoke-virtual {v4, v2, v5, p3, v0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onError(IILjava/lang/Exception;I)V

    .line 1148
    if-nez p2, :cond_4

    .line 1156
    :goto_1
    return-void

    .line 1137
    .end local v2    # "logErrorType":I
    :cond_0
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v4, v4, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-nez v4, :cond_1

    if-nez v3, :cond_1

    .line 1138
    const/16 v2, 0xe

    .restart local v2    # "logErrorType":I
    goto :goto_0

    .line 1139
    .end local v2    # "logErrorType":I
    :cond_1
    iget v4, p3, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    const/16 v5, -0x4269

    if-ne v4, v5, :cond_2

    .line 1140
    const/16 v2, 0xb

    .restart local v2    # "logErrorType":I
    goto :goto_0

    .line 1141
    .end local v2    # "logErrorType":I
    :cond_2
    iget v4, p3, Lcom/google/android/videos/player/legacy/MediaPlayerException;->what:I

    const/4 v5, 0x1

    if-ne v4, v5, :cond_3

    .line 1142
    const/16 v2, 0xa

    .restart local v2    # "logErrorType":I
    goto :goto_0

    .line 1145
    .end local v2    # "logErrorType":I
    :cond_3
    const/4 v2, 0x0

    .restart local v2    # "logErrorType":I
    goto :goto_0

    .line 1153
    :cond_4
    iget-object v4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;

    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v5, v5, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    iget-object v6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->initData:Lcom/google/android/videos/player/DirectorInitializer$InitializationData;

    iget-boolean v6, v6, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->isRental:Z

    invoke-static {v4, p3, v3, v5, v6}, Lcom/google/android/videos/player/PlayerUtil;->computePlayerError(Landroid/content/Context;Lcom/google/android/videos/player/legacy/MediaPlayerException;ZZZ)Landroid/util/Pair;

    move-result-object v1

    .line 1155
    .local v1, "error":Landroid/util/Pair;, "Landroid/util/Pair<Ljava/lang/String;Ljava/lang/Boolean;>;"
    iget-object v4, v1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v4, Ljava/lang/String;

    iget-object v5, v1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v5, Ljava/lang/Boolean;

    invoke-virtual {v5}, Ljava/lang/Boolean;->booleanValue()Z

    move-result v5

    if-eqz v5, :cond_5

    iget-object v5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->directorRetryAction:Lcom/google/android/videos/utils/RetryAction;

    :goto_2
    invoke-direct {p0, v4, v5}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerFailed(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    goto :goto_1

    :cond_5
    const/4 v5, 0x0

    goto :goto_2
.end method

.method public onLegacyPlayerStateChanged(Lcom/google/android/videos/player/legacy/LegacyPlayer;ZI)V
    .locals 0
    .param p1, "player"    # Lcom/google/android/videos/player/legacy/LegacyPlayer;
    .param p2, "playWhenReady"    # Z
    .param p3, "playerState"    # I

    .prologue
    .line 1059
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerStateChanged(ZI)V

    .line 1060
    return-void
.end method

.method public onLegacyReleased(I)V
    .locals 2
    .param p1, "finalPosition"    # I

    .prologue
    .line 1165
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->endSession(I)V

    .line 1166
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setSessionInProgress(Z)V

    .line 1167
    return-void
.end method

.method public onResponse(Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "response"    # Lcom/google/android/videos/subtitles/Subtitles;

    .prologue
    .line 1264
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-ne p1, v0, :cond_0

    .line 1265
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0, p2}, Lcom/google/android/videos/player/VideosPlayer;->setSubtitles(Lcom/google/android/videos/subtitles/Subtitles;)V

    .line 1266
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onSubtitleEnabled(Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 1268
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 121
    check-cast p1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/android/videos/subtitles/Subtitles;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onResponse(Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;)V

    return-void
.end method

.method public onScrubbingCanceled()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 642
    iput-boolean v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->scrubbing:Z

    .line 643
    iget v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    const/4 v1, 0x1

    if-ne v0, v1, :cond_0

    .line 644
    iget-boolean v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playWhenScrubbingEnds:Z

    if-eqz v0, :cond_1

    .line 645
    invoke-virtual {p0, v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->play(Z)V

    .line 650
    :cond_0
    :goto_0
    return-void

    .line 647
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onPlayerPause(I)V

    goto :goto_0
.end method

.method public onScrubbingStart(Z)V
    .locals 3
    .param p1, "isFineScrubbing"    # Z

    .prologue
    const/4 v2, 0x1

    .line 611
    iput-boolean v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->scrubbing:Z

    .line 612
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onPlayerScrubbingStart()V

    .line 613
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1, v2, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onUserSeekingChanged(IZZ)V

    .line 614
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0}, Lcom/google/android/videos/player/VideosPlayer;->getPlayWhenReady()Z

    move-result v0

    iput-boolean v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playWhenScrubbingEnds:Z

    .line 615
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->setPlayWhenReady(Z)V

    .line 616
    return-void
.end method

.method public onSeekTo(ZIZ)V
    .locals 4
    .param p1, "isFineScrubbing"    # Z
    .param p2, "time"    # I
    .param p3, "scrubbingEnded"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 620
    if-nez p3, :cond_0

    .line 621
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v2, v0}, Lcom/google/android/videos/player/VideosPlayer;->setTrickPlayEnabled(Z)V

    .line 622
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0, p2, v1}, Lcom/google/android/videos/player/VideosPlayer;->seekTo(IZ)V

    .line 638
    :goto_0
    return-void

    .line 625
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v2, v1}, Lcom/google/android/videos/player/VideosPlayer;->setTrickPlayEnabled(Z)V

    .line 626
    iget v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    if-ne v2, v0, :cond_4

    .line 627
    iget-boolean v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playWhenScrubbingEnds:Z

    if-eqz v2, :cond_2

    .line 628
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0, p2, v1}, Lcom/google/android/videos/player/VideosPlayer;->seekTo(IZ)V

    .line 629
    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->play(Z)V

    .line 636
    :cond_1
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackLogger:Lcom/google/android/videos/player/logging/PlaybackLogger;

    invoke-virtual {v0, p2, v1, p1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->onUserSeekingChanged(IZZ)V

    .line 637
    iput-boolean v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->scrubbing:Z

    goto :goto_0

    .line 631
    :cond_2
    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    iget-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    if-eqz v3, :cond_3

    :goto_2
    invoke-interface {v2, p2, v0}, Lcom/google/android/videos/player/VideosPlayer;->seekTo(IZ)V

    goto :goto_1

    :cond_3
    move v0, v1

    goto :goto_2

    .line 633
    :cond_4
    iget v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    if-nez v0, :cond_1

    .line 634
    iput p2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localResumeTimeMillis:I

    goto :goto_1
.end method

.method public onSelectedAudioTrackIndexChanged(I)V
    .locals 4
    .param p1, "trackIndex"    # I

    .prologue
    const/4 v3, 0x1

    .line 1217
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/player/PlaybackResumeState;->setSelectedAudioTrackIndex(I)V

    .line 1218
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v1, p1}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setSelectedAudioTrackIndex(I)V

    .line 1219
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1, p1}, Lcom/google/android/videos/player/VideosPlayer;->setSelectedAudioTrack(I)V

    .line 1220
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getAudioTracks()[Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    move-result-object v1

    aget-object v1, v1, p1

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    .line 1221
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    iget-boolean v1, v1, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-eqz v1, :cond_1

    .line 1222
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->subtitleTracks:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    .line 1224
    .local v0, "forcedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-direct {p0, v0, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;Z)V

    .line 1226
    .end local v0    # "forcedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_1
    return-void
.end method

.method public onSelectedSubtitleTrackChanged(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 4
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    const/4 v3, 0x1

    .line 1231
    if-nez p1, :cond_0

    .line 1233
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->subtitleTracks:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedAudioTrackLanguage:Ljava/lang/String;

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object p1

    .line 1234
    const/4 v0, 0x0

    .line 1238
    .local v0, "languageCode":Ljava/lang/String;
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/PlaybackResumeState;->setSubtitleLanguage(Ljava/lang/String;)V

    .line 1239
    invoke-direct {p0, p1, v3}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setSelectedSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;Z)V

    .line 1240
    return-void

    .line 1236
    .end local v0    # "languageCode":Ljava/lang/String;
    :cond_0
    iget-object v0, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    .restart local v0    # "languageCode":Ljava/lang/String;
    goto :goto_0
.end method

.method public onStreamingAccepted(Z)V
    .locals 1
    .param p1, "acceptedThroughDialog"    # Z

    .prologue
    .line 562
    if-eqz p1, :cond_0

    .line 563
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 565
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    .line 566
    return-void
.end method

.method public onStreamingDeclined()V
    .locals 1

    .prologue
    const/4 v0, 0x0

    .line 587
    invoke-virtual {p0, v0, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setState(ZZ)V

    .line 588
    return-void
.end method

.method public onTouch(Landroid/view/View;Landroid/view/MotionEvent;)Z
    .locals 1
    .param p1, "view"    # Landroid/view/View;
    .param p2, "event"    # Landroid/view/MotionEvent;

    .prologue
    .line 724
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->showControls()V

    .line 725
    const/4 v0, 0x1

    return v0
.end method

.method public onWifiConnected()V
    .locals 0

    .prologue
    .line 598
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    .line 599
    return-void
.end method

.method public onWifiDisconnected()V
    .locals 1

    .prologue
    .line 592
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onDialogShown()V

    .line 593
    invoke-virtual {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->pause()V

    .line 594
    return-void
.end method

.method public openDrm([BLjava/lang/String;ZI[B)V
    .locals 8
    .param p1, "psshData"    # [B
    .param p2, "mimeType"    # Ljava/lang/String;
    .param p3, "isOffline"    # Z
    .param p4, "cencSecurityLevel"    # I
    .param p5, "cencKeySetId"    # [B

    .prologue
    .line 293
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->maybeCreateExoVideosPlayer()Lcom/google/android/videos/player/exo/ExoVideosPlayer;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoId:Ljava/lang/String;

    move-object v3, p1

    move-object v4, p2

    move v5, p3

    move v6, p4

    move-object v7, p5

    invoke-virtual/range {v0 .. v7}, Lcom/google/android/videos/player/exo/ExoVideosPlayer;->openDrm(Ljava/lang/String;Ljava/lang/String;[BLjava/lang/String;ZI[B)V

    .line 295
    return-void
.end method

.method public pause()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 603
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setMediaSessionState(I)V

    .line 604
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView;->setKeepScreenOn(Z)V

    .line 605
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0, v1}, Lcom/google/android/videos/player/VideosPlayer;->setPlayWhenReady(Z)V

    .line 606
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setStreamingInProgress(Z)V

    .line 607
    return-void
.end method

.method public play(Z)V
    .locals 3
    .param p1, "onInitialized"    # Z

    .prologue
    .line 538
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onPlayerPlay()V

    .line 539
    const/4 v0, 0x3

    invoke-direct {p0, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->setMediaSessionState(I)V

    .line 541
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    const/high16 v1, -0x80000000

    const/4 v2, 0x1

    invoke-virtual {v0, p0, v1, v2}, Landroid/media/AudioManager;->requestAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;II)I

    .line 543
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->videoInfo:Lcom/google/android/videos/player/VideoInfo;

    iget-boolean v0, v0, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v0, :cond_1

    .line 544
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->playVideoInternal()V

    .line 550
    :cond_0
    :goto_0
    return-void

    .line 545
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-nez v0, :cond_2

    .line 546
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->onStreamingAccepted(Z)V

    goto :goto_0

    .line 547
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->onPlaybackStarted()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 548
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onDialogShown()V

    goto :goto_0
.end method

.method public release()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/4 v2, 0x0

    .line 400
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual {v0, v2}, Landroid/support/v4/media/session/MediaSessionCompat;->setActive(Z)V

    .line 401
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->mediaSession:Landroid/support/v4/media/session/MediaSessionCompat;

    invoke-virtual {v0}, Landroid/support/v4/media/session/MediaSessionCompat;->release()V

    .line 402
    invoke-direct {p0}, Lcom/google/android/videos/player/LocalPlaybackHelper;->getAudioManager()Landroid/media/AudioManager;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/media/AudioManager;->abandonAudioFocus(Landroid/media/AudioManager$OnAudioFocusChangeListener;)I

    .line 403
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    if-eqz v0, :cond_0

    .line 404
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->streamingWarningHelper:Lcom/google/android/videos/ui/StreamingWarningHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/StreamingWarningHelper;->unregister()V

    .line 406
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->internalBroadcastReceiver:Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;

    invoke-virtual {v0}, Lcom/google/android/videos/player/LocalPlaybackHelper$InternalBroadcastReceiver;->unregister()V

    .line 407
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackStatusNotifier:Lcom/google/android/videos/player/PlaybackStatusNotifier;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/PlaybackStatusNotifier;->setStreamingInProgress(Z)V

    .line 408
    iput-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->selectedSubtitleTrack:Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 409
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-eqz v0, :cond_1

    iget v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    if-eqz v0, :cond_1

    .line 410
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlaybackResumeState;->setResumeTimeMillis(I)V

    .line 411
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->lastPlaybackSaver:Lcom/google/android/videos/pinning/LastPlaybackSaver;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v1}, Lcom/google/android/videos/player/VideosPlayer;->getCurrentPosition()I

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pinning/LastPlaybackSaver;->persistLastPlayback(I)V

    .line 414
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->uiHandler:Landroid/os/Handler;

    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->positionUpdateRunnable:Ljava/lang/Runnable;

    invoke-virtual {v0, v1}, Landroid/os/Handler;->removeCallbacks(Ljava/lang/Runnable;)V

    .line 415
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    if-eqz v0, :cond_2

    .line 416
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    invoke-interface {v0}, Lcom/google/android/videos/player/VideosPlayer;->release()V

    .line 417
    iput-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->player:Lcom/google/android/videos/player/VideosPlayer;

    .line 419
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->reset()V

    .line 420
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    if-eqz v0, :cond_3

    .line 421
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    invoke-interface {v0}, Lcom/google/android/videos/player/LocalPlaybackViewHolder;->release()V

    .line 422
    iput-object v3, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localPlaybackViewHolder:Lcom/google/android/videos/player/LocalPlaybackViewHolder;

    .line 424
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    if-eqz v0, :cond_4

    .line 425
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/RemoteScreenPanelHelper;->reset()V

    .line 427
    :cond_4
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/player/PlayerView;->setKeepScreenOn(Z)V

    .line 428
    iput v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 429
    return-void
.end method

.method public setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V
    .locals 2
    .param p1, "errorMessage"    # Ljava/lang/String;
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    const/4 v0, 0x0

    .line 703
    const/4 v1, 0x2

    iput v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->state:I

    .line 704
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->listener:Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;

    invoke-interface {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper$Listener;->onLocalPlaybackError()V

    .line 705
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/player/PlayerView;->setKeepScreenOn(Z)V

    .line 706
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playbackResumeState:Lcom/google/android/videos/player/PlaybackResumeState;

    if-nez p2, :cond_0

    const/4 v0, 0x1

    :cond_0
    invoke-virtual {v1, p1, v0}, Lcom/google/android/videos/player/PlaybackResumeState;->setPlaybackError(Ljava/lang/String;Z)V

    .line 707
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setErrorState(Ljava/lang/String;Lcom/google/android/videos/utils/RetryAction;)V

    .line 708
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    if-eqz v0, :cond_1

    .line 709
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setErrorState(Ljava/lang/String;)V

    .line 711
    :cond_1
    return-void
.end method

.method public setState(ZZ)V
    .locals 1
    .param p1, "playWhenReady"    # Z
    .param p2, "loading"    # Z

    .prologue
    .line 715
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/player/PlayerView;->setKeepScreenOn(Z)V

    .line 716
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/player/overlay/ControllerOverlay;->setState(ZZ)V

    .line 717
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    if-eqz v0, :cond_0

    .line 718
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenInfoOverlay:Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;->setState(ZZ)V

    .line 720
    :cond_0
    return-void
.end method

.method public setViews(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/ControllerOverlay;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/ui/RemoteScreenPanelHelper;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V
    .locals 1
    .param p1, "playerView"    # Lcom/google/android/videos/player/PlayerView;
    .param p2, "controllerOverlay"    # Lcom/google/android/videos/player/overlay/ControllerOverlay;
    .param p3, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .param p4, "remoteScreenPanelHelper"    # Lcom/google/android/videos/ui/RemoteScreenPanelHelper;
    .param p5, "localKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p6, "localTaglessKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;

    .prologue
    .line 282
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/PlayerView;

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 283
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/overlay/ControllerOverlay;

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localControllerOverlay:Lcom/google/android/videos/player/overlay/ControllerOverlay;

    .line 284
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    iput-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localSubtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 285
    iput-object p4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->remoteScreenPanelHelper:Lcom/google/android/videos/ui/RemoteScreenPanelHelper;

    .line 286
    iput-object p5, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 287
    iput-object p6, p0, Lcom/google/android/videos/player/LocalPlaybackHelper;->localTaglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 288
    invoke-virtual {p1, p0}, Lcom/google/android/videos/player/PlayerView;->setOnTouchListener(Landroid/view/View$OnTouchListener;)V

    .line 289
    return-void
.end method
