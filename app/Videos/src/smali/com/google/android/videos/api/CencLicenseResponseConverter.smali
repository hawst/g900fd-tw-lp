.class final Lcom/google/android/videos/api/CencLicenseResponseConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "CencLicenseResponseConverter.java"


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<[B>;"
    }
.end annotation


# static fields
.field private static final DOUBLE_LINE_TERMINATOR:[B

.field private static final FIRST_LINE_PATTERN:Ljava/util/regex/Pattern;

.field private static final SINGLE_LINE_TERMINATOR:[B


# direct methods
.method static constructor <clinit>()V
    .locals 1

    .prologue
    .line 22
    const-string v0, "^GLS/(1\\.\\d{1,9}) (\\d+) (.+?)$"

    invoke-static {v0}, Ljava/util/regex/Pattern;->compile(Ljava/lang/String;)Ljava/util/regex/Pattern;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/api/CencLicenseResponseConverter;->FIRST_LINE_PATTERN:Ljava/util/regex/Pattern;

    .line 24
    const/4 v0, 0x2

    new-array v0, v0, [B

    fill-array-data v0, :array_0

    sput-object v0, Lcom/google/android/videos/api/CencLicenseResponseConverter;->SINGLE_LINE_TERMINATOR:[B

    .line 25
    const/4 v0, 0x4

    new-array v0, v0, [B

    fill-array-data v0, :array_1

    sput-object v0, Lcom/google/android/videos/api/CencLicenseResponseConverter;->DOUBLE_LINE_TERMINATOR:[B

    return-void

    .line 24
    nop

    :array_0
    .array-data 1
        0xdt
        0xat
    .end array-data

    .line 25
    nop

    :array_1
    .array-data 1
        0xdt
        0xat
        0xdt
        0xat
    .end array-data
.end method

.method constructor <init>()V
    .locals 0

    .prologue
    .line 20
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    return-void
.end method

.method private static indexOf([B[B)I
    .locals 6
    .param p0, "data"    # [B
    .param p1, "item"    # [B

    .prologue
    .line 71
    array-length v4, p0

    array-length v5, p1

    sub-int/2addr v4, v5

    add-int/lit8 v0, v4, 0x1

    .line 72
    .local v0, "endIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_3

    .line 73
    const/4 v3, 0x1

    .line 74
    .local v3, "matches":Z
    const/4 v2, 0x0

    .local v2, "j":I
    :goto_1
    array-length v4, p1

    if-ge v2, v4, :cond_0

    .line 75
    add-int v4, v1, v2

    aget-byte v4, p0, v4

    aget-byte v5, p1, v2

    if-eq v4, v5, :cond_1

    .line 76
    const/4 v3, 0x0

    .line 80
    :cond_0
    if-eqz v3, :cond_2

    .line 84
    .end local v1    # "i":I
    .end local v2    # "j":I
    .end local v3    # "matches":Z
    :goto_2
    return v1

    .line 74
    .restart local v1    # "i":I
    .restart local v2    # "j":I
    .restart local v3    # "matches":Z
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 72
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 84
    .end local v2    # "j":I
    .end local v3    # "matches":Z
    :cond_3
    const/4 v1, -0x1

    goto :goto_2
.end method


# virtual methods
.method protected bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 20
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/CencLicenseResponseConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)[B

    move-result-object v0

    return-object v0
.end method

.method protected final convertResponseEntity(Lorg/apache/http/HttpEntity;)[B
    .locals 13
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    const/4 v12, 0x0

    const/4 v11, -0x1

    .line 30
    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toByteArray(Lorg/apache/http/HttpEntity;)[B

    move-result-object v7

    .line 32
    .local v7, "responseData":[B
    sget-object v10, Lcom/google/android/videos/api/CencLicenseResponseConverter;->SINGLE_LINE_TERMINATOR:[B

    invoke-static {v7, v10}, Lcom/google/android/videos/api/CencLicenseResponseConverter;->indexOf([B[B)I

    move-result v4

    .line 33
    .local v4, "firstLineTerminatorOffset":I
    if-ne v4, v11, :cond_0

    .line 34
    new-instance v10, Lcom/google/android/videos/converter/ConverterException;

    const-string v11, "Could not locate first line terminator"

    invoke-direct {v10, v11}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 37
    :cond_0
    new-instance v3, Ljava/lang/String;

    const-string v10, "UTF-8"

    invoke-direct {v3, v7, v12, v4, v10}, Ljava/lang/String;-><init>([BIILjava/lang/String;)V

    .line 38
    .local v3, "firstLineString":Ljava/lang/String;
    sget-object v10, Lcom/google/android/videos/api/CencLicenseResponseConverter;->FIRST_LINE_PATTERN:Ljava/util/regex/Pattern;

    invoke-virtual {v10, v3}, Ljava/util/regex/Pattern;->matcher(Ljava/lang/CharSequence;)Ljava/util/regex/Matcher;

    move-result-object v6

    .line 39
    .local v6, "matcher":Ljava/util/regex/Matcher;
    invoke-virtual {v6}, Ljava/util/regex/Matcher;->matches()Z

    move-result v10

    if-nez v10, :cond_1

    .line 40
    new-instance v10, Lcom/google/android/videos/converter/ConverterException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid first line: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 43
    :cond_1
    const/4 v10, 0x2

    invoke-virtual {v6, v10}, Ljava/util/regex/Matcher;->group(I)Ljava/lang/String;

    move-result-object v9

    .line 46
    .local v9, "statusCodeString":Ljava/lang/String;
    :try_start_0
    invoke-static {v9}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0

    move-result v8

    .line 51
    .local v8, "statusCode":I
    sget-object v10, Lcom/google/android/videos/api/CencLicenseResponseConverter;->DOUBLE_LINE_TERMINATOR:[B

    invoke-static {v7, v10}, Lcom/google/android/videos/api/CencLicenseResponseConverter;->indexOf([B[B)I

    move-result v5

    .line 52
    .local v5, "lastTerminatorsOffset":I
    if-ne v5, v11, :cond_2

    .line 53
    new-instance v10, Lcom/google/android/videos/converter/ConverterException;

    const-string v11, "Failed to locate end of headers"

    invoke-direct {v10, v11}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 47
    .end local v5    # "lastTerminatorsOffset":I
    .end local v8    # "statusCode":I
    :catch_0
    move-exception v2

    .line 48
    .local v2, "e":Ljava/lang/NumberFormatException;
    new-instance v10, Lcom/google/android/videos/converter/ConverterException;

    new-instance v11, Ljava/lang/StringBuilder;

    invoke-direct {v11}, Ljava/lang/StringBuilder;-><init>()V

    const-string v12, "Invalid status code: "

    invoke-virtual {v11, v12}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11, v9}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v11

    invoke-virtual {v11}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v11}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/String;)V

    throw v10

    .line 56
    .end local v2    # "e":Ljava/lang/NumberFormatException;
    .restart local v5    # "lastTerminatorsOffset":I
    .restart local v8    # "statusCode":I
    :cond_2
    array-length v10, v7

    sub-int/2addr v10, v5

    add-int/lit8 v0, v10, -0x4

    .line 57
    .local v0, "dataLength":I
    const/4 v1, 0x0

    .line 58
    .local v1, "drmMessageData":[B
    if-lez v0, :cond_3

    .line 59
    new-array v1, v0, [B

    .line 60
    add-int/lit8 v10, v5, 0x4

    invoke-static {v7, v10, v1, v12, v0}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 63
    :cond_3
    if-eqz v8, :cond_4

    .line 64
    new-instance v10, Lcom/google/android/videos/api/CencLicenseException;

    invoke-direct {v10, v8, v1}, Lcom/google/android/videos/api/CencLicenseException;-><init>(I[B)V

    throw v10

    .line 67
    :cond_4
    return-object v1
.end method
