.class Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;
.super Ljava/lang/Object;
.source "TimestampedCachingRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/async/TimestampedCachingRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CachingCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<TR;TE;>;"
    }
.end annotation


# instance fields
.field private final targetCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/async/TimestampedCachingRequester;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/TimestampedCachingRequester;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<TR;TE;>;)V"
        }
    .end annotation

    .prologue
    .line 112
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;, "Lcom/google/android/videos/async/TimestampedCachingRequester<TR;TK;TE;>.CachingCallback;"
    .local p2, "targetCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<TR;TE;>;"
    iput-object p1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->this$0:Lcom/google/android/videos/async/TimestampedCachingRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 113
    iput-object p2, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    .line 114
    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;, "Lcom/google/android/videos/async/TimestampedCachingRequester<TR;TK;TE;>.CachingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    iget-object v0, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 126
    return-void
.end method

.method public onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TR;TE;)V"
        }
    .end annotation

    .prologue
    .line 119
    .local p0, "this":Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;, "Lcom/google/android/videos/async/TimestampedCachingRequester<TR;TK;TE;>.CachingCallback;"
    .local p1, "request":Ljava/lang/Object;, "TR;"
    .local p2, "response":Ljava/lang/Object;, "TE;"
    iget-object v0, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->this$0:Lcom/google/android/videos/async/TimestampedCachingRequester;

    # getter for: Lcom/google/android/videos/async/TimestampedCachingRequester;->cache:Lcom/google/android/videos/cache/Cache;
    invoke-static {v0}, Lcom/google/android/videos/async/TimestampedCachingRequester;->access$000(Lcom/google/android/videos/async/TimestampedCachingRequester;)Lcom/google/android/videos/cache/Cache;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->this$0:Lcom/google/android/videos/async/TimestampedCachingRequester;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/async/TimestampedCachingRequester;->toKey(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    new-instance v2, Lcom/google/android/videos/async/Timestamped;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    invoke-direct {v2, p2, v4, v5}, Lcom/google/android/videos/async/Timestamped;-><init>(Ljava/lang/Object;J)V

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/cache/Cache;->put(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/async/TimestampedCachingRequester$CachingCallback;->targetCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 121
    return-void
.end method
