.class Lcom/google/android/videos/ui/VideosDrawerHelper$4;
.super Ljava/lang/Object;
.source "VideosDrawerHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/VideosDrawerHelper;->createVerticalItems(Landroid/util/SparseArray;Z)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/VideosDrawerHelper;)V
    .locals 0

    .prologue
    .line 170
    iput-object p1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 173
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$000(Lcom/google/android/videos/ui/VideosDrawerHelper;)Lcom/google/android/videos/activity/DrawerActivity;

    move-result-object v1

    instance-of v1, v1, Lcom/google/android/videos/activity/HomeActivity;

    if-eqz v1, :cond_0

    .line 174
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$000(Lcom/google/android/videos/ui/VideosDrawerHelper;)Lcom/google/android/videos/activity/DrawerActivity;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/activity/HomeActivity;

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I
    invoke-static {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$300(Lcom/google/android/videos/ui/VideosDrawerHelper;)I

    move-result v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/activity/HomeActivity;->onLibraryVerticalSelected(I)V

    .line 185
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    const/4 v2, 0x0

    # setter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I
    invoke-static {v1, v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$302(Lcom/google/android/videos/ui/VideosDrawerHelper;I)I

    .line 186
    return-void

    .line 176
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I
    invoke-static {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$300(Lcom/google/android/videos/ui/VideosDrawerHelper;)I

    move-result v1

    if-nez v1, :cond_1

    const/16 v0, 0x10

    .line 179
    .local v0, "libraryVertical":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;
    invoke-static {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$000(Lcom/google/android/videos/ui/VideosDrawerHelper;)Lcom/google/android/videos/activity/DrawerActivity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->activity:Lcom/google/android/videos/activity/DrawerActivity;
    invoke-static {v2}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$000(Lcom/google/android/videos/ui/VideosDrawerHelper;)Lcom/google/android/videos/activity/DrawerActivity;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->account:Ljava/lang/String;
    invoke-static {v3}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$100(Lcom/google/android/videos/ui/VideosDrawerHelper;)Ljava/lang/String;

    move-result-object v3

    invoke-static {v0}, Lcom/google/android/videos/ui/VerticalsHelper;->getVerticalExternalName(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v2, v3, v4}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Lcom/google/android/videos/activity/DrawerActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0

    .line 176
    .end local v0    # "libraryVertical":I
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/VideosDrawerHelper$4;->this$0:Lcom/google/android/videos/ui/VideosDrawerHelper;

    # getter for: Lcom/google/android/videos/ui/VideosDrawerHelper;->libraryContentVertical:I
    invoke-static {v1}, Lcom/google/android/videos/ui/VideosDrawerHelper;->access$300(Lcom/google/android/videos/ui/VideosDrawerHelper;)I

    move-result v0

    goto :goto_1
.end method
