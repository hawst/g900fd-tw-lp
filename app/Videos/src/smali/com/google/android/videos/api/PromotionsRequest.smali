.class public Lcom/google/android/videos/api/PromotionsRequest;
.super Lcom/google/android/videos/async/Request;
.source "PromotionsRequest.java"


# instance fields
.field public final country:Ljava/lang/String;

.field public final locale:Ljava/util/Locale;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/util/Locale;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "country"    # Ljava/lang/String;
    .param p3, "locale"    # Ljava/util/Locale;

    .prologue
    .line 20
    invoke-direct {p0, p1}, Lcom/google/android/videos/async/Request;-><init>(Ljava/lang/String;)V

    .line 21
    iput-object p2, p0, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    .line 22
    iput-object p3, p0, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    .line 23
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "obj"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 37
    if-ne p0, p1, :cond_1

    .line 41
    :cond_0
    :goto_0
    return v1

    .line 38
    :cond_1
    if-nez p1, :cond_2

    move v1, v2

    goto :goto_0

    .line 39
    :cond_2
    invoke-virtual {p0}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v3

    invoke-virtual {p1}, Ljava/lang/Object;->getClass()Ljava/lang/Class;

    move-result-object v4

    if-eq v3, v4, :cond_3

    move v1, v2

    goto :goto_0

    :cond_3
    move-object v0, p1

    .line 40
    check-cast v0, Lcom/google/android/videos/api/PromotionsRequest;

    .line 41
    .local v0, "that":Lcom/google/android/videos/api/PromotionsRequest;
    iget-object v3, p0, Lcom/google/android/videos/api/PromotionsRequest;->account:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/PromotionsRequest;->account:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    iget-object v4, v0, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    invoke-static {v3, v4}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public hashCode()I
    .locals 5

    .prologue
    const/4 v3, 0x0

    .line 27
    const/16 v0, 0x1f

    .line 28
    .local v0, "prime":I
    const/4 v1, 0x1

    .line 29
    .local v1, "result":I
    iget-object v2, p0, Lcom/google/android/videos/api/PromotionsRequest;->account:Ljava/lang/String;

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    add-int/lit8 v1, v2, 0x1f

    .line 30
    mul-int/lit8 v4, v1, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    if-nez v2, :cond_1

    move v2, v3

    :goto_1
    add-int v1, v4, v2

    .line 31
    mul-int/lit8 v2, v1, 0x1f

    iget-object v4, p0, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    if-nez v4, :cond_2

    :goto_2
    add-int v1, v2, v3

    .line 32
    return v1

    .line 29
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/api/PromotionsRequest;->account:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_0

    .line 30
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    goto :goto_1

    .line 31
    :cond_2
    iget-object v3, p0, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v3}, Ljava/util/Locale;->hashCode()I

    move-result v3

    goto :goto_2
.end method
