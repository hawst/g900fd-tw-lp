.class final Lcom/google/android/videos/api/UserLibraryListConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "UserLibraryListConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/UserLibraryRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 41
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 42
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "userlibrary"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/UserLibraryListConverter;->baseUri:Ljava/lang/String;

    .line 43
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/UserLibraryListConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 44
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/videos/api/UserLibraryRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/UserLibraryListConverter;->convertRequest(Lcom/google/android/videos/api/UserLibraryRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/UserLibraryRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 6
    .param p1, "request"    # Lcom/google/android/videos/api/UserLibraryRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 48
    new-instance v3, Lcom/google/android/videos/utils/UriBuilder;

    iget-object v4, p0, Lcom/google/android/videos/api/UserLibraryListConverter;->baseUri:Ljava/lang/String;

    invoke-direct {v3, v4}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    .line 49
    .local v3, "uriBuilder":Lcom/google/android/videos/utils/UriBuilder;
    iget-boolean v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->includeParents:Z

    if-eqz v4, :cond_1

    const-string v1, "pe"

    .line 51
    .local v1, "fieldSelector":Ljava/lang/String;
    :goto_0
    const-string v4, "fs"

    invoke-virtual {v3, v4, v1}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 52
    iget-object v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    if-eqz v4, :cond_2

    .line 53
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    iget-object v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v4

    if-ge v2, v4, :cond_2

    .line 54
    iget-object v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->assetResourceIds:Ljava/util/List;

    invoke-interface {v4, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 55
    .local v0, "assetId":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    .line 56
    const-string v4, "aid"

    invoke-virtual {v3, v4, v0}, Lcom/google/android/videos/utils/UriBuilder;->addQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 53
    :cond_0
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 49
    .end local v0    # "assetId":Ljava/lang/String;
    .end local v1    # "fieldSelector":Ljava/lang/String;
    .end local v2    # "i":I
    :cond_1
    const-string v1, "e"

    goto :goto_0

    .line 60
    .restart local v1    # "fieldSelector":Ljava/lang/String;
    :cond_2
    iget-object v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->snapshotToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_3

    .line 61
    const-string v4, "snapshotToken"

    iget-object v5, p1, Lcom/google/android/videos/api/UserLibraryRequest;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 62
    const-string v4, "ac"

    iget-object v5, p1, Lcom/google/android/videos/api/UserLibraryRequest;->pageToken:Ljava/lang/String;

    invoke-static {v5}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v5

    invoke-static {v5}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 65
    :cond_3
    iget-object v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->pageToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_4

    .line 66
    const-string v4, "pageToken"

    iget-object v5, p1, Lcom/google/android/videos/api/UserLibraryRequest;->pageToken:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 68
    :cond_4
    iget v4, p1, Lcom/google/android/videos/api/UserLibraryRequest;->maxResults:I

    const/4 v5, -0x1

    if-eq v4, v5, :cond_5

    .line 69
    const-string v4, "maxResults"

    iget v5, p1, Lcom/google/android/videos/api/UserLibraryRequest;->maxResults:I

    invoke-static {v5}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v3, v4, v5}, Lcom/google/android/videos/utils/UriBuilder;->setQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    .line 71
    :cond_5
    invoke-virtual {v3}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v4

    invoke-static {v4}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v4

    return-object v4
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 77
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/UserLibraryListConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/UserLibraryListConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/UserLibraryListResponse;

    move-result-object v0

    return-object v0
.end method
