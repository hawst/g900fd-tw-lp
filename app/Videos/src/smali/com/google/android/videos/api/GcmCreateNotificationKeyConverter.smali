.class final Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "GcmCreateNotificationKeyConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Ljava/lang/String;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 22
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 22
    check-cast p1, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;->convertRequest(Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 7
    .param p1, "request"    # Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 32
    :try_start_0
    new-instance v3, Lorg/json/JSONObject;

    invoke-direct {v3}, Lorg/json/JSONObject;-><init>()V

    .line 33
    .local v3, "requestBody":Lorg/json/JSONObject;
    const-string v4, "operation"

    const-string v5, "add"

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 34
    const-string v4, "notification_key_name"

    iget-object v5, p1, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->account:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 35
    const-string v4, "id_token"

    iget-object v5, p1, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->authToken:Ljava/lang/String;

    invoke-virtual {v3, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 36
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 37
    .local v2, "registrationIds":Lorg/json/JSONArray;
    iget-object v4, p1, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->registrationId:Ljava/lang/String;

    invoke-virtual {v2, v4}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 38
    const-string v4, "registration_ids"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 40
    const-string v4, "https://android.googleapis.com/gcm/googlenotification"

    invoke-virtual {v3}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v5

    const-string v6, "UTF-8"

    invoke-virtual {v5, v6}, Ljava/lang/String;->getBytes(Ljava/lang/String;)[B

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createPost(Ljava/lang/String;[B)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    .line 42
    .local v1, "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    const-string v4, "Content-Type"

    const-string v5, "application/json"

    invoke-interface {v1, v4, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 43
    const-string v4, "project_id"

    iget-object v5, p1, Lcom/google/android/videos/api/GcmCreateUserNotificationKeyRequest;->projectId:Ljava/lang/String;

    invoke-interface {v1, v4, v5}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/io/UnsupportedEncodingException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_1

    .line 44
    return-object v1

    .line 45
    .end local v1    # "httpUriRequest":Lorg/apache/http/client/methods/HttpUriRequest;
    .end local v2    # "registrationIds":Lorg/json/JSONArray;
    .end local v3    # "requestBody":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 46
    .local v0, "e":Ljava/io/UnsupportedEncodingException;
    new-instance v4, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v4, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v4

    .line 47
    .end local v0    # "e":Ljava/io/UnsupportedEncodingException;
    :catch_1
    move-exception v0

    .line 48
    .local v0, "e":Lorg/json/JSONException;
    new-instance v4, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v4, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v4
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 22
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/GcmCreateNotificationKeyConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/String;
    .locals 3
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 55
    :try_start_0
    new-instance v1, Lorg/json/JSONObject;

    invoke-static {p1}, Lorg/apache/http/util/EntityUtils;->toString(Lorg/apache/http/HttpEntity;)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 56
    .local v1, "json":Lorg/json/JSONObject;
    const-string v2, "notification_key"

    invoke-virtual {v1, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 57
    .end local v1    # "json":Lorg/json/JSONObject;
    :catch_0
    move-exception v0

    .line 58
    .local v0, "e":Ljava/lang/Exception;
    new-instance v2, Lcom/google/android/videos/converter/ConverterException;

    invoke-direct {v2, v0}, Lcom/google/android/videos/converter/ConverterException;-><init>(Ljava/lang/Throwable;)V

    throw v2
.end method
