.class Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "KnowledgeControlsAdapter.java"

# interfaces
.implements Landroid/view/View$OnFocusChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "CardPresenter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)V
    .locals 0

    .prologue
    .line 145
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
    .param p2, "x1"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;

    .prologue
    .line 145
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;-><init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 7
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 157
    move-object v0, p2

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 158
    .local v0, "entity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v3, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    .line 159
    .local v3, "view":Landroid/view/View;
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # invokes: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->getIndexOf(Lcom/google/android/videos/tagging/KnowledgeEntity;)I
    invoke-static {v4, v0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$300(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/tagging/KnowledgeEntity;)I

    move-result v2

    .line 161
    .local v2, "position":I
    iget-object v4, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    const v5, 0x7f0f00a5

    invoke-virtual {v4, v5}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 162
    .local v1, "imageView":Landroid/widget/ImageView;
    const/4 v4, 0x0

    invoke-virtual {v1, v4}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 163
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$500(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/tagging/KnowledgeBundle;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->recentActorsStartIndex:I
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$400(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)I

    move-result v4

    if-lt v2, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$200(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v5, v0, v4, v6, v3}, Lcom/google/android/videos/tagging/KnowledgeBundle;->initThumbnailItem(Lcom/google/android/videos/tagging/KnowledgeEntity;ZLandroid/app/Activity;Landroid/view/View;)V

    .line 165
    invoke-virtual {v3, p2}, Landroid/view/View;->setTag(Ljava/lang/Object;)V

    .line 166
    return-void

    .line 163
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 4
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 149
    iget-object v1, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$200(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Landroid/app/Activity;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/Activity;->getLayoutInflater()Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040081

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p1, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 151
    .local v0, "view":Landroid/view/View;
    invoke-virtual {v0, p0}, Landroid/view/View;->setOnFocusChangeListener(Landroid/view/View$OnFocusChangeListener;)V

    .line 152
    new-instance v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-direct {v1, v0}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public onFocusChange(Landroid/view/View;Z)V
    .locals 4
    .param p1, "view"    # Landroid/view/View;
    .param p2, "hasFocus"    # Z

    .prologue
    .line 174
    if-nez p2, :cond_0

    .line 175
    iget-object v2, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;
    invoke-static {v2}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$600(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    move-result-object v2

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;->onFocusKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 181
    :goto_0
    return-void

    .line 178
    :cond_0
    const v2, 0x7f0f019b

    invoke-virtual {p1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 179
    .local v1, "itemView":Landroid/view/View;
    invoke-virtual {v1}, Landroid/view/View;->getTag()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 180
    .local v0, "item":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v2, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$CardPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->listener:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;
    invoke-static {v2}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$600(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;

    move-result-object v2

    invoke-interface {v2, v0}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$Listener;->onFocusKnowledgeEntity(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    goto :goto_0
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 170
    return-void
.end method
