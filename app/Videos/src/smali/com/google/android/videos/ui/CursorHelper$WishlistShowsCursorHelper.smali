.class public final Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "WishlistShowsCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/ui/SyncHelper;)V
    .locals 0
    .param p1, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;

    .prologue
    .line 645
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;-><init>(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 646
    return-void
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "downloadedOnly"    # Z

    .prologue
    .line 650
    invoke-static {p1}, Lcom/google/android/videos/adapter/WishlistDataSource$Query;->createRequestForShows(Ljava/lang/String;)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Z

    .prologue
    .line 641
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$WishlistShowsCursorHelper;->createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    move-result-object v0

    return-object v0
.end method

.method public bridge synthetic onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 0
    .param p1, "x0"    # Ljava/util/List;

    .prologue
    .line 641
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->onMovieMetadataUpdated(Ljava/util/List;)V

    return-void
.end method

.method public bridge synthetic onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 641
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->onShowMetadataUpdated(Ljava/lang/String;)V

    return-void
.end method

.method public bridge synthetic onWishlistUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/String;

    .prologue
    .line 641
    invoke-super {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomeWishlistCursorHelper;->onWishlistUpdated(Ljava/lang/String;)V

    return-void
.end method
