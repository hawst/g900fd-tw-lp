.class public abstract Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;
.super Ljava/lang/Object;
.source "DelegatingMediaPlayer.java"

# interfaces
.implements Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;
.implements Lcom/google/android/videos/player/legacy/MediaPlayerInterface;


# instance fields
.field private final delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

.field private listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .line 29
    invoke-interface {p1, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V

    .line 30
    return-void
.end method


# virtual methods
.method public getAudioSessionId()I
    .locals 1

    .prologue
    .line 156
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getAudioSessionId()I

    move-result v0

    return v0
.end method

.method public getCurrentPosition()I
    .locals 1

    .prologue
    .line 65
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getCurrentPosition()I

    move-result v0

    return v0
.end method

.method public getDuration()I
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->getDuration()I

    move-result v0

    return v0
.end method

.method public isPlaying()Z
    .locals 1

    .prologue
    .line 105
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->isPlaying()Z

    move-result v0

    return v0
.end method

.method protected final notifyBufferingUpdate(I)V
    .locals 1
    .param p1, "percent"    # I

    .prologue
    .line 182
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 183
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V

    .line 185
    :cond_0
    return-void
.end method

.method protected final notifyCompletion()V
    .locals 1

    .prologue
    .line 170
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onCompletion(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 173
    :cond_0
    return-void
.end method

.method protected final notifyError(II)Z
    .locals 1
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    .line 166
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final notifyInfo(II)Z
    .locals 1
    .param p1, "what"    # I
    .param p2, "extra"    # I

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z

    move-result v0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final notifyPrepared()V
    .locals 1

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 197
    :cond_0
    return-void
.end method

.method protected final notifySeekComplete()V
    .locals 1

    .prologue
    .line 176
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V

    .line 179
    :cond_0
    return-void
.end method

.method protected final notifyVideoSizeChanged(II)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 188
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    if-eqz v0, :cond_0

    .line 189
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    invoke-interface {v0, p0, p1, p2}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;->onVideoSizeChanged(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)V

    .line 191
    :cond_0
    return-void
.end method

.method public onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "percent"    # I

    .prologue
    .line 141
    invoke-virtual {p0, p2}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyBufferingUpdate(I)V

    .line 142
    return-void
.end method

.method public onCompletion(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 131
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyCompletion()V

    .line 132
    return-void
.end method

.method public onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 126
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyError(II)Z

    move-result v0

    return v0
.end method

.method public onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
    .locals 1
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "what"    # I
    .param p3, "extra"    # I

    .prologue
    .line 121
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyInfo(II)Z

    move-result v0

    return v0
.end method

.method public onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 151
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyPrepared()V

    .line 152
    return-void
.end method

.method public onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    .prologue
    .line 136
    invoke-virtual {p0}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifySeekComplete()V

    .line 137
    return-void
.end method

.method public onVideoSizeChanged(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)V
    .locals 0
    .param p1, "delegate"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
    .param p2, "width"    # I
    .param p3, "height"    # I

    .prologue
    .line 146
    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->notifyVideoSizeChanged(II)V

    .line 147
    return-void
.end method

.method public pause()V
    .locals 1

    .prologue
    .line 50
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->pause()V

    .line 51
    return-void
.end method

.method public prepareAsync()V
    .locals 1

    .prologue
    .line 40
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->prepareAsync()V

    .line 41
    return-void
.end method

.method public release()V
    .locals 1

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->release()V

    .line 61
    return-void
.end method

.method public seekTo(I)V
    .locals 1
    .param p1, "millis"    # I

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->seekTo(I)V

    .line 76
    return-void
.end method

.method public setAudioStreamType(I)V
    .locals 1
    .param p1, "streamMusic"    # I

    .prologue
    .line 90
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setAudioStreamType(I)V

    .line 91
    return-void
.end method

.method public setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Landroid/net/Uri;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    .local p3, "headers":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setDataSource(Landroid/content/Context;Landroid/net/Uri;Ljava/util/Map;)V

    .line 36
    return-void
.end method

.method public setDisplay(Landroid/view/SurfaceHolder;)V
    .locals 1
    .param p1, "holder"    # Landroid/view/SurfaceHolder;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setDisplay(Landroid/view/SurfaceHolder;)V

    .line 96
    return-void
.end method

.method public setListener(Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .prologue
    .line 110
    iput-object p1, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->listener:Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;

    .line 111
    return-void
.end method

.method public setScreenOnWhilePlaying(Z)V
    .locals 1
    .param p1, "b"    # Z

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setScreenOnWhilePlaying(Z)V

    .line 86
    return-void
.end method

.method public setVideoScalingModeV16(I)V
    .locals 1
    .param p1, "mode"    # I

    .prologue
    .line 80
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0, p1}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->setVideoScalingModeV16(I)V

    .line 81
    return-void
.end method

.method public start()V
    .locals 1

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/videos/player/legacy/DelegatingMediaPlayer;->delegate:Lcom/google/android/videos/player/legacy/MediaPlayerInterface;

    invoke-interface {v0}, Lcom/google/android/videos/player/legacy/MediaPlayerInterface;->start()V

    .line 46
    return-void
.end method
