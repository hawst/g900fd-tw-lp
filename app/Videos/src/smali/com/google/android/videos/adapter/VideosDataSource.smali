.class public interface abstract Lcom/google/android/videos/adapter/VideosDataSource;
.super Ljava/lang/Object;
.source "VideosDataSource.java"

# interfaces
.implements Lcom/google/android/videos/adapter/DataSource;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/adapter/DataSource",
        "<",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# virtual methods
.method public abstract changeCursor(Landroid/database/Cursor;)V
.end method

.method public abstract getAccount(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;
.end method

.method public abstract getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;
.end method

.method public abstract getDurationSeconds(Landroid/database/Cursor;)I
.end method

.method public abstract getExpirationTimestamp(Landroid/database/Cursor;)J
.end method

.method public abstract getHaveLicense(Landroid/database/Cursor;)Z
.end method

.method public abstract getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;
.end method

.method public abstract getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
.end method

.method public abstract getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
.end method

.method public abstract getResumeTimestamp(Landroid/database/Cursor;)I
.end method

.method public abstract getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getShowId(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getTitle(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
.end method

.method public abstract isActive(Landroid/database/Cursor;)Z
.end method

.method public abstract isPinned(Landroid/database/Cursor;)Z
.end method

.method public abstract isRental(Landroid/database/Cursor;)Z
.end method

.method public abstract setNetworkConnected(Z)V
.end method
