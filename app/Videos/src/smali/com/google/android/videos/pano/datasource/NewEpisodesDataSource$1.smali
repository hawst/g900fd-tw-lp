.class Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "NewEpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;)V
    .locals 0

    .prologue
    .line 29
    iput-object p1, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;

    invoke-direct {p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 38
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->scheduleUpdate(Z)V

    .line 39
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 33
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->scheduleUpdate(Z)V

    .line 34
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 43
    iget-object v0, p0, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource$1;->this$0:Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/datasource/NewEpisodesDataSource;->scheduleUpdate(Z)V

    .line 44
    return-void
.end method
