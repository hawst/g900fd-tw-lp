.class Lcom/google/android/videos/logging/EventProtoCache$ElementCache;
.super Ljava/lang/Object;
.source "EventProtoCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/logging/EventProtoCache;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ElementCache"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<T:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final cache:[Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "[TT;"
        }
    .end annotation
.end field

.field clazz:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<*>;"
        }
    .end annotation
.end field

.field private count:I

.field private highWater:I

.field private final limit:I


# direct methods
.method public constructor <init>(Ljava/lang/Class;I)V
    .locals 1
    .param p2, "limit"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/Class",
            "<*>;I)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/logging/EventProtoCache$ElementCache;, "Lcom/google/android/videos/logging/EventProtoCache$ElementCache<TT;>;"
    .local p1, "clazz":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v0, 0x0

    .line 111
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 112
    iput p2, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->limit:I

    .line 113
    iput-object p1, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->clazz:Ljava/lang/Class;

    .line 114
    iput v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    .line 115
    iput v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->highWater:I

    .line 116
    invoke-static {p1, p2}, Ljava/lang/reflect/Array;->newInstance(Ljava/lang/Class;I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Ljava/lang/Object;

    check-cast v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->cache:[Ljava/lang/Object;

    .line 117
    return-void
.end method


# virtual methods
.method public obtain()Ljava/lang/Object;
    .locals 5
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()TT;"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/logging/EventProtoCache$ElementCache;, "Lcom/google/android/videos/logging/EventProtoCache$ElementCache<TT;>;"
    const/4 v2, 0x0

    .line 122
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 123
    iget v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    if-lez v3, :cond_0

    .line 127
    iget v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    add-int/lit8 v3, v3, -0x1

    iput v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    .line 128
    iget-object v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->cache:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    aget-object v1, v3, v4

    .line 129
    .local v1, "result":Ljava/lang/Object;, "TT;"
    iget-object v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->cache:[Ljava/lang/Object;

    iget v4, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    aput-object v2, v3, v4

    .line 137
    .end local v1    # "result":Ljava/lang/Object;, "TT;"
    :goto_0
    return-object v1

    .line 134
    :cond_0
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->clazz:Ljava/lang/Class;

    invoke-virtual {v3}, Ljava/lang/Class;->newInstance()Ljava/lang/Object;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    goto :goto_0

    .line 135
    :catch_0
    move-exception v0

    .line 136
    .local v0, "e":Ljava/lang/Exception;
    const-string v3, "Exception from mClazz.newInstance "

    invoke-static {v3, v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    move-object v1, v2

    .line 137
    goto :goto_0
.end method

.method public recycle(Ljava/lang/Object;)V
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TT;)V"
        }
    .end annotation

    .prologue
    .line 143
    .local p0, "this":Lcom/google/android/videos/logging/EventProtoCache$ElementCache;, "Lcom/google/android/videos/logging/EventProtoCache$ElementCache<TT;>;"
    .local p1, "element":Ljava/lang/Object;, "TT;"
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 154
    iget v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    iget v1, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->limit:I

    if-ge v0, v1, :cond_0

    .line 155
    iget-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->cache:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    aput-object p1, v0, v1

    .line 156
    iget v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    add-int/lit8 v0, v0, 0x1

    iput v0, p0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->count:I

    .line 168
    :cond_0
    return-void
.end method
