.class public final Lcom/google/android/videos/pano/ui/MovieCardView;
.super Landroid/widget/LinearLayout;
.source "MovieCardView.java"


# instance fields
.field private badgeView:Landroid/widget/ImageView;

.field private imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

.field private priceView:Lcom/google/android/play/layout/PlayCardLabelView;

.field private progressBar:Landroid/view/View;

.field private ratingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private subtitleView:Landroid/widget/TextView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 40
    const v0, 0x7f010104

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 41
    return-void
.end method


# virtual methods
.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 118
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 56
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 45
    const v0, 0x7f0f0156

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/ui/RequestingImageView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    .line 46
    const v0, 0x7f0f012e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->titleView:Landroid/widget/TextView;

    .line 47
    const v0, 0x7f0f0198

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0f0196

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/View;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->progressBar:Landroid/view/View;

    .line 49
    const v0, 0x7f0f0138

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 50
    const v0, 0x7f0f00d3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 51
    const v0, 0x7f0f0199

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/MovieCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->badgeView:Landroid/widget/ImageView;

    .line 52
    return-void
.end method

.method public setExtra(Ljava/lang/String;IILcom/google/android/videos/utils/OfferUtil$CheapestOffer;F)V
    .locals 8
    .param p1, "subtitle"    # Ljava/lang/String;
    .param p2, "badgeResourceId"    # I
    .param p3, "durationInSeconds"    # I
    .param p4, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p5, "starRating"    # F

    .prologue
    const v4, 0x7f0a0054

    const/4 v1, 0x0

    const/16 v7, 0x8

    .line 75
    if-eqz p1, :cond_2

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 85
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->badgeView:Landroid/widget/ImageView;

    if-eqz p2, :cond_4

    move v0, v1

    :goto_1
    invoke-virtual {v2, v0}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 86
    if-eqz p2, :cond_0

    .line 87
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->badgeView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 90
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v0, 0x0

    cmpl-float v0, p5, v0

    if-lez v0, :cond_5

    move v0, v1

    :goto_2
    invoke-virtual {v2, v0}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 91
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, p5}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 93
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    if-eqz p4, :cond_6

    :goto_3
    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 94
    if-eqz p4, :cond_1

    .line 95
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/MovieCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 96
    .local v6, "resources":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/MovieCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->getFormattedAmount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p4, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 100
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 103
    iget-object v0, p4, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 104
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v7}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 107
    .end local v6    # "resources":Landroid/content/res/Resources;
    :cond_1
    return-void

    .line 78
    :cond_2
    if-lez p3, :cond_3

    .line 79
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/MovieCardView;->getContext()Landroid/content/Context;

    move-result-object v2

    invoke-static {p3, v2}, Lcom/google/android/videos/pano/ui/PanoHelper;->formatVideoDuration(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_0

    .line 82
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    goto :goto_0

    :cond_4
    move v0, v7

    .line 85
    goto :goto_1

    :cond_5
    move v0, v7

    .line 90
    goto :goto_2

    :cond_6
    move v1, v7

    .line 93
    goto :goto_3
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 60
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageResource(I)V

    .line 61
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 66
    return-void
.end method

.method public setProgress(F)V
    .locals 2
    .param p1, "progress"    # F

    .prologue
    .line 110
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 112
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    const/4 v1, 0x0

    iput v1, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 113
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 114
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 115
    return-void
.end method

.method public setTitle(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 69
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/MovieCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    return-void
.end method
