.class public final Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "ShowEpisodesCursorHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/CursorHelper",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        ">;",
        "Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field private final allowUnpurchased:Z

.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

.field private final showId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 1
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "account"    # Ljava/lang/String;
    .param p7, "showId"    # Ljava/lang/String;
    .param p8, "allowUnpurchased"    # Z
    .param p9, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 547
    invoke-direct/range {p0 .. p5}, Lcom/google/android/videos/ui/CursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;)V

    .line 548
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->account:Ljava/lang/String;

    .line 549
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->showId:Ljava/lang/String;

    .line 550
    iput-boolean p8, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->allowUnpurchased:Z

    .line 551
    iput-object p9, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 552
    return-void
.end method

.method public static create(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/DownloadedOnlyManager;)Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;
    .locals 10
    .param p0, "activity"    # Landroid/app/Activity;
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "uiHandler"    # Landroid/os/Handler;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p5, "account"    # Ljava/lang/String;
    .param p6, "showId"    # Ljava/lang/String;
    .param p7, "allowUnpurchased"    # Z
    .param p8, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 535
    new-instance v0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Ljava/lang/String;Ljava/lang/String;ZLcom/google/android/videos/utils/DownloadedOnlyManager;)V

    return-object v0
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "downloadedOnly"    # Z

    .prologue
    .line 575
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->showId:Ljava/lang/String;

    if-nez v0, :cond_0

    .line 576
    const/4 v0, 0x0

    .line 578
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->showId:Ljava/lang/String;

    iget-boolean v1, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->allowUnpurchased:Z

    invoke-static {p1, v0, v1, p2}, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForSingleShow;->createRequest(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    goto :goto_0
.end method

.method protected bridge synthetic createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Z

    .prologue
    .line 529
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 570
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->setDownloadedOnly(Z)V

    .line 571
    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 584
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 585
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 589
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->showId:Ljava/lang/String;

    invoke-static {p1, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 590
    invoke-virtual {p0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->scheduleUpdate()V

    .line 592
    :cond_0
    return-void
.end method

.method public onShowUserAssetsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 596
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->showId:Ljava/lang/String;

    invoke-static {p2, v0}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 597
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 599
    :cond_0
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 556
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 557
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 558
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->account:Ljava/lang/String;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->onAccount(Ljava/lang/String;)V

    .line 559
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->onDownloadedOnlyChanged(Z)V

    .line 560
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 564
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 565
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$ShowEpisodesCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 566
    return-void
.end method
