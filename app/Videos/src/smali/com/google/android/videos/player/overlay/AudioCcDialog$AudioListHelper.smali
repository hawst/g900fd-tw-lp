.class Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;
.super Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;
.source "AudioCcDialog.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/AudioCcDialog;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "AudioListHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/overlay/AudioCcDialog;Landroid/content/Context;Ljava/util/ArrayList;)V
    .locals 0
    .param p2, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 220
    .local p3, "audioInfos":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;->this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;

    .line 221
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/player/overlay/AudioCcDialog$ListHelper;-><init>(Landroid/content/Context;Ljava/util/ArrayList;)V

    .line 222
    return-void
.end method


# virtual methods
.method public getView(ILandroid/view/View;Landroid/view/ViewGroup;)Landroid/view/View;
    .locals 4
    .param p1, "position"    # I
    .param p2, "convertView"    # Landroid/view/View;
    .param p3, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 226
    move-object v0, p2

    check-cast v0, Landroid/widget/TextView;

    .line 227
    .local v0, "view":Landroid/widget/TextView;
    if-nez v0, :cond_0

    .line 228
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x109000f

    const/4 v3, 0x0

    invoke-virtual {v1, v2, p3, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .end local v0    # "view":Landroid/widget/TextView;
    check-cast v0, Landroid/widget/TextView;

    .line 230
    .restart local v0    # "view":Landroid/widget/TextView;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;->adjustItemPadding(Landroid/view/View;)V

    .line 233
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;->getItem(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Util;->getLanguageName(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 234
    return-object v0
.end method

.method public onItemClick(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 2
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 239
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/AudioCcDialog$AudioListHelper;->this$0:Lcom/google/android/videos/player/overlay/AudioCcDialog;

    invoke-virtual {v1}, Lcom/google/android/videos/player/overlay/AudioCcDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    .line 240
    .local v0, "activity":Landroid/app/Activity;
    if-eqz v0, :cond_0

    .line 241
    check-cast v0, Lcom/google/android/videos/player/overlay/TrackChangeListener;

    .end local v0    # "activity":Landroid/app/Activity;
    invoke-interface {v0, p3}, Lcom/google/android/videos/player/overlay/TrackChangeListener;->onSelectAudioTrackIndex(I)V

    .line 243
    :cond_0
    return-void
.end method
