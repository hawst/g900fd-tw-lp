.class final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AssetResourceBatchingRequester"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# instance fields
.field private final apiAssetsCachingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 202
    .local p1, "apiAssetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 203
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsCachingRequester:Lcom/google/android/videos/async/Requester;

    .line 204
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;

    .prologue
    .line 196
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->apiAssetsCachingRequester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 209
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;

    invoke-direct {v0, p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/async/Callback;)V

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester$Worker;->requestNextBatch()V

    .line 210
    return-void
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 196
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourceBatchingRequester;->request(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$AssetResourcesRequest;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method
