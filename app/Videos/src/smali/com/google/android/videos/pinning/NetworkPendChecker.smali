.class public Lcom/google/android/videos/pinning/NetworkPendChecker;
.super Ljava/lang/Object;
.source "NetworkPendChecker.java"


# direct methods
.method public static pendDueToConnection(Landroid/content/Context;Lcom/google/android/videos/pinning/InternetConnectionChecker;)Z
    .locals 1
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "connectionChecker"    # Lcom/google/android/videos/pinning/InternetConnectionChecker;

    .prologue
    .line 23
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    if-eqz v0, :cond_0

    if-eqz p1, :cond_1

    invoke-virtual {p1}, Lcom/google/android/videos/pinning/InternetConnectionChecker;->haveInternetConnection()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static pendDueToWifi(Landroid/content/Context;)Z
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 28
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isWiFiNetwork()Z

    move-result v0

    .line 29
    .local v0, "haveWifi":Z
    if-nez v0, :cond_0

    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-static {p0, v1}, Lcom/google/android/videos/utils/SettingsUtil;->getDownloadOnWiFiOnly(Landroid/content/Context;Landroid/content/SharedPreferences;)Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    return v1

    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
