.class final Lcom/google/android/videos/logging/PlayAnalyticsClient;
.super Ljava/lang/Object;
.source "PlayAnalyticsClient.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/logging/AnalyticsClient;


# instance fields
.field private accountEventLogger:Lcom/google/android/play/analytics/EventLogger;

.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

.field private final androidId:J

.field private final applicationVersion:Ljava/lang/String;

.field private final configuration:Lcom/google/android/play/analytics/EventLogger$Configuration;

.field private final context:Landroid/content/Context;

.field private final country:Ljava/lang/String;

.field private final eventLoggers:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Landroid/accounts/Account;",
            "Lcom/google/android/play/analytics/EventLogger;",
            ">;"
        }
    .end annotation
.end field

.field private final mccMnc:Ljava/lang/String;

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final userAgent:Ljava/lang/String;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/accounts/AccountManagerWrapper;Lcom/google/android/videos/accounts/SignInManager;Landroid/telephony/TelephonyManager;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p3, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p4, "telephonyManager"    # Landroid/telephony/TelephonyManager;
    .param p5, "applicationVersion"    # Ljava/lang/String;
    .param p6, "userAgent"    # Ljava/lang/String;
    .param p7, "experiments"    # [Ljava/lang/String;
    .param p8, "androidId"    # J
    .param p10, "country"    # Ljava/lang/String;
    .param p11, "serverUrl"    # Ljava/lang/String;

    .prologue
    .line 59
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 60
    invoke-virtual {p1}, Landroid/content/Context;->getApplicationContext()Landroid/content/Context;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->context:Landroid/content/Context;

    .line 61
    iput-object p2, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 62
    iput-object p3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 63
    invoke-virtual {p4}, Landroid/telephony/TelephonyManager;->getSimOperator()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->mccMnc:Ljava/lang/String;

    .line 64
    iput-object p5, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->applicationVersion:Ljava/lang/String;

    .line 65
    iput-object p6, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->userAgent:Ljava/lang/String;

    .line 66
    iput-wide p8, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->androidId:J

    .line 67
    iput-object p10, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->country:Ljava/lang/String;

    .line 68
    new-instance v0, Lcom/google/android/play/analytics/EventLogger$Configuration;

    invoke-direct {v0}, Lcom/google/android/play/analytics/EventLogger$Configuration;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->configuration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->configuration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    iput-object p11, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->mServerUrl:Ljava/lang/String;

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->configuration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    const-wide/16 v2, 0x6000

    iput-wide v2, v0, Lcom/google/android/play/analytics/EventLogger$Configuration;->recommendedLogFileSize:J

    .line 72
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->eventLoggers:Ljava/util/HashMap;

    .line 73
    invoke-virtual {p3, p0}, Lcom/google/android/videos/accounts/SignInManager;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 74
    invoke-virtual {p0}, Lcom/google/android/videos/logging/PlayAnalyticsClient;->update()V

    .line 76
    if-eqz p7, :cond_0

    array-length v0, p7

    if-eqz v0, :cond_0

    .line 77
    new-instance v0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-direct {v0}, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    .line 78
    iget-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    iput-object p7, v0, Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;->clientAlteringExperiment:[Ljava/lang/String;

    .line 82
    :goto_0
    return-void

    .line 80
    :cond_0
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    goto :goto_0
.end method

.method private getEventLogger(Landroid/accounts/Account;)Lcom/google/android/play/analytics/EventLogger;
    .locals 13
    .param p1, "account"    # Landroid/accounts/Account;

    .prologue
    .line 131
    iget-object v1, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->eventLoggers:Ljava/util/HashMap;

    invoke-virtual {v1, p1}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/analytics/EventLogger;

    .line 132
    .local v0, "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    if-nez v0, :cond_0

    .line 133
    new-instance v0, Lcom/google/android/play/analytics/EventLogger;

    .end local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    iget-object v1, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->context:Landroid/content/Context;

    invoke-direct {p0}, Lcom/google/android/videos/logging/PlayAnalyticsClient;->getLoggingId()Ljava/lang/String;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getScope()Ljava/lang/String;

    move-result-object v3

    sget-object v4, Lcom/google/android/play/analytics/EventLogger$LogSource;->VIDEO:Lcom/google/android/play/analytics/EventLogger$LogSource;

    iget-object v5, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->userAgent:Ljava/lang/String;

    iget-wide v6, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->androidId:J

    iget-object v8, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->applicationVersion:Ljava/lang/String;

    iget-object v9, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->mccMnc:Ljava/lang/String;

    iget-object v10, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->country:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->configuration:Lcom/google/android/play/analytics/EventLogger$Configuration;

    move-object v12, p1

    invoke-direct/range {v0 .. v12}, Lcom/google/android/play/analytics/EventLogger;-><init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$LogSource;Ljava/lang/String;JLjava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/play/analytics/EventLogger$Configuration;Landroid/accounts/Account;)V

    .line 136
    .restart local v0    # "eventLogger":Lcom/google/android/play/analytics/EventLogger;
    iget-object v1, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->eventLoggers:Ljava/util/HashMap;

    invoke-virtual {v1, p1, v0}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 139
    :cond_0
    return-object v0
.end method

.method private getLoggingId()Ljava/lang/String;
    .locals 4

    .prologue
    .line 144
    iget-object v2, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->context:Landroid/content/Context;

    invoke-static {v2}, Landroid/preference/PreferenceManager;->getDefaultSharedPreferences(Landroid/content/Context;)Landroid/content/SharedPreferences;

    move-result-object v1

    .line 145
    .local v1, "prefs":Landroid/content/SharedPreferences;
    const-string v2, "ANALYTICS_LOGGING_ID_KEY"

    const/4 v3, 0x0

    invoke-interface {v1, v2, v3}, Landroid/content/SharedPreferences;->getString(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 146
    .local v0, "loggingId":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 147
    new-instance v2, Ljava/security/SecureRandom;

    invoke-direct {v2}, Ljava/security/SecureRandom;-><init>()V

    invoke-virtual {v2}, Ljava/security/SecureRandom;->nextLong()J

    move-result-wide v2

    invoke-static {v2, v3}, Ljava/lang/Long;->toHexString(J)Ljava/lang/String;

    move-result-object v0

    .line 148
    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    const-string v3, "ANALYTICS_LOGGING_ID_KEY"

    invoke-interface {v2, v3, v0}, Landroid/content/SharedPreferences$Editor;->putString(Ljava/lang/String;Ljava/lang/String;)Landroid/content/SharedPreferences$Editor;

    move-result-object v2

    invoke-interface {v2}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 150
    :cond_0
    return-object v0
.end method


# virtual methods
.method public trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;)V
    .locals 1
    .param p1, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;

    .prologue
    .line 86
    const/4 v0, 0x0

    check-cast v0, [Ljava/lang/String;

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/logging/PlayAnalyticsClient;->trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;[Ljava/lang/String;)V

    .line 87
    return-void
.end method

.method public varargs trackEvent(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;[Ljava/lang/String;)V
    .locals 1
    .param p1, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .param p2, "extras"    # [Ljava/lang/String;

    .prologue
    .line 91
    const-string v0, "PlayMoviesLogEvent"

    invoke-virtual {p0, v0, p1, p2}, Lcom/google/android/videos/logging/PlayAnalyticsClient;->trackEvent(Ljava/lang/String;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;[Ljava/lang/String;)V

    .line 92
    return-void
.end method

.method public varargs trackEvent(Ljava/lang/String;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;[Ljava/lang/String;)V
    .locals 4
    .param p1, "tag"    # Ljava/lang/String;
    .param p2, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;
    .param p3, "extras"    # [Ljava/lang/String;

    .prologue
    .line 101
    iget-object v2, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountEventLogger:Lcom/google/android/play/analytics/EventLogger;

    if-eqz v2, :cond_0

    invoke-static {}, Landroid/app/ActivityManager;->isUserAMonkey()Z

    move-result v2

    if-eqz v2, :cond_1

    .line 115
    :cond_0
    :goto_0
    return-void

    .line 106
    :cond_1
    if-nez p2, :cond_2

    .line 107
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountEventLogger:Lcom/google/android/play/analytics/EventLogger;

    iget-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v2, p1, v3, p3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[Ljava/lang/String;)V
    :try_end_0
    .catch Ljava/lang/RuntimeException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 112
    :catch_0
    move-exception v0

    .line 113
    .local v0, "e":Ljava/lang/RuntimeException;
    const-string v2, "Failed to log event"

    invoke-static {v2, v0}, Landroid/util/Log;->w(Ljava/lang/String;Ljava/lang/Throwable;)I

    goto :goto_0

    .line 109
    .end local v0    # "e":Ljava/lang/RuntimeException;
    :cond_2
    :try_start_1
    invoke-static {p2}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    .line 110
    .local v1, "protoData":[B
    iget-object v2, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountEventLogger:Lcom/google/android/play/analytics/EventLogger;

    iget-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->activeExperiments:Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;

    invoke-virtual {v2, p1, v3, v1, p3}, Lcom/google/android/play/analytics/EventLogger;->logEvent(Ljava/lang/String;Lcom/google/android/play/analytics/ClientAnalytics$ActiveExperiments;[B[Ljava/lang/String;)V
    :try_end_1
    .catch Ljava/lang/RuntimeException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_0
.end method

.method public update()V
    .locals 4

    .prologue
    .line 119
    iget-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v1

    .line 120
    .local v1, "accountName":Ljava/lang/String;
    if-nez v1, :cond_0

    const/4 v0, 0x0

    .line 123
    .local v0, "account":Landroid/accounts/Account;
    :goto_0
    :try_start_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/logging/PlayAnalyticsClient;->getEventLogger(Landroid/accounts/Account;)Lcom/google/android/play/analytics/EventLogger;

    move-result-object v3

    iput-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountEventLogger:Lcom/google/android/play/analytics/EventLogger;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 128
    :goto_1
    return-void

    .line 120
    .end local v0    # "account":Landroid/accounts/Account;
    :cond_0
    new-instance v0, Landroid/accounts/Account;

    iget-object v3, p0, Lcom/google/android/videos/logging/PlayAnalyticsClient;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccountType()Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v3}, Landroid/accounts/Account;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 124
    .restart local v0    # "account":Landroid/accounts/Account;
    :catch_0
    move-exception v2

    .line 126
    .local v2, "e":Ljava/lang/Exception;
    const-string v3, "Failed to initialize eventLogger"

    invoke-static {v3, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method
