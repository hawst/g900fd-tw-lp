.class public Lcom/google/android/videos/logging/EventProtoCache;
.super Ljava/lang/Object;
.source "EventProtoCache.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/logging/EventProtoCache$ElementCache;
    }
.end annotation


# instance fields
.field private final cacheClickEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/logging/EventProtoCache$ElementCache",
            "<",
            "Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheImpressionEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/logging/EventProtoCache$ElementCache",
            "<",
            "Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;",
            ">;"
        }
    .end annotation
.end field

.field private final cacheUiElement:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/logging/EventProtoCache$ElementCache",
            "<",
            "Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 3

    .prologue
    const/16 v2, 0xa

    .line 44
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 45
    new-instance v0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    const-class v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheImpressionEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    .line 47
    new-instance v0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    const-class v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheClickEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    .line 49
    new-instance v0, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    const-class v1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    const/16 v2, 0x32

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;-><init>(Ljava/lang/Class;I)V

    iput-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheUiElement:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    .line 51
    return-void
.end method


# virtual methods
.method public obtainClickEvent()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;
    .locals 1

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheClickEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    return-object v0
.end method

.method public obtainImpressionEvent()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;
    .locals 1

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheImpressionEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    return-object v0
.end method

.method public obtainUiElement()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    .locals 1

    .prologue
    .line 85
    iget-object v0, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheUiElement:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v0}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->obtain()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    return-object v0
.end method

.method public recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .prologue
    .line 77
    iget-object v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;->elementPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .local v0, "arr$":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 78
    .local v1, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-virtual {p0, v1}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 77
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 80
    .end local v1    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesClickEvent;

    .line 81
    iget-object v4, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheClickEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    .line 82
    return-void
.end method

.method public recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;)V
    .locals 5
    .param p1, "event"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .prologue
    .line 61
    iget-object v4, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    if-eqz v4, :cond_0

    iget-object v4, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->tree:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    invoke-virtual {p0, v4}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 62
    :cond_0
    iget-object v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->referrerPath:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .local v0, "arr$":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_1

    aget-object v1, v0, v2

    .line 63
    .local v1, "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-virtual {p0, v1}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 62
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 65
    .end local v1    # "element":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_1
    invoke-virtual {p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesImpressionEvent;

    .line 66
    iget-object v4, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheImpressionEvent:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    .line 67
    return-void
.end method

.method public recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V
    .locals 5
    .param p1, "element"    # Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .prologue
    .line 92
    iget-object v0, p1, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->child:[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .local v0, "arr$":[Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v1, v0, v2

    .line 93
    .local v1, "child":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    invoke-virtual {p0, v1}, Lcom/google/android/videos/logging/EventProtoCache;->recycle(Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;)V

    .line 92
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 95
    .end local v1    # "child":Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;
    :cond_0
    invoke-virtual {p1}, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->clear()Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    .line 96
    iget-object v4, p0, Lcom/google/android/videos/logging/EventProtoCache;->cacheUiElement:Lcom/google/android/videos/logging/EventProtoCache$ElementCache;

    invoke-virtual {v4, p1}, Lcom/google/android/videos/logging/EventProtoCache$ElementCache;->recycle(Ljava/lang/Object;)V

    .line 97
    return-void
.end method
