.class final Lcom/google/android/videos/api/PromotionsConverter;
.super Lcom/google/android/videos/converter/HttpResponseConverter;
.source "PromotionsConverter.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/converter/HttpResponseConverter",
        "<",
        "Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;",
        ">;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/api/PromotionsRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# instance fields
.field private final baseUri:Ljava/lang/String;

.field private final byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;


# direct methods
.method public constructor <init>(Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArrayPool;)V
    .locals 2
    .param p1, "baseUri"    # Landroid/net/Uri;
    .param p2, "byteArrayPool"    # Lcom/google/android/videos/utils/ByteArrayPool;

    .prologue
    .line 39
    invoke-direct {p0}, Lcom/google/android/videos/converter/HttpResponseConverter;-><init>()V

    .line 40
    new-instance v0, Lcom/google/android/videos/utils/UriBuilder;

    invoke-virtual {p1}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;-><init>(Ljava/lang/String;)V

    const-string v1, "promotion"

    invoke-virtual {v0, v1}, Lcom/google/android/videos/utils/UriBuilder;->addSegment(Ljava/lang/String;)Lcom/google/android/videos/utils/UriBuilder;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/utils/UriBuilder;->build()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/api/PromotionsConverter;->baseUri:Ljava/lang/String;

    .line 41
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/utils/ByteArrayPool;

    iput-object v0, p0, Lcom/google/android/videos/api/PromotionsConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    .line 42
    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 24
    check-cast p1, Lcom/google/android/videos/api/PromotionsRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/PromotionsConverter;->convertRequest(Lcom/google/android/videos/api/PromotionsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/api/PromotionsRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/api/PromotionsRequest;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 47
    iget-object v1, p0, Lcom/google/android/videos/api/PromotionsConverter;->baseUri:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/api/ApiUriBuilder;->create(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "list"

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->appendEncodedSegment(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/api/PromotionsRequest;->country:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictCountry(Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    iget-object v2, p1, Lcom/google/android/videos/api/PromotionsRequest;->locale:Ljava/util/Locale;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/api/ApiUriBuilder;->restrictLocale(Ljava/util/Locale;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "make"

    sget-object v3, Landroid/os/Build;->MANUFACTURER:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "model"

    sget-object v3, Landroid/os/Build;->MODEL:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "product"

    sget-object v3, Landroid/os/Build;->PRODUCT:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v1

    const-string v2, "device"

    sget-object v3, Landroid/os/Build;->DEVICE:Ljava/lang/String;

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/api/ApiUriBuilder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/api/ApiUriBuilder;

    move-result-object v0

    .line 55
    .local v0, "uriBuilder":Lcom/google/android/videos/api/ApiUriBuilder;
    invoke-virtual {v0}, Lcom/google/android/videos/api/ApiUriBuilder;->build()Landroid/net/Uri;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v1

    return-object v1
.end method

.method public convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;
    .locals 2
    .param p1, "entity"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;,
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 61
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;-><init>()V

    iget-object v1, p0, Lcom/google/android/videos/api/PromotionsConverter;->byteArrayPool:Lcom/google/android/videos/utils/ByteArrayPool;

    invoke-static {v0, p1, v1}, Lcom/google/android/videos/utils/EntityUtils;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;Lorg/apache/http/HttpEntity;Lcom/google/android/videos/utils/ByteArrayPool;)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    check-cast v0, Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    return-object v0
.end method

.method public bridge synthetic convertResponseEntity(Lorg/apache/http/HttpEntity;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Lorg/apache/http/HttpEntity;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;,
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 24
    invoke-virtual {p0, p1}, Lcom/google/android/videos/api/PromotionsConverter;->convertResponseEntity(Lorg/apache/http/HttpEntity;)Lcom/google/wireless/android/video/magma/proto/PromotionListResponse;

    move-result-object v0

    return-object v0
.end method
