.class public Lcom/google/android/videos/adapter/MoviesDataSource;
.super Lcom/google/android/videos/adapter/CursorDataSource;
.source "MoviesDataSource.java"

# interfaces
.implements Lcom/google/android/videos/adapter/VideosDataSource;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/MoviesDataSource$Query;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 26
    invoke-direct {p0}, Lcom/google/android/videos/adapter/CursorDataSource;-><init>()V

    .line 177
    return-void
.end method


# virtual methods
.method public final getAccount(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 79
    const/16 v0, 0x11

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 99
    const/16 v0, 0xb

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 94
    const/16 v0, 0xc

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getLongOrNull(Landroid/database/Cursor;I)Ljava/lang/Long;

    move-result-object v0

    return-object v0
.end method

.method public final getDurationSeconds(Landroid/database/Cursor;)I
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 49
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v0

    return v0
.end method

.method public final getExpirationTimestamp(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 84
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getHaveLicense(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 35
    const/4 v0, 0x7

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public getItemIdentifier(I)Ljava/lang/Object;
    .locals 1
    .param p1, "position"    # I

    .prologue
    .line 136
    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromMovieId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getLastActivityTimestamp(Landroid/database/Cursor;)J
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 131
    const/16 v0, 0x10

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v0

    return-wide v0
.end method

.method public final getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 89
    const/16 v0, 0xa

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 54
    const/16 v0, 0x8

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 59
    const/16 v0, 0x9

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getPosterUri(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 123
    const/16 v0, 0xe

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getReleaseYear(Landroid/database/Cursor;)Ljava/lang/Integer;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 44
    const/4 v0, 0x4

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getIntOrNull(Landroid/database/Cursor;I)Ljava/lang/Integer;

    move-result-object v0

    return-object v0
.end method

.method public final getResumeTimestamp(Landroid/database/Cursor;)I
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 104
    const/16 v0, 0xd

    const/4 v1, 0x0

    invoke-static {p1, v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getIntOrDefault(Landroid/database/Cursor;II)I

    move-result v0

    return v0
.end method

.method public final getScreenshotUri(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 127
    const/16 v0, 0xf

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public final getSeasonId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 109
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getShowId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 114
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getShowTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 119
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getTitle(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 40
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;
    .locals 8
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 163
    new-instance v0, Lcom/google/android/videos/store/VideoDownloadStatus;

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v1

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getHaveLicense(Landroid/database/Cursor;)Z

    move-result v2

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v4

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v6

    invoke-virtual {p0, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getBytesDownloaded(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v7

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/store/VideoDownloadStatus;-><init>(ZZLjava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Integer;Ljava/lang/Long;Ljava/lang/Long;)V

    return-object v0
.end method

.method public final getVideoId(Landroid/database/Cursor;)Ljava/lang/String;
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 74
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;
    .locals 18
    .param p1, "cursor"    # Landroid/database/Cursor;
    .param p2, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 140
    const/16 v2, 0x18

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getInt(I)I

    move-result v2

    const/4 v3, 0x2

    if-ne v2, v3, :cond_0

    const/4 v14, 0x1

    .line 141
    .local v14, "purchasedHd":Z
    :goto_0
    new-instance v2, Lcom/google/android/videos/store/VideoMetadata;

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    const/16 v5, 0x12

    move-object/from16 v0, p1

    invoke-interface {v0, v5}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getReleaseYear(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/Integer;->intValue()I

    move-result v6

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getDurationSeconds(Landroid/database/Cursor;)I

    move-result v7

    const/16 v8, 0x14

    move-object/from16 v0, p1

    invoke-static {v0, v8}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v8

    const/16 v9, 0x13

    move-object/from16 v0, p1

    invoke-static {v0, v9}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v9

    const/16 v10, 0x15

    move-object/from16 v0, p1

    invoke-static {v0, v10}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v10

    const/16 v11, 0x16

    move-object/from16 v0, p1

    invoke-static {v0, v11}, Lcom/google/android/videos/utils/DbUtils;->getStringList(Landroid/database/Cursor;I)Ljava/util/List;

    move-result-object v11

    const/16 v12, 0x17

    move-object/from16 v0, p1

    invoke-interface {v0, v12}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v12

    invoke-virtual/range {p0 .. p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v13

    if-eqz v14, :cond_1

    const/16 v15, 0x19

    move-object/from16 v0, p1

    invoke-static {v0, v15}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v15

    if-eqz v15, :cond_1

    invoke-interface/range {p2 .. p2}, Lcom/google/android/videos/Config;->allowSurroundSoundFormats()Z

    move-result v15

    if-eqz v15, :cond_1

    const/4 v15, 0x1

    :goto_1
    invoke-interface/range {p2 .. p2}, Lcom/google/android/videos/Config;->knowledgeEnabled()Z

    move-result v16

    if-eqz v16, :cond_2

    const/16 v16, 0x1a

    move-object/from16 v0, p1

    move/from16 v1, v16

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v16

    if-eqz v16, :cond_2

    const/16 v16, 0x1

    :goto_2
    const/16 v17, 0x1b

    move-object/from16 v0, p1

    move/from16 v1, v17

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v17

    invoke-direct/range {v2 .. v17}, Lcom/google/android/videos/store/VideoMetadata;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILjava/util/List;Ljava/util/List;Ljava/util/List;Ljava/util/List;Ljava/lang/String;ZZZZZ)V

    return-object v2

    .line 140
    .end local v14    # "purchasedHd":Z
    :cond_0
    const/4 v14, 0x0

    goto :goto_0

    .line 141
    .restart local v14    # "purchasedHd":Z
    :cond_1
    const/4 v15, 0x0

    goto :goto_1

    :cond_2
    const/16 v16, 0x0

    goto :goto_2
.end method

.method public final isActive(Landroid/database/Cursor;)Z
    .locals 3
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x0

    .line 64
    invoke-interface {p1, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    const/4 v2, 0x2

    if-ne v1, v2, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public final isPinned(Landroid/database/Cursor;)Z
    .locals 1
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 30
    const/4 v0, 0x6

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v0

    return v0
.end method

.method public isRental(Landroid/database/Cursor;)Z
    .locals 2
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    const/4 v0, 0x1

    .line 69
    const/16 v1, 0x1c

    invoke-interface {p1, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-ne v1, v0, :cond_0

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
