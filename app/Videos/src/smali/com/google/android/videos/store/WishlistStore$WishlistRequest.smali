.class public Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
.super Ljava/lang/Object;
.source "WishlistStore.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/WishlistStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "WishlistRequest"
.end annotation


# instance fields
.field private final columns:[Ljava/lang/String;

.field private final orderByColumn:Ljava/lang/String;

.field private final tables:Ljava/lang/String;

.field private final whereArgs:[Ljava/lang/String;

.field private final whereClause:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 93
    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3"

    const/4 v0, 0x1

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const-string v5, "wishlist_item_order"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 96
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemType"    # I
    .param p3, "itemId"    # Ljava/lang/String;

    .prologue
    .line 105
    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3 AND wishlist_item_type = ? AND wishlist_item_id = ?"

    const/4 v0, 0x3

    new-array v4, v0, [Ljava/lang/String;

    const/4 v0, 0x0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x1

    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const/4 v0, 0x2

    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v0

    const-string v5, "wishlist_item_order"

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    .line 112
    return-void
.end method

.method public constructor <init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "tables"    # Ljava/lang/String;
    .param p2, "columns"    # [Ljava/lang/String;
    .param p3, "whereClause"    # Ljava/lang/String;
    .param p4, "whereArgs"    # [Ljava/lang/String;
    .param p5, "orderByColumn"    # Ljava/lang/String;

    .prologue
    .line 115
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 116
    iput-object p1, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->tables:Ljava/lang/String;

    .line 117
    iput-object p2, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->columns:[Ljava/lang/String;

    .line 118
    iput-object p3, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;

    .line 119
    iput-object p4, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;

    .line 120
    iput-object p5, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->orderByColumn:Ljava/lang/String;

    .line 121
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->tables:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$100(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->columns:[Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereClause:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->orderByColumn:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;)[Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    .prologue
    .line 22
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;->whereArgs:[Ljava/lang/String;

    return-object v0
.end method

.method public static createRequestForMoviesAndShows(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
    .locals 6
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "columns"    # [Ljava/lang/String;
    .param p2, "orderByColumn"    # Ljava/lang/String;

    .prologue
    .line 67
    new-instance v0, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    const-string v1, "wishlist LEFT JOIN shows ON wishlist_item_id = shows_id LEFT JOIN videos ON wishlist_item_id = video_id"

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3 AND wishlist_item_type IN (6,18) AND ( shows_id IS NOT NULL OR video_id IS NOT NULL)"

    const/4 v2, 0x1

    new-array v4, v2, [Ljava/lang/String;

    const/4 v2, 0x0

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v4, v2

    move-object v2, p1

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/WishlistStore$WishlistRequest;-><init>(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
