.class Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;
.super Ljava/lang/Object;
.source "PanoControllerOverlay.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/playback/PanoControllerOverlay;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/TrackChangeListener;Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)V
    .locals 0

    .prologue
    .line 191
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 194
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->updateProgress()V

    .line 195
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->handler:Landroid/os/Handler;
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$400(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/os/Handler;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$2;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->controlGlue:Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;
    invoke-static {v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$300(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$PlaybackControlGlue;->getUpdatePeriod()I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, p0, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 196
    return-void
.end method
