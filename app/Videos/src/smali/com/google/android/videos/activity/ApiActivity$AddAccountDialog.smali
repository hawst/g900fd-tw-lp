.class public Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;
.super Landroid/support/v4/app/DialogFragment;
.source "ApiActivity.java"

# interfaces
.implements Landroid/content/DialogInterface$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/ApiActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "AddAccountDialog"
.end annotation


# instance fields
.field private account:Ljava/lang/String;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 418
    invoke-direct {p0}, Landroid/support/v4/app/DialogFragment;-><init>()V

    return-void
.end method

.method private onCancel()V
    .locals 3

    .prologue
    .line 467
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/ApiActivity;

    .line 468
    .local v0, "activity":Lcom/google/android/videos/activity/ApiActivity;
    const/4 v1, 0x0

    new-instance v2, Ljava/lang/Throwable;

    invoke-direct {v2}, Ljava/lang/Throwable;-><init>()V

    # invokes: Lcom/google/android/videos/activity/ApiActivity;->finishWithFailure(ILjava/lang/Throwable;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/activity/ApiActivity;->access$200(Lcom/google/android/videos/activity/ApiActivity;ILjava/lang/Throwable;)V

    .line 469
    return-void
.end method

.method public static showInstance(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;)V
    .locals 4
    .param p0, "activity"    # Landroid/support/v4/app/FragmentActivity;
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 424
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 425
    .local v0, "args":Landroid/os/Bundle;
    const-string v2, "authAccount"

    invoke-virtual {v0, v2, p1}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 427
    new-instance v1, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;

    invoke-direct {v1}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;-><init>()V

    .line 428
    .local v1, "instance":Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;
    invoke-virtual {v1, v0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->setArguments(Landroid/os/Bundle;)V

    .line 429
    invoke-virtual {p0}, Landroid/support/v4/app/FragmentActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "AddAccountDialog"

    invoke-virtual {v1, v2, v3}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->show(Landroid/support/v4/app/FragmentManager;Ljava/lang/String;)V

    .line 430
    return-void
.end method


# virtual methods
.method public onCancel(Landroid/content/DialogInterface;)V
    .locals 0
    .param p1, "dialog"    # Landroid/content/DialogInterface;

    .prologue
    .line 445
    invoke-super {p0, p1}, Landroid/support/v4/app/DialogFragment;->onCancel(Landroid/content/DialogInterface;)V

    .line 446
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->onCancel()V

    .line 447
    return-void
.end method

.method public onClick(Landroid/content/DialogInterface;I)V
    .locals 3
    .param p1, "dialog"    # Landroid/content/DialogInterface;
    .param p2, "which"    # I

    .prologue
    .line 453
    const/4 v1, -0x1

    if-ne p2, v1, :cond_0

    .line 454
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/ApiActivity;

    .line 455
    .local v0, "activity":Lcom/google/android/videos/activity/ApiActivity;
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    new-instance v2, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog$1;

    invoke-direct {v2, p0, v0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog$1;-><init>(Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;Lcom/google/android/videos/activity/ApiActivity;)V

    invoke-virtual {v1, v2}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 464
    .end local v0    # "activity":Lcom/google/android/videos/activity/ApiActivity;
    :goto_0
    return-void

    .line 462
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->onCancel()V

    goto :goto_0
.end method

.method public onCreateDialog(Landroid/os/Bundle;)Landroid/app/Dialog;
    .locals 6
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 434
    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->getArguments()Landroid/os/Bundle;

    move-result-object v0

    .line 435
    .local v0, "args":Landroid/os/Bundle;
    const-string v1, "authAccount"

    invoke-virtual {v0, v1}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->account:Ljava/lang/String;

    .line 436
    new-instance v1, Landroid/app/AlertDialog$Builder;

    invoke-virtual {p0}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/app/AlertDialog$Builder;-><init>(Landroid/content/Context;)V

    const v2, 0x7f0b014b

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->account:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {p0, v2, v3}, Lcom/google/android/videos/activity/ApiActivity$AddAccountDialog;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/AlertDialog$Builder;->setMessage(Ljava/lang/CharSequence;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b014c

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setPositiveButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    const v2, 0x7f0b010d

    invoke-virtual {v1, v2, p0}, Landroid/app/AlertDialog$Builder;->setNegativeButton(ILandroid/content/DialogInterface$OnClickListener;)Landroid/app/AlertDialog$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/app/AlertDialog$Builder;->create()Landroid/app/AlertDialog;

    move-result-object v1

    return-object v1
.end method
