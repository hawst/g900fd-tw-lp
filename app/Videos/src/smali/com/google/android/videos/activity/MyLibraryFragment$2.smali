.class Lcom/google/android/videos/activity/MyLibraryFragment$2;
.super Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;
.source "MyLibraryFragment.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/MyLibraryFragment;->onCreateView(Landroid/view/LayoutInflater;Landroid/view/ViewGroup;Landroid/os/Bundle;)Landroid/view/View;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MyLibraryFragment;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/MyLibraryFragment;)V
    .locals 0

    .prologue
    .line 188
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$2;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-direct {p0}, Landroid/support/v4/view/ViewPager$SimpleOnPageChangeListener;-><init>()V

    return-void
.end method


# virtual methods
.method public onPageSelected(I)V
    .locals 2
    .param p1, "position"    # I

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$2;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/ArrayList;->size()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 192
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$2;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$300(Lcom/google/android/videos/activity/MyLibraryFragment;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$2;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # getter for: Lcom/google/android/videos/activity/MyLibraryFragment;->currentVerticals:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$200(Lcom/google/android/videos/activity/MyLibraryFragment;)Ljava/util/ArrayList;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onVerticalOpened(I)V

    .line 194
    :cond_0
    return-void
.end method
