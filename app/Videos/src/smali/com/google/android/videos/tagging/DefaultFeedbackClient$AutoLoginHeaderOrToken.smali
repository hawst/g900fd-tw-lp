.class final Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;
.super Ljava/lang/Object;
.source "DefaultFeedbackClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultFeedbackClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "AutoLoginHeaderOrToken"
.end annotation


# instance fields
.field public final autoLoginHeader:Ljava/lang/String;

.field public final token:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "autoLoginHeader"    # Ljava/lang/String;
    .param p2, "token"    # Ljava/lang/String;

    .prologue
    .line 204
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 205
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->autoLoginHeader:Ljava/lang/String;

    .line 206
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->token:Ljava/lang/String;

    .line 207
    return-void
.end method


# virtual methods
.method public hasToken()Z
    .locals 1

    .prologue
    .line 210
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultFeedbackClient$AutoLoginHeaderOrToken;->token:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
