.class public Lcom/google/android/videos/pano/repositories/StoreStatusObservable;
.super Ljava/lang/Object;
.source "StoreStatusObservable.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Observable;
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;


# instance fields
.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private final updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/accounts/SignInManager;Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V
    .locals 2
    .param p1, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "database"    # Lcom/google/android/videos/store/Database;
    .param p4, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p5, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    new-instance v0, Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-direct {v0, p2, p3, p4, p5}, Lcom/google/android/videos/store/StoreStatusMonitor;-><init>(Landroid/content/Context;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;)V

    .line 30
    .local v0, "storeStatusMonitor":Lcom/google/android/videos/store/StoreStatusMonitor;
    iput-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 31
    iput-object p1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 33
    new-instance v1, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;

    invoke-direct {v1, p0, p1, v0}, Lcom/google/android/videos/pano/repositories/StoreStatusObservable$1;-><init>(Lcom/google/android/videos/pano/repositories/StoreStatusObservable;Lcom/google/android/videos/accounts/SignInManager;Lcom/google/android/videos/store/StoreStatusMonitor;)V

    invoke-static {v1}, Lcom/google/android/repolib/observers/AsyncUpdateDispatcher;->asyncUpdateDispatcher(Lcom/google/android/repolib/observers/UpdatablesChanged;)Lcom/google/android/repolib/observers/UpdateDispatcher;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    .line 49
    return-void
.end method


# virtual methods
.method public addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 53
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 54
    return-void
.end method

.method public getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;
    .locals 1

    .prologue
    .line 76
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    return-object v0
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 1
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 72
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0}, Lcom/google/android/repolib/observers/UpdateDispatcher;->update()V

    .line 73
    return-void
.end method

.method public removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V
    .locals 1
    .param p1, "updatable"    # Lcom/google/android/repolib/observers/Updatable;

    .prologue
    .line 58
    iget-object v0, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->updateDispatcher:Lcom/google/android/repolib/observers/UpdateDispatcher;

    invoke-interface {v0, p1}, Lcom/google/android/repolib/observers/UpdateDispatcher;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 59
    return-void
.end method

.method public update()V
    .locals 2

    .prologue
    .line 63
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 64
    .local v0, "account":Ljava/lang/String;
    if-eqz v0, :cond_0

    .line 65
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/store/StoreStatusMonitor;->init(Ljava/lang/String;)V

    .line 66
    iget-object v1, p0, Lcom/google/android/videos/pano/repositories/StoreStatusObservable;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 68
    :cond_0
    return-void
.end method
