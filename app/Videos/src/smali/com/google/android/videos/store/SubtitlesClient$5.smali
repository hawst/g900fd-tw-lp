.class final Lcom/google/android/videos/store/SubtitlesClient$5;
.super Lcom/google/android/videos/store/AbstractFileStore;
.source "SubtitlesClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/SubtitlesClient;->getSubtitlesStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/AbstractFileStore",
        "<",
        "Lcom/google/android/videos/subtitles/SubtitleTrack;",
        "Lcom/google/android/videos/subtitles/Subtitles;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 174
    .local p2, "x1":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-void
.end method


# virtual methods
.method protected generateFilepath(Ljava/io/File;Lcom/google/android/videos/subtitles/SubtitleTrack;)Ljava/io/File;
    .locals 3
    .param p1, "rootFileDir"    # Ljava/io/File;
    .param p2, "key"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 178
    iget v1, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->fileVersion:I

    const/4 v2, 0x5

    if-ge v1, v2, :cond_0

    .line 179
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".cc"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 184
    .local v0, "filename":Ljava/lang/String;
    :goto_0
    new-instance v1, Ljava/io/File;

    # invokes: Lcom/google/android/videos/store/SubtitlesClient;->getSubtitlesDir(Ljava/io/File;)Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/videos/store/SubtitlesClient;->access$200(Ljava/io/File;)Ljava/io/File;

    move-result-object v2

    invoke-direct {v1, v2, v0}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1

    .line 181
    .end local v0    # "filename":Ljava/lang/String;
    :cond_0
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".v5"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .restart local v0    # "filename":Ljava/lang/String;
    goto :goto_0
.end method

.method protected bridge synthetic generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 174
    check-cast p2, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/SubtitlesClient$5;->generateFilepath(Ljava/io/File;Lcom/google/android/videos/subtitles/SubtitleTrack;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
