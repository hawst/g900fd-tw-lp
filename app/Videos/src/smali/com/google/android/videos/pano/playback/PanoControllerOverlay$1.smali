.class Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;
.super Landroid/support/v17/leanback/widget/PresenterSelector;
.source "PanoControllerOverlay.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/playback/PanoControllerOverlay;-><init>(Landroid/support/v4/app/FragmentActivity;Lcom/google/android/videos/player/overlay/TrackChangeListener;Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

.field final synthetic val$controlsPresenter:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

.field final synthetic val$knowledgePresenter:Landroid/support/v17/leanback/widget/Presenter;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;Landroid/support/v17/leanback/widget/Presenter;)V
    .locals 0

    .prologue
    .line 174
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    iput-object p2, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->val$controlsPresenter:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    iput-object p3, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->val$knowledgePresenter:Landroid/support/v17/leanback/widget/Presenter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/PresenterSelector;-><init>()V

    return-void
.end method


# virtual methods
.method public getPresenter(Ljava/lang/Object;)Landroid/support/v17/leanback/widget/Presenter;
    .locals 1
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    .line 177
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->playbackControlsRow:Lcom/google/android/recline/widget/PlaybackControlsRow;
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$100(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Lcom/google/android/recline/widget/PlaybackControlsRow;

    move-result-object v0

    if-ne p1, v0, :cond_0

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->val$controlsPresenter:Landroid/support/v17/leanback/widget/PlaybackControlsRowPresenter;

    .line 183
    :goto_0
    return-object v0

    .line 180
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->this$0:Lcom/google/android/videos/pano/playback/PanoControllerOverlay;

    # getter for: Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->knowledgeRow:Landroid/support/v17/leanback/widget/Row;
    invoke-static {v0}, Lcom/google/android/videos/pano/playback/PanoControllerOverlay;->access$200(Lcom/google/android/videos/pano/playback/PanoControllerOverlay;)Landroid/support/v17/leanback/widget/Row;

    move-result-object v0

    if-ne p1, v0, :cond_1

    .line 181
    iget-object v0, p0, Lcom/google/android/videos/pano/playback/PanoControllerOverlay$1;->val$knowledgePresenter:Landroid/support/v17/leanback/widget/Presenter;

    goto :goto_0

    .line 183
    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
