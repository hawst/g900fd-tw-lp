.class Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;
.super Ljava/lang/Object;
.source "LockScreenTransportControlV18.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnPlaybackPositionUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/LockScreenTransportControlV18;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "PlaybackPositionUpdateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;)V
    .locals 0

    .prologue
    .line 67
    iput-object p1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;->this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/LockScreenTransportControlV18;
    .param p2, "x1"    # Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;

    .prologue
    .line 67
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;-><init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;)V

    return-void
.end method


# virtual methods
.method public onPlaybackPositionUpdate(J)V
    .locals 5
    .param p1, "position"    # J

    .prologue
    .line 72
    iget-object v3, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$PlaybackPositionUpdateListener;->this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;

    invoke-virtual {v3}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getListeners()Ljava/util/List;

    move-result-object v2

    .line 73
    .local v2, "listeners":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/remote/TransportControl$Listener;>;"
    invoke-interface {v2}, Ljava/util/List;->iterator()Ljava/util/Iterator;

    move-result-object v0

    .local v0, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v0}, Ljava/util/Iterator;->hasNext()Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-interface {v0}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/remote/TransportControl$Listener;

    .line 74
    .local v1, "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    long-to-int v3, p1

    invoke-interface {v1, v3}, Lcom/google/android/videos/remote/TransportControl$Listener;->onSeek(I)V

    goto :goto_0

    .line 76
    .end local v1    # "listener":Lcom/google/android/videos/remote/TransportControl$Listener;
    :cond_0
    return-void
.end method
