.class Lcom/google/android/videos/DefaultContentNotificationProcessor$1;
.super Ljava/lang/Object;
.source "DefaultContentNotificationProcessor.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/DefaultContentNotificationProcessor;->processNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/String;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/DefaultContentNotificationProcessor;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$episodeIdList:Ljava/util/List;

.field final synthetic val$firstEpisodeTitle:Ljava/lang/String;

.field final synthetic val$firstSeasonId:Ljava/lang/String;

.field final synthetic val$showId:Ljava/lang/String;

.field final synthetic val$showTitle:Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/DefaultContentNotificationProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 46
    iput-object p1, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->this$0:Lcom/google/android/videos/DefaultContentNotificationProcessor;

    iput-object p2, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$account:Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$showId:Ljava/lang/String;

    iput-object p4, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$showTitle:Ljava/lang/String;

    iput-object p5, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$episodeIdList:Ljava/util/List;

    iput-object p6, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$firstSeasonId:Ljava/lang/String;

    iput-object p7, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$firstEpisodeTitle:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 46
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->onError(Ljava/lang/String;Ljava/lang/Exception;)V

    return-void
.end method

.method public onError(Ljava/lang/String;Ljava/lang/Exception;)V
    .locals 4
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "err"    # Ljava/lang/Exception;

    .prologue
    .line 55
    const-string v0, "Could not get poster for show with id %s."

    const/4 v1, 0x1

    new-array v1, v1, [Ljava/lang/Object;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$showId:Ljava/lang/String;

    aput-object v3, v1, v2

    invoke-static {v0, v1}, Ljava/lang/String;->format(Ljava/lang/String;[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 56
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    .line 57
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 46
    check-cast p1, Ljava/lang/String;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public onResponse(Ljava/lang/String;Landroid/graphics/Bitmap;)V
    .locals 8
    .param p1, "request"    # Ljava/lang/String;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 49
    iget-object v0, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->this$0:Lcom/google/android/videos/DefaultContentNotificationProcessor;

    iget-object v1, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$showId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$showTitle:Ljava/lang/String;

    iget-object v5, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$episodeIdList:Ljava/util/List;

    iget-object v6, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$firstSeasonId:Ljava/lang/String;

    iget-object v7, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;->val$firstEpisodeTitle:Ljava/lang/String;

    move-object v4, p2

    # invokes: Lcom/google/android/videos/DefaultContentNotificationProcessor;->notifyNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    invoke-static/range {v0 .. v7}, Lcom/google/android/videos/DefaultContentNotificationProcessor;->access$000(Lcom/google/android/videos/DefaultContentNotificationProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 51
    return-void
.end method
