.class final Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Requester;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ImageCachingRequester"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Requester",
        "<",
        "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private alwaysRequest:Z

.field private final globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

.field private final knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

.field private final storage:I

.field private final target:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/bitmap/BitmapLruCache;Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p1, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p2, "storage"    # I
    .param p3, "globalBitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "I",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 349
    .local p4, "target":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 350
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 351
    iput p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->storage:I

    .line 352
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    .line 353
    iput-object p4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    .line 354
    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;)Lcom/google/android/videos/bitmap/BitmapLruCache;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    .prologue
    .line 338
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    return-object v0
.end method


# virtual methods
.method public request(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Callback;)V
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 366
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    new-instance v2, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->storage:I

    invoke-direct {v2, v3, v4, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;-><init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILcom/google/android/videos/tagging/KnowledgeEntity$Image;)V

    .line 367
    .local v2, "imageRequest":Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$ImageRequest;->toFileName()Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    const-string v4, ".k"

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    .line 368
    .local v0, "cacheKey":Ljava/lang/String;
    iget-boolean v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->alwaysRequest:Z

    if-eqz v3, :cond_0

    const/4 v1, 0x0

    .line 369
    .local v1, "cached":Landroid/graphics/Bitmap;
    :goto_0
    if-eqz v1, :cond_1

    .line 370
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 384
    :goto_1
    return-void

    .line 368
    .end local v1    # "cached":Landroid/graphics/Bitmap;
    :cond_0
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->globalBitmapCache:Lcom/google/android/videos/bitmap/BitmapLruCache;

    invoke-virtual {v3, v0}, Lcom/google/android/videos/bitmap/BitmapLruCache;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Landroid/graphics/Bitmap;

    move-object v1, v3

    goto :goto_0

    .line 373
    .restart local v1    # "cached":Landroid/graphics/Bitmap;
    :cond_1
    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->target:Lcom/google/android/videos/async/Requester;

    new-instance v4, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;

    invoke-direct {v4, p0, v0, p2, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester$1;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Ljava/lang/String;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;)V

    invoke-interface {v3, v2, v4}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_1
.end method

.method public bridge synthetic request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 338
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method public setAlwaysRequest(Z)V
    .locals 0
    .param p1, "alwaysRequest"    # Z

    .prologue
    .line 361
    iput-boolean p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->alwaysRequest:Z

    .line 362
    return-void
.end method
