.class final Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;
.super Ljava/lang/Object;
.source "ActorsCards.java"

# interfaces
.implements Landroid/view/View$OnClickListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/ActorsCards;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "ActorCardItemClickListener"
.end annotation


# instance fields
.field private final actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

.field private final onActorsCardClickListener:Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;

.field private final profileImageView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;)V
    .locals 0
    .param p1, "profileImageView"    # Landroid/view/View;
    .param p2, "actor"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .param p3, "onActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;

    .prologue
    .line 87
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 88
    iput-object p1, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->profileImageView:Landroid/view/View;

    .line 89
    iput-object p2, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 90
    iput-object p3, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->onActorsCardClickListener:Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;

    .line 91
    return-void
.end method


# virtual methods
.method public onClick(Landroid/view/View;)V
    .locals 3
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 95
    iget-object v0, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->onActorsCardClickListener:Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ActorsCards$ActorCardItemClickListener;->profileImageView:Landroid/view/View;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;->onActorClick(Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/view/View;)V

    .line 96
    return-void
.end method
