.class public Lcom/google/android/videos/pano/nowtifications/Nowtification;
.super Ljava/lang/Object;
.source "Nowtification.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# instance fields
.field private final id:Ljava/lang/String;

.field private final notification:Landroid/app/Notification$Builder;


# direct methods
.method public constructor <init>(Landroid/app/Notification$Builder;Ljava/lang/String;)V
    .locals 0
    .param p1, "notification"    # Landroid/app/Notification$Builder;
    .param p2, "tag"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    iput-object p1, p0, Lcom/google/android/videos/pano/nowtifications/Nowtification;->notification:Landroid/app/Notification$Builder;

    .line 20
    iput-object p2, p0, Lcom/google/android/videos/pano/nowtifications/Nowtification;->id:Ljava/lang/String;

    .line 21
    return-void
.end method


# virtual methods
.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 30
    iget-object v0, p0, Lcom/google/android/videos/pano/nowtifications/Nowtification;->id:Ljava/lang/String;

    return-object v0
.end method

.method public getNotification()Landroid/app/Notification$Builder;
    .locals 1

    .prologue
    .line 24
    iget-object v0, p0, Lcom/google/android/videos/pano/nowtifications/Nowtification;->notification:Landroid/app/Notification$Builder;

    return-object v0
.end method
