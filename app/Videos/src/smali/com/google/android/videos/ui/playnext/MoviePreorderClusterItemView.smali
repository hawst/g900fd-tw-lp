.class public Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "MoviePreorderClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;

.field private videoId:Ljava/lang/String;

.field private yearAndDurationView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 33
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 34
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 37
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 38
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 41
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 42
    return-void
.end method


# virtual methods
.method bind(Lcom/google/android/videos/adapter/MoviesDataSource;ILcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;)V
    .locals 8
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "index"    # I
    .param p4, "binder"    # Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/MoviesDataSource;",
            "I",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .local p3, "posterRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    const/4 v4, 0x0

    .line 56
    invoke-virtual {p1, p2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v6

    .line 58
    .local v6, "cursor":Landroid/database/Cursor;
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {p1, v6}, Lcom/google/android/videos/adapter/MoviesDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 59
    invoke-virtual {p1, v6}, Lcom/google/android/videos/adapter/MoviesDataSource;->getReleaseYear(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v7

    .line 60
    .local v7, "year":Ljava/lang/Integer;
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    if-nez v7, :cond_0

    move-object v0, v4

    :goto_0
    invoke-virtual {v1, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 63
    invoke-virtual {p1, v6}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->videoId:Ljava/lang/String;

    .line 64
    const v1, 0x7f0f00c9

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const-class v5, Ljava/lang/String;

    move-object v0, p0

    move-object v3, p3

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 67
    invoke-virtual {p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->isNetworkConnected()Z

    move-result v0

    if-nez v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->setDimmedStyle(Z)V

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->videoId:Ljava/lang/String;

    invoke-static {v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->assetInfoFromMovieId(Ljava/lang/String;)Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;

    move-result-object v0

    invoke-virtual {p4, p0, v0}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$AssetInfo;)V

    .line 70
    return-void

    .line 60
    :cond_0
    invoke-virtual {v7}, Ljava/lang/Integer;->intValue()I

    move-result v0

    invoke-static {v0}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 67
    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 1
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 82
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 83
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 46
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 47
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->titleView:Landroid/widget/TextView;

    .line 48
    const v0, 0x7f0f00cb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->yearAndDurationView:Landroid/widget/TextView;

    .line 49
    const v0, 0x7f0f00c9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 52
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 74
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView;->videoId:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 77
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
