.class interface abstract Lcom/google/android/videos/tagging/SuggestionGridLayout$GridItem;
.super Ljava/lang/Object;
.source "SuggestionGridLayout.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/SuggestionGridLayout;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x608
    name = "GridItem"
.end annotation


# virtual methods
.method public abstract getGridLayoutParams()Lcom/google/android/videos/tagging/SuggestionGridLayout$LayoutParams;
.end method

.method public abstract getMeasuredHeight()I
.end method

.method public abstract getMeasuredWidth()I
.end method

.method public abstract getViews()Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "()",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/view/View;",
            ">;"
        }
    .end annotation
.end method

.method public abstract gridLayout(IIII)V
.end method

.method public abstract gridMeasure(II)V
.end method

.method public abstract isGone()Z
.end method

.method public abstract removeView(Landroid/view/View;)V
.end method
