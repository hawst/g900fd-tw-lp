.class public abstract Lcom/google/android/videos/cast/v2/PlayMoviesChannel;
.super Ljava/lang/Object;
.source "PlayMoviesChannel.java"

# interfaces
.implements Lcom/google/android/gms/cast/Cast$MessageReceivedCallback;


# instance fields
.field private final audioTrackIdMap:Ljava/util/HashMap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/HashMap",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field private final verboseLogging:Z


# direct methods
.method protected constructor <init>(Z)V
    .locals 1
    .param p1, "verboseLogging"    # Z

    .prologue
    .line 76
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 77
    iput-boolean p1, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->verboseLogging:Z

    .line 78
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->audioTrackIdMap:Ljava/util/HashMap;

    .line 79
    return-void
.end method

.method public static getNamespace()Ljava/lang/String;
    .locals 1

    .prologue
    .line 187
    const-string v0, "urn:x-cast:com.google.play.movies"

    return-object v0
.end method

.method private handleAudioTrackList(Lorg/json/JSONArray;)V
    .locals 10
    .param p1, "jsonTracks"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    const/4 v9, 0x2

    .line 254
    iget-object v7, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->audioTrackIdMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 255
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 256
    .local v6, "trackCount":I
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0, v6}, Ljava/util/ArrayList;-><init>(I)V

    .line 257
    .local v0, "audioTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    const/4 v3, -0x1

    .line 258
    .local v3, "selectedTrackIndex":I
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_5

    .line 259
    invoke-virtual {p1, v1}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v2

    .line 260
    .local v2, "jsonTrack":Lorg/json/JSONObject;
    new-instance v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;

    invoke-direct {v4}, Lcom/google/wireless/android/video/magma/proto/AudioInfo;-><init>()V

    .line 261
    .local v4, "track":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    const-string v7, "language"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v7

    iput-object v7, v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->language:Ljava/lang/String;

    .line 262
    const-string v7, "content"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    .line 263
    .local v5, "trackContent":Ljava/lang/String;
    const-string v7, "PRIMARY"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_2

    .line 264
    const/4 v7, 0x1

    iput v7, v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    .line 273
    :cond_0
    :goto_1
    const-string v7, "selected"

    invoke-virtual {v2, v7}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v7

    if-eqz v7, :cond_1

    .line 274
    move v3, v1

    .line 276
    :cond_1
    invoke-interface {v0, v4}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 277
    iget-object v7, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->audioTrackIdMap:Ljava/util/HashMap;

    const-string v8, "id"

    invoke-virtual {v2, v8}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v8

    invoke-virtual {v7, v4, v8}, Ljava/util/HashMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 258
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 265
    :cond_2
    const-string v7, "PRIMARY_DESCRIPTIVE"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_3

    .line 266
    const/4 v7, 0x3

    iput v7, v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    goto :goto_1

    .line 267
    :cond_3
    const-string v7, "DIRECTOR_COMMENTARY"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_4

    .line 268
    iput v9, v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    goto :goto_1

    .line 269
    :cond_4
    const-string v7, "ACTOR_COMMENTARY"

    invoke-virtual {v7, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 271
    iput v9, v4, Lcom/google/wireless/android/video/magma/proto/AudioInfo;->type:I

    goto :goto_1

    .line 279
    .end local v2    # "jsonTrack":Lorg/json/JSONObject;
    .end local v4    # "track":Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .end local v5    # "trackContent":Ljava/lang/String;
    :cond_5
    if-gez v3, :cond_6

    .line 280
    invoke-interface {v0}, Ljava/util/List;->clear()V

    .line 281
    iget-object v7, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->audioTrackIdMap:Ljava/util/HashMap;

    invoke-virtual {v7}, Ljava/util/HashMap;->clear()V

    .line 283
    :cond_6
    invoke-virtual {p0, v0, v3}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->onAudioTracksChanged(Ljava/util/List;I)V

    .line 284
    return-void
.end method

.method private handleTimedTextTrackList(Lorg/json/JSONArray;)V
    .locals 12
    .param p1, "jsonTracks"    # Lorg/json/JSONArray;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lorg/json/JSONException;
        }
    .end annotation

    .prologue
    .line 222
    invoke-virtual {p1}, Lorg/json/JSONArray;->length()I

    move-result v11

    .line 223
    .local v11, "trackCount":I
    new-instance v9, Ljava/util/ArrayList;

    invoke-direct {v9, v11}, Ljava/util/ArrayList;-><init>(I)V

    .line 224
    .local v9, "subtitleTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v8, 0x0

    .line 225
    .local v8, "selectedTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    const/4 v6, 0x0

    .local v6, "i":I
    :goto_0
    if-ge v6, v11, :cond_2

    .line 226
    invoke-virtual {p1, v6}, Lorg/json/JSONArray;->getJSONObject(I)Lorg/json/JSONObject;

    move-result-object v7

    .line 229
    .local v7, "jsonTrack":Lorg/json/JSONObject;
    const-string v0, "DAHH"

    const-string v2, "accessibility"

    invoke-virtual {v7, v2}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    const-string v1, "[CC]"

    .line 231
    .local v1, "trackName":Ljava/lang/String;
    :goto_1
    const-string v0, "language"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    const-string v2, "AAAAAAAAAAA"

    const/4 v3, 0x1

    const-string v4, "id"

    invoke-virtual {v7, v4}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    const/4 v5, 0x0

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v10

    .line 234
    .local v10, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    const-string v0, "selected"

    invoke-virtual {v7, v0}, Lorg/json/JSONObject;->getBoolean(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 235
    move-object v8, v10

    .line 237
    :cond_0
    invoke-interface {v9, v10}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 225
    add-int/lit8 v6, v6, 0x1

    goto :goto_0

    .line 229
    .end local v1    # "trackName":Ljava/lang/String;
    .end local v10    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_1
    const-string v1, ""

    goto :goto_1

    .line 239
    .end local v7    # "jsonTrack":Lorg/json/JSONObject;
    :cond_2
    invoke-virtual {p0, v9, v8}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->onSubtitleTracksChanged(Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 240
    return-void
.end method

.method private sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 3
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "message"    # Lorg/json/JSONObject;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Lorg/json/JSONObject;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 304
    iget-boolean v0, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->verboseLogging:Z

    if-eqz v0, :cond_0

    .line 305
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Sent message: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 307
    :cond_0
    sget-object v0, Lcom/google/android/gms/cast/Cast;->CastApi:Lcom/google/android/gms/cast/Cast$CastApi;

    const-string v1, "urn:x-cast:com.google.play.movies"

    invoke-virtual {p2}, Lorg/json/JSONObject;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-interface {v0, p1, v1, v2}, Lcom/google/android/gms/cast/Cast$CastApi;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v0

    return-object v0
.end method


# virtual methods
.method protected abstract onAudioTracksChanged(Ljava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation
.end method

.method protected abstract onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V
.end method

.method public onMessageReceived(Lcom/google/android/gms/cast/CastDevice;Ljava/lang/String;Ljava/lang/String;)V
    .locals 12
    .param p1, "castDevice"    # Lcom/google/android/gms/cast/CastDevice;
    .param p2, "namespace"    # Ljava/lang/String;
    .param p3, "message"    # Ljava/lang/String;

    .prologue
    .line 156
    iget-boolean v10, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->verboseLogging:Z

    if-eqz v10, :cond_0

    .line 157
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "Received message: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 160
    :cond_0
    :try_start_0
    new-instance v5, Lorg/json/JSONObject;

    invoke-direct {v5, p3}, Lorg/json/JSONObject;-><init>(Ljava/lang/String;)V

    .line 161
    .local v5, "payload":Lorg/json/JSONObject;
    const-string v10, "type"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 163
    .local v9, "type":Ljava/lang/String;
    const-string v10, "KEY_REQUEST"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_3

    .line 165
    const-string v10, "cmd_id"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getLong(Ljava/lang/String;)J

    move-result-wide v2

    .line 166
    .local v2, "id":J
    const-string v10, "method"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 167
    .local v4, "method":Ljava/lang/String;
    const-string v10, "requests"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v7

    .line 170
    .local v7, "requestJSONArray":Lorg/json/JSONArray;
    invoke-virtual {v7}, Lorg/json/JSONArray;->length()I

    move-result v6

    .line 171
    .local v6, "requestCount":I
    new-array v8, v6, [Ljava/lang/String;

    .line 172
    .local v8, "requests":[Ljava/lang/String;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v6, :cond_1

    .line 173
    invoke-virtual {v7, v1}, Lorg/json/JSONArray;->get(I)Ljava/lang/Object;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v10

    aput-object v10, v8, v1

    .line 172
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 175
    :cond_1
    invoke-virtual {p0, v2, v3, v4, v8}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->onKeyRequested(JLjava/lang/String;[Ljava/lang/String;)V

    .line 184
    .end local v1    # "i":I
    .end local v2    # "id":J
    .end local v4    # "method":Ljava/lang/String;
    .end local v5    # "payload":Lorg/json/JSONObject;
    .end local v6    # "requestCount":I
    .end local v7    # "requestJSONArray":Lorg/json/JSONArray;
    .end local v8    # "requests":[Ljava/lang/String;
    .end local v9    # "type":Ljava/lang/String;
    :cond_2
    :goto_1
    return-void

    .line 176
    .restart local v5    # "payload":Lorg/json/JSONObject;
    .restart local v9    # "type":Ljava/lang/String;
    :cond_3
    const-string v10, "TT_TRACK_LIST_NOTIFICATION"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_4

    .line 177
    const-string v10, "tracks"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->handleTimedTextTrackList(Lorg/json/JSONArray;)V
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_1

    .line 181
    .end local v5    # "payload":Lorg/json/JSONObject;
    .end local v9    # "type":Ljava/lang/String;
    :catch_0
    move-exception v0

    .line 182
    .local v0, "e":Lorg/json/JSONException;
    new-instance v10, Ljava/lang/StringBuilder;

    invoke-direct {v10}, Ljava/lang/StringBuilder;-><init>()V

    const-string v11, "error parsing message: "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, p3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    const-string v11, " "

    invoke-virtual {v10, v11}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v10

    invoke-virtual {v10}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-static {v10}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    goto :goto_1

    .line 178
    .end local v0    # "e":Lorg/json/JSONException;
    .restart local v5    # "payload":Lorg/json/JSONObject;
    .restart local v9    # "type":Ljava/lang/String;
    :cond_4
    :try_start_1
    const-string v10, "AUDIO_TRACK_LIST_NOTIFICATION"

    invoke-virtual {v10, v9}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v10

    if-eqz v10, :cond_2

    .line 179
    const-string v10, "tracks"

    invoke-virtual {v5, v10}, Lorg/json/JSONObject;->getJSONArray(Ljava/lang/String;)Lorg/json/JSONArray;

    move-result-object v10

    invoke-direct {p0, v10}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->handleAudioTrackList(Lorg/json/JSONArray;)V
    :try_end_1
    .catch Lorg/json/JSONException; {:try_start_1 .. :try_end_1} :catch_0

    goto :goto_1
.end method

.method protected abstract onSubtitleTracksChanged(Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ")V"
        }
    .end annotation
.end method

.method public requestAudioTracks(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 3
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 243
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 245
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "type"

    const-string v2, "AUDIO_TRACK_LIST_REQUEST"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 246
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 250
    :goto_0
    return-object v1

    .line 247
    :catch_0
    move-exception v1

    .line 250
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public requestSubtitleTracks(Lcom/google/android/gms/common/api/GoogleApiClient;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 3
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 191
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 193
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "type"

    const-string v2, "TT_TRACK_LIST_REQUEST"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 194
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 198
    :goto_0
    return-object v1

    .line 195
    :catch_0
    move-exception v1

    .line 198
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public selectAudioTrack(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/wireless/android/video/magma/proto/AudioInfo;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 5
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "newTrack"    # Lcom/google/wireless/android/video/magma/proto/AudioInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 288
    iget-object v3, p0, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->audioTrackIdMap:Ljava/util/HashMap;

    invoke-virtual {v3, p2}, Ljava/util/HashMap;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/String;

    .line 290
    .local v1, "trackId":Ljava/lang/String;
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 292
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 293
    .local v2, "tracks":Lorg/json/JSONArray;
    invoke-virtual {v2, v1}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 294
    const-string v3, "type"

    const-string v4, "AUDIO_TRACK_SELECT_REQUEST"

    invoke-virtual {v0, v3, v4}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v3

    const-string v4, "selected"

    invoke-virtual {v3, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 296
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v3

    .line 300
    .end local v2    # "tracks":Lorg/json/JSONArray;
    :goto_0
    return-object v3

    .line 297
    :catch_0
    move-exception v3

    .line 300
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public selectSubtitleTrack(Lcom/google/android/gms/common/api/GoogleApiClient;Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 4
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "newTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 203
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 205
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    new-instance v1, Lorg/json/JSONArray;

    invoke-direct {v1}, Lorg/json/JSONArray;-><init>()V

    .line 209
    .local v1, "tracks":Lorg/json/JSONArray;
    if-eqz p2, :cond_0

    .line 210
    iget-object v2, p2, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    invoke-virtual {v1, v2}, Lorg/json/JSONArray;->put(Ljava/lang/Object;)Lorg/json/JSONArray;

    .line 212
    :cond_0
    const-string v2, "type"

    const-string v3, "TT_TRACK_SELECT_REQUEST"

    invoke-virtual {v0, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v2

    const-string v3, "selected"

    invoke-virtual {v2, v3, v1}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 214
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    .line 218
    .end local v1    # "tracks":Lorg/json/JSONArray;
    :goto_0
    return-object v2

    .line 215
    :catch_0
    move-exception v2

    .line 218
    const/4 v2, 0x0

    goto :goto_0
.end method

.method public sendIdentity(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 3
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "name"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 97
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 99
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "type"

    const-string v2, "HELLO"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 100
    const-string v1, "device_name"

    invoke-virtual {v0, v1, p2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    .line 104
    :goto_0
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;

    move-result-object v1

    return-object v1

    .line 101
    :catch_0
    move-exception v1

    goto :goto_0
.end method

.method public final sendKeyResponse(Lcom/google/android/gms/common/api/GoogleApiClient;J[Ljava/lang/String;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 6
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .param p2, "requestId"    # J
    .param p4, "tokens"    # [Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "J[",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/lang/IllegalStateException;
        }
    .end annotation

    .prologue
    .line 135
    new-instance v1, Lorg/json/JSONObject;

    invoke-direct {v1}, Lorg/json/JSONObject;-><init>()V

    .line 138
    .local v1, "payload":Lorg/json/JSONObject;
    :try_start_0
    array-length v3, p4

    .line 139
    .local v3, "tokenCount":I
    new-instance v2, Lorg/json/JSONArray;

    invoke-direct {v2}, Lorg/json/JSONArray;-><init>()V

    .line 140
    .local v2, "tokenArray":Lorg/json/JSONArray;
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    if-ge v0, v3, :cond_0

    .line 141
    aget-object v4, p4, v0

    invoke-virtual {v2, v0, v4}, Lorg/json/JSONArray;->put(ILjava/lang/Object;)Lorg/json/JSONArray;

    .line 140
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 144
    :cond_0
    const-string v4, "cmd_id"

    invoke-virtual {v1, v4, p2, p3}, Lorg/json/JSONObject;->put(Ljava/lang/String;J)Lorg/json/JSONObject;

    .line 145
    const-string v4, "type"

    const-string v5, "KEY_RESPONSE"

    invoke-virtual {v1, v4, v5}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 146
    const-string v4, "tokens"

    invoke-virtual {v1, v4, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 147
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v4

    .line 151
    .end local v0    # "i":I
    .end local v2    # "tokenArray":Lorg/json/JSONArray;
    .end local v3    # "tokenCount":I
    :goto_1
    return-object v4

    .line 148
    :catch_0
    move-exception v4

    .line 151
    const/4 v4, 0x0

    goto :goto_1
.end method

.method public sendRecommendations(Lcom/google/android/gms/common/api/GoogleApiClient;Ljava/util/List;)Lcom/google/android/gms/common/api/PendingResult;
    .locals 4
    .param p1, "apiClient"    # Lcom/google/android/gms/common/api/GoogleApiClient;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/gms/common/api/GoogleApiClient;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)",
            "Lcom/google/android/gms/common/api/PendingResult",
            "<",
            "Lcom/google/android/gms/common/api/Status;",
            ">;"
        }
    .end annotation

    .prologue
    .line 115
    .local p2, "recommendations":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lorg/json/JSONObject;

    invoke-direct {v0}, Lorg/json/JSONObject;-><init>()V

    .line 117
    .local v0, "message":Lorg/json/JSONObject;
    :try_start_0
    const-string v1, "type"

    const-string v2, "RECOMMENDATION"

    invoke-virtual {v0, v1, v2}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    move-result-object v1

    const-string v2, "ids"

    new-instance v3, Lorg/json/JSONArray;

    invoke-direct {v3, p2}, Lorg/json/JSONArray;-><init>(Ljava/util/Collection;)V

    invoke-virtual {v1, v2, v3}, Lorg/json/JSONObject;->put(Ljava/lang/String;Ljava/lang/Object;)Lorg/json/JSONObject;

    .line 118
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/cast/v2/PlayMoviesChannel;->sendMessage(Lcom/google/android/gms/common/api/GoogleApiClient;Lorg/json/JSONObject;)Lcom/google/android/gms/common/api/PendingResult;
    :try_end_0
    .catch Lorg/json/JSONException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .line 122
    :goto_0
    return-object v1

    .line 119
    :catch_0
    move-exception v1

    .line 122
    const/4 v1, 0x0

    goto :goto_0
.end method
