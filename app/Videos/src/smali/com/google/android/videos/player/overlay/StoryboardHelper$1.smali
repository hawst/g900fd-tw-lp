.class Lcom/google/android/videos/player/overlay/StoryboardHelper$1;
.super Ljava/lang/Object;
.source "StoryboardHelper.java"

# interfaces
.implements Landroid/animation/ValueAnimator$AnimatorUpdateListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewVisibility(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;)V
    .locals 0

    .prologue
    .line 243
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$1;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationUpdate(Landroid/animation/ValueAnimator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/ValueAnimator;

    .prologue
    .line 246
    invoke-virtual {p1}, Landroid/animation/ValueAnimator;->getAnimatedValue()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Ljava/lang/Integer;

    invoke-virtual {v1}, Ljava/lang/Integer;->intValue()I

    move-result v0

    .line 247
    .local v0, "alpha":I
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$1;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    # getter for: Lcom/google/android/videos/player/overlay/StoryboardHelper;->overlayColorDrawable:Landroid/graphics/drawable/ColorDrawable;
    invoke-static {v1}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->access$000(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/graphics/drawable/ColorDrawable;

    move-result-object v1

    invoke-virtual {v1, v0}, Landroid/graphics/drawable/ColorDrawable;->setAlpha(I)V

    .line 248
    return-void
.end method
