.class public Lcom/google/android/videos/DefaultContentNotificationProcessor;
.super Ljava/lang/Object;
.source "DefaultContentNotificationProcessor.java"

# interfaces
.implements Lcom/google/android/videos/ContentNotificationManager$Processor;


# instance fields
.field private final context:Landroid/content/Context;

.field private final notificationManager:Landroid/app/NotificationManager;

.field private final posterStore:Lcom/google/android/videos/store/PosterStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/store/PosterStore;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "posterStore"    # Lcom/google/android/videos/store/PosterStore;

    .prologue
    .line 27
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 28
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    .line 29
    const-string v0, "notification"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/app/NotificationManager;

    iput-object v0, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->notificationManager:Landroid/app/NotificationManager;

    .line 31
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PosterStore;

    iput-object v0, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->posterStore:Lcom/google/android/videos/store/PosterStore;

    .line 32
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/DefaultContentNotificationProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/DefaultContentNotificationProcessor;
    .param p1, "x1"    # Ljava/lang/String;
    .param p2, "x2"    # Ljava/lang/String;
    .param p3, "x3"    # Ljava/lang/String;
    .param p4, "x4"    # Landroid/graphics/Bitmap;
    .param p5, "x5"    # Ljava/util/List;
    .param p6, "x6"    # Ljava/lang/String;
    .param p7, "x7"    # Ljava/lang/String;

    .prologue
    .line 21
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/DefaultContentNotificationProcessor;->notifyNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    return-void
.end method

.method private createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 119
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "new_episodes_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "_"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private notifyNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V
    .locals 23
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "showTitle"    # Ljava/lang/String;
    .param p4, "showPoster"    # Landroid/graphics/Bitmap;
    .param p6, "firstSeasonId"    # Ljava/lang/String;
    .param p7, "firstEpisodeTitle"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 81
    .local p5, "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-interface/range {p5 .. p5}, Ljava/util/List;->size()I

    move-result v17

    .line 82
    .local v17, "size":I
    move/from16 v0, v17

    new-array v1, v0, [Ljava/lang/String;

    move-object/from16 v0, p5

    invoke-interface {v0, v1}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v7

    check-cast v7, [Ljava/lang/String;

    .line 83
    .local v7, "episodeIds":[Ljava/lang/String;
    const/4 v1, 0x0

    aget-object v4, v7, v1

    .line 85
    .local v4, "firstEpisodeId":Ljava/lang/String;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    const-string v2, "com.google.android.videos.DETAILS"

    move-object/from16 v3, p1

    move-object/from16 v5, p6

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v7}, Lcom/google/android/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v12

    .line 89
    .local v12, "detailsPendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    const-string v2, "com.google.android.videos.CANCEL"

    move-object/from16 v3, p1

    move-object/from16 v5, p6

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v7}, Lcom/google/android/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v15

    .line 95
    .local v15, "cancelPendingIntent":Landroid/app/PendingIntent;
    const/4 v1, 0x1

    move/from16 v0, v17

    if-ne v0, v1, :cond_0

    .line 97
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    const-string v2, "com.google.android.videos.PLAY"

    move-object/from16 v3, p1

    move-object/from16 v5, p6

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v7}, Lcom/google/android/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v13

    .line 101
    .local v13, "playPendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    const-string v2, "com.google.android.videos.DOWNLOAD"

    move-object/from16 v3, p1

    move-object/from16 v5, p6

    move-object/from16 v6, p2

    invoke-static/range {v1 .. v7}, Lcom/google/android/videos/ContentNotificationService;->getPendingIntentForAction(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v14

    .line 105
    .local v14, "pinPendingIntent":Landroid/app/PendingIntent;
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/videos/NotificationUtil;

    move-result-object v8

    move-object/from16 v9, p7

    move-object/from16 v10, p3

    move-object/from16 v11, p4

    invoke-virtual/range {v8 .. v15}, Lcom/google/android/videos/NotificationUtil;->createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v22

    .line 114
    .end local v13    # "playPendingIntent":Landroid/app/PendingIntent;
    .end local v14    # "pinPendingIntent":Landroid/app/PendingIntent;
    .local v22, "notification":Landroid/app/Notification;
    :goto_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->notificationManager:Landroid/app/NotificationManager;

    invoke-direct/range {p0 .. p2}, Lcom/google/android/videos/DefaultContentNotificationProcessor;->createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const v3, 0x7f0f003c

    move-object/from16 v0, v22

    invoke-virtual {v1, v2, v3, v0}, Landroid/app/NotificationManager;->notify(Ljava/lang/String;ILandroid/app/Notification;)V

    .line 116
    return-void

    .line 109
    .end local v22    # "notification":Landroid/app/Notification;
    :cond_0
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/videos/NotificationUtil;->create(Landroid/content/Context;)Lcom/google/android/videos/NotificationUtil;

    move-result-object v16

    move-object/from16 v18, p3

    move-object/from16 v19, p4

    move-object/from16 v20, v12

    move-object/from16 v21, v15

    invoke-virtual/range {v16 .. v21}, Lcom/google/android/videos/NotificationUtil;->createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;

    move-result-object v22

    .restart local v22    # "notification":Landroid/app/Notification;
    goto :goto_0
.end method


# virtual methods
.method public beginProcessing()V
    .locals 0

    .prologue
    .line 39
    return-void
.end method

.method public dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "episodeIds"    # [Ljava/lang/String;

    .prologue
    .line 74
    iget-object v0, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->notificationManager:Landroid/app/NotificationManager;

    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/DefaultContentNotificationProcessor;->createNewEpisodesTag(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const v2, 0x7f0f003c

    invoke-virtual {v0, v1, v2}, Landroid/app/NotificationManager;->cancel(Ljava/lang/String;I)V

    .line 76
    return-void
.end method

.method public dismissContentNotification(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;

    .prologue
    .line 70
    return-void
.end method

.method public finishProcessing()V
    .locals 0

    .prologue
    .line 65
    return-void
.end method

.method public processNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "showTitle"    # Ljava/lang/String;
    .param p5, "firstSeasonId"    # Ljava/lang/String;
    .param p6, "firstEpisodeTitle"    # Ljava/lang/String;
    .param p7, "showPosterUri"    # Ljava/lang/String;
    .param p8, "showBannerUri"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p4, "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v0, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move-object v7, p6

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/DefaultContentNotificationProcessor$1;-><init>(Lcom/google/android/videos/DefaultContentNotificationProcessor;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;)V

    .line 59
    .local v0, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iget-object v1, p0, Lcom/google/android/videos/DefaultContentNotificationProcessor;->posterStore:Lcom/google/android/videos/store/PosterStore;

    const/4 v2, 0x1

    invoke-virtual {v1, v2, p2, v0}, Lcom/google/android/videos/store/PosterStore;->getImage(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 60
    return-void
.end method
