.class public Lcom/google/android/videos/pinning/UpdateWishlistKey;
.super Ljava/lang/Object;
.source "UpdateWishlistKey.java"

# interfaces
.implements Lcom/google/android/videos/pinning/Task$Key;


# instance fields
.field public final account:Ljava/lang/String;

.field public final itemId:Ljava/lang/String;

.field public final itemType:I


# direct methods
.method public constructor <init>(Ljava/lang/String;ILjava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "itemType"    # I
    .param p3, "itemId"    # Ljava/lang/String;

    .prologue
    .line 18
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 19
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    .line 20
    invoke-static {p2}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    .line 21
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    .line 22
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 4
    .param p1, "object"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x0

    .line 26
    instance-of v2, p1, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    if-eqz v2, :cond_0

    move-object v0, p1

    .line 27
    check-cast v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;

    .line 28
    .local v0, "other":Lcom/google/android/videos/pinning/UpdateWishlistKey;
    iget v2, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    iget v3, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    if-ne v2, v3, :cond_0

    iget-object v2, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, v0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-virtual {v2, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v2

    if-eqz v2, :cond_0

    const/4 v1, 0x1

    .line 31
    .end local v0    # "other":Lcom/google/android/videos/pinning/UpdateWishlistKey;
    :cond_0
    return v1
.end method

.method public hashCode()I
    .locals 2

    .prologue
    .line 36
    iget-object v0, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-virtual {v0}, Ljava/lang/String;->hashCode()I

    move-result v0

    iget v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    xor-int/2addr v0, v1

    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-virtual {v1}, Ljava/lang/String;->hashCode()I

    move-result v1

    xor-int/2addr v0, v1

    return v0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 41
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "[updateWishlist "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->account:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/utils/Hashing;->sha1(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemType:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/pinning/UpdateWishlistKey;->itemId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "]"

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
