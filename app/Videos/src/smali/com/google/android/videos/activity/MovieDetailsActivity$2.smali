.class Lcom/google/android/videos/activity/MovieDetailsActivity$2;
.super Ljava/lang/Object;
.source "MovieDetailsActivity.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/MovieDetailsActivity;->onCreate(Landroid/os/Bundle;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/MovieDetailsActivity;)V
    .locals 0

    .prologue
    .line 338
    iput-object p1, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$2;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 345
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$2;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onSyncError(Ljava/lang/Exception;)V
    invoke-static {v0, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$300(Lcom/google/android/videos/activity/MovieDetailsActivity;Ljava/lang/Exception;)V

    .line 346
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 338
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity$2;->onError(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .param p2, "response"    # Ljava/lang/Void;

    .prologue
    .line 341
    iget-object v0, p0, Lcom/google/android/videos/activity/MovieDetailsActivity$2;->this$0:Lcom/google/android/videos/activity/MovieDetailsActivity;

    # invokes: Lcom/google/android/videos/activity/MovieDetailsActivity;->onSyncSuccess()V
    invoke-static {v0}, Lcom/google/android/videos/activity/MovieDetailsActivity;->access$200(Lcom/google/android/videos/activity/MovieDetailsActivity;)V

    .line 342
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 338
    check-cast p1, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/activity/MovieDetailsActivity$2;->onResponse(Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;Ljava/lang/Void;)V

    return-void
.end method
