.class public final Lcom/google/android/videos/ContentNotificationManager;
.super Ljava/lang/Object;
.source "ContentNotificationManager.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ContentNotificationManager$NewEpisodesQuery;,
        Lcom/google/android/videos/ContentNotificationManager$Processor;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Landroid/database/Cursor;",
        ">;"
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final gcmSender:Lcom/google/android/videos/gcm/GcmSender;

.field private pendingUpdateRequest:Z

.field private processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private updateInProgress:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/gcm/GcmSender;)V
    .locals 1
    .param p1, "database"    # Lcom/google/android/videos/store/Database;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p4, "gcmSender"    # Lcom/google/android/videos/gcm/GcmSender;

    .prologue
    .line 54
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/Database;

    iput-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->database:Lcom/google/android/videos/store/Database;

    .line 56
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->config:Lcom/google/android/videos/Config;

    .line 57
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 58
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/gcm/GcmSender;

    iput-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;

    .line 59
    return-void
.end method

.method private static getSqlUpdateIsNewNotification(I)Ljava/lang/String;
    .locals 2
    .param p0, "videoIdsCount"    # I

    .prologue
    .line 218
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "UPDATE purchased_assets SET is_new_notification_dismissed = 1 WHERE account = ? AND asset_type = 20 AND "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "asset_id"

    invoke-static {v1, p0}, Lcom/google/android/videos/utils/DbUtils;->buildInMultipleParamsClause(Ljava/lang/String;I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method private processCursor(Landroid/database/Cursor;)V
    .locals 11
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 109
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    if-nez v0, :cond_0

    .line 152
    :goto_0
    return-void

    .line 112
    :cond_0
    const/4 v1, 0x0

    .line 113
    .local v1, "account":Ljava/lang/String;
    const/4 v2, 0x0

    .line 114
    .local v2, "showId":Ljava/lang/String;
    const/4 v3, 0x0

    .line 115
    .local v3, "showTitle":Ljava/lang/String;
    const/4 v7, 0x0

    .line 116
    .local v7, "showPosterUri":Ljava/lang/String;
    const/4 v8, 0x0

    .line 117
    .local v8, "showBannerUri":Ljava/lang/String;
    const/4 v5, 0x0

    .line 118
    .local v5, "firstSeasonId":Ljava/lang/String;
    const/4 v6, 0x0

    .line 119
    .local v6, "firstEpisodeTitle":Ljava/lang/String;
    const/4 v4, 0x0

    .line 121
    .local v4, "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface {v0}, Lcom/google/android/videos/ContentNotificationManager$Processor;->beginProcessing()V

    .line 122
    :goto_1
    invoke-interface {p1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_4

    .line 123
    const/4 v0, 0x0

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 124
    .local v9, "newAccount":Ljava/lang/String;
    const/4 v0, 0x3

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v10

    .line 126
    .local v10, "newShowId":Ljava/lang/String;
    invoke-static {v1, v9}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    invoke-static {v2, v10}, Landroid/text/TextUtils;->equals(Ljava/lang/CharSequence;Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    .line 127
    :cond_1
    if-eqz v2, :cond_2

    .line 128
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface/range {v0 .. v8}, Lcom/google/android/videos/ContentNotificationManager$Processor;->processNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 132
    :cond_2
    move-object v1, v9

    .line 133
    move-object v2, v10

    .line 134
    const/4 v0, 0x4

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 135
    const/4 v0, 0x6

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 136
    const/4 v0, 0x7

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 137
    const/4 v0, 0x5

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 138
    const/4 v0, 0x2

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 139
    new-instance v4, Ljava/util/ArrayList;

    .end local v4    # "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 142
    .restart local v4    # "episodeIdList":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    :cond_3
    const/4 v0, 0x1

    invoke-interface {p1, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    invoke-interface {v4, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_1

    .line 146
    .end local v9    # "newAccount":Ljava/lang/String;
    .end local v10    # "newShowId":Ljava/lang/String;
    :cond_4
    if-eqz v2, :cond_5

    .line 147
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface/range {v0 .. v8}, Lcom/google/android/videos/ContentNotificationManager$Processor;->processNewEpisodesForAccountAndShow(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/util/List;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 151
    :cond_5
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface {v0}, Lcom/google/android/videos/ContentNotificationManager$Processor;->finishProcessing()V

    goto :goto_0
.end method

.method private updateIsNewNotification(Ljava/lang/String;[Ljava/lang/String;)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;

    .prologue
    const/4 v6, 0x0

    .line 199
    iget-object v3, p0, Lcom/google/android/videos/ContentNotificationManager;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v3}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v2

    .line 200
    .local v2, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v1, 0x0

    .line 202
    .local v1, "success":Z
    :try_start_0
    array-length v3, p2

    add-int/lit8 v3, v3, 0x1

    new-array v0, v3, [Ljava/lang/String;

    .line 203
    .local v0, "args":[Ljava/lang/String;
    const/4 v3, 0x0

    aput-object p1, v0, v3

    .line 204
    const/4 v3, 0x0

    const/4 v4, 0x1

    array-length v5, p2

    invoke-static {p2, v3, v0, v4, v5}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 205
    array-length v3, p2

    invoke-static {v3}, Lcom/google/android/videos/ContentNotificationManager;->getSqlUpdateIsNewNotification(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3, v0}, Landroid/database/sqlite/SQLiteDatabase;->execSQL(Ljava/lang/String;[Ljava/lang/Object;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 206
    const/4 v1, 0x1

    .line 208
    iget-object v3, p0, Lcom/google/android/videos/ContentNotificationManager;->database:Lcom/google/android/videos/store/Database;

    new-array v4, v6, [Ljava/lang/Object;

    invoke-virtual {v3, v2, v1, v6, v4}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 210
    return-void

    .line 208
    .end local v0    # "args":[Ljava/lang/String;
    :catchall_0
    move-exception v3

    iget-object v4, p0, Lcom/google/android/videos/ContentNotificationManager;->database:Lcom/google/android/videos/store/Database;

    new-array v5, v6, [Ljava/lang/Object;

    invoke-virtual {v4, v2, v1, v6, v5}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v3
.end method


# virtual methods
.method public declared-synchronized checkForNewEpisodes()V
    .locals 12

    .prologue
    .line 66
    monitor-enter p0

    :try_start_0
    iget-boolean v1, p0, Lcom/google/android/videos/ContentNotificationManager;->updateInProgress:Z

    if-eqz v1, :cond_0

    .line 67
    const/4 v1, 0x1

    iput-boolean v1, p0, Lcom/google/android/videos/ContentNotificationManager;->pendingUpdateRequest:Z
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 81
    :goto_0
    monitor-exit p0

    return-void

    .line 69
    :cond_0
    const/4 v1, 0x1

    :try_start_1
    iput-boolean v1, p0, Lcom/google/android/videos/ContentNotificationManager;->updateInProgress:Z

    .line 70
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    iget-object v1, p0, Lcom/google/android/videos/ContentNotificationManager;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->maxNewContentNotificationDelayMillis()J

    move-result-wide v4

    sub-long/2addr v2, v4

    const-wide/16 v4, 0x3e8

    div-long v10, v2, v4

    .line 72
    .local v10, "oldestNewDateSeconds":J
    new-instance v0, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    const/4 v1, 0x0

    const-string v2, "purchased_assets, videos, seasons, shows ON asset_type = 20 AND asset_id = video_id AND episode_season_id = season_id AND show_id = shows_id"

    sget-object v3, Lcom/google/android/videos/ContentNotificationManager$NewEpisodesQuery;->PROJECTION:[Ljava/lang/String;

    const-string v4, "rating_id"

    const-string v5, "publish_timestamp IS NOT NULL AND publish_timestamp > ? AND is_new_notification_dismissed = 0 AND last_playback_start_timestamp = 0 AND EXISTS (SELECT * FROM purchased_assets AS PA WHERE  PA.account = purchased_assets.account AND  PA.asset_type = 19 AND  PA.asset_id = episode_season_id AND  PA.purchase_timestamp_seconds < publish_timestamp)"

    const/4 v6, 0x1

    new-array v6, v6, [Ljava/lang/String;

    const/4 v7, 0x0

    invoke-static {v10, v11}, Ljava/lang/String;->valueOf(J)Ljava/lang/String;

    move-result-object v8

    aput-object v8, v6, v7

    const/4 v7, 0x0

    const-string v8, "account, show_id, CAST(season_title AS INTEGER) DESC, season_title DESC, CAST(episode_number_text AS INTEGER), episode_number_text"

    const/4 v9, -0x1

    invoke-direct/range {v0 .. v9}, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;-><init>(ZLjava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    .line 79
    .local v0, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    iget-object v1, p0, Lcom/google/android/videos/ContentNotificationManager;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v1, v0, p0}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 66
    .end local v0    # "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .end local v10    # "oldestNewDateSeconds":J
    :catchall_0
    move-exception v1

    monitor-exit p0

    throw v1
.end method

.method public dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "episodeIds"    # [Ljava/lang/String;
    .param p4, "sendGcmNotification"    # Z

    .prologue
    .line 162
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p3, :cond_0

    array-length v0, p3

    if-nez v0, :cond_1

    .line 178
    :cond_0
    :goto_0
    return-void

    .line 167
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    if-eqz v0, :cond_2

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface {v0, p1, p2, p3}, Lcom/google/android/videos/ContentNotificationManager$Processor;->dismissContentNotification(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)V

    .line 171
    :cond_2
    invoke-direct {p0, p1, p3}, Lcom/google/android/videos/ContentNotificationManager;->updateIsNewNotification(Ljava/lang/String;[Ljava/lang/String;)V

    .line 173
    if-eqz p4, :cond_3

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;

    invoke-static {p1, p2, p3}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->createDismissMessageBundle(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/gcm/GcmSender;->sendUserNotification(Ljava/lang/String;Landroid/os/Bundle;)V

    .line 177
    :cond_3
    invoke-virtual {p0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    goto :goto_0
.end method

.method public dismissContentNotification(Ljava/lang/String;[Ljava/lang/String;Z)V
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoIds"    # [Ljava/lang/String;
    .param p3, "sendGcmNotification"    # Z

    .prologue
    .line 182
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    if-eqz p2, :cond_0

    array-length v0, p2

    if-nez v0, :cond_1

    .line 196
    :cond_0
    :goto_0
    return-void

    .line 186
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    if-eqz v0, :cond_2

    .line 187
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    invoke-interface {v0, p1, p2}, Lcom/google/android/videos/ContentNotificationManager$Processor;->dismissContentNotification(Ljava/lang/String;[Ljava/lang/String;)V

    .line 190
    :cond_2
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ContentNotificationManager;->updateIsNewNotification(Ljava/lang/String;[Ljava/lang/String;)V

    .line 192
    if-eqz p3, :cond_0

    .line 193
    iget-object v0, p0, Lcom/google/android/videos/ContentNotificationManager;->gcmSender:Lcom/google/android/videos/gcm/GcmSender;

    invoke-static {p1, p2}, Lcom/google/android/videos/gcm/GcmMessageReceiver;->createDismissMessageBundle(Ljava/lang/String;[Ljava/lang/String;)Landroid/os/Bundle;

    move-result-object v1

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/gcm/GcmSender;->sendUserNotification(Ljava/lang/String;Landroid/os/Bundle;)V

    goto :goto_0
.end method

.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 105
    const-string v0, "Error when fetching new episode entries."

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 106
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ContentNotificationManager;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 90
    :try_start_0
    invoke-direct {p0, p2}, Lcom/google/android/videos/ContentNotificationManager;->processCursor(Landroid/database/Cursor;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    .line 94
    monitor-enter p0

    .line 95
    const/4 v0, 0x0

    :try_start_1
    iput-boolean v0, p0, Lcom/google/android/videos/ContentNotificationManager;->updateInProgress:Z

    .line 96
    iget-boolean v0, p0, Lcom/google/android/videos/ContentNotificationManager;->pendingUpdateRequest:Z

    if-eqz v0, :cond_0

    .line 97
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ContentNotificationManager;->pendingUpdateRequest:Z

    .line 98
    invoke-virtual {p0}, Lcom/google/android/videos/ContentNotificationManager;->checkForNewEpisodes()V

    .line 100
    :cond_0
    monitor-exit p0
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 101
    return-void

    .line 92
    :catchall_0
    move-exception v0

    invoke-interface {p2}, Landroid/database/Cursor;->close()V

    throw v0

    .line 100
    :catchall_1
    move-exception v0

    :try_start_2
    monitor-exit p0
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v0
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/database/Cursor;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ContentNotificationManager;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;)V

    return-void
.end method

.method public setNotificationProcessor(Lcom/google/android/videos/ContentNotificationManager$Processor;)V
    .locals 0
    .param p1, "processor"    # Lcom/google/android/videos/ContentNotificationManager$Processor;

    .prologue
    .line 84
    iput-object p1, p0, Lcom/google/android/videos/ContentNotificationManager;->processor:Lcom/google/android/videos/ContentNotificationManager$Processor;

    .line 85
    return-void
.end method
