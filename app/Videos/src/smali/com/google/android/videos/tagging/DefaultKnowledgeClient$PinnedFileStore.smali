.class final Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;
.super Lcom/google/android/videos/store/AbstractFileStore;
.source "DefaultKnowledgeClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "PinnedFileStore"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<S:",
        "Ljava/lang/Object;",
        ">",
        "Lcom/google/android/videos/store/AbstractFileStore",
        "<",
        "Ljava/lang/String;",
        "TS;>;"
    }
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/cache/Converter",
            "<TS;>;)V"
        }
    .end annotation

    .prologue
    .line 396
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;, "Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore<TS;>;"
    .local p2, "converter":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<TS;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    .line 397
    return-void
.end method


# virtual methods
.method public bridge synthetic generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 393
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;, "Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore<TS;>;"
    check-cast p2, Ljava/lang/String;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;->generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method public generateFilepath(Ljava/io/File;Ljava/lang/String;)Ljava/io/File;
    .locals 2
    .param p1, "rootFileDir"    # Ljava/io/File;
    .param p2, "key"    # Ljava/lang/String;

    .prologue
    .line 401
    .local p0, "this":Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore;, "Lcom/google/android/videos/tagging/DefaultKnowledgeClient$PinnedFileStore<TS;>;"
    new-instance v0, Ljava/io/File;

    const-string v1, "knowledge"

    invoke-direct {v0, p1, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    .line 402
    .local v0, "directory":Ljava/io/File;
    new-instance v1, Ljava/io/File;

    invoke-direct {v1, v0, p2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v1
.end method
