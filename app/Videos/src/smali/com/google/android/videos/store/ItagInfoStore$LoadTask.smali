.class Lcom/google/android/videos/store/ItagInfoStore$LoadTask;
.super Ljava/lang/Object;
.source "ItagInfoStore.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/ItagInfoStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "LoadTask"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;"
        }
    .end annotation
.end field

.field private final request:I

.field final synthetic this$0:Lcom/google/android/videos/store/ItagInfoStore;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/ItagInfoStore;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p2, "request"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/async/Callback",
            "<-",
            "Ljava/lang/Integer;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 166
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<-Ljava/lang/Integer;Ljava/lang/Void;>;"
    iput-object p1, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 167
    iput p2, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->request:I

    .line 168
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Callback;

    iput-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->callback:Lcom/google/android/videos/async/Callback;

    .line 169
    return-void
.end method


# virtual methods
.method public run()V
    .locals 13

    .prologue
    const/4 v3, 0x0

    .line 173
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    # getter for: Lcom/google/android/videos/store/ItagInfoStore;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/ItagInfoStore;->access$000(Lcom/google/android/videos/store/ItagInfoStore;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "video_formats"

    sget-object v2, Lcom/google/android/videos/store/ItagInfoStore$LoadQuery;->PROJECTION:[Ljava/lang/String;

    move-object v4, v3

    move-object v5, v3

    move-object v6, v3

    move-object v7, v3

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v11

    .line 176
    .local v11, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v11}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 177
    const/4 v0, 0x0

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v12

    .line 178
    .local v12, "itag":I
    new-instance v4, Lcom/google/android/videos/streams/ItagInfo;

    const/4 v0, 0x1

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v5

    const/4 v0, 0x2

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v6

    const/4 v0, 0x4

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v7

    const/4 v0, 0x5

    invoke-static {v11, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v8

    const/4 v0, 0x3

    invoke-interface {v11, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v9

    const/4 v0, 0x6

    invoke-static {v11, v0}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v10

    invoke-direct/range {v4 .. v10}, Lcom/google/android/videos/streams/ItagInfo;-><init>(IIIZIZ)V

    .line 186
    .local v4, "info":Lcom/google/android/videos/streams/ItagInfo;
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->this$0:Lcom/google/android/videos/store/ItagInfoStore;

    # getter for: Lcom/google/android/videos/store/ItagInfoStore;->itagsData:Ljava/util/concurrent/ConcurrentMap;
    invoke-static {v0}, Lcom/google/android/videos/store/ItagInfoStore;->access$100(Lcom/google/android/videos/store/ItagInfoStore;)Ljava/util/concurrent/ConcurrentMap;

    move-result-object v0

    invoke-static {v12}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v4}, Ljava/util/concurrent/ConcurrentMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 189
    .end local v4    # "info":Lcom/google/android/videos/streams/ItagInfo;
    .end local v12    # "itag":I
    :catchall_0
    move-exception v0

    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v11}, Landroid/database/Cursor;->close()V

    .line 191
    iget-object v0, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->callback:Lcom/google/android/videos/async/Callback;

    iget v1, p0, Lcom/google/android/videos/store/ItagInfoStore$LoadTask;->request:I

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1, v3}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 192
    return-void
.end method
