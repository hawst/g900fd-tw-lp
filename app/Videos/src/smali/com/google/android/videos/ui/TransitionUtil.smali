.class public Lcom/google/android/videos/ui/TransitionUtil;
.super Ljava/lang/Object;
.source "TransitionUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/TransitionUtil$ZeroStartDelayPropagation;,
        Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;,
        Lcom/google/android/videos/ui/TransitionUtil$MoveTopLeft;,
        Lcom/google/android/videos/ui/TransitionUtil$InfoBounds;,
        Lcom/google/android/videos/ui/TransitionUtil$CornerPathMotion;,
        Lcom/google/android/videos/ui/TransitionUtil$ExpandTransition;,
        Lcom/google/android/videos/ui/TransitionUtil$ImageTintTransition;,
        Lcom/google/android/videos/ui/TransitionUtil$CircularSplash;
    }
.end annotation


# direct methods
.method public static createTransitionSafeCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "centerX"    # I
    .param p2, "centerY"    # I
    .param p3, "startRadius"    # F
    .param p4, "endRadius"    # F

    .prologue
    .line 204
    invoke-static {p0, p1, p2, p3, p4}, Landroid/view/ViewAnimationUtils;->createCircularReveal(Landroid/view/View;IIFF)Landroid/animation/Animator;

    move-result-object v0

    .line 206
    .local v0, "reveal":Landroid/animation/Animator;
    new-instance v1, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;

    invoke-direct {v1, v0}, Lcom/google/android/play/utils/NoPauseAnimatorWrapper;-><init>(Landroid/animation/Animator;)V

    return-object v1
.end method

.method public static encodeAndSetTransitionName(Landroid/view/View;ILjava/lang/String;)V
    .locals 2
    .param p0, "view"    # Landroid/view/View;
    .param p1, "transition"    # I
    .param p2, "extra"    # Ljava/lang/String;

    .prologue
    .line 49
    sget v0, Lcom/google/android/exoplayer/util/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 50
    invoke-virtual {p0}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0, p1, p2}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p0, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 52
    :cond_0
    return-void
.end method

.method public static encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "transition"    # I
    .param p2, "extra"    # Ljava/lang/String;

    .prologue
    .line 43
    invoke-virtual {p0, p1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 44
    .local v0, "element":Ljava/lang/String;
    const-string v1, ":"

    invoke-virtual {v0, v1}, Ljava/lang/String;->contains(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-static {v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 45
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ":"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1

    .line 44
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
