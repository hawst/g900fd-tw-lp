.class Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;
.super Lcom/google/android/videos/pinning/LegacyLicenseTask;
.source "LegacyLicenseTask.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/LegacyLicenseTask;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = "Release"
.end annotation


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;)V
    .locals 13
    .param p1, "key"    # Lcom/google/android/videos/pinning/DownloadKey;
    .param p2, "wakeLock"    # Landroid/os/PowerManager$WakeLock;
    .param p3, "wifiLock"    # Landroid/net/wifi/WifiManager$WifiLock;
    .param p4, "listener"    # Lcom/google/android/videos/pinning/Task$Listener;
    .param p5, "database"    # Lcom/google/android/videos/store/Database;
    .param p6, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p7, "accountManagerWrapper"    # Lcom/google/android/videos/accounts/AccountManagerWrapper;
    .param p8, "legacyDownloadsHaveAppLevelDrm"    # Z
    .param p9, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p10, "itagInfoStore"    # Lcom/google/android/videos/store/ItagInfoStore;

    .prologue
    .line 287
    const/4 v1, 0x1

    const/4 v12, 0x0

    move-object v0, p0

    move-object v2, p1

    move-object v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move-object/from16 v7, p6

    move-object/from16 v8, p7

    move/from16 v9, p8

    move-object/from16 v10, p9

    move-object/from16 v11, p10

    invoke-direct/range {v0 .. v12}, Lcom/google/android/videos/pinning/LegacyLicenseTask;-><init>(ILcom/google/android/videos/pinning/DownloadKey;Landroid/os/PowerManager$WakeLock;Landroid/net/wifi/WifiManager$WifiLock;Lcom/google/android/videos/pinning/Task$Listener;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/accounts/AccountManagerWrapper;ZLcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/ItagInfoStore;Lcom/google/android/videos/pinning/LegacyLicenseTask$1;)V

    .line 289
    return-void
.end method

.method private clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V
    .locals 6
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;

    .prologue
    .line 329
    invoke-static {}, Lcom/google/android/videos/pinning/PinningDbHelper;->getClearedLicenseContentValues()Landroid/content/ContentValues;

    move-result-object v0

    .line 330
    .local v0, "values":Landroid/content/ContentValues;
    if-eqz p1, :cond_0

    iget-wide v2, p1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->mergedExpirationTimestamp:J

    const-wide v4, 0x7fffffffffffffffL

    cmp-long v1, v2, v4

    if-eqz v1, :cond_0

    .line 335
    const-string v1, "expiration_timestamp_seconds"

    iget-wide v2, p1, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;->mergedExpirationTimestamp:J

    const-wide/16 v4, 0x3e8

    div-long/2addr v2, v4

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 338
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v1, Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v2, v1, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 339
    return-void
.end method

.method private logError(Ljava/lang/Throwable;)V
    .locals 3
    .param p1, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 342
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->getThrowableToLog(Ljava/lang/Throwable;)Ljava/lang/Throwable;

    move-result-object v2

    invoke-interface {v1, v0, v2}, Lcom/google/android/videos/logging/EventLogger;->onLicenseReleaseError(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 343
    return-void
.end method


# virtual methods
.method protected buildRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p3, "ids"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .prologue
    .line 293
    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-static {p1, p2, v0, p3}, Lcom/google/android/videos/drm/DrmRequest;->createUnpinRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v0

    return-object v0
.end method

.method protected onCompleted()V
    .locals 2

    .prologue
    .line 349
    iget-object v1, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v0, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    check-cast v0, Lcom/google/android/videos/pinning/DownloadKey;

    iget-object v0, v0, Lcom/google/android/videos/pinning/DownloadKey;->videoId:Ljava/lang/String;

    invoke-interface {v1, v0}, Lcom/google/android/videos/logging/EventLogger;->onLicenseReleaseCompleted(Ljava/lang/String;)V

    .line 350
    return-void
.end method

.method protected onError(Ljava/lang/Throwable;ZZ)V
    .locals 0
    .param p1, "cause"    # Ljava/lang/Throwable;
    .param p2, "maxRetries"    # Z
    .param p3, "fatal"    # Z

    .prologue
    .line 354
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->logError(Ljava/lang/Throwable;)V

    .line 355
    return-void
.end method

.method protected onRequestCompleted(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Lcom/google/android/videos/drm/DrmResponse;)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "response"    # Lcom/google/android/videos/drm/DrmResponse;

    .prologue
    .line 299
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V

    .line 300
    return-void
.end method

.method protected onRequestError(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 4
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/pinning/Task$TaskException;
        }
    .end annotation

    .prologue
    .line 304
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmFallbackException;

    if-eqz v1, :cond_0

    .line 305
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    .line 314
    :goto_0
    return-void

    .line 309
    :cond_0
    instance-of v1, p2, Lcom/google/android/videos/drm/DrmException;

    if-eqz v1, :cond_2

    move-object v0, p2

    .line 310
    check-cast v0, Lcom/google/android/videos/drm/DrmException;

    .line 311
    .local v0, "drmException":Lcom/google/android/videos/drm/DrmException;
    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->KEY_VERIFICATION_FAILED:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-eq v1, v2, :cond_1

    iget-object v1, v0, Lcom/google/android/videos/drm/DrmException;->drmError:Lcom/google/android/videos/drm/DrmException$DrmError;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    if-ne v1, v2, :cond_2

    .line 313
    :cond_1
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V

    goto :goto_0

    .line 318
    .end local v0    # "drmException":Lcom/google/android/videos/drm/DrmException;
    :cond_2
    new-instance v1, Lcom/google/android/videos/pinning/Task$TaskException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Unable to release "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->key:Lcom/google/android/videos/pinning/Task$Key;

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2, p2}, Lcom/google/android/videos/pinning/Task$TaskException;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    throw v1
.end method

.method protected onRequestImpossible(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;Ljava/lang/Throwable;)V
    .locals 0
    .param p1, "details"    # Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;
    .param p2, "cause"    # Ljava/lang/Throwable;

    .prologue
    .line 324
    invoke-direct {p0, p1}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->clearLicense(Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetails;)V

    .line 325
    invoke-direct {p0, p2}, Lcom/google/android/videos/pinning/LegacyLicenseTask$Release;->logError(Ljava/lang/Throwable;)V

    .line 326
    return-void
.end method
