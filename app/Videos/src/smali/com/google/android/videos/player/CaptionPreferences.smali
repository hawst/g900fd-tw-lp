.class public abstract Lcom/google/android/videos/player/CaptionPreferences;
.super Ljava/lang/Object;
.source "CaptionPreferences.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/CaptionPreferences$V21;,
        Lcom/google/android/videos/player/CaptionPreferences$V19;,
        Lcom/google/android/videos/player/CaptionPreferences$V8;
    }
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 125
    return-void
.end method

.method public static create(Landroid/content/Context;Landroid/content/SharedPreferences;)Lcom/google/android/videos/player/CaptionPreferences;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 35
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 36
    new-instance v0, Lcom/google/android/videos/player/CaptionPreferences$V21;

    invoke-direct {v0, p0}, Lcom/google/android/videos/player/CaptionPreferences$V21;-><init>(Landroid/content/Context;)V

    .line 40
    :goto_0
    return-object v0

    .line 37
    :cond_0
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x13

    if-lt v0, v1, :cond_1

    .line 38
    new-instance v0, Lcom/google/android/videos/player/CaptionPreferences$V19;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/player/CaptionPreferences$V19;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0

    .line 40
    :cond_1
    new-instance v0, Lcom/google/android/videos/player/CaptionPreferences$V8;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/player/CaptionPreferences$V8;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    goto :goto_0
.end method


# virtual methods
.method public abstract getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;
.end method

.method public abstract getFontScale()F
.end method

.method public abstract getLocale()Ljava/util/Locale;
.end method

.method public abstract isEnabled()Z
.end method
