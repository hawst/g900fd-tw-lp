.class public Lcom/google/android/videos/pano/model/UriItem;
.super Ljava/lang/Object;
.source "UriItem.java"

# interfaces
.implements Lcom/google/android/repolib/common/Entity;


# instance fields
.field private final entityId:Ljava/lang/String;

.field private hashCode:I

.field private final iconTitle:I

.field private final imageResource:I

.field private final uri:Ljava/lang/String;

.field private final viewTitle:I


# direct methods
.method public constructor <init>(IILjava/lang/String;I)V
    .locals 1
    .param p1, "imageResource"    # I
    .param p2, "iconTitle"    # I
    .param p3, "uri"    # Ljava/lang/String;
    .param p4, "viewTitle"    # I

    .prologue
    .line 24
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    .line 25
    iput p1, p0, Lcom/google/android/videos/pano/model/UriItem;->imageResource:I

    .line 26
    iput p2, p0, Lcom/google/android/videos/pano/model/UriItem;->iconTitle:I

    .line 27
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/model/UriItem;->uri:Ljava/lang/String;

    .line 28
    iput p4, p0, Lcom/google/android/videos/pano/model/UriItem;->viewTitle:I

    .line 29
    invoke-static {p2}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/model/UriItem;->entityId:Ljava/lang/String;

    .line 30
    return-void
.end method


# virtual methods
.method public equals(Ljava/lang/Object;)Z
    .locals 5
    .param p1, "o"    # Ljava/lang/Object;

    .prologue
    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 47
    if-ne p0, p1, :cond_1

    .line 60
    :cond_0
    :goto_0
    return v1

    .line 51
    :cond_1
    instance-of v3, p1, Lcom/google/android/videos/pano/model/UriItem;

    if-nez v3, :cond_2

    move v1, v2

    .line 52
    goto :goto_0

    :cond_2
    move-object v0, p1

    .line 54
    check-cast v0, Lcom/google/android/videos/pano/model/UriItem;

    .line 56
    .local v0, "i":Lcom/google/android/videos/pano/model/UriItem;
    iget v3, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    if-eqz v3, :cond_3

    iget v3, v0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    if-eqz v3, :cond_3

    iget v3, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    iget v4, v0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    if-eq v3, v4, :cond_3

    move v1, v2

    .line 57
    goto :goto_0

    .line 60
    :cond_3
    iget v3, p0, Lcom/google/android/videos/pano/model/UriItem;->imageResource:I

    iget v4, v0, Lcom/google/android/videos/pano/model/UriItem;->imageResource:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/pano/model/UriItem;->iconTitle:I

    iget v4, v0, Lcom/google/android/videos/pano/model/UriItem;->iconTitle:I

    if-ne v3, v4, :cond_4

    iget v3, p0, Lcom/google/android/videos/pano/model/UriItem;->viewTitle:I

    iget v4, v0, Lcom/google/android/videos/pano/model/UriItem;->viewTitle:I

    if-ne v3, v4, :cond_4

    iget-object v3, p0, Lcom/google/android/videos/pano/model/UriItem;->uri:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/pano/model/UriItem;->uri:Ljava/lang/String;

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-nez v3, :cond_0

    :cond_4
    move v1, v2

    goto :goto_0
.end method

.method public getEntityId()Ljava/lang/String;
    .locals 1

    .prologue
    .line 83
    iget-object v0, p0, Lcom/google/android/videos/pano/model/UriItem;->entityId:Ljava/lang/String;

    return-object v0
.end method

.method public getIconTitle()I
    .locals 1

    .prologue
    .line 69
    iget v0, p0, Lcom/google/android/videos/pano/model/UriItem;->iconTitle:I

    return v0
.end method

.method public getImageResourceId()I
    .locals 1

    .prologue
    .line 65
    iget v0, p0, Lcom/google/android/videos/pano/model/UriItem;->imageResource:I

    return v0
.end method

.method public getUri()Ljava/lang/String;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/pano/model/UriItem;->uri:Ljava/lang/String;

    return-object v0
.end method

.method public getViewTitle()I
    .locals 1

    .prologue
    .line 73
    iget v0, p0, Lcom/google/android/videos/pano/model/UriItem;->viewTitle:I

    return v0
.end method

.method public hashCode()I
    .locals 3

    .prologue
    .line 34
    iget v1, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    if-nez v1, :cond_0

    .line 35
    const/16 v0, 0x11

    .line 36
    .local v0, "result":I
    iget v1, p0, Lcom/google/android/videos/pano/model/UriItem;->imageResource:I

    add-int/lit16 v0, v1, 0x20f

    .line 37
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/pano/model/UriItem;->iconTitle:I

    add-int v0, v1, v2

    .line 38
    mul-int/lit8 v1, v0, 0x1f

    iget v2, p0, Lcom/google/android/videos/pano/model/UriItem;->viewTitle:I

    add-int v0, v1, v2

    .line 39
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/pano/model/UriItem;->uri:Ljava/lang/String;

    invoke-virtual {v2}, Ljava/lang/String;->hashCode()I

    move-result v2

    add-int v0, v1, v2

    .line 40
    iput v0, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    .line 42
    .end local v0    # "result":I
    :cond_0
    iget v1, p0, Lcom/google/android/videos/pano/model/UriItem;->hashCode:I

    return v1
.end method
