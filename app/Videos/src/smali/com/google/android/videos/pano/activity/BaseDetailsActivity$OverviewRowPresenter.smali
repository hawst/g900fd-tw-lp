.class Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;
.super Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;
.source "BaseDetailsActivity.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "OverviewRowPresenter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V
    .locals 1

    .prologue
    .line 563
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .line 564
    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->descriptionPresenter:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;
    invoke-static {p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$900(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 565
    return-void
.end method


# virtual methods
.method protected onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V
    .locals 17
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;

    .prologue
    .line 569
    move-object/from16 v0, p2

    instance-of v15, v0, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    if-eqz v15, :cond_0

    move-object/from16 v15, p2

    .line 570
    check-cast v15, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    invoke-virtual {v15}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->updateBitmapIfLoaded()V

    .line 573
    :cond_0
    invoke-super/range {p0 .. p2}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->onBindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Ljava/lang/Object;)V

    .line 574
    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    const v16, 0x7f0f013e

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v3

    .line 575
    .local v3, "detailsFrame":Landroid/view/View;
    const-string v15, "hero"

    invoke-virtual {v3, v15}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 577
    const v15, 0x7f0f013f

    invoke-virtual {v3, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v4

    .line 578
    .local v4, "detailsOverview":Landroid/view/View;
    const v15, 0x7f0f0140

    invoke-virtual {v3, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v12

    check-cast v12, Landroid/widget/ImageView;

    .line 579
    .local v12, "poster":Landroid/widget/ImageView;
    const v15, 0x7f0f0143

    invoke-virtual {v3, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 581
    .local v1, "actions":Landroid/view/View;
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    const v16, 0x7f0b0077

    invoke-virtual/range {v15 .. v16}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v15

    invoke-virtual {v12, v15}, Landroid/widget/ImageView;->setTransitionName(Ljava/lang/String;)V

    .line 583
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    iget-boolean v15, v15, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-nez v15, :cond_1

    .line 586
    move-object/from16 v0, p1

    iget-object v15, v0, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    const v16, 0x7f0f013a

    invoke-virtual/range {v15 .. v16}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    .line 587
    .local v2, "body":Landroid/view/View;
    new-instance v15, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$1;

    move-object/from16 v0, p0

    invoke-direct {v15, v0, v2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$1;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;Landroid/view/View;)V

    invoke-virtual {v1, v15}, Landroid/view/View;->post(Ljava/lang/Runnable;)Z

    .line 667
    .end local v2    # "body":Landroid/view/View;
    :goto_0
    return-void

    .line 600
    :cond_1
    new-instance v15, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;

    move-object/from16 v0, p0

    invoke-direct {v15, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter$2;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;)V

    invoke-virtual {v3, v15}, Landroid/view/View;->addOnLayoutChangeListener(Landroid/view/View$OnLayoutChangeListener;)V

    .line 627
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    # getter for: Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->backgroundColor:I
    invoke-static {v15}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->access$1000(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)I

    move-result v15

    invoke-virtual {v4, v15}, Landroid/view/View;->setBackgroundColor(I)V

    .line 631
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v15}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e0050

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v10

    .line 634
    .local v10, "padding":I
    invoke-virtual {v3}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 635
    .local v11, "params":Landroid/view/ViewGroup$LayoutParams;
    const/4 v15, -0x2

    iput v15, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 637
    invoke-virtual {v4}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 638
    const/4 v15, -0x2

    iput v15, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 640
    const v15, 0x7f0f0141

    invoke-virtual {v3, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v11

    .line 641
    const/4 v15, -0x2

    iput v15, v11, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 643
    const v15, 0x7f0f0142

    invoke-virtual {v3, v15}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v15

    invoke-virtual {v15}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 645
    .local v5, "linearParams":Landroid/widget/LinearLayout$LayoutParams;
    iput v10, v5, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    .line 646
    const/4 v15, -0x2

    iput v15, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 650
    move-object/from16 v0, p0

    iget-object v15, v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;->this$0:Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    invoke-virtual {v15}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v15

    const v16, 0x7f0e004a

    invoke-virtual/range {v15 .. v16}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v8

    .line 652
    .local v8, "normalHeight":I
    move v6, v8

    .line 653
    .local v6, "newHeight":I
    invoke-virtual {v12}, Landroid/widget/ImageView;->getDrawable()Landroid/graphics/drawable/Drawable;

    move-result-object v13

    .line 654
    .local v13, "posterDrawable":Landroid/graphics/drawable/Drawable;
    if-eqz v13, :cond_2

    .line 655
    const/high16 v15, 0x3f800000    # 1.0f

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicWidth()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    mul-float v15, v15, v16

    invoke-virtual {v13}, Landroid/graphics/drawable/Drawable;->getIntrinsicHeight()I

    move-result v16

    move/from16 v0, v16

    int-to-float v0, v0

    move/from16 v16, v0

    div-float v14, v15, v16

    .line 657
    .local v14, "posterRatio":F
    int-to-float v15, v8

    mul-float/2addr v15, v14

    float-to-int v9, v15

    .line 658
    .local v9, "normalWidth":I
    sub-int v7, v9, v10

    .line 659
    .local v7, "newWidth":I
    mul-int v15, v7, v8

    div-int v6, v15, v9

    .line 662
    .end local v7    # "newWidth":I
    .end local v9    # "normalWidth":I
    .end local v14    # "posterRatio":F
    :cond_2
    invoke-virtual {v12}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v5

    .end local v5    # "linearParams":Landroid/widget/LinearLayout$LayoutParams;
    check-cast v5, Landroid/widget/LinearLayout$LayoutParams;

    .line 663
    .restart local v5    # "linearParams":Landroid/widget/LinearLayout$LayoutParams;
    iput v6, v5, Landroid/widget/LinearLayout$LayoutParams;->height:I

    .line 664
    iput v10, v5, Landroid/widget/LinearLayout$LayoutParams;->bottomMargin:I

    iput v10, v5, Landroid/widget/LinearLayout$LayoutParams;->topMargin:I

    iput v10, v5, Landroid/widget/LinearLayout$LayoutParams;->leftMargin:I

    .line 666
    const/16 v15, 0x8

    invoke-virtual {v1, v15}, Landroid/view/View;->setVisibility(I)V

    goto/16 :goto_0
.end method

.method protected onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V
    .locals 4
    .param p1, "holder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;

    .prologue
    const/4 v3, 0x0

    .line 672
    iget-object v1, p1, Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;->view:Landroid/view/View;

    const v2, 0x7f0f013a

    invoke-virtual {v1, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 673
    .local v0, "body":Landroid/view/View;
    invoke-virtual {v0, v3}, Landroid/view/View;->setClickable(Z)V

    .line 674
    invoke-virtual {v0, v3}, Landroid/view/View;->setFocusable(Z)V

    .line 676
    invoke-super {p0, p1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->onUnbindRowViewHolder(Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;)V

    .line 677
    return-void
.end method
