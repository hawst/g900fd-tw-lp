.class public abstract Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;
.super Ljava/lang/Object;
.source "RemoteControlListener.java"

# interfaces
.implements Lcom/google/android/videos/remote/RemoteControlListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemoteControlListener;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseRemoteControlListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 16
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onAudioTracksChanged(Ljava/util/List;I)V
    .locals 0
    .param p2, "selectedTrackIndex"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/wireless/android/video/magma/proto/AudioInfo;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 30
    .local p1, "audioTracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/wireless/android/video/magma/proto/AudioInfo;>;"
    return-void
.end method

.method public onDisconnected()V
    .locals 0

    .prologue
    .line 34
    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 0
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    .line 26
    return-void
.end method

.method public onRemoteControlError(ILcom/google/android/videos/utils/RetryAction;)V
    .locals 0
    .param p1, "error"    # I
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 18
    return-void
.end method

.method public onRemoteControlErrorCleared()V
    .locals 0

    .prologue
    .line 20
    return-void
.end method

.method public onSubtitleTracksChanged(Lcom/google/android/videos/remote/SubtitleTrackList;)V
    .locals 0
    .param p1, "subtitleTrackList"    # Lcom/google/android/videos/remote/SubtitleTrackList;

    .prologue
    .line 28
    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
    .locals 0
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    .line 32
    return-void
.end method
