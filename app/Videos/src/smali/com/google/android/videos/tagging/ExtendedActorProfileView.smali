.class public Lcom/google/android/videos/tagging/ExtendedActorProfileView;
.super Landroid/widget/FrameLayout;
.source "ExtendedActorProfileView.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/flow/ViewHolderCreator;
.implements Lcom/google/android/videos/store/StoreStatusMonitor$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/widget/FrameLayout;",
        "Lcom/google/android/repolib/observers/Updatable;",
        "Lcom/google/android/videos/flow/ViewHolderCreator",
        "<",
        "Landroid/support/v7/widget/RecyclerView$ViewHolder;",
        ">;",
        "Lcom/google/android/videos/store/StoreStatusMonitor$Listener;"
    }
.end annotation


# instance fields
.field private account:Ljava/lang/String;

.field private activity:Landroid/app/Activity;

.field private actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

.field private actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow",
            "<**>;"
        }
    .end annotation
.end field

.field private final avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

.field private binder:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;

.field private bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView",
            "<",
            "Landroid/widget/TextView;",
            ">;"
        }
    .end annotation
.end field

.field private final closeButton:Landroid/widget/ImageButton;

.field private entitiesViewGroup:Landroid/view/ViewGroup;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/adapter/ArrayDataSource",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Film;",
            ">;"
        }
    .end annotation
.end field

.field private filmographyHeaderBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

.field private isHiding:Z

.field private listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

.field private networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private final profileContent:Landroid/support/v7/widget/RecyclerView;

.field private final profileImageDimension:I

.field private final profileTopFlow:Lcom/google/android/videos/flow/SingleViewFlow;

.field private profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

.field private requester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

.field private transitionProfileImageView:Landroid/view/View;

.field private viewRoot:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 109
    const/4 v0, 0x0

    const/4 v1, 0x0

    invoke-direct {p0, p1, v0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 110
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 113
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 114
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 4
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    const/4 v3, 0x1

    .line 117
    invoke-direct {p0, p1, p2, p3}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 118
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const v2, 0x7f040039

    invoke-virtual {v1, v2, p0, v3}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    .line 119
    const v1, 0x7f0a00e8

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->setBackgroundResource(I)V

    .line 120
    const v1, 0x7f0f0108

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageButton;

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->closeButton:Landroid/widget/ImageButton;

    .line 121
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->closeButton:Landroid/widget/ImageButton;

    new-instance v2, Lcom/google/android/videos/tagging/ExtendedActorProfileView$1;

    invoke-direct {v2, p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$1;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V

    invoke-virtual {v1, v2}, Landroid/widget/ImageButton;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 127
    const v1, 0x7f0f0107

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v7/widget/RecyclerView;

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    .line 129
    new-instance v1, Lcom/google/android/videos/flow/SingleViewFlow;

    const v2, 0x7f04003a

    invoke-direct {v1, v2, p0}, Lcom/google/android/videos/flow/SingleViewFlow;-><init>(ILcom/google/android/videos/flow/ViewHolderCreator;)V

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    .line 130
    new-instance v0, Lcom/google/android/videos/flow/SequentialFlow;

    new-array v1, v3, [Lcom/google/android/videos/flow/Flow;

    const/4 v2, 0x0

    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v3, v1, v2

    invoke-direct {v0, v1}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    .line 131
    .local v0, "contentFlow":Lcom/google/android/videos/flow/SequentialFlow;
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v3, "ExtendedActorProfileView"

    invoke-direct {v2, v3}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 132
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/google/android/videos/flow/FlowAdapter;

    invoke-direct {v2, v0}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 133
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    new-instance v2, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v2}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 135
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/play/image/AvatarCropTransformation;->getFullAvatarCrop(Landroid/content/res/Resources;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    .line 136
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0e01f6

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v1

    iput v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileImageDimension:I

    .line 138
    new-instance v1, Lcom/google/android/videos/adapter/ArrayDataSource;

    invoke-direct {v1}, Lcom/google/android/videos/adapter/ArrayDataSource;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    .line 139
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/View;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    return-object v0
.end method

.method static synthetic access$002(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/View;)Landroid/view/View;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;
    .param p1, "x1"    # Landroid/view/View;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    return-object p1
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/support/v7/widget/RecyclerView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    return-object v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/play/image/AvatarCropTransformation;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;

    return-object v0
.end method

.method static synthetic access$1100(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileImageDimension:I

    return v0
.end method

.method static synthetic access$1200(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/async/Requester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->requester:Lcom/google/android/videos/async/Requester;

    return-object v0
.end method

.method static synthetic access$1300(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/app/Activity;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->activity:Landroid/app/Activity;

    return-object v0
.end method

.method static synthetic access$1400(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/logging/EventLogger;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    return-object v0
.end method

.method static synthetic access$1500(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->account:Ljava/lang/String;

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;
    .param p1, "x1"    # Z

    .prologue
    .line 70
    invoke-direct {p0, p1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->startHideTransitionStepTwo(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$502(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$600(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/view/ViewGroup;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    return-object v0
.end method

.method static synthetic access$602(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/ViewGroup;)Landroid/view/ViewGroup;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;
    .param p1, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    return-object p1
.end method

.method static synthetic access$700(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hideInternal()V

    return-void
.end method

.method static synthetic access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    return-object v0
.end method

.method static synthetic access$900(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .prologue
    .line 70
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    return-object v0
.end method

.method static synthetic access$902(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;)Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    .prologue
    .line 70
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    return-object p1
.end method

.method private createFadeTransition(I)Landroid/transition/Transition;
    .locals 2
    .param p1, "fadingMode"    # I

    .prologue
    .line 363
    new-instance v0, Landroid/transition/Fade;

    invoke-direct {v0, p1}, Landroid/transition/Fade;-><init>(I)V

    .line 364
    .local v0, "transition":Landroid/transition/Transition;
    const v1, 0x7f0f010d

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 365
    const v1, 0x7f0f0112

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 366
    const v1, 0x7f0f010a

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 367
    const v1, 0x7f0f010b

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 368
    const v1, 0x7f0f010c

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 369
    const v1, 0x7f0f0108

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 370
    const v1, 0x7f0f01f6

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 371
    return-object v0
.end method

.method private createProfileImageMoveTransition()Landroid/transition/Transition;
    .locals 6

    .prologue
    .line 376
    new-instance v0, Landroid/view/animation/PathInterpolator;

    const v2, 0x3ecccccd    # 0.4f

    const/4 v3, 0x0

    const v4, 0x3e4ccccd    # 0.2f

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v0, v2, v3, v4, v5}, Landroid/view/animation/PathInterpolator;-><init>(FFFF)V

    .line 377
    .local v0, "interpolator":Landroid/view/animation/Interpolator;
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 378
    .local v1, "transition":Landroid/transition/TransitionSet;
    new-instance v2, Landroid/transition/ChangeBounds;

    invoke-direct {v2}, Landroid/transition/ChangeBounds;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/ChangeBounds;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 379
    new-instance v2, Landroid/transition/ChangeTransform;

    invoke-direct {v2}, Landroid/transition/ChangeTransform;-><init>()V

    invoke-virtual {v2, v0}, Landroid/transition/ChangeTransform;->setInterpolator(Landroid/animation/TimeInterpolator;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 380
    const-string v2, "actor_image_transition"

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTarget(Ljava/lang/String;)Landroid/transition/TransitionSet;

    .line 381
    return-object v1
.end method

.method private hideInternal()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 385
    iput-boolean v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isHiding:Z

    .line 386
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    if-eqz v0, :cond_0

    .line 387
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    invoke-static {v0}, Lcom/google/android/videos/ui/BitmapLoader;->cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    .line 389
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    if-eqz v0, :cond_1

    .line 390
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 392
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    if-eqz v0, :cond_2

    .line 393
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->removeListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 395
    :cond_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    if-eqz v0, :cond_3

    .line 396
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;->onExtendedProfileVisibilityChanged(Landroid/view/View;Z)V

    .line 398
    :cond_3
    return-void
.end method

.method private showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V
    .locals 3
    .param p1, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p2, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const/4 v1, 0x0

    .line 201
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 202
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->requester:Lcom/google/android/videos/async/Requester;

    .line 203
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    if-eqz v0, :cond_0

    .line 204
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->setHideItems(Z)V

    .line 206
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->closeButton:Landroid/widget/ImageButton;

    invoke-virtual {v0, v1}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 207
    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->setVisibility(I)V

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->scrollToPosition(I)V

    .line 210
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/flow/SingleViewFlow;->notifyItemChanged()V

    .line 212
    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createOrUpdateActorVideosFlow(Z)V

    .line 213
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    if-eqz v0, :cond_3

    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_3

    .line 214
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    iget-object v2, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->filmography:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v2

    new-array v2, v2, [Lcom/google/android/videos/tagging/KnowledgeEntity$Film;

    invoke-interface {v1, v2}, Ljava/util/List;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/ArrayDataSource;->updateArray([Ljava/lang/Object;)V

    .line 218
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    if-eqz v0, :cond_1

    .line 219
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 221
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->update()V

    .line 222
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 223
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/StoreStatusMonitor;->addListener(Lcom/google/android/videos/store/StoreStatusMonitor$Listener;)V

    .line 224
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    if-eqz v0, :cond_2

    .line 225
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    const/4 v1, 0x1

    invoke-interface {v0, p0, v1}, Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;->onExtendedProfileVisibilityChanged(Landroid/view/View;Z)V

    .line 227
    :cond_2
    return-void

    .line 216
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/ArrayDataSource;->reset()V

    goto :goto_0
.end method

.method private startHideTransitionStepOne(Z)V
    .locals 5
    .param p1, "zeroDurationTransition"    # Z

    .prologue
    const/4 v4, 0x1

    .line 289
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHasTransientState(Z)V

    .line 290
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 291
    .local v0, "hideTransitionStepOne":Landroid/transition/TransitionSet;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 293
    new-instance v1, Landroid/transition/Slide;

    invoke-direct {v1}, Landroid/transition/Slide;-><init>()V

    .line 294
    .local v1, "slide":Landroid/transition/Transition;
    const v2, 0x7f0f00c8

    invoke-virtual {v1, v2}, Landroid/transition/Transition;->addTarget(I)Landroid/transition/Transition;

    .line 295
    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 297
    const/4 v2, 0x2

    invoke-direct {p0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createFadeTransition(I)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 298
    new-instance v2, Lcom/google/android/videos/tagging/ExtendedActorProfileView$4;

    invoke-direct {v2, p0, p1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$4;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Z)V

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 304
    if-eqz p1, :cond_0

    .line 305
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    .line 307
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    invoke-static {v2, v0}, Landroid/transition/TransitionManager;->beginDelayedTransition(Landroid/view/ViewGroup;Landroid/transition/Transition;)V

    .line 308
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    invoke-virtual {v2, v4}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->setHideItems(Z)V

    .line 309
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->hideViewsExceptProfileImage()V

    .line 310
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->closeButton:Landroid/widget/ImageButton;

    const/4 v3, 0x4

    invoke-virtual {v2, v3}, Landroid/widget/ImageButton;->setVisibility(I)V

    .line 311
    return-void
.end method

.method private startHideTransitionStepTwo(Z)V
    .locals 4
    .param p1, "zeroDurationTransition"    # Z

    .prologue
    const/4 v3, 0x1

    .line 315
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 316
    .local v0, "hideTransitionStepTwo":Landroid/transition/TransitionSet;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 317
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    iget-object v1, v1, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->itemView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->isAttachedToWindow()Z

    move-result v1

    if-eqz v1, :cond_3

    .line 318
    const-string v1, "actor_image_transition"

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_2

    .line 319
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createProfileImageMoveTransition()Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 337
    :cond_0
    :goto_0
    new-instance v1, Landroid/transition/Fade;

    invoke-direct {v1, v3}, Landroid/transition/Fade;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v1, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 338
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;

    invoke-direct {v1, p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$5;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 349
    if-eqz p1, :cond_1

    .line 350
    const-wide/16 v2, 0x0

    invoke-virtual {v0, v2, v3}, Landroid/transition/TransitionSet;->setDuration(J)Landroid/transition/TransitionSet;

    .line 352
    :cond_1
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedActorProfileView$6;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$6;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/transition/TransitionSet;)V

    invoke-virtual {p0, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->post(Ljava/lang/Runnable;)Z

    .line 359
    return-void

    .line 322
    :cond_2
    new-instance v1, Landroid/transition/Fade;

    const/4 v2, 0x2

    invoke-direct {v1, v2}, Landroid/transition/Fade;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->access$400(Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;)Landroid/widget/TextView;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto :goto_0

    .line 328
    :cond_3
    const-string v1, "actor_image_transition"

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v2}, Landroid/view/View;->getTransitionName()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    new-instance v1, Landroid/transition/Fade;

    invoke-direct {v1, v3}, Landroid/transition/Fade;-><init>(I)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    goto :goto_0
.end method

.method private startShowTransition(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;)V
    .locals 5
    .param p1, "transitionRoot"    # Landroid/widget/FrameLayout;
    .param p2, "transitionViewGroup"    # Landroid/view/ViewGroup;
    .param p3, "frame"    # Landroid/view/ViewGroup;
    .param p4, "image"    # Landroid/view/View;

    .prologue
    const/4 v4, 0x1

    .line 232
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    .line 233
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    .line 234
    iput-object p4, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    .line 235
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    const-string v3, "actor_image_transition"

    invoke-virtual {v2, v3}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 236
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    invoke-virtual {v2, v4}, Landroid/view/View;->setHasTransientState(Z)V

    .line 238
    new-instance v1, Landroid/transition/TransitionSet;

    invoke-direct {v1}, Landroid/transition/TransitionSet;-><init>()V

    .line 239
    .local v1, "showTransition":Landroid/transition/TransitionSet;
    invoke-virtual {v1, v4}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 241
    new-instance v0, Landroid/transition/TransitionSet;

    invoke-direct {v0}, Landroid/transition/TransitionSet;-><init>()V

    .line 242
    .local v0, "fadeMoveTransition":Landroid/transition/TransitionSet;
    const/4 v2, 0x0

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->setOrdering(I)Landroid/transition/TransitionSet;

    .line 243
    new-instance v2, Landroid/transition/Fade;

    const/4 v3, 0x2

    invoke-direct {v2, v3}, Landroid/transition/Fade;-><init>(I)V

    invoke-virtual {v2, p2}, Landroid/transition/Fade;->addTarget(Landroid/view/View;)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 244
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createProfileImageMoveTransition()Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 245
    invoke-virtual {v1, v0}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 247
    invoke-direct {p0, v4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createFadeTransition(I)Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addTransition(Landroid/transition/Transition;)Landroid/transition/TransitionSet;

    .line 249
    new-instance v2, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;

    invoke-direct {v2, p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$2;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)V

    invoke-virtual {v1, v2}, Landroid/transition/TransitionSet;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/TransitionSet;

    .line 256
    new-instance v2, Lcom/google/android/videos/tagging/ExtendedActorProfileView$3;

    invoke-direct {v2, p0, p1, v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$3;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/widget/FrameLayout;Landroid/transition/TransitionSet;)V

    invoke-virtual {p0, v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->post(Ljava/lang/Runnable;)Z

    .line 263
    return-void
.end method


# virtual methods
.method public createOrUpdateActorVideosFlow(Z)V
    .locals 8
    .param p1, "visible"    # Z

    .prologue
    .line 167
    new-instance v0, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmographyHeaderBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    const v3, 0x7f040022

    iget-object v4, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->binder:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->binder:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;

    invoke-virtual {v5}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;->getItemClickListener()Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$ItemClickListener;

    move-result-object v5

    const/4 v6, -0x1

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;-><init>(Lcom/google/android/videos/ui/ItemsWithHeadingFlow$SectionHeadingBinder;Lcom/google/android/videos/adapter/DataSource;ILcom/google/android/videos/ui/playnext/ClusterItemView$Binder;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;I)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->setVisible(Z)V

    .line 175
    new-instance v7, Lcom/google/android/videos/flow/SequentialFlow;

    const/4 v0, 0x2

    new-array v0, v0, [Lcom/google/android/videos/flow/Flow;

    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopFlow:Lcom/google/android/videos/flow/SingleViewFlow;

    aput-object v2, v0, v1

    const/4 v1, 0x1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    aput-object v2, v0, v1

    invoke-direct {v7, v0}, Lcom/google/android/videos/flow/SequentialFlow;-><init>([Lcom/google/android/videos/flow/Flow;)V

    .line 176
    .local v7, "contentFlow":Lcom/google/android/videos/flow/SequentialFlow;
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/flow/FlowAdapter;

    invoke-direct {v1, v7}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 177
    return-void
.end method

.method public bridge synthetic createViewHolder(ILandroid/view/ViewGroup;)Landroid/support/v7/widget/RecyclerView$ViewHolder;
    .locals 1
    .param p1, "x0"    # I
    .param p2, "x1"    # Landroid/view/ViewGroup;

    .prologue
    .line 70
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    move-result-object v0

    return-object v0
.end method

.method public createViewHolder(ILandroid/view/ViewGroup;)Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;
    .locals 3
    .param p1, "viewType"    # I
    .param p2, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 411
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {v1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v1

    const/4 v2, 0x0

    invoke-virtual {v1, p1, p2, v2}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;Z)Landroid/view/View;

    move-result-object v0

    .line 412
    .local v0, "itemView":Landroid/view/View;
    new-instance v1, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    invoke-direct {v1, p0, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;-><init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/View;)V

    iput-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    .line 413
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileTopViewHolder:Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    return-object v1
.end method

.method public hide(Z)V
    .locals 4
    .param p1, "withTransition"    # Z

    .prologue
    const/4 v0, 0x1

    const/4 v1, 0x0

    .line 266
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isHiding:Z

    if-nez v2, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isVisible()Z

    move-result v2

    if-nez v2, :cond_1

    .line 281
    :cond_0
    :goto_0
    return-void

    .line 269
    :cond_1
    iput-boolean v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isHiding:Z

    .line 270
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_2

    .line 271
    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    invoke-virtual {v2, v1}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 273
    :cond_2
    sget v2, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v3, 0x15

    if-lt v2, v3, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    if-eqz v2, :cond_3

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->transitionProfileImageView:Landroid/view/View;

    if-nez v2, :cond_4

    .line 274
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    invoke-virtual {v0, p0}, Landroid/view/ViewGroup;->removeView(Landroid/view/View;)V

    .line 275
    invoke-direct {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->hideInternal()V

    goto :goto_0

    .line 280
    :cond_4
    if-nez p1, :cond_5

    :goto_1
    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->startHideTransitionStepOne(Z)V

    goto :goto_0

    :cond_5
    move v0, v1

    goto :goto_1
.end method

.method public isVisible()Z
    .locals 1

    .prologue
    .line 401
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onInitKnowledge(Landroid/app/Activity;Ljava/lang/String;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/async/Requester;)V
    .locals 9
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p4, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p5, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/app/Activity;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p6, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    const/4 v8, 0x0

    const/4 v7, 0x0

    .line 148
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->activity:Landroid/app/Activity;

    .line 149
    iput-object p5, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 150
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->account:Ljava/lang/String;

    .line 151
    iput-object p3, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->storeStatusMonitor:Lcom/google/android/videos/store/StoreStatusMonitor;

    .line 152
    iput-object p4, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 153
    new-instance v0, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;

    move-object v1, p1

    move-object v2, p3

    move-object v3, p5

    move-object v4, p6

    move-object v5, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;-><init>(Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/async/Requester;Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->binder:Lcom/google/android/videos/tagging/ActorFilmographyClusterItemView$Binder;

    .line 155
    new-instance v0, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0b01d9

    move v3, v7

    move v4, v7

    move-object v5, v8

    move v6, v7

    move-object v7, v8

    invoke-direct/range {v0 .. v7}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;-><init>(Landroid/content/Context;IIILandroid/view/View$OnClickListener;ILjava/lang/Object;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmographyHeaderBinder:Lcom/google/android/videos/ui/ItemsWithHeadingFlow$StaticSectionHeadingBinder;

    .line 157
    return-void
.end method

.method public onOrientationChange()V
    .locals 1

    .prologue
    .line 160
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->isVisible()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 162
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->createOrUpdateActorVideosFlow(Z)V

    .line 164
    :cond_0
    return-void
.end method

.method public onStoreStatusChanged(Lcom/google/android/videos/store/StoreStatusMonitor;)V
    .locals 1
    .param p1, "sender"    # Lcom/google/android/videos/store/StoreStatusMonitor;

    .prologue
    .line 406
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;->notifyItemStatesChanged()V

    .line 407
    return-void
.end method

.method public setListener(Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    .prologue
    .line 142
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->listener:Lcom/google/android/videos/tagging/ExtendedProfileVisiblityListener;

    .line 143
    return-void
.end method

.method public show(Landroid/view/ViewGroup;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "viewRoot"    # Landroid/view/ViewGroup;
    .param p2, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/view/ViewGroup;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 193
    .local p3, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->viewRoot:Landroid/view/ViewGroup;

    .line 194
    invoke-virtual {p1, p0}, Landroid/view/ViewGroup;->addView(Landroid/view/View;)V

    .line 195
    invoke-direct {p0, p2, p3}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 196
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileContent:Landroid/support/v7/widget/RecyclerView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actorVideosFlow:Lcom/google/android/videos/ui/HideableItemsWithHeadingFlow;

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    .line 197
    return-void
.end method

.method public show(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V
    .locals 2
    .param p1, "viewRoot"    # Landroid/widget/FrameLayout;
    .param p2, "entitiesViewGroup"    # Landroid/view/ViewGroup;
    .param p3, "frame"    # Landroid/view/ViewGroup;
    .param p4, "image"    # Landroid/view/View;
    .param p5, "entity"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/FrameLayout;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/ViewGroup;",
            "Landroid/view/View;",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 181
    .local p6, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-ge v0, v1, :cond_0

    .line 182
    invoke-virtual {p0, p1, p5, p6}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->show(Landroid/view/ViewGroup;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 183
    iput-object p2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->entitiesViewGroup:Landroid/view/ViewGroup;

    .line 184
    const/16 v0, 0x8

    invoke-virtual {p2, v0}, Landroid/view/ViewGroup;->setVisibility(I)V

    .line 189
    :goto_0
    return-void

    .line 187
    :cond_0
    invoke-direct {p0, p5, p6}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->showInternal(Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Lcom/google/android/videos/async/Requester;)V

    .line 188
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->startShowTransition(Landroid/widget/FrameLayout;Landroid/view/ViewGroup;Landroid/view/ViewGroup;Landroid/view/View;)V

    goto :goto_0
.end method

.method public update()V
    .locals 2

    .prologue
    .line 418
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->filmDataSource:Lcom/google/android/videos/adapter/ArrayDataSource;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/ArrayDataSource;->setNetworkConnected(Z)V

    .line 419
    return-void
.end method
