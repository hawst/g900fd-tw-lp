.class public Lcom/google/android/videos/activity/WatchNowFragment;
.super Lcom/google/android/videos/activity/HomeFragment;
.source "WatchNowFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const/4 v0, 0x1

    invoke-direct {p0, v0}, Lcom/google/android/videos/activity/HomeFragment;-><init>(I)V

    .line 17
    return-void
.end method


# virtual methods
.method protected getHeaderBottomMargin()I
    .locals 1

    .prologue
    .line 68
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getHeaderBottomMargin(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getHeaderHeight()I
    .locals 1

    .prologue
    .line 63
    invoke-virtual {p0}, Lcom/google/android/videos/activity/WatchNowFragment;->getActivity()Landroid/support/v4/app/FragmentActivity;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/ui/WatchNowFlowHelper;->getHeaderHeight(Landroid/content/Context;)I

    move-result v0

    return v0
.end method

.method protected getTitleResourceId()I
    .locals 1

    .prologue
    .line 58
    const v0, 0x7f0b0096

    return v0
.end method

.method protected getUiElementType()I
    .locals 1

    .prologue
    .line 53
    const/4 v0, 0x1

    return v0
.end method

.method protected onCreateHelper(Lcom/google/android/videos/activity/HomeActivity;Lcom/google/android/videos/VideosGlobals;)Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
    .locals 27
    .param p1, "homeActivity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p2, "videosGlobals"    # Lcom/google/android/videos/VideosGlobals;

    .prologue
    .line 23
    new-instance v1, Lcom/google/android/videos/ui/WatchNowHelper;

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v2

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfigurationStore()Lcom/google/android/videos/store/ConfigurationStore;

    move-result-object v3

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v4

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/WatchNowFragment;->getView()Landroid/view/View;

    move-result-object v6

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v7

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v8

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v9

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v10

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v11

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getErrorHelper()Lcom/google/android/videos/utils/ErrorHelper;

    move-result-object v12

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSyncHelper()Lcom/google/android/videos/ui/SyncHelper;

    move-result-object v13

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getPinHelper()Lcom/google/android/videos/ui/PinHelper;

    move-result-object v14

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v15

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getCpuExecutor()Ljava/util/concurrent/ExecutorService;

    move-result-object v16

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v17

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getSuggestionsOverflowMenuHelper()Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;

    move-result-object v18

    invoke-virtual/range {p1 .. p1}, Lcom/google/android/videos/activity/HomeActivity;->getMediaRouteProvider()Lcom/google/android/videos/remote/MediaRouteProvider;

    move-result-object v19

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v20

    invoke-virtual/range {p0 .. p0}, Lcom/google/android/videos/activity/WatchNowFragment;->getDownloadedOnlyManager()Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v21

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v5

    invoke-interface {v5}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v22

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/activity/WatchNowFragment;->rootUiElementNode:Lcom/google/android/videos/logging/RootUiElementNode;

    move-object/from16 v23, v0

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getUiEventLoggingHelper()Lcom/google/android/videos/logging/UiEventLoggingHelper;

    move-result-object v24

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getAssetImageUriCreator()Lcom/google/android/videos/ui/AssetImageUriCreator;

    move-result-object v25

    invoke-virtual/range {p2 .. p2}, Lcom/google/android/videos/VideosGlobals;->getNetworkStatus()Lcom/google/android/videos/utils/NetworkStatus;

    move-result-object v26

    move-object/from16 v5, p1

    invoke-direct/range {v1 .. v26}, Lcom/google/android/videos/ui/WatchNowHelper;-><init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Landroid/content/SharedPreferences;Lcom/google/android/videos/activity/HomeActivity;Landroid/view/View;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Ljava/util/concurrent/Executor;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/ui/SuggestionOverflowMenuHelper;Lcom/google/android/videos/remote/MediaRouteProvider;Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/utils/DownloadedOnlyManager;ZLcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;Lcom/google/android/videos/ui/AssetImageUriCreator;Lcom/google/android/videos/utils/NetworkStatus;)V

    return-object v1
.end method
