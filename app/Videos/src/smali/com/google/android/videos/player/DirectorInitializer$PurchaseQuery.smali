.class interface abstract Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/DirectorInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "PurchaseQuery"
.end annotation


# static fields
.field public static final COLUMNS:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 953
    const/16 v0, 0x12

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "purchase_status"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807)"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "rental_short_timer_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "has_subtitles"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "default_subtitle_language"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "audio_track_languages"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "subtitle_mode"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "shows_title"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "purchase_type = 1"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "end_credit_start_seconds"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "poster_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "shows_poster_uri"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "license_cenc_pssh_data"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "license_cenc_mimetype"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "streaming_pssh_data"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "streaming_mimetype"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/player/DirectorInitializer$PurchaseQuery;->COLUMNS:[Ljava/lang/String;

    return-void
.end method
