.class public abstract Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;
.source "CursorHelper.java"

# interfaces
.implements Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "HomePurchaseCursorHelper"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        ">;",
        "Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;"
    }
.end annotation


# instance fields
.field private final downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "store"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p7, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 340
    invoke-direct/range {p0 .. p6}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/ui/SyncHelper;)V

    .line 341
    iput-object p7, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .line 342
    return-void
.end method


# virtual methods
.method public onDownloadedOnlyChanged(Z)V
    .locals 0
    .param p1, "downloadedOnly"    # Z

    .prologue
    .line 373
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->setDownloadedOnly(Z)V

    .line 374
    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 368
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 369
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 363
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->maybeScheduleUpdate(Ljava/lang/String;)V

    .line 364
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 346
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->onStart()V

    .line 347
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v0, :cond_0

    .line 348
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->registerObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 349
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->setDownloadedOnly(Z)V

    .line 351
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 1

    .prologue
    .line 355
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    if-eqz v0, :cond_0

    .line 356
    iget-object v0, p0, Lcom/google/android/videos/ui/CursorHelper$HomePurchaseCursorHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->unregisterObserver(Lcom/google/android/videos/utils/DownloadedOnlyManager$Listener;)V

    .line 358
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/ui/CursorHelper$HomeCursorHelper;->onStop()V

    .line 359
    return-void
.end method
