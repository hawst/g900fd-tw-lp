.class Lcom/google/android/videos/cache/InMemoryLruCache$1;
.super Ljava/util/LinkedHashMap;
.source "InMemoryLruCache.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/cache/InMemoryLruCache;-><init>(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/util/LinkedHashMap",
        "<TK;TE;>;"
    }
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/cache/InMemoryLruCache;


# direct methods
.method constructor <init>(Lcom/google/android/videos/cache/InMemoryLruCache;IFZ)V
    .locals 0
    .param p2, "x0"    # I
    .param p3, "x1"    # F
    .param p4, "x2"    # Z

    .prologue
    .line 42
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache$1;, "Lcom/google/android/videos/cache/InMemoryLruCache.1;"
    iput-object p1, p0, Lcom/google/android/videos/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-direct {p0, p2, p3, p4}, Ljava/util/LinkedHashMap;-><init>(IFZ)V

    return-void
.end method


# virtual methods
.method protected removeEldestEntry(Ljava/util/Map$Entry;)Z
    .locals 2
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/Map$Entry",
            "<TK;TE;>;)Z"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "this":Lcom/google/android/videos/cache/InMemoryLruCache$1;, "Lcom/google/android/videos/cache/InMemoryLruCache.1;"
    .local p1, "eldest":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<TK;TE;>;"
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/videos/cache/InMemoryLruCache;

    # getter for: Lcom/google/android/videos/cache/InMemoryLruCache;->map:Ljava/util/LinkedHashMap;
    invoke-static {v0}, Lcom/google/android/videos/cache/InMemoryLruCache;->access$000(Lcom/google/android/videos/cache/InMemoryLruCache;)Ljava/util/LinkedHashMap;

    move-result-object v0

    invoke-virtual {v0}, Ljava/util/LinkedHashMap;->size()I

    move-result v0

    iget-object v1, p0, Lcom/google/android/videos/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/videos/cache/InMemoryLruCache;

    # getter for: Lcom/google/android/videos/cache/InMemoryLruCache;->maxCacheSize:I
    invoke-static {v1}, Lcom/google/android/videos/cache/InMemoryLruCache;->access$100(Lcom/google/android/videos/cache/InMemoryLruCache;)I

    move-result v1

    if-le v0, v1, :cond_0

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/cache/InMemoryLruCache$1;->this$0:Lcom/google/android/videos/cache/InMemoryLruCache;

    invoke-interface {p1}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/cache/InMemoryLruCache;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    .line 48
    :cond_0
    const/4 v0, 0x0

    return v0
.end method
