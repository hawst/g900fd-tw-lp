.class Lcom/google/android/videos/remote/RemoteTracker$ControlListener;
.super Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;
.source "RemoteTracker.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/RemoteTracker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ControlListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/RemoteTracker;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;)V
    .locals 0

    .prologue
    .line 497
    iput-object p1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    invoke-direct {p0}, Lcom/google/android/videos/remote/RemoteControlListener$BaseRemoteControlListener;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Lcom/google/android/videos/remote/RemoteTracker$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "x1"    # Lcom/google/android/videos/remote/RemoteTracker$1;

    .prologue
    .line 497
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;-><init>(Lcom/google/android/videos/remote/RemoteTracker;)V

    return-void
.end method


# virtual methods
.method public onDisconnected()V
    .locals 2

    .prologue
    .line 527
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->maybeCacheResumeTimestamp()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$800(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 528
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    const/4 v1, 0x1

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V
    invoke-static {v0, v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$900(Lcom/google/android/videos/remote/RemoteTracker;Z)V

    .line 529
    return-void
.end method

.method public onPlayerStateChanged(Lcom/google/android/videos/remote/PlayerState;)V
    .locals 5
    .param p1, "playerState"    # Lcom/google/android/videos/remote/PlayerState;

    .prologue
    const/4 v2, 0x1

    .line 510
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->onPlayerStateChanged()V
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$1200(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 511
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->maybeCacheResumeTimestamp()V
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$800(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 513
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->isPlaying:Z
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$1300(Lcom/google/android/videos/remote/RemoteTracker;)Z

    move-result v0

    .line 514
    .local v0, "wasPlaying":Z
    iget-object v3, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    if-eqz p1, :cond_1

    iget v1, p1, Lcom/google/android/videos/remote/PlayerState;->state:I

    const/4 v4, 0x2

    if-ne v1, v4, :cond_1

    move v1, v2

    :goto_0
    # setter for: Lcom/google/android/videos/remote/RemoteTracker;->isPlaying:Z
    invoke-static {v3, v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$1302(Lcom/google/android/videos/remote/RemoteTracker;Z)Z

    .line 515
    if-eqz v0, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # getter for: Lcom/google/android/videos/remote/RemoteTracker;->isPlaying:Z
    invoke-static {v1}, Lcom/google/android/videos/remote/RemoteTracker;->access$1300(Lcom/google/android/videos/remote/RemoteTracker;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 516
    iget-object v1, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->flushResumeTimestampIfDirty(Z)V
    invoke-static {v1, v2}, Lcom/google/android/videos/remote/RemoteTracker;->access$900(Lcom/google/android/videos/remote/RemoteTracker;Z)V

    .line 518
    :cond_0
    return-void

    .line 514
    :cond_1
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onRemoteControlError(ILcom/google/android/videos/utils/RetryAction;)V
    .locals 1
    .param p1, "error"    # I
    .param p2, "retryAction"    # Lcom/google/android/videos/utils/RetryAction;

    .prologue
    .line 500
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->onErrorChanged()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$1100(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 501
    return-void
.end method

.method public onRemoteControlErrorCleared()V
    .locals 1

    .prologue
    .line 505
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->onErrorChanged()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$1100(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 506
    return-void
.end method

.method public onVideoInfoChanged(Lcom/google/android/videos/remote/RemoteVideoInfo;)V
    .locals 1
    .param p1, "videoInfo"    # Lcom/google/android/videos/remote/RemoteVideoInfo;

    .prologue
    .line 522
    iget-object v0, p0, Lcom/google/android/videos/remote/RemoteTracker$ControlListener;->this$0:Lcom/google/android/videos/remote/RemoteTracker;

    # invokes: Lcom/google/android/videos/remote/RemoteTracker;->onVideoInfoChanged()V
    invoke-static {v0}, Lcom/google/android/videos/remote/RemoteTracker;->access$1400(Lcom/google/android/videos/remote/RemoteTracker;)V

    .line 523
    return-void
.end method
