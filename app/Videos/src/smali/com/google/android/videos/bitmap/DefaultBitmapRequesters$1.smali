.class final Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$1;
.super Ljava/lang/Object;
.source "DefaultBitmapRequesters.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/bitmap/DefaultBitmapRequesters;->createBitmapBytesNetworkRequester(Lorg/apache/http/client/HttpClient;)Lcom/google/android/videos/async/Requester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Landroid/net/Uri;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>()V
    .locals 0

    .prologue
    .line 125
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 125
    check-cast p1, Landroid/net/Uri;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/bitmap/DefaultBitmapRequesters$1;->convertRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;

    .prologue
    .line 128
    invoke-static {p1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Landroid/net/Uri;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method
