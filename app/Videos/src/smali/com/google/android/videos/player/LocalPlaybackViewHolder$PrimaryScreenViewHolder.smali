.class public Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;
.super Ljava/lang/Object;
.source "LocalPlaybackViewHolder.java"

# interfaces
.implements Lcom/google/android/videos/player/LocalPlaybackViewHolder;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/LocalPlaybackViewHolder;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PrimaryScreenViewHolder"
.end annotation


# instance fields
.field private final playerView:Lcom/google/android/videos/player/PlayerView;

.field private final subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

.field private final taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

.field private final taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/PlayerView;Lcom/google/android/videos/player/overlay/SubtitlesOverlay;Lcom/google/android/videos/tagging/KnowledgeView;Lcom/google/android/videos/tagging/KnowledgeView;)V
    .locals 0
    .param p1, "playerView"    # Lcom/google/android/videos/player/PlayerView;
    .param p2, "subtitlesOverlay"    # Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .param p3, "taggingKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;
    .param p4, "taglessKnowledgeView"    # Lcom/google/android/videos/tagging/KnowledgeView;

    .prologue
    .line 53
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 54
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    .line 55
    iput-object p2, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    .line 56
    iput-object p3, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 57
    iput-object p4, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    .line 58
    return-void
.end method


# virtual methods
.method public getPlayerView()Lcom/google/android/videos/player/PlayerView;
    .locals 1

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    return-object v0
.end method

.method public getRemoteScreenInfoOverlay()Lcom/google/android/videos/player/overlay/RemoteScreenInfoOverlay;
    .locals 1

    .prologue
    .line 72
    const/4 v0, 0x0

    return-object v0
.end method

.method public getSubtitlesOverlay()Lcom/google/android/videos/player/overlay/SubtitlesOverlay;
    .locals 1

    .prologue
    .line 67
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->subtitlesOverlay:Lcom/google/android/videos/player/overlay/SubtitlesOverlay;

    return-object v0
.end method

.method public getTaggingKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->taggingKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    return-object v0
.end method

.method public getTaggingKnowledgeViewResources()Landroid/content/res/Resources;
    .locals 1

    .prologue
    .line 82
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->playerView:Lcom/google/android/videos/player/PlayerView;

    invoke-virtual {v0}, Lcom/google/android/videos/player/PlayerView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    return-object v0
.end method

.method public getTaglessKnowledgeView()Lcom/google/android/videos/tagging/KnowledgeView;
    .locals 1

    .prologue
    .line 87
    iget-object v0, p0, Lcom/google/android/videos/player/LocalPlaybackViewHolder$PrimaryScreenViewHolder;->taglessKnowledgeView:Lcom/google/android/videos/tagging/KnowledgeView;

    return-object v0
.end method

.method public release()V
    .locals 0

    .prologue
    .line 93
    return-void
.end method
