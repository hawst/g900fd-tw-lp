.class public abstract Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
.super Lcom/google/android/videos/pano/activity/DetailsActivity;
.source "BaseDetailsActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnActionClickedListener;
.implements Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
.implements Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;,
        Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;,
        Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;
    }
.end annotation


# instance fields
.field protected account:Ljava/lang/String;

.field private backgroundColor:I

.field private bodyHadFocus:Z

.field classPresenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

.field private database:Lcom/google/android/videos/store/Database;

.field private descriptionPresenter:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

.field private detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

.field private detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

.field private directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

.field private eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private finishAfterTransition:Z

.field private finishedInitialAdapterLoad:Z

.field fullAdapterContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;"
        }
    .end annotation
.end field

.field private handler:Landroid/os/Handler;

.field isTransitioning:Z

.field protected itemId:Ljava/lang/String;

.field private lastBackgroundUri:Ljava/lang/String;

.field private objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

.field pendingEnterTransition:Z

.field protected purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

.field rowAdapterContents:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;"
        }
    .end annotation
.end field

.field protected synopsisMode:Z

.field private updateNotifier:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;

.field protected videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 62
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;-><init>()V

    .line 102
    new-instance v0, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    invoke-direct {v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    .line 561
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Z
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition:Z

    return v0
.end method

.method static synthetic access$1000(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)I
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->backgroundColor:I

    return v0
.end method

.method static synthetic access$102(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition:Z

    return p1
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Landroid/os/Handler;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->handler:Landroid/os/Handler;

    return-object v0
.end method

.method static synthetic access$400(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Z)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setOverviewClipped(Z)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    return-object v0
.end method

.method static synthetic access$600(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Ljava/lang/String;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setBackground(Ljava/lang/String;)V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p1, "x1"    # I

    .prologue
    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapter(I)V

    return-void
.end method

.method static synthetic access$802(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Z)Z
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;
    .param p1, "x1"    # Z

    .prologue
    .line 62
    iput-boolean p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishedInitialAdapterLoad:Z

    return p1
.end method

.method static synthetic access$900(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity;

    .prologue
    .line 62
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->descriptionPresenter:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    return-object v0
.end method

.method private createIsTransitioningListener()Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
    .locals 1

    .prologue
    .line 263
    new-instance v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$5;

    invoke-direct {v0, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$5;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    return-object v0
.end method

.method private logDirectPurchaseResult(I)V
    .locals 7
    .param p1, "result"    # I

    .prologue
    .line 485
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    if-eqz v0, :cond_0

    .line 486
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget v1, v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->eventSource:I

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget v3, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->filteringType:I

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget-object v4, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->videoId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget-object v5, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->showId:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    iget-object v6, v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->seasonId:Ljava/lang/String;

    move v2, p1

    invoke-interface/range {v0 .. v6}, Lcom/google/android/videos/logging/EventLogger;->onDirectPurchase(IIILjava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    .line 489
    :cond_0
    return-void
.end method

.method public static markAsRunningAnimation(Landroid/content/Intent;)V
    .locals 2
    .param p0, "intent"    # Landroid/content/Intent;

    .prologue
    .line 80
    const-string v0, "run_animation"

    const/4 v1, 0x1

    invoke-virtual {p0, v0, v1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 81
    return-void
.end method

.method private setBackground(Ljava/lang/String;)V
    .locals 1
    .param p1, "screenshotUri"    # Ljava/lang/String;

    .prologue
    .line 492
    if-eqz p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->lastBackgroundUri:Ljava/lang/String;

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->lastBackgroundUri:Ljava/lang/String;

    invoke-virtual {v0, p1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 498
    :goto_0
    return-void

    .line 496
    :cond_0
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setBackgroundUri(Ljava/lang/String;)V

    .line 497
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->lastBackgroundUri:Ljava/lang/String;

    goto :goto_0
.end method

.method private setOverviewClipped(Z)V
    .locals 3
    .param p1, "enabled"    # Z

    .prologue
    .line 277
    const v1, 0x7f0f013f

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/view/ViewGroup;

    .line 278
    .local v0, "view":Landroid/view/ViewGroup;
    :goto_0
    if-eqz v0, :cond_0

    .line 279
    invoke-virtual {v0, p1}, Landroid/view/ViewGroup;->setClipChildren(Z)V

    .line 280
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getId()I

    move-result v1

    const v2, 0x7f0f0167

    if-ne v1, v2, :cond_1

    .line 285
    :cond_0
    return-void

    .line 283
    :cond_1
    invoke-virtual {v0}, Landroid/view/ViewGroup;->getParent()Landroid/view/ViewParent;

    move-result-object v0

    .end local v0    # "view":Landroid/view/ViewGroup;
    check-cast v0, Landroid/view/ViewGroup;

    .restart local v0    # "view":Landroid/view/ViewGroup;
    goto :goto_0
.end method

.method private setupNormalTransition()V
    .locals 2

    .prologue
    .line 181
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    const-string v1, "hero"

    invoke-virtual {v0, p0, v1}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setSharedElementEnterTransition(Landroid/app/Activity;Ljava/lang/String;)V

    .line 183
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 205
    return-void
.end method

.method private setupTransitionForSynopsis()V
    .locals 4

    .prologue
    .line 219
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$2;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$2;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 233
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/Window;->getSharedElementReturnTransition()Landroid/transition/Transition;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$3;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$3;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    invoke-virtual {v0, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 250
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->postponeEnterTransition()V

    .line 254
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->handler:Landroid/os/Handler;

    new-instance v1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$4;

    invoke-direct {v1, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$4;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    const-wide/16 v2, 0x1f4

    invoke-virtual {v0, v1, v2, v3}, Landroid/os/Handler;->postDelayed(Ljava/lang/Runnable;J)Z

    .line 260
    return-void
.end method

.method private updateAdapter(I)V
    .locals 2
    .param p1, "updateType"    # I

    .prologue
    .line 401
    new-instance v0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;I)V

    const/4 v1, 0x0

    new-array v1, v1, [Ljava/lang/Void;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$6;->execute([Ljava/lang/Object;)Landroid/os/AsyncTask;

    .line 468
    return-void
.end method

.method private updateAdapterIfReady()V
    .locals 1

    .prologue
    .line 383
    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->pendingEnterTransition:Z

    if-nez v0, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishedInitialAdapterLoad:Z

    if-eqz v0, :cond_0

    .line 384
    const/4 v0, 0x2

    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapter(I)V

    .line 386
    :cond_0
    return-void
.end method


# virtual methods
.method protected abstract addRows(Ljava/util/ArrayList;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Row;",
            ">;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;",
            ")V"
        }
    .end annotation
.end method

.method public finishAfterTransition()V
    .locals 1

    .prologue
    .line 289
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition:Z

    .line 290
    invoke-super {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->finishAfterTransition()V

    .line 291
    return-void
.end method

.method protected abstract getListItemHelper()Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.end method

.method protected abstract getMainItemId(Landroid/content/Intent;)Ljava/lang/String;
.end method

.method protected getObjectAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
    .locals 1

    .prologue
    .line 390
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    return-object v0
.end method

.method public onActionClicked(Landroid/support/v17/leanback/widget/Action;)V
    .locals 1
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 395
    instance-of v0, p1, Lcom/google/android/videos/pano/ui/ClickableAction;

    if-eqz v0, :cond_0

    .line 396
    check-cast p1, Lcom/google/android/videos/pano/ui/ClickableAction;

    .end local p1    # "action":Landroid/support/v17/leanback/widget/Action;
    invoke-virtual {p1, p0}, Lcom/google/android/videos/pano/ui/ClickableAction;->onClick(Landroid/app/Activity;)V

    .line 398
    :cond_0
    return-void
.end method

.method protected onActivityResult(IILandroid/content/Intent;)V
    .locals 2
    .param p1, "requestCode"    # I
    .param p2, "result"    # I
    .param p3, "data"    # Landroid/content/Intent;

    .prologue
    .line 472
    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    invoke-virtual {v1, p1, p2, p3}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;->onActivityResult(IILandroid/content/Intent;)Z

    move-result v1

    if-eqz v1, :cond_0

    .line 474
    const/4 v1, -0x1

    if-eq p2, v1, :cond_1

    .line 475
    const/16 v0, 0xe

    .line 480
    .local v0, "purchaseResult":I
    :goto_0
    invoke-direct {p0, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->logDirectPurchaseResult(I)V

    .line 482
    .end local v0    # "purchaseResult":I
    :cond_0
    return-void

    .line 477
    :cond_1
    const/4 v0, -0x1

    .line 478
    .restart local v0    # "purchaseResult":I
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->onPurchasesUpdated()V

    goto :goto_0
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstance"    # Landroid/os/Bundle;

    .prologue
    const/4 v6, 0x0

    const/4 v3, 0x0

    .line 126
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v0

    .line 127
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getMainItemId(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->itemId:Ljava/lang/String;

    .line 128
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 129
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 130
    new-instance v2, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStoreSync()Lcom/google/android/videos/store/PurchaseStoreSync;

    move-result-object v4

    invoke-direct {v2, v4}, Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;-><init>(Lcom/google/android/videos/store/PurchaseStoreSync;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->directPurchaseFlowResultHandler:Lcom/google/android/videos/utils/PlayStoreUtil$DirectPurchaseFlowResultHandler;

    .line 132
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    .line 133
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 134
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->chooseFirstAccount()Ljava/lang/String;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    .line 136
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 137
    invoke-super {p0, p1}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 138
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finish()V

    .line 178
    :goto_0
    return-void

    .line 141
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getListItemHelper()Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    .line 142
    const-string v2, "synopsis_mode"

    invoke-virtual {v0, v2, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    iput-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    .line 143
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getDatabase()Lcom/google/android/videos/store/Database;

    move-result-object v2

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    .line 144
    new-instance v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;

    invoke-direct {v2, p0, v6}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;Lcom/google/android/videos/pano/activity/BaseDetailsActivity$1;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateNotifier:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;

    .line 145
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateNotifier:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;

    invoke-virtual {v2, v4}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 146
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v4, 0x7f0a0079

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iput v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->backgroundColor:I

    .line 147
    new-instance v2, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v4

    invoke-direct {v2, p0, v4}, Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;-><init>(Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter$DetailsDescriptionListener;Lcom/google/android/videos/Config;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->descriptionPresenter:Lcom/google/android/videos/pano/ui/DetailsDescriptionPresenter;

    .line 149
    new-instance v2, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;

    invoke-direct {v2, p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$OverviewRowPresenter;-><init>(Lcom/google/android/videos/pano/activity/BaseDetailsActivity;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    .line 150
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    invoke-virtual {v2, p0}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setOnActionClickedListener(Landroid/support/v17/leanback/widget/OnActionClickedListener;)V

    .line 151
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    iget v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->backgroundColor:I

    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;->setBackgroundColor(I)V

    .line 152
    new-instance v2, Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-direct {v2}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->classPresenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    .line 153
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->classPresenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    const-class v4, Landroid/support/v17/leanback/widget/ListRow;

    new-instance v5, Landroid/support/v17/leanback/widget/ListRowPresenter;

    invoke-direct {v5}, Landroid/support/v17/leanback/widget/ListRowPresenter;-><init>()V

    invoke-virtual {v2, v4, v5}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 154
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->classPresenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    const-class v4, Landroid/support/v17/leanback/widget/DetailsOverviewRow;

    iget-object v5, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsOverviewRowPresenter:Landroid/support/v17/leanback/widget/DetailsOverviewRowPresenter;

    invoke-virtual {v2, v4, v5}, Landroid/support/v17/leanback/widget/ClassPresenterSelector;->addClassPresenter(Ljava/lang/Class;Landroid/support/v17/leanback/widget/Presenter;)V

    .line 156
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->objectAdapter:Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    iget-object v4, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->classPresenterSelector:Landroid/support/v17/leanback/widget/ClassPresenterSelector;

    invoke-virtual {v2, v4}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->setPresenterSelector(Landroid/support/v17/leanback/widget/PresenterSelector;)V

    .line 158
    invoke-super {p0, p1}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onCreate(Landroid/os/Bundle;)V

    .line 161
    invoke-virtual {p0, v6}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setTitle(Ljava/lang/CharSequence;)V

    .line 163
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    const-string v4, "run_animation"

    invoke-virtual {v2, v4, v3}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_3

    if-nez p1, :cond_3

    const/4 v2, 0x1

    :goto_1
    iput-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->pendingEnterTransition:Z

    .line 165
    new-instance v2, Landroid/os/Handler;

    invoke-direct {v2}, Landroid/os/Handler;-><init>()V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->handler:Landroid/os/Handler;

    .line 167
    iget-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->pendingEnterTransition:Z

    if-eqz v2, :cond_2

    .line 168
    iget-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-eqz v2, :cond_4

    .line 169
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setupTransitionForSynopsis()V

    .line 173
    :goto_2
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->createIsTransitioningListener()Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;

    move-result-object v1

    .line 174
    .local v1, "isTransitioningListener":Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getSharedElementEnterTransition()Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 175
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getWindow()Landroid/view/Window;

    move-result-object v2

    invoke-virtual {v2}, Landroid/view/Window;->getSharedElementReturnTransition()Landroid/transition/Transition;

    move-result-object v2

    invoke-virtual {v2, v1}, Landroid/transition/Transition;->addListener(Landroid/transition/Transition$TransitionListener;)Landroid/transition/Transition;

    .line 177
    .end local v1    # "isTransitioningListener":Lcom/google/android/videos/ui/TransitionUtil$TransitionListenerAdapter;
    :cond_2
    invoke-direct {p0, v3}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapter(I)V

    goto/16 :goto_0

    :cond_3
    move v2, v3

    .line 163
    goto :goto_1

    .line 171
    :cond_4
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->setupNormalTransition()V

    goto :goto_2
.end method

.method protected onDestroy()V
    .locals 2

    .prologue
    .line 321
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    if-eqz v0, :cond_0

    .line 322
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->database:Lcom/google/android/videos/store/Database;

    iget-object v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateNotifier:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$UpdateNotifier;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 324
    :cond_0
    invoke-super {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onDestroy()V

    .line 325
    return-void
.end method

.method public onEpisodePurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
    .locals 5
    .param p1, "purchase"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .prologue
    .line 344
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 346
    const/4 v0, 0x0

    .line 347
    .local v0, "extra":Landroid/os/Bundle;
    iget-object v2, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->filteringType:I

    invoke-static {p0, v2, v3, v4, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startEpisodeDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I

    move-result v1

    .line 349
    .local v1, "result":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 350
    invoke-direct {p0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->logDirectPurchaseResult(I)V

    .line 351
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 353
    :cond_0
    return-void
.end method

.method public onKeyUp(ILandroid/view/KeyEvent;)Z
    .locals 3
    .param p1, "keyCode"    # I
    .param p2, "event"    # Landroid/view/KeyEvent;

    .prologue
    const/4 v1, 0x1

    .line 516
    const/16 v2, 0x17

    if-eq p1, v2, :cond_0

    const/16 v2, 0x42

    if-ne p1, v2, :cond_1

    :cond_0
    move v0, v1

    .line 517
    .local v0, "isEnter":Z
    :goto_0
    iget-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->synopsisMode:Z

    if-eqz v2, :cond_2

    if-eqz v0, :cond_2

    .line 518
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finishAfterTransition()V

    .line 521
    :goto_1
    return v1

    .line 516
    .end local v0    # "isEnter":Z
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    .line 521
    .restart local v0    # "isEnter":Z
    :cond_2
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onKeyUp(ILandroid/view/KeyEvent;)Z

    move-result v1

    goto :goto_1
.end method

.method public onMoviePurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
    .locals 5
    .param p1, "purchase"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .prologue
    .line 331
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 333
    const/4 v0, 0x0

    .line 334
    .local v0, "extra":Landroid/os/Bundle;
    iget-object v2, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->videoId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->filteringType:I

    invoke-static {p0, v2, v3, v4, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startMovieDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I

    move-result v1

    .line 336
    .local v1, "result":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 337
    invoke-direct {p0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->logDirectPurchaseResult(I)V

    .line 338
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 340
    :cond_0
    return-void
.end method

.method protected onPause()V
    .locals 2

    .prologue
    .line 314
    const v1, 0x7f0f013a

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 315
    .local v0, "body":Landroid/view/View;
    if-eqz v0, :cond_0

    invoke-virtual {v0}, Landroid/view/View;->hasFocus()Z

    move-result v1

    if-eqz v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->bodyHadFocus:Z

    .line 316
    invoke-super {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onPause()V

    .line 317
    return-void

    .line 315
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method

.method public onPurchasesUpdated()V
    .locals 1

    .prologue
    .line 369
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->onPurchasesUpdated()V

    .line 370
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapterIfReady()V

    .line 371
    return-void
.end method

.method public onReadMoreClicked()V
    .locals 7

    .prologue
    const/4 v6, 0x1

    .line 503
    new-instance v1, Landroid/content/Intent;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getIntent()Landroid/content/Intent;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 504
    .local v1, "intent":Landroid/content/Intent;
    const-string v2, "synopsis_mode"

    invoke-virtual {v1, v2, v6}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 505
    invoke-static {v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->markAsRunningAnimation(Landroid/content/Intent;)V

    .line 506
    const/4 v2, 0x2

    new-array v2, v2, [Landroid/util/Pair;

    const/4 v3, 0x0

    const v4, 0x7f0f013e

    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v4

    const-string v5, "hero"

    invoke-static {v4, v5}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v4

    aput-object v4, v2, v3

    const v3, 0x7f0f0140

    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v3

    const v4, 0x7f0b0077

    invoke-virtual {p0, v4}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->getString(I)Ljava/lang/String;

    move-result-object v4

    invoke-static {v3, v4}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v3

    aput-object v3, v2, v6

    invoke-static {p0, v2}, Landroid/app/ActivityOptions;->makeSceneTransitionAnimation(Landroid/app/Activity;[Landroid/util/Pair;)Landroid/app/ActivityOptions;

    move-result-object v2

    invoke-virtual {v2}, Landroid/app/ActivityOptions;->toBundle()Landroid/os/Bundle;

    move-result-object v0

    .line 511
    .local v0, "bundle":Landroid/os/Bundle;
    invoke-virtual {p0, v1, v0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->startActivity(Landroid/content/Intent;Landroid/os/Bundle;)V

    .line 512
    return-void
.end method

.method protected onResume()V
    .locals 3

    .prologue
    .line 295
    invoke-super {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->onResume()V

    .line 296
    iget-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v2}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 297
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->finish()V

    .line 310
    :cond_0
    :goto_0
    return-void

    .line 301
    :cond_1
    const v2, 0x7f0f0143

    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v0

    .line 302
    .local v0, "actions":Landroid/view/View;
    const v2, 0x7f0f013a

    invoke-virtual {p0, v2}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    .line 304
    .local v1, "body":Landroid/view/View;
    if-eqz v1, :cond_0

    if-eqz v0, :cond_0

    invoke-virtual {v1}, Landroid/view/View;->hasFocus()Z

    move-result v2

    if-eqz v2, :cond_0

    iget-boolean v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->bodyHadFocus:Z

    if-nez v2, :cond_0

    .line 308
    invoke-virtual {v0}, Landroid/view/View;->requestFocus()Z

    goto :goto_0
.end method

.method public onSeasonPassPurchase(Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;)V
    .locals 5
    .param p1, "purchase"    # Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .prologue
    .line 357
    iput-object p1, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 359
    const/4 v0, 0x0

    .line 360
    .local v0, "extra":Landroid/os/Bundle;
    iget-object v2, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->seasonId:Ljava/lang/String;

    iget-object v3, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->account:Ljava/lang/String;

    iget v4, p1, Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;->filteringType:I

    invoke-static {p0, v2, v3, v4, v0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startSeasonDirectPurchaseFlow(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;ILandroid/os/Bundle;)I

    move-result v1

    .line 362
    .local v1, "result":I
    const/4 v2, -0x1

    if-eq v1, v2, :cond_0

    .line 363
    invoke-direct {p0, v1}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->logDirectPurchaseResult(I)V

    .line 364
    const/4 v2, 0x0

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->purchase:Lcom/google/android/videos/pano/activity/BaseDetailsActivity$PurchaseInfo;

    .line 366
    :cond_0
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 374
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->detailsRowHelper:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->onWatchTimestampsUpdated(Ljava/lang/String;)V

    .line 375
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapterIfReady()V

    .line 376
    return-void
.end method

.method public onWishlistUpdated()V
    .locals 0

    .prologue
    .line 379
    invoke-direct {p0}, Lcom/google/android/videos/pano/activity/BaseDetailsActivity;->updateAdapterIfReady()V

    .line 380
    return-void
.end method
