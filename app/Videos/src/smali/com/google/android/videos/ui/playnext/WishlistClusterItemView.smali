.class public Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "WishlistClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/WishlistClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private bitmapRequest:Ljava/lang/String;

.field private overflowView:Landroid/view/View;

.field private priceView:Lcom/google/android/play/layout/PlayCardLabelView;

.field private ratingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private subtitleView:Landroid/widget/TextView;

.field private thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 49
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 50
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 53
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 54
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 57
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 58
    return-void
.end method

.method private final getYearAndDurationText(II)Ljava/lang/String;
    .locals 8
    .param p1, "year"    # I
    .param p2, "duration"    # I

    .prologue
    const/4 v0, 0x0

    const/4 v7, 0x0

    const/4 v6, 0x1

    .line 113
    if-nez p1, :cond_0

    move-object v1, v0

    .line 114
    .local v1, "yearText":Ljava/lang/String;
    :goto_0
    if-nez p2, :cond_1

    .line 116
    .local v0, "durationText":Ljava/lang/String;
    :goto_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/String;

    aput-object v1, v3, v7

    aput-object v0, v3, v6

    invoke-static {v2, v6, v3}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 113
    .end local v0    # "durationText":Ljava/lang/String;
    .end local v1    # "yearText":Ljava/lang/String;
    :cond_0
    invoke-static {p1}, Lcom/google/android/videos/utils/TimeUtil;->getStandaloneYearString(I)Ljava/lang/String;

    move-result-object v1

    goto :goto_0

    .line 114
    .restart local v1    # "yearText":Ljava/lang/String;
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0138

    new-array v4, v6, [Ljava/lang/Object;

    div-int/lit8 v5, p2, 0x3c

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    aput-object v5, v4, v7

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_1
.end method


# virtual methods
.method bindCommon(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 5
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/16 v4, 0x8

    .line 73
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v1, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 74
    if-nez p2, :cond_1

    .line 75
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v1, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 76
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 77
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    if-eqz v1, :cond_0

    .line 78
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 91
    :cond_0
    :goto_0
    return-void

    .line 80
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    if-eqz v1, :cond_0

    .line 81
    iget-object v1, p2, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v0

    .line 84
    .local v0, "viewerRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-nez v0, :cond_2

    .line 85
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v1, v4}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    goto :goto_0

    .line 87
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 88
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    iget v2, v0, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    goto :goto_0
.end method

.method bindMovieSpecific(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 7
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v5, 0x0

    .line 94
    if-eqz p1, :cond_0

    .line 95
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    .line 96
    .local v1, "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v5}, Landroid/widget/TextView;->setVisibility(I)V

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    invoke-static {v1}, Lcom/google/android/videos/api/AssetResourceUtil;->getYearIfAvailable(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;)I

    move-result v3

    iget v4, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->durationSec:I

    invoke-direct {p0, v3, v4}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->getYearAndDurationText(II)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 99
    const/4 v2, 0x1

    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    invoke-static {v2, v3}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v0

    .line 100
    .local v0, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-nez v0, :cond_1

    .line 101
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    const/16 v3, 0x8

    invoke-virtual {v2, v3}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 109
    .end local v0    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .end local v1    # "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    :cond_0
    :goto_0
    const v2, 0x3f31a787

    invoke-virtual {p0, v2}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->setThumbnailAspectRatio(F)V

    .line 110
    return-void

    .line 103
    .restart local v0    # "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .restart local v1    # "metadata":Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v2, v5}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 104
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    iget-object v3, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    iget-object v4, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v4, v4, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    const/4 v5, 0x4

    const-string v6, ""

    invoke-virtual {v2, v3, v4, v5, v6}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;Ljava/lang/String;ILjava/lang/String;)V

    goto :goto_0
.end method

.method bindPoster(Ljava/lang/String;Lcom/google/android/videos/async/Requester;)V
    .locals 6
    .param p1, "bitmapRequest"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Ljava/lang/String;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bitmapRequest:Ljava/lang/String;

    .line 131
    const v1, 0x7f0f00c9

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v4, 0x0

    const-class v5, Ljava/lang/String;

    move-object v0, p0

    move-object v3, p2

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 133
    return-void
.end method

.method bindShowSpecific(Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 4
    .param p1, "assetResource"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v3, 0x0

    .line 120
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 121
    if-eqz p1, :cond_0

    .line 122
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 123
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->broadcaster:[Ljava/lang/String;

    invoke-static {v1, v3, v2}, Lcom/google/android/videos/utils/Util;->buildListString(Landroid/content/res/Resources;Z[Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 126
    :cond_0
    const/high16 v0, 0x3f800000    # 1.0f

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->setThumbnailAspectRatio(F)V

    .line 127
    return-void
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 145
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 146
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->overflowView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 147
    return-void
.end method

.method protected onFinishInflate()V
    .locals 2

    .prologue
    .line 62
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 63
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->titleView:Landroid/widget/TextView;

    .line 64
    const v0, 0x7f0f00de

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->subtitleView:Landroid/widget/TextView;

    .line 65
    const v0, 0x7f0f00d2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->overflowView:Landroid/view/View;

    .line 66
    const v0, 0x7f0f00d3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 67
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const v1, 0x7f0a00d7

    invoke-static {v0, v1}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;I)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->thumbnailBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 69
    const v0, 0x7f0f00db

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 70
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 1
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .line 137
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const v0, 0x7f0f00c9

    if-ne p1, v0, :cond_0

    .line 138
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WishlistClusterItemView;->bitmapRequest:Ljava/lang/String;

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    .line 140
    :goto_0
    return-object v0

    :cond_0
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0
.end method
