.class public Lcom/google/android/videos/InvalidateAuthTokensService;
.super Landroid/app/IntentService;
.source "InvalidateAuthTokensService.java"


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 16
    const-string v0, "InvalidateAuthTokensService"

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 17
    return-void
.end method


# virtual methods
.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 21
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v5

    .line 22
    .local v5, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    .line 23
    .local v0, "accountManager":Lcom/google/android/videos/accounts/AccountManagerWrapper;
    invoke-virtual {v0}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v1

    .line 24
    .local v1, "accounts":[Landroid/accounts/Account;
    const/4 v4, 0x0

    .line 25
    .local v4, "invalidatedCount":I
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    array-length v6, v1

    if-ge v3, v6, :cond_0

    .line 27
    :try_start_0
    aget-object v6, v1, v3

    iget-object v6, v6, Landroid/accounts/Account;->name:Ljava/lang/String;

    invoke-virtual {v0, v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingGetAuthToken(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 28
    .local v2, "authToken":Ljava/lang/String;
    invoke-virtual {v0, v2}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->invalidateAuthToken(Ljava/lang/String;)V
    :try_end_0
    .catch Lcom/google/android/videos/accounts/AccountManagerWrapper$AuthTokenException; {:try_start_0 .. :try_end_0} :catch_0

    .line 29
    add-int/lit8 v4, v4, 0x1

    .line 25
    .end local v2    # "authToken":Ljava/lang/String;
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 34
    :cond_0
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Auth tokens invalidated: "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->d(Ljava/lang/String;)V

    .line 35
    return-void

    .line 30
    :catch_0
    move-exception v6

    goto :goto_1
.end method
