.class public abstract Lcom/google/android/videos/tagging/Tag;
.super Ljava/lang/Object;
.source "Tag.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/Tag$1;,
        Lcom/google/android/videos/tagging/Tag$RotatedOvalTagShape;,
        Lcom/google/android/videos/tagging/Tag$FaceRectTag;,
        Lcom/google/android/videos/tagging/Tag$CircleTag;,
        Lcom/google/android/videos/tagging/Tag$ShapelessTag;,
        Lcom/google/android/videos/tagging/Tag$TagShape;
    }
.end annotation


# instance fields
.field public final interpolates:Z

.field private interpolatesIn:Z

.field private nextTag:Lcom/google/android/videos/tagging/Tag;

.field public final splitId:I

.field public final timeMillis:I


# direct methods
.method private constructor <init>(IIZ)V
    .locals 0
    .param p1, "splitId"    # I
    .param p2, "timeMillis"    # I
    .param p3, "interpolates"    # Z

    .prologue
    .line 79
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 80
    iput p1, p0, Lcom/google/android/videos/tagging/Tag;->splitId:I

    .line 81
    iput p2, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    .line 82
    iput-boolean p3, p0, Lcom/google/android/videos/tagging/Tag;->interpolates:Z

    .line 83
    return-void
.end method

.method synthetic constructor <init>(IIZLcom/google/android/videos/tagging/Tag$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # I
    .param p3, "x2"    # Z
    .param p4, "x3"    # Lcom/google/android/videos/tagging/Tag$1;

    .prologue
    .line 16
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/tagging/Tag;-><init>(IIZ)V

    return-void
.end method

.method static parseFrom(Lcom/google/android/videos/proto/TagProtos$Tag;IZ)Lcom/google/android/videos/tagging/Tag;
    .locals 8
    .param p0, "tagProto"    # Lcom/google/android/videos/proto/TagProtos$Tag;
    .param p1, "timeMillis"    # I
    .param p2, "interpolates"    # Z

    .prologue
    .line 70
    iget v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    if-eqz v0, :cond_0

    .line 71
    new-instance v0, Lcom/google/android/videos/tagging/Tag$CircleTag;

    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    iget v3, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->circle:I

    const/4 v5, 0x0

    move v2, p1

    move v4, p2

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/Tag$CircleTag;-><init>(IIIZLcom/google/android/videos/tagging/Tag$1;)V

    .line 75
    :goto_0
    return-object v0

    .line 72
    :cond_0
    iget-wide v0, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    const-wide/16 v2, 0x0

    cmp-long v0, v0, v2

    if-eqz v0, :cond_1

    .line 73
    new-instance v1, Lcom/google/android/videos/tagging/Tag$FaceRectTag;

    iget v2, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    iget-wide v4, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->faceRectShape:J

    const/4 v7, 0x0

    move v3, p1

    move v6, p2

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/tagging/Tag$FaceRectTag;-><init>(IIJZLcom/google/android/videos/tagging/Tag$1;)V

    move-object v0, v1

    goto :goto_0

    .line 75
    :cond_1
    new-instance v0, Lcom/google/android/videos/tagging/Tag$ShapelessTag;

    iget v1, p0, Lcom/google/android/videos/proto/TagProtos$Tag;->splitId:I

    const/4 v2, 0x0

    invoke-direct {v0, v1, p1, p2, v2}, Lcom/google/android/videos/tagging/Tag$ShapelessTag;-><init>(IIZLcom/google/android/videos/tagging/Tag$1;)V

    goto :goto_0
.end method


# virtual methods
.method public final getFirstTagAt(I)Lcom/google/android/videos/tagging/Tag;
    .locals 2
    .param p1, "atMillis"    # I

    .prologue
    const/4 v0, 0x0

    .line 126
    iget v1, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    if-ge p1, v1, :cond_1

    move-object p0, v0

    .line 139
    .end local p0    # "this":Lcom/google/android/videos/tagging/Tag;
    :cond_0
    :goto_0
    return-object p0

    .line 129
    .restart local p0    # "this":Lcom/google/android/videos/tagging/Tag;
    :cond_1
    iget v1, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    if-eq v1, p1, :cond_0

    .line 132
    iget-boolean v1, p0, Lcom/google/android/videos/tagging/Tag;->interpolates:Z

    if-eqz v1, :cond_2

    iget-object v1, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    if-eqz v1, :cond_0

    iget-object v1, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    iget v1, v1, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    if-lt p1, v1, :cond_0

    .line 136
    :cond_2
    iget-object v1, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    if-eqz v1, :cond_3

    .line 137
    iget-object v0, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/Tag;->getFirstTagAt(I)Lcom/google/android/videos/tagging/Tag;

    move-result-object p0

    goto :goto_0

    :cond_3
    move-object p0, v0

    .line 139
    goto :goto_0
.end method

.method protected final getNextTag()Lcom/google/android/videos/tagging/Tag;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    return-object v0
.end method

.method public abstract getTagShape(IFFF)Lcom/google/android/videos/tagging/Tag$TagShape;
.end method

.method protected final interpolate(IFIF)F
    .locals 2
    .param p1, "atMillis"    # I
    .param p2, "startValue"    # F
    .param p3, "endMillis"    # I
    .param p4, "endValue"    # F

    .prologue
    .line 148
    iget v0, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    if-gt v0, p1, :cond_0

    if-ge p1, p3, :cond_0

    const/4 v0, 0x1

    :goto_0
    const-string v1, "times must satisfy millis <= atMillis < endMillis"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 150
    sub-float v0, p4, p2

    iget v1, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    sub-int v1, p1, v1

    int-to-float v1, v1

    mul-float/2addr v0, v1

    iget v1, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    sub-int v1, p3, v1

    int-to-float v1, v1

    div-float/2addr v0, v1

    add-float/2addr v0, p2

    return v0

    .line 148
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method protected final interpolateAngle(IFIF)F
    .locals 3
    .param p1, "atMillis"    # I
    .param p2, "startAngle"    # F
    .param p3, "endMillis"    # I
    .param p4, "endAngle"    # F

    .prologue
    const/high16 v2, 0x43340000    # 180.0f

    const/high16 v1, 0x43b40000    # 360.0f

    .line 168
    sub-float v0, p4, p2

    cmpl-float v0, v0, v2

    if-lez v0, :cond_1

    .line 169
    add-float/2addr p2, v1

    .line 173
    :cond_0
    :goto_0
    invoke-virtual {p0, p1, p2, p3, p4}, Lcom/google/android/videos/tagging/Tag;->interpolate(IFIF)F

    move-result v0

    rem-float/2addr v0, v1

    return v0

    .line 170
    :cond_1
    sub-float v0, p2, p4

    cmpl-float v0, v0, v2

    if-lez v0, :cond_0

    .line 171
    add-float/2addr p4, v1

    goto :goto_0
.end method

.method interpolatesIn()Z
    .locals 1

    .prologue
    .line 86
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/Tag;->interpolatesIn:Z

    return v0
.end method

.method final setInterpolatesIn(Z)V
    .locals 0
    .param p1, "interpolatesIn"    # Z

    .prologue
    .line 90
    iput-boolean p1, p0, Lcom/google/android/videos/tagging/Tag;->interpolatesIn:Z

    .line 91
    return-void
.end method

.method final setNextTag(Lcom/google/android/videos/tagging/Tag;)V
    .locals 0
    .param p1, "nextTag"    # Lcom/google/android/videos/tagging/Tag;

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    .line 115
    return-void
.end method

.method protected final shouldInterpolate(I)Z
    .locals 1
    .param p1, "atMillis"    # I

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/videos/tagging/Tag;->interpolates:Z

    if-eqz v0, :cond_0

    iget v0, p0, Lcom/google/android/videos/tagging/Tag;->timeMillis:I

    if-ge v0, p1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/tagging/Tag;->nextTag:Lcom/google/android/videos/tagging/Tag;

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
