.class Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;
.super Ljava/lang/Object;
.source "NowtificationFromItemFactory.java"

# interfaces
.implements Lcom/google/android/repolib/common/Factory;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/repolib/common/Factory",
        "<",
        "Lcom/google/android/videos/pano/nowtifications/Nowtification;",
        "Lcom/google/android/videos/pano/model/GroupedVideoItem;",
        ">;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final context:Landroid/content/Context;


# direct methods
.method public constructor <init>(Landroid/content/Context;Lcom/google/android/videos/async/Requester;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 33
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 34
    iput-object p1, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    .line 35
    iput-object p2, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 36
    return-void
.end method


# virtual methods
.method public createFrom(Lcom/google/android/videos/pano/model/GroupedVideoItem;)Lcom/google/android/videos/pano/nowtifications/Nowtification;
    .locals 14
    .param p1, "groupedItem"    # Lcom/google/android/videos/pano/model/GroupedVideoItem;

    .prologue
    const/4 v12, 0x1

    const/4 v13, 0x0

    .line 42
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/GroupedVideoItem;->getVideoItem()Lcom/google/android/videos/pano/model/VideoItem;

    move-result-object v7

    .line 43
    .local v7, "item":Lcom/google/android/videos/pano/model/VideoItem;
    invoke-virtual {p1}, Lcom/google/android/videos/pano/model/GroupedVideoItem;->getGroup()Ljava/lang/String;

    move-result-object v4

    .line 44
    .local v4, "group":Ljava/lang/String;
    new-instance v3, Landroid/os/Bundle;

    invoke-direct {v3}, Landroid/os/Bundle;-><init>()V

    .line 45
    .local v3, "extras":Landroid/os/Bundle;
    const-string v9, "android.backgroundImageUri"

    iget-object v10, v7, Lcom/google/android/videos/pano/model/VideoItem;->wallpaperUri:Ljava/lang/String;

    invoke-static {v10}, Lcom/google/android/videos/pano/nowtifications/NowtificationsContentProvider;->createBitmapUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-virtual {v10}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v10

    invoke-virtual {v3, v9, v10}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 47
    const/4 v8, 0x0

    .line 48
    .local v8, "largeIcon":Landroid/graphics/Bitmap;
    iget-object v9, v7, Lcom/google/android/videos/pano/model/VideoItem;->posterUri:Ljava/lang/String;

    if-eqz v9, :cond_0

    .line 49
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 50
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    iget-object v9, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v10, v7, Lcom/google/android/videos/pano/model/VideoItem;->posterUri:Ljava/lang/String;

    invoke-static {v10}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v10

    invoke-interface {v9, v10, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 52
    :try_start_0
    invoke-virtual {v1}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v9

    move-object v0, v9

    check-cast v0, Landroid/graphics/Bitmap;

    move-object v8, v0
    :try_end_0
    .catch Ljava/util/concurrent/CancellationException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 59
    .end local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    :cond_0
    :goto_0
    if-nez v8, :cond_1

    .line 60
    iget-object v9, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    invoke-virtual {v9}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    const v10, 0x7f02017e

    invoke-static {v9, v10}, Landroid/graphics/BitmapFactory;->decodeResource(Landroid/content/res/Resources;I)Landroid/graphics/Bitmap;

    move-result-object v8

    .line 64
    :cond_1
    new-instance v5, Landroid/content/Intent;

    iget-object v9, v7, Lcom/google/android/videos/pano/model/VideoItem;->intent:Landroid/content/Intent;

    invoke-direct {v5, v9}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    .line 65
    .local v5, "intent":Landroid/content/Intent;
    invoke-virtual {v5, v12}, Landroid/content/Intent;->toUri(I)Ljava/lang/String;

    move-result-object v9

    invoke-static {v9}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v9

    invoke-virtual {v5, v9}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    .line 66
    const/4 v9, 0x2

    new-array v6, v9, [Landroid/content/Intent;

    iget-object v9, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    invoke-static {v9}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v9

    aput-object v9, v6, v13

    aput-object v5, v6, v12

    .line 69
    .local v6, "intents":[Landroid/content/Intent;
    new-instance v10, Lcom/google/android/videos/pano/nowtifications/Nowtification;

    new-instance v9, Landroid/app/Notification$Builder;

    iget-object v11, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    invoke-direct {v9, v11}, Landroid/app/Notification$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v9, v13}, Landroid/app/Notification$Builder;->setAutoCancel(Z)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v13}, Landroid/app/Notification$Builder;->setDefaults(I)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/app/Notification$Builder;->setOnlyAlertOnce(Z)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v12}, Landroid/app/Notification$Builder;->setOngoing(Z)Landroid/app/Notification$Builder;

    move-result-object v9

    const v11, 0x7f0200cf

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setSmallIcon(I)Landroid/app/Notification$Builder;

    move-result-object v9

    iget-object v11, v7, Lcom/google/android/videos/pano/model/VideoItem;->title:Ljava/lang/String;

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v11

    iget-object v9, v7, Lcom/google/android/videos/pano/model/VideoItem;->subtitle:Ljava/lang/String;

    invoke-static {v9, v4}, Lcom/google/android/videos/utils/Util;->firstNonNull(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v9

    check-cast v9, Ljava/lang/CharSequence;

    invoke-virtual {v11, v9}, Landroid/app/Notification$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    const v12, 0x7f0b007f

    invoke-virtual {v11, v12}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setContentInfo(Ljava/lang/CharSequence;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v3}, Landroid/app/Notification$Builder;->setExtras(Landroid/os/Bundle;)Landroid/app/Notification$Builder;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    invoke-virtual {v11}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v11

    const v12, 0x7f0a0079

    invoke-virtual {v11, v12}, Landroid/content/res/Resources;->getColor(I)I

    move-result v11

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v4}, Landroid/app/Notification$Builder;->setGroup(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v9

    const-string v11, "recommendation"

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setCategory(Ljava/lang/String;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v9, v8}, Landroid/app/Notification$Builder;->setLargeIcon(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v9

    iget-object v11, p0, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->context:Landroid/content/Context;

    const/high16 v12, 0x8000000

    invoke-static {v11, v13, v6, v12}, Landroid/app/PendingIntent;->getActivities(Landroid/content/Context;I[Landroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v11

    invoke-virtual {v9, v11}, Landroid/app/Notification$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/app/Notification$Builder;

    move-result-object v9

    invoke-virtual {v7}, Lcom/google/android/videos/pano/model/VideoItem;->getEntityId()Ljava/lang/String;

    move-result-object v11

    invoke-direct {v10, v9, v11}, Lcom/google/android/videos/pano/nowtifications/Nowtification;-><init>(Landroid/app/Notification$Builder;Ljava/lang/String;)V

    return-object v10

    .line 53
    .end local v5    # "intent":Landroid/content/Intent;
    .end local v6    # "intents":[Landroid/content/Intent;
    .restart local v1    # "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    :catch_0
    move-exception v2

    .line 54
    .local v2, "e":Ljava/util/concurrent/CancellationException;
    const-string v9, "Failed to load poster bitmap."

    invoke-static {v9, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0

    .line 55
    .end local v2    # "e":Ljava/util/concurrent/CancellationException;
    :catch_1
    move-exception v2

    .line 56
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    const-string v9, "Failed to load poster bitmap."

    invoke-static {v9, v2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto/16 :goto_0
.end method

.method public bridge synthetic createFrom(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;

    .prologue
    .line 27
    check-cast p1, Lcom/google/android/videos/pano/model/GroupedVideoItem;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/pano/nowtifications/NowtificationFromItemFactory;->createFrom(Lcom/google/android/videos/pano/model/GroupedVideoItem;)Lcom/google/android/videos/pano/nowtifications/Nowtification;

    move-result-object v0

    return-object v0
.end method
