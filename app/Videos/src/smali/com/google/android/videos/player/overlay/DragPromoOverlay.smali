.class public Lcom/google/android/videos/player/overlay/DragPromoOverlay;
.super Landroid/widget/FrameLayout;
.source "DragPromoOverlay.java"

# interfaces
.implements Landroid/view/animation/Animation$AnimationListener;
.implements Lcom/google/android/videos/player/PlayerView$PlayerOverlay;
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;
    }
.end annotation


# instance fields
.field private final arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

.field private final arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

.field private backwardArrow:Landroid/widget/ImageView;

.field private final backwardTranslation:Landroid/view/animation/TranslateAnimation;

.field private dragHand:Landroid/view/View;

.field private fineGrainedScrubbingStartTimeMillis:J

.field private forwardArrow:Landroid/widget/ImageView;

.field private final forwardTranslation:Landroid/view/animation/TranslateAnimation;

.field private final halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

.field private final halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

.field private final halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

.field private final promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

.field private showing:Z


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 8
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 55
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 56
    iput-object p2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    .line 57
    const v3, 0x7f0a00e7

    invoke-virtual {p0, v3}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->setBackgroundResource(I)V

    .line 58
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->resetAndInflate()V

    .line 60
    new-instance v1, Landroid/view/animation/AccelerateDecelerateInterpolator;

    invoke-direct {v1}, Landroid/view/animation/AccelerateDecelerateInterpolator;-><init>()V

    .line 61
    .local v1, "interpolator":Landroid/view/animation/Interpolator;
    new-instance v0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;

    const/high16 v3, 0x3f000000    # 0.5f

    const/high16 v4, 0x3f000000    # 0.5f

    invoke-direct {v0, v1, v3, v4}, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;-><init>(Landroid/view/animation/Interpolator;FF)V

    .line 65
    .local v0, "halfInterpolator":Landroid/view/animation/Interpolator;
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->dragHand:Landroid/view/View;

    invoke-virtual {v3}, Landroid/view/View;->getPaddingLeft()I

    move-result v3

    mul-int/lit8 v3, v3, 0x50

    div-int/lit8 v2, v3, 0x64

    .line 66
    .local v2, "translation":I
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    neg-int v4, v2

    int-to-float v4, v4

    int-to-float v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    .line 67
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 68
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, p0}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 69
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v0}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 70
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v4, 0x190

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 72
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    neg-int v4, v2

    int-to-float v4, v4

    int-to-float v5, v2

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    .line 73
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 74
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, p0}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 75
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 76
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v4, 0x320

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 78
    new-instance v3, Landroid/view/animation/TranslateAnimation;

    int-to-float v4, v2

    neg-int v5, v2

    int-to-float v5, v5

    const/4 v6, 0x0

    const/4 v7, 0x0

    invoke-direct {v3, v4, v5, v6, v7}, Landroid/view/animation/TranslateAnimation;-><init>(FFFF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    .line 79
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/TranslateAnimation;->setFillAfter(Z)V

    .line 80
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, p0}, Landroid/view/animation/TranslateAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 81
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/TranslateAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 82
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    const-wide/16 v4, 0x320

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/TranslateAnimation;->setDuration(J)V

    .line 84
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    .line 85
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 86
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v0}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 87
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0x190

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 89
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    .line 90
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 91
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 92
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0x320

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 94
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    .line 95
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 96
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v0}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 97
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0x190

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 99
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    .line 100
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 101
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 102
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0x320

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 104
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x0

    const/high16 v5, 0x3f800000    # 1.0f

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    .line 105
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 106
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 107
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 108
    new-instance v3, Landroid/view/animation/AlphaAnimation;

    const/high16 v4, 0x3f800000    # 1.0f

    const/4 v5, 0x0

    invoke-direct {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;-><init>(FF)V

    iput-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    .line 109
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, v1}, Landroid/view/animation/AlphaAnimation;->setInterpolator(Landroid/view/animation/Interpolator;)V

    .line 110
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const-wide/16 v4, 0xc8

    invoke-virtual {v3, v4, v5}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 111
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    const/4 v4, 0x1

    invoke-virtual {v3, v4}, Landroid/view/animation/AlphaAnimation;->setFillAfter(Z)V

    .line 112
    iget-object v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v3, p0}, Landroid/view/animation/AlphaAnimation;->setAnimationListener(Landroid/view/animation/Animation$AnimationListener;)V

    .line 113
    return-void
.end method

.method private hidePromo()V
    .locals 5

    .prologue
    const/4 v4, 0x0

    .line 202
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->showing:Z

    if-nez v0, :cond_1

    .line 212
    :cond_0
    :goto_0
    return-void

    .line 205
    :cond_1
    iput-boolean v4, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->showing:Z

    .line 206
    invoke-virtual {p0, p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 207
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 209
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "scrubber_promo_show_count"

    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "scrubber_promo_show_count"

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v2

    add-int/lit8 v2, v2, 0x1

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences$Editor;->putInt(Ljava/lang/String;I)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    goto :goto_0
.end method

.method private resetAndInflate()V
    .locals 2

    .prologue
    .line 190
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->showing:Z

    .line 191
    invoke-virtual {p0, p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->removeCallbacks(Ljava/lang/Runnable;)Z

    .line 192
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->removeAllViews()V

    .line 194
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->getContext()Landroid/content/Context;

    move-result-object v0

    invoke-static {v0}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    const v1, 0x7f040036

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 195
    const v0, 0x7f0f0101

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardArrow:Landroid/widget/ImageView;

    .line 196
    const v0, 0x7f0f00ff

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardArrow:Landroid/widget/ImageView;

    .line 197
    const v0, 0x7f0f0100

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->dragHand:Landroid/view/View;

    .line 198
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->setVisibility(I)V

    .line 199
    return-void
.end method


# virtual methods
.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 126
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x1

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 121
    return-object p0
.end method

.method public onAnimationEnd(Landroid/view/animation/Animation;)V
    .locals 4
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    const-wide/16 v2, 0x320

    .line 168
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    if-ne p1, v1, :cond_1

    .line 169
    const/16 v1, 0x8

    invoke-virtual {p0, v1}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->setVisibility(I)V

    .line 179
    :cond_0
    :goto_0
    return-void

    .line 170
    :cond_1
    invoke-virtual {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->getVisibility()I

    move-result v1

    if-nez v1, :cond_0

    .line 172
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    if-ne p1, v1, :cond_2

    const/4 v0, 0x1

    .line 173
    .local v0, "movingToRight":Z
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 174
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v1, v2, v3}, Landroid/view/animation/AlphaAnimation;->setDuration(J)V

    .line 175
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->dragHand:Landroid/view/View;

    if-eqz v0, :cond_3

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardTranslation:Landroid/view/animation/TranslateAnimation;

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 176
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_4

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    :goto_3
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 177
    iget-object v2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardArrow:Landroid/widget/ImageView;

    if-eqz v0, :cond_5

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    :goto_4
    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    goto :goto_0

    .line 172
    .end local v0    # "movingToRight":Z
    :cond_2
    const/4 v0, 0x0

    goto :goto_1

    .line 175
    .restart local v0    # "movingToRight":Z
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardTranslation:Landroid/view/animation/TranslateAnimation;

    goto :goto_2

    .line 176
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    goto :goto_3

    .line 177
    :cond_5
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->arrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    goto :goto_4
.end method

.method public onAnimationRepeat(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 183
    return-void
.end method

.method public onAnimationStart(Landroid/view/animation/Animation;)V
    .locals 0
    .param p1, "animation"    # Landroid/view/animation/Animation;

    .prologue
    .line 187
    return-void
.end method

.method public onOrientationChanged()V
    .locals 0

    .prologue
    .line 116
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->resetAndInflate()V

    .line 117
    return-void
.end method

.method public onScrubbingCanceled()V
    .locals 0

    .prologue
    .line 154
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->hidePromo()V

    .line 155
    return-void
.end method

.method public onScrubbingEnd(Z)V
    .locals 8
    .param p1, "isFineScrubbing"    # Z

    .prologue
    .line 141
    if-eqz p1, :cond_0

    .line 142
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->fineGrainedScrubbingStartTimeMillis:J

    sub-long v0, v4, v6

    .line 144
    .local v0, "fineGrainedScrubbingDuration":J
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    const-string v5, "fine_grained_scrubbing_total_interact_time"

    const-wide/16 v6, 0x0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v2

    .line 145
    .local v2, "interactTime":J
    const-wide v4, 0x7fffffffffffffffL

    sub-long/2addr v4, v0

    cmp-long v4, v2, v4

    if-gtz v4, :cond_0

    .line 146
    iget-object v4, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    invoke-interface {v4}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    const-string v5, "fine_grained_scrubbing_total_interact_time"

    add-long v6, v2, v0

    invoke-interface {v4, v5, v6, v7}, Landroid/content/SharedPreferences$Editor;->putLong(Ljava/lang/String;J)Landroid/content/SharedPreferences$Editor;

    move-result-object v4

    invoke-interface {v4}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 150
    .end local v0    # "fineGrainedScrubbingDuration":J
    .end local v2    # "interactTime":J
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->hidePromo()V

    .line 151
    return-void
.end method

.method public onScrubbingStart(Z)V
    .locals 4
    .param p1, "isFineScrubbing"    # Z

    .prologue
    .line 130
    if-eqz p1, :cond_1

    .line 131
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->fineGrainedScrubbingStartTimeMillis:J

    .line 138
    :cond_0
    :goto_0
    return-void

    .line 132
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->showing:Z

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "scrubber_promo_show_count"

    const/4 v2, 0x0

    invoke-interface {v0, v1, v2}, Landroid/content/SharedPreferences;->getInt(Ljava/lang/String;I)I

    move-result v0

    const/4 v1, 0x5

    if-ge v0, v1, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->preferences:Landroid/content/SharedPreferences;

    const-string v1, "fine_grained_scrubbing_total_interact_time"

    const-wide/16 v2, 0x0

    invoke-interface {v0, v1, v2, v3}, Landroid/content/SharedPreferences;->getLong(Ljava/lang/String;J)J

    move-result-wide v0

    const-wide/16 v2, 0x1388

    cmp-long v0, v0, v2

    if-gez v0, :cond_0

    .line 135
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->showing:Z

    .line 136
    const-wide/16 v0, 0x1f4

    invoke-virtual {p0, p0, v0, v1}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->postDelayed(Ljava/lang/Runnable;J)Z

    goto :goto_0
.end method

.method public run()V
    .locals 2

    .prologue
    .line 159
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->setVisibility(I)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->promoFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->startAnimation(Landroid/view/animation/Animation;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->dragHand:Landroid/view/View;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfForwardTranslation:Landroid/view/animation/TranslateAnimation;

    invoke-virtual {v0, v1}, Landroid/view/View;->startAnimation(Landroid/view/animation/Animation;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->forwardArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeInAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->backwardArrow:Landroid/widget/ImageView;

    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay;->halfArrowFadeOutAnimation:Landroid/view/animation/AlphaAnimation;

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->startAnimation(Landroid/view/animation/Animation;)V

    .line 164
    return-void
.end method
