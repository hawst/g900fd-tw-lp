.class Lcom/google/android/videos/VideosGlobalsImpl$2;
.super Ljava/lang/Object;
.source "VideosGlobalsImpl.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/VideosGlobalsImpl;->onTrimMemory(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/VideosGlobalsImpl;


# direct methods
.method constructor <init>(Lcom/google/android/videos/VideosGlobalsImpl;)V
    .locals 0

    .prologue
    .line 318
    iput-object p1, p0, Lcom/google/android/videos/VideosGlobalsImpl$2;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 5

    .prologue
    .line 322
    iget-object v2, p0, Lcom/google/android/videos/VideosGlobalsImpl$2;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    monitor-enter v2

    .line 323
    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/VideosGlobalsImpl$2;->this$0:Lcom/google/android/videos/VideosGlobalsImpl;

    # getter for: Lcom/google/android/videos/VideosGlobalsImpl;->httpClient:Lorg/apache/http/client/HttpClient;
    invoke-static {v1}, Lcom/google/android/videos/VideosGlobalsImpl;->access$100(Lcom/google/android/videos/VideosGlobalsImpl;)Lorg/apache/http/client/HttpClient;

    move-result-object v0

    .line 324
    .local v0, "client":Lorg/apache/http/client/HttpClient;
    monitor-exit v2
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 325
    if-eqz v0, :cond_0

    .line 326
    invoke-interface {v0}, Lorg/apache/http/client/HttpClient;->getConnectionManager()Lorg/apache/http/conn/ClientConnectionManager;

    move-result-object v1

    const-wide/16 v2, 0x5

    sget-object v4, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface {v1, v2, v3, v4}, Lorg/apache/http/conn/ClientConnectionManager;->closeIdleConnections(JLjava/util/concurrent/TimeUnit;)V

    .line 328
    :cond_0
    return-void

    .line 324
    .end local v0    # "client":Lorg/apache/http/client/HttpClient;
    :catchall_0
    move-exception v1

    :try_start_1
    monitor-exit v2
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    throw v1
.end method
