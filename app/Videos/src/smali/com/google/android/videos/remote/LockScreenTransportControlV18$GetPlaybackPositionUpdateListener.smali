.class Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;
.super Ljava/lang/Object;
.source "LockScreenTransportControlV18.java"

# interfaces
.implements Landroid/media/RemoteControlClient$OnGetPlaybackPositionListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/remote/LockScreenTransportControlV18;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "GetPlaybackPositionUpdateListener"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;)V
    .locals 0

    .prologue
    .line 56
    iput-object p1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;->this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/remote/LockScreenTransportControlV18;
    .param p2, "x1"    # Lcom/google/android/videos/remote/LockScreenTransportControlV18$1;

    .prologue
    .line 56
    invoke-direct {p0, p1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;-><init>(Lcom/google/android/videos/remote/LockScreenTransportControlV18;)V

    return-void
.end method


# virtual methods
.method public onGetPlaybackPosition()J
    .locals 4

    .prologue
    .line 61
    iget-object v1, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV18$GetPlaybackPositionUpdateListener;->this$0:Lcom/google/android/videos/remote/LockScreenTransportControlV18;

    invoke-virtual {v1}, Lcom/google/android/videos/remote/LockScreenTransportControlV18;->getRemoteTracker()Lcom/google/android/videos/remote/RemoteTracker;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/remote/RemoteTracker;->getPlayerState()Lcom/google/android/videos/remote/PlayerState;

    move-result-object v0

    .line 62
    .local v0, "playerState":Lcom/google/android/videos/remote/PlayerState;
    if-nez v0, :cond_0

    const-wide/16 v2, 0x0

    :goto_0
    return-wide v2

    :cond_0
    iget v1, v0, Lcom/google/android/videos/remote/PlayerState;->time:I

    int-to-long v2, v1

    goto :goto_0
.end method
