.class public abstract Lcom/google/android/videos/store/Database$BaseListener;
.super Ljava/lang/Object;
.source "Database.java"

# interfaces
.implements Lcom/google/android/videos/store/Database$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/Database;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x409
    name = "BaseListener"
.end annotation


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 117
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onMovieMetadataUpdated(Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 123
    .local p1, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public onMovieUserAssetsUpdated(Ljava/lang/String;Ljava/util/List;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 125
    .local p2, "movieIds":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    return-void
.end method

.method public onPinningStateChanged(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 139
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "movieId"    # Ljava/lang/String;

    .prologue
    .line 131
    return-void
.end method

.method public onPurchasesUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 119
    return-void
.end method

.method public onScreenshotUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 135
    return-void
.end method

.method public onShowBannerUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 137
    return-void
.end method

.method public onShowMetadataUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 127
    return-void
.end method

.method public onShowPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 133
    return-void
.end method

.method public onShowUserAssetsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 129
    return-void
.end method

.method public onWatchTimestampsUpdated(Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    .line 141
    return-void
.end method

.method public onWishlistUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 121
    return-void
.end method
