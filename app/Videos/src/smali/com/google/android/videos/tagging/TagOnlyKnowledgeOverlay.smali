.class public Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;
.super Landroid/widget/FrameLayout;
.source "TagOnlyKnowledgeOverlay.java"

# interfaces
.implements Lcom/google/android/videos/player/PlayerView$PlayerOverlay;
.implements Lcom/google/android/videos/tagging/KnowledgeView;


# instance fields
.field private final tagsView:Lcom/google/android/videos/tagging/TagsView;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    const/4 v2, -0x1

    .line 23
    invoke-direct {p0, p1}, Landroid/widget/FrameLayout;-><init>(Landroid/content/Context;)V

    .line 24
    new-instance v0, Lcom/google/android/videos/tagging/TagsView;

    invoke-direct {v0, p1}, Lcom/google/android/videos/tagging/TagsView;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    .line 25
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/TagsView;->setInteractionEnabled(Z)V

    .line 26
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    new-instance v1, Landroid/widget/FrameLayout$LayoutParams;

    invoke-direct {v1, v2, v2}, Landroid/widget/FrameLayout$LayoutParams;-><init>(II)V

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 27
    return-void
.end method

.method private clearKnowledge()V
    .locals 1

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clear()V

    .line 78
    const/16 v0, 0x8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->setVisibility(I)V

    .line 79
    return-void
.end method


# virtual methods
.method public clearSpotlight()V
    .locals 1

    .prologue
    .line 103
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0}, Lcom/google/android/videos/tagging/TagsView;->clearSpotlight()V

    .line 104
    return-void
.end method

.method public generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x1

    .line 36
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public getView()Landroid/view/View;
    .locals 0

    .prologue
    .line 31
    return-object p0
.end method

.method public hasContent()Z
    .locals 1

    .prologue
    .line 42
    invoke-virtual {p0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->getVisibility()I

    move-result v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public hideKnowledge()V
    .locals 0

    .prologue
    .line 68
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->clearKnowledge()V

    .line 69
    return-void
.end method

.method public initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V
    .locals 0
    .param p1, "knowledgeBundle"    # Lcom/google/android/videos/tagging/KnowledgeBundle;
    .param p2, "timeSupplier"    # Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
    .param p3, "mode"    # I

    .prologue
    .line 48
    return-void
.end method

.method public isInteracting()Z
    .locals 1

    .prologue
    .line 84
    const/4 v0, 0x0

    return v0
.end method

.method public onBackPressed()Z
    .locals 1

    .prologue
    .line 90
    const/4 v0, 0x0

    return v0
.end method

.method public reset()V
    .locals 0

    .prologue
    .line 73
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->clearKnowledge()V

    .line 74
    return-void
.end method

.method public setContentVisible(Z)V
    .locals 2
    .param p1, "visible"    # Z

    .prologue
    .line 95
    iget-object v1, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    if-eqz p1, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/videos/tagging/TagsView;->setVisibility(I)V

    .line 96
    return-void

    .line 95
    :cond_0
    const/16 v0, 0x8

    goto :goto_0
.end method

.method public setSpotlight(Lcom/google/android/videos/tagging/KnowledgeEntity;)V
    .locals 1
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;

    .prologue
    .line 99
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/TagsView;->setSpotlight(Lcom/google/android/videos/tagging/KnowledgeEntity;)V

    .line 100
    return-void
.end method

.method public showPausedKnowledge(IIILjava/util/List;I)V
    .locals 1
    .param p1, "timeMillis"    # I
    .param p2, "videoDisplayWidth"    # I
    .param p3, "videoDisplayHeight"    # I
    .param p5, "showRecentActorsWithinMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;I)V"
        }
    .end annotation

    .prologue
    .line 53
    .local p4, "taggedKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;>;"
    if-lez p2, :cond_0

    if-lez p3, :cond_0

    invoke-interface {p4}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    .line 54
    iget-object v0, p0, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->tagsView:Lcom/google/android/videos/tagging/TagsView;

    invoke-virtual {v0, p4, p2, p3}, Lcom/google/android/videos/tagging/TagsView;->show(Ljava/util/List;II)V

    .line 55
    const/4 v0, 0x0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->setVisibility(I)V

    .line 59
    :goto_0
    return-void

    .line 57
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->clearKnowledge()V

    goto :goto_0
.end method

.method public showPlayingKnowledge()V
    .locals 0

    .prologue
    .line 63
    invoke-direct {p0}, Lcom/google/android/videos/tagging/TagOnlyKnowledgeOverlay;->clearKnowledge()V

    .line 64
    return-void
.end method
