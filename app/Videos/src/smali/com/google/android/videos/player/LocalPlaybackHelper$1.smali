.class Lcom/google/android/videos/player/LocalPlaybackHelper$1;
.super Ljava/lang/Object;
.source "LocalPlaybackHelper.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/LocalPlaybackHelper;->persistLocalShortClockActivation()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

.field final synthetic val$expirationTimestamp:J

.field final synthetic val$key:Lcom/google/android/videos/pinning/DownloadKey;


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/LocalPlaybackHelper;JLcom/google/android/videos/pinning/DownloadKey;)V
    .locals 0

    .prologue
    .line 662
    iput-object p1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    iput-wide p2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->val$expirationTimestamp:J

    iput-object p4, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->val$key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 4

    .prologue
    .line 672
    const-wide/16 v2, 0x7d0

    :try_start_0
    invoke-static {v2, v3}, Ljava/lang/Thread;->sleep(J)V
    :try_end_0
    .catch Ljava/lang/InterruptedException; {:try_start_0 .. :try_end_0} :catch_0

    .line 676
    :goto_0
    new-instance v0, Landroid/content/ContentValues;

    invoke-direct {v0}, Landroid/content/ContentValues;-><init>()V

    .line 677
    .local v0, "values":Landroid/content/ContentValues;
    const-string v1, "license_activation"

    invoke-static {}, Lcom/google/android/videos/drm/Activation;->now()Lcom/google/android/videos/drm/Activation;

    move-result-object v2

    invoke-virtual {v2}, Lcom/google/android/videos/drm/Activation;->toBytes()[B

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;[B)V

    .line 678
    const-string v1, "license_expiration_timestamp"

    iget-wide v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->val$expirationTimestamp:J

    invoke-static {v2, v3}, Ljava/lang/Long;->valueOf(J)Ljava/lang/Long;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Long;)V

    .line 679
    const-string v1, "license_force_sync"

    const/4 v2, 0x1

    invoke-static {v2}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 680
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$500(Lcom/google/android/videos/player/LocalPlaybackHelper;)Lcom/google/android/videos/store/Database;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->val$key:Lcom/google/android/videos/pinning/DownloadKey;

    invoke-static {v1, v2, v0}, Lcom/google/android/videos/pinning/PinningDbHelper;->updatePinningStateForVideo(Lcom/google/android/videos/store/Database;Lcom/google/android/videos/pinning/DownloadKey;Landroid/content/ContentValues;)V

    .line 682
    iget-object v1, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$600(Lcom/google/android/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/player/LocalPlaybackHelper$1;->this$0:Lcom/google/android/videos/player/LocalPlaybackHelper;

    # getter for: Lcom/google/android/videos/player/LocalPlaybackHelper;->activity:Landroid/app/Activity;
    invoke-static {v2}, Lcom/google/android/videos/player/LocalPlaybackHelper;->access$600(Lcom/google/android/videos/player/LocalPlaybackHelper;)Landroid/app/Activity;

    move-result-object v2

    invoke-static {v2}, Lcom/google/android/videos/pinning/TransferService;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/app/Activity;->startService(Landroid/content/Intent;)Landroid/content/ComponentName;

    .line 683
    return-void

    .line 673
    .end local v0    # "values":Landroid/content/ContentValues;
    :catch_0
    move-exception v1

    goto :goto_0
.end method
