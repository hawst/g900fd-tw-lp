.class Lcom/google/android/videos/ui/DefaultItemAnimator$3;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "DefaultItemAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/ui/DefaultItemAnimator;->runMoveAnimation(Landroid/support/v7/widget/RecyclerView$ViewHolder;IIII)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

.field final synthetic val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

.field final synthetic val$deltaX:I

.field final synthetic val$deltaY:I

.field final synthetic val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;


# direct methods
.method constructor <init>(Lcom/google/android/videos/ui/DefaultItemAnimator;Landroid/support/v7/widget/RecyclerView$ViewHolder;IILandroid/support/v4/view/ViewPropertyAnimatorCompat;)V
    .locals 0

    .prologue
    .line 193
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$3;, "Lcom/google/android/videos/ui/DefaultItemAnimator.3;"
    iput-object p1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iput-object p2, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    iput p3, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$deltaX:I

    iput p4, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$deltaY:I

    iput-object p5, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$3;, "Lcom/google/android/videos/ui/DefaultItemAnimator.3;"
    const/4 v1, 0x0

    .line 201
    iget v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$deltaX:I

    if-eqz v0, :cond_0

    .line 202
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 204
    :cond_0
    iget v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$deltaY:I

    if-eqz v0, :cond_1

    .line 205
    invoke-static {p1, v1}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 207
    :cond_1
    return-void
.end method

.method public onAnimationEnd(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 211
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$3;, "Lcom/google/android/videos/ui/DefaultItemAnimator.3;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$animation:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 212
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveFinished(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 213
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    # getter for: Lcom/google/android/videos/ui/DefaultItemAnimator;->mMoveAnimations:Ljava/util/ArrayList;
    invoke-static {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->access$200(Lcom/google/android/videos/ui/DefaultItemAnimator;)Ljava/util/ArrayList;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Ljava/util/ArrayList;->remove(Ljava/lang/Object;)Z

    .line 214
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchFinishedWhenDone()V

    .line 215
    return-void
.end method

.method public onAnimationStart(Landroid/view/View;)V
    .locals 2
    .param p1, "view"    # Landroid/view/View;

    .prologue
    .line 196
    .local p0, "this":Lcom/google/android/videos/ui/DefaultItemAnimator$3;, "Lcom/google/android/videos/ui/DefaultItemAnimator.3;"
    iget-object v0, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->this$0:Lcom/google/android/videos/ui/DefaultItemAnimator;

    iget-object v1, p0, Lcom/google/android/videos/ui/DefaultItemAnimator$3;->val$holder:Landroid/support/v7/widget/RecyclerView$ViewHolder;

    invoke-virtual {v0, v1}, Lcom/google/android/videos/ui/DefaultItemAnimator;->dispatchMoveStarting(Landroid/support/v7/widget/RecyclerView$ViewHolder;)V

    .line 197
    return-void
.end method
