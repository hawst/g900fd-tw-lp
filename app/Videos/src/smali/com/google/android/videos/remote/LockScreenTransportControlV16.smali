.class Lcom/google/android/videos/remote/LockScreenTransportControlV16;
.super Lcom/google/android/videos/remote/LockScreenTransportControlV14;
.source "LockScreenTransportControlV16.java"


# instance fields
.field private final mediaRouter:Landroid/support/v7/media/MediaRouter;


# direct methods
.method constructor <init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V
    .locals 1
    .param p1, "remoteTracker"    # Lcom/google/android/videos/remote/RemoteTracker;
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 19
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;-><init>(Lcom/google/android/videos/remote/RemoteTracker;Landroid/content/Context;)V

    .line 20
    invoke-static {p2}, Landroid/support/v7/media/MediaRouter;->getInstance(Landroid/content/Context;)Landroid/support/v7/media/MediaRouter;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    .line 21
    return-void
.end method


# virtual methods
.method protected register()V
    .locals 2

    .prologue
    .line 25
    invoke-super {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->register()V

    .line 26
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->addRemoteControlClient(Ljava/lang/Object;)V

    .line 27
    return-void
.end method

.method protected unregister()V
    .locals 2

    .prologue
    .line 31
    iget-object v0, p0, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->mediaRouter:Landroid/support/v7/media/MediaRouter;

    invoke-virtual {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV16;->getRemoteControlClient()Landroid/media/RemoteControlClient;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/support/v7/media/MediaRouter;->removeRemoteControlClient(Ljava/lang/Object;)V

    .line 32
    invoke-super {p0}, Lcom/google/android/videos/remote/LockScreenTransportControlV14;->unregister()V

    .line 33
    return-void
.end method
