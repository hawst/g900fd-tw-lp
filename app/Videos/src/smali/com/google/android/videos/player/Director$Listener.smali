.class public interface abstract Lcom/google/android/videos/player/Director$Listener;
.super Ljava/lang/Object;
.source "Director.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/Director;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onCardsViewScrollChanged(I)V
.end method

.method public abstract onKnowledgeEnteredFullScreen()V
.end method

.method public abstract onKnowledgeExitedFullScreen()V
.end method

.method public abstract onPlaybackError()V
.end method

.method public abstract onPlaybackPaused()V
.end method

.method public abstract onPlaybackStarted()V
.end method

.method public abstract onUserInteractionEnding()V
.end method

.method public abstract onUserInteractionExpected()V
.end method

.method public abstract onUserInteractionNotExpected()V
.end method

.method public abstract onVideoTitle(Ljava/lang/String;)V
.end method
