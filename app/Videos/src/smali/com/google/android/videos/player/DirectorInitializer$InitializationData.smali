.class public final Lcom/google/android/videos/player/DirectorInitializer$InitializationData;
.super Ljava/lang/Object;
.source "DirectorInitializer.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/DirectorInitializer;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "InitializationData"
.end annotation


# instance fields
.field public final cencKeySetId:[B

.field public final cencSecurityLevel:I

.field public final drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

.field public final durationMillis:I

.field public final haveAudioInDeviceLanguage:Z

.field public final isRental:Z

.field public final pinnedStorage:I

.field public final posterUri:Ljava/lang/String;

.field public final resumeTimeMillis:I

.field public final shortClockMillis:J

.field public final shortClockNotActivated:Z

.field public final showTitle:Ljava/lang/String;

.field public final streams:Lcom/google/android/videos/streams/Streams;

.field public final subtitleDefaultLanguage:Ljava/lang/String;

.field public final subtitleMode:I

.field public final videoTitle:Ljava/lang/String;


# direct methods
.method public constructor <init>(ZZJIILcom/google/android/videos/streams/Streams;Lcom/google/android/videos/drm/DrmManager$Identifiers;[BIILjava/lang/String;ZLjava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V
    .locals 1
    .param p1, "isRental"    # Z
    .param p2, "showShortClockDialog"    # Z
    .param p3, "shortClockMillis"    # J
    .param p5, "durationMillis"    # I
    .param p6, "resumeTimeMillis"    # I
    .param p7, "streams"    # Lcom/google/android/videos/streams/Streams;
    .param p8, "drmIdentifiers"    # Lcom/google/android/videos/drm/DrmManager$Identifiers;
    .param p9, "cencKeySetId"    # [B
    .param p10, "cencSecurityLevel"    # I
    .param p11, "subtitleMode"    # I
    .param p12, "subtitleDefaultLanguage"    # Ljava/lang/String;
    .param p13, "haveAudioInDeviceLanguage"    # Z
    .param p14, "videoTitle"    # Ljava/lang/String;
    .param p15, "showTitle"    # Ljava/lang/String;
    .param p16, "posterUri"    # Ljava/lang/String;
    .param p17, "pinnedStorage"    # I

    .prologue
    .line 118
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 119
    iput-boolean p1, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->isRental:Z

    .line 120
    iput-boolean p2, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockNotActivated:Z

    .line 121
    iput-wide p3, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->shortClockMillis:J

    .line 122
    iput p5, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->durationMillis:I

    .line 123
    iput p6, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->resumeTimeMillis:I

    .line 124
    iput-object p7, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->streams:Lcom/google/android/videos/streams/Streams;

    .line 125
    iput-object p8, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    .line 126
    iput-object p9, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->cencKeySetId:[B

    .line 127
    iput p10, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->cencSecurityLevel:I

    .line 128
    iput p11, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleMode:I

    .line 129
    iput-object p12, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->subtitleDefaultLanguage:Ljava/lang/String;

    .line 130
    iput-boolean p13, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->haveAudioInDeviceLanguage:Z

    .line 131
    iput-object p14, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->videoTitle:Ljava/lang/String;

    .line 132
    move-object/from16 v0, p15

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->showTitle:Ljava/lang/String;

    .line 133
    move-object/from16 v0, p16

    iput-object v0, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->posterUri:Ljava/lang/String;

    .line 134
    move/from16 v0, p17

    iput v0, p0, Lcom/google/android/videos/player/DirectorInitializer$InitializationData;->pinnedStorage:I

    .line 135
    return-void
.end method
