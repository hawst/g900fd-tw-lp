.class Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;
.super Landroid/view/SurfaceView;
.source "DefaultPlayerSurface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/DefaultPlayerSurface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "InternalSurfaceView"
.end annotation


# instance fields
.field protected horizontalLetterboxFraction:F

.field final synthetic this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

.field protected verticalLetterboxFraction:F

.field private final zoomChangedRunnable:Ljava/lang/Runnable;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/player/DefaultPlayerSurface;Landroid/content/Context;)V
    .locals 1
    .param p2, "context"    # Landroid/content/Context;

    .prologue
    .line 249
    iput-object p1, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    .line 250
    invoke-direct {p0, p2}, Landroid/view/SurfaceView;-><init>(Landroid/content/Context;)V

    .line 251
    new-instance v0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView$1;-><init>(Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;Lcom/google/android/videos/player/DefaultPlayerSurface;)V

    iput-object v0, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->zoomChangedRunnable:Ljava/lang/Runnable;

    .line 260
    return-void
.end method


# virtual methods
.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 269
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    invoke-static {v9, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getDefaultSize(II)I

    move-result v4

    .line 270
    .local v4, "maxWidth":I
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    invoke-static {v9, p2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->getDefaultSize(II)I

    move-result v3

    .line 272
    .local v3, "maxHeight":I
    move v8, v4

    .line 273
    .local v8, "width":I
    move v1, v3

    .line 275
    .local v1, "height":I
    const/high16 v2, 0x3f800000    # 1.0f

    .line 276
    .local v2, "horizontalLetterboxFraction":F
    const/high16 v5, 0x3f800000    # 1.0f

    .line 278
    .local v5, "verticalLetterboxFraction":F
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    if-lez v9, :cond_1

    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    if-lez v9, :cond_1

    .line 279
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    int-to-float v9, v9

    iget-object v10, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v10}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v10

    int-to-float v10, v10

    div-float v6, v9, v10

    .line 280
    .local v6, "videoAspectRatio":F
    int-to-float v9, v8

    int-to-float v10, v1

    div-float v7, v9, v10

    .line 281
    .local v7, "viewAspectRatio":F
    div-float v9, v6, v7

    const/high16 v10, 0x3f800000    # 1.0f

    sub-float v0, v9, v10

    .line 283
    .local v0, "aspectDeformation":F
    const v9, 0x3c23d70a    # 0.01f

    cmpl-float v9, v0, v9

    if-lez v9, :cond_4

    .line 285
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    mul-int/2addr v9, v8

    iget-object v10, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v10}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v10

    div-int v1, v9, v10

    .line 290
    :cond_0
    :goto_0
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v8}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$402(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    .line 291
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v1}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$502(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    .line 293
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->zoomSupported:Z
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$600(Lcom/google/android/videos/player/DefaultPlayerSurface;)Z

    move-result v9

    if-eqz v9, :cond_1

    .line 294
    if-ge v8, v4, :cond_5

    .line 295
    int-to-float v9, v8

    int-to-float v10, v4

    div-float v2, v9, v10

    .line 296
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->zoom:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$700(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    sub-int v10, v4, v8

    mul-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x64

    add-int/2addr v8, v9

    .line 297
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v8}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$402(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    .line 298
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    int-to-float v10, v8

    div-float/2addr v10, v6

    float-to-int v10, v10

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v10}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$502(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    .line 308
    .end local v0    # "aspectDeformation":F
    .end local v6    # "videoAspectRatio":F
    .end local v7    # "viewAspectRatio":F
    :cond_1
    :goto_1
    iget v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    cmpl-float v9, v9, v2

    if-nez v9, :cond_2

    iget v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    cmpl-float v9, v9, v5

    if-eqz v9, :cond_3

    .line 310
    :cond_2
    iput v2, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->horizontalLetterboxFraction:F

    .line 311
    iput v5, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->verticalLetterboxFraction:F

    .line 312
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->onDisplayParametersChangedListener:Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$100(Lcom/google/android/videos/player/DefaultPlayerSurface;)Lcom/google/android/videos/player/PlayerSurface$OnDisplayParametersChangedListener;

    move-result-object v9

    if-eqz v9, :cond_3

    .line 313
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->zoomChangedRunnable:Ljava/lang/Runnable;

    invoke-virtual {p0, v9}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->post(Ljava/lang/Runnable;)Z

    .line 317
    :cond_3
    invoke-static {v8, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->resolveSize(II)I

    move-result v8

    .line 318
    invoke-static {v1, p2}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->resolveSize(II)I

    move-result v1

    .line 320
    invoke-virtual {p0, v8, v1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->setMeasuredDimension(II)V

    .line 321
    return-void

    .line 286
    .restart local v0    # "aspectDeformation":F
    .restart local v6    # "videoAspectRatio":F
    .restart local v7    # "viewAspectRatio":F
    :cond_4
    const v9, -0x43dc28f6    # -0.01f

    cmpg-float v9, v0, v9

    if-gez v9, :cond_0

    .line 288
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoWidth:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$200(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    mul-int/2addr v9, v1

    iget-object v10, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoHeight:I
    invoke-static {v10}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$300(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v10

    div-int v8, v9, v10

    goto :goto_0

    .line 299
    :cond_5
    if-ge v1, v3, :cond_1

    .line 300
    int-to-float v9, v1

    int-to-float v10, v3

    div-float v5, v9, v10

    .line 301
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # getter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->zoom:I
    invoke-static {v9}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$700(Lcom/google/android/videos/player/DefaultPlayerSurface;)I

    move-result v9

    sub-int v10, v3, v1

    mul-int/2addr v9, v10

    div-int/lit8 v9, v9, 0x64

    add-int/2addr v1, v9

    .line 302
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayHeight:I
    invoke-static {v9, v1}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$502(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    .line 303
    iget-object v9, p0, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->this$0:Lcom/google/android/videos/player/DefaultPlayerSurface;

    int-to-float v10, v1

    mul-float/2addr v10, v6

    float-to-int v10, v10

    # setter for: Lcom/google/android/videos/player/DefaultPlayerSurface;->videoDisplayWidth:I
    invoke-static {v9, v10}, Lcom/google/android/videos/player/DefaultPlayerSurface;->access$402(Lcom/google/android/videos/player/DefaultPlayerSurface;I)I

    goto :goto_1
.end method

.method public setSecureV17(Z)V
    .locals 0
    .param p1, "secure"    # Z

    .prologue
    .line 264
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface$InternalSurfaceView;->setSecure(Z)V

    .line 265
    return-void
.end method
