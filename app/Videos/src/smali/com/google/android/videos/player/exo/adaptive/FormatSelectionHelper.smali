.class Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;
.super Ljava/lang/Object;
.source "FormatSelectionHelper.java"


# instance fields
.field private audioBitrate:I

.field private final bandwidthFraction:F

.field private final startResolutionAlgorithm:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/Config;)V
    .locals 1
    .param p1, "config"    # Lcom/google/android/videos/Config;

    .prologue
    .line 26
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 27
    invoke-interface {p1}, Lcom/google/android/videos/Config;->exoBandwidthFraction()F

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->bandwidthFraction:F

    .line 28
    invoke-interface {p1}, Lcom/google/android/videos/Config;->exoStartResolutionAlgorithm()I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->startResolutionAlgorithm:I

    .line 29
    return-void
.end method

.method private determineFormatBasedOnBitrate([Lcom/google/android/exoplayer/chunk/Format;JZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 4
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "bitrateEstimate"    # J
    .param p4, "isHdDisabled"    # Z

    .prologue
    .line 114
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v1, p1

    if-ge v0, v1, :cond_2

    .line 115
    if-eqz p4, :cond_0

    aget-object v1, p1, v0

    iget v1, v1, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v2, 0x2d0

    if-ge v1, v2, :cond_1

    :cond_0
    aget-object v1, p1, v0

    invoke-direct {p0, v1, p2, p3}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->isBitrateSufficient(Lcom/google/android/exoplayer/chunk/Format;J)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 117
    aget-object v1, p1, v0

    .line 120
    :goto_1
    return-object v1

    .line 114
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 120
    :cond_2
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private determineFormatBasedOnHistory([Lcom/google/android/exoplayer/chunk/Format;JZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 2
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "historyPredictedBitrate"    # J
    .param p4, "isHdDisabled"    # Z

    .prologue
    .line 92
    const-wide/16 v0, -0x1

    cmp-long v0, p2, v0

    if-nez v0, :cond_0

    .line 93
    const/4 v0, 0x0

    .line 95
    :goto_0
    return-object v0

    :cond_0
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineFormatBasedOnBitrate([Lcom/google/android/exoplayer/chunk/Format;JZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v0

    goto :goto_0
.end method

.method private determineFormatBasedOnNetwork([Lcom/google/android/exoplayer/chunk/Format;ZZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 5
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "isHdDisabled"    # Z
    .param p3, "isFastNetwork"    # Z

    .prologue
    .line 100
    const/4 v2, 0x0

    .line 101
    .local v2, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v3, p1

    if-ge v1, v3, :cond_3

    .line 102
    aget-object v0, p1, v1

    .line 103
    .local v0, "format":Lcom/google/android/exoplayer/chunk/Format;
    if-eqz v2, :cond_0

    iget v3, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    iget v4, v2, Lcom/google/android/exoplayer/chunk/Format;->height:I

    if-ne v3, v4, :cond_2

    :cond_0
    if-eqz p2, :cond_1

    iget v3, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v4, 0x2d0

    if-ge v3, v4, :cond_2

    :cond_1
    invoke-direct {p0, v0, p3}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->isNetworkSufficient(Lcom/google/android/exoplayer/chunk/Format;Z)Z

    move-result v3

    if-eqz v3, :cond_2

    .line 106
    move-object v2, v0

    .line 101
    :cond_2
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 109
    .end local v0    # "format":Lcom/google/android/exoplayer/chunk/Format;
    :cond_3
    return-object v2
.end method

.method private isBitrateSufficient(Lcom/google/android/exoplayer/chunk/Format;J)Z
    .locals 2
    .param p1, "format"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "totalBitrate"    # J

    .prologue
    .line 143
    iget v0, p1, Lcom/google/android/exoplayer/chunk/Format;->bitrate:I

    invoke-virtual {p0, p2, p3}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->getAvailableBitrateForVideo(J)I

    move-result v1

    if-ge v0, v1, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private isNetworkSufficient(Lcom/google/android/exoplayer/chunk/Format;Z)Z
    .locals 2
    .param p1, "format"    # Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "isFastNetwork"    # Z

    .prologue
    .line 124
    iget v1, p1, Lcom/google/android/exoplayer/chunk/Format;->height:I

    if-eqz p2, :cond_0

    const/16 v0, 0x1e0

    :goto_0
    if-gt v1, v0, :cond_1

    const/4 v0, 0x1

    :goto_1
    return v0

    :cond_0
    const/16 v0, 0x168

    goto :goto_0

    :cond_1
    const/4 v0, 0x0

    goto :goto_1
.end method


# virtual methods
.method public determineIdealFormat([Lcom/google/android/exoplayer/chunk/Format;JJZZ)Lcom/google/android/exoplayer/chunk/Format;
    .locals 4
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "bitrateEstimate"    # J
    .param p4, "historyPredictedBandwidth"    # J
    .param p6, "isHdDisabled"    # Z
    .param p7, "isFastNetwork"    # Z

    .prologue
    .line 45
    const/4 v0, 0x0

    .line 46
    .local v0, "selectedFormat":Lcom/google/android/exoplayer/chunk/Format;
    const-wide/16 v2, -0x1

    cmp-long v1, p2, v2

    if-nez v1, :cond_1

    .line 47
    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->startResolutionAlgorithm:I

    const/4 v2, 0x1

    if-ne v1, v2, :cond_0

    .line 48
    invoke-direct {p0, p1, p4, p5, p6}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineFormatBasedOnHistory([Lcom/google/android/exoplayer/chunk/Format;JZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v0

    .line 50
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 65
    :goto_0
    return-object v1

    .line 54
    :cond_0
    invoke-direct {p0, p1, p6, p7}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineFormatBasedOnNetwork([Lcom/google/android/exoplayer/chunk/Format;ZZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v0

    .line 55
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 56
    goto :goto_0

    .line 59
    :cond_1
    invoke-direct {p0, p1, p2, p3, p6}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->determineFormatBasedOnBitrate([Lcom/google/android/exoplayer/chunk/Format;JZ)Lcom/google/android/exoplayer/chunk/Format;

    move-result-object v0

    .line 60
    if-eqz v0, :cond_2

    move-object v1, v0

    .line 61
    goto :goto_0

    .line 65
    :cond_2
    array-length v1, p1

    add-int/lit8 v1, v1, -0x1

    aget-object v1, p1, v1

    goto :goto_0
.end method

.method public determineTrickPlayFormat([Lcom/google/android/exoplayer/chunk/Format;J)Lcom/google/android/exoplayer/chunk/Format;
    .locals 4
    .param p1, "formats"    # [Lcom/google/android/exoplayer/chunk/Format;
    .param p2, "bitrateEstimate"    # J

    .prologue
    .line 72
    const-wide/16 v2, -0x1

    cmp-long v2, p2, v2

    if-eqz v2, :cond_1

    .line 74
    const-wide/16 v2, 0x80

    div-long/2addr p2, v2

    .line 75
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    array-length v2, p1

    if-ge v1, v2, :cond_1

    .line 76
    aget-object v0, p1, v1

    .line 77
    .local v0, "format":Lcom/google/android/exoplayer/chunk/Format;
    iget v2, v0, Lcom/google/android/exoplayer/chunk/Format;->height:I

    const/16 v3, 0x2d0

    if-ge v2, v3, :cond_0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->isBitrateSufficient(Lcom/google/android/exoplayer/chunk/Format;J)Z

    move-result v2

    if-eqz v2, :cond_0

    .line 83
    .end local v0    # "format":Lcom/google/android/exoplayer/chunk/Format;
    .end local v1    # "i":I
    :goto_1
    return-object v0

    .line 75
    .restart local v0    # "format":Lcom/google/android/exoplayer/chunk/Format;
    .restart local v1    # "i":I
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 83
    .end local v0    # "format":Lcom/google/android/exoplayer/chunk/Format;
    .end local v1    # "i":I
    :cond_1
    array-length v2, p1

    add-int/lit8 v2, v2, -0x1

    aget-object v0, p1, v2

    goto :goto_1
.end method

.method public getAvailableBitrateForVideo(J)I
    .locals 3
    .param p1, "bitrateEstimate"    # J

    .prologue
    .line 133
    const-wide/16 v0, -0x1

    cmp-long v0, p1, v0

    if-nez v0, :cond_0

    const/high16 v0, -0x40800000    # -1.0f

    :goto_0
    float-to-int v0, v0

    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->audioBitrate:I

    int-to-long v0, v0

    sub-long v0, p1, v0

    long-to-float v0, v0

    iget v1, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->bandwidthFraction:F

    mul-float/2addr v0, v1

    goto :goto_0
.end method

.method public setAudioBitrate(I)V
    .locals 0
    .param p1, "audioBitrate"    # I

    .prologue
    .line 37
    iput p1, p0, Lcom/google/android/videos/player/exo/adaptive/FormatSelectionHelper;->audioBitrate:I

    .line 38
    return-void
.end method
