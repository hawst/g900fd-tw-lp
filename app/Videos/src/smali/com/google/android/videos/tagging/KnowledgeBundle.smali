.class public Lcom/google/android/videos/tagging/KnowledgeBundle;
.super Ljava/lang/Object;
.source "KnowledgeBundle.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/KnowledgeBundle$RecentActorComparator;,
        Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;,
        Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    }
.end annotation


# instance fields
.field public final imageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field public final tagStream:Lcom/google/android/videos/tagging/TagStream;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/TagStream;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "tagStream"    # Lcom/google/android/videos/tagging/TagStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/TagStream;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p2, "imageRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 61
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/tagging/TagStream;

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    .line 62
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    .line 64
    return-void
.end method


# virtual methods
.method public getDescription(Lcom/google/android/videos/tagging/KnowledgeEntity;ZLandroid/app/Activity;)Ljava/lang/String;
    .locals 4
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p2, "isRecentActor"    # Z
    .param p3, "activity"    # Landroid/app/Activity;

    .prologue
    const/4 v1, 0x1

    const/4 v3, 0x0

    .line 188
    if-eqz p2, :cond_0

    .line 189
    const v0, 0x7f0b01ea

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p3, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 193
    :goto_0
    return-object v0

    .line 190
    :cond_0
    instance-of v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Song;

    if-eqz v0, :cond_1

    .line 191
    const v0, 0x7f0b01eb

    new-array v1, v1, [Ljava/lang/Object;

    iget-object v2, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    aput-object v2, v1, v3

    invoke-virtual {p3, v0, v1}, Landroid/app/Activity;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0

    .line 193
    :cond_1
    iget-object v0, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->name:Ljava/lang/String;

    goto :goto_0
.end method

.method public getRecentActors(IILjava/util/Set;Ljava/util/List;)V
    .locals 8
    .param p1, "fromMillis"    # I
    .param p2, "toMillis"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Ljava/util/List",
            "<-",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Person;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 148
    .local p3, "excludeLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    .local p4, "outList":Ljava/util/List;, "Ljava/util/List<-Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v6}, Lcom/google/android/videos/tagging/TagStream;->isValid()Z

    move-result v6

    if-nez v6, :cond_0

    .line 169
    :goto_0
    return-void

    .line 151
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v6}, Lcom/google/android/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v1

    .line 153
    .local v1, "allKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    new-instance v5, Ljava/util/TreeMap;

    sget-object v6, Lcom/google/android/videos/tagging/KnowledgeBundle$RecentActorComparator;->INSTANCE:Lcom/google/android/videos/tagging/KnowledgeBundle$RecentActorComparator;

    invoke-direct {v5, v6}, Ljava/util/TreeMap;-><init>(Ljava/util/Comparator;)V

    .line 154
    .local v5, "recentActorsWithLastAppearance":Ljava/util/TreeMap;, "Ljava/util/TreeMap<Landroid/util/Pair<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Ljava/lang/Integer;>;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_1
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v6

    if-ge v2, v6, :cond_3

    .line 155
    invoke-interface {v1, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 156
    .local v3, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    instance-of v6, v3, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v6, :cond_2

    if-eqz p3, :cond_1

    iget v6, v3, Lcom/google/android/videos/tagging/KnowledgeEntity;->localId:I

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-interface {p3, v6}, Ljava/util/Set;->contains(Ljava/lang/Object;)Z

    move-result v6

    if-nez v6, :cond_2

    :cond_1
    move-object v0, v3

    .line 158
    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 159
    .local v0, "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-virtual {v0, p2}, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->lastAppearance(I)I

    move-result v4

    .line 160
    .local v4, "lastAppearanceMillis":I
    if-gt p1, v4, :cond_2

    if-ge v4, p2, :cond_2

    .line 161
    invoke-static {v4}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    invoke-static {v0, v6}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v6

    invoke-virtual {v5, v6, v0}, Ljava/util/TreeMap;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 162
    invoke-virtual {v5}, Ljava/util/TreeMap;->size()I

    move-result v6

    const/4 v7, 0x5

    if-le v6, v7, :cond_2

    .line 163
    invoke-virtual {v5}, Ljava/util/TreeMap;->pollLastEntry()Ljava/util/Map$Entry;

    .line 154
    .end local v0    # "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    .end local v4    # "lastAppearanceMillis":I
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_1

    .line 168
    .end local v3    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    :cond_3
    invoke-virtual {v5}, Ljava/util/TreeMap;->values()Ljava/util/Collection;

    move-result-object v6

    invoke-interface {p4, v6}, Ljava/util/List;->addAll(Ljava/util/Collection;)Z

    goto :goto_0
.end method

.method public inflateCurrentActorsCard(ILjava/util/Set;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Landroid/app/Activity;)Landroid/view/View;
    .locals 6
    .param p1, "timeMillis"    # I
    .param p3, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p4, "onActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
    .param p5, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;",
            "Landroid/app/Activity;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 90
    .local p2, "excludeLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 91
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 92
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 94
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 95
    .local v2, "currentActors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    iget-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->tagStream:Lcom/google/android/videos/tagging/TagStream;

    invoke-virtual {v5}, Lcom/google/android/videos/tagging/TagStream;->getAllKnowledgeEntities()Ljava/util/List;

    move-result-object v1

    .line 96
    .local v1, "allKnowledgeEntities":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity;>;"
    const/4 v3, 0x0

    .local v3, "i":I
    :goto_0
    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v5

    if-ge v3, v5, :cond_1

    .line 97
    invoke-interface {v1, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 98
    .local v4, "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    instance-of v5, v4, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v5, :cond_0

    move-object v0, v4

    .line 99
    check-cast v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    .line 100
    .local v0, "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-virtual {v0, p1}, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->appearsAt(I)Z

    move-result v5

    if-eqz v5, :cond_0

    iget v5, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->localId:I

    invoke-static {v5}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p2, v5}, Ljava/util/Set;->add(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 101
    invoke-interface {v2, v0}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 96
    .end local v0    # "actor":Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    :cond_0
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 105
    .end local v4    # "knowledgeEntity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    :cond_1
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v5

    if-eqz v5, :cond_2

    .line 106
    const/4 v5, 0x0

    .line 108
    :goto_1
    return-object v5

    :cond_2
    iget-object v5, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {v2, p3, p4, v5, p5}, Lcom/google/android/videos/tagging/ActorsCards;->createCurrentActorsCard(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)Landroid/view/View;

    move-result-object v5

    goto :goto_1
.end method

.method public inflateRecentActorsCard(IILjava/util/Set;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Landroid/app/Activity;)Landroid/view/View;
    .locals 2
    .param p1, "fromMillis"    # I
    .param p2, "toMillis"    # I
    .param p4, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p5, "onRecentActorsCardClickListener"    # Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;
    .param p6, "activity"    # Landroid/app/Activity;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II",
            "Ljava/util/Set",
            "<",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;",
            "Landroid/app/Activity;",
            ")",
            "Landroid/view/View;"
        }
    .end annotation

    .prologue
    .line 124
    .local p3, "excludeLocalIds":Ljava/util/Set;, "Ljava/util/Set<Ljava/lang/Integer;>;"
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 125
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 126
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 128
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    .line 129
    .local v0, "recentActors":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Person;>;"
    invoke-virtual {p0, p1, p2, p3, v0}, Lcom/google/android/videos/tagging/KnowledgeBundle;->getRecentActors(IILjava/util/Set;Ljava/util/List;)V

    .line 130
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 131
    const/4 v1, 0x0

    .line 133
    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {v0, p4, p5, v1, p6}, Lcom/google/android/videos/tagging/ActorsCards;->createRecentActorsCard(Ljava/util/List;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/tagging/KnowledgeBundle$OnActorClickListener;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;)Landroid/view/View;

    move-result-object v1

    goto :goto_0
.end method

.method public inflateSongCards(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;Ljava/util/Collection;)V
    .locals 7
    .param p1, "song"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Song;
    .param p2, "cardInflater"    # Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "storeStatusMonitor"    # Lcom/google/android/videos/store/StoreStatusMonitor;
    .param p5, "account"    # Ljava/lang/String;
    .param p6, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Song;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;",
            "Landroid/app/Activity;",
            "Lcom/google/android/videos/store/StoreStatusMonitor;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Ljava/util/Collection",
            "<",
            "Landroid/view/View;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 74
    .local p7, "knowledgeCards":Ljava/util/Collection;, "Ljava/util/Collection<Landroid/view/View;>;"
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 75
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 76
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 77
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 78
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 79
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    .line 80
    iget-object v2, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    move-object v0, p1

    move-object v1, p2

    move-object v3, p3

    move-object v4, p4

    move-object v5, p5

    move-object v6, p6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/tagging/SongCards;->create(Lcom/google/android/videos/tagging/KnowledgeEntity$Song;Lcom/google/android/videos/tagging/KnowledgeBundle$CardInflater;Lcom/google/android/videos/async/Requester;Landroid/app/Activity;Lcom/google/android/videos/store/StoreStatusMonitor;Ljava/lang/String;Lcom/google/android/videos/logging/EventLogger;)Landroid/view/View;

    move-result-object v0

    invoke-interface {p7, v0}, Ljava/util/Collection;->add(Ljava/lang/Object;)Z

    .line 82
    return-void
.end method

.method public initThumbnailItem(Lcom/google/android/videos/tagging/KnowledgeEntity;ZLandroid/app/Activity;Landroid/view/View;)V
    .locals 5
    .param p1, "knowledgeEntity"    # Lcom/google/android/videos/tagging/KnowledgeEntity;
    .param p2, "isRecentActor"    # Z
    .param p3, "activity"    # Landroid/app/Activity;
    .param p4, "itemView"    # Landroid/view/View;

    .prologue
    .line 173
    const v3, 0x7f0f00a5

    invoke-virtual {p4, v3}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    .line 174
    .local v1, "imageView":Landroid/widget/ImageView;
    iget-object v3, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-eqz v3, :cond_1

    .line 175
    instance-of v3, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    if-eqz v3, :cond_0

    .line 176
    invoke-virtual {p3}, Landroid/app/Activity;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    .line 177
    .local v2, "resources":Landroid/content/res/Resources;
    iget-object v3, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    const v4, 0x7f0e01bb

    invoke-virtual {v2, v4}, Landroid/content/res/Resources;->getDimensionPixelSize(I)I

    move-result v4

    invoke-static {v3, v1, v4}, Lcom/google/android/videos/tagging/Cards;->setImageMatrixIfSquareCropExists(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/widget/ImageView;I)V

    .line 180
    .end local v2    # "resources":Landroid/content/res/Resources;
    :cond_0
    iget-object v3, p1, Lcom/google/android/videos/tagging/KnowledgeEntity;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    iget-object v4, p0, Lcom/google/android/videos/tagging/KnowledgeBundle;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {v1, v3, v4}, Lcom/google/android/videos/tagging/Cards;->requestImage(Landroid/widget/ImageView;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Requester;)V

    .line 182
    :cond_1
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/tagging/KnowledgeBundle;->getDescription(Lcom/google/android/videos/tagging/KnowledgeEntity;ZLandroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 183
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v1, v0}, Landroid/widget/ImageView;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 184
    return-void
.end method
