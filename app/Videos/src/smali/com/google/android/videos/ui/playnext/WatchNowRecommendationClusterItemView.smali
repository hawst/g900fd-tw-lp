.class public Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;
.super Lcom/google/android/videos/ui/playnext/CardItemView;
.source "WatchNowRecommendationClusterItemView.java"

# interfaces
.implements Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;
    }
.end annotation


# instance fields
.field private backgroundView:Landroid/view/View;

.field private boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field private buyButtonView:Landroid/widget/Button;

.field private buyPanelView:Landroid/view/View;

.field private defaultBackgroundDrawable:Landroid/graphics/drawable/Drawable;

.field private episodePriceFromView:Landroid/widget/TextView;

.field private fiveStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private infoPanelView:Landroid/view/View;

.field private isShow:Z

.field private minCardWidthForBuyPanelBesidesPoster:I

.field private playButtonView:Landroid/view/View;

.field private posterUrl:Ljava/lang/String;

.field private posterView:Landroid/widget/ImageView;

.field private preorderButtonView:Landroid/widget/Button;

.field private purchaseFromButtonView:Landroid/widget/Button;

.field private ratingPanelView:Landroid/view/View;

.field private remeasureNeeded:Z

.field private rentButtonView:Landroid/widget/Button;

.field private rottenTomatoRatingView:Landroid/widget/TextView;

.field private screenshotBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

.field private showEpisodesButtonView:Landroid/widget/Button;

.field private spacerView:Landroid/view/View;

.field private titleView:Landroid/widget/TextView;

.field private wishlistButtonView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 87
    invoke-direct {p0, p1}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;)V

    .line 88
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 91
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 92
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;
    .param p3, "defStyle"    # I

    .prologue
    .line 95
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/playnext/CardItemView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 96
    return-void
.end method

.method private bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z
    .locals 6
    .param p1, "view"    # Landroid/widget/TextView;
    .param p2, "preorderAllowed"    # Z
    .param p3, "offers"    # [Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;
    .param p4, "offerType"    # I
    .param p5, "singleOfferStringResId"    # I
    .param p6, "cheapestOfferStringResId"    # I

    .prologue
    const/4 v2, 0x1

    const/4 v1, 0x0

    .line 258
    invoke-static {p2, p3, p4}, Lcom/google/android/videos/utils/OfferUtil;->getCheapestOffer(Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;I)Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;

    move-result-object v0

    .line 259
    .local v0, "cheapestOffer":Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    if-nez v0, :cond_0

    .line 260
    const/16 v2, 0x8

    invoke-virtual {p1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 267
    .end local p5    # "singleOfferStringResId":I
    :goto_0
    return v1

    .line 263
    .restart local p5    # "singleOfferStringResId":I
    :cond_0
    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 264
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v3

    iget-boolean v4, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->singleOffer:Z

    if-eqz v4, :cond_1

    .end local p5    # "singleOfferStringResId":I
    :goto_1
    new-array v4, v2, [Ljava/lang/Object;

    iget-object v5, v0, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedAmount:Ljava/lang/String;

    aput-object v5, v4, v1

    invoke-virtual {v3, p5, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p1, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    move v1, v2

    .line 267
    goto :goto_0

    .restart local p5    # "singleOfferStringResId":I
    :cond_1
    move p5, p6

    .line 264
    goto :goto_1
.end method

.method private static isShowingMultiline(Landroid/widget/TextView;)Z
    .locals 3
    .param p0, "view"    # Landroid/widget/TextView;

    .prologue
    const/4 v1, 0x0

    .line 382
    invoke-virtual {p0}, Landroid/widget/TextView;->getVisibility()I

    move-result v2

    if-eqz v2, :cond_1

    .line 386
    :cond_0
    :goto_0
    return v1

    .line 385
    :cond_1
    invoke-virtual {p0}, Landroid/widget/TextView;->getLayout()Landroid/text/Layout;

    move-result-object v0

    .line 386
    .local v0, "layout":Landroid/text/Layout;
    if-eqz v0, :cond_0

    invoke-virtual {v0, v1}, Landroid/text/Layout;->getEllipsisCount(I)I

    move-result v2

    if-lez v2, :cond_0

    const/4 v1, 0x1

    goto :goto_0
.end method

.method private removeButtonOutlineProvidersV21()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 133
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 134
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 135
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 136
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->showEpisodesButtonView:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setOutlineProvider(Landroid/view/ViewOutlineProvider;)V

    .line 137
    return-void
.end method


# virtual methods
.method bind(Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Lcom/google/android/videos/ui/AssetImageUriCreator;ZZLcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;)V
    .locals 14
    .param p1, "recommendation"    # Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;
    .param p3, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "imageUriCreator"    # Lcom/google/android/videos/ui/AssetImageUriCreator;
    .param p5, "supportsDirectPurchase"    # Z
    .param p6, "isNetworkConnected"    # Z
    .param p7, "binder"    # Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/ui/AssetImageUriCreator;",
            "ZZ",
            "Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;",
            ")V"
        }
    .end annotation

    .prologue
    .line 145
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Landroid/graphics/Bitmap;>;"
    iget-object v8, p1, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 146
    .local v8, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    iget-object v1, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    const/16 v2, 0x12

    if-ne v1, v2, :cond_0

    const/4 v1, 0x1

    :goto_0
    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShow:Z

    .line 149
    iget-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShow:Z

    if-nez v1, :cond_1

    invoke-static {v8}, Lcom/google/android/videos/api/AssetResourceUtil;->findTrailerId(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v12, 0x1

    .line 151
    .local v12, "playTrailerButtonVisible":Z
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->playButtonView:Landroid/view/View;

    if-eqz v12, :cond_2

    const/4 v1, 0x0

    :goto_2
    invoke-virtual {v2, v1}, Landroid/view/View;->setVisibility(I)V

    .line 154
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->titleView:Landroid/widget/TextView;

    iget-object v2, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 157
    iget-object v1, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    const/4 v2, 0x1

    const/4 v3, 0x2

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v10

    .line 159
    .local v10, "fiveStarRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-nez v10, :cond_3

    .line 160
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->fiveStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 165
    :goto_3
    iget-object v1, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->viewerRating:[Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    const/4 v2, 0x2

    const/4 v3, 0x3

    invoke-static {v1, v2, v3}, Lcom/google/android/videos/api/AssetResourceUtil;->getAggregatedUserRating([Lcom/google/wireless/android/video/magma/proto/ViewerRating;II)Lcom/google/wireless/android/video/magma/proto/ViewerRating;

    move-result-object v13

    .line 167
    .local v13, "rottenTomatoRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    if-nez v13, :cond_4

    .line 168
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rottenTomatoRatingView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 179
    :goto_4
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->wishlistButtonView:Landroid/view/View;

    iget-boolean v2, p1, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    invoke-virtual {v1, v2}, Landroid/view/View;->setSelected(Z)V

    .line 180
    iget-boolean v1, p1, Lcom/google/android/videos/adapter/WatchNowRecommendationDataSource$Item;->isWishlisted:Z

    if-eqz v1, :cond_6

    const v9, 0x7f0b01b3

    .line 183
    .local v9, "contentDescriptionResId":I
    :goto_5
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->wishlistButtonView:Landroid/view/View;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v2, v9, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/view/View;->setContentDescription(Ljava/lang/CharSequence;)V

    .line 187
    const/4 v1, 0x0

    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->remeasureNeeded:Z

    .line 188
    iget-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShow:Z

    if-eqz v1, :cond_7

    .line 189
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 190
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 191
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 192
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 193
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->showEpisodesButtonView:Landroid/widget/Button;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 194
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->episodePriceFromView:Landroid/widget/TextView;

    const/4 v3, 0x1

    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, -0x1

    const v6, 0x7f0b0191

    const v7, 0x7f0b0191

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z

    .line 197
    move-object/from16 v0, p4

    invoke-virtual {v0, v8}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getShowPosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterUrl:Ljava/lang/String;

    .line 232
    :goto_6
    iput-object v8, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 233
    const v2, 0x7f0f00c9

    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->screenshotBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    const/4 v5, 0x0

    const-class v6, Landroid/net/Uri;

    move-object v1, p0

    move-object/from16 v4, p2

    invoke-virtual/range {v1 .. v6}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 235
    const v2, 0x7f0f00e5

    const/4 v6, 0x0

    const-class v7, Landroid/net/Uri;

    move-object v1, p0

    move-object v3, p0

    move-object/from16 v4, p2

    move-object/from16 v5, p3

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->registerBitmapView(ILcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V

    .line 239
    if-nez p6, :cond_a

    const/4 v1, 0x1

    :goto_7
    invoke-virtual {p0, v1}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->setDimmedStyle(Z)V

    .line 246
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyPanelView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 247
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->ratingPanelView:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->requestLayout()V

    .line 249
    move-object/from16 v0, p7

    invoke-virtual {v0, p0, v8}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView$Binder;->childImpression(Lcom/google/android/videos/ui/playnext/ClusterItemView;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    .line 250
    return-void

    .line 146
    .end local v9    # "contentDescriptionResId":I
    .end local v10    # "fiveStarRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    .end local v12    # "playTrailerButtonVisible":Z
    .end local v13    # "rottenTomatoRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_0
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 149
    :cond_1
    const/4 v12, 0x0

    goto/16 :goto_1

    .line 151
    .restart local v12    # "playTrailerButtonVisible":Z
    :cond_2
    const/16 v1, 0x8

    goto/16 :goto_2

    .line 162
    .restart local v10    # "fiveStarRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_3
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->fiveStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 163
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->fiveStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    iget v2, v10, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    invoke-virtual {v1, v2}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    goto/16 :goto_3

    .line 170
    .restart local v13    # "rottenTomatoRating":Lcom/google/wireless/android/video/magma/proto/ViewerRating;
    :cond_4
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rottenTomatoRatingView:Landroid/widget/TextView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 171
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rottenTomatoRatingView:Landroid/widget/TextView;

    iget-object v1, v13, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->thumbsUpAndDown:Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;

    iget-boolean v1, v1, Lcom/google/wireless/android/video/magma/proto/ViewerRating$ThumbsUpAndDown;->recommended:Z

    if-eqz v1, :cond_5

    const v1, 0x7f0200ac

    :goto_8
    const/4 v3, 0x0

    const/4 v4, 0x0

    const/4 v5, 0x0

    invoke-virtual {v2, v1, v3, v4, v5}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 174
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rottenTomatoRatingView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    const v3, 0x7f0b0190

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/Object;

    const/4 v5, 0x0

    iget v6, v13, Lcom/google/wireless/android/video/magma/proto/ViewerRating;->ratingScore:F

    float-to-int v6, v6

    invoke-static {v6}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v6

    aput-object v6, v4, v5

    invoke-virtual {v2, v3, v4}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto/16 :goto_4

    .line 171
    :cond_5
    const v1, 0x7f020106

    goto :goto_8

    .line 180
    :cond_6
    const v9, 0x7f0b01b2

    goto/16 :goto_5

    .line 199
    .restart local v9    # "contentDescriptionResId":I
    :cond_7
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->episodePriceFromView:Landroid/widget/TextView;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 200
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->showEpisodesButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 201
    if-eqz p5, :cond_9

    .line 203
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, 0x2

    const v6, 0x7f0b0198

    const v7, 0x7f0b0195

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z

    move-result v11

    .line 206
    .local v11, "hasBuyOrRentButton":Z
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    const/4 v3, 0x0

    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, 0x1

    const v6, 0x7f0b0199

    const v7, 0x7f0b0196

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z

    move-result v1

    or-int/2addr v11, v1

    .line 210
    if-eqz v11, :cond_8

    .line 211
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 218
    :goto_9
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    const/4 v3, 0x1

    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, -0x1

    const v6, 0x7f0b0192

    const v7, 0x7f0b0192

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z

    move-result v1

    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->remeasureNeeded:Z

    .line 227
    .end local v11    # "hasBuyOrRentButton":Z
    :goto_a
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 228
    move-object/from16 v0, p4

    invoke-virtual {v0, v8}, Lcom/google/android/videos/ui/AssetImageUriCreator;->getMoviePosterUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterUrl:Ljava/lang/String;

    goto/16 :goto_6

    .line 213
    .restart local v11    # "hasBuyOrRentButton":Z
    :cond_8
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    const/4 v3, 0x1

    iget-object v4, v8, Lcom/google/wireless/android/video/magma/proto/AssetResource;->offer:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    const/4 v5, -0x1

    const v6, 0x7f0b019e

    const v7, 0x7f0b0197

    move-object v1, p0

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->bindViewWithPrice(Landroid/widget/TextView;Z[Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;III)Z

    goto :goto_9

    .line 222
    .end local v11    # "hasBuyOrRentButton":Z
    :cond_9
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 223
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    .line 224
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    const/16 v2, 0x8

    invoke-virtual {v1, v2}, Landroid/widget/Button;->setVisibility(I)V

    goto :goto_a

    .line 239
    :cond_a
    const/4 v1, 0x0

    goto/16 :goto_7
.end method

.method protected collectClickableViews([Landroid/view/View;)V
    .locals 2
    .param p1, "clickableViews"    # [Landroid/view/View;

    .prologue
    .line 419
    const/4 v0, 0x0

    aput-object p0, p1, v0

    .line 420
    const/4 v0, 0x1

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->playButtonView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 421
    const/4 v0, 0x2

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->wishlistButtonView:Landroid/view/View;

    aput-object v1, p1, v0

    .line 422
    const/4 v0, 0x3

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    aput-object v1, p1, v0

    .line 423
    const/4 v0, 0x4

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    aput-object v1, p1, v0

    .line 424
    const/4 v0, 0x5

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    aput-object v1, p1, v0

    .line 425
    const/4 v0, 0x6

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    aput-object v1, p1, v0

    .line 426
    const/4 v0, 0x7

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->showEpisodesButtonView:Landroid/widget/Button;

    aput-object v1, p1, v0

    .line 427
    return-void
.end method

.method public getThumbnailTag()Ljava/lang/Object;
    .locals 1

    .prologue
    .line 393
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getTag()Ljava/lang/Object;

    move-result-object v0

    return-object v0
.end method

.method protected onFinishInflate()V
    .locals 3

    .prologue
    .line 100
    invoke-super {p0}, Lcom/google/android/videos/ui/playnext/CardItemView;->onFinishInflate()V

    .line 101
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    const v2, 0x7f020131

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/BitmapLoader;->createDefaultBitmapView(Landroid/widget/ImageView;ZI)Lcom/google/android/videos/ui/BitmapLoader$DefaultBitmapView;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->screenshotBitmapView:Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    .line 103
    const v0, 0x7f0f00e0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->spacerView:Landroid/view/View;

    .line 104
    const v0, 0x7f0f00e1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->playButtonView:Landroid/view/View;

    .line 105
    const v0, 0x7f0f00e5

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    .line 106
    const v0, 0x7f0f00e2

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->infoPanelView:Landroid/view/View;

    .line 107
    const v0, 0x7f0f00e3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->backgroundView:Landroid/view/View;

    .line 108
    const v0, 0x7f0f00a8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->titleView:Landroid/widget/TextView;

    .line 109
    const v0, 0x7f0f00ca

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->wishlistButtonView:Landroid/view/View;

    .line 110
    const v0, 0x7f0f00e7

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->ratingPanelView:Landroid/view/View;

    .line 111
    const v0, 0x7f0f00db

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->fiveStarRatingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 112
    const v0, 0x7f0f00e8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rottenTomatoRatingView:Landroid/widget/TextView;

    .line 113
    const v0, 0x7f0f00e4

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyPanelView:Landroid/view/View;

    .line 114
    const v0, 0x7f0f00e9

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->episodePriceFromView:Landroid/widget/TextView;

    .line 115
    const v0, 0x7f0f00eb

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    .line 116
    const v0, 0x7f0f00ea

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    .line 117
    const v0, 0x7f0f00ec

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    .line 118
    const v0, 0x7f0f00ed

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    .line 119
    const v0, 0x7f0f00ee

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->showEpisodesButtonView:Landroid/widget/Button;

    .line 121
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->backgroundView:Landroid/view/View;

    invoke-virtual {v0}, Landroid/view/View;->getBackground()Landroid/graphics/drawable/Drawable;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->defaultBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    .line 122
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v0

    const v1, 0x7f0e01d5

    invoke-virtual {v0, v1}, Landroid/content/res/Resources;->getDimensionPixelOffset(I)I

    move-result v0

    iput v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->minCardWidthForBuyPanelBesidesPoster:I

    .line 125
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 126
    invoke-direct {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->removeButtonOutlineProvidersV21()V

    .line 128
    :cond_0
    return-void
.end method

.method protected onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;
    .locals 7
    .param p1, "id"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<Q:",
            "Ljava/lang/Object;",
            ">(I",
            "Ljava/lang/Class",
            "<TQ;>;)TQ;"
        }
    .end annotation

    .prologue
    .local p2, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 275
    sparse-switch p1, :sswitch_data_0

    .line 294
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    .line 296
    :goto_0
    return-object v0

    .line 278
    :sswitch_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x4

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;

    move-result-object v6

    .line 281
    .local v6, "url":Ljava/lang/String;
    if-nez v6, :cond_0

    iget-boolean v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShow:Z

    if-eqz v0, :cond_0

    .line 283
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->boundAsset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->images:[Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;

    const/4 v1, 0x2

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v2}, Landroid/widget/ImageView;->getWidth()I

    move-result v2

    iget-object v3, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->thumbnailView:Landroid/widget/ImageView;

    invoke-virtual {v3}, Landroid/widget/ImageView;->getHeight()I

    move-result v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl([Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata$Image;IIIFZ)Ljava/lang/String;

    move-result-object v6

    .line 296
    :cond_0
    :goto_1
    if-nez v6, :cond_1

    const/4 v0, 0x0

    goto :goto_0

    .line 290
    .end local v6    # "url":Ljava/lang/String;
    :sswitch_1
    iget-object v6, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterUrl:Ljava/lang/String;

    .line 291
    .restart local v6    # "url":Ljava/lang/String;
    goto :goto_1

    .line 296
    :cond_1
    invoke-static {v6}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/lang/Class;->cast(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    goto :goto_0

    .line 275
    :sswitch_data_0
    .sparse-switch
        0x7f0f00c9 -> :sswitch_0
        0x7f0f00e5 -> :sswitch_1
    .end sparse-switch
.end method

.method protected onMeasure(II)V
    .locals 11
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    .line 303
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v9

    if-eqz v9, :cond_1

    .line 304
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v9

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getPaddingLeft()I

    move-result v10

    sub-int/2addr v9, v10

    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getPaddingRight()I

    move-result v10

    sub-int v0, v9, v10

    .line 311
    .local v0, "availableWidth":I
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getParent()Landroid/view/ViewParent;

    move-result-object v5

    .line 312
    .local v5, "parent":Landroid/view/ViewParent;
    instance-of v9, v5, Landroid/view/View;

    if-eqz v9, :cond_4

    check-cast v5, Landroid/view/View;

    .end local v5    # "parent":Landroid/view/ViewParent;
    invoke-virtual {v5}, Landroid/view/View;->getHeight()I

    move-result v2

    .line 314
    .local v2, "containerHeight":I
    :goto_0
    if-ge v2, v0, :cond_5

    const/4 v8, 0x1

    .line 317
    .local v8, "tooShort":Z
    :goto_1
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->spacerView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v7

    check-cast v7, Landroid/widget/RelativeLayout$LayoutParams;

    .line 319
    .local v7, "spacerViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->infoPanelView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v3

    check-cast v3, Landroid/widget/RelativeLayout$LayoutParams;

    .line 321
    .local v3, "infoPanelViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v8, :cond_6

    .line 324
    const/16 v9, 0x8

    const v10, 0x7f0f00c9

    invoke-virtual {v3, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 325
    const/4 v9, 0x6

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 326
    const/4 v9, 0x6

    const v10, 0x7f0f00e2

    invoke-virtual {v7, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 327
    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 336
    :goto_2
    int-to-float v10, v0

    if-eqz v8, :cond_7

    const/high16 v9, 0x40900000    # 4.5f

    :goto_3
    div-float v9, v10, v9

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v6, v9

    .line 337
    .local v6, "posterWidth":I
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    invoke-virtual {v9}, Landroid/widget/ImageView;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 339
    .local v4, "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->width:I

    .line 340
    iget-boolean v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShow:Z

    if-eqz v9, :cond_8

    .line 342
    iput v6, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 343
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->backgroundView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v9

    check-cast v9, Landroid/widget/RelativeLayout$LayoutParams;

    iget v9, v9, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    iget v10, v4, Landroid/widget/RelativeLayout$LayoutParams;->leftMargin:I

    add-int/2addr v9, v10

    iput v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    .line 351
    :goto_4
    iget v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    iget-object v10, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    invoke-virtual {v10}, Landroid/widget/ImageView;->getPaddingBottom()I

    move-result v10

    add-int/2addr v9, v10

    iput v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 355
    if-nez v8, :cond_0

    iget v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->minCardWidthForBuyPanelBesidesPoster:I

    if-le v0, v9, :cond_9

    :cond_0
    const/4 v1, 0x1

    .line 357
    .local v1, "buyPanelBesidesPoster":Z
    :goto_5
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyPanelView:Landroid/view/View;

    invoke-virtual {v9}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v4

    .end local v4    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    check-cast v4, Landroid/widget/RelativeLayout$LayoutParams;

    .line 358
    .restart local v4    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    if-eqz v1, :cond_a

    .line 359
    const/4 v9, 0x1

    const v10, 0x7f0f00e5

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 360
    const/16 v9, 0x8

    const v10, 0x7f0f00e5

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 361
    const/4 v9, 0x3

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 369
    .end local v0    # "availableWidth":I
    .end local v1    # "buyPanelBesidesPoster":Z
    .end local v2    # "containerHeight":I
    .end local v3    # "infoPanelViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v4    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v6    # "posterWidth":I
    .end local v7    # "spacerViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .end local v8    # "tooShort":Z
    :cond_1
    :goto_6
    invoke-super {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/CardItemView;->onMeasure(II)V

    .line 371
    iget-boolean v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->remeasureNeeded:Z

    if-eqz v9, :cond_3

    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    invoke-static {v9}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShowingMultiline(Landroid/widget/TextView;)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    invoke-static {v9}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShowingMultiline(Landroid/widget/TextView;)Z

    move-result v9

    if-nez v9, :cond_2

    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    invoke-static {v9}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->isShowingMultiline(Landroid/widget/TextView;)Z

    move-result v9

    if-eqz v9, :cond_3

    .line 373
    :cond_2
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->purchaseFromButtonView:Landroid/widget/Button;

    const/4 v10, 0x0

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 374
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->buyButtonView:Landroid/widget/Button;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 375
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->rentButtonView:Landroid/widget/Button;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 376
    iget-object v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->preorderButtonView:Landroid/widget/Button;

    const/16 v10, 0x8

    invoke-virtual {v9, v10}, Landroid/widget/Button;->setVisibility(I)V

    .line 378
    :cond_3
    const/4 v9, 0x0

    iput-boolean v9, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->remeasureNeeded:Z

    .line 379
    return-void

    .line 312
    .restart local v0    # "availableWidth":I
    .restart local v5    # "parent":Landroid/view/ViewParent;
    :cond_4
    invoke-virtual {p0}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->getResources()Landroid/content/res/Resources;

    move-result-object v9

    invoke-virtual {v9}, Landroid/content/res/Resources;->getDisplayMetrics()Landroid/util/DisplayMetrics;

    move-result-object v9

    iget v2, v9, Landroid/util/DisplayMetrics;->heightPixels:I

    goto/16 :goto_0

    .line 314
    .end local v5    # "parent":Landroid/view/ViewParent;
    .restart local v2    # "containerHeight":I
    :cond_5
    const/4 v8, 0x0

    goto/16 :goto_1

    .line 329
    .restart local v3    # "infoPanelViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v7    # "spacerViewLayoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v8    # "tooShort":Z
    :cond_6
    const/16 v9, 0x8

    const v10, 0x7f0f00c9

    invoke-virtual {v7, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 330
    const/4 v9, 0x6

    const/4 v10, 0x0

    invoke-virtual {v7, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 331
    const/4 v9, 0x6

    const v10, 0x7f0f00e0

    invoke-virtual {v3, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 332
    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual {v3, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_2

    .line 336
    :cond_7
    const/high16 v9, 0x40800000    # 4.0f

    goto/16 :goto_3

    .line 348
    .restart local v4    # "layoutParams":Landroid/widget/RelativeLayout$LayoutParams;
    .restart local v6    # "posterWidth":I
    :cond_8
    int-to-float v9, v6

    const v10, 0x3f31a787

    div-float/2addr v9, v10

    const/high16 v10, 0x3f000000    # 0.5f

    add-float/2addr v9, v10

    float-to-int v9, v9

    iput v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->height:I

    .line 349
    const/4 v9, 0x0

    iput v9, v4, Landroid/widget/RelativeLayout$LayoutParams;->topMargin:I

    goto/16 :goto_4

    .line 355
    :cond_9
    const/4 v1, 0x0

    goto/16 :goto_5

    .line 363
    .restart local v1    # "buyPanelBesidesPoster":Z
    :cond_a
    const/4 v9, 0x3

    const v10, 0x7f0f00e5

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 364
    const/16 v9, 0x8

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    .line 365
    const/4 v9, 0x1

    const/4 v10, 0x0

    invoke-virtual {v4, v9, v10}, Landroid/widget/RelativeLayout$LayoutParams;->addRule(II)V

    goto/16 :goto_6
.end method

.method public setThumbnail(Landroid/util/Pair;Z)V
    .locals 3
    .param p2, "missingThumbnail"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/util/Pair",
            "<",
            "Landroid/graphics/Bitmap;",
            "Landroid/support/v7/graphics/Palette;",
            ">;Z)V"
        }
    .end annotation

    .prologue
    .local p1, "thumbnail":Landroid/util/Pair;, "Landroid/util/Pair<Landroid/graphics/Bitmap;Landroid/support/v7/graphics/Palette;>;"
    const/4 v0, 0x0

    .line 404
    if-nez p2, :cond_0

    if-eqz p1, :cond_0

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    if-nez v1, :cond_1

    .line 405
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    const/4 v2, 0x0

    invoke-virtual {v1, v2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 406
    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->backgroundView:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->defaultBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    .line 413
    :goto_0
    return-void

    .line 408
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    iget-object v1, p1, Landroid/util/Pair;->first:Ljava/lang/Object;

    check-cast v1, Landroid/graphics/Bitmap;

    invoke-virtual {v2, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 409
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    if-nez v1, :cond_2

    .line 410
    .local v0, "backgroundColor":I
    :goto_1
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->backgroundView:Landroid/view/View;

    if-nez v0, :cond_3

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->defaultBackgroundDrawable:Landroid/graphics/drawable/Drawable;

    :goto_2
    invoke-static {v2, v1}, Lcom/google/android/videos/utils/ViewUtil;->setViewBackground(Landroid/view/View;Landroid/graphics/drawable/Drawable;)V

    goto :goto_0

    .line 409
    .end local v0    # "backgroundColor":I
    :cond_2
    iget-object v1, p1, Landroid/util/Pair;->second:Ljava/lang/Object;

    check-cast v1, Landroid/support/v7/graphics/Palette;

    invoke-virtual {v1, v0}, Landroid/support/v7/graphics/Palette;->getDarkMutedColor(I)I

    move-result v0

    goto :goto_1

    .line 410
    .restart local v0    # "backgroundColor":I
    :cond_3
    new-instance v1, Landroid/graphics/drawable/ColorDrawable;

    invoke-direct {v1, v0}, Landroid/graphics/drawable/ColorDrawable;-><init>(I)V

    goto :goto_2
.end method

.method public bridge synthetic setThumbnail(Ljava/lang/Object;Z)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Z

    .prologue
    .line 55
    check-cast p1, Landroid/util/Pair;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->setThumbnail(Landroid/util/Pair;Z)V

    return-void
.end method

.method public setThumbnailTag(Ljava/lang/Object;)V
    .locals 1
    .param p1, "tag"    # Ljava/lang/Object;

    .prologue
    .line 398
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/WatchNowRecommendationClusterItemView;->posterView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setTag(Ljava/lang/Object;)V

    .line 399
    return-void
.end method
