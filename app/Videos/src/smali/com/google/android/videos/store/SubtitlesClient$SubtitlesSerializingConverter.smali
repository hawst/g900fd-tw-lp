.class Lcom/google/android/videos/store/SubtitlesClient$SubtitlesSerializingConverter;
.super Lcom/google/android/videos/cache/SerializingConverter;
.source "SubtitlesClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SubtitlesClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "SubtitlesSerializingConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/cache/SerializingConverter",
        "<",
        "Lcom/google/android/videos/subtitles/Subtitles;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/videos/cache/SerializingConverter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/store/SubtitlesClient$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/store/SubtitlesClient$1;

    .prologue
    .line 157
    invoke-direct {p0}, Lcom/google/android/videos/store/SubtitlesClient$SubtitlesSerializingConverter;-><init>()V

    return-void
.end method


# virtual methods
.method protected createObjectInputStream(Ljava/io/InputStream;)Ljava/io/ObjectInputStream;
    .locals 3
    .param p1, "source"    # Ljava/io/InputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 161
    new-instance v0, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;

    invoke-direct {v0, p1}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;-><init>(Ljava/io/InputStream;)V

    .line 163
    .local v0, "legacyInputStream":Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;
    const-string v1, "com.google.android.youtube.core.model.Subtitle"

    const-class v2, Lcom/google/android/videos/store/V1Subtitle;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V

    .line 165
    const-string v1, "com.google.android.youtube.core.model.Subtitle$Line"

    const-class v2, Lcom/google/android/videos/store/V1Subtitle$Line;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/BackwardCompatibleObjectInputStream;->registerDelegate(Ljava/lang/String;Ljava/lang/Class;)V

    .line 167
    return-object v0
.end method
