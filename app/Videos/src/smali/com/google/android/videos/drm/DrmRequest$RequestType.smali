.class public final enum Lcom/google/android/videos/drm/DrmRequest$RequestType;
.super Ljava/lang/Enum;
.source "DrmRequest.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/drm/DrmRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x4019
    name = "RequestType"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Enum",
        "<",
        "Lcom/google/android/videos/drm/DrmRequest$RequestType;",
        ">;"
    }
.end annotation


# static fields
.field private static final synthetic $VALUES:[Lcom/google/android/videos/drm/DrmRequest$RequestType;

.field public static final enum OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

.field public static final enum OFFLINE_REFRESH:Lcom/google/android/videos/drm/DrmRequest$RequestType;

.field public static final enum OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

.field public static final enum STREAMING:Lcom/google/android/videos/drm/DrmRequest$RequestType;


# instance fields
.field public final isOffline:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    const/4 v5, 0x3

    const/4 v4, 0x2

    const/4 v3, 0x0

    const/4 v2, 0x1

    .line 19
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    const-string v1, "STREAMING"

    invoke-direct {v0, v1, v3, v3}, Lcom/google/android/videos/drm/DrmRequest$RequestType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->STREAMING:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    .line 25
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    const-string v1, "OFFLINE"

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/videos/drm/DrmRequest$RequestType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    .line 31
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    const-string v1, "OFFLINE_REFRESH"

    invoke-direct {v0, v1, v4, v2}, Lcom/google/android/videos/drm/DrmRequest$RequestType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    .line 35
    new-instance v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    const-string v1, "OFFLINE_UNPIN"

    invoke-direct {v0, v1, v5, v2}, Lcom/google/android/videos/drm/DrmRequest$RequestType;-><init>(Ljava/lang/String;IZ)V

    sput-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    .line 15
    const/4 v0, 0x4

    new-array v0, v0, [Lcom/google/android/videos/drm/DrmRequest$RequestType;

    sget-object v1, Lcom/google/android/videos/drm/DrmRequest$RequestType;->STREAMING:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    aput-object v1, v0, v3

    sget-object v1, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    aput-object v1, v0, v2

    sget-object v1, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_REFRESH:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    aput-object v1, v0, v4

    sget-object v1, Lcom/google/android/videos/drm/DrmRequest$RequestType;->OFFLINE_UNPIN:Lcom/google/android/videos/drm/DrmRequest$RequestType;

    aput-object v1, v0, v5

    sput-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->$VALUES:[Lcom/google/android/videos/drm/DrmRequest$RequestType;

    return-void
.end method

.method private constructor <init>(Ljava/lang/String;IZ)V
    .locals 0
    .param p3, "isOffline"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(Z)V"
        }
    .end annotation

    .prologue
    .line 39
    invoke-direct {p0, p1, p2}, Ljava/lang/Enum;-><init>(Ljava/lang/String;I)V

    .line 40
    iput-boolean p3, p0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->isOffline:Z

    .line 41
    return-void
.end method

.method public static valueOf(Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest$RequestType;
    .locals 1
    .param p0, "name"    # Ljava/lang/String;

    .prologue
    .line 15
    const-class v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    invoke-static {v0, p0}, Ljava/lang/Enum;->valueOf(Ljava/lang/Class;Ljava/lang/String;)Ljava/lang/Enum;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;

    return-object v0
.end method

.method public static values()[Lcom/google/android/videos/drm/DrmRequest$RequestType;
    .locals 1

    .prologue
    .line 15
    sget-object v0, Lcom/google/android/videos/drm/DrmRequest$RequestType;->$VALUES:[Lcom/google/android/videos/drm/DrmRequest$RequestType;

    invoke-virtual {v0}, [Lcom/google/android/videos/drm/DrmRequest$RequestType;->clone()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, [Lcom/google/android/videos/drm/DrmRequest$RequestType;

    return-object v0
.end method
