.class Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;
.super Ljava/lang/Object;
.source "DrmManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/drm/DrmManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DrmRequestRunnable"
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final request:Lcom/google/android/videos/drm/DrmRequest;

.field final synthetic this$0:Lcom/google/android/videos/drm/DrmManager;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p2, "request"    # Lcom/google/android/videos/drm/DrmRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/drm/DrmRequest;",
            "Lcom/google/android/videos/drm/DrmResponse;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 303
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iput-object p1, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->this$0:Lcom/google/android/videos/drm/DrmManager;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 304
    iput-object p2, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->request:Lcom/google/android/videos/drm/DrmRequest;

    .line 305
    iput-object p3, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->callback:Lcom/google/android/videos/async/Callback;

    .line 306
    return-void
.end method


# virtual methods
.method public run()V
    .locals 3

    .prologue
    .line 310
    iget-object v0, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->this$0:Lcom/google/android/videos/drm/DrmManager;

    iget-object v1, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->request:Lcom/google/android/videos/drm/DrmRequest;

    iget-object v2, p0, Lcom/google/android/videos/drm/DrmManager$DrmRequestRunnable;->callback:Lcom/google/android/videos/async/Callback;

    # invokes: Lcom/google/android/videos/drm/DrmManager;->requestLicense(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V
    invoke-static {v0, v1, v2}, Lcom/google/android/videos/drm/DrmManager;->access$100(Lcom/google/android/videos/drm/DrmManager;Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 311
    return-void
.end method
