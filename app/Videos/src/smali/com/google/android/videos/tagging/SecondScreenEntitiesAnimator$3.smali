.class Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;
.super Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;
.source "SecondScreenEntitiesAnimator.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;->startTranslationChange(Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;II)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

.field final synthetic val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

.field final synthetic val$translationAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;


# direct methods
.method constructor <init>(Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;Landroid/support/v4/view/ViewPropertyAnimatorCompat;Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;)V
    .locals 0

    .prologue
    .line 114
    iput-object p1, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->this$0:Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator;

    iput-object p2, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->val$translationAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    iput-object p3, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    invoke-direct {p0}, Lcom/google/android/videos/ui/DefaultItemAnimator$VpaListenerAdapter;-><init>()V

    return-void
.end method


# virtual methods
.method public onAnimationEnd(Landroid/view/View;)V
    .locals 3
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v2, 0x0

    .line 117
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->val$translationAnimator:Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v4/view/ViewPropertyAnimatorCompat;->setListener(Landroid/support/v4/view/ViewPropertyAnimatorListener;)Landroid/support/v4/view/ViewPropertyAnimatorCompat;

    .line 118
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationX(Landroid/view/View;F)V

    .line 119
    iget-object v0, p0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAnimator$3;->val$holder:Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;

    iget-object v0, v0, Lcom/google/android/videos/tagging/SecondScreenEntitiesAdapter$EntityViewHolder;->itemView:Landroid/view/View;

    invoke-static {v0, v2}, Landroid/support/v4/view/ViewCompat;->setTranslationY(Landroid/view/View;F)V

    .line 120
    return-void
.end method
