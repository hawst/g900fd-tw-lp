.class Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;
.super Landroid/support/v17/leanback/widget/Presenter;
.source "KnowledgeControlsAdapter.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "DescriptionPresenter"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)V
    .locals 0

    .prologue
    .line 185
    iput-object p1, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    invoke-direct {p0}, Landroid/support/v17/leanback/widget/Presenter;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;
    .param p2, "x1"    # Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$1;

    .prologue
    .line 185
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;-><init>(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)V

    return-void
.end method


# virtual methods
.method public onBindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;)V
    .locals 7
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "object"    # Ljava/lang/Object;

    .prologue
    .line 189
    iget-object v3, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v3, Landroid/widget/TextView;

    .local v3, "textView":Landroid/widget/TextView;
    move-object v1, p2

    .line 190
    check-cast v1, Lcom/google/android/videos/tagging/KnowledgeEntity;

    .line 191
    .local v1, "entity":Lcom/google/android/videos/tagging/KnowledgeEntity;
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # invokes: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->getIndexOf(Lcom/google/android/videos/tagging/KnowledgeEntity;)I
    invoke-static {v4, v1}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$300(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;Lcom/google/android/videos/tagging/KnowledgeEntity;)I

    move-result v2

    .line 192
    .local v2, "position":I
    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$500(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Lcom/google/android/videos/tagging/KnowledgeBundle;

    move-result-object v5

    iget-object v4, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->recentActorsStartIndex:I
    invoke-static {v4}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$400(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)I

    move-result v4

    if-lt v2, v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter$DescriptionPresenter;->this$0:Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;

    # getter for: Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->activity:Landroid/app/Activity;
    invoke-static {v6}, Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;->access$200(Lcom/google/android/videos/pano/playback/KnowledgeControlsAdapter;)Landroid/app/Activity;

    move-result-object v6

    invoke-virtual {v5, v1, v4, v6}, Lcom/google/android/videos/tagging/KnowledgeBundle;->getDescription(Lcom/google/android/videos/tagging/KnowledgeEntity;ZLandroid/app/Activity;)Ljava/lang/String;

    move-result-object v0

    .line 194
    .local v0, "description":Ljava/lang/String;
    invoke-virtual {v3, v0}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 195
    return-void

    .line 192
    .end local v0    # "description":Ljava/lang/String;
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method public onCreateViewHolder(Landroid/view/ViewGroup;)Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .locals 2
    .param p1, "parent"    # Landroid/view/ViewGroup;

    .prologue
    .line 199
    new-instance v0, Landroid/widget/TextView;

    invoke-virtual {p1}, Landroid/view/ViewGroup;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/widget/TextView;-><init>(Landroid/content/Context;)V

    .line 200
    .local v0, "view":Landroid/widget/TextView;
    new-instance v1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    invoke-direct {v1, v0}, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;-><init>(Landroid/view/View;)V

    return-object v1
.end method

.method public onUnbindViewHolder(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 2
    .param p1, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 205
    iget-object v0, p1, Landroid/support/v17/leanback/widget/Presenter$ViewHolder;->view:Landroid/view/View;

    check-cast v0, Landroid/widget/TextView;

    .line 206
    .local v0, "textView":Landroid/widget/TextView;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 207
    return-void
.end method
