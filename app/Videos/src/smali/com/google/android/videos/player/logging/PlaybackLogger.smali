.class public Lcom/google/android/videos/player/logging/PlaybackLogger;
.super Landroid/content/BroadcastReceiver;
.source "PlaybackLogger.java"

# interfaces
.implements Lcom/google/android/videos/player/logging/SessionTimeProvider;


# static fields
.field private static final networkIntentFilter:Landroid/content/IntentFilter;


# instance fields
.field private final allClients:[Lcom/google/android/videos/player/logging/LoggingClient;

.field private final connectivityManager:Landroid/net/ConnectivityManager;

.field private final context:Landroid/content/Context;

.field private final derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

.field private networkSubtype:I

.field private networkType:I

.field private final offlineClients:[Lcom/google/android/videos/player/logging/LoggingClient;

.field private sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

.field private sessionStartTimeMs:J

.field private sessionStarted:Z


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 75
    new-instance v0, Landroid/content/IntentFilter;

    const-string v1, "android.net.conn.CONNECTIVITY_CHANGE"

    invoke-direct {v0, v1}, Landroid/content/IntentFilter;-><init>(Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkIntentFilter:Landroid/content/IntentFilter;

    return-void
.end method

.method public varargs constructor <init>(Lcom/google/android/videos/Config;Landroid/content/Context;[Lcom/google/android/videos/player/logging/LoggingClient;)V
    .locals 2
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "context"    # Landroid/content/Context;
    .param p3, "playbackLoggingClients"    # [Lcom/google/android/videos/player/logging/LoggingClient;

    .prologue
    .line 91
    invoke-direct {p0}, Landroid/content/BroadcastReceiver;-><init>()V

    .line 92
    iput-object p2, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->context:Landroid/content/Context;

    .line 93
    iput-object p3, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->allClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .line 94
    invoke-static {p3}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getOfflineClients([Lcom/google/android/videos/player/logging/LoggingClient;)[Lcom/google/android/videos/player/logging/LoggingClient;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->offlineClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .line 95
    new-instance v0, Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-interface {p1}, Lcom/google/android/videos/Config;->exoEarlyPlaybackCutoffTimeMs()I

    move-result v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;-><init>(I)V

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    .line 96
    const-string v0, "connectivity"

    invoke-virtual {p2, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/net/ConnectivityManager;

    iput-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->connectivityManager:Landroid/net/ConnectivityManager;

    .line 98
    return-void
.end method

.method private static getOfflineClients([Lcom/google/android/videos/player/logging/LoggingClient;)[Lcom/google/android/videos/player/logging/LoggingClient;
    .locals 7
    .param p0, "playbackLoggingClients"    # [Lcom/google/android/videos/player/logging/LoggingClient;

    .prologue
    .line 101
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 102
    .local v4, "offlineClientsList":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/player/logging/LoggingClient;>;"
    move-object v0, p0

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v5, v0, v1

    .line 103
    .local v5, "playbackLoggingClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v5}, Lcom/google/android/videos/player/logging/LoggingClient;->supportsOfflinePlaybacks()Z

    move-result v6

    if-eqz v6, :cond_0

    .line 104
    invoke-virtual {v4, v5}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 102
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 107
    .end local v5    # "playbackLoggingClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_1
    invoke-virtual {v4}, Ljava/util/ArrayList;->size()I

    move-result v6

    new-array v3, v6, [Lcom/google/android/videos/player/logging/LoggingClient;

    .line 108
    .local v3, "offlineClients":[Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-virtual {v4, v3}, Ljava/util/ArrayList;->toArray([Ljava/lang/Object;)[Ljava/lang/Object;

    .line 109
    return-object v3
.end method

.method private maybeNotifyNetworkType(IZ)V
    .locals 9
    .param p1, "sessionTimeMs"    # I
    .param p2, "forceNotify"    # Z

    .prologue
    const/4 v5, -0x1

    .line 348
    iget-object v8, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->connectivityManager:Landroid/net/ConnectivityManager;

    invoke-virtual {v8}, Landroid/net/ConnectivityManager;->getActiveNetworkInfo()Landroid/net/NetworkInfo;

    move-result-object v4

    .line 349
    .local v4, "networkInfo":Landroid/net/NetworkInfo;
    if-eqz v4, :cond_2

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->isConnected()Z

    move-result v8

    if-eqz v8, :cond_2

    const/4 v1, 0x1

    .line 350
    .local v1, "connected":Z
    :goto_0
    if-eqz v1, :cond_3

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getType()I

    move-result v6

    .line 351
    .local v6, "networkType":I
    :goto_1
    if-eqz v1, :cond_0

    invoke-virtual {v4}, Landroid/net/NetworkInfo;->getSubtype()I

    move-result v5

    .line 352
    .local v5, "networkSubtype":I
    :cond_0
    if-nez p2, :cond_1

    iget v8, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkType:I

    if-ne v8, v6, :cond_1

    iget v8, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkSubtype:I

    if-eq v8, v5, :cond_4

    .line 353
    :cond_1
    iput v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkType:I

    .line 354
    iput v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkSubtype:I

    .line 355
    iget-object v8, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v8, v6}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onNetworkType(I)V

    .line 356
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_2
    if-ge v2, v3, :cond_4

    aget-object v7, v0, v2

    .line 357
    .local v7, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v7, p1, v6, v5}, Lcom/google/android/videos/player/logging/LoggingClient;->onNetworkType(III)V

    .line 356
    add-int/lit8 v2, v2, 0x1

    goto :goto_2

    .line 349
    .end local v0    # "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    .end local v1    # "connected":Z
    .end local v2    # "i$":I
    .end local v3    # "len$":I
    .end local v5    # "networkSubtype":I
    .end local v6    # "networkType":I
    .end local v7    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_2
    const/4 v1, 0x0

    goto :goto_0

    .restart local v1    # "connected":Z
    :cond_3
    move v6, v5

    .line 350
    goto :goto_1

    .line 360
    .restart local v5    # "networkSubtype":I
    .restart local v6    # "networkType":I
    :cond_4
    return-void
.end method


# virtual methods
.method public endSession(I)V
    .locals 7
    .param p1, "mediaTimeMs"    # I

    .prologue
    .line 366
    iget-boolean v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v6}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 367
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v5

    .line 368
    .local v5, "sessionTimeMs":I
    iget-object v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v6, v5}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->endSession(I)Lcom/google/android/videos/player/logging/DerivedStats;

    move-result-object v1

    .line 369
    .local v1, "derivedStats":Lcom/google/android/videos/player/logging/DerivedStats;
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v3, v0

    .local v3, "len$":I
    const/4 v2, 0x0

    .local v2, "i$":I
    :goto_0
    if-ge v2, v3, :cond_0

    aget-object v4, v0, v2

    .line 370
    .local v4, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v4, v5, p1, v1}, Lcom/google/android/videos/player/logging/LoggingClient;->endSession(IILcom/google/android/videos/player/logging/DerivedStats;)V

    .line 369
    add-int/lit8 v2, v2, 0x1

    goto :goto_0

    .line 372
    .end local v4    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    iget-object v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->context:Landroid/content/Context;

    invoke-virtual {v6, p0}, Landroid/content/Context;->unregisterReceiver(Landroid/content/BroadcastReceiver;)V

    .line 373
    const/4 v6, 0x0

    iput-boolean v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    .line 374
    const/4 v6, 0x0

    iput-object v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .line 375
    return-void
.end method

.method public getSessionTimeMs()I
    .locals 4

    .prologue
    .line 379
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    iget-wide v2, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStartTimeMs:J

    sub-long/2addr v0, v2

    long-to-int v0, v0

    return v0
.end method

.method public isSessionStarted()Z
    .locals 1

    .prologue
    .line 145
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    return v0
.end method

.method public onBandwidthSample(IJJ)V
    .locals 10
    .param p1, "elapsedMs"    # I
    .param p2, "bytes"    # J
    .param p4, "bitrateEstimate"    # J

    .prologue
    .line 283
    iget-boolean v3, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 284
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v2

    .line 285
    .local v2, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v9, v0

    .local v9, "len$":I
    const/4 v8, 0x0

    .local v8, "i$":I
    :goto_0
    if-ge v8, v9, :cond_0

    aget-object v1, v0, v8

    .local v1, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    move v3, p1

    move-wide v4, p2

    move-wide v6, p4

    .line 286
    invoke-interface/range {v1 .. v7}, Lcom/google/android/videos/player/logging/LoggingClient;->onBandwidthSample(IIJJ)V

    .line 285
    add-int/lit8 v8, v8, 0x1

    goto :goto_0

    .line 288
    .end local v1    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onDroppedFrames(I)V
    .locals 6
    .param p1, "count"    # I

    .prologue
    .line 210
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 211
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 212
    .local v4, "sessionTimeMs":I
    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v5, p1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onDroppedFrames(I)V

    .line 213
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 214
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1}, Lcom/google/android/videos/player/logging/LoggingClient;->onDroppedFrames(II)V

    .line 213
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 216
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onError(IILjava/lang/Exception;I)V
    .locals 9
    .param p1, "errorType"    # I
    .param p2, "errorCode"    # I
    .param p3, "exception"    # Ljava/lang/Exception;
    .param p4, "mediaTimeMs"    # I

    .prologue
    .line 177
    iget-boolean v2, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v2}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 178
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v1

    .line 179
    .local v1, "sessionTimeMs":I
    iget-object v2, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onError()V

    .line 180
    iget-object v6, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v6, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v8, v6

    .local v8, "len$":I
    const/4 v7, 0x0

    .local v7, "i$":I
    :goto_0
    if-ge v7, v8, :cond_0

    aget-object v0, v6, v7

    .local v0, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    move v2, p4

    move v3, p1

    move v4, p2

    move-object v5, p3

    .line 181
    invoke-interface/range {v0 .. v5}, Lcom/google/android/videos/player/logging/LoggingClient;->onError(IIIILjava/lang/Exception;)V

    .line 180
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 184
    .end local v0    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onFailed(I)V
    .locals 6
    .param p1, "mediaTimeMs"    # I

    .prologue
    .line 196
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 198
    .local v4, "sessionTimeMs":I
    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v5}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onFailed()V

    .line 199
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 200
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1}, Lcom/google/android/videos/player/logging/LoggingClient;->onFailed(II)V

    .line 199
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 202
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onFormatEnabled(II)V
    .locals 6
    .param p1, "itag"    # I
    .param p2, "trigger"    # I

    .prologue
    .line 254
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 255
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 256
    .local v4, "sessionTimeMs":I
    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v5, v4, p1}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onFormatEnabled(II)V

    .line 257
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 258
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1, p2}, Lcom/google/android/videos/player/logging/LoggingClient;->onFormatEnabled(III)V

    .line 257
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 260
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onFormatSelected(II)V
    .locals 6
    .param p1, "itag"    # I
    .param p2, "trigger"    # I

    .prologue
    .line 241
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 242
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 243
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 244
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1, p2}, Lcom/google/android/videos/player/logging/LoggingClient;->onFormatSelected(III)V

    .line 243
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 246
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onHttpDataSourceOpened(J)V
    .locals 7
    .param p1, "elapsedMs"    # J

    .prologue
    .line 291
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 292
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 293
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 294
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1, p2}, Lcom/google/android/videos/player/logging/LoggingClient;->onHttpDataSourceOpened(IJ)V

    .line 293
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 296
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onLoadingChanged(Z)V
    .locals 6
    .param p1, "loading"    # Z

    .prologue
    .line 268
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 269
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 270
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 271
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1}, Lcom/google/android/videos/player/logging/LoggingClient;->onLoadingChanged(IZ)V

    .line 270
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 273
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onReceive(Landroid/content/Context;Landroid/content/Intent;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "intent"    # Landroid/content/Intent;

    .prologue
    .line 340
    iget-boolean v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    if-nez v0, :cond_0

    .line 345
    :goto_0
    return-void

    .line 344
    :cond_0
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v0

    const/4 v1, 0x0

    invoke-direct {p0, v0, v1}, Lcom/google/android/videos/player/logging/PlaybackLogger;->maybeNotifyNetworkType(IZ)V

    goto :goto_0
.end method

.method public onStateChanged(ZII)V
    .locals 6
    .param p1, "playWhenReady"    # Z
    .param p2, "playbackState"    # I
    .param p3, "mediaTimeMs"    # I

    .prologue
    .line 158
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 159
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 160
    .local v4, "sessionTimeMs":I
    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v5, v4, p1, p2}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onStateChanged(IZI)V

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 162
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p3, p1, p2}, Lcom/google/android/videos/player/logging/LoggingClient;->onStateChanged(IIZI)V

    .line 161
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 165
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onSubtitleEnabled(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 6
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 317
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 318
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 319
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 320
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1}, Lcom/google/android/videos/player/logging/LoggingClient;->onSubtitleEnabled(ILcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 319
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 322
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onSubtitleError(Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V
    .locals 6
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 331
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 332
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 333
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 334
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1, p2}, Lcom/google/android/videos/player/logging/LoggingClient;->onSubtitleError(ILcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Exception;)V

    .line 333
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 336
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onSubtitleSelected(Lcom/google/android/videos/subtitles/SubtitleTrack;)V
    .locals 6
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;

    .prologue
    .line 304
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 305
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 306
    .local v4, "sessionTimeMs":I
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 307
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1}, Lcom/google/android/videos/player/logging/LoggingClient;->onSubtitleSelected(ILcom/google/android/videos/subtitles/SubtitleTrack;)V

    .line 306
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 309
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public onUserSeekingChanged(IZZ)V
    .locals 6
    .param p1, "mediaTimeMs"    # I
    .param p2, "seeking"    # Z
    .param p3, "isFineScrubbing"    # Z

    .prologue
    .line 225
    iget-boolean v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 226
    invoke-virtual {p0}, Lcom/google/android/videos/player/logging/PlaybackLogger;->getSessionTimeMs()I

    move-result v4

    .line 227
    .local v4, "sessionTimeMs":I
    iget-object v5, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    invoke-virtual {v5, v4, p2}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->onUserSeekingChanged(IZ)V

    .line 228
    iget-object v0, p0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v0, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_0

    aget-object v3, v0, v1

    .line 229
    .local v3, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    invoke-interface {v3, v4, p1, p2, p3}, Lcom/google/android/videos/player/logging/LoggingClient;->onUserSeekingChanged(IIZZ)V

    .line 228
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 231
    .end local v3    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    :cond_0
    return-void
.end method

.method public startSession(Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZILjava/util/List;)V
    .locals 17
    .param p1, "sessionNonce"    # Ljava/lang/String;
    .param p2, "playerType"    # I
    .param p3, "account"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "showId"    # Ljava/lang/String;
    .param p6, "seasonId"    # Ljava/lang/String;
    .param p7, "isTrailer"    # Z
    .param p8, "isOffline"    # Z
    .param p9, "durationMs"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "ZZI",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/streams/MediaStream;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 130
    .local p10, "mediaStreams":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/streams/MediaStream;>;"
    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    if-nez v5, :cond_0

    const/4 v5, 0x1

    :goto_0
    invoke-static {v5}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 131
    invoke-static/range {p1 .. p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 132
    if-eqz p8, :cond_1

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->offlineClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    :goto_1
    move-object/from16 v0, p0

    iput-object v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .line 133
    const/4 v5, 0x1

    move-object/from16 v0, p0

    iput-boolean v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStarted:Z

    .line 134
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v6

    move-object/from16 v0, p0

    iput-wide v6, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionStartTimeMs:J

    .line 135
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->derivedStatsHelper:Lcom/google/android/videos/player/logging/DerivedStatsHelper;

    move/from16 v0, p2

    move/from16 v1, p9

    move-object/from16 v2, p10

    invoke-virtual {v5, v0, v1, v2}, Lcom/google/android/videos/player/logging/DerivedStatsHelper;->startSession(IILjava/util/List;)V

    .line 136
    move-object/from16 v0, p0

    iget-object v14, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->sessionClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    .local v14, "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    array-length v0, v14

    move/from16 v16, v0

    .local v16, "len$":I
    const/4 v15, 0x0

    .local v15, "i$":I
    :goto_2
    move/from16 v0, v16

    if-ge v15, v0, :cond_2

    aget-object v4, v14, v15

    .local v4, "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    move-object/from16 v5, p0

    move-object/from16 v6, p1

    move/from16 v7, p2

    move-object/from16 v8, p3

    move-object/from16 v9, p4

    move-object/from16 v10, p5

    move-object/from16 v11, p6

    move/from16 v12, p7

    move/from16 v13, p8

    .line 137
    invoke-interface/range {v4 .. v13}, Lcom/google/android/videos/player/logging/LoggingClient;->startSession(Lcom/google/android/videos/player/logging/SessionTimeProvider;Ljava/lang/String;ILjava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    .line 136
    add-int/lit8 v15, v15, 0x1

    goto :goto_2

    .line 130
    .end local v4    # "sessionClient":Lcom/google/android/videos/player/logging/LoggingClient;
    .end local v14    # "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    .end local v15    # "i$":I
    .end local v16    # "len$":I
    :cond_0
    const/4 v5, 0x0

    goto :goto_0

    .line 132
    :cond_1
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->allClients:[Lcom/google/android/videos/player/logging/LoggingClient;

    goto :goto_1

    .line 140
    .restart local v14    # "arr$":[Lcom/google/android/videos/player/logging/LoggingClient;
    .restart local v15    # "i$":I
    .restart local v16    # "len$":I
    :cond_2
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/player/logging/PlaybackLogger;->context:Landroid/content/Context;

    sget-object v6, Lcom/google/android/videos/player/logging/PlaybackLogger;->networkIntentFilter:Landroid/content/IntentFilter;

    move-object/from16 v0, p0

    invoke-virtual {v5, v0, v6}, Landroid/content/Context;->registerReceiver(Landroid/content/BroadcastReceiver;Landroid/content/IntentFilter;)Landroid/content/Intent;

    .line 141
    const/4 v5, 0x0

    const/4 v6, 0x1

    move-object/from16 v0, p0

    invoke-direct {v0, v5, v6}, Lcom/google/android/videos/player/logging/PlaybackLogger;->maybeNotifyNetworkType(IZ)V

    .line 142
    return-void
.end method
