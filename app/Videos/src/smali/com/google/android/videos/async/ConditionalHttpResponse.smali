.class public final Lcom/google/android/videos/async/ConditionalHttpResponse;
.super Ljava/lang/Object;
.source "ConditionalHttpResponse.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# static fields
.field private static final NOT_MODIFIED:Lcom/google/android/videos/async/ConditionalHttpResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<",
            "Ljava/lang/Object;",
            ">;"
        }
    .end annotation
.end field


# instance fields
.field public final eTag:Ljava/lang/String;

.field public final isNotModified:Z

.field public final lastModified:Ljava/lang/String;

.field public final targetResponse:Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "TE;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    .line 20
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpResponse;

    const/4 v1, 0x1

    invoke-direct {v0, v1, v2, v2, v2}, Lcom/google/android/videos/async/ConditionalHttpResponse;-><init>(ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    sput-object v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->NOT_MODIFIED:Lcom/google/android/videos/async/ConditionalHttpResponse;

    return-void
.end method

.method private constructor <init>(ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V
    .locals 0
    .param p1, "isNotModified"    # Z
    .param p3, "eTag"    # Ljava/lang/String;
    .param p4, "lastModified"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(ZTE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 54
    .local p0, "this":Lcom/google/android/videos/async/ConditionalHttpResponse;, "Lcom/google/android/videos/async/ConditionalHttpResponse<TE;>;"
    .local p2, "targetResponse":Ljava/lang/Object;, "TE;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 55
    iput-boolean p1, p0, Lcom/google/android/videos/async/ConditionalHttpResponse;->isNotModified:Z

    .line 56
    iput-object p2, p0, Lcom/google/android/videos/async/ConditionalHttpResponse;->targetResponse:Ljava/lang/Object;

    .line 57
    iput-object p3, p0, Lcom/google/android/videos/async/ConditionalHttpResponse;->eTag:Ljava/lang/String;

    .line 58
    iput-object p4, p0, Lcom/google/android/videos/async/ConditionalHttpResponse;->lastModified:Ljava/lang/String;

    .line 59
    return-void
.end method

.method static synthetic access$000()Lcom/google/android/videos/async/ConditionalHttpResponse;
    .locals 1

    .prologue
    .line 18
    sget-object v0, Lcom/google/android/videos/async/ConditionalHttpResponse;->NOT_MODIFIED:Lcom/google/android/videos/async/ConditionalHttpResponse;

    return-object v0
.end method

.method public static create(Ljava/lang/Object;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/async/ConditionalHttpResponse;
    .locals 2
    .param p1, "eTag"    # Ljava/lang/String;
    .param p2, "lastModified"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(TE;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 25
    .local p0, "targetResponse":Ljava/lang/Object;, "TE;"
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpResponse;

    const/4 v1, 0x0

    invoke-direct {v0, v1, p0, p1, p2}, Lcom/google/android/videos/async/ConditionalHttpResponse;-><init>(ZLjava/lang/Object;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createConverter(Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/converter/HttpResponseConverter;
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Ljava/lang/Object;",
            ">(",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<TE;>;)",
            "Lcom/google/android/videos/converter/HttpResponseConverter",
            "<",
            "Lcom/google/android/videos/async/ConditionalHttpResponse",
            "<TE;>;>;"
        }
    .end annotation

    .prologue
    .line 30
    .local p0, "targetConverter":Lcom/google/android/videos/converter/HttpResponseConverter;, "Lcom/google/android/videos/converter/HttpResponseConverter<TE;>;"
    new-instance v0, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;

    invoke-direct {v0, p0}, Lcom/google/android/videos/async/ConditionalHttpResponse$Converter;-><init>(Lcom/google/android/videos/converter/HttpResponseConverter;)V

    return-object v0
.end method
