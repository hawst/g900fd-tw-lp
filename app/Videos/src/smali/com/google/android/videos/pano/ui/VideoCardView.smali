.class public Lcom/google/android/videos/pano/ui/VideoCardView;
.super Landroid/support/v17/leanback/widget/BaseCardView;
.source "VideoCardView.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "Landroid/support/v17/leanback/widget/BaseCardView;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/async/ControllableRequest",
        "<",
        "Landroid/net/Uri;",
        ">;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final extraView:Landroid/widget/TextView;

.field private fallbackResourceId:I

.field private final imageView:Landroid/widget/ImageView;

.field private final priceView:Lcom/google/android/play/layout/PlayCardLabelView;

.field private final progressBar:Landroid/view/View;

.field private requestTaskControl:Lcom/google/android/videos/async/TaskControl;

.field private final starsView:Lcom/google/android/play/layout/StarRatingBar;

.field private final titleView:Landroid/widget/TextView;

.field private final topView:Landroid/view/ViewGroup;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 48
    const/4 v1, 0x0

    const v2, 0x7f010104

    invoke-direct {p0, p1, v1, v2}, Landroid/support/v17/leanback/widget/BaseCardView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 50
    invoke-static {p1}, Landroid/view/LayoutInflater;->from(Landroid/content/Context;)Landroid/view/LayoutInflater;

    move-result-object v0

    .line 51
    .local v0, "inflater":Landroid/view/LayoutInflater;
    const v1, 0x7f040083

    invoke-virtual {v0, v1, p0}, Landroid/view/LayoutInflater;->inflate(ILandroid/view/ViewGroup;)Landroid/view/View;

    .line 52
    const/4 v1, 0x1

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->setFocusable(Z)V

    .line 54
    const v1, 0x7f0f019c

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/view/ViewGroup;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->topView:Landroid/view/ViewGroup;

    .line 55
    const v1, 0x7f0f0156

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/ImageView;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    .line 56
    const v1, 0x7f0f012e

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->titleView:Landroid/widget/TextView;

    .line 57
    const v1, 0x7f0f0138

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->starsView:Lcom/google/android/play/layout/StarRatingBar;

    .line 58
    const v1, 0x7f0f0082

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/widget/TextView;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    .line 59
    const v1, 0x7f0f0196

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->progressBar:Landroid/view/View;

    .line 60
    const v1, 0x7f0f00d3

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/VideoCardView;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Lcom/google/android/play/layout/PlayCardLabelView;

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    .line 61
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->reset()V

    .line 62
    return-void
.end method

.method private fadeInImageView()V
    .locals 4

    .prologue
    .line 130
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 131
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->isAttachedToWindow()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 132
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/view/ViewPropertyAnimator;->alpha(F)Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const/high16 v2, 0x10e0000

    invoke-virtual {v1, v2}, Landroid/content/res/Resources;->getInteger(I)I

    move-result v1

    int-to-long v2, v1

    invoke-virtual {v0, v2, v3}, Landroid/view/ViewPropertyAnimator;->setDuration(J)Landroid/view/ViewPropertyAnimator;

    .line 135
    :cond_0
    return-void
.end method


# virtual methods
.method public cancelAnimation()V
    .locals 2

    .prologue
    .line 93
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->animate()Landroid/view/ViewPropertyAnimator;

    move-result-object v0

    invoke-virtual {v0}, Landroid/view/ViewPropertyAnimator;->cancel()V

    .line 94
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    const/high16 v1, 0x3f800000    # 1.0f

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setAlpha(F)V

    .line 95
    return-void
.end method

.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 98
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 204
    const/4 v0, 0x0

    return v0
.end method

.method protected onAttachedToWindow()V
    .locals 2

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0}, Landroid/widget/ImageView;->getAlpha()F

    move-result v0

    const/4 v1, 0x0

    cmpl-float v0, v0, v1

    if-nez v0, :cond_0

    .line 82
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->fadeInImageView()V

    .line 84
    :cond_0
    return-void
.end method

.method protected onDetachedFromWindow()V
    .locals 0

    .prologue
    .line 88
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->cancelAnimation()V

    .line 89
    invoke-super {p0}, Landroid/support/v17/leanback/widget/BaseCardView;->onDetachedFromWindow()V

    .line 90
    return-void
.end method

.method public onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p2, "exception"    # Ljava/lang/Exception;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Ljava/lang/Exception;",
            ")V"
        }
    .end annotation

    .prologue
    .line 139
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    iget v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->fallbackResourceId:I

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/VideoCardView;->setImage(I)V

    .line 140
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/VideoCardView;->onError(Lcom/google/android/videos/async/ControllableRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p2, "response"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ")V"
        }
    .end annotation

    .prologue
    .line 125
    .local p1, "request":Lcom/google/android/videos/async/ControllableRequest;, "Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p2}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 126
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->fadeInImageView()V

    .line 127
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 33
    check-cast p1, Lcom/google/android/videos/async/ControllableRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/pano/ui/VideoCardView;->onResponse(Lcom/google/android/videos/async/ControllableRequest;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public reset()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    const/4 v1, 0x0

    .line 65
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 66
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, v3}, Landroid/widget/ImageView;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    .line 67
    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/ui/VideoCardView;->setTitleText(Ljava/lang/CharSequence;)V

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v3}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 70
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v1, v1, v1, v1}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 71
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->cancelAnimation()V

    .line 73
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->starsView:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 74
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v2}, Landroid/widget/TextView;->setVisibility(I)V

    .line 75
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v0, v2}, Landroid/view/View;->setVisibility(I)V

    .line 76
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v0, v2}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 77
    return-void
.end method

.method public setExtra(Ljava/lang/String;IILcom/google/android/videos/utils/OfferUtil$CheapestOffer;F)V
    .locals 9
    .param p1, "subtitle"    # Ljava/lang/String;
    .param p2, "badgeResourceId"    # I
    .param p3, "durationInSeconds"    # I
    .param p4, "cheapestOffer"    # Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;
    .param p5, "starRating"    # F

    .prologue
    const v4, 0x7f0a0054

    const/16 v8, 0x8

    const/4 v7, 0x0

    .line 157
    if-lez p3, :cond_0

    .line 158
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-static {p3, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->formatVideoDuration(ILandroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 159
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 162
    :cond_0
    if-eqz p1, :cond_1

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 167
    :cond_1
    if-eqz p2, :cond_2

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v7, v7, p2, v7}, Landroid/widget/TextView;->setCompoundDrawablesWithIntrinsicBounds(IIII)V

    .line 169
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v7}, Landroid/widget/TextView;->setVisibility(I)V

    .line 172
    :cond_2
    const/4 v0, 0x0

    cmpl-float v0, p5, v0

    if-lez v0, :cond_3

    .line 173
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->starsView:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v7}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 174
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->starsView:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, p5}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 177
    :cond_3
    if-eqz p4, :cond_4

    .line 178
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    .line 179
    .local v6, "resources":Landroid/content/res/Resources;
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-virtual {p4, v1}, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->getFormattedAmount(Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v2

    iget-object v3, p4, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-virtual {v6, v4}, Landroid/content/res/Resources;->getColor(I)I

    move-result v4

    const-string v5, ""

    invoke-virtual/range {v0 .. v5}, Lcom/google/android/play/layout/PlayCardLabelView;->setText(Ljava/lang/String;ILjava/lang/String;ILjava/lang/String;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->priceView:Lcom/google/android/play/layout/PlayCardLabelView;

    invoke-virtual {v0, v7}, Lcom/google/android/play/layout/PlayCardLabelView;->setVisibility(I)V

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->extraView:Landroid/widget/TextView;

    invoke-virtual {v0, v8}, Landroid/widget/TextView;->setVisibility(I)V

    .line 187
    iget-object v0, p4, Lcom/google/android/videos/utils/OfferUtil$CheapestOffer;->offer:Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/AssetResource$Offer;->formattedFullAmount:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 188
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->starsView:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, v8}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 191
    .end local v6    # "resources":Landroid/content/res/Resources;
    :cond_4
    return-void
.end method

.method public setImage(I)V
    .locals 1
    .param p1, "resourceId"    # I

    .prologue
    .line 102
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 103
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->imageView:Landroid/widget/ImageView;

    invoke-virtual {v0, p1}, Landroid/widget/ImageView;->setImageResource(I)V

    .line 104
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/VideoCardView;->fadeInImageView()V

    .line 105
    return-void
.end method

.method public setImage(Ljava/lang/String;Lcom/google/android/videos/async/Requester;Landroid/os/Handler;I)V
    .locals 3
    .param p1, "uri"    # Ljava/lang/String;
    .param p3, "handler"    # Landroid/os/Handler;
    .param p4, "fallbackResourceId"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Landroid/os/Handler;",
            "I)V"
        }
    .end annotation

    .prologue
    .line 110
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    if-nez p1, :cond_0

    .line 111
    invoke-virtual {p0, p4}, Lcom/google/android/videos/pano/ui/VideoCardView;->setImage(I)V

    .line 121
    :goto_0
    return-void

    .line 115
    :cond_0
    iput p4, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->fallbackResourceId:I

    .line 116
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v1}, Lcom/google/android/videos/async/TaskControl;->cancel(Lcom/google/android/videos/async/TaskControl;)V

    .line 117
    new-instance v1, Lcom/google/android/videos/async/TaskControl;

    invoke-direct {v1}, Lcom/google/android/videos/async/TaskControl;-><init>()V

    iput-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    .line 118
    invoke-static {p1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 119
    .local v0, "imageUri":Landroid/net/Uri;
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->requestTaskControl:Lcom/google/android/videos/async/TaskControl;

    invoke-static {v0, v1}, Lcom/google/android/videos/async/ControllableRequest;->create(Ljava/lang/Object;Lcom/google/android/videos/async/TaskStatus;)Lcom/google/android/videos/async/ControllableRequest;

    move-result-object v1

    invoke-static {p3, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v2

    invoke-interface {p2, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method public setMainDimensions(II)V
    .locals 2
    .param p1, "width"    # I
    .param p2, "height"    # I

    .prologue
    .line 143
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->topView:Landroid/view/ViewGroup;

    invoke-virtual {v1}, Landroid/view/ViewGroup;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    .line 144
    .local v0, "params":Landroid/view/ViewGroup$LayoutParams;
    iput p1, v0, Landroid/view/ViewGroup$LayoutParams;->width:I

    .line 145
    iput p2, v0, Landroid/view/ViewGroup$LayoutParams;->height:I

    .line 146
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->topView:Landroid/view/ViewGroup;

    invoke-virtual {v1, v0}, Landroid/view/ViewGroup;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 147
    return-void
.end method

.method public setProgress(F)V
    .locals 3
    .param p1, "progress"    # F

    .prologue
    const/4 v2, 0x0

    .line 194
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v1}, Landroid/view/View;->getLayoutParams()Landroid/view/ViewGroup$LayoutParams;

    move-result-object v0

    check-cast v0, Landroid/widget/LinearLayout$LayoutParams;

    .line 196
    .local v0, "params":Landroid/widget/LinearLayout$LayoutParams;
    iput v2, v0, Landroid/widget/LinearLayout$LayoutParams;->width:I

    .line 197
    iput p1, v0, Landroid/widget/LinearLayout$LayoutParams;->weight:F

    .line 198
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v0}, Landroid/view/View;->setLayoutParams(Landroid/view/ViewGroup$LayoutParams;)V

    .line 199
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 200
    return-void
.end method

.method public setTitleText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 151
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/VideoCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 152
    return-void
.end method
