.class Lcom/google/android/videos/activity/CaptionSettingsActivity$1;
.super Ljava/lang/Object;
.source "CaptionSettingsActivity.java"

# interfaces
.implements Landroid/widget/CompoundButton$OnCheckedChangeListener;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/activity/CaptionSettingsActivity;->installActionBarToggleSwitch()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/activity/CaptionSettingsActivity;


# direct methods
.method constructor <init>(Lcom/google/android/videos/activity/CaptionSettingsActivity;)V
    .locals 0

    .prologue
    .line 246
    iput-object p1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity$1;->this$0:Lcom/google/android/videos/activity/CaptionSettingsActivity;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public onCheckedChanged(Landroid/widget/CompoundButton;Z)V
    .locals 2
    .param p1, "buttonView"    # Landroid/widget/CompoundButton;
    .param p2, "isChecked"    # Z

    .prologue
    .line 249
    iget-object v1, p0, Lcom/google/android/videos/activity/CaptionSettingsActivity$1;->this$0:Lcom/google/android/videos/activity/CaptionSettingsActivity;

    # getter for: Lcom/google/android/videos/activity/CaptionSettingsActivity;->preferences:Landroid/content/SharedPreferences;
    invoke-static {v1}, Lcom/google/android/videos/activity/CaptionSettingsActivity;->access$000(Lcom/google/android/videos/activity/CaptionSettingsActivity;)Landroid/content/SharedPreferences;

    move-result-object v1

    invoke-interface {v1}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    .line 250
    .local v0, "editor":Landroid/content/SharedPreferences$Editor;
    const-string v1, "captioning_enabled"

    invoke-interface {v0, v1, p2}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    .line 251
    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->apply()V

    .line 252
    return-void
.end method
