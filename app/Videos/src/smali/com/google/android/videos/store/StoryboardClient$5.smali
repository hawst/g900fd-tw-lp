.class final Lcom/google/android/videos/store/StoryboardClient$5;
.super Lcom/google/android/videos/store/AbstractFileStore;
.source "StoryboardClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/store/StoryboardClient;->getStoryboardImageStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/AbstractFileStore",
        "<",
        "Lcom/google/android/videos/store/StoryboardImageRequest;",
        "Lcom/google/android/videos/utils/ByteArray;",
        ">;"
    }
.end annotation


# direct methods
.method constructor <init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V
    .locals 0
    .param p1, "x0"    # Landroid/content/Context;

    .prologue
    .line 135
    .local p2, "x1":Lcom/google/android/videos/cache/Converter;, "Lcom/google/android/videos/cache/Converter<Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/store/AbstractFileStore;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-void
.end method


# virtual methods
.method protected generateFilepath(Ljava/io/File;Lcom/google/android/videos/store/StoryboardImageRequest;)Ljava/io/File;
    .locals 3
    .param p1, "rootFileDir"    # Ljava/io/File;
    .param p2, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;

    .prologue
    .line 138
    new-instance v0, Ljava/io/File;

    # invokes: Lcom/google/android/videos/store/StoryboardClient;->getStoryboardDir(Ljava/io/File;)Ljava/io/File;
    invoke-static {p1}, Lcom/google/android/videos/store/StoryboardClient;->access$100(Ljava/io/File;)Ljava/io/File;

    move-result-object v1

    iget-object v2, p2, Lcom/google/android/videos/store/StoryboardImageRequest;->uniqueKey:Ljava/lang/String;

    invoke-direct {v0, v1, v2}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method protected bridge synthetic generateFilepath(Ljava/io/File;Ljava/lang/Object;)Ljava/io/File;
    .locals 1
    .param p1, "x0"    # Ljava/io/File;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 135
    check-cast p2, Lcom/google/android/videos/store/StoryboardImageRequest;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/StoryboardClient$5;->generateFilepath(Ljava/io/File;Lcom/google/android/videos/store/StoryboardImageRequest;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method
