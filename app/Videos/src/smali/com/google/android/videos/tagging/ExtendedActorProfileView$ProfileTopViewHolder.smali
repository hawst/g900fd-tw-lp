.class Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ExtendedActorProfileView.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/ExtendedActorProfileView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ProfileTopViewHolder"
.end annotation


# instance fields
.field private final ageAndBirthplaceLayout:Landroid/view/View;

.field private final ageLabelText:Landroid/widget/TextView;

.field private final ageText:Landroid/widget/TextView;

.field private final birthplaceLabelText:Landroid/widget/TextView;

.field private final birthplaceText:Landroid/widget/TextView;

.field private final dividerLine:Landroid/view/View;

.field private final googleSearchButton:Landroid/widget/Button;

.field private final nameText:Landroid/widget/TextView;

.field private final profileImage:Landroid/widget/TextView;

.field private final profileImageFrame:Landroid/widget/FrameLayout;

.field private final roleText:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Landroid/view/View;)V
    .locals 2
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 436
    iput-object p1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    .line 437
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 438
    const v0, 0x7f0f0109

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/FrameLayout;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImageFrame:Landroid/widget/FrameLayout;

    .line 439
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImageFrame:Landroid/widget/FrameLayout;

    invoke-virtual {v0, p0}, Landroid/widget/FrameLayout;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 440
    const v0, 0x7f0f0112

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Button;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->googleSearchButton:Landroid/widget/Button;

    .line 441
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->googleSearchButton:Landroid/widget/Button;

    invoke-virtual {v0, p0}, Landroid/widget/Button;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 442
    const v0, 0x7f0f010d

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageAndBirthplaceLayout:Landroid/view/View;

    .line 443
    const v0, 0x7f0f00a5

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;

    .line 444
    const v0, 0x7f0f010a

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->nameText:Landroid/widget/TextView;

    .line 445
    const v0, 0x7f0f010b

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->roleText:Landroid/widget/TextView;

    .line 446
    const v0, 0x7f0f010f

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageText:Landroid/widget/TextView;

    .line 447
    const v0, 0x7f0f0111

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceText:Landroid/widget/TextView;

    .line 448
    const v0, 0x7f0f010e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageLabelText:Landroid/widget/TextView;

    .line 449
    const v0, 0x7f0f0110

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceLabelText:Landroid/widget/TextView;

    .line 450
    const v0, 0x7f0f010c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->dividerLine:Landroid/view/View;

    .line 451
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;

    const-string v1, "actor_image_transition"

    invoke-static {v0, v1}, Landroid/support/v4/view/ViewCompat;->setTransitionName(Landroid/view/View;Ljava/lang/String;)V

    .line 452
    return-void
.end method

.method static synthetic access$400(Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;)Landroid/widget/TextView;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;

    .prologue
    .line 421
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;

    return-object v0
.end method


# virtual methods
.method public bind()V
    .locals 15

    .prologue
    .line 464
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    if-nez v0, :cond_3

    .line 465
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    const/4 v2, 0x1

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/tagging/Cards;->setDefaultActorImage(Landroid/widget/TextView;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Z)V

    .line 471
    :goto_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->nameText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 472
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->googleSearchButton:Landroid/widget/Button;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->googleSearchButton:Landroid/widget/Button;

    invoke-virtual {v1}, Landroid/widget/Button;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01dc

    const/4 v3, 0x1

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v5}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setText(Ljava/lang/CharSequence;)V

    .line 474
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->roleText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/tagging/Cards;->buildCharacterNamesString(Landroid/content/res/Resources;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 475
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 476
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 477
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageLabelText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 478
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceLabelText:Landroid/widget/TextView;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 479
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageAndBirthplaceLayout:Landroid/view/View;

    const/16 v1, 0x8

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 480
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 482
    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 485
    sget-object v0, Ljava/util/Locale;->US:Ljava/util/Locale;

    invoke-static {v0}, Ljava/util/Calendar;->getInstance(Ljava/util/Locale;)Ljava/util/Calendar;

    move-result-object v7

    .line 486
    .local v7, "calendar":Ljava/util/Calendar;
    const/4 v0, 0x1

    invoke-virtual {v7, v0}, Ljava/util/Calendar;->get(I)I

    move-result v12

    .line 487
    .local v12, "nowYear":I
    const-string v0, "MM-dd"

    invoke-static {v0, v7}, Landroid/text/format/DateFormat;->format(Ljava/lang/CharSequence;Ljava/util/Calendar;)Ljava/lang/CharSequence;

    move-result-object v0

    invoke-interface {v0}, Ljava/lang/CharSequence;->toString()Ljava/lang/String;

    move-result-object v11

    .line 488
    .local v11, "nowDay":Ljava/lang/String;
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v1, 0x0

    const/4 v2, 0x4

    invoke-virtual {v0, v1, v2}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v9

    .line 489
    .local v9, "dateOfBirthYear":I
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v1, 0x5

    invoke-virtual {v0, v1}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v8

    .line 490
    .local v8, "dateOfBirthDay":Ljava/lang/String;
    sub-int v6, v12, v9

    .line 491
    .local v6, "age":I
    invoke-virtual {v11, v8}, Ljava/lang/String;->compareTo(Ljava/lang/String;)I

    move-result v0

    if-gez v0, :cond_0

    .line 492
    add-int/lit8 v6, v6, -0x1

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageText:Landroid/widget/TextView;

    invoke-static {v6}, Ljava/lang/String;->valueOf(I)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 495
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageLabelText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 500
    .end local v6    # "age":I
    .end local v7    # "calendar":Ljava/util/Calendar;
    .end local v8    # "dateOfBirthDay":Ljava/lang/String;
    .end local v9    # "dateOfBirthYear":I
    .end local v11    # "nowDay":Ljava/lang/String;
    .end local v12    # "nowYear":I
    :goto_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 501
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageAndBirthplaceLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V
    :try_end_0
    .catch Ljava/lang/NumberFormatException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_0 .. :try_end_0} :catch_1

    .line 510
    :cond_1
    :goto_2
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v0

    iget-object v0, v0, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 511
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 512
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceLabelText:Landroid/widget/TextView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 513
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->birthplaceText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->placeOfBirth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 514
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageAndBirthplaceLayout:Landroid/view/View;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 516
    :cond_2
    return-void

    .line 467
    :cond_3
    iget-object v13, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    new-instance v0, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->avatarCropTransformation:Lcom/google/android/play/image/AvatarCropTransformation;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1000(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/play/image/AvatarCropTransformation;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v2

    iget-object v3, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->profileImage:Landroid/widget/TextView;

    iget-object v4, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->profileImageDimension:I
    invoke-static {v4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1100(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)I

    move-result v4

    const/4 v5, 0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView$ActorBitmapView;-><init>(Lcom/google/android/play/image/AvatarCropTransformation;Lcom/google/android/videos/tagging/KnowledgeEntity$Person;Landroid/widget/TextView;IZ)V

    # setter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    invoke-static {v13, v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$902(Lcom/google/android/videos/tagging/ExtendedActorProfileView;Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;)Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    .line 469
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->bitmapView:Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;
    invoke-static {v0}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$900(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntityBitmapView;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->requester:Lcom/google/android/videos/async/Requester;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1200(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->image:Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;)V

    goto/16 :goto_0

    .line 497
    :cond_4
    :try_start_1
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageText:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    invoke-virtual {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    const v2, 0x7f0b01d7

    const/4 v3, 0x2

    new-array v3, v3, [Ljava/lang/Object;

    const/4 v4, 0x0

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v5}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x4

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    const/4 v4, 0x1

    iget-object v5, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v5}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v5

    iget-object v5, v5, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    const/4 v13, 0x0

    const/4 v14, 0x4

    invoke-virtual {v5, v13, v14}, Ljava/lang/String;->substring(II)Ljava/lang/String;

    move-result-object v5

    aput-object v5, v3, v4

    invoke-virtual {v1, v2, v3}, Landroid/content/res/Resources;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V
    :try_end_1
    .catch Ljava/lang/NumberFormatException; {:try_start_1 .. :try_end_1} :catch_0
    .catch Ljava/lang/IndexOutOfBoundsException; {:try_start_1 .. :try_end_1} :catch_1

    goto/16 :goto_1

    .line 502
    :catch_0
    move-exception v10

    .line 503
    .local v10, "e":Ljava/lang/NumberFormatException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error reading actor\'s date of birth ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") or death ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Ljava/lang/NumberFormatException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_2

    .line 505
    .end local v10    # "e":Ljava/lang/NumberFormatException;
    :catch_1
    move-exception v10

    .line 506
    .local v10, "e":Ljava/lang/IndexOutOfBoundsException;
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error reading actor\'s date of birth ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfBirth:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ") or death ("

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v1

    iget-object v1, v1, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->dateOfDeath:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, "): "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v10}, Ljava/lang/IndexOutOfBoundsException;->getMessage()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto/16 :goto_2
.end method

.method public hideViewsExceptProfileImage()V
    .locals 2

    .prologue
    const/4 v1, 0x4

    .line 455
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->ageAndBirthplaceLayout:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 456
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->googleSearchButton:Landroid/widget/Button;

    invoke-virtual {v0, v1}, Landroid/widget/Button;->setVisibility(I)V

    .line 457
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->nameText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 458
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->roleText:Landroid/widget/TextView;

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setVisibility(I)V

    .line 459
    iget-object v0, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->dividerLine:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 460
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 6
    .param p1, "view"    # Landroid/view/View;

    .prologue
    const/4 v5, 0x2

    .line 520
    invoke-virtual {p1}, Landroid/view/View;->getId()I

    move-result v1

    const v2, 0x7f0f0112

    if-ne v1, v2, :cond_0

    .line 521
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1300(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/BrowserUtil;->startWebSearch(Landroid/app/Activity;Ljava/lang/String;)I

    move-result v0

    .line 522
    .local v0, "result":I
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->eventLogger:Lcom/google/android/videos/logging/EventLogger;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1400(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/logging/EventLogger;

    move-result-object v1

    invoke-interface {v1, v5, v0}, Lcom/google/android/videos/logging/EventLogger;->onWebSearch(II)V

    .line 527
    .end local v0    # "result":I
    :goto_0
    return-void

    .line 524
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->activity:Landroid/app/Activity;
    invoke-static {v1}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1300(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Landroid/app/Activity;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->actor:Lcom/google/android/videos/tagging/KnowledgeEntity$Person;
    invoke-static {v2}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$800(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Lcom/google/android/videos/tagging/KnowledgeEntity$Person;

    move-result-object v2

    iget-object v2, v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Person;->name:Ljava/lang/String;

    invoke-static {v2}, Lcom/google/android/videos/tagging/Cards;->toQuery(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const-string v3, "movies"

    iget-object v4, p0, Lcom/google/android/videos/tagging/ExtendedActorProfileView$ProfileTopViewHolder;->this$0:Lcom/google/android/videos/tagging/ExtendedActorProfileView;

    # getter for: Lcom/google/android/videos/tagging/ExtendedActorProfileView;->account:Ljava/lang/String;
    invoke-static {v4}, Lcom/google/android/videos/tagging/ExtendedActorProfileView;->access$1500(Lcom/google/android/videos/tagging/ExtendedActorProfileView;)Ljava/lang/String;

    move-result-object v4

    invoke-static {v1, v2, v3, v4, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0
.end method
