.class public Lcom/google/android/videos/player/DownloadProgressNotifier;
.super Ljava/lang/Object;
.source "DownloadProgressNotifier.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;
    }
.end annotation


# instance fields
.field private final file:Ljava/io/File;

.field private final listener:Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;

.field private scheduler:Ljava/util/concurrent/ScheduledExecutorService;

.field private final totalFileSize:J


# direct methods
.method public constructor <init>(Landroid/net/Uri;JLcom/google/android/videos/player/DownloadProgressNotifier$Listener;)V
    .locals 2
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "totalFileSize"    # J
    .param p4, "listener"    # Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;

    .prologue
    .line 30
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 31
    const-string v0, "file"

    invoke-virtual {p1}, Landroid/net/Uri;->getScheme()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    const-string v1, "Uri is not a file"

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 32
    new-instance v0, Ljava/io/File;

    invoke-virtual {p1}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/io/File;-><init>(Ljava/lang/String;)V

    iput-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->file:Ljava/io/File;

    .line 33
    iput-wide p2, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    .line 34
    iput-object p4, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->listener:Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;

    .line 35
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/DownloadProgressNotifier;)J
    .locals 2
    .param p0, "x0"    # Lcom/google/android/videos/player/DownloadProgressNotifier;

    .prologue
    .line 17
    invoke-direct {p0}, Lcom/google/android/videos/player/DownloadProgressNotifier;->updateFileSize()J

    move-result-wide v0

    return-wide v0
.end method

.method private updateFileSize()J
    .locals 7

    .prologue
    .line 75
    iget-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->file:Ljava/io/File;

    invoke-virtual {v0}, Ljava/io/File;->length()J

    move-result-wide v2

    .line 77
    .local v2, "currentFileSize":J
    iget-wide v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    const-wide/16 v4, 0x0

    cmp-long v0, v0, v4

    if-lez v0, :cond_1

    .line 78
    const-wide/16 v0, 0x64

    mul-long/2addr v0, v2

    iget-wide v4, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    div-long/2addr v0, v4

    long-to-int v6, v0

    .line 82
    .local v6, "bufferedPercent":I
    :goto_0
    iget-object v1, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->listener:Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;

    iget-wide v4, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    invoke-interface/range {v1 .. v6}, Lcom/google/android/videos/player/DownloadProgressNotifier$Listener;->onDownloadProgress(JJI)V

    .line 83
    iget-wide v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    cmp-long v0, v2, v0

    if-nez v0, :cond_0

    .line 84
    invoke-virtual {p0}, Lcom/google/android/videos/player/DownloadProgressNotifier;->stop()V

    .line 86
    :cond_0
    return-wide v2

    .line 80
    .end local v6    # "bufferedPercent":I
    :cond_1
    const/4 v6, 0x0

    .restart local v6    # "bufferedPercent":I
    goto :goto_0
.end method


# virtual methods
.method public declared-synchronized start()V
    .locals 10

    .prologue
    const/4 v0, 0x1

    .line 54
    monitor-enter p0

    :try_start_0
    iget-object v1, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    if-nez v1, :cond_1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkState(Z)V

    .line 55
    invoke-direct {p0}, Lcom/google/android/videos/player/DownloadProgressNotifier;->updateFileSize()J

    move-result-wide v8

    .line 56
    .local v8, "currentFileSize":J
    iget-wide v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->totalFileSize:J

    cmp-long v0, v8, v0

    if-gez v0, :cond_0

    .line 57
    const/4 v0, 0x1

    invoke-static {v0}, Ljava/util/concurrent/Executors;->newScheduledThreadPool(I)Ljava/util/concurrent/ScheduledExecutorService;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    .line 58
    iget-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    new-instance v1, Lcom/google/android/videos/player/DownloadProgressNotifier$1;

    invoke-direct {v1, p0}, Lcom/google/android/videos/player/DownloadProgressNotifier$1;-><init>(Lcom/google/android/videos/player/DownloadProgressNotifier;)V

    const-wide/16 v2, 0x0

    const-wide/16 v4, 0x1

    sget-object v6, Ljava/util/concurrent/TimeUnit;->SECONDS:Ljava/util/concurrent/TimeUnit;

    invoke-interface/range {v0 .. v6}, Ljava/util/concurrent/ScheduledExecutorService;->scheduleWithFixedDelay(Ljava/lang/Runnable;JJLjava/util/concurrent/TimeUnit;)Ljava/util/concurrent/ScheduledFuture;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 65
    :cond_0
    monitor-exit p0

    return-void

    .line 54
    .end local v8    # "currentFileSize":J
    :cond_1
    const/4 v0, 0x0

    goto :goto_0

    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method

.method public declared-synchronized stop()V
    .locals 1

    .prologue
    .line 68
    monitor-enter p0

    :try_start_0
    iget-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    if-eqz v0, :cond_0

    .line 69
    iget-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;

    invoke-interface {v0}, Ljava/util/concurrent/ScheduledExecutorService;->shutdown()V

    .line 70
    const/4 v0, 0x0

    iput-object v0, p0, Lcom/google/android/videos/player/DownloadProgressNotifier;->scheduler:Ljava/util/concurrent/ScheduledExecutorService;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 72
    :cond_0
    monitor-exit p0

    return-void

    .line 68
    :catchall_0
    move-exception v0

    monitor-exit p0

    throw v0
.end method
