.class public Lcom/google/android/videos/activity/LauncherActivity;
.super Landroid/app/Activity;
.source "LauncherActivity.java"


# instance fields
.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 35
    invoke-direct {p0}, Landroid/app/Activity;-><init>()V

    return-void
.end method

.method private static addPinningErrorDialog(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0, "builder"    # Landroid/net/Uri$Builder;

    .prologue
    .line 154
    const-string v0, "pining_error_dialog"

    const/4 v1, 0x1

    invoke-static {v1}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {p0, v0, v1}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 155
    return-object p0
.end method

.method private static buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "uri"    # Landroid/net/Uri;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 167
    new-instance v0, Landroid/content/Intent;

    const-class v1, Lcom/google/android/videos/activity/LauncherActivity;

    invoke-direct {v0, p0, v1}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v0, p1}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "authAccount"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {v0, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private static builderForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 3
    .param p0, "videoId"    # Ljava/lang/String;
    .param p1, "seasonId"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 146
    const-string v1, "shows"

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    .line 147
    .local v0, "builder":Landroid/net/Uri$Builder;
    const-string v1, "v"

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 148
    const-string v1, "se"

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 149
    const-string v1, "sh"

    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    .line 150
    return-object v0
.end method

.method private static builderForMovie(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0, "videoId"    # Ljava/lang/String;

    .prologue
    .line 140
    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 141
    const-string v0, "movies"

    invoke-static {v0}, Lcom/google/android/videos/activity/LauncherActivity;->builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "v"

    invoke-virtual {v0, v1, p0}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method private static builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;
    .locals 2
    .param p0, "vertical"    # Ljava/lang/String;

    .prologue
    .line 159
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "http"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "play.google.com"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "movies"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p0}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    return-object v0
.end method

.method public static createEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "additionalFlags"    # I

    .prologue
    .line 113
    invoke-static {p4, p3, p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 114
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p5}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createEpisodeDeepLinkingIntentWithPinningError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "additionalFlags"    # I

    .prologue
    .line 119
    invoke-static {p4, p3, p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->addPinningErrorDialog(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 120
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p5}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "startDownload"    # Z
    .param p6, "pinningErrorDialog"    # Z

    .prologue
    .line 582
    invoke-static/range {p0 .. p6}, Lcom/google/android/videos/activity/ShowActivity;->createEpisodeIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;
    .locals 2
    .param p1, "baseIntent"    # Landroid/content/Intent;

    .prologue
    .line 554
    new-instance v0, Landroid/content/Intent;

    invoke-direct {v0, p1}, Landroid/content/Intent;-><init>(Landroid/content/Intent;)V

    const-class v1, Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createManageDownloadsDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "additionalFlags"    # I

    .prologue
    .line 133
    const-string v1, "watchnow"

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "mdl"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 136
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p2}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createManageDownloadsIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 592
    invoke-static {p0}, Lcom/google/android/videos/activity/ManageDownloadsActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 73
    invoke-static {p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForMovie(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 74
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p3}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createMovieDeepLinkingIntentWithPinningError(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 79
    invoke-static {p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForMovie(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->addPinningErrorDialog(Landroid/net/Uri$Builder;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 80
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p3}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createMovieIntent(Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "startDownload"    # Z
    .param p4, "pinningErrorDialog"    # Z

    .prologue
    .line 568
    invoke-static {p0, p1, p2, p3, p4}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createPanoHomeIntent()Landroid/content/Intent;
    .locals 1

    .prologue
    .line 550
    invoke-static {p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;

    .prologue
    .line 563
    const/4 v0, 0x0

    invoke-direct {p0, p1, p2, v0}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;
    .param p3, "forceCloseDrawer"    # Z

    .prologue
    .line 559
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/activity/HomeActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createSeasonDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "additionalFlags"    # I

    .prologue
    .line 93
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 94
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 95
    const-string v1, "shows"

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "se"

    invoke-virtual {v1, v2, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "sh"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 99
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p4}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createSeasonIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 577
    invoke-static {p0, p1, p2, p3}, Lcom/google/android/videos/activity/ShowActivity;->createSeasonIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createShowDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 104
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    .line 105
    const-string v1, "shows"

    invoke-static {v1}, Lcom/google/android/videos/activity/LauncherActivity;->builderForVertical(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "sh"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 108
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p3}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createShowIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 573
    invoke-static {p0, p1, p2}, Lcom/google/android/videos/activity/ShowActivity;->createShowIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createWatchEpisodeDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "videoId"    # Ljava/lang/String;
    .param p5, "additionalFlags"    # I

    .prologue
    .line 125
    invoke-static {p4, p3, p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "play"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 128
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p5}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;
    .locals 1
    .param p1, "accountName"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "isTrailer"    # Z

    .prologue
    .line 588
    invoke-static/range {p0 .. p5}, Lcom/google/android/videos/activity/WatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method public static createWatchMovieDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 4
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 85
    invoke-static {p2}, Lcom/google/android/videos/activity/LauncherActivity;->builderForMovie(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "play"

    const/4 v3, 0x1

    invoke-static {v3}, Ljava/lang/Boolean;->toString(Z)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v1, v2, v3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 88
    .local v0, "uri":Landroid/net/Uri;
    invoke-static {p0, v0, p1, p3}, Lcom/google/android/videos/activity/LauncherActivity;->buildIntent(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method public static createWatchTrailerDeepLinkingIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)Landroid/content/Intent;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "additionalFlags"    # I

    .prologue
    .line 60
    new-instance v1, Landroid/net/Uri$Builder;

    invoke-direct {v1}, Landroid/net/Uri$Builder;-><init>()V

    const-string v2, "http"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "play.google.com"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "trailers"

    invoke-virtual {v1, v2}, Landroid/net/Uri$Builder;->appendEncodedPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "v"

    invoke-virtual {v1, v2, p2}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 65
    .local v0, "uri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-class v2, Lcom/google/android/videos/activity/LauncherActivity;

    invoke-direct {v1, p0, v2}, Landroid/content/Intent;-><init>(Landroid/content/Context;Ljava/lang/Class;)V

    invoke-virtual {v1, v0}, Landroid/content/Intent;->setData(Landroid/net/Uri;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    invoke-virtual {v1, p3}, Landroid/content/Intent;->addFlags(I)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private getAccount(Landroid/content/Intent;)Ljava/lang/String;
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 396
    const-string v3, "intent_extra_data_key"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 397
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    move-object v1, v0

    .line 415
    .end local v0    # "account":Ljava/lang/String;
    .local v1, "account":Ljava/lang/String;
    :goto_0
    return-object v1

    .line 400
    .end local v1    # "account":Ljava/lang/String;
    .restart local v0    # "account":Ljava/lang/String;
    :cond_0
    const-string v3, "authAccount"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 401
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_1

    move-object v1, v0

    .line 402
    .end local v0    # "account":Ljava/lang/String;
    .restart local v1    # "account":Ljava/lang/String;
    goto :goto_0

    .line 404
    .end local v1    # "account":Ljava/lang/String;
    .restart local v0    # "account":Ljava/lang/String;
    :cond_1
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 405
    .local v2, "data":Landroid/net/Uri;
    if-eqz v2, :cond_2

    .line 406
    const-string v3, "u"

    invoke-virtual {v2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 408
    :cond_2
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    iget-object v3, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v3

    if-eqz v3, :cond_3

    .line 410
    iget-object v3, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 411
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 412
    iget-object v3, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v3

    invoke-virtual {v3}, Lcom/google/android/videos/accounts/SignInManager;->chooseFirstAccount()Ljava/lang/String;

    move-result-object v0

    :cond_3
    move-object v1, v0

    .line 415
    .end local v0    # "account":Ljava/lang/String;
    .restart local v1    # "account":Ljava/lang/String;
    goto :goto_0
.end method

.method private handleLegacyIntent(Landroid/content/Intent;)V
    .locals 14
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 281
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v11

    .line 282
    .local v11, "uri":Landroid/net/Uri;
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->getAccount(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 283
    .local v1, "account":Ljava/lang/String;
    const-string v0, "force_close_drawer"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 284
    .local v7, "forceCloseDrawer":Z
    const-string v0, "intent_extra_data_key"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->hasExtra(Ljava/lang/String;)Z

    move-result v8

    .line 286
    .local v8, "fromQuickSearchBox":Z
    const/4 v10, 0x0

    .line 287
    .local v10, "startPlayback":Z
    const/4 v5, 0x0

    .line 288
    .local v5, "startDownload":Z
    const-string v0, "download_video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 289
    .local v2, "videoId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_6

    .line 290
    const/4 v5, 0x1

    .line 297
    :cond_0
    :goto_0
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 301
    const-string v0, "details_video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 303
    :cond_1
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_2

    if-eqz v11, :cond_2

    .line 304
    const-string v0, "v"

    invoke-virtual {v11, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 305
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_2

    .line 306
    const-string v6, "start_playback"

    if-nez v8, :cond_7

    const/4 v0, 0x1

    :goto_1
    invoke-virtual {p1, v6, v0}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    .line 310
    :cond_2
    if-nez v11, :cond_8

    const/4 v4, 0x0

    .line 311
    .local v4, "showId":Ljava/lang/String;
    :goto_2
    const-string v0, "season_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 312
    .local v3, "seasonId":Ljava/lang/String;
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_3

    if-eqz v11, :cond_3

    .line 313
    const-string v0, "season_id"

    invoke-virtual {v11, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v9

    .line 314
    .local v9, "queryParameter":Ljava/lang/String;
    invoke-static {v9}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_3

    const-string v0, "tvseason-"

    invoke-virtual {v9, v0}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 315
    const/16 v0, 0x9

    invoke-virtual {v9, v0}, Ljava/lang/String;->substring(I)Ljava/lang/String;

    move-result-object v3

    .line 319
    .end local v9    # "queryParameter":Ljava/lang/String;
    :cond_3
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_4

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    :cond_4
    const-string v12, "shows"

    .line 323
    .local v12, "vertical":Ljava/lang/String;
    :goto_3
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->hasPremiumError()Z

    move-result v0

    if-eqz v0, :cond_b

    .line 324
    invoke-direct {p0, v1, v12, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 365
    :cond_5
    :goto_4
    return-void

    .line 292
    .end local v3    # "seasonId":Ljava/lang/String;
    .end local v4    # "showId":Ljava/lang/String;
    .end local v12    # "vertical":Ljava/lang/String;
    :cond_6
    const-string v0, "video_id"

    invoke-virtual {p1, v0}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 293
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 294
    const-string v0, "start_playback"

    const/4 v6, 0x0

    invoke-virtual {p1, v0, v6}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v10

    goto/16 :goto_0

    .line 306
    :cond_7
    const/4 v0, 0x0

    goto :goto_1

    .line 310
    :cond_8
    const-string v0, "p"

    invoke-virtual {v11, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    goto :goto_2

    .line 319
    .restart local v3    # "seasonId":Ljava/lang/String;
    .restart local v4    # "showId":Ljava/lang/String;
    :cond_9
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_a

    const-string v12, "movies"

    goto :goto_3

    :cond_a
    const/4 v12, 0x0

    goto :goto_3

    .line 325
    .restart local v12    # "vertical":Ljava/lang/String;
    :cond_b
    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_d

    .line 326
    const-string v0, "android.intent.action.MAIN"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v0, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_c

    .line 327
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHomeWithLauncherFlag(Landroid/content/Intent;)V

    .line 328
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-nez v0, :cond_5

    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v0

    const-string v6, "onboard_tutorial_shown"

    const/4 v13, 0x0

    invoke-interface {v0, v6, v13}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v0

    if-nez v0, :cond_5

    .line 330
    invoke-static {p0}, Lcom/google/android/videos/onboard/OnboardHostActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_4

    .line 333
    :cond_c
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto :goto_4

    .line 335
    :cond_d
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_f

    .line 336
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_e

    .line 337
    invoke-static {p0}, Lcom/google/android/videos/pano/activity/PanoHomeActivity;->createIntent(Landroid/content/Context;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 339
    :cond_e
    const/4 v0, 0x0

    invoke-direct {p0, v0, v12}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto/16 :goto_4

    .line 342
    :cond_f
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 343
    if-eqz v10, :cond_11

    .line 344
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_10

    .line 345
    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/activity/LauncherActivity;->startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 347
    :cond_10
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/videos/activity/LauncherActivity;->startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 349
    :cond_11
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_13

    .line 350
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_12

    .line 351
    const/4 v0, 0x0

    invoke-direct {p0, v1, v2, v5, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startMovieDetails(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_4

    .line 353
    :cond_12
    const/4 v6, 0x0

    move-object v0, p0

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/activity/LauncherActivity;->startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_4

    .line 355
    :cond_13
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_15

    .line 356
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_14

    .line 357
    invoke-direct {p0, v1, v4, v3}, Lcom/google/android/videos/activity/LauncherActivity;->startSeasonDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 359
    :cond_14
    invoke-direct {p0, v1, v4}, Lcom/google/android/videos/activity/LauncherActivity;->startShowDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_4

    .line 362
    :cond_15
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto/16 :goto_4
.end method

.method private handleMoviesUriIntent(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;)V
    .locals 9
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uri"    # Landroid/net/Uri;
    .param p3, "vertical"    # Ljava/lang/String;

    .prologue
    const/4 v8, 0x0

    .line 216
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->getAccount(Landroid/content/Intent;)Ljava/lang/String;

    move-result-object v1

    .line 217
    .local v1, "account":Ljava/lang/String;
    const-string v0, "force_close_drawer"

    invoke-virtual {p1, v0, v8}, Landroid/content/Intent;->getBooleanExtra(Ljava/lang/String;Z)Z

    move-result v7

    .line 218
    .local v7, "forceCloseDrawer":Z
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->hasPremiumError()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 219
    invoke-direct {p0, v1, p3, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    .line 266
    :goto_0
    return-void

    .line 222
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->accountExists(Ljava/lang/String;)Z

    move-result v0

    if-nez v0, :cond_1

    .line 223
    const/4 v0, 0x0

    invoke-direct {p0, v0, p3, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 226
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 227
    const-string v0, "pining_error_dialog"

    invoke-virtual {p2, v0, v8}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v6

    .line 228
    .local v6, "pinningErrorDialog":Z
    const-string v0, "dl"

    invoke-virtual {p2, v0, v8}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v5

    .line 229
    .local v5, "download":Z
    const-string v0, "mdl"

    invoke-virtual {p2, v0, v8}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 230
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/LauncherActivity;->startManageDownloads(Ljava/lang/String;)V

    goto :goto_0

    .line 231
    :cond_2
    const-string v0, "movies"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_5

    .line 232
    const-string v0, "v"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 233
    .local v2, "videoId":Ljava/lang/String;
    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_4

    .line 234
    const-string v0, "play"

    invoke-virtual {p2, v0, v8}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 235
    invoke-direct {p0, v1, v2}, Lcom/google/android/videos/activity/LauncherActivity;->startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V

    goto :goto_0

    .line 237
    :cond_3
    invoke-direct {p0, v1, v2, v5, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startMovieDetails(Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto :goto_0

    .line 240
    :cond_4
    const-string v0, "movies"

    invoke-direct {p0, v1, v0, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    goto :goto_0

    .line 242
    .end local v2    # "videoId":Ljava/lang/String;
    :cond_5
    const-string v0, "shows"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_a

    .line 243
    const-string v0, "sh"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 244
    .local v4, "showId":Ljava/lang/String;
    const-string v0, "se"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 245
    .local v3, "seasonId":Ljava/lang/String;
    const-string v0, "v"

    invoke-virtual {p2, v0}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 246
    .restart local v2    # "videoId":Ljava/lang/String;
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    invoke-static {v2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_7

    .line 247
    const-string v0, "play"

    invoke-virtual {p2, v0, v8}, Landroid/net/Uri;->getBooleanQueryParameter(Ljava/lang/String;Z)Z

    move-result v0

    if-eqz v0, :cond_6

    .line 248
    invoke-direct {p0, v1, v2, v3, v4}, Lcom/google/android/videos/activity/LauncherActivity;->startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    :cond_6
    move-object v0, p0

    .line 250
    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/activity/LauncherActivity;->startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V

    goto/16 :goto_0

    .line 252
    :cond_7
    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_9

    .line 253
    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-nez v0, :cond_8

    .line 254
    invoke-direct {p0, v1, v4, v3}, Lcom/google/android/videos/activity/LauncherActivity;->startSeasonDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 256
    :cond_8
    invoke-direct {p0, v1, v4}, Lcom/google/android/videos/activity/LauncherActivity;->startShowDetails(Ljava/lang/String;Ljava/lang/String;)V

    goto/16 :goto_0

    .line 259
    :cond_9
    const-string v0, "shows"

    invoke-direct {p0, v1, v0, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 261
    .end local v2    # "videoId":Ljava/lang/String;
    .end local v3    # "seasonId":Ljava/lang/String;
    .end local v4    # "showId":Ljava/lang/String;
    :cond_a
    const-string v0, "watchnow"

    invoke-virtual {p3, v0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_b

    .line 262
    const-string v0, "watchnow"

    invoke-direct {p0, v1, v0, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startVertical(Ljava/lang/String;Ljava/lang/String;Z)V

    goto/16 :goto_0

    .line 264
    :cond_b
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto/16 :goto_0
.end method

.method private handlePlayUriIntent(Landroid/content/Intent;)V
    .locals 7
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v6, 0x1

    const/4 v5, 0x0

    .line 201
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v1

    .line 202
    .local v1, "uri":Landroid/net/Uri;
    invoke-virtual {v1}, Landroid/net/Uri;->getPathSegments()Ljava/util/List;

    move-result-object v2

    .line 203
    .local v2, "uriPathSegments":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    if-eqz v2, :cond_0

    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 204
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    .line 213
    :goto_0
    return-void

    .line 205
    :cond_1
    const-string v3, "movies"

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_2

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v3

    if-le v3, v6, :cond_2

    .line 206
    invoke-interface {v2, v6}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    .line 207
    .local v0, "segment":Ljava/lang/String;
    invoke-direct {p0, p1, v1, v0}, Lcom/google/android/videos/activity/LauncherActivity;->handleMoviesUriIntent(Landroid/content/Intent;Landroid/net/Uri;Ljava/lang/String;)V

    goto :goto_0

    .line 208
    .end local v0    # "segment":Ljava/lang/String;
    :cond_2
    const-string v3, "trailers"

    invoke-interface {v2, v5}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    invoke-virtual {v3, v4}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v3

    if-eqz v3, :cond_3

    .line 209
    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/activity/LauncherActivity;->handleTrailersUriIntent(Landroid/content/Intent;Landroid/net/Uri;)V

    goto :goto_0

    .line 211
    :cond_3
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->startHome(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private handleSearchIntent(Landroid/content/Intent;Lcom/google/android/videos/accounts/SignInManager;)V
    .locals 6
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "signInManager"    # Lcom/google/android/videos/accounts/SignInManager;

    .prologue
    const/16 v5, 0xe

    .line 368
    invoke-virtual {p1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v2

    .line 369
    .local v2, "uri":Landroid/net/Uri;
    const-string v3, "intent_extra_data_key"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 370
    .local v0, "accountName":Ljava/lang/String;
    if-nez v0, :cond_0

    .line 373
    invoke-virtual {p2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 376
    :cond_0
    if-nez v2, :cond_3

    .line 377
    const-string v3, "query"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 378
    .local v1, "query":Ljava/lang/String;
    if-nez v1, :cond_2

    .line 379
    iget-object v3, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v3

    if-eqz v3, :cond_1

    .line 380
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startPanoSearch(Landroid/app/Activity;)V

    .line 393
    .end local v1    # "query":Ljava/lang/String;
    :goto_0
    return-void

    .line 382
    .restart local v1    # "query":Ljava/lang/String;
    :cond_1
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected search intent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 385
    :cond_2
    const-string v3, "movies"

    invoke-static {p0, v1, v3, v0, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startSearch(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;I)V

    goto :goto_0

    .line 388
    .end local v1    # "query":Ljava/lang/String;
    :cond_3
    invoke-virtual {v2}, Landroid/net/Uri;->getPath()Ljava/lang/String;

    move-result-object v3

    const-string v4, "/store/"

    invoke-virtual {v3, v4}, Ljava/lang/String;->startsWith(Ljava/lang/String;)Z

    move-result v3

    if-eqz v3, :cond_4

    .line 389
    invoke-static {p0, v2, v0, v5}, Lcom/google/android/videos/utils/PlayStoreUtil;->startForUri(Landroid/content/Context;Landroid/net/Uri;Ljava/lang/String;I)V

    goto :goto_0

    .line 391
    :cond_4
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "Unexpected intent "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0
.end method

.method private handleTrailersUriIntent(Landroid/content/Intent;Landroid/net/Uri;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;
    .param p2, "uri"    # Landroid/net/Uri;

    .prologue
    .line 269
    iget-object v3, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v1

    .line 270
    .local v1, "signInManager":Lcom/google/android/videos/accounts/SignInManager;
    const-string v3, "v"

    invoke-virtual {p2, v3}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 271
    .local v2, "videoId":Ljava/lang/String;
    const-string v3, "authAccount"

    invoke-virtual {p1, v3}, Landroid/content/Intent;->getStringExtra(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 272
    .local v0, "account":Ljava/lang/String;
    invoke-virtual {v1}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-eqz v3, :cond_0

    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v3

    if-nez v3, :cond_0

    .line 275
    invoke-virtual {v1, v0}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 277
    :cond_0
    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/activity/LauncherActivity;->startTrailerPlayback(Ljava/lang/String;Ljava/lang/String;)V

    .line 278
    return-void
.end method

.method private startEpisodeDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;
    .param p5, "startDownload"    # Z
    .param p6, "pinningErrorDialog"    # Z

    .prologue
    const/4 v1, 0x2

    const/4 v8, 0x1

    const/4 v2, 0x0

    .line 479
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 480
    new-array v0, v1, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p0, p4, p3, p2}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v8

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 489
    :goto_0
    return-void

    .line 484
    :cond_0
    new-array v7, v1, [Landroid/content/Intent;

    const-string v0, "shows"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v2

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p2

    move v5, p5

    move v6, p6

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v8

    invoke-virtual {p0, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startEpisodePlayback(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x3

    const/4 v9, 0x2

    const/4 v8, 0x1

    const/4 v5, 0x0

    .line 449
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 450
    new-array v6, v1, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-static {p0, p4, p3, p2}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v9

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 460
    :goto_0
    return-void

    .line 455
    :cond_0
    new-array v7, v1, [Landroid/content/Intent;

    const-string v0, "shows"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p4

    move-object v3, p3

    move-object v4, p2

    move v6, v5

    invoke-direct/range {v0 .. v6}, Lcom/google/android/videos/activity/LauncherActivity;->createEpisodeIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v8

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v3, p3

    move-object v4, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v7, v9

    invoke-virtual {p0, v7}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startHome(Landroid/content/Intent;)V
    .locals 1
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 542
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 543
    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 547
    :goto_0
    return-void

    .line 545
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->createHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startHomeWithLauncherFlag(Landroid/content/Intent;)V
    .locals 3
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    const/4 v2, 0x1

    .line 534
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 535
    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    const-string v1, "started_from_launcher"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 539
    :goto_0
    return-void

    .line 537
    :cond_0
    invoke-direct {p0, p1}, Lcom/google/android/videos/activity/LauncherActivity;->createHomeIntent(Landroid/content/Intent;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "started_from_launcher"

    invoke-virtual {v0, v1, v2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startManageDownloads(Ljava/lang/String;)V
    .locals 3
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 516
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 517
    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 523
    :goto_0
    return-void

    .line 519
    :cond_0
    const/4 v0, 0x2

    new-array v0, v0, [Landroid/content/Intent;

    const/4 v1, 0x0

    const-string v2, "watchnow"

    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    const/4 v1, 0x1

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createManageDownloadsIntent()Landroid/content/Intent;

    move-result-object v2

    aput-object v2, v0, v1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startMovieDetails(Ljava/lang/String;Ljava/lang/String;ZZ)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "startDownload"    # Z
    .param p4, "pinningErrorDialog"    # Z

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 464
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 465
    new-array v0, v1, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p0, p2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 474
    :goto_0
    return-void

    .line 470
    :cond_0
    new-array v0, v1, [Landroid/content/Intent;

    const-string v1, "movies"

    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/activity/LauncherActivity;->createMovieIntent(Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startMoviePlayback(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x3

    const/4 v7, 0x2

    const/4 v1, 0x1

    const/4 v3, 0x0

    const/4 v5, 0x0

    .line 433
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 434
    new-array v6, v2, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-static {p0, p2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 445
    :goto_0
    return-void

    .line 440
    :cond_0
    new-array v6, v2, [Landroid/content/Intent;

    const-string v0, "movies"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-direct {p0, p1, p2, v5, v5}, Lcom/google/android/videos/activity/LauncherActivity;->createMovieIntent(Ljava/lang/String;Ljava/lang/String;ZZ)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startSeasonDetails(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 492
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 493
    new-array v0, v1, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p0, p2, p3}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 501
    :goto_0
    return-void

    .line 497
    :cond_0
    new-array v0, v1, [Landroid/content/Intent;

    const-string v1, "shows"

    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/activity/LauncherActivity;->createSeasonIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startShowDetails(Ljava/lang/String;Ljava/lang/String;)V
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x2

    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 504
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 505
    new-array v0, v1, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-static {p0, p2}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 513
    :goto_0
    return-void

    .line 509
    :cond_0
    new-array v0, v1, [Landroid/content/Intent;

    const-string v1, "shows"

    invoke-direct {p0, p1, v1}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v2

    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/activity/LauncherActivity;->createShowIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    aput-object v1, v0, v3

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startTrailerPlayback(Ljava/lang/String;Ljava/lang/String;)V
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x2

    const/4 v1, 0x0

    const/4 v3, 0x0

    const/4 v5, 0x1

    .line 419
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 420
    const/4 v0, 0x3

    new-array v6, v0, [Landroid/content/Intent;

    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v1

    invoke-static {p0, p2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v5

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/activity/PanoWatchActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v7

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    .line 430
    :goto_0
    return-void

    .line 426
    :cond_0
    new-array v6, v7, [Landroid/content/Intent;

    const-string v0, "watchnow"

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v1

    move-object v0, p0

    move-object v1, p1

    move-object v2, p2

    move-object v4, v3

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/activity/LauncherActivity;->createWatchIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    aput-object v0, v6, v5

    invoke-virtual {p0, v6}, Lcom/google/android/videos/activity/LauncherActivity;->startActivities([Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private startVertical(Ljava/lang/String;Ljava/lang/String;Z)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;
    .param p3, "forceCloseDrawer"    # Z

    .prologue
    .line 526
    iget-object v0, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getConfig()Lcom/google/android/videos/Config;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/Config;->panoEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 527
    invoke-direct {p0}, Lcom/google/android/videos/activity/LauncherActivity;->createPanoHomeIntent()Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    .line 531
    :goto_0
    return-void

    .line 529
    :cond_0
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/activity/LauncherActivity;->createRootHomeIntent(Ljava/lang/String;Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/activity/LauncherActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method


# virtual methods
.method protected onCreate(Landroid/os/Bundle;)V
    .locals 7
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 175
    invoke-super {p0, p1}, Landroid/app/Activity;->onCreate(Landroid/os/Bundle;)V

    .line 176
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v5

    iput-object v5, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 177
    iget-object v5, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getEventLogger()Lcom/google/android/videos/logging/EventLogger;

    move-result-object v0

    .line 179
    .local v0, "eventLogger":Lcom/google/android/videos/logging/EventLogger;
    invoke-virtual {p0}, Lcom/google/android/videos/activity/LauncherActivity;->getIntent()Landroid/content/Intent;

    move-result-object v1

    .line 180
    .local v1, "intent":Landroid/content/Intent;
    const-string v5, "android.intent.action.SEARCH"

    invoke-virtual {v1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_0

    .line 181
    iget-object v5, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v5

    invoke-direct {p0, v1, v5}, Lcom/google/android/videos/activity/LauncherActivity;->handleSearchIntent(Landroid/content/Intent;Lcom/google/android/videos/accounts/SignInManager;)V

    .line 182
    invoke-virtual {p0}, Lcom/google/android/videos/activity/LauncherActivity;->finish()V

    .line 198
    :goto_0
    return-void

    .line 186
    :cond_0
    iget-object v5, p0, Lcom/google/android/videos/activity/LauncherActivity;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v5}, Lcom/google/android/videos/VideosGlobals;->updatePremiumStatus()I

    move-result v2

    .line 188
    .local v2, "premiumStatus":I
    invoke-virtual {v1}, Landroid/content/Intent;->getData()Landroid/net/Uri;

    move-result-object v4

    .line 189
    .local v4, "uri":Landroid/net/Uri;
    if-eqz v4, :cond_1

    const-string v5, "play.google.com"

    invoke-virtual {v4}, Landroid/net/Uri;->getHost()Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v5

    if-eqz v5, :cond_1

    .line 190
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/LauncherActivity;->handlePlayUriIntent(Landroid/content/Intent;)V

    .line 195
    :goto_1
    if-eqz v4, :cond_2

    const-string v5, "ref"

    invoke-virtual {v4, v5}, Landroid/net/Uri;->getQueryParameter(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    .line 196
    .local v3, "referer":Ljava/lang/String;
    :goto_2
    invoke-interface {v0, p0, v2, v3}, Lcom/google/android/videos/logging/EventLogger;->onVideosStart(Landroid/app/Activity;ILjava/lang/String;)V

    .line 197
    invoke-virtual {p0}, Lcom/google/android/videos/activity/LauncherActivity;->finish()V

    goto :goto_0

    .line 192
    .end local v3    # "referer":Ljava/lang/String;
    :cond_1
    invoke-direct {p0, v1}, Lcom/google/android/videos/activity/LauncherActivity;->handleLegacyIntent(Landroid/content/Intent;)V

    goto :goto_1

    .line 195
    :cond_2
    const/4 v3, 0x0

    goto :goto_2
.end method
