.class Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;
.super Ljava/lang/Object;
.source "DragPromoOverlay.java"

# interfaces
.implements Landroid/view/animation/Interpolator;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/overlay/DragPromoOverlay;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "ScalingInterpolator"
.end annotation


# instance fields
.field private final offset:F

.field private final scaleFactor:F

.field private final wrapped:Landroid/view/animation/Interpolator;


# direct methods
.method public constructor <init>(Landroid/view/animation/Interpolator;FF)V
    .locals 0
    .param p1, "wrapped"    # Landroid/view/animation/Interpolator;
    .param p2, "scaleFactor"    # F
    .param p3, "offset"    # F

    .prologue
    .line 220
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 221
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->wrapped:Landroid/view/animation/Interpolator;

    .line 222
    iput p2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->scaleFactor:F

    .line 223
    iput p3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->offset:F

    .line 224
    return-void
.end method


# virtual methods
.method public getInterpolation(F)F
    .locals 4
    .param p1, "input"    # F

    .prologue
    .line 228
    const/4 v1, 0x0

    iget v2, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->scaleFactor:F

    mul-float/2addr v2, p1

    iget v3, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->offset:F

    add-float/2addr v2, v3

    const/high16 v3, 0x3f800000    # 1.0f

    invoke-static {v2, v3}, Ljava/lang/Math;->min(FF)F

    move-result v2

    invoke-static {v1, v2}, Ljava/lang/Math;->max(FF)F

    move-result v0

    .line 229
    .local v0, "scaledInput":F
    iget-object v1, p0, Lcom/google/android/videos/player/overlay/DragPromoOverlay$ScalingInterpolator;->wrapped:Landroid/view/animation/Interpolator;

    invoke-interface {v1, v0}, Landroid/view/animation/Interpolator;->getInterpolation(F)F

    move-result v1

    return v1
.end method
