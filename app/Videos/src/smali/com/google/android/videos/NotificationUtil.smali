.class public abstract Lcom/google/android/videos/NotificationUtil;
.super Ljava/lang/Object;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/NotificationUtil$V14;,
        Lcom/google/android/videos/NotificationUtil$V16;,
        Lcom/google/android/videos/NotificationUtil$V21;
    }
.end annotation


# instance fields
.field protected final context:Landroid/content/Context;


# direct methods
.method protected constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 38
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 39
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/content/Context;

    iput-object v0, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    .line 40
    return-void
.end method

.method public static create(Landroid/content/Context;)Lcom/google/android/videos/NotificationUtil;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;

    .prologue
    .line 32
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    new-instance v0, Lcom/google/android/videos/NotificationUtil$V21;

    invoke-direct {v0, p0}, Lcom/google/android/videos/NotificationUtil$V21;-><init>(Landroid/content/Context;)V

    :goto_0
    return-object v0

    :cond_0
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x10

    if-lt v0, v1, :cond_1

    new-instance v0, Lcom/google/android/videos/NotificationUtil$V16;

    invoke-direct {v0, p0}, Lcom/google/android/videos/NotificationUtil$V16;-><init>(Landroid/content/Context;)V

    goto :goto_0

    :cond_1
    new-instance v0, Lcom/google/android/videos/NotificationUtil$V14;

    invoke-direct {v0, p0}, Lcom/google/android/videos/NotificationUtil$V14;-><init>(Landroid/content/Context;)V

    goto :goto_0
.end method

.method public static scaleBitmapForNotification(Landroid/content/Context;Landroid/graphics/Bitmap;)Landroid/graphics/Bitmap;
    .locals 8
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "bitmap"    # Landroid/graphics/Bitmap;

    .prologue
    .line 90
    if-nez p1, :cond_0

    .line 91
    const/4 v6, 0x0

    .line 101
    :goto_0
    return-object v6

    .line 94
    :cond_0
    invoke-virtual {p0}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v4

    .line 95
    .local v4, "res":Landroid/content/res/Resources;
    const v6, 0x1050006

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v1

    .line 96
    .local v1, "iconHeight":F
    const v6, 0x1050005

    invoke-virtual {v4, v6}, Landroid/content/res/Resources;->getDimension(I)F

    move-result v2

    .line 97
    .local v2, "iconWidth":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    div-float v6, v2, v6

    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v7

    int-to-float v7, v7

    div-float v7, v1, v7

    invoke-static {v6, v7}, Ljava/lang/Math;->min(FF)F

    move-result v3

    .line 99
    .local v3, "ratio":F
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getWidth()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    float-to-int v5, v6

    .line 100
    .local v5, "width":I
    invoke-virtual {p1}, Landroid/graphics/Bitmap;->getHeight()I

    move-result v6

    int-to-float v6, v6

    mul-float/2addr v6, v3

    float-to-int v0, v6

    .line 101
    .local v0, "height":I
    const/4 v6, 0x0

    invoke-static {p1, v5, v0, v6}, Landroid/graphics/Bitmap;->createScaledBitmap(Landroid/graphics/Bitmap;IIZ)Landroid/graphics/Bitmap;

    move-result-object v6

    goto :goto_0
.end method


# virtual methods
.method public abstract createDownloadCompletedNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadCompleted;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadErrorNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadError;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadPendingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadPending;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createDownloadsOngoingNotification(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createNewEpisodeNotification(Ljava/lang/String;Ljava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method public abstract createNewEpisodesNotification(ILjava/lang/String;Landroid/graphics/Bitmap;Landroid/app/PendingIntent;Landroid/app/PendingIntent;)Landroid/app/Notification;
.end method

.method protected getDownloadProgressText(Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;)Ljava/lang/String;
    .locals 8
    .param p1, "ongoing"    # Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;

    .prologue
    .line 66
    iget-boolean v3, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotalIsIndeterminate:Z

    if-eqz v3, :cond_0

    .line 68
    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    const v4, 0x7f0b01a4

    invoke-virtual {v3, v4}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v3

    .line 74
    :goto_0
    return-object v3

    .line 71
    :cond_0
    iget-wide v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransferred:J

    const-wide/16 v6, 0x64

    mul-long/2addr v4, v6

    iget-wide v6, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    div-long/2addr v4, v6

    long-to-int v1, v4

    .line 72
    .local v1, "progress":I
    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    iget-wide v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTransferred:J

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v0

    .line 73
    .local v0, "current":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    iget-wide v4, p1, Lcom/google/android/videos/pinning/DownloadNotificationManager$DownloadsOngoing;->bytesTotal:J

    invoke-static {v3, v4, v5}, Landroid/text/format/Formatter;->formatShortFileSize(Landroid/content/Context;J)Ljava/lang/String;

    move-result-object v2

    .line 74
    .local v2, "total":Ljava/lang/String;
    iget-object v3, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    const v4, 0x7f0b01a3

    const/4 v5, 0x3

    new-array v5, v5, [Ljava/lang/Object;

    const/4 v6, 0x0

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v7

    aput-object v7, v5, v6

    const/4 v6, 0x1

    aput-object v0, v5, v6

    const/4 v6, 0x2

    aput-object v2, v5, v6

    invoke-virtual {v3, v4, v5}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v3

    goto :goto_0
.end method

.method protected getTitleForDownloadInProgress(Ljava/lang/String;I)Ljava/lang/String;
    .locals 4
    .param p1, "title"    # Ljava/lang/String;
    .param p2, "ongoingCount"    # I

    .prologue
    const/4 v2, 0x1

    .line 79
    if-eq p2, v2, :cond_0

    .line 80
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    const v1, 0x7f0b01a1

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 82
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/NotificationUtil;->context:Landroid/content/Context;

    const v1, 0x7f0b01a2

    new-array v2, v2, [Ljava/lang/Object;

    const/4 v3, 0x0

    aput-object p1, v2, v3

    invoke-virtual {v0, v1, v2}, Landroid/content/Context;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    goto :goto_0
.end method
