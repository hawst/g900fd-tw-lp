.class Lcom/google/android/videos/player/CaptionPreferences$V19;
.super Lcom/google/android/videos/player/CaptionPreferences$V8;
.source "CaptionPreferences.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/CaptionPreferences;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V19"
.end annotation


# instance fields
.field private final captioningManager:Landroid/view/accessibility/CaptioningManager;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;

    .prologue
    .line 95
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/CaptionPreferences$V8;-><init>(Landroid/content/Context;Landroid/content/SharedPreferences;)V

    .line 96
    const-string v0, "captioning"

    invoke-virtual {p1, v0}, Landroid/content/Context;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/accessibility/CaptioningManager;

    iput-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V19;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    .line 97
    return-void
.end method


# virtual methods
.method public getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;
    .locals 1

    .prologue
    .line 116
    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 117
    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->getCaptionStyle()Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v0

    .line 119
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V19;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getUserStyle()Landroid/view/accessibility/CaptioningManager$CaptionStyle;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/exoplayer/text/CaptionStyleCompat;->createFromCaptionStyle(Landroid/view/accessibility/CaptioningManager$CaptionStyle;)Lcom/google/android/exoplayer/text/CaptionStyleCompat;

    move-result-object v0

    goto :goto_0
.end method

.method public getFontScale()F
    .locals 1

    .prologue
    .line 111
    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->getFontScale()F

    move-result v0

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V19;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getFontScale()F

    move-result v0

    goto :goto_0
.end method

.method public getLocale()Ljava/util/Locale;
    .locals 1

    .prologue
    .line 106
    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->getLocale()Ljava/util/Locale;

    move-result-object v0

    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V19;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->getLocale()Ljava/util/Locale;

    move-result-object v0

    goto :goto_0
.end method

.method public isEnabled()Z
    .locals 1

    .prologue
    .line 101
    iget-object v0, p0, Lcom/google/android/videos/player/CaptionPreferences$V19;->captioningManager:Landroid/view/accessibility/CaptioningManager;

    invoke-virtual {v0}, Landroid/view/accessibility/CaptioningManager;->isEnabled()Z

    move-result v0

    if-nez v0, :cond_0

    invoke-super {p0}, Lcom/google/android/videos/player/CaptionPreferences$V8;->isEnabled()Z

    move-result v0

    if-eqz v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method
