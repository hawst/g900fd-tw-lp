.class Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;
.super Ljava/lang/Object;
.source "MyLibraryFragment.java"

# interfaces
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/activity/MyLibraryFragment;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "MyLibraryHelper"
.end annotation


# instance fields
.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field final synthetic this$0:Lcom/google/android/videos/activity/MyLibraryFragment;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/activity/MyLibraryFragment;Lcom/google/android/videos/ui/SyncHelper;)V
    .locals 0
    .param p2, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;

    .prologue
    .line 330
    iput-object p1, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 331
    iput-object p2, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 332
    return-void
.end method


# virtual methods
.method public onDestroy()V
    .locals 0

    .prologue
    .line 364
    return-void
.end method

.method public onStart()V
    .locals 1

    .prologue
    .line 336
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 337
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # invokes: Lcom/google/android/videos/activity/MyLibraryFragment;->setupPager()V
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$400(Lcom/google/android/videos/activity/MyLibraryFragment;)V

    .line 338
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # invokes: Lcom/google/android/videos/activity/MyLibraryFragment;->tryRestorePager()Z
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$500(Lcom/google/android/videos/activity/MyLibraryFragment;)Z

    move-result v0

    if-nez v0, :cond_0

    .line 339
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # invokes: Lcom/google/android/videos/activity/MyLibraryFragment;->selectInitialVertical()V
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$600(Lcom/google/android/videos/activity/MyLibraryFragment;)V

    .line 341
    :cond_0
    return-void
.end method

.method public onStop()V
    .locals 4

    .prologue
    .line 345
    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # invokes: Lcom/google/android/videos/activity/MyLibraryFragment;->savePager()V
    invoke-static {v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$700(Lcom/google/android/videos/activity/MyLibraryFragment;)V

    .line 346
    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v3, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 348
    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    .line 349
    .local v0, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    if-nez v0, :cond_1

    .line 358
    :cond_0
    return-void

    .line 352
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 353
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 354
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v3, v1, Lcom/google/android/videos/activity/HomeFragment;

    if-eqz v3, :cond_2

    .line 355
    check-cast v1, Lcom/google/android/videos/activity/HomeFragment;

    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeFragment;->ensureHelperStopped()V

    .line 352
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method

.method public onSyncStateChanged(I)V
    .locals 1
    .param p1, "state"    # I

    .prologue
    .line 368
    iget-object v0, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    # invokes: Lcom/google/android/videos/activity/MyLibraryFragment;->setupPager()V
    invoke-static {v0}, Lcom/google/android/videos/activity/MyLibraryFragment;->access$400(Lcom/google/android/videos/activity/MyLibraryFragment;)V

    .line 369
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 4
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 373
    iget-object v3, p0, Lcom/google/android/videos/activity/MyLibraryFragment$MyLibraryHelper;->this$0:Lcom/google/android/videos/activity/MyLibraryFragment;

    invoke-virtual {v3}, Lcom/google/android/videos/activity/MyLibraryFragment;->getChildFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v3

    invoke-virtual {v3}, Landroid/support/v4/app/FragmentManager;->getFragments()Ljava/util/List;

    move-result-object v0

    .line 374
    .local v0, "childFragments":Ljava/util/List;, "Ljava/util/List<Landroid/support/v4/app/Fragment;>;"
    if-nez v0, :cond_1

    .line 383
    :cond_0
    return-void

    .line 377
    :cond_1
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-interface {v0}, Ljava/util/List;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 378
    invoke-interface {v0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/support/v4/app/Fragment;

    .line 379
    .local v1, "fragment":Landroid/support/v4/app/Fragment;
    instance-of v3, v1, Lcom/google/android/videos/activity/HomeFragment;

    if-eqz v3, :cond_2

    .line 380
    check-cast v1, Lcom/google/android/videos/activity/HomeFragment;

    .end local v1    # "fragment":Landroid/support/v4/app/Fragment;
    invoke-virtual {v1, p1}, Lcom/google/android/videos/activity/HomeFragment;->onTransitioningChanged(Z)V

    .line 377
    :cond_2
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
