.class Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;
.super Ljava/lang/Object;
.source "SubtitleTrack.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/SubtitleTrack;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "Serializer"
.end annotation


# static fields
.field private static final serialVersionUID:J = 0x1L


# instance fields
.field private captionProto:[B

.field private version:I

.field private videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 163
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/subtitles/SubtitleTrack$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/subtitles/SubtitleTrack$1;

    .prologue
    .line 163
    invoke-direct {p0}, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;-><init>()V

    return-void
.end method

.method private readResolve()Ljava/lang/Object;
    .locals 4
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 185
    :try_start_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->captionProto:[B

    invoke-static {v2}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->parseFrom([B)Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    move-result-object v0

    .line 186
    .local v0, "caption":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->videoId:Ljava/lang/String;

    iget v3, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->version:I

    # invokes: Lcom/google/android/videos/subtitles/SubtitleTrack;->create(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;
    invoke-static {v2, v0, v3}, Lcom/google/android/videos/subtitles/SubtitleTrack;->access$100(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;I)Lcom/google/android/videos/subtitles/SubtitleTrack;
    :try_end_0
    .catch Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v2

    return-object v2

    .line 187
    .end local v0    # "caption":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    :catch_0
    move-exception v1

    .line 188
    .local v1, "e":Lcom/google/protobuf/nano/InvalidProtocolBufferNanoException;
    new-instance v2, Ljava/io/StreamCorruptedException;

    invoke-direct {v2}, Ljava/io/StreamCorruptedException;-><init>()V

    throw v2
.end method


# virtual methods
.method public setSubtitleTrack(Lcom/google/android/videos/subtitles/SubtitleTrack;)Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;
    .locals 4
    .param p1, "track"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/ObjectStreamException;
        }
    .end annotation

    .prologue
    .line 171
    iget-object v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->videoId:Ljava/lang/String;

    iput-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->videoId:Ljava/lang/String;

    .line 172
    new-instance v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;

    invoke-direct {v0}, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;-><init>()V

    .line 173
    .local v0, "caption":Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;
    iget-object v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->lang:Ljava/lang/String;

    .line 174
    iget-object v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->trackName:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->name:Ljava/lang/String;

    .line 175
    iget v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->format:I

    int-to-long v2, v1

    iput-wide v2, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->format:J

    .line 176
    iget-object v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->url:Ljava/lang/String;

    iput-object v1, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->url:Ljava/lang/String;

    .line 177
    iget-boolean v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    iput-boolean v1, v0, Lcom/google/wireless/android/video/magma/proto/MediaPresentationDescription$Caption;->forced:Z

    .line 178
    invoke-static {v0}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->captionProto:[B

    .line 179
    iget v1, p1, Lcom/google/android/videos/subtitles/SubtitleTrack;->fileVersion:I

    iput v1, p0, Lcom/google/android/videos/subtitles/SubtitleTrack$Serializer;->version:I

    .line 180
    return-object p0
.end method
