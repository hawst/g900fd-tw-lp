.class public Lcom/google/android/videos/subtitles/TrackSelectionUtil;
.super Ljava/lang/Object;
.source "TrackSelectionUtil.java"


# direct methods
.method public static findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 1
    .param p1, "languageCode"    # Ljava/lang/String;
    .param p2, "forced"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Ljava/lang/String;",
            "Z)",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;"
        }
    .end annotation

    .prologue
    .line 32
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 33
    const/4 v0, 0x0

    .line 37
    :goto_0
    return-object v0

    :cond_0
    new-instance v0, Ljava/util/Locale;

    invoke-direct {v0, p1}, Ljava/util/Locale;-><init>(Ljava/lang/String;)V

    invoke-static {p0, v0, p2}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/util/Locale;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    goto :goto_0
.end method

.method public static findTrack(Ljava/util/List;Ljava/util/Locale;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 8
    .param p1, "locale"    # Ljava/util/Locale;
    .param p2, "forced"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Ljava/util/Locale;",
            "Z)",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;"
        }
    .end annotation

    .prologue
    .line 45
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v7

    if-nez v7, :cond_0

    if-nez p1, :cond_2

    .line 46
    :cond_0
    const/4 v1, 0x0

    .line 63
    :cond_1
    return-object v1

    .line 48
    :cond_2
    const/4 v1, 0x0

    .line 49
    .local v1, "bestTrack":Lcom/google/android/videos/subtitles/SubtitleTrack;
    const/4 v0, 0x0

    .line 50
    .local v0, "bestScore":I
    invoke-virtual {p1}, Ljava/util/Locale;->toString()Ljava/lang/String;

    move-result-object v4

    .line 51
    .local v4, "localeString":Ljava/lang/String;
    const/4 v3, 0x0

    .local v3, "i":I
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v2

    .local v2, "count":I
    :goto_0
    if-ge v3, v2, :cond_1

    .line 52
    invoke-interface {p0, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v6

    check-cast v6, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 53
    .local v6, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-boolean v7, v6, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-eq v7, p2, :cond_4

    .line 51
    :cond_3
    :goto_1
    add-int/lit8 v3, v3, 0x1

    goto :goto_0

    .line 57
    :cond_4
    iget-object v7, v6, Lcom/google/android/videos/subtitles/SubtitleTrack;->languageCode:Ljava/lang/String;

    invoke-static {v7, v4}, Lcom/google/android/videos/utils/Util;->getLanguageMatchScore(Ljava/lang/String;Ljava/lang/String;)I

    move-result v5

    .line 58
    .local v5, "score":I
    if-le v5, v0, :cond_3

    .line 59
    move v0, v5

    .line 60
    move-object v1, v6

    goto :goto_1
.end method

.method public static getSelectableTracks(Ljava/util/List;Ljava/lang/String;)Ljava/util/ArrayList;
    .locals 6
    .param p1, "disableOptionLabel"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/ArrayList",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;"
        }
    .end annotation

    .prologue
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v3, 0x0

    .line 139
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 141
    .local v1, "subtitleTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    if-eqz p1, :cond_0

    .line 143
    invoke-virtual {v1, v3}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_0
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    invoke-interface {p0}, Ljava/util/List;->size()I

    move-result v4

    if-ge v0, v4, :cond_2

    .line 147
    invoke-interface {p0, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/subtitles/SubtitleTrack;

    .line 148
    .local v2, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    iget-boolean v4, v2, Lcom/google/android/videos/subtitles/SubtitleTrack;->isForced:Z

    if-nez v4, :cond_1

    .line 149
    invoke-virtual {v1, v2}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 146
    :cond_1
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 153
    .end local v2    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_2
    invoke-virtual {v1}, Ljava/util/ArrayList;->size()I

    move-result v4

    const/4 v5, 0x2

    if-ge v4, v5, :cond_4

    move-object v1, v3

    .line 159
    .end local v1    # "subtitleTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    :cond_3
    :goto_1
    return-object v1

    .line 155
    .restart local v1    # "subtitleTracks":Ljava/util/ArrayList;, "Ljava/util/ArrayList<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    :cond_4
    if-eqz p1, :cond_3

    .line 156
    const/4 v3, 0x0

    invoke-static {p1}, Lcom/google/android/videos/subtitles/SubtitleTrack;->createDisableTrack(Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v4

    invoke-virtual {v1, v3, v4}, Ljava/util/ArrayList;->set(ILjava/lang/Object;)Ljava/lang/Object;

    goto :goto_1
.end method

.method public static persistSelectSubtitleWhenNoAudioInDeviceLanguage(Landroid/content/SharedPreferences;Z)V
    .locals 2
    .param p0, "preferences"    # Landroid/content/SharedPreferences;
    .param p1, "enabled"    # Z

    .prologue
    .line 68
    invoke-interface {p0}, Landroid/content/SharedPreferences;->edit()Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    const-string v1, "select_subtitle_when_no_audio_in_device_language"

    invoke-interface {v0, v1, p1}, Landroid/content/SharedPreferences$Editor;->putBoolean(Ljava/lang/String;Z)Landroid/content/SharedPreferences$Editor;

    move-result-object v0

    invoke-interface {v0}, Landroid/content/SharedPreferences$Editor;->commit()Z

    .line 71
    return-void
.end method

.method public static selectTrack(Ljava/util/List;Lcom/google/android/videos/player/CaptionPreferences;Landroid/content/SharedPreferences;ILjava/lang/String;ZLcom/google/android/videos/player/PlaybackResumeState;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 5
    .param p1, "captionPreferences"    # Lcom/google/android/videos/player/CaptionPreferences;
    .param p2, "preferences"    # Landroid/content/SharedPreferences;
    .param p3, "subtitleMode"    # I
    .param p4, "subtitleDefaultLanguage"    # Ljava/lang/String;
    .param p5, "haveAudioInDeviceLanguage"    # Z
    .param p6, "playbackResumeState"    # Lcom/google/android/videos/player/PlaybackResumeState;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Lcom/google/android/videos/player/CaptionPreferences;",
            "Landroid/content/SharedPreferences;",
            "I",
            "Ljava/lang/String;",
            "Z",
            "Lcom/google/android/videos/player/PlaybackResumeState;",
            ")",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;"
        }
    .end annotation

    .prologue
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v2, 0x0

    const/4 v4, 0x0

    .line 80
    if-eqz p0, :cond_0

    invoke-interface {p0}, Ljava/util/List;->isEmpty()Z

    move-result v3

    if-eqz v3, :cond_2

    :cond_0
    move-object v1, v2

    .line 108
    :cond_1
    :goto_0
    return-object v1

    .line 83
    :cond_2
    if-eqz p6, :cond_3

    invoke-virtual {p6}, Lcom/google/android/videos/player/PlaybackResumeState;->areSubtitlesTurnedOff()Z

    move-result v3

    if-eqz v3, :cond_3

    move-object v1, v2

    .line 84
    goto :goto_0

    .line 86
    :cond_3
    const/4 v1, 0x0

    .line 87
    .local v1, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    if-eqz p6, :cond_4

    invoke-virtual {p6}, Lcom/google/android/videos/player/PlaybackResumeState;->areSubtitlesTurnedOn()Z

    move-result v3

    if-eqz v3, :cond_4

    .line 88
    invoke-virtual {p6}, Lcom/google/android/videos/player/PlaybackResumeState;->getSubtitlesLanguage()Ljava/lang/String;

    move-result-object v3

    invoke-static {p0, v3, v4}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v1

    .line 91
    :cond_4
    if-nez v1, :cond_5

    invoke-virtual {p1}, Lcom/google/android/videos/player/CaptionPreferences;->isEnabled()Z

    move-result v3

    if-eqz v3, :cond_5

    .line 92
    invoke-virtual {p1}, Lcom/google/android/videos/player/CaptionPreferences;->getLocale()Ljava/util/Locale;

    move-result-object v0

    .line 93
    .local v0, "locale":Ljava/util/Locale;
    if-nez v0, :cond_8

    .line 95
    invoke-static {p0, v2}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->selectTrack(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v1

    .line 100
    .end local v0    # "locale":Ljava/util/Locale;
    :cond_5
    :goto_1
    if-nez v1, :cond_7

    const/4 v2, 0x2

    if-eq p3, v2, :cond_6

    const/4 v2, 0x3

    if-ne p3, v2, :cond_7

    .line 102
    :cond_6
    invoke-static {p0, p4}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->selectTrack(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v1

    .line 104
    :cond_7
    if-nez v1, :cond_1

    if-nez p5, :cond_1

    const-string v2, "select_subtitle_when_no_audio_in_device_language"

    const/4 v3, 0x1

    invoke-interface {p2, v2, v3}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-eqz v2, :cond_1

    .line 106
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v2

    invoke-static {p0, v2, v4}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/util/Locale;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v1

    goto :goto_0

    .line 97
    .restart local v0    # "locale":Ljava/util/Locale;
    :cond_8
    invoke-static {p0, v0, v4}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/util/Locale;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v1

    goto :goto_1
.end method

.method private static selectTrack(Ljava/util/List;Ljava/lang/String;)Lcom/google/android/videos/subtitles/SubtitleTrack;
    .locals 3
    .param p1, "preferredLanguage"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;",
            "Ljava/lang/String;",
            ")",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;"
        }
    .end annotation

    .prologue
    .local p0, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    const/4 v2, 0x0

    .line 120
    if-eqz p1, :cond_0

    .line 121
    invoke-static {p0, p1, v2}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/lang/String;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    .line 122
    .local v0, "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    if-eqz v0, :cond_0

    move-object v1, v0

    .line 130
    :goto_0
    return-object v1

    .line 126
    .end local v0    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    :cond_0
    invoke-static {}, Ljava/util/Locale;->getDefault()Ljava/util/Locale;

    move-result-object v1

    invoke-static {p0, v1, v2}, Lcom/google/android/videos/subtitles/TrackSelectionUtil;->findTrack(Ljava/util/List;Ljava/util/Locale;Z)Lcom/google/android/videos/subtitles/SubtitleTrack;

    move-result-object v0

    .line 127
    .restart local v0    # "track":Lcom/google/android/videos/subtitles/SubtitleTrack;
    if-eqz v0, :cond_1

    move-object v1, v0

    .line 128
    goto :goto_0

    .line 130
    :cond_1
    invoke-interface {p0, v2}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/subtitles/SubtitleTrack;

    goto :goto_0
.end method
