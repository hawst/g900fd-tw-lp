.class final Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;
.super Ljava/lang/Object;
.source "DefaultKnowledgeRequesters.java"

# interfaces
.implements Lcom/google/android/videos/converter/RequestConverter;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "TagStreamHttpRequestConverter"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/converter/RequestConverter",
        "<",
        "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;",
        "Lorg/apache/http/client/methods/HttpUriRequest;",
        ">;"
    }
.end annotation


# direct methods
.method private constructor <init>()V
    .locals 0

    .prologue
    .line 126
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$1;

    .prologue
    .line 126
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;-><init>()V

    return-void
.end method


# virtual methods
.method public bridge synthetic convertRequest(Ljava/lang/Object;)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/Object;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Lcom/google/android/videos/converter/ConverterException;
        }
    .end annotation

    .prologue
    .line 126
    check-cast p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequestConverter;->convertRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    return-object v0
.end method

.method public convertRequest(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;)Lorg/apache/http/client/methods/HttpUriRequest;
    .locals 3
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;

    .prologue
    .line 131
    iget-object v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->url:Ljava/lang/String;

    invoke-static {v1}, Lcom/google/android/videos/async/HttpUriRequestFactory;->createGet(Ljava/lang/String;)Lorg/apache/http/client/methods/HttpUriRequest;

    move-result-object v0

    .line 132
    .local v0, "httpGet":Lorg/apache/http/client/methods/HttpUriRequest;
    iget-object v1, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->ifModifiedSince:Ljava/lang/String;

    invoke-static {v1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 133
    const-string v1, "If-Modified-Since"

    iget-object v2, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamHttpRequest;->ifModifiedSince:Ljava/lang/String;

    invoke-interface {v0, v1, v2}, Lorg/apache/http/client/methods/HttpUriRequest;->setHeader(Ljava/lang/String;Ljava/lang/String;)V

    .line 135
    :cond_0
    return-object v0
.end method
