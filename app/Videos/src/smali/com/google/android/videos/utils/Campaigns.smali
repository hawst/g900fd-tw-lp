.class public Lcom/google/android/videos/utils/Campaigns;
.super Ljava/lang/Object;
.source "Campaigns.java"


# direct methods
.method public static getCampaignId(I)Ljava/lang/String;
    .locals 1
    .param p0, "eventSource"    # I

    .prologue
    .line 34
    packed-switch p0, :pswitch_data_0

    .line 73
    :pswitch_0
    const-string v0, ""

    :goto_0
    return-object v0

    .line 36
    :pswitch_1
    const-string v0, "movies_info_card_movies"

    goto :goto_0

    .line 38
    :pswitch_2
    const-string v0, "movies_info_card_actor"

    goto :goto_0

    .line 40
    :pswitch_3
    const-string v0, "movies_info_card_song"

    goto :goto_0

    .line 42
    :pswitch_4
    const-string v0, "movies_next_episode"

    goto :goto_0

    .line 44
    :pswitch_5
    const-string v0, "movies_pano_movie_details"

    goto :goto_0

    .line 46
    :pswitch_6
    const-string v0, "movies_suggestion"

    goto :goto_0

    .line 48
    :pswitch_7
    const-string v0, "movies_see_more"

    goto :goto_0

    .line 50
    :pswitch_8
    const-string v0, "movies_warm_welcome"

    goto :goto_0

    .line 52
    :pswitch_9
    const-string v0, "movies_knowledge_promo_welcome"

    goto :goto_0

    .line 54
    :pswitch_a
    const-string v0, "movies_free_movie_promo_welcome"

    goto :goto_0

    .line 56
    :pswitch_b
    const-string v0, "movies_returning_user_welcome"

    goto :goto_0

    .line 58
    :pswitch_c
    const-string v0, "movies_purchase_proxy"

    goto :goto_0

    .line 60
    :pswitch_d
    const-string v0, "movies_side_drawer"

    goto :goto_0

    .line 62
    :pswitch_e
    const-string v0, "movies_search_intent"

    goto :goto_0

    .line 64
    :pswitch_f
    const-string v0, "movies_expired_rental"

    goto :goto_0

    .line 66
    :pswitch_10
    const-string v0, "movies_play_widget"

    goto :goto_0

    .line 68
    :pswitch_11
    const-string v0, "movies_play_widget_promo"

    goto :goto_0

    .line 70
    :pswitch_12
    const-string v0, "movies_new_season_notification"

    goto :goto_0

    .line 34
    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_3
        :pswitch_4
        :pswitch_5
        :pswitch_6
        :pswitch_7
        :pswitch_8
        :pswitch_9
        :pswitch_a
        :pswitch_b
        :pswitch_c
        :pswitch_d
        :pswitch_e
        :pswitch_f
        :pswitch_0
        :pswitch_10
        :pswitch_11
        :pswitch_0
        :pswitch_0
        :pswitch_0
        :pswitch_12
    .end packed-switch
.end method
