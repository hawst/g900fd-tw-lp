.class public Lcom/google/android/videos/ui/VerticalsHelper;
.super Ljava/lang/Object;
.source "VerticalsHelper.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;,
        Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;,
        Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;
    }
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
        "Ljava/util/HashMap",
        "<",
        "Ljava/lang/String;",
        "Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;",
        ">;>;"
    }
.end annotation


# instance fields
.field private final config:Lcom/google/android/videos/Config;

.field private final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field private final onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/concurrent/CopyOnWriteArrayList",
            "<",
            "Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;",
            ">;"
        }
    .end annotation
.end field

.field private pendingUpdateRequest:Z

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final purchaseStoreCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Landroid/database/Cursor;",
            ">;"
        }
    .end annotation
.end field

.field private updateInProgress:Z

.field private final usersContent:Ljava/util/Map;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/os/Handler;Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/store/PurchaseStore;)V
    .locals 2
    .param p1, "mainHandler"    # Landroid/os/Handler;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p4, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;

    .prologue
    .line 67
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 68
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/Config;

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->config:Lcom/google/android/videos/Config;

    .line 69
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 70
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 71
    new-instance v0, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;

    invoke-static {p1, p0}, Lcom/google/android/videos/async/HandlerCallback;->create(Landroid/os/Handler;Lcom/google/android/videos/async/Callback;)Lcom/google/android/videos/async/HandlerCallback;

    move-result-object v1

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/VerticalsHelper$CursorProcessingCallback;-><init>(Lcom/google/android/videos/async/Callback;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->purchaseStoreCallback:Lcom/google/android/videos/async/Callback;

    .line 73
    new-instance v0, Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-direct {v0}, Ljava/util/concurrent/CopyOnWriteArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    .line 74
    new-instance v0, Ljava/util/HashMap;

    invoke-direct {v0}, Ljava/util/HashMap;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->usersContent:Ljava/util/Map;

    .line 75
    return-void
.end method

.method private areVerticalsEnabled(Ljava/lang/String;I)Z
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "verticals"    # I

    .prologue
    const/4 v3, 0x1

    const/4 v2, 0x0

    .line 177
    const/4 v0, 0x0

    .line 178
    .local v0, "enabledVerticals":I
    invoke-static {p1}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 179
    iget-object v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v1}, Lcom/google/android/videos/Config;->disableWatchNow()Z

    move-result v1

    if-nez v1, :cond_1

    const/4 v1, 0x4

    :goto_0
    or-int/2addr v0, v1

    .line 180
    iget-object v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->moviesVerticalEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_2

    move v1, v3

    :goto_1
    or-int/2addr v0, v1

    .line 181
    iget-object v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    invoke-virtual {v1, p1}, Lcom/google/android/videos/store/ConfigurationStore;->showsVerticalEnabled(Ljava/lang/String;)Z

    move-result v1

    if-eqz v1, :cond_3

    const/4 v1, 0x2

    :goto_2
    or-int/2addr v0, v1

    .line 183
    :cond_0
    and-int v1, v0, p2

    if-ne v1, p2, :cond_4

    :goto_3
    return v3

    :cond_1
    move v1, v2

    .line 179
    goto :goto_0

    :cond_2
    move v1, v2

    .line 180
    goto :goto_1

    :cond_3
    move v1, v2

    .line 181
    goto :goto_2

    :cond_4
    move v3, v2

    .line 183
    goto :goto_3
.end method

.method public static getHelpContext(I)Ljava/lang/String;
    .locals 1
    .param p0, "vertical"    # I

    .prologue
    .line 248
    packed-switch p0, :pswitch_data_0

    .line 256
    :pswitch_0
    const-string v0, "mobile_movies_default"

    :goto_0
    return-object v0

    .line 250
    :pswitch_1
    const-string v0, "mobile_my_movies"

    goto :goto_0

    .line 252
    :pswitch_2
    const-string v0, "mobile_my_tv_shows"

    goto :goto_0

    .line 254
    :pswitch_3
    const-string v0, "mobile_watch_now"

    goto :goto_0

    .line 248
    nop

    :pswitch_data_0
    .packed-switch 0x1
        :pswitch_1
        :pswitch_2
        :pswitch_0
        :pswitch_3
    .end packed-switch
.end method

.method public static getVerticalExternalName(I)Ljava/lang/String;
    .locals 3
    .param p0, "vertical"    # I

    .prologue
    .line 214
    sparse-switch p0, :sswitch_data_0

    .line 226
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected vertical: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 216
    :sswitch_0
    const-string v0, "movies"

    .line 224
    :goto_0
    return-object v0

    .line 218
    :sswitch_1
    const-string v0, "shows"

    goto :goto_0

    .line 220
    :sswitch_2
    const-string v0, "watchnow"

    goto :goto_0

    .line 222
    :sswitch_3
    const-string v0, "wishlist"

    goto :goto_0

    .line 224
    :sswitch_4
    const-string v0, "library"

    goto :goto_0

    .line 214
    nop

    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_2
        0x8 -> :sswitch_3
        0x10 -> :sswitch_4
    .end sparse-switch
.end method

.method public static getVerticalTitleResourceId(I)I
    .locals 3
    .param p0, "vertical"    # I

    .prologue
    .line 231
    sparse-switch p0, :sswitch_data_0

    .line 243
    new-instance v0, Ljava/lang/IllegalStateException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Unexpected vertical: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Ljava/lang/IllegalStateException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 233
    :sswitch_0
    const v0, 0x7f0b0094

    .line 241
    :goto_0
    return v0

    .line 235
    :sswitch_1
    const v0, 0x7f0b0095

    goto :goto_0

    .line 237
    :sswitch_2
    const v0, 0x7f0b009a

    goto :goto_0

    .line 239
    :sswitch_3
    const v0, 0x7f0b0096

    goto :goto_0

    .line 241
    :sswitch_4
    const v0, 0x7f0b0097

    goto :goto_0

    .line 231
    :sswitch_data_0
    .sparse-switch
        0x1 -> :sswitch_0
        0x2 -> :sswitch_1
        0x4 -> :sswitch_3
        0x8 -> :sswitch_4
        0x10 -> :sswitch_2
    .end sparse-switch
.end method

.method private onUpdateCompleted()V
    .locals 2

    .prologue
    const/4 v1, 0x0

    .line 105
    iput-boolean v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->updateInProgress:Z

    .line 106
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->pendingUpdateRequest:Z

    if-eqz v0, :cond_0

    .line 107
    iput-boolean v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->pendingUpdateRequest:Z

    .line 108
    invoke-virtual {p0}, Lcom/google/android/videos/ui/VerticalsHelper;->updateUsersContent()V

    .line 110
    :cond_0
    return-void
.end method

.method public static parseExternalVerticalName(Ljava/lang/String;)I
    .locals 1
    .param p0, "verticalName"    # Ljava/lang/String;

    .prologue
    .line 195
    const-string v0, "movies"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 196
    const/4 v0, 0x1

    .line 210
    :goto_0
    return v0

    .line 198
    :cond_0
    const-string v0, "shows"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 199
    const/4 v0, 0x2

    goto :goto_0

    .line 201
    :cond_1
    const-string v0, "watchnow"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_2

    .line 202
    const/4 v0, 0x4

    goto :goto_0

    .line 204
    :cond_2
    const-string v0, "wishlist"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_3

    .line 205
    const/16 v0, 0x8

    goto :goto_0

    .line 207
    :cond_3
    const-string v0, "library"

    invoke-virtual {v0, p0}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_4

    .line 208
    const/16 v0, 0x10

    goto :goto_0

    .line 210
    :cond_4
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private setHasContent(Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;)V
    .locals 7
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "newContentTypes"    # Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    .prologue
    .line 113
    iget-object v5, p0, Lcom/google/android/videos/ui/VerticalsHelper;->usersContent:Ljava/util/Map;

    invoke-interface {v5, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    .line 114
    .local v0, "currentContentTypes":Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;
    if-eqz v0, :cond_1

    iget v5, v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->contentTypes:I

    iget v6, p2, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->contentTypes:I

    if-ne v5, v6, :cond_1

    iget v5, v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->pinnedContentTypes:I

    iget v6, p2, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->pinnedContentTypes:I

    if-ne v5, v6, :cond_1

    .line 130
    :cond_0
    return-void

    .line 119
    :cond_1
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v1

    .line 120
    .local v1, "currentEnabledVerticals":I
    iget-object v5, p0, Lcom/google/android/videos/ui/VerticalsHelper;->usersContent:Ljava/util/Map;

    invoke-interface {v5, p1, p2}, Ljava/util/Map;->put(Ljava/lang/Object;Ljava/lang/Object;)Ljava/lang/Object;

    .line 121
    invoke-virtual {p0, p1}, Lcom/google/android/videos/ui/VerticalsHelper;->enabledVerticalsForUser(Ljava/lang/String;)I

    move-result v4

    .line 122
    .local v4, "newEnabledVerticals":I
    iget-object v5, p0, Lcom/google/android/videos/ui/VerticalsHelper;->onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_3

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;

    .line 123
    .local v3, "listener":Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;
    if-nez v0, :cond_2

    const/4 v5, 0x1

    :goto_1
    invoke-interface {v3, p1, v5}, Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;->onHasContentChanged(Ljava/lang/String;Z)V

    goto :goto_0

    :cond_2
    const/4 v5, 0x0

    goto :goto_1

    .line 125
    .end local v3    # "listener":Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;
    :cond_3
    if-eq v1, v4, :cond_0

    .line 126
    iget-object v5, p0, Lcom/google/android/videos/ui/VerticalsHelper;->onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v5}, Ljava/util/concurrent/CopyOnWriteArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    :goto_2
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v5

    if-eqz v5, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;

    .line 127
    .restart local v3    # "listener":Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;
    invoke-interface {v3, p1, v4}, Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;->onEnabledVerticalsChanged(Ljava/lang/String;I)V

    goto :goto_2
.end method


# virtual methods
.method public addOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;

    .prologue
    .line 187
    iget-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->add(Ljava/lang/Object;)Z

    .line 188
    return-void
.end method

.method public enabledVerticalsForUser(Ljava/lang/String;)I
    .locals 9
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v7, 0x4

    const/4 v6, 0x2

    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 156
    const/4 v3, 0x0

    .line 157
    .local v3, "verticals":I
    invoke-direct {p0, p1, v5}, Lcom/google/android/videos/ui/VerticalsHelper;->areVerticalsEnabled(Ljava/lang/String;I)Z

    move-result v8

    if-nez v8, :cond_0

    invoke-virtual {p0, p1, v5, v4}, Lcom/google/android/videos/ui/VerticalsHelper;->hasContent(Ljava/lang/String;IZ)Z

    move-result v8

    if-eqz v8, :cond_6

    :cond_0
    move v0, v5

    .line 160
    .local v0, "showMoviesVertical":Z
    :goto_0
    invoke-direct {p0, p1, v6}, Lcom/google/android/videos/ui/VerticalsHelper;->areVerticalsEnabled(Ljava/lang/String;I)Z

    move-result v8

    if-nez v8, :cond_1

    invoke-virtual {p0, p1, v6, v4}, Lcom/google/android/videos/ui/VerticalsHelper;->hasContent(Ljava/lang/String;IZ)Z

    move-result v8

    if-eqz v8, :cond_7

    :cond_1
    move v1, v5

    .line 163
    .local v1, "showShowsVertical":Z
    :goto_1
    invoke-direct {p0, p1, v7}, Lcom/google/android/videos/ui/VerticalsHelper;->areVerticalsEnabled(Ljava/lang/String;I)Z

    move-result v8

    if-eqz v8, :cond_8

    if-nez v0, :cond_2

    if-eqz v1, :cond_8

    :cond_2
    move v2, v5

    .line 166
    .local v2, "showWatchNow":Z
    :goto_2
    if-eqz v2, :cond_9

    :goto_3
    or-int/2addr v3, v7

    .line 167
    if-eqz v0, :cond_a

    :goto_4
    or-int/2addr v3, v5

    .line 168
    if-eqz v1, :cond_b

    move v5, v6

    :goto_5
    or-int/2addr v3, v5

    .line 169
    if-nez v0, :cond_3

    if-eqz v1, :cond_c

    :cond_3
    const/16 v5, 0x10

    :goto_6
    or-int/2addr v3, v5

    .line 170
    iget-object v5, p0, Lcom/google/android/videos/ui/VerticalsHelper;->config:Lcom/google/android/videos/Config;

    invoke-interface {v5}, Lcom/google/android/videos/Config;->suggestionsEnabled()Z

    move-result v5

    if-eqz v5, :cond_5

    if-nez v0, :cond_4

    if-eqz v1, :cond_5

    :cond_4
    const/16 v4, 0x8

    :cond_5
    or-int/2addr v3, v4

    .line 173
    return v3

    .end local v0    # "showMoviesVertical":Z
    .end local v1    # "showShowsVertical":Z
    .end local v2    # "showWatchNow":Z
    :cond_6
    move v0, v4

    .line 157
    goto :goto_0

    .restart local v0    # "showMoviesVertical":Z
    :cond_7
    move v1, v4

    .line 160
    goto :goto_1

    .restart local v1    # "showShowsVertical":Z
    :cond_8
    move v2, v4

    .line 163
    goto :goto_2

    .restart local v2    # "showWatchNow":Z
    :cond_9
    move v7, v4

    .line 166
    goto :goto_3

    :cond_a
    move v5, v4

    .line 167
    goto :goto_4

    :cond_b
    move v5, v4

    .line 168
    goto :goto_5

    :cond_c
    move v5, v4

    .line 169
    goto :goto_6
.end method

.method public getAvailableContentTypes(Ljava/lang/String;Z)I
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "downloadedOnly"    # Z

    .prologue
    .line 133
    iget-object v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->usersContent:Ljava/util/Map;

    invoke-interface {v1, p1}, Ljava/util/Map;->get(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    .line 134
    .local v0, "content":Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;
    if-nez v0, :cond_0

    .line 135
    const/4 v1, 0x0

    .line 139
    :goto_0
    return v1

    .line 136
    :cond_0
    if-eqz p2, :cond_1

    .line 137
    iget v1, v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->pinnedContentTypes:I

    goto :goto_0

    .line 139
    :cond_1
    iget v1, v0, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;->contentTypes:I

    goto :goto_0
.end method

.method public hasContent(Ljava/lang/String;)Z
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v0, 0x0

    .line 144
    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/ui/VerticalsHelper;->getAvailableContentTypes(Ljava/lang/String;Z)I

    move-result v1

    if-eqz v1, :cond_0

    const/4 v0, 0x1

    :cond_0
    return v0
.end method

.method public hasContent(Ljava/lang/String;IZ)Z
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "contentTypes"    # I
    .param p3, "downloadedOnly"    # Z

    .prologue
    .line 152
    invoke-virtual {p0, p1, p3}, Lcom/google/android/videos/ui/VerticalsHelper;->getAvailableContentTypes(Ljava/lang/String;Z)I

    move-result v0

    and-int/2addr v0, p2

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 101
    invoke-direct {p0}, Lcom/google/android/videos/ui/VerticalsHelper;->onUpdateCompleted()V

    .line 102
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/VerticalsHelper;->onError(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/util/HashMap;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;",
            "Ljava/util/HashMap",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 93
    .local p2, "userContentVerticals":Ljava/util/HashMap;, "Ljava/util/HashMap<Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;>;"
    invoke-virtual {p2}, Ljava/util/HashMap;->entrySet()Ljava/util/Set;

    move-result-object v2

    invoke-interface {v2}, Ljava/util/Set;->iterator()Ljava/util/Iterator;

    move-result-object v1

    .local v1, "i$":Ljava/util/Iterator;
    :goto_0
    invoke-interface {v1}, Ljava/util/Iterator;->hasNext()Z

    move-result v2

    if-eqz v2, :cond_0

    invoke-interface {v1}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/util/Map$Entry;

    .line 94
    .local v0, "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;>;"
    invoke-interface {v0}, Ljava/util/Map$Entry;->getKey()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Ljava/lang/String;

    invoke-interface {v0}, Ljava/util/Map$Entry;->getValue()Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;

    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/ui/VerticalsHelper;->setHasContent(Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;)V

    goto :goto_0

    .line 96
    .end local v0    # "entry":Ljava/util/Map$Entry;, "Ljava/util/Map$Entry<Ljava/lang/String;Lcom/google/android/videos/ui/VerticalsHelper$ContentVerticals;>;"
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/VerticalsHelper;->onUpdateCompleted()V

    .line 97
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 26
    check-cast p1, Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/util/HashMap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/VerticalsHelper;->onResponse(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Ljava/util/HashMap;)V

    return-void
.end method

.method public removeOnContentChangedListener(Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;)V
    .locals 1
    .param p1, "listener"    # Lcom/google/android/videos/ui/VerticalsHelper$OnContentChangedListener;

    .prologue
    .line 191
    iget-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->onContentChangedListeners:Ljava/util/concurrent/CopyOnWriteArrayList;

    invoke-virtual {v0, p1}, Ljava/util/concurrent/CopyOnWriteArrayList;->remove(Ljava/lang/Object;)Z

    .line 192
    return-void
.end method

.method public updateUsersContent()V
    .locals 4

    .prologue
    const/4 v1, 0x1

    .line 80
    iget-boolean v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->updateInProgress:Z

    if-eqz v0, :cond_0

    .line 81
    iput-boolean v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->pendingUpdateRequest:Z

    .line 88
    :goto_0
    return-void

    .line 83
    :cond_0
    iput-boolean v1, p0, Lcom/google/android/videos/ui/VerticalsHelper;->updateInProgress:Z

    .line 84
    iget-object v0, p0, Lcom/google/android/videos/ui/VerticalsHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {v2, v3}, Lcom/google/android/videos/store/PurchaseRequests;->createContentTypesRequest(J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/VerticalsHelper;->purchaseStoreCallback:Lcom/google/android/videos/async/Callback;

    invoke-virtual {v0, v1, v2}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method
