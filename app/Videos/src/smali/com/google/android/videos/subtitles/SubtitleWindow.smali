.class public final Lcom/google/android/videos/subtitles/SubtitleWindow;
.super Ljava/lang/Object;
.source "SubtitleWindow.java"

# interfaces
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/subtitles/SubtitleWindow$1;,
        Lcom/google/android/videos/subtitles/SubtitleWindow$Builder;
    }
.end annotation


# instance fields
.field private final id:I

.field final settingsTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

.field final textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;


# direct methods
.method private constructor <init>(ILcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;)V
    .locals 0
    .param p1, "id"    # I
    .param p2, "textTimeline"    # Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;
    .param p3, "settingsTimeline"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    .prologue
    .line 20
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 21
    iput p1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->id:I

    .line 22
    iput-object p2, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    .line 23
    iput-object p3, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->settingsTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    .line 24
    return-void
.end method

.method synthetic constructor <init>(ILcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;Lcom/google/android/videos/subtitles/SubtitleWindow$1;)V
    .locals 0
    .param p1, "x0"    # I
    .param p2, "x1"    # Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;
    .param p3, "x2"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;
    .param p4, "x3"    # Lcom/google/android/videos/subtitles/SubtitleWindow$1;

    .prologue
    .line 13
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/subtitles/SubtitleWindow;-><init>(ILcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;)V

    return-void
.end method


# virtual methods
.method public getSettingsAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    .locals 1
    .param p1, "timeMillis"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->settingsTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;->getSubtitleWindowSettingsAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    move-result-object v0

    return-object v0
.end method

.method public getSubtitleWindowSnapshotAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;
    .locals 4
    .param p1, "timeMillis"    # I

    .prologue
    .line 49
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->id:I

    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindow;->getTextAt(I)Ljava/lang/String;

    move-result-object v2

    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindow;->getSettingsAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    move-result-object v3

    invoke-direct {v0, v1, p1, v2, v3}, Lcom/google/android/videos/subtitles/SubtitleWindowSnapshot;-><init>(IILjava/lang/String;Lcom/google/android/videos/subtitles/SubtitleWindowSettings;)V

    return-object v0
.end method

.method public getTextAt(I)Ljava/lang/String;
    .locals 4
    .param p1, "timeMillis"    # I

    .prologue
    .line 38
    invoke-virtual {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindow;->getSettingsAt(I)Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    move-result-object v1

    .line 39
    .local v1, "settingsAt":Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
    if-eqz v1, :cond_0

    invoke-virtual {v1}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->getScrollDirection()I

    move-result v2

    const/4 v3, -0x1

    if-eq v2, v3, :cond_0

    const/4 v0, 0x1

    .line 41
    .local v0, "rollUp":Z
    :goto_0
    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    invoke-virtual {v2, p1, v0}, Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;->getSubtitleWindowTextAt(IZ)Ljava/lang/String;

    move-result-object v2

    return-object v2

    .line 39
    .end local v0    # "rollUp":Z
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "id: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->id:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " text: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->textTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowTextTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, " settings: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindow;->settingsTimeline:Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method
