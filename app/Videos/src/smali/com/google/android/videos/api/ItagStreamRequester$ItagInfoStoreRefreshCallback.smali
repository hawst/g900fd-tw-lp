.class Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;
.super Ljava/lang/Object;
.source "ItagStreamRequester.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/api/ItagStreamRequester;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ItagInfoStoreRefreshCallback"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Ljava/lang/Integer;",
        "Ljava/lang/Void;",
        ">;"
    }
.end annotation


# instance fields
.field private final mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

.field private final mpdResponse:Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

.field private final originalCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/api/ItagStreamRequester;


# direct methods
.method private constructor <init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)V
    .locals 0
    .param p3, "mpdRequest"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p4, "mpdResponse"    # Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/android/videos/streams/Streams;",
            ">;",
            "Lcom/google/android/videos/api/MpdGetRequest;",
            "Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;",
            ")V"
        }
    .end annotation

    .prologue
    .line 154
    .local p2, "originalCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/android/videos/streams/Streams;>;"
    iput-object p1, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 155
    iput-object p2, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    .line 156
    iput-object p3, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

    .line 157
    iput-object p4, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdResponse:Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    .line 158
    return-void
.end method

.method synthetic constructor <init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;Lcom/google/android/videos/api/ItagStreamRequester$1;)V
    .locals 0
    .param p1, "x0"    # Lcom/google/android/videos/api/ItagStreamRequester;
    .param p2, "x1"    # Lcom/google/android/videos/async/Callback;
    .param p3, "x2"    # Lcom/google/android/videos/api/MpdGetRequest;
    .param p4, "x3"    # Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;
    .param p5, "x4"    # Lcom/google/android/videos/api/ItagStreamRequester$1;

    .prologue
    .line 147
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;-><init>(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)V

    return-void
.end method


# virtual methods
.method public onError(Ljava/lang/Integer;Ljava/lang/Exception;)V
    .locals 5
    .param p1, "itagRequest"    # Ljava/lang/Integer;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 169
    const-string v0, "Could not refresh itag knowledge"

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 170
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v2, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    iget-object v3, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v4, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdResponse:Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    # invokes: Lcom/google/android/videos/api/ItagStreamRequester;->convertToStreams(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;
    invoke-static {v2, v3, v4}, Lcom/google/android/videos/api/ItagStreamRequester;->access$300(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 171
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 147
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->onError(Ljava/lang/Integer;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Ljava/lang/Integer;Ljava/lang/Void;)V
    .locals 5
    .param p1, "itagRequest"    # Ljava/lang/Integer;
    .param p2, "itagResponse"    # Ljava/lang/Void;

    .prologue
    .line 162
    iget-object v0, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->originalCallback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v2, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->this$0:Lcom/google/android/videos/api/ItagStreamRequester;

    iget-object v3, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdRequest:Lcom/google/android/videos/api/MpdGetRequest;

    iget-object v4, p0, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->mpdResponse:Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;

    # invokes: Lcom/google/android/videos/api/ItagStreamRequester;->convertToStreams(Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;
    invoke-static {v2, v3, v4}, Lcom/google/android/videos/api/ItagStreamRequester;->access$300(Lcom/google/android/videos/api/ItagStreamRequester;Lcom/google/android/videos/api/MpdGetRequest;Lcom/google/wireless/android/video/magma/proto/MpdGetResponse;)Lcom/google/android/videos/streams/Streams;

    move-result-object v2

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 163
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 147
    check-cast p1, Ljava/lang/Integer;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Ljava/lang/Void;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/api/ItagStreamRequester$ItagInfoStoreRefreshCallback;->onResponse(Ljava/lang/Integer;Ljava/lang/Void;)V

    return-void
.end method
