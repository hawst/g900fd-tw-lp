.class public Lcom/google/android/videos/NewSeasonNotificationService;
.super Landroid/app/IntentService;
.source "NewSeasonNotificationService.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/NewSeasonNotificationService$Query;
    }
.end annotation


# instance fields
.field private videosGlobals:Lcom/google/android/videos/VideosGlobals;


# direct methods
.method public constructor <init>()V
    .locals 1

    .prologue
    .line 47
    const-class v0, Lcom/google/android/videos/NewSeasonNotificationService;

    invoke-virtual {v0}, Ljava/lang/Class;->getSimpleName()Ljava/lang/String;

    move-result-object v0

    invoke-direct {p0, v0}, Landroid/app/IntentService;-><init>(Ljava/lang/String;)V

    .line 48
    return-void
.end method

.method public static createNotifyIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;

    .prologue
    .line 192
    new-instance v0, Landroid/content/Intent;

    const-string v1, "com.google.android.videos.NOTIFY_NEW_SEASON"

    invoke-direct {v0, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v1, Lcom/google/android/videos/NewSeasonNotificationService;

    invoke-virtual {v0, p0, v1}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "account"

    invoke-virtual {v0, v1, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "show_id"

    invoke-virtual {v0, v1, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    const-string v1, "season_id"

    invoke-virtual {v0, v1, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private createViewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 182
    new-instance v1, Landroid/content/Intent;

    const-string v2, "android.intent.action.VIEW"

    invoke-direct {v1, v2}, Landroid/content/Intent;-><init>(Ljava/lang/String;)V

    const-class v2, Lcom/google/android/videos/NewSeasonNotificationService;

    invoke-virtual {v1, p0, v2}, Landroid/content/Intent;->setClass(Landroid/content/Context;Ljava/lang/Class;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "account"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "show_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v0

    .line 186
    .local v0, "intent":Landroid/content/Intent;
    invoke-virtual {p2}, Ljava/lang/String;->hashCode()I

    move-result v1

    const/high16 v2, 0x8000000

    invoke-static {p0, v1, v0, v2}, Landroid/app/PendingIntent;->getService(Landroid/content/Context;ILandroid/content/Intent;I)Landroid/app/PendingIntent;

    move-result-object v1

    return-object v1
.end method

.method private getNotificationText(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;
    .locals 6
    .param p1, "season"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v5, 0x1

    const/4 v4, 0x0

    .line 135
    if-nez p1, :cond_0

    .line 136
    const v1, 0x7f0b01ae

    invoke-virtual {p0, v1}, Lcom/google/android/videos/NewSeasonNotificationService;->getString(I)Ljava/lang/String;

    move-result-object v1

    .line 143
    :goto_0
    return-object v1

    .line 139
    :cond_0
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v0, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    .line 140
    .local v0, "seasonTitle":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 141
    const v1, 0x7f0b00c7

    new-array v2, v5, [Ljava/lang/Object;

    iget-object v3, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v3, v3, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->sequenceNumber:Ljava/lang/String;

    aput-object v3, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/videos/NewSeasonNotificationService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v0

    .line 143
    :cond_1
    const v1, 0x7f0b01ad

    new-array v2, v5, [Ljava/lang/Object;

    aput-object v0, v2, v4

    invoke-virtual {p0, v1, v2}, Lcom/google/android/videos/NewSeasonNotificationService;->getString(I[Ljava/lang/Object;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getSeasonAsset(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 9
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "seasonId"    # Ljava/lang/String;

    .prologue
    const/4 v5, 0x0

    const/4 v8, 0x1

    .line 162
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 163
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v4, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    invoke-virtual {v4, p1}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v4

    invoke-virtual {v4, v8}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v0

    .line 167
    .local v0, "builder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    invoke-static {p2}, Lcom/google/android/videos/api/AssetResourceUtil;->idFromSeasonId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v0, v4}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    .line 168
    iget-object v4, p0, Lcom/google/android/videos/NewSeasonNotificationService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getApiRequesters()Lcom/google/android/videos/api/ApiRequesters;

    move-result-object v4

    invoke-interface {v4}, Lcom/google/android/videos/api/ApiRequesters;->getAssetsCachingRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v4

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v6

    invoke-interface {v4, v6, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 171
    const-wide/16 v6, 0x2710

    :try_start_0
    invoke-virtual {v1, v6, v7}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    iget-object v3, v4, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 172
    .local v3, "result":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    array-length v4, v3

    if-ne v4, v8, :cond_0

    const/4 v4, 0x0

    aget-object v4, v3, v4
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 178
    .end local v3    # "result":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :goto_0
    return-object v4

    .restart local v3    # "result":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :cond_0
    move-object v4, v5

    .line 172
    goto :goto_0

    .line 173
    .end local v3    # "result":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :catch_0
    move-exception v2

    .line 174
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    const-string v4, "timeout"

    invoke-static {v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .end local v2    # "e":Ljava/util/concurrent/TimeoutException;
    :goto_1
    move-object v4, v5

    .line 178
    goto :goto_0

    .line 175
    :catch_1
    move-exception v2

    .line 176
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Failed to fetch season"

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-static {v4, v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private getSeasonsContainingPurchases(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;

    .prologue
    .line 147
    iget-object v4, p0, Lcom/google/android/videos/NewSeasonNotificationService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v4}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v2

    .line 148
    .local v2, "purchaseStore":Lcom/google/android/videos/store/PurchaseStore;
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 149
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    invoke-static {p1, p2}, Lcom/google/android/videos/NewSeasonNotificationService$Query;->createPurchaseRequest(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v3

    .line 150
    .local v3, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    invoke-virtual {v2, v3, v0}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 152
    const-wide/16 v4, 0x2710

    :try_start_0
    invoke-virtual {v0, v4, v5}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    .line 158
    :goto_0
    return-object v4

    .line 153
    :catch_0
    move-exception v1

    .line 154
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    const-string v4, "timeout"

    invoke-static {v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 158
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    :goto_1
    const/4 v4, 0x0

    goto :goto_0

    .line 155
    :catch_1
    move-exception v1

    .line 156
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Failed to fetch purchases"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_1
.end method

.method private handleNotify(Landroid/content/Intent;)V
    .locals 8
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 74
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "account"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 75
    .local v0, "account":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "show_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v4

    .line 76
    .local v4, "showId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v6

    const-string v7, "season_id"

    invoke-virtual {v6, v7}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    .line 78
    .local v2, "seasonId":Ljava/lang/String;
    const/4 v5, 0x0

    .line 79
    .local v5, "showTitle":Ljava/lang/String;
    invoke-direct {p0, v0, v4}, Lcom/google/android/videos/NewSeasonNotificationService;->getSeasonsContainingPurchases(Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v3

    .line 80
    .local v3, "seasons":Landroid/database/Cursor;
    if-nez v3, :cond_1

    .line 102
    :goto_0
    return-void

    .line 89
    :cond_0
    const/4 v6, 0x0

    :try_start_0
    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v5

    .line 84
    :cond_1
    invoke-interface {v3}, Landroid/database/Cursor;->moveToNext()Z

    move-result v6

    if-eqz v6, :cond_2

    .line 85
    const/4 v6, 0x1

    invoke-interface {v3, v6}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 86
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "already own something from season "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 92
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    goto :goto_0

    :cond_2
    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    .line 95
    if-nez v5, :cond_3

    .line 96
    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "don\'t own anything from show "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-static {v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 92
    :catchall_0
    move-exception v6

    invoke-interface {v3}, Landroid/database/Cursor;->close()V

    throw v6

    .line 100
    :cond_3
    invoke-direct {p0, v0, v2}, Lcom/google/android/videos/NewSeasonNotificationService;->getSeasonAsset(Ljava/lang/String;Ljava/lang/String;)Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v1

    .line 101
    .local v1, "season":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-direct {p0, v0, v4, v5, v1}, Lcom/google/android/videos/NewSeasonNotificationService;->showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V

    goto :goto_0
.end method

.method private handleView(Landroid/content/Intent;)V
    .locals 4
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 67
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "show_id"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    .line 68
    .local v1, "showId":Ljava/lang/String;
    invoke-virtual {p1}, Landroid/content/Intent;->getExtras()Landroid/os/Bundle;

    move-result-object v2

    const-string v3, "account"

    invoke-virtual {v2, v3}, Landroid/os/Bundle;->getString(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    .line 69
    .local v0, "account":Ljava/lang/String;
    const/16 v2, 0x16

    invoke-static {p0, v1, v0, v2}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 71
    return-void
.end method

.method private showNotification(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;)V
    .locals 6
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "showTitle"    # Ljava/lang/String;
    .param p4, "season"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    const/4 v5, 0x1

    .line 107
    const-string v3, "notification"

    invoke-virtual {p0, v3}, Lcom/google/android/videos/NewSeasonNotificationService;->getSystemService(Ljava/lang/String;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/app/NotificationManager;

    .line 109
    .local v2, "manager":Landroid/app/NotificationManager;
    new-instance v3, Landroid/support/v4/app/NotificationCompat$Builder;

    invoke-direct {v3, p0}, Landroid/support/v4/app/NotificationCompat$Builder;-><init>(Landroid/content/Context;)V

    invoke-virtual {v3, p3}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentTitle(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-direct {p0, p4}, Lcom/google/android/videos/NewSeasonNotificationService;->getNotificationText(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Ljava/lang/String;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentText(Ljava/lang/CharSequence;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-virtual {v3, v5}, Landroid/support/v4/app/NotificationCompat$Builder;->setAutoCancel(Z)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    const v4, 0x7f0200d8

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setSmallIcon(I)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v3

    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/NewSeasonNotificationService;->createViewIntent(Ljava/lang/String;Ljava/lang/String;)Landroid/app/PendingIntent;

    move-result-object v4

    invoke-virtual {v3, v4}, Landroid/support/v4/app/NotificationCompat$Builder;->setContentIntent(Landroid/app/PendingIntent;)Landroid/support/v4/app/NotificationCompat$Builder;

    move-result-object v0

    .line 116
    .local v0, "builder":Landroid/support/v4/app/NotificationCompat$Builder;
    new-instance v1, Lcom/google/android/videos/NewSeasonNotificationService$1;

    invoke-direct {v1, p0, v0, v2, p2}, Lcom/google/android/videos/NewSeasonNotificationService$1;-><init>(Lcom/google/android/videos/NewSeasonNotificationService;Landroid/support/v4/app/NotificationCompat$Builder;Landroid/app/NotificationManager;Ljava/lang/String;)V

    .line 131
    .local v1, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Landroid/graphics/Bitmap;>;"
    iget-object v3, p0, Lcom/google/android/videos/NewSeasonNotificationService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    invoke-virtual {v3}, Lcom/google/android/videos/VideosGlobals;->getPosterStore()Lcom/google/android/videos/store/PosterStore;

    move-result-object v3

    invoke-virtual {v3, v5, p2, v1}, Lcom/google/android/videos/store/PosterStore;->getImage(ILjava/lang/String;Lcom/google/android/videos/async/Callback;)V

    .line 132
    return-void
.end method


# virtual methods
.method public onCreate()V
    .locals 1

    .prologue
    .line 52
    invoke-super {p0}, Landroid/app/IntentService;->onCreate()V

    .line 53
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/NewSeasonNotificationService;->videosGlobals:Lcom/google/android/videos/VideosGlobals;

    .line 54
    return-void
.end method

.method protected onHandleIntent(Landroid/content/Intent;)V
    .locals 2
    .param p1, "intent"    # Landroid/content/Intent;

    .prologue
    .line 58
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Got intent "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->i(Ljava/lang/String;)V

    .line 59
    const-string v0, "com.google.android.videos.NOTIFY_NEW_SEASON"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 60
    invoke-direct {p0, p1}, Lcom/google/android/videos/NewSeasonNotificationService;->handleNotify(Landroid/content/Intent;)V

    .line 64
    :cond_0
    :goto_0
    return-void

    .line 61
    :cond_1
    const-string v0, "android.intent.action.VIEW"

    invoke-virtual {p1}, Landroid/content/Intent;->getAction()Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 62
    invoke-direct {p0, p1}, Lcom/google/android/videos/NewSeasonNotificationService;->handleView(Landroid/content/Intent;)V

    goto :goto_0
.end method
