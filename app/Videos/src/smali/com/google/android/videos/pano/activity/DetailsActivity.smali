.class public abstract Lcom/google/android/videos/pano/activity/DetailsActivity;
.super Landroid/support/v4/app/FragmentActivity;
.source "DetailsActivity.java"

# interfaces
.implements Landroid/support/v17/leanback/widget/OnItemViewClickedListener;


# instance fields
.field private backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 23
    invoke-direct {p0}, Landroid/support/v4/app/FragmentActivity;-><init>()V

    return-void
.end method


# virtual methods
.method protected abstract getObjectAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;
.end method

.method protected onCreate(Landroid/os/Bundle;)V
    .locals 5
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 33
    invoke-super {p0, p1}, Landroid/support/v4/app/FragmentActivity;->onCreate(Landroid/os/Bundle;)V

    .line 35
    invoke-static {p0}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v1

    .line 37
    .local v1, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    const-string v3, "details_fragment"

    invoke-virtual {v2, v3}, Landroid/support/v4/app/FragmentManager;->findFragmentByTag(Ljava/lang/String;)Landroid/support/v4/app/Fragment;

    move-result-object v2

    if-nez v2, :cond_0

    .line 38
    new-instance v0, Landroid/support/v17/leanback/app/DetailsSupportFragment;

    invoke-direct {v0}, Landroid/support/v17/leanback/app/DetailsSupportFragment;-><init>()V

    .line 39
    .local v0, "fragment":Landroid/support/v17/leanback/app/DetailsSupportFragment;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->getObjectAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v2

    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/app/DetailsSupportFragment;->setAdapter(Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    .line 40
    invoke-virtual {v0, p0}, Landroid/support/v17/leanback/app/DetailsSupportFragment;->setOnItemViewClickedListener(Landroid/support/v17/leanback/widget/OnItemViewClickedListener;)V

    .line 41
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->getSupportFragmentManager()Landroid/support/v4/app/FragmentManager;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentManager;->beginTransaction()Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    const v3, 0x1020002

    const-string v4, "details_fragment"

    invoke-virtual {v2, v3, v0, v4}, Landroid/support/v4/app/FragmentTransaction;->replace(ILandroid/support/v4/app/Fragment;Ljava/lang/String;)Landroid/support/v4/app/FragmentTransaction;

    move-result-object v2

    invoke-virtual {v2}, Landroid/support/v4/app/FragmentTransaction;->commit()I

    .line 46
    .end local v0    # "fragment":Landroid/support/v17/leanback/app/DetailsSupportFragment;
    :cond_0
    new-instance v2, Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v3

    invoke-interface {v3}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getControllableBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v3

    invoke-direct {v2, p0, v3}, Lcom/google/android/videos/pano/ui/BackgroundHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/async/Requester;)V

    iput-object v2, p0, Lcom/google/android/videos/pano/activity/DetailsActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    .line 48
    return-void
.end method

.method protected onDestroy()V
    .locals 1

    .prologue
    .line 52
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/DetailsActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->release()V

    .line 53
    invoke-super {p0}, Landroid/support/v4/app/FragmentActivity;->onDestroy()V

    .line 54
    return-void
.end method

.method public onItemClicked(Landroid/support/v17/leanback/widget/Presenter$ViewHolder;Ljava/lang/Object;Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;Landroid/support/v17/leanback/widget/Row;)V
    .locals 1
    .param p1, "itemViewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;
    .param p2, "item"    # Ljava/lang/Object;
    .param p3, "rowViewHolder"    # Landroid/support/v17/leanback/widget/RowPresenter$ViewHolder;
    .param p4, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 59
    instance-of v0, p2, Lcom/google/android/videos/pano/model/Item;

    if-eqz v0, :cond_0

    .line 60
    check-cast p2, Lcom/google/android/videos/pano/model/Item;

    .end local p2    # "item":Ljava/lang/Object;
    invoke-virtual {p2, p0, p1}, Lcom/google/android/videos/pano/model/Item;->onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V

    .line 62
    :cond_0
    return-void
.end method

.method public onSearchRequested()Z
    .locals 1

    .prologue
    .line 66
    invoke-static {p0}, Lcom/google/android/videos/utils/PlayStoreUtil;->startPanoSearch(Landroid/app/Activity;)V

    .line 67
    const/4 v0, 0x1

    return v0
.end method

.method protected setBackgroundUri(Ljava/lang/String;)V
    .locals 1
    .param p1, "uri"    # Ljava/lang/String;

    .prologue
    .line 71
    iget-object v0, p0, Lcom/google/android/videos/pano/activity/DetailsActivity;->backgroundHelper:Lcom/google/android/videos/pano/ui/BackgroundHelper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/BackgroundHelper;->setBackgroundUri(Ljava/lang/String;)V

    .line 72
    return-void
.end method

.method public setSelectedRowSmooth(Landroid/support/v17/leanback/widget/Row;)V
    .locals 4
    .param p1, "row"    # Landroid/support/v17/leanback/widget/Row;

    .prologue
    .line 80
    const v3, 0x7f0f0167

    invoke-virtual {p0, v3}, Lcom/google/android/videos/pano/activity/DetailsActivity;->findViewById(I)Landroid/view/View;

    move-result-object v1

    check-cast v1, Landroid/support/v17/leanback/widget/VerticalGridView;

    .line 82
    .local v1, "gridView":Landroid/support/v17/leanback/widget/VerticalGridView;
    invoke-virtual {p0}, Lcom/google/android/videos/pano/activity/DetailsActivity;->getObjectAdapter()Landroid/support/v17/leanback/widget/ObjectAdapter;

    move-result-object v0

    .line 83
    .local v0, "adapter":Landroid/support/v17/leanback/widget/ObjectAdapter;
    const/4 v2, 0x0

    .local v2, "i":I
    :goto_0
    invoke-virtual {v0}, Landroid/support/v17/leanback/widget/ObjectAdapter;->size()I

    move-result v3

    if-ge v2, v3, :cond_0

    .line 84
    invoke-virtual {v0, v2}, Landroid/support/v17/leanback/widget/ObjectAdapter;->get(I)Ljava/lang/Object;

    move-result-object v3

    if-ne v3, p1, :cond_1

    .line 85
    invoke-virtual {v1, v2}, Landroid/support/v17/leanback/widget/VerticalGridView;->setSelectedPositionSmooth(I)V

    .line 89
    :cond_0
    return-void

    .line 83
    :cond_1
    add-int/lit8 v2, v2, 0x1

    goto :goto_0
.end method
