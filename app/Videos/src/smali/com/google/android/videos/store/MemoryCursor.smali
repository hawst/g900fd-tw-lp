.class public Lcom/google/android/videos/store/MemoryCursor;
.super Landroid/database/AbstractCursor;
.source "MemoryCursor.java"


# instance fields
.field private final columnCount:I

.field private final columnNames:[Ljava/lang/String;

.field private data:[Ljava/lang/Object;

.field private rowCount:I


# direct methods
.method public constructor <init>([Ljava/lang/String;I)V
    .locals 1
    .param p1, "columnNames"    # [Ljava/lang/String;
    .param p2, "initialCapacity"    # I

    .prologue
    .line 32
    invoke-direct {p0}, Landroid/database/AbstractCursor;-><init>()V

    .line 22
    const/4 v0, 0x0

    iput v0, p0, Lcom/google/android/videos/store/MemoryCursor;->rowCount:I

    .line 33
    iput-object p1, p0, Lcom/google/android/videos/store/MemoryCursor;->columnNames:[Ljava/lang/String;

    .line 34
    array-length v0, p1

    iput v0, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    .line 36
    const/4 v0, 0x1

    if-ge p2, v0, :cond_0

    .line 37
    const/4 p2, 0x1

    .line 40
    :cond_0
    iget v0, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    mul-int/2addr v0, p2

    new-array v0, v0, [Ljava/lang/Object;

    iput-object v0, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    .line 41
    return-void
.end method

.method private ensureCapacity(I)V
    .locals 5
    .param p1, "size"    # I

    .prologue
    const/4 v4, 0x0

    .line 90
    iget-object v2, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    array-length v2, v2

    if-le p1, v2, :cond_1

    .line 91
    iget-object v1, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    .line 92
    .local v1, "oldData":[Ljava/lang/Object;
    iget-object v2, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    array-length v2, v2

    mul-int/lit8 v0, v2, 0x2

    .line 93
    .local v0, "newSize":I
    if-ge v0, p1, :cond_0

    .line 94
    move v0, p1

    .line 96
    :cond_0
    new-array v2, v0, [Ljava/lang/Object;

    iput-object v2, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    .line 97
    iget-object v2, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    array-length v3, v1

    invoke-static {v1, v4, v2, v4, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 99
    .end local v0    # "newSize":I
    .end local v1    # "oldData":[Ljava/lang/Object;
    :cond_1
    return-void
.end method

.method private get(I)Ljava/lang/Object;
    .locals 3
    .param p1, "column"    # I

    .prologue
    .line 57
    if-ltz p1, :cond_0

    iget v0, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    if-lt p1, v0, :cond_1

    .line 58
    :cond_0
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "Requested column: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ", # of columns: "

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 61
    :cond_1
    iget v0, p0, Lcom/google/android/videos/store/MemoryCursor;->mPos:I

    if-gez v0, :cond_2

    .line 62
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "Before first row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 64
    :cond_2
    iget v0, p0, Lcom/google/android/videos/store/MemoryCursor;->mPos:I

    iget v1, p0, Lcom/google/android/videos/store/MemoryCursor;->rowCount:I

    if-lt v0, v1, :cond_3

    .line 65
    new-instance v0, Landroid/database/CursorIndexOutOfBoundsException;

    const-string v1, "After last row."

    invoke-direct {v0, v1}, Landroid/database/CursorIndexOutOfBoundsException;-><init>(Ljava/lang/String;)V

    throw v0

    .line 67
    :cond_3
    iget-object v0, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    iget v1, p0, Lcom/google/android/videos/store/MemoryCursor;->mPos:I

    iget v2, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    mul-int/2addr v1, v2

    add-int/2addr v1, p1

    aget-object v0, v0, v1

    return-object v0
.end method


# virtual methods
.method public addRow([Ljava/lang/Object;)V
    .locals 4
    .param p1, "columnValues"    # [Ljava/lang/Object;

    .prologue
    .line 78
    array-length v1, p1

    iget v2, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    if-eq v1, v2, :cond_0

    .line 79
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "columnNames.length = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    iget v3, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    const-string v3, ", columnValues.length = "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    array-length v3, p1

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1

    .line 83
    :cond_0
    iget v1, p0, Lcom/google/android/videos/store/MemoryCursor;->rowCount:I

    add-int/lit8 v2, v1, 0x1

    iput v2, p0, Lcom/google/android/videos/store/MemoryCursor;->rowCount:I

    iget v2, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    mul-int v0, v1, v2

    .line 84
    .local v0, "start":I
    iget v1, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    add-int/2addr v1, v0

    invoke-direct {p0, v1}, Lcom/google/android/videos/store/MemoryCursor;->ensureCapacity(I)V

    .line 85
    const/4 v1, 0x0

    iget-object v2, p0, Lcom/google/android/videos/store/MemoryCursor;->data:[Ljava/lang/Object;

    iget v3, p0, Lcom/google/android/videos/store/MemoryCursor;->columnCount:I

    invoke-static {p1, v1, v2, v0, v3}, Ljava/lang/System;->arraycopy(Ljava/lang/Object;ILjava/lang/Object;II)V

    .line 86
    return-void
.end method

.method public getBlob(I)[B
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 184
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 185
    .local v0, "value":Ljava/lang/Object;
    check-cast v0, [B

    .end local v0    # "value":Ljava/lang/Object;
    check-cast v0, [B

    return-object v0
.end method

.method public getColumnNames()[Ljava/lang/String;
    .locals 1

    .prologue
    .line 110
    iget-object v0, p0, Lcom/google/android/videos/store/MemoryCursor;->columnNames:[Ljava/lang/String;

    return-object v0
.end method

.method public getCount()I
    .locals 1

    .prologue
    .line 105
    invoke-virtual {p0}, Lcom/google/android/videos/store/MemoryCursor;->isClosed()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    return v0

    :cond_0
    iget v0, p0, Lcom/google/android/videos/store/MemoryCursor;->rowCount:I

    goto :goto_0
.end method

.method public getDouble(I)D
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 172
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 173
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 174
    const-wide/16 v2, 0x0

    .line 179
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return-wide v2

    .line 176
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 177
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->doubleValue()D

    move-result-wide v2

    goto :goto_0

    .line 179
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Double;->parseDouble(Ljava/lang/String;)D

    move-result-wide v2

    goto :goto_0
.end method

.method public getFloat(I)F
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 160
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 161
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 162
    const/4 v1, 0x0

    .line 167
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return v1

    .line 164
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 165
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->floatValue()F

    move-result v1

    goto :goto_0

    .line 167
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Float;->parseFloat(Ljava/lang/String;)F

    move-result v1

    goto :goto_0
.end method

.method public getInt(I)I
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 136
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 137
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 138
    const/4 v1, 0x0

    .line 143
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return v1

    .line 140
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 141
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->intValue()I

    move-result v1

    goto :goto_0

    .line 143
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Integer;->parseInt(Ljava/lang/String;)I

    move-result v1

    goto :goto_0
.end method

.method public getLong(I)J
    .locals 4
    .param p1, "column"    # I

    .prologue
    .line 148
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 149
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 150
    const-wide/16 v2, 0x0

    .line 155
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return-wide v2

    .line 152
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 153
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->longValue()J

    move-result-wide v2

    goto :goto_0

    .line 155
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Long;->parseLong(Ljava/lang/String;)J

    move-result-wide v2

    goto :goto_0
.end method

.method public getShort(I)S
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 124
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 125
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 126
    const/4 v1, 0x0

    .line 131
    .end local v0    # "value":Ljava/lang/Object;
    :goto_0
    return v1

    .line 128
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_0
    instance-of v1, v0, Ljava/lang/Number;

    if-eqz v1, :cond_1

    .line 129
    check-cast v0, Ljava/lang/Number;

    .end local v0    # "value":Ljava/lang/Object;
    invoke-virtual {v0}, Ljava/lang/Number;->shortValue()S

    move-result v1

    goto :goto_0

    .line 131
    .restart local v0    # "value":Ljava/lang/Object;
    :cond_1
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Ljava/lang/Short;->parseShort(Ljava/lang/String;)S

    move-result v1

    goto :goto_0
.end method

.method public getString(I)Ljava/lang/String;
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 115
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 116
    .local v0, "value":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 117
    const/4 v1, 0x0

    .line 119
    :goto_0
    return-object v1

    :cond_0
    invoke-virtual {v0}, Ljava/lang/Object;->toString()Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method public getType(I)I
    .locals 2
    .param p1, "column"    # I

    .prologue
    .line 190
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    .line 191
    .local v0, "obj":Ljava/lang/Object;
    if-nez v0, :cond_0

    .line 192
    const/4 v1, 0x0

    .line 201
    :goto_0
    return v1

    .line 193
    :cond_0
    instance-of v1, v0, [B

    if-eqz v1, :cond_1

    .line 194
    const/4 v1, 0x4

    goto :goto_0

    .line 195
    :cond_1
    instance-of v1, v0, Ljava/lang/Float;

    if-nez v1, :cond_2

    instance-of v1, v0, Ljava/lang/Double;

    if-eqz v1, :cond_3

    .line 196
    :cond_2
    const/4 v1, 0x2

    goto :goto_0

    .line 197
    :cond_3
    instance-of v1, v0, Ljava/lang/Long;

    if-nez v1, :cond_4

    instance-of v1, v0, Ljava/lang/Integer;

    if-nez v1, :cond_4

    instance-of v1, v0, Ljava/lang/Short;

    if-nez v1, :cond_4

    instance-of v1, v0, Ljava/lang/Byte;

    if-eqz v1, :cond_5

    .line 199
    :cond_4
    const/4 v1, 0x1

    goto :goto_0

    .line 201
    :cond_5
    const/4 v1, 0x3

    goto :goto_0
.end method

.method public isNull(I)Z
    .locals 1
    .param p1, "column"    # I

    .prologue
    .line 207
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/MemoryCursor;->get(I)Ljava/lang/Object;

    move-result-object v0

    if-nez v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method
