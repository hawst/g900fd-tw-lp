.class public Lcom/google/android/videos/store/SyncTaskManager$SyncException;
.super Ljava/lang/Exception;
.source "SyncTaskManager.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/SyncTaskManager;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SyncException"
.end annotation


# instance fields
.field public final errorLevel:I


# direct methods
.method public constructor <init>(ILjava/lang/Exception;)V
    .locals 1
    .param p1, "errorLevel"    # I
    .param p2, "cause"    # Ljava/lang/Exception;

    .prologue
    .line 54
    const-string v0, "An error occurred during a sync."

    invoke-direct {p0, v0, p2}, Ljava/lang/Exception;-><init>(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 55
    iput p1, p0, Lcom/google/android/videos/store/SyncTaskManager$SyncException;->errorLevel:I

    .line 56
    return-void
.end method
