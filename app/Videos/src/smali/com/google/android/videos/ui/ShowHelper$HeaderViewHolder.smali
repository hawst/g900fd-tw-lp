.class final Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "ShowHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Landroid/widget/AdapterView$OnItemSelectedListener;
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/ShowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "HeaderViewHolder"
.end annotation


# instance fields
.field private final broadcastersView:Landroid/widget/TextView;

.field private final headerExtension:Landroid/view/View;

.field private final headerMainView:Landroid/view/View;

.field private final moreEpisodesButton:Landroid/view/View;

.field private final noContentView:Landroid/view/View;

.field private final seasonSelectSpinner:Landroid/widget/Spinner;

.field private final singleSeasonTitleView:Landroid/widget/TextView;

.field private final spinnerAdapter:Landroid/widget/ArrayAdapter;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/widget/ArrayAdapter",
            "<",
            "Ljava/lang/String;",
            ">;"
        }
    .end annotation
.end field

.field final synthetic this$0:Lcom/google/android/videos/ui/ShowHelper;

.field private final titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/ShowHelper;Landroid/view/View;)V
    .locals 3
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 556
    iput-object p1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    .line 557
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 558
    new-instance v0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder$1;

    invoke-virtual {p2}, Landroid/view/View;->getContext()Landroid/content/Context;

    move-result-object v1

    const v2, 0x7f0400bb

    invoke-direct {v0, p0, v1, v2, p1}, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder$1;-><init>(Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;Landroid/content/Context;ILcom/google/android/videos/ui/ShowHelper;)V

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    .line 568
    const v0, 0x7f0f01fb

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->headerMainView:Landroid/view/View;

    .line 569
    const v0, 0x7f0f0201

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->headerExtension:Landroid/view/View;

    .line 570
    const v0, 0x7f0f01fe

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/Spinner;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    .line 571
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v0, v1}, Landroid/widget/Spinner;->setAdapter(Landroid/widget/SpinnerAdapter;)V

    .line 572
    const v0, 0x7f0f00a8

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->titleView:Landroid/widget/TextView;

    .line 573
    const v0, 0x7f0f01fc

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->broadcastersView:Landroid/widget/TextView;

    .line 574
    const v0, 0x7f0f0200

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->moreEpisodesButton:Landroid/view/View;

    .line 575
    const v0, 0x7f0f01ff

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->singleSeasonTitleView:Landroid/widget/TextView;

    .line 576
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->moreEpisodesButton:Landroid/view/View;

    invoke-virtual {v0, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 577
    const v0, 0x7f0f018e

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->noContentView:Landroid/view/View;

    .line 578
    new-instance v0, Lcom/google/android/videos/ui/NoContentViewHolder;

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->noContentView:Landroid/view/View;

    invoke-direct {v0, v1}, Lcom/google/android/videos/ui/NoContentViewHolder;-><init>(Landroid/view/View;)V

    invoke-static {v0}, Lcom/google/android/videos/ui/NoDownloadedContentFlow;->bindView(Lcom/google/android/videos/ui/NoContentViewHolder;)V

    .line 579
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 7

    .prologue
    const/16 v5, 0x8

    const/4 v4, 0x4

    const/4 v3, 0x0

    .line 584
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->headerMainView:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->headerBackgroundColor:I
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowHelper;->access$200(Lcom/google/android/videos/ui/ShowHelper;)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 585
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->headerExtension:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->headerBackgroundColor:I
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowHelper;->access$200(Lcom/google/android/videos/ui/ShowHelper;)I

    move-result v6

    invoke-virtual {v2, v6}, Landroid/view/View;->setBackgroundColor(I)V

    .line 586
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->titleView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->title:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowHelper;->access$300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 587
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->broadcastersView:Landroid/widget/TextView;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->broadcasters:Ljava/lang/String;
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowHelper;->access$400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v2, v6}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 588
    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->moreEpisodesButton:Landroid/view/View;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->suggestionsEnabled:Z
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$500(Lcom/google/android/videos/ui/ShowHelper;)Z

    move-result v2

    if-eqz v2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->ownedCompleteShow:Z
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$600(Lcom/google/android/videos/ui/ShowHelper;)Z

    move-result v2

    if-nez v2, :cond_0

    move v2, v3

    :goto_0
    invoke-virtual {v6, v2}, Landroid/view/View;->setVisibility(I)V

    .line 591
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$700(Lcom/google/android/videos/ui/ShowHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2}, Ljava/util/ArrayList;->size()I

    move-result v1

    .line 592
    .local v1, "seasonCount":I
    if-nez v1, :cond_2

    .line 593
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->noContentView:Landroid/view/View;

    iget-object v6, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->downloadedOnlyManager:Lcom/google/android/videos/utils/DownloadedOnlyManager;
    invoke-static {v6}, Lcom/google/android/videos/ui/ShowHelper;->access$800(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/utils/DownloadedOnlyManager;

    move-result-object v6

    invoke-virtual {v6}, Lcom/google/android/videos/utils/DownloadedOnlyManager;->isDownloadedOnly()Z

    move-result v6

    if-eqz v6, :cond_1

    :goto_1
    invoke-virtual {v2, v3}, Landroid/view/View;->setVisibility(I)V

    .line 595
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->singleSeasonTitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 596
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 618
    :goto_2
    return-void

    .end local v1    # "seasonCount":I
    :cond_0
    move v2, v4

    .line 588
    goto :goto_0

    .restart local v1    # "seasonCount":I
    :cond_1
    move v3, v5

    .line 593
    goto :goto_1

    .line 597
    :cond_2
    const/4 v2, 0x1

    if-ne v1, v2, :cond_3

    .line 598
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->noContentView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 599
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->singleSeasonTitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v3}, Landroid/widget/TextView;->setVisibility(I)V

    .line 600
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v4}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 601
    iget-object v4, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->singleSeasonTitleView:Landroid/widget/TextView;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$700(Lcom/google/android/videos/ui/ShowHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v3}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    iget-object v2, v2, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->seasonTitle:Ljava/lang/String;

    invoke-virtual {v4, v2}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    goto :goto_2

    .line 603
    :cond_3
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->noContentView:Landroid/view/View;

    invoke-virtual {v2, v5}, Landroid/view/View;->setVisibility(I)V

    .line 604
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->singleSeasonTitleView:Landroid/widget/TextView;

    invoke-virtual {v2, v4}, Landroid/widget/TextView;->setVisibility(I)V

    .line 605
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setVisibility(I)V

    .line 607
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    .line 609
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    invoke-virtual {v2}, Landroid/widget/ArrayAdapter;->clear()V

    .line 610
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_3
    if-ge v0, v1, :cond_4

    .line 611
    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->spinnerAdapter:Landroid/widget/ArrayAdapter;

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->seasonDataList:Ljava/util/ArrayList;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$700(Lcom/google/android/videos/ui/ShowHelper;)Ljava/util/ArrayList;

    move-result-object v2

    invoke-virtual {v2, v0}, Ljava/util/ArrayList;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    iget-object v2, v2, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->seasonTitle:Ljava/lang/String;

    invoke-virtual {v3, v2}, Landroid/widget/ArrayAdapter;->add(Ljava/lang/Object;)V

    .line 610
    add-int/lit8 v0, v0, 0x1

    goto :goto_3

    .line 614
    :cond_4
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    iget-object v3, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I
    invoke-static {v3}, Lcom/google/android/videos/ui/ShowHelper;->access$900(Lcom/google/android/videos/ui/ShowHelper;)I

    move-result v3

    invoke-virtual {v2, v3}, Landroid/widget/Spinner;->setSelection(I)V

    .line 616
    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->seasonSelectSpinner:Landroid/widget/Spinner;

    invoke-virtual {v2, p0}, Landroid/widget/Spinner;->setOnItemSelectedListener(Landroid/widget/AdapterView$OnItemSelectedListener;)V

    goto :goto_2
.end method

.method public onClick(Landroid/view/View;)V
    .locals 4
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 636
    iget-object v0, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->activity:Lcom/google/android/videos/activity/ShowActivity;
    invoke-static {v0}, Lcom/google/android/videos/ui/ShowHelper;->access$1200(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/activity/ShowActivity;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->showId:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1300(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v1

    iget-object v2, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->account:Ljava/lang/String;
    invoke-static {v2}, Lcom/google/android/videos/ui/ShowHelper;->access$1400(Lcom/google/android/videos/ui/ShowHelper;)Ljava/lang/String;

    move-result-object v2

    const/16 v3, 0x14

    invoke-static {v0, v1, v2, v3}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewShowDetails(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;I)V

    .line 637
    return-void
.end method

.method public onItemSelected(Landroid/widget/AdapterView;Landroid/view/View;IJ)V
    .locals 3
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I
    .param p4, "id"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;",
            "Landroid/view/View;",
            "IJ)V"
        }
    .end annotation

    .prologue
    .line 622
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # invokes: Lcom/google/android/videos/ui/ShowHelper;->getSeasonData(I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    invoke-static {v1, p3}, Lcom/google/android/videos/ui/ShowHelper;->access$1000(Lcom/google/android/videos/ui/ShowHelper;I)Lcom/google/android/videos/ui/ShowHelper$SeasonData;

    move-result-object v0

    .line 623
    .local v0, "data":Lcom/google/android/videos/ui/ShowHelper$SeasonData;
    if-eqz v0, :cond_0

    .line 624
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # setter for: Lcom/google/android/videos/ui/ShowHelper;->currentSeasonIndex:I
    invoke-static {v1, p3}, Lcom/google/android/videos/ui/ShowHelper;->access$902(Lcom/google/android/videos/ui/ShowHelper;I)I

    .line 625
    iget-object v1, p0, Lcom/google/android/videos/ui/ShowHelper$HeaderViewHolder;->this$0:Lcom/google/android/videos/ui/ShowHelper;

    # getter for: Lcom/google/android/videos/ui/ShowHelper;->seasonFlow:Lcom/google/android/videos/ui/ItemsWithHeadingFlow;
    invoke-static {v1}, Lcom/google/android/videos/ui/ShowHelper;->access$1100(Lcom/google/android/videos/ui/ShowHelper;)Lcom/google/android/videos/ui/ItemsWithHeadingFlow;

    move-result-object v1

    iget-object v2, v0, Lcom/google/android/videos/ui/ShowHelper$SeasonData;->dataSource:Lcom/google/android/videos/adapter/EpisodesDataSource$EpisodesSubRangeDataSource;

    invoke-virtual {v1, v2}, Lcom/google/android/videos/ui/ItemsWithHeadingFlow;->setDataSource(Lcom/google/android/videos/adapter/DataSource;)V

    .line 627
    :cond_0
    return-void
.end method

.method public onNothingSelected(Landroid/widget/AdapterView;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/widget/AdapterView",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 632
    .local p1, "parent":Landroid/widget/AdapterView;, "Landroid/widget/AdapterView<*>;"
    return-void
.end method
