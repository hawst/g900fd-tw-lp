.class public interface abstract Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "SubQuery"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForOnDevice;,
        Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForSingleShow;
    }
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 703
    const/16 v0, 0x18

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "video_id"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "episode_number_text"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "title"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "description"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "(pinned IS NOT NULL AND pinned > 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "(license_type IS NOT NULL AND license_type != 0)"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "pinning_status"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "pinning_status_reason"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "pinning_drm_error_code"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "download_bytes_downloaded"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "last_watched_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "season_id"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "season_title"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "season_long_title"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "account IS NOT NULL"

    aput-object v2, v0, v1

    const/16 v1, 0x10

    const-string v2, "screenshot_uri"

    aput-object v2, v0, v1

    const/16 v1, 0x11

    const-string v2, "purchase_status = 2 AND merged_expiration_timestamp > ?"

    aput-object v2, v0, v1

    const/16 v1, 0x12

    const-string v2, "merged_expiration_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x13

    const-string v2, "resume_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x14

    const-string v2, "duration_seconds"

    aput-object v2, v0, v1

    const/16 v1, 0x15

    const-string v2, "user_assets_account"

    aput-object v2, v0, v1

    const/16 v1, 0x16

    const-string v2, "publish_timestamp"

    aput-object v2, v0, v1

    const/16 v1, 0x17

    const-string v2, "purchase_type"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
