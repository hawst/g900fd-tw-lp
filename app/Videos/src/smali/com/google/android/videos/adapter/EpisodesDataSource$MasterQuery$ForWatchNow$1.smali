.class final Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;
.super Ljava/lang/Object;
.source "EpisodesDataSource.java"

# interfaces
.implements Lcom/google/android/videos/store/PurchaseStore$SubRequestCreator;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow;->createRequest(Ljava/lang/String;ZJZZI)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x8
    name = null
.end annotation


# instance fields
.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$allowUnpurchased:Z

.field final synthetic val$downloadedOnly:Z

.field final synthetic val$excludeFinishedLastWatched:Z

.field final synthetic val$maxEpisodesPerShow:I


# direct methods
.method constructor <init>(Ljava/lang/String;IZZZ)V
    .locals 0

    .prologue
    .line 597
    iput-object p1, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    iput p2, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$maxEpisodesPerShow:I

    iput-boolean p3, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    iput-boolean p4, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$excludeFinishedLastWatched:Z

    iput-boolean p5, p0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$allowUnpurchased:Z

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public createSubRequest(Landroid/database/Cursor;)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 16
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 600
    const/16 v1, 0xa

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v1

    if-nez v1, :cond_0

    .line 601
    const/4 v1, 0x0

    .line 644
    :goto_0
    return-object v1

    .line 605
    :cond_0
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->isNull(I)Z

    move-result v1

    if-eqz v1, :cond_1

    .line 607
    const/16 v1, 0xb

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 608
    .local v3, "seasonSeqno":I
    const/16 v1, 0xc

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 609
    .local v4, "startEpisodeSeqno":I
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$maxEpisodesPerShow:I

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;->createRequestStartingFromEpisodeSeqno(Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    goto :goto_0

    .line 613
    .end local v3    # "seasonSeqno":I
    .end local v4    # "startEpisodeSeqno":I
    :cond_1
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$excludeFinishedLastWatched:Z

    if-eqz v1, :cond_2

    const/16 v1, 0xf

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v1

    if-eqz v1, :cond_2

    const/4 v15, 0x1

    .line 615
    .local v15, "skipLastWatched":Z
    :goto_1
    const/4 v1, 0x4

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v7

    .line 616
    .local v7, "nextEpisodeId":Ljava/lang/String;
    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_4

    const/4 v1, 0x5

    move-object/from16 v0, p1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v1

    if-nez v1, :cond_4

    .line 620
    if-eqz v15, :cond_3

    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    move-object/from16 v0, p0

    iget-boolean v2, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$allowUnpurchased:Z

    move-object/from16 v0, p0

    iget-boolean v5, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    invoke-static {v1, v7, v2, v5}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;->createRequestForNextEpisodeOnly(Ljava/lang/String;Ljava/lang/String;ZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    goto :goto_0

    .line 613
    .end local v7    # "nextEpisodeId":Ljava/lang/String;
    .end local v15    # "skipLastWatched":Z
    :cond_2
    const/4 v15, 0x0

    goto :goto_1

    .line 620
    .restart local v7    # "nextEpisodeId":Ljava/lang/String;
    .restart local v15    # "skipLastWatched":Z
    :cond_3
    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    const/4 v1, 0x3

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    move-object/from16 v0, p0

    iget v8, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$maxEpisodesPerShow:I

    move-object/from16 v0, p0

    iget-boolean v9, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$allowUnpurchased:Z

    move-object/from16 v0, p0

    iget-boolean v10, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    invoke-static/range {v5 .. v10}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;->createRequestForLastWatchedAndNextEpisode(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IZZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    goto/16 :goto_0

    .line 628
    :cond_4
    if-eqz v15, :cond_5

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-eqz v1, :cond_5

    .line 629
    const/4 v1, 0x0

    goto/16 :goto_0

    .line 633
    :cond_5
    const/16 v1, 0xe

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v3

    .line 634
    .restart local v3    # "seasonSeqno":I
    const/16 v1, 0xd

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getInt(I)I

    move-result v4

    .line 635
    .restart local v4    # "startEpisodeSeqno":I
    if-eqz v15, :cond_6

    .line 636
    add-int/lit8 v4, v4, 0x1

    .line 639
    :cond_6
    move-object/from16 v0, p0

    iget-boolean v1, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$allowUnpurchased:Z

    if-eqz v1, :cond_7

    invoke-static {v7}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_7

    .line 640
    move-object/from16 v0, p0

    iget-object v8, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    const/4 v1, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    move-object/from16 v0, p0

    iget v13, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$maxEpisodesPerShow:I

    move-object/from16 v0, p0

    iget-boolean v14, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    move v10, v3

    move v11, v4

    move-object v12, v7

    invoke-static/range {v8 .. v14}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;->createRequestStartingFromEpisodeSeqnoIncludingNextEpisode(Ljava/lang/String;Ljava/lang/String;IILjava/lang/String;IZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    goto/16 :goto_0

    .line 644
    :cond_7
    move-object/from16 v0, p0

    iget-object v1, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$account:Ljava/lang/String;

    const/4 v2, 0x1

    move-object/from16 v0, p1

    invoke-interface {v0, v2}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v2

    move-object/from16 v0, p0

    iget v5, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$maxEpisodesPerShow:I

    move-object/from16 v0, p0

    iget-boolean v6, v0, Lcom/google/android/videos/adapter/EpisodesDataSource$MasterQuery$ForWatchNow$1;->val$downloadedOnly:Z

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/adapter/EpisodesDataSource$SubQuery$ForWatchNow;->createRequestStartingFromEpisodeSeqno(Ljava/lang/String;Ljava/lang/String;IIIZ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v1

    goto/16 :goto_0
.end method
