.class public Lcom/google/android/videos/store/StoryboardClient;
.super Ljava/lang/Object;
.source "StoryboardClient.java"


# instance fields
.field private final cachedImageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final imageRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final imageStorer:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private final storyboardStore:Lcom/google/android/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V
    .locals 6
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p5, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            ")V"
        }
    .end annotation

    .prologue
    .line 46
    .local p4, "syncBitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 47
    iput-object p3, p0, Lcom/google/android/videos/store/StoryboardClient;->localExecutor:Ljava/util/concurrent/Executor;

    .line 48
    invoke-static {p1}, Lcom/google/android/videos/store/StoryboardClient;->getStoryboardStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->storyboardStore:Lcom/google/android/videos/store/AbstractFileStore;

    .line 49
    invoke-static {p1}, Lcom/google/android/videos/store/StoryboardClient;->getStoryboardImageStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v3

    .line 51
    .local v3, "imageStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/store/StoryboardImageRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    const/4 v5, 0x0

    move-object v0, p2

    move-object v1, p3

    move-object v2, p4

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/store/StoryboardClient;->createImageRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/bitmap/BitmapLruCache;Z)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->imageRequester:Lcom/google/android/videos/async/Requester;

    .line 53
    const/4 v5, 0x1

    move-object v0, p2

    move-object v1, p3

    move-object v2, p4

    move-object v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/store/StoryboardClient;->createImageRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/bitmap/BitmapLruCache;Z)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->cachedImageRequester:Lcom/google/android/videos/async/Requester;

    .line 55
    invoke-static {p2, p4, v3}, Lcom/google/android/videos/store/StoryboardClient;->createImageStorer(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->imageStorer:Lcom/google/android/videos/async/Requester;

    .line 56
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/StoryboardClient;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/StoryboardClient;

    .prologue
    .line 37
    iget-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->storyboardStore:Lcom/google/android/videos/store/AbstractFileStore;

    return-object v0
.end method

.method static synthetic access$100(Ljava/io/File;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Ljava/io/File;

    .prologue
    .line 37
    invoke-static {p0}, Lcom/google/android/videos/store/StoryboardClient;->getStoryboardDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static createImageRequester(Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/bitmap/BitmapLruCache;Z)Lcom/google/android/videos/async/Requester;
    .locals 8
    .param p0, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p1, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "bitmapCache"    # Lcom/google/android/videos/bitmap/BitmapLruCache;
    .param p5, "allowNetwork"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/bitmap/BitmapLruCache;",
            "Z)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 154
    .local p2, "bitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    .local p3, "imageStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/store/StoryboardImageRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v5, Lcom/google/android/videos/store/StoryboardClient$6;

    invoke-direct {v5}, Lcom/google/android/videos/store/StoryboardClient$6;-><init>()V

    .line 163
    .local v5, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/net/Uri;>;"
    invoke-static {p3}, Lcom/google/android/videos/async/StoreCachingRequester;->create(Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/StoreCachingRequester;

    move-result-object v2

    .line 166
    .local v2, "bytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    if-eqz p5, :cond_0

    .line 168
    invoke-static {p2, v5}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v6

    .line 170
    .local v6, "syncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-static {p0, v6}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    .line 173
    .local v0, "asyncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-static {v2, v0}, Lcom/google/android/videos/async/FallbackRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/FallbackRequester;

    move-result-object v2

    .line 177
    .end local v0    # "asyncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    .end local v6    # "syncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    :cond_0
    new-instance v3, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v7, 0x1

    invoke-direct {v3, v7}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(Z)V

    .line 179
    .local v3, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {v2, v3}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v4

    .line 183
    .local v4, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/graphics/Bitmap;>;"
    invoke-static {p1, v4}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v1

    .line 185
    .local v1, "asyncRequester":Lcom/google/android/videos/async/AsyncRequester;, "Lcom/google/android/videos/async/AsyncRequester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/graphics/Bitmap;>;"
    new-instance v7, Lcom/google/android/videos/store/StoryboardClient$7;

    invoke-direct {v7, v1, p4}, Lcom/google/android/videos/store/StoryboardClient$7;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/bitmap/BitmapLruCache;)V

    return-object v7
.end method

.method private static createImageStorer(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;
    .locals 6
    .param p0, "executor"    # Ljava/util/concurrent/Executor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation

    .prologue
    .line 197
    .local p1, "bitmapBytesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/net/Uri;Lcom/google/android/videos/utils/ByteArray;>;"
    .local p2, "imageStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/store/StoryboardImageRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v3, Lcom/google/android/videos/store/StoryboardClient$8;

    invoke-direct {v3}, Lcom/google/android/videos/store/StoryboardClient$8;-><init>()V

    .line 204
    .local v3, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/net/Uri;>;"
    invoke-static {p1, v3}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/RequestConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    .line 207
    .local v2, "networkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/store/StoryboardImageRequest;Lcom/google/android/videos/utils/ByteArray;>;"
    invoke-static {p2, v2}, Lcom/google/android/videos/async/StoreCachingRequester;->create(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/StoreCachingRequester;

    move-result-object v4

    .line 211
    .local v4, "storingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Lcom/google/android/videos/utils/ByteArray;>;"
    new-instance v0, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;

    const/4 v5, 0x1

    invoke-direct {v0, v5}, Lcom/google/android/videos/bitmap/BytesToBitmapResponseConverter;-><init>(Z)V

    .line 213
    .local v0, "bytesToBitmapConverter":Lcom/google/android/videos/converter/ResponseConverter;, "Lcom/google/android/videos/converter/ResponseConverter<Lcom/google/android/videos/utils/ByteArray;Landroid/graphics/Bitmap;>;"
    invoke-static {v4, v0}, Lcom/google/android/videos/async/ConvertingRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/converter/ResponseConverter;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    .line 217
    .local v1, "convertingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/graphics/Bitmap;>;"
    invoke-static {p0, v1}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v5

    return-object v5
.end method

.method private static getStoryboardDir(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p0, "rootFileDir"    # Ljava/io/File;

    .prologue
    .line 144
    new-instance v0, Ljava/io/File;

    const-string v1, "storyboard"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getStoryboardImageStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation

    .prologue
    .line 134
    new-instance v0, Lcom/google/android/videos/store/StoryboardClient$5;

    new-instance v1, Lcom/google/android/videos/cache/ByteArrayConverter;

    invoke-direct {v1}, Lcom/google/android/videos/cache/ByteArrayConverter;-><init>()V

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/store/StoryboardClient$5;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-object v0
.end method

.method private static getStoryboardStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 2
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ">;"
        }
    .end annotation

    .prologue
    .line 122
    new-instance v0, Lcom/google/android/videos/store/StoryboardClient$4;

    const-class v1, Lcom/google/wireless/android/video/magma/proto/Storyboard;

    invoke-static {v1}, Lcom/google/android/videos/cache/ProtoConverter;->create(Ljava/lang/Class;)Lcom/google/android/videos/cache/ProtoConverter;

    move-result-object v1

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/store/StoryboardClient$4;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-object v0
.end method

.method private requestStoryboardImageInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p2, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p3, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Ljava/lang/Integer;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 77
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/store/StoryboardImageRequest;Ljava/lang/Integer;>;Landroid/graphics/Bitmap;>;"
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;>;"
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/store/StoryboardClient$1;

    invoke-direct {v1, p0, p4}, Lcom/google/android/videos/store/StoryboardClient$1;-><init>(Lcom/google/android/videos/store/StoryboardClient;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {p1, v0, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 88
    return-void
.end method


# virtual methods
.method public requestCachedStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 60
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->cachedImageRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/StoryboardClient;->requestStoryboardImageInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    .line 61
    return-void
.end method

.method public requestOfflineStoryboard(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 107
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/StoryboardClient$3;

    invoke-direct {v1, p0, p3, p1, p2}, Lcom/google/android/videos/store/StoryboardClient$3;-><init>(Lcom/google/android/videos/store/StoryboardClient;Lcom/google/android/videos/async/Callback;Ljava/lang/String;I)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 117
    return-void
.end method

.method public requestStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 65
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->imageRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/StoryboardClient;->requestStoryboardImageInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    .line 66
    return-void
.end method

.method public saveOfflineStoryboard(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;ILcom/google/android/videos/async/Callback;)V
    .locals 7
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p3, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/Storyboard;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 92
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v6, p0, Lcom/google/android/videos/store/StoryboardClient;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/videos/store/StoryboardClient$2;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/StoryboardClient$2;-><init>(Lcom/google/android/videos/store/StoryboardClient;Ljava/lang/String;ILcom/google/wireless/android/video/magma/proto/Storyboard;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 103
    return-void
.end method

.method public saveStoryboardImage(Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/store/StoryboardImageRequest;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/store/StoryboardImageRequest;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 70
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/store/StoryboardImageRequest;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/StoryboardClient;->imageStorer:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/StoryboardClient;->requestStoryboardImageInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/StoryboardImageRequest;ILcom/google/android/videos/async/Callback;)V

    .line 71
    return-void
.end method
