.class public Lcom/google/android/videos/store/SubtitlesClient;
.super Ljava/lang/Object;
.source "SubtitlesClient.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/SubtitlesClient$SubtitlesSerializingConverter;,
        Lcom/google/android/videos/store/SubtitlesClient$SubtitleTrackSerializingConverter;
    }
.end annotation


# instance fields
.field private final localExecutor:Ljava/util/concurrent/Executor;

.field private final subtitleTracksStore:Lcom/google/android/videos/store/AbstractFileStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;>;"
        }
    .end annotation
.end field

.field private final subtitlesRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;"
        }
    .end annotation
.end field

.field private final subtitlesStorer:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/utils/XmlParser;)V
    .locals 2
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p5, "xmlParser"    # Lcom/google/android/videos/utils/XmlParser;

    .prologue
    .line 50
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 51
    iput-object p3, p0, Lcom/google/android/videos/store/SubtitlesClient;->localExecutor:Ljava/util/concurrent/Executor;

    .line 52
    invoke-static {p1}, Lcom/google/android/videos/store/SubtitlesClient;->getSubtitlesStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v0

    .line 53
    .local v0, "subtitlesStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p1}, Lcom/google/android/videos/store/SubtitlesClient;->getSubtitleTracksStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitleTracksStore:Lcom/google/android/videos/store/AbstractFileStore;

    .line 54
    invoke-static {p4, p2, p3, p5, v0}, Lcom/google/android/videos/store/SubtitlesClient;->createSubtitlesRequester(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/XmlParser;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitlesRequester:Lcom/google/android/videos/async/Requester;

    .line 56
    invoke-static {p4, p2, p5, v0}, Lcom/google/android/videos/store/SubtitlesClient;->createSubtitlesStorer(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/XmlParser;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitlesStorer:Lcom/google/android/videos/async/Requester;

    .line 58
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/SubtitlesClient;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/SubtitlesClient;

    .prologue
    .line 42
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitleTracksStore:Lcom/google/android/videos/store/AbstractFileStore;

    return-object v0
.end method

.method static synthetic access$200(Ljava/io/File;)Ljava/io/File;
    .locals 1
    .param p0, "x0"    # Ljava/io/File;

    .prologue
    .line 42
    invoke-static {p0}, Lcom/google/android/videos/store/SubtitlesClient;->getSubtitlesDir(Ljava/io/File;)Ljava/io/File;

    move-result-object v0

    return-object v0
.end method

.method private static createSubtitlesRequester(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/XmlParser;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;
    .locals 7
    .param p0, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p1, "networkExecutor"    # Ljava/util/concurrent/Executor;
    .param p2, "localExecutor"    # Ljava/util/concurrent/Executor;
    .param p3, "xmlParser"    # Lcom/google/android/videos/utils/XmlParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/HttpClient;",
            "Ljava/util/concurrent/Executor;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/utils/XmlParser;",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;"
        }
    .end annotation

    .prologue
    .line 198
    .local p4, "subtitlesStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    new-instance v1, Lcom/google/android/videos/subtitles/SubtitlesConverter;

    invoke-direct {v1, p3}, Lcom/google/android/videos/subtitles/SubtitlesConverter;-><init>(Lcom/google/android/videos/utils/XmlParser;)V

    .line 199
    .local v1, "converter":Lcom/google/android/videos/subtitles/SubtitlesConverter;
    new-instance v3, Lcom/google/android/videos/store/SubtitlesClient$6;

    invoke-direct {v3, v1}, Lcom/google/android/videos/store/SubtitlesClient$6;-><init>(Lcom/google/android/videos/subtitles/SubtitlesConverter;)V

    .line 207
    .local v3, "requestConverter":Lcom/google/android/videos/converter/RequestConverter;, "Lcom/google/android/videos/converter/RequestConverter<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lorg/apache/http/client/methods/HttpUriRequest;>;"
    invoke-static {p0, v3, v1}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v5

    .line 209
    .local v5, "syncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p1, v5}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v0

    .line 212
    .local v0, "asyncNetworkRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p4}, Lcom/google/android/videos/async/StoreCachingRequester;->create(Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/StoreCachingRequester;

    move-result-object v4

    .line 215
    .local v4, "storeReadingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {v4, v0}, Lcom/google/android/videos/async/FallbackRequester;->create(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/FallbackRequester;

    move-result-object v2

    .line 217
    .local v2, "fallbackRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p2, v2}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v6

    return-object v6
.end method

.method private static createSubtitlesStorer(Lorg/apache/http/client/HttpClient;Ljava/util/concurrent/Executor;Lcom/google/android/videos/utils/XmlParser;Lcom/google/android/videos/store/AbstractFileStore;)Lcom/google/android/videos/async/Requester;
    .locals 4
    .param p0, "httpClient"    # Lorg/apache/http/client/HttpClient;
    .param p1, "executor"    # Ljava/util/concurrent/Executor;
    .param p2, "xmlParser"    # Lcom/google/android/videos/utils/XmlParser;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lorg/apache/http/client/HttpClient;",
            "Ljava/util/concurrent/Executor;",
            "Lcom/google/android/videos/utils/XmlParser;",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;)",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;"
        }
    .end annotation

    .prologue
    .line 223
    .local p3, "subtitlesStore":Lcom/google/android/videos/store/AbstractFileStore;, "Lcom/google/android/videos/store/AbstractFileStore<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitlesConverter;

    invoke-direct {v0, p2}, Lcom/google/android/videos/subtitles/SubtitlesConverter;-><init>(Lcom/google/android/videos/utils/XmlParser;)V

    .line 224
    .local v0, "converter":Lcom/google/android/videos/subtitles/SubtitlesConverter;
    invoke-static {p0, v0, v0}, Lcom/google/android/videos/async/HttpRequester;->create(Lorg/apache/http/client/HttpClient;Lcom/google/android/videos/converter/RequestConverter;Lcom/google/android/videos/converter/HttpResponseConverter;)Lcom/google/android/videos/async/HttpRequester;

    move-result-object v1

    .line 227
    .local v1, "networkRequester":Lcom/google/android/videos/async/HttpRequester;, "Lcom/google/android/videos/async/HttpRequester<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p3, v1}, Lcom/google/android/videos/async/StoreCachingRequester;->create(Lcom/google/android/videos/store/AbstractFileStore;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/StoreCachingRequester;

    move-result-object v2

    .line 229
    .local v2, "storingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p1, v2}, Lcom/google/android/videos/async/AsyncRequester;->create(Ljava/util/concurrent/Executor;Lcom/google/android/videos/async/Requester;)Lcom/google/android/videos/async/AsyncRequester;

    move-result-object v3

    return-object v3
.end method

.method private static getSubtitleTracksStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;>;"
        }
    .end annotation

    .prologue
    .line 148
    new-instance v0, Lcom/google/android/videos/store/SubtitlesClient$4;

    new-instance v1, Lcom/google/android/videos/store/SubtitlesClient$SubtitleTrackSerializingConverter;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/videos/store/SubtitlesClient$SubtitleTrackSerializingConverter;-><init>(Lcom/google/android/videos/store/SubtitlesClient$1;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/store/SubtitlesClient$4;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-object v0
.end method

.method private static getSubtitlesDir(Ljava/io/File;)Ljava/io/File;
    .locals 2
    .param p0, "rootFileDir"    # Ljava/io/File;

    .prologue
    .line 190
    new-instance v0, Ljava/io/File;

    const-string v1, "subtitles"

    invoke-direct {v0, p0, v1}, Ljava/io/File;-><init>(Ljava/io/File;Ljava/lang/String;)V

    return-object v0
.end method

.method private static getSubtitlesStore(Landroid/content/Context;)Lcom/google/android/videos/store/AbstractFileStore;
    .locals 3
    .param p0, "context"    # Landroid/content/Context;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            ")",
            "Lcom/google/android/videos/store/AbstractFileStore",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;"
        }
    .end annotation

    .prologue
    .line 173
    new-instance v0, Lcom/google/android/videos/store/SubtitlesClient$5;

    new-instance v1, Lcom/google/android/videos/store/SubtitlesClient$SubtitlesSerializingConverter;

    const/4 v2, 0x0

    invoke-direct {v1, v2}, Lcom/google/android/videos/store/SubtitlesClient$SubtitlesSerializingConverter;-><init>(Lcom/google/android/videos/store/SubtitlesClient$1;)V

    invoke-direct {v0, p0, v1}, Lcom/google/android/videos/store/SubtitlesClient$5;-><init>(Landroid/content/Context;Lcom/google/android/videos/cache/Converter;)V

    return-object v0
.end method

.method private requestSubtitlesInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p2, "subtitleTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p3, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/util/Pair",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Ljava/lang/Integer;",
            ">;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 73
    .local p1, "requester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Landroid/util/Pair<Lcom/google/android/videos/subtitles/SubtitleTrack;Ljava/lang/Integer;>;Lcom/google/android/videos/subtitles/Subtitles;>;"
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    invoke-static {p3}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    invoke-static {p2, v0}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/store/SubtitlesClient$1;

    invoke-direct {v1, p0, p4}, Lcom/google/android/videos/store/SubtitlesClient$1;-><init>(Lcom/google/android/videos/store/SubtitlesClient;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {p1, v0, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 84
    return-void
.end method


# virtual methods
.method public requestOfflineSubtitleTracks(Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V
    .locals 2
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;>;)V"
        }
    .end annotation

    .prologue
    .line 103
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v1, Lcom/google/android/videos/store/SubtitlesClient$3;

    invoke-direct {v1, p0, p1, p2, p3}, Lcom/google/android/videos/store/SubtitlesClient$3;-><init>(Lcom/google/android/videos/store/SubtitlesClient;Ljava/lang/String;ILcom/google/android/videos/async/Callback;)V

    invoke-interface {v0, v1}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 126
    return-void
.end method

.method public requestSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "subtitleTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 62
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitlesRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/SubtitlesClient;->requestSubtitlesInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V

    .line 63
    return-void
.end method

.method public saveOfflineSubtitleTracks(Ljava/lang/String;Ljava/util/List;ILcom/google/android/videos/async/Callback;)V
    .locals 7
    .param p1, "videoId"    # Ljava/lang/String;
    .param p3, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            ">;I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Ljava/lang/String;",
            "Ljava/lang/Void;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 88
    .local p2, "tracks":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/subtitles/SubtitleTrack;>;"
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Ljava/lang/String;Ljava/lang/Void;>;"
    iget-object v6, p0, Lcom/google/android/videos/store/SubtitlesClient;->localExecutor:Ljava/util/concurrent/Executor;

    new-instance v0, Lcom/google/android/videos/store/SubtitlesClient$2;

    move-object v1, p0

    move-object v2, p1

    move v3, p3

    move-object v4, p2

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/store/SubtitlesClient$2;-><init>(Lcom/google/android/videos/store/SubtitlesClient;Ljava/lang/String;ILjava/util/List;Lcom/google/android/videos/async/Callback;)V

    invoke-interface {v6, v0}, Ljava/util/concurrent/Executor;->execute(Ljava/lang/Runnable;)V

    .line 99
    return-void
.end method

.method public saveSubtitles(Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "subtitleTrack"    # Lcom/google/android/videos/subtitles/SubtitleTrack;
    .param p2, "storage"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleTrack;",
            "Lcom/google/android/videos/subtitles/Subtitles;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 67
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/subtitles/SubtitleTrack;Lcom/google/android/videos/subtitles/Subtitles;>;"
    iget-object v0, p0, Lcom/google/android/videos/store/SubtitlesClient;->subtitlesStorer:Lcom/google/android/videos/async/Requester;

    invoke-direct {p0, v0, p1, p2, p3}, Lcom/google/android/videos/store/SubtitlesClient;->requestSubtitlesInternal(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/subtitles/SubtitleTrack;ILcom/google/android/videos/async/Callback;)V

    .line 68
    return-void
.end method
