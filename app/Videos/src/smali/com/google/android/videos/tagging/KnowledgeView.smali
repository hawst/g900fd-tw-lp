.class public interface abstract Lcom/google/android/videos/tagging/KnowledgeView;
.super Ljava/lang/Object;
.source "KnowledgeView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;
    }
.end annotation


# virtual methods
.method public abstract hasContent()Z
.end method

.method public abstract hideKnowledge()V
.end method

.method public abstract initKnowledge(Lcom/google/android/videos/tagging/KnowledgeBundle;Lcom/google/android/videos/tagging/KnowledgeView$PlayerTimeSupplier;I)V
.end method

.method public abstract isInteracting()Z
.end method

.method public abstract onBackPressed()Z
.end method

.method public abstract reset()V
.end method

.method public abstract setContentVisible(Z)V
.end method

.method public abstract showPausedKnowledge(IIILjava/util/List;I)V
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(III",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/TaggedKnowledgeEntity;",
            ">;I)V"
        }
    .end annotation
.end method

.method public abstract showPlayingKnowledge()V
.end method
