.class public Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
.super Ljava/lang/Object;
.source "PurchaseStoreSync.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/PurchaseStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "PurchaseStoreSyncRequest"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field public final eidrId:Ljava/lang/String;

.field public final isFullSync:Z

.field public final videoId:Ljava/lang/String;


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V
    .locals 1
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "eidrId"    # Ljava/lang/String;

    .prologue
    .line 74
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 75
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->account:Ljava/lang/String;

    .line 76
    iput-object p2, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->videoId:Ljava/lang/String;

    .line 77
    iput-object p3, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->eidrId:Ljava/lang/String;

    .line 78
    invoke-static {p2}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    invoke-static {p3}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    iput-boolean v0, p0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;->isFullSync:Z

    .line 79
    return-void

    .line 78
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public static createForEidrId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .locals 4
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "eidrId"    # Ljava/lang/String;

    .prologue
    .line 70
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    const/4 v2, 0x0

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v3

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createForFullSync(Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .locals 3
    .param p0, "account"    # Ljava/lang/String;

    .prologue
    const/4 v2, 0x0

    .line 60
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-direct {v0, v1, v2, v2}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method

.method public static createForId(Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;
    .locals 4
    .param p0, "account"    # Ljava/lang/String;
    .param p1, "id"    # Ljava/lang/String;

    .prologue
    .line 64
    new-instance v0, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;

    invoke-static {p0}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotEmpty(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v2

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/store/PurchaseStoreSync$PurchaseStoreSyncRequest;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)V

    return-object v0
.end method
