.class Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;
.super Ljava/lang/Object;
.source "LegacyPlayerStreamsSelector.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;
    }
.end annotation


# instance fields
.field private final display:Landroid/view/Display;

.field private final displaySupportsProtectedBuffers:Z

.field private final drmManager:Lcom/google/android/videos/drm/DrmManager;

.field private final streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

.field private final surroundSound:Z


# direct methods
.method public constructor <init>(Landroid/view/Display;Lcom/google/android/videos/streams/LegacyStreamsSelector;Lcom/google/android/videos/drm/DrmManager;ZZ)V
    .locals 1
    .param p1, "display"    # Landroid/view/Display;
    .param p2, "streamsSelector"    # Lcom/google/android/videos/streams/LegacyStreamsSelector;
    .param p3, "drmManager"    # Lcom/google/android/videos/drm/DrmManager;
    .param p4, "surroundSound"    # Z
    .param p5, "displaySupportsProtectedBuffers"    # Z

    .prologue
    .line 36
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 37
    invoke-static {p1}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/view/Display;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->display:Landroid/view/Display;

    .line 38
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/streams/LegacyStreamsSelector;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    .line 39
    invoke-static {p3}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/drm/DrmManager;

    iput-object v0, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    .line 40
    iput-boolean p4, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->surroundSound:Z

    .line 41
    iput-boolean p5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->displaySupportsProtectedBuffers:Z

    .line 42
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;
    .param p1, "x1"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "x2"    # I
    .param p3, "x3"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->selectDrmStreamsInternal(Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;
    .param p1, "x1"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "x2"    # Lcom/google/android/videos/streams/LegacyStreamSelection;
    .param p3, "x3"    # Z
    .param p4, "x4"    # Lcom/google/android/videos/async/Callback;

    .prologue
    .line 27
    invoke-direct {p0, p1, p2, p3, p4}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->licenseStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V

    return-void
.end method

.method private createDrmRequest(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/MediaStream;ZLjava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;
    .locals 3
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "stream"    # Lcom/google/android/videos/streams/MediaStream;
    .param p3, "authenticateOfflineRequest"    # Z
    .param p4, "playbackId"    # Ljava/lang/String;

    .prologue
    .line 122
    iget-object v0, p1, Lcom/google/android/videos/player/VideoInfo;->account:Ljava/lang/String;

    .line 123
    .local v0, "account":Ljava/lang/String;
    iget-object v1, p1, Lcom/google/android/videos/player/VideoInfo;->videoId:Ljava/lang/String;

    .line 124
    .local v1, "videoId":Ljava/lang/String;
    iget-boolean v2, p2, Lcom/google/android/videos/streams/MediaStream;->isOffline:Z

    if-eqz v2, :cond_1

    .line 125
    if-eqz p3, :cond_0

    iget-object v2, p1, Lcom/google/android/videos/player/VideoInfo;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    invoke-static {v0, p2, v1, v2}, Lcom/google/android/videos/drm/DrmRequest;->createOfflineRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v2

    .line 129
    :goto_0
    return-object v2

    .line 125
    :cond_0
    iget-object v2, p1, Lcom/google/android/videos/player/VideoInfo;->drmIdentifiers:Lcom/google/android/videos/drm/DrmManager$Identifiers;

    invoke-static {p2, v1, v2}, Lcom/google/android/videos/drm/DrmRequest;->createOfflineRequest(Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Lcom/google/android/videos/drm/DrmManager$Identifiers;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v2

    goto :goto_0

    .line 129
    :cond_1
    invoke-static {v0, p2, v1, p4}, Lcom/google/android/videos/drm/DrmRequest;->createStreamingRequest(Ljava/lang/String;Lcom/google/android/videos/streams/MediaStream;Ljava/lang/String;Ljava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v2

    goto :goto_0
.end method

.method private licenseStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V
    .locals 10
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "streamSelection"    # Lcom/google/android/videos/streams/LegacyStreamSelection;
    .param p3, "authenticateOfflineRequest"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            "Z",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p4, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    const/4 v9, 0x0

    .line 105
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    const/16 v1, 0x10

    invoke-static {v2, v3, v1}, Ljava/lang/Long;->toString(JI)Ljava/lang/String;

    move-result-object v8

    .line 106
    .local v8, "playbackId":Ljava/lang/String;
    new-instance v0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;

    move-object v1, p0

    move-object v2, p1

    move-object v3, p2

    move v4, p3

    move-object v5, p4

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector$DrmCallback;-><init>(Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V

    .line 108
    .local v0, "activityCallback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/drm/DrmResponse;>;"
    iget-object v1, p2, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {p0, p1, v1, p3, v8}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->createDrmRequest(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/MediaStream;ZLjava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v6

    .line 110
    .local v6, "hiRequest":Lcom/google/android/videos/drm/DrmRequest;
    iget-boolean v1, p2, Lcom/google/android/videos/streams/LegacyStreamSelection;->supportsQualityToggle:Z

    if-nez v1, :cond_0

    .line 111
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1, v6, v0}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 118
    :goto_0
    return-void

    .line 113
    :cond_0
    iget-object v1, p2, Lcom/google/android/videos/streams/LegacyStreamSelection;->lo:Ljava/util/List;

    invoke-interface {v1, v9}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {p0, p1, v1, p3, v8}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->createDrmRequest(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/MediaStream;ZLjava/lang/String;)Lcom/google/android/videos/drm/DrmRequest;

    move-result-object v7

    .line 115
    .local v7, "loRequest":Lcom/google/android/videos/drm/DrmRequest;
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1, v6, v0}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    .line 116
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1, v7, v0}, Lcom/google/android/videos/drm/DrmManager;->request(Lcom/google/android/videos/drm/DrmRequest;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private selectClearStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V
    .locals 6
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 57
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    :try_start_0
    iget-boolean v2, p1, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v2, :cond_0

    .line 58
    new-instance v1, Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v2, p1, Lcom/google/android/videos/player/VideoInfo;->offlineStreams:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {v1, v2}, Lcom/google/android/videos/streams/LegacyStreamSelection;-><init>(Lcom/google/android/videos/streams/MediaStream;)V

    .line 63
    .local v1, "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :goto_0
    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 67
    .end local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :goto_1
    return-void

    .line 60
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->display:Landroid/view/Display;

    iget-object v4, p1, Lcom/google/android/videos/player/VideoInfo;->onlineStreams:Ljava/util/List;

    iget-boolean v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->surroundSound:Z

    invoke-virtual {v2, v3, v4, v5}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getOnlineClearStreams(Landroid/view/Display;Ljava/util/List;Z)Lcom/google/android/videos/streams/LegacyStreamSelection;
    :try_end_0
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    move-result-object v1

    .restart local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    goto :goto_0

    .line 64
    .end local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :catch_0
    move-exception v0

    .line 65
    .local v0, "exception":Lcom/google/android/videos/streams/MissingStreamException;
    invoke-interface {p2, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_1
.end method

.method private selectDrmStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V
    .locals 4
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 71
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    iget-object v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->drmManager:Lcom/google/android/videos/drm/DrmManager;

    invoke-virtual {v1}, Lcom/google/android/videos/drm/DrmManager;->getDrmLevel()I

    move-result v0

    .line 72
    .local v0, "initialDrmLevel":I
    if-gez v0, :cond_0

    .line 73
    new-instance v1, Lcom/google/android/videos/drm/DrmException;

    sget-object v2, Lcom/google/android/videos/drm/DrmException$DrmError;->ROOTED_DEVICE:Lcom/google/android/videos/drm/DrmException$DrmError;

    const/4 v3, 0x0

    invoke-direct {v1, v2, v3}, Lcom/google/android/videos/drm/DrmException;-><init>(Lcom/google/android/videos/drm/DrmException$DrmError;I)V

    invoke-interface {p2, p1, v1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 80
    :goto_0
    return-void

    .line 76
    :cond_0
    iget-boolean v1, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->displaySupportsProtectedBuffers:Z

    if-nez v1, :cond_1

    .line 77
    const/4 v1, 0x1

    invoke-static {v0, v1}, Ljava/lang/Math;->min(II)I

    move-result v0

    .line 79
    :cond_1
    invoke-direct {p0, p1, v0, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->selectDrmStreamsInternal(Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method

.method private selectDrmStreamsInternal(Lcom/google/android/videos/player/VideoInfo;ILcom/google/android/videos/async/Callback;)V
    .locals 6
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .param p2, "drmLevel"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "I",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 86
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    :try_start_0
    iget-boolean v2, p1, Lcom/google/android/videos/player/VideoInfo;->isOffline:Z

    if-eqz v2, :cond_0

    .line 87
    new-instance v1, Lcom/google/android/videos/streams/LegacyStreamSelection;

    iget-object v2, p1, Lcom/google/android/videos/player/VideoInfo;->offlineStreams:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    invoke-direct {v1, v2}, Lcom/google/android/videos/streams/LegacyStreamSelection;-><init>(Lcom/google/android/videos/streams/MediaStream;)V

    .line 88
    .local v1, "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    iget-object v2, v1, Lcom/google/android/videos/streams/LegacyStreamSelection;->hi:Ljava/util/List;

    const/4 v3, 0x0

    invoke-interface {v2, v3}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/streams/MediaStream;

    iget-object v2, v2, Lcom/google/android/videos/streams/MediaStream;->itagInfo:Lcom/google/android/videos/streams/ItagInfo;

    iget v2, v2, Lcom/google/android/videos/streams/ItagInfo;->drmType:I

    const/4 v3, 0x2

    if-ne v2, v3, :cond_1

    iget-boolean v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->displaySupportsProtectedBuffers:Z

    if-nez v2, :cond_1

    .line 90
    new-instance v2, Lcom/google/android/videos/player/ProtectedBufferException;

    invoke-direct {v2}, Lcom/google/android/videos/player/ProtectedBufferException;-><init>()V

    invoke-interface {p3, p1, v2}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    .line 101
    .end local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :goto_0
    return-void

    .line 94
    :cond_0
    iget-object v2, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->streamsSelector:Lcom/google/android/videos/streams/LegacyStreamsSelector;

    iget-object v3, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->display:Landroid/view/Display;

    iget-object v4, p1, Lcom/google/android/videos/player/VideoInfo;->onlineStreams:Ljava/util/List;

    iget-boolean v5, p0, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->surroundSound:Z

    invoke-virtual {v2, v3, v4, v5, p2}, Lcom/google/android/videos/streams/LegacyStreamsSelector;->getOnlineDrmStreams(Landroid/view/Display;Ljava/util/List;ZI)Lcom/google/android/videos/streams/LegacyStreamSelection;

    move-result-object v1

    .line 97
    .restart local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :cond_1
    const/4 v2, 0x0

    invoke-direct {p0, p1, v1, v2, p3}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->licenseStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;ZLcom/google/android/videos/async/Callback;)V
    :try_end_0
    .catch Lcom/google/android/videos/streams/MissingStreamException; {:try_start_0 .. :try_end_0} :catch_0

    goto :goto_0

    .line 98
    .end local v1    # "selection":Lcom/google/android/videos/streams/LegacyStreamSelection;
    :catch_0
    move-exception v0

    .line 99
    .local v0, "exception":Lcom/google/android/videos/streams/MissingStreamException;
    invoke-interface {p3, p1, v0}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method


# virtual methods
.method public getStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V
    .locals 1
    .param p1, "videoInfo"    # Lcom/google/android/videos/player/VideoInfo;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/player/VideoInfo;",
            "Lcom/google/android/videos/streams/LegacyStreamSelection;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 46
    .local p2, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/streams/LegacyStreamSelection;>;"
    iget-boolean v0, p1, Lcom/google/android/videos/player/VideoInfo;->isEncrypted:Z

    if-nez v0, :cond_0

    .line 47
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->selectClearStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V

    .line 51
    :goto_0
    return-void

    .line 49
    :cond_0
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/player/legacy/LegacyPlayerStreamsSelector;->selectDrmStreams(Lcom/google/android/videos/player/VideoInfo;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method
