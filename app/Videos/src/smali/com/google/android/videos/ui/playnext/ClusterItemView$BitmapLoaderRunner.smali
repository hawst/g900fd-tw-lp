.class final Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;
.super Ljava/lang/Object;
.source "ClusterItemView.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/playnext/ClusterItemView;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x1a
    name = "BitmapLoaderRunner"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "<Q:",
        "Ljava/lang/Object;",
        ">",
        "Ljava/lang/Object;"
    }
.end annotation


# instance fields
.field private final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field private final bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;"
        }
    .end annotation
.end field

.field private final cpuExecutor:Ljava/util/concurrent/Executor;

.field private final missingBitmap:Landroid/graphics/Bitmap;

.field private final requestType:Ljava/lang/Class;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/Class",
            "<TQ;>;"
        }
    .end annotation
.end field

.field private shouldRequest:Z


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)V
    .locals 1
    .param p3, "cpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "missingBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<TQ;",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Class",
            "<TQ;>;)V"
        }
    .end annotation

    .prologue
    .line 440
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<TQ;>;"
    .local p1, "bitmapView":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<*>;"
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<TQ;Landroid/graphics/Bitmap;>;"
    .local p5, "requestType":Ljava/lang/Class;, "Ljava/lang/Class<TQ;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 441
    iput-object p1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    .line 442
    iput-object p2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 443
    iput-object p3, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->cpuExecutor:Ljava/util/concurrent/Executor;

    .line 444
    iput-object p4, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->missingBitmap:Landroid/graphics/Bitmap;

    .line 445
    iput-object p5, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->requestType:Ljava/lang/Class;

    .line 447
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    .line 448
    return-void
.end method

.method private sendRequest(Ljava/lang/Object;)V
    .locals 6
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TQ;)V"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<TQ;>;"
    .local p1, "request":Ljava/lang/Object;, "TQ;"
    const/4 v5, 0x0

    .line 490
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->cpuExecutor:Ljava/util/concurrent/Executor;

    if-nez v0, :cond_0

    .line 491
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    check-cast v0, Lcom/google/android/videos/ui/BitmapLoader$BitmapView;

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->missingBitmap:Landroid/graphics/Bitmap;

    invoke-static {v0, v1, p1, v2, v5}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/lang/Object;Ljava/lang/Object;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    .line 497
    :goto_0
    return-void

    .line 494
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    check-cast v0, Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;

    iget-object v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->cpuExecutor:Ljava/util/concurrent/Executor;

    iget-object v4, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->missingBitmap:Landroid/graphics/Bitmap;

    move-object v3, p1

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/ui/BitmapLoader;->setBitmapAsync(Lcom/google/android/videos/ui/BitmapLoader$BitmapPaletteView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Ljava/lang/Object;Landroid/graphics/Bitmap;Lcom/google/android/videos/ui/BitmapLoader$CompletionCallback;)V

    goto :goto_0
.end method


# virtual methods
.method public kick(ILcom/google/android/videos/ui/playnext/ClusterItemView;)V
    .locals 1
    .param p1, "id"    # I
    .param p2, "view"    # Lcom/google/android/videos/ui/playnext/ClusterItemView;

    .prologue
    .line 478
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<TQ;>;"
    iget-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    if-eqz v0, :cond_0

    .line 479
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    .line 480
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->requestType:Ljava/lang/Class;

    invoke-virtual {p2, p1, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView;->onGenerateBitmapViewRequest(ILjava/lang/Class;)Ljava/lang/Object;

    move-result-object v0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->sendRequest(Ljava/lang/Object;)V

    .line 482
    :cond_0
    return-void
.end method

.method public rebindAndDisableIfDifferent(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;Lcom/google/android/videos/async/Requester;Ljava/util/concurrent/Executor;Landroid/graphics/Bitmap;Ljava/lang/Class;)Z
    .locals 3
    .param p3, "newCpuExecutor"    # Ljava/util/concurrent/Executor;
    .param p4, "newMissingBitmap"    # Landroid/graphics/Bitmap;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView",
            "<*>;",
            "Lcom/google/android/videos/async/Requester",
            "<*",
            "Landroid/graphics/Bitmap;",
            ">;",
            "Ljava/util/concurrent/Executor;",
            "Landroid/graphics/Bitmap;",
            "Ljava/lang/Class",
            "<*>;)Z"
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<TQ;>;"
    .local p1, "newBitmapView":Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;, "Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView<*>;"
    .local p2, "newBitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<*Landroid/graphics/Bitmap;>;"
    .local p5, "newRequestType":Ljava/lang/Class;, "Ljava/lang/Class<*>;"
    const/4 v1, 0x1

    const/4 v0, 0x0

    .line 461
    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    if-ne v2, p1, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    if-ne v2, p2, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->cpuExecutor:Ljava/util/concurrent/Executor;

    if-ne v2, p3, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->missingBitmap:Landroid/graphics/Bitmap;

    if-ne v2, p4, :cond_0

    iget-object v2, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->requestType:Ljava/lang/Class;

    if-ne v2, p5, :cond_0

    .line 464
    iput-boolean v1, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    .line 469
    :goto_0
    return v0

    .line 467
    :cond_0
    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    .line 468
    iget-object v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->bitmapView:Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;

    invoke-static {v0}, Lcom/google/android/videos/ui/BitmapLoader;->cancel(Lcom/google/android/videos/ui/BitmapLoader$GenericBitmapView;)V

    move v0, v1

    .line 469
    goto :goto_0
.end method

.method public reset()V
    .locals 1

    .prologue
    .line 485
    .local p0, "this":Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;, "Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner<TQ;>;"
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->sendRequest(Ljava/lang/Object;)V

    .line 486
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/playnext/ClusterItemView$BitmapLoaderRunner;->shouldRequest:Z

    .line 487
    return-void
.end method
