.class public Lcom/google/android/videos/store/StoryboardImageRequest;
.super Ljava/lang/Object;
.source "StoryboardImageRequest.java"


# instance fields
.field public final imageIndex:I

.field public final storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

.field public final uniqueKey:Ljava/lang/String;

.field public final videoId:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/Storyboard;I)V
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;
    .param p2, "storyboard"    # Lcom/google/wireless/android/video/magma/proto/Storyboard;
    .param p3, "imageIndex"    # I

    .prologue
    .line 22
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 23
    iput-object p1, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->videoId:Ljava/lang/String;

    .line 24
    iput-object p2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    .line 25
    iput p3, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    .line 26
    invoke-virtual {p0}, Lcom/google/android/videos/store/StoryboardImageRequest;->computeUniqueKey()Ljava/lang/String;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->uniqueKey:Ljava/lang/String;

    .line 27
    return-void
.end method


# virtual methods
.method public computeUniqueKey()Ljava/lang/String;
    .locals 7

    .prologue
    .line 34
    const/4 v0, 0x1

    .line 35
    .local v0, "storyboardHash":I
    iget-object v1, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v1, v1, Lcom/google/wireless/android/video/magma/proto/Storyboard;->videoLengthMs:I

    add-int/lit8 v0, v1, 0x1f

    .line 36
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameHeight:I

    add-int v0, v1, v2

    .line 37
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->frameWidth:I

    add-int v0, v1, v2

    .line 38
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->numberOfFrames:I

    add-int v0, v1, v2

    .line 39
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerColumn:I

    add-int v0, v1, v2

    .line 40
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->maxFramesPerRow:I

    add-int v0, v1, v2

    .line 41
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->samplingIntervalMs:I

    add-int v0, v1, v2

    .line 42
    mul-int/lit8 v1, v0, 0x1f

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget-wide v2, v2, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    iget-object v4, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->storyboard:Lcom/google/wireless/android/video/magma/proto/Storyboard;

    iget-wide v4, v4, Lcom/google/wireless/android/video/magma/proto/Storyboard;->totalImageBytes:J

    const/16 v6, 0x20

    ushr-long/2addr v4, v6

    xor-long/2addr v2, v4

    long-to-int v2, v2

    add-int v0, v1, v2

    .line 44
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    iget-object v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->videoId:Ljava/lang/String;

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "1"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    iget v2, p0, Lcom/google/android/videos/store/StoryboardImageRequest;->imageIndex:I

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, "."

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, v0}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v1

    const-string v2, ".stbim"

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    return-object v1
.end method
