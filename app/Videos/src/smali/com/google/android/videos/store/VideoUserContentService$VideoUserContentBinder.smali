.class Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;
.super Lcom/google/android/play/IUserContentService$Stub;
.source "VideoUserContentService.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/VideoUserContentService;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "VideoUserContentBinder"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder$Query;
    }
.end annotation


# static fields
.field private static final THUMBNAIL_BASE_URI:Landroid/net/Uri;


# instance fields
.field private final accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

.field private final freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

.field private final mainHandler:Landroid/os/Handler;

.field private final packageManager:Landroid/content/pm/PackageManager;

.field private final preferences:Landroid/content/SharedPreferences;

.field private final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field private final resources:Landroid/content/res/Resources;

.field private final signInManager:Lcom/google/android/videos/accounts/SignInManager;

.field private final syncBitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/utils/ByteArray;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method static constructor <clinit>()V
    .locals 2

    .prologue
    .line 126
    new-instance v0, Landroid/net/Uri$Builder;

    invoke-direct {v0}, Landroid/net/Uri$Builder;-><init>()V

    const-string v1, "content"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->scheme(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "com.google.android.videos"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->authority(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "thumbnail"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->path(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    sput-object v0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->THUMBNAIL_BASE_URI:Landroid/net/Uri;

    return-void
.end method

.method public constructor <init>(Landroid/content/Context;)V
    .locals 3
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 152
    invoke-direct {p0}, Lcom/google/android/play/IUserContentService$Stub;-><init>()V

    .line 153
    invoke-static {p1}, Lcom/google/android/videos/VideosGlobals;->from(Landroid/content/Context;)Lcom/google/android/videos/VideosGlobals;

    move-result-object v0

    .line 154
    .local v0, "videosGlobals":Lcom/google/android/videos/VideosGlobals;
    new-instance v1, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v2

    invoke-direct {v1, v2}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->mainHandler:Landroid/os/Handler;

    .line 155
    invoke-virtual {p1}, Landroid/content/Context;->getPackageManager()Landroid/content/pm/PackageManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->packageManager:Landroid/content/pm/PackageManager;

    .line 156
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPreferences()Landroid/content/SharedPreferences;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->preferences:Landroid/content/SharedPreferences;

    .line 157
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getAccountManagerWrapper()Lcom/google/android/videos/accounts/AccountManagerWrapper;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    .line 158
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getSignInManager()Lcom/google/android/videos/accounts/SignInManager;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    .line 159
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getPurchaseStore()Lcom/google/android/videos/store/PurchaseStore;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 160
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getFreeMoviePromoWelcome()Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    .line 161
    invoke-virtual {p1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->resources:Landroid/content/res/Resources;

    .line 162
    invoke-virtual {v0}, Lcom/google/android/videos/VideosGlobals;->getBitmapRequesters()Lcom/google/android/videos/bitmap/BitmapRequesters;

    move-result-object v1

    invoke-interface {v1}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getSyncBitmapBytesRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v1

    iput-object v1, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 163
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;
    .param p1, "x1"    # Ljava/lang/String;

    .prologue
    .line 121
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getPromoUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private convertPromoToBundle(Ljava/lang/String;Landroid/net/Uri;)Landroid/os/Bundle;
    .locals 8
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "promoUri"    # Landroid/net/Uri;

    .prologue
    .line 295
    const-string v2, "watchnow"

    const/16 v3, 0x12

    invoke-static {v3}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v3

    invoke-direct {p0, p1, v2, v3}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getVerticalIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    .line 298
    .local v1, "intent":Landroid/content/Intent;
    new-instance v0, Landroid/os/Bundle;

    invoke-direct {v0}, Landroid/os/Bundle;-><init>()V

    .line 299
    .local v0, "bundle":Landroid/os/Bundle;
    const-string v2, "Play.ViewIntent"

    invoke-virtual {v0, v2, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 300
    const-string v2, "Play.LastUpdateTimeMillis"

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    const-wide/32 v6, 0x36ee80

    add-long/2addr v4, v6

    invoke-virtual {v0, v2, v4, v5}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 302
    const-string v2, "Play.ImageUri"

    invoke-virtual {p2}, Landroid/net/Uri;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v3}, Lcom/google/android/videos/search/VideoSearchProvider;->getSuggestThumbnailPath(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 304
    const-string v2, "Play.Reason"

    iget-object v3, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->resources:Landroid/content/res/Resources;

    const v4, 0x7f0b01b0

    invoke-virtual {v3, v4}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v0, v2, v3}, Landroid/os/Bundle;->putString(Ljava/lang/String;Ljava/lang/String;)V

    .line 305
    return-object v0
.end method

.method private convertShowToBundle(Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 13
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 309
    const/4 v11, 0x6

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 310
    .local v9, "showId":Ljava/lang/String;
    const/4 v11, 0x7

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v8

    .line 311
    .local v8, "seasonId":Ljava/lang/String;
    const/4 v11, 0x1

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v0

    .line 312
    .local v0, "account":Ljava/lang/String;
    const/4 v11, 0x2

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 313
    .local v6, "purchaseTimestamp":J
    const/4 v11, 0x4

    invoke-interface {p1, v11}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v4

    .line 314
    .local v4, "lastWatchedTimestamp":J
    invoke-static {v6, v7, v4, v5}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v2

    .line 316
    .local v2, "lastUpdateTimestamp":J
    const/16 v11, 0x11

    invoke-static {v11}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v11

    invoke-direct {p0, v0, v9, v8, v11}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getShowIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v10

    .line 318
    .local v10, "showIntent":Landroid/content/Intent;
    new-instance v1, Landroid/os/Bundle;

    invoke-direct {v1}, Landroid/os/Bundle;-><init>()V

    .line 319
    .local v1, "bundle":Landroid/os/Bundle;
    const-string v11, "Play.ViewIntent"

    invoke-virtual {v1, v11, v10}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 320
    const-string v11, "Play.LastUpdateTimeMillis"

    invoke-virtual {v1, v11, v2, v3}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 321
    const-string v11, "Play.ImageUri"

    invoke-direct {p0, v9}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getShowPosterUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v12

    invoke-virtual {v1, v11, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 322
    return-object v1
.end method

.method private convertToBundle(Landroid/database/Cursor;)Landroid/os/Bundle;
    .locals 24
    .param p1, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 326
    const/16 v19, 0x0

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v18

    .line 327
    .local v18, "videoId":Ljava/lang/String;
    const/16 v19, 0x1

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v4

    .line 328
    .local v4, "account":Ljava/lang/String;
    const/16 v19, 0x2

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v16

    .line 329
    .local v16, "purchaseTimestamp":J
    const/16 v19, 0x4

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v10

    .line 330
    .local v10, "lastWatchedTimestamp":J
    const/16 v19, 0x3

    move-object/from16 v0, p1

    move/from16 v1, v19

    invoke-interface {v0, v1}, Landroid/database/Cursor;->getLong(I)J

    move-result-wide v6

    .line 332
    .local v6, "expirationTimestamp":J
    move-wide/from16 v0, v16

    invoke-static {v0, v1, v10, v11}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v8

    .line 333
    .local v8, "lastUpdateTimestamp":J
    const/16 v19, 0x11

    invoke-static/range {v19 .. v19}, Lcom/google/android/videos/utils/Campaigns;->getCampaignId(I)Ljava/lang/String;

    move-result-object v19

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-direct {v0, v4, v1, v2}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getMovieIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v12

    .line 336
    .local v12, "movieIntent":Landroid/content/Intent;
    new-instance v5, Landroid/os/Bundle;

    invoke-direct {v5}, Landroid/os/Bundle;-><init>()V

    .line 337
    .local v5, "bundle":Landroid/os/Bundle;
    const-string v19, "Play.ViewIntent"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v12}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 338
    const-string v19, "Play.LastUpdateTimeMillis"

    move-object/from16 v0, v19

    invoke-virtual {v5, v0, v8, v9}, Landroid/os/Bundle;->putLong(Ljava/lang/String;J)V

    .line 339
    const-string v19, "Play.ImageUri"

    move-object/from16 v0, p0

    move-object/from16 v1, v18

    invoke-direct {v0, v1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getImageUri(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v20

    move-object/from16 v0, v19

    move-object/from16 v1, v20

    invoke-virtual {v5, v0, v1}, Landroid/os/Bundle;->putParcelable(Ljava/lang/String;Landroid/os/Parcelable;)V

    .line 341
    const/4 v13, 0x0

    .line 342
    .local v13, "reason":Ljava/lang/String;
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v14

    .line 343
    .local v14, "now":J
    const-wide/16 v20, 0x0

    cmp-long v19, v6, v20

    if-eqz v19, :cond_2

    sub-long v20, v6, v14

    const-wide/32 v22, 0x5265c00

    cmp-long v19, v20, v22

    if-gez v19, :cond_2

    .line 345
    sub-long v20, v6, v14

    move-object/from16 v0, p0

    move-wide/from16 v1, v20

    invoke-direct {v0, v1, v2}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getShortExpirationWarning(J)Ljava/lang/String;

    move-result-object v13

    .line 349
    :cond_0
    :goto_0
    if-eqz v13, :cond_1

    .line 353
    :cond_1
    return-object v5

    .line 346
    :cond_2
    const-wide/16 v20, 0x0

    cmp-long v19, v10, v20

    if-nez v19, :cond_0

    sub-long v20, v14, v16

    const-wide/32 v22, 0x5265c00

    cmp-long v19, v20, v22

    if-gez v19, :cond_0

    .line 347
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->resources:Landroid/content/res/Resources;

    move-object/from16 v19, v0

    const v20, 0x7f0b0112

    invoke-virtual/range {v19 .. v20}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v13

    goto :goto_0
.end method

.method private enforceCallingPackage(Ljava/lang/String;)V
    .locals 9
    .param p1, "allowedPackage"    # Ljava/lang/String;

    .prologue
    .line 181
    invoke-static {}, Landroid/os/Binder;->getCallingUid()I

    move-result v5

    .line 182
    .local v5, "uid":I
    iget-object v6, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->packageManager:Landroid/content/pm/PackageManager;

    invoke-virtual {v6, v5}, Landroid/content/pm/PackageManager;->getPackagesForUid(I)[Ljava/lang/String;

    move-result-object v4

    .line 183
    .local v4, "packageNames":[Ljava/lang/String;
    if-eqz v4, :cond_1

    .line 184
    move-object v0, v4

    .local v0, "arr$":[Ljava/lang/String;
    array-length v2, v0

    .local v2, "len$":I
    const/4 v1, 0x0

    .local v1, "i$":I
    :goto_0
    if-ge v1, v2, :cond_1

    aget-object v3, v0, v1

    .line 185
    .local v3, "name":Ljava/lang/String;
    invoke-virtual {p1, v3}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 186
    return-void

    .line 184
    :cond_0
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 190
    .end local v0    # "arr$":[Ljava/lang/String;
    .end local v1    # "i$":I
    .end local v2    # "len$":I
    .end local v3    # "name":Ljava/lang/String;
    :cond_1
    new-instance v6, Ljava/lang/SecurityException;

    new-instance v7, Ljava/lang/StringBuilder;

    invoke-direct {v7}, Ljava/lang/StringBuilder;-><init>()V

    invoke-virtual {v7, v5}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v7

    const-string v8, " not allowed"

    invoke-virtual {v7, v8}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v7

    invoke-virtual {v7}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v7

    invoke-direct {v6, v7}, Ljava/lang/SecurityException;-><init>(Ljava/lang/String;)V

    throw v6
.end method

.method private getImageUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 1
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 364
    sget-object v0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->THUMBNAIL_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getMovieIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "videoId"    # Ljava/lang/String;
    .param p3, "referer"    # Ljava/lang/String;

    .prologue
    .line 370
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://www.youtube.com/watch/?v="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ref"

    invoke-virtual {v1, v2, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 373
    .local v0, "watchUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.videos.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "video_id"

    invoke-virtual {v1, v2, p2}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private getPromoUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    const/4 v1, 0x0

    .line 257
    invoke-static {}, Lcom/google/android/videos/utils/Preconditions;->checkMainThread()V

    .line 258
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    invoke-virtual {v0, p1, v1, v1}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->prepareIfEligible(Ljava/lang/String;ZZ)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 259
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->freeMoviePromoWelcome:Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    invoke-virtual {v0}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;->getNetworkBitmapUri()Landroid/net/Uri;

    move-result-object v0

    .line 261
    :goto_0
    return-object v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private getPromoUriOnMainThread(Ljava/lang/String;)Landroid/net/Uri;
    .locals 4
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 242
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 243
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Ljava/lang/String;Landroid/net/Uri;>;"
    iget-object v2, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->mainHandler:Landroid/os/Handler;

    new-instance v3, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder$1;

    invoke-direct {v3, p0, v0, p1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder$1;-><init>(Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;Lcom/google/android/videos/async/SyncCallback;Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 250
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Landroid/net/Uri;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 252
    :goto_0
    return-object v2

    .line 251
    :catch_0
    move-exception v1

    .line 252
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getPromoVideo(Ljava/lang/String;)Ljava/util/List;
    .locals 4
    .param p1, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            ")",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 231
    new-instance v1, Ljava/util/ArrayList;

    invoke-direct {v1}, Ljava/util/ArrayList;-><init>()V

    .line 232
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-direct {p0, p1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getPromoUriOnMainThread(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v0

    .line 233
    .local v0, "promoUri":Landroid/net/Uri;
    if-eqz v0, :cond_0

    .line 235
    iget-object v2, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->syncBitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-static {}, Lcom/google/android/videos/async/IgnoreCallback;->get()Lcom/google/android/videos/async/NewCallback;

    move-result-object v3

    invoke-interface {v2, v0, v3}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 236
    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->convertPromoToBundle(Ljava/lang/String;Landroid/net/Uri;)Landroid/os/Bundle;

    move-result-object v2

    invoke-interface {v1, v2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 238
    :cond_0
    return-object v1
.end method

.method private getShortExpirationWarning(J)Ljava/lang/String;
    .locals 7
    .param p1, "millisRemaining"    # J

    .prologue
    const-wide/16 v4, 0x3c

    .line 398
    const-wide/16 v2, 0x3e8

    div-long v2, p1, v2

    div-long/2addr v2, v4

    div-long v0, v2, v4

    .line 399
    .local v0, "hoursRemaining":J
    const-wide/16 v2, 0x4

    cmp-long v2, v0, v2

    if-gez v2, :cond_0

    .line 400
    iget-object v2, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->resources:Landroid/content/res/Resources;

    const v3, 0x7f0b01af

    invoke-virtual {v2, v3}, Landroid/content/res/Resources;->getString(I)Ljava/lang/String;

    move-result-object v2

    .line 402
    :goto_0
    return-object v2

    :cond_0
    const/4 v2, 0x0

    goto :goto_0
.end method

.method private getShowIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 3
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "showId"    # Ljava/lang/String;
    .param p3, "seasonId"    # Ljava/lang/String;
    .param p4, "referer"    # Ljava/lang/String;

    .prologue
    .line 379
    new-instance v1, Ljava/lang/StringBuilder;

    invoke-direct {v1}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "http://www.youtube.com/show/?p="

    invoke-virtual {v1, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v1

    invoke-virtual {v1}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v1

    invoke-static {v1}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v1

    const-string v2, "ref"

    invoke-virtual {v1, v2, p4}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v1

    invoke-virtual {v1}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    .line 382
    .local v0, "showUri":Landroid/net/Uri;
    new-instance v1, Landroid/content/Intent;

    const-string v2, "com.google.android.videos.intent.action.VIEW"

    invoke-direct {v1, v2, v0}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v2, "authAccount"

    invoke-virtual {v1, v2, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    const-string v2, "season_id"

    invoke-virtual {v1, v2, p3}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    return-object v1
.end method

.method private getShowPosterUri(Ljava/lang/String;)Landroid/net/Uri;
    .locals 2
    .param p1, "showId"    # Ljava/lang/String;

    .prologue
    .line 357
    sget-object v0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->THUMBNAIL_BASE_URI:Landroid/net/Uri;

    invoke-virtual {v0}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v0

    const-string v1, "show"

    invoke-virtual {v0, v1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0, p1}, Landroid/net/Uri$Builder;->appendPath(Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v0

    invoke-virtual {v0}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v0

    return-object v0
.end method

.method private getVerticalIntent(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;
    .locals 5
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "vertical"    # Ljava/lang/String;
    .param p3, "referer"    # Ljava/lang/String;

    .prologue
    .line 388
    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "http://play.google.com/movies/"

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2, p2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-static {v2}, Landroid/net/Uri;->parse(Ljava/lang/String;)Landroid/net/Uri;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri;->buildUpon()Landroid/net/Uri$Builder;

    move-result-object v2

    const-string v3, "ref"

    invoke-virtual {v2, v3, p3}, Landroid/net/Uri$Builder;->appendQueryParameter(Ljava/lang/String;Ljava/lang/String;)Landroid/net/Uri$Builder;

    move-result-object v2

    invoke-virtual {v2}, Landroid/net/Uri$Builder;->build()Landroid/net/Uri;

    move-result-object v1

    .line 391
    .local v1, "uri":Landroid/net/Uri;
    new-instance v2, Landroid/content/Intent;

    const-string v3, "com.google.android.videos.intent.action.VIEW"

    invoke-direct {v2, v3, v1}, Landroid/content/Intent;-><init>(Ljava/lang/String;Landroid/net/Uri;)V

    const-string v3, "authAccount"

    invoke-virtual {v2, v3, p1}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v2

    const-string v3, "force_close_drawer"

    const/4 v4, 0x1

    invoke-virtual {v2, v3, v4}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    move-result-object v0

    .line 394
    .local v0, "intent":Landroid/content/Intent;
    return-object v0
.end method

.method private getWhatsNext(I)Ljava/util/List;
    .locals 5
    .param p1, "maxItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 194
    iget-object v2, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v2}, Lcom/google/android/videos/accounts/SignInManager;->getSignedInAccount()Ljava/lang/String;

    move-result-object v0

    .line 195
    .local v0, "currentAccount":Ljava/lang/String;
    if-nez v0, :cond_1

    .line 196
    invoke-direct {p0}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->selectDefaultAccount()Ljava/lang/String;

    move-result-object v0

    .line 197
    if-nez v0, :cond_1

    .line 199
    invoke-static {}, Ljava/util/Collections;->emptyList()Ljava/util/List;

    move-result-object v1

    .line 212
    :cond_0
    :goto_0
    return-object v1

    .line 203
    :cond_1
    iget-object v2, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->preferences:Landroid/content/SharedPreferences;

    const-string v3, "initial_sync_completed"

    const/4 v4, 0x0

    invoke-interface {v2, v3, v4}, Landroid/content/SharedPreferences;->getBoolean(Ljava/lang/String;Z)Z

    move-result v2

    if-nez v2, :cond_2

    .line 205
    const/4 v1, 0x0

    goto :goto_0

    .line 208
    :cond_2
    invoke-direct {p0, v0}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getPromoVideo(Ljava/lang/String;)Ljava/util/List;

    move-result-object v1

    .line 209
    .local v1, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-interface {v1}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 210
    invoke-direct {p0, v0, p1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getWhatsNext(Ljava/lang/String;I)Ljava/util/List;

    move-result-object v1

    goto :goto_0
.end method

.method private getWhatsNext(Ljava/lang/String;I)Ljava/util/List;
    .locals 10
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "maxItems"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/String;",
            "I)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .prologue
    .line 265
    new-instance v5, Ljava/util/HashSet;

    invoke-direct {v5}, Ljava/util/HashSet;-><init>()V

    .line 266
    .local v5, "seenShows":Ljava/util/HashSet;, "Ljava/util/HashSet<Ljava/lang/String;>;"
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 267
    .local v4, "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 268
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Landroid/database/Cursor;>;"
    sget-object v7, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder$Query;->PROJECTION:[Ljava/lang/String;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v8

    invoke-static {p1, v7, v8, v9}, Lcom/google/android/videos/store/PurchaseRequests;->createNowPlayingRequestForUser(Ljava/lang/String;[Ljava/lang/String;J)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v3

    .line 270
    .local v3, "request":Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    iget-object v7, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    invoke-virtual {v7, v3, v0}, Lcom/google/android/videos/store/PurchaseStore;->getPurchases(Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;Lcom/google/android/videos/async/Callback;)V

    .line 272
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 274
    .local v1, "cursor":Landroid/database/Cursor;
    :cond_0
    :goto_0
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToNext()Z

    move-result v7

    if-eqz v7, :cond_2

    invoke-interface {v4}, Ljava/util/List;->size()I

    move-result v7

    if-ge v7, p2, :cond_2

    .line 275
    const/4 v7, 0x6

    invoke-interface {v1, v7}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v6

    .line 276
    .local v6, "showId":Ljava/lang/String;
    if-eqz v6, :cond_1

    .line 277
    const/16 v7, 0x8

    invoke-static {v1, v7}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v7

    if-eqz v7, :cond_0

    invoke-virtual {v5, v6}, Ljava/util/HashSet;->add(Ljava/lang/Object;)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 278
    invoke-direct {p0, v1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->convertShowToBundle(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    goto :goto_0

    .line 285
    .end local v6    # "showId":Ljava/lang/String;
    :catchall_0
    move-exception v7

    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v7
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    .line 288
    .end local v1    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 289
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    const-string v7, "Failed to fetch purchases"

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v8

    invoke-static {v7, v8}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 291
    const/4 v4, 0x0

    .end local v2    # "e":Ljava/util/concurrent/ExecutionException;
    .end local v4    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    :goto_1
    return-object v4

    .line 280
    .restart local v1    # "cursor":Landroid/database/Cursor;
    .restart local v4    # "result":Ljava/util/List;, "Ljava/util/List<Landroid/os/Bundle;>;"
    .restart local v6    # "showId":Ljava/lang/String;
    :cond_1
    const/4 v7, 0x5

    :try_start_3
    invoke-static {v1, v7}, Lcom/google/android/videos/utils/DbUtils;->getBoolean(Landroid/database/Cursor;I)Z

    move-result v7

    if-eqz v7, :cond_0

    .line 281
    invoke-direct {p0, v1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->convertToBundle(Landroid/database/Cursor;)Landroid/os/Bundle;

    move-result-object v7

    invoke-interface {v4, v7}, Ljava/util/List;->add(Ljava/lang/Object;)Z
    :try_end_3
    .catchall {:try_start_3 .. :try_end_3} :catchall_0

    goto :goto_0

    .line 285
    .end local v6    # "showId":Ljava/lang/String;
    :cond_2
    :try_start_4
    invoke-interface {v1}, Landroid/database/Cursor;->close()V
    :try_end_4
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_4 .. :try_end_4} :catch_0

    goto :goto_1
.end method

.method private selectDefaultAccount()Ljava/lang/String;
    .locals 7

    .prologue
    .line 217
    iget-object v6, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v6}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->getAccounts()[Landroid/accounts/Account;

    move-result-object v2

    .line 218
    .local v2, "allAccounts":[Landroid/accounts/Account;
    if-eqz v2, :cond_1

    .line 219
    move-object v3, v2

    .local v3, "arr$":[Landroid/accounts/Account;
    array-length v5, v3

    .local v5, "len$":I
    const/4 v4, 0x0

    .local v4, "i$":I
    :goto_0
    if-ge v4, v5, :cond_1

    aget-object v0, v3, v4

    .line 220
    .local v0, "account":Landroid/accounts/Account;
    iget-object v1, v0, Landroid/accounts/Account;->name:Ljava/lang/String;

    .line 221
    .local v1, "accountName":Ljava/lang/String;
    invoke-direct {p0, v1}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->silentSignIn(Ljava/lang/String;)Z

    move-result v6

    if-eqz v6, :cond_0

    .line 227
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :goto_1
    return-object v1

    .line 219
    .restart local v0    # "account":Landroid/accounts/Account;
    .restart local v1    # "accountName":Ljava/lang/String;
    .restart local v3    # "arr$":[Landroid/accounts/Account;
    .restart local v4    # "i$":I
    .restart local v5    # "len$":I
    :cond_0
    add-int/lit8 v4, v4, 0x1

    goto :goto_0

    .line 227
    .end local v0    # "account":Landroid/accounts/Account;
    .end local v1    # "accountName":Ljava/lang/String;
    .end local v3    # "arr$":[Landroid/accounts/Account;
    .end local v4    # "i$":I
    .end local v5    # "len$":I
    :cond_1
    const/4 v1, 0x0

    goto :goto_1
.end method

.method private silentSignIn(Ljava/lang/String;)Z
    .locals 1
    .param p1, "account"    # Ljava/lang/String;

    .prologue
    .line 412
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->accountManagerWrapper:Lcom/google/android/videos/accounts/AccountManagerWrapper;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/accounts/AccountManagerWrapper;->blockingAuthenticate(Ljava/lang/String;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 413
    iget-object v0, p0, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->signInManager:Lcom/google/android/videos/accounts/SignInManager;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/accounts/SignInManager;->setSignedInAccount(Ljava/lang/String;)V

    .line 414
    const/4 v0, 0x1

    .line 416
    :goto_0
    return v0

    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public getDocuments(II)Ljava/util/List;
    .locals 2
    .param p1, "dataTypeToFetch"    # I
    .param p2, "numItemsToReturn"    # I
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(II)",
            "Ljava/util/List",
            "<",
            "Landroid/os/Bundle;",
            ">;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Landroid/os/RemoteException;
        }
    .end annotation

    .prologue
    .line 168
    const-string v0, "com.android.vending"

    invoke-direct {p0, v0}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->enforceCallingPackage(Ljava/lang/String;)V

    .line 170
    const/4 v0, 0x5

    invoke-static {p2, v0}, Ljava/lang/Math;->min(II)I

    move-result p2

    .line 171
    packed-switch p1, :pswitch_data_0

    .line 175
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Unknown dataTypeToFetch: "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 176
    new-instance v0, Landroid/os/RemoteException;

    invoke-direct {v0}, Landroid/os/RemoteException;-><init>()V

    throw v0

    .line 173
    :pswitch_0
    invoke-direct {p0, p2}, Lcom/google/android/videos/store/VideoUserContentService$VideoUserContentBinder;->getWhatsNext(I)Ljava/util/List;

    move-result-object v0

    return-object v0

    .line 171
    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
    .end packed-switch
.end method
