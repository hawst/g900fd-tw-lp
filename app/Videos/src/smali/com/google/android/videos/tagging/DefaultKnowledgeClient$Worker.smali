.class final Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "Worker"
.end annotation

.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;,
        Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;
    }
.end annotation


# instance fields
.field private final callback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;"
        }
    .end annotation
.end field

.field private knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

.field private final knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

.field private final localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

.field private final localResourcesRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final localTagStreamRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private final parallelizeImageRequests:Z

.field private final resourcesCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;"
        }
    .end annotation
.end field

.field private final tagStreamCallback:Lcom/google/android/videos/async/Callback;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;"
        }
    .end annotation
.end field

.field private tagStreamParser:Lcom/google/android/videos/tagging/TagStreamParser;

.field final synthetic this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient;Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/async/Callback;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;Z)V
    .locals 1
    .param p2, "knowledgeRequest"    # Lcom/google/android/videos/tagging/KnowledgeRequest;
    .param p6, "localImageRequester"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    .param p7, "parallelizeImageRequests"    # Z
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/async/Callback",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeRequest;",
            "Lcom/google/android/videos/tagging/KnowledgeBundle;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;",
            "Lcom/google/android/videos/tagging/TagStreamParser;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;>;",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;",
            "Z)V"
        }
    .end annotation

    .prologue
    .line 160
    .local p3, "callback":Lcom/google/android/videos/async/Callback;, "Lcom/google/android/videos/async/Callback<Lcom/google/android/videos/tagging/KnowledgeRequest;Lcom/google/android/videos/tagging/KnowledgeBundle;>;"
    .local p4, "localTagStreamRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;>;"
    .local p5, "localResourcesRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 161
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    .line 162
    iput-object p3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/videos/async/Callback;

    .line 163
    iput-object p4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localTagStreamRequester:Lcom/google/android/videos/async/Requester;

    .line 164
    iput-boolean p7, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->parallelizeImageRequests:Z

    .line 165
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$1;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$1;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeClient;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamCallback:Lcom/google/android/videos/async/Callback;

    .line 175
    iput-object p5, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localResourcesRequester:Lcom/google/android/videos/async/Requester;

    .line 176
    new-instance v0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$2;

    invoke-direct {v0, p0, p1}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$2;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeClient;)V

    iput-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->resourcesCallback:Lcom/google/android/videos/async/Callback;

    .line 188
    iput-object p6, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    .line 189
    return-void
.end method

.method static synthetic access$000(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "x2"    # Lcom/google/android/videos/tagging/TagStreamParser;

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;)V

    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onTagStreamRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2, "x2"    # Ljava/util/Map;

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$500(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
    .param p1, "x1"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "x2"    # Ljava/lang/Exception;

    .prologue
    .line 139
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageRequestError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method static synthetic access$600(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    .prologue
    .line 139
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageLoadingFinished()V

    return-void
.end method

.method static synthetic access$700(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    .prologue
    .line 139
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    return-object v0
.end method

.method private end(Ljava/lang/Exception;)V
    .locals 3
    .param p1, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 241
    if-nez p1, :cond_0

    .line 242
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->setAlwaysRequest(Z)V

    .line 243
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Callback;->onResponse(Ljava/lang/Object;Ljava/lang/Object;)V

    .line 247
    :goto_0
    return-void

    .line 245
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->callback:Lcom/google/android/videos/async/Callback;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-interface {v0, v1, p1}, Lcom/google/android/videos/async/Callback;->onError(Ljava/lang/Object;Ljava/lang/Exception;)V

    goto :goto_0
.end method

.method private onImageLoadingFinished()V
    .locals 1

    .prologue
    .line 237
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    .line 238
    return-void
.end method

.method private onImageRequestError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 233
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error requesting knowledge bundle image "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;->localImageId:Ljava/lang/String;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 234
    return-void
.end method

.method private onResourcesRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/lang/Exception;)V
    .locals 2
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 212
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "Error requesting asset resources for knowledge request "

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v0, p2}, Lcom/google/android/videos/L;->e(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 213
    invoke-static {}, Ljava/util/Collections;->emptyMap()Ljava/util/Map;

    move-result-object v0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onResourcesResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V

    .line 214
    return-void
.end method

.method private onResourcesResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;Ljava/util/Map;)V
    .locals 4
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;",
            "Ljava/util/Map",
            "<",
            "Ljava/lang/String;",
            "Lcom/google/wireless/android/video/magma/proto/AssetResource;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 218
    .local p2, "response":Ljava/util/Map;, "Ljava/util/Map<Ljava/lang/String;Lcom/google/wireless/android/video/magma/proto/AssetResource;>;"
    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/videos/tagging/TagStreamParser;

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->this$0:Lcom/google/android/videos/tagging/DefaultKnowledgeClient;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->desiredPosterWidth:I
    invoke-static {v3}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient;->access$400(Lcom/google/android/videos/tagging/DefaultKnowledgeClient;)I

    move-result v3

    invoke-virtual {v2, p2, v3}, Lcom/google/android/videos/tagging/TagStreamParser;->build(Ljava/util/Map;I)Lcom/google/android/videos/tagging/TagStream;

    move-result-object v1

    .line 219
    .local v1, "tagStream":Lcom/google/android/videos/tagging/TagStream;
    new-instance v2, Lcom/google/android/videos/tagging/KnowledgeBundle;

    iget-object v3, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    invoke-direct {v2, v1, v3}, Lcom/google/android/videos/tagging/KnowledgeBundle;-><init>(Lcom/google/android/videos/tagging/TagStream;Lcom/google/android/videos/async/Requester;)V

    iput-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeBundle:Lcom/google/android/videos/tagging/KnowledgeBundle;

    .line 220
    invoke-virtual {v1}, Lcom/google/android/videos/tagging/TagStream;->getUniquePrefetchImages()Ljava/util/List;

    move-result-object v0

    .line 221
    .local v0, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v2

    if-eqz v2, :cond_0

    .line 222
    invoke-direct {p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageLoadingFinished()V

    .line 228
    :goto_0
    return-void

    .line 223
    :cond_0
    iget-boolean v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->parallelizeImageRequests:Z

    if-eqz v2, :cond_1

    .line 224
    new-instance v2, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;

    invoke-direct {v2, p0, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Ljava/util/List;)V

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->start()V

    goto :goto_0

    .line 226
    :cond_1
    new-instance v2, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;

    invoke-direct {v2, p0, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;-><init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Ljava/util/List;)V

    invoke-virtual {v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$SerialImageWorker;->start()V

    goto :goto_0
.end method

.method private onTagStreamRequestError(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 196
    invoke-direct {p0, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    .line 197
    return-void
.end method

.method private onTagStreamResponse(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;Lcom/google/android/videos/tagging/TagStreamParser;)V
    .locals 5
    .param p1, "request"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;
    .param p2, "response"    # Lcom/google/android/videos/tagging/TagStreamParser;

    .prologue
    .line 200
    if-nez p2, :cond_0

    .line 201
    const/4 v0, 0x0

    invoke-direct {p0, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->end(Ljava/lang/Exception;)V

    .line 207
    :goto_0
    return-void

    .line 204
    :cond_0
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/videos/tagging/TagStreamParser;

    .line 205
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localResourcesRequester:Lcom/google/android/videos/async/Requester;

    new-instance v1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;

    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->knowledgeRequest:Lcom/google/android/videos/tagging/KnowledgeRequest;

    iget v3, p1, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;->storage:I

    iget-object v4, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamParser:Lcom/google/android/videos/tagging/TagStreamParser;

    invoke-virtual {v4}, Lcom/google/android/videos/tagging/TagStreamParser;->getFilmographyIds()Ljava/util/List;

    move-result-object v4

    invoke-direct {v1, v2, v3, v4}, Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$FilmographyResourcesRequest;-><init>(Lcom/google/android/videos/tagging/KnowledgeRequest;ILjava/util/List;)V

    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->resourcesCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, v1, v2}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    goto :goto_0
.end method


# virtual methods
.method public start(Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;)V
    .locals 2
    .param p1, "tagStreamRequest"    # Lcom/google/android/videos/tagging/DefaultKnowledgeRequesters$TagStreamRequest;

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localTagStreamRequester:Lcom/google/android/videos/async/Requester;

    iget-object v1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->tagStreamCallback:Lcom/google/android/videos/async/Callback;

    invoke-interface {v0, p1, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 193
    return-void
.end method
