.class public Lcom/google/android/videos/pano/model/PromoItem;
.super Lcom/google/android/videos/pano/model/MovieItem;
.source "PromoItem.java"


# direct methods
.method private constructor <init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;)V
    .locals 15
    .param p1, "id"    # Ljava/lang/String;
    .param p2, "title"    # Ljava/lang/String;
    .param p3, "subtitle"    # Ljava/lang/String;
    .param p4, "posterUri"    # Ljava/lang/String;
    .param p5, "wallpaperUri"    # Ljava/lang/String;
    .param p6, "width"    # I
    .param p7, "height"    # I
    .param p8, "intent"    # Landroid/content/Intent;

    .prologue
    .line 40
    const-wide v10, 0x7fffffffffffffffL

    const/4 v12, 0x0

    const/4 v13, -0x1

    const/4 v14, 0x0

    move-object v1, p0

    move-object/from16 v2, p1

    move-object/from16 v3, p2

    move-object/from16 v4, p3

    move-object/from16 v5, p4

    move-object/from16 v6, p5

    move/from16 v7, p6

    move/from16 v8, p7

    move-object/from16 v9, p8

    invoke-direct/range {v1 .. v14}, Lcom/google/android/videos/pano/model/MovieItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;JIIF)V

    .line 42
    return-void
.end method

.method public static newInstance(Landroid/content/Context;Lcom/google/wireless/android/video/magma/proto/PromotionResource;)Lcom/google/android/videos/pano/model/PromoItem;
    .locals 17
    .param p0, "context"    # Landroid/content/Context;
    .param p1, "promotion"    # Lcom/google/wireless/android/video/magma/proto/PromotionResource;

    .prologue
    .line 23
    move-object/from16 v0, p1

    iget-object v1, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->asset:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v2, 0x0

    aget-object v14, v1, v2

    .line 24
    .local v14, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    invoke-static/range {p0 .. p0}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v4

    .line 25
    .local v4, "height":I
    int-to-float v1, v4

    const v2, 0x3f31a787

    mul-float/2addr v1, v2

    float-to-int v3, v1

    .line 26
    .local v3, "width":I
    new-instance v15, Lcom/google/android/videos/pano/model/PromoItem;

    iget-object v1, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v11, v1, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    iget-object v1, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    iget-object v12, v1, Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;->title:Ljava/lang/String;

    const v1, 0x7f0b01b1

    move-object/from16 v0, p0

    invoke-virtual {v0, v1}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v16

    iget-object v1, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v2, 0x3

    const/4 v5, 0x0

    const/4 v6, 0x0

    invoke-static/range {v1 .. v6}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v1

    iget-object v5, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    const/4 v6, 0x4

    const/4 v7, 0x0

    const/4 v8, 0x0

    const/4 v9, 0x0

    const/4 v10, 0x0

    invoke-static/range {v5 .. v10}, Lcom/google/android/videos/api/AssetResourceUtil;->findBestImageUrl(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIIFZ)Ljava/lang/String;

    move-result-object v10

    iget-object v2, v14, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    move-object/from16 v0, p1

    iget-object v5, v0, Lcom/google/wireless/android/video/magma/proto/PromotionResource;->promotionCode:Ljava/lang/String;

    move-object/from16 v0, p0

    invoke-static {v0, v2, v5}, Lcom/google/android/videos/pano/activity/FreeMoviePromoDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v13

    move-object v5, v15

    move-object v6, v11

    move-object v7, v12

    move-object/from16 v8, v16

    move-object v9, v1

    move v11, v3

    move v12, v4

    invoke-direct/range {v5 .. v13}, Lcom/google/android/videos/pano/model/PromoItem;-><init>(Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;IILandroid/content/Intent;)V

    return-object v15
.end method
