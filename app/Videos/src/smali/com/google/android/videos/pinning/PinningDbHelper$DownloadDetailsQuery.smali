.class interface abstract Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetailsQuery;
.super Ljava/lang/Object;
.source "PinningDbHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pinning/PinningDbHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x60a
    name = "DownloadDetailsQuery"
.end annotation


# static fields
.field public static final PROJECTION:[Ljava/lang/String;


# direct methods
.method static constructor <clinit>()V
    .locals 3

    .prologue
    .line 340
    const/16 v0, 0x10

    new-array v0, v0, [Ljava/lang/String;

    const/4 v1, 0x0

    const-string v2, "download_relative_filepath"

    aput-object v2, v0, v1

    const/4 v1, 0x1

    const-string v2, "pinning_download_size"

    aput-object v2, v0, v1

    const/4 v1, 0x2

    const-string v2, "download_quality"

    aput-object v2, v0, v1

    const/4 v1, 0x3

    const-string v2, "download_extra_proto"

    aput-object v2, v0, v1

    const/4 v1, 0x4

    const-string v2, "license_type"

    aput-object v2, v0, v1

    const/4 v1, 0x5

    const-string v2, "license_activation"

    aput-object v2, v0, v1

    const/4 v1, 0x6

    const-string v2, "license_cenc_key_set_id"

    aput-object v2, v0, v1

    const/4 v1, 0x7

    const-string v2, "license_cenc_pssh_data"

    aput-object v2, v0, v1

    const/16 v1, 0x8

    const-string v2, "license_cenc_security_level"

    aput-object v2, v0, v1

    const/16 v1, 0x9

    const-string v2, "have_subtitles"

    aput-object v2, v0, v1

    const/16 v1, 0xa

    const-string v2, "asset_type = 20"

    aput-object v2, v0, v1

    const/16 v1, 0xb

    const-string v2, "external_storage_index"

    aput-object v2, v0, v1

    const/16 v1, 0xc

    const-string v2, "ifnull(min(expiration_timestamp_seconds * 1000,ifnull(license_expiration_timestamp,9223372036854775807)),9223372036854775807)"

    aput-object v2, v0, v1

    const/16 v1, 0xd

    const-string v2, "duration_seconds * 1000"

    aput-object v2, v0, v1

    const/16 v1, 0xe

    const-string v2, "download_last_modified"

    aput-object v2, v0, v1

    const/16 v1, 0xf

    const-string v2, "license_video_format"

    aput-object v2, v0, v1

    sput-object v0, Lcom/google/android/videos/pinning/PinningDbHelper$DownloadDetailsQuery;->PROJECTION:[Ljava/lang/String;

    return-void
.end method
