.class public Lcom/google/android/videos/logging/RootUiElementNodeImpl;
.super Ljava/lang/Object;
.source "RootUiElementNodeImpl.java"

# interfaces
.implements Lcom/google/android/videos/logging/RootUiElementNode;


# instance fields
.field private final impressionHandler:Landroid/os/Handler;

.field private impressionId:J

.field private final uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;


# direct methods
.method public constructor <init>(ILcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 2
    .param p1, "uiElementType"    # I
    .param p2, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .prologue
    .line 23
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 24
    if-lez p1, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 25
    invoke-static {p2}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iput-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 26
    new-instance v0, Lcom/google/android/videos/logging/UiElementWrapper;

    invoke-direct {v0}, Lcom/google/android/videos/logging/UiElementWrapper;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    .line 27
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    iget-object v0, v0, Lcom/google/android/videos/logging/UiElementWrapper;->uiElement:Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;

    iput p1, v0, Lcom/google/wireless/android/play/playlog/proto/PlayMovies$PlayMoviesLogEvent$PlayMoviesUiElement;->type:I

    .line 28
    new-instance v0, Landroid/os/Handler;

    invoke-static {}, Landroid/os/Looper;->getMainLooper()Landroid/os/Looper;

    move-result-object v1

    invoke-direct {v0, v1}, Landroid/os/Handler;-><init>(Landroid/os/Looper;)V

    iput-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionHandler:Landroid/os/Handler;

    .line 29
    return-void

    .line 24
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method


# virtual methods
.method public final childImpression(Lcom/google/android/videos/logging/UiElementNode;)V
    .locals 4
    .param p1, "childNode"    # Lcom/google/android/videos/logging/UiElementNode;

    .prologue
    .line 45
    if-eqz p1, :cond_0

    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v0

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(Z)V

    .line 46
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-interface {p1}, Lcom/google/android/videos/logging/UiElementNode;->getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;

    move-result-object v1

    invoke-virtual {v0, p0, v1}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->addChild(Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiElementWrapper;)Z

    .line 47
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iget-object v1, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionId:J

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendImpressionDelayed(Landroid/os/Handler;JLcom/google/android/videos/logging/UiElementNode;)V

    .line 48
    return-void

    .line 45
    :cond_0
    const/4 v0, 0x0

    goto :goto_0
.end method

.method public final flushImpression()V
    .locals 4

    .prologue
    .line 59
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    iget-object v1, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionHandler:Landroid/os/Handler;

    iget-wide v2, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionId:J

    invoke-virtual {v0, v1, v2, v3, p0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->flushImpression(Landroid/os/Handler;JLcom/google/android/videos/logging/RootUiElementNode;)V

    .line 60
    return-void
.end method

.method public final getParentNode()Lcom/google/android/videos/logging/UiElementNode;
    .locals 1

    .prologue
    .line 40
    const/4 v0, 0x0

    return-object v0
.end method

.method public final getUiElementWrapper()Lcom/google/android/videos/logging/UiElementWrapper;
    .locals 1

    .prologue
    .line 35
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiElementWrapper:Lcom/google/android/videos/logging/UiElementWrapper;

    return-object v0
.end method

.method public final startNewImpression()V
    .locals 2

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->getNextImpressionId()J

    move-result-wide v0

    iput-wide v0, p0, Lcom/google/android/videos/logging/RootUiElementNodeImpl;->impressionId:J

    .line 55
    return-void
.end method
