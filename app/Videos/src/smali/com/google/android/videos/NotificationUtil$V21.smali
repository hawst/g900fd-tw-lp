.class Lcom/google/android/videos/NotificationUtil$V21;
.super Lcom/google/android/videos/NotificationUtil$V16;
.source "NotificationUtil.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/NotificationUtil;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0xa
    name = "V21"
.end annotation


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 0
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 108
    invoke-direct {p0, p1}, Lcom/google/android/videos/NotificationUtil$V16;-><init>(Landroid/content/Context;)V

    .line 109
    return-void
.end method


# virtual methods
.method protected getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;
    .locals 3
    .param p1, "poster"    # Landroid/graphics/Bitmap;

    .prologue
    .line 113
    invoke-super {p0, p1}, Lcom/google/android/videos/NotificationUtil$V16;->getDownloadBuilder(Landroid/graphics/Bitmap;)Landroid/app/Notification$Builder;

    move-result-object v0

    .line 114
    .local v0, "downloadBuilder":Landroid/app/Notification$Builder;
    iget-object v1, p0, Lcom/google/android/videos/NotificationUtil$V21;->context:Landroid/content/Context;

    invoke-virtual {v1}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v2

    if-eqz p1, :cond_0

    const v1, 0x7f0a0079

    :goto_0
    invoke-virtual {v2, v1}, Landroid/content/res/Resources;->getColor(I)I

    move-result v1

    invoke-virtual {v0, v1}, Landroid/app/Notification$Builder;->setColor(I)Landroid/app/Notification$Builder;

    .line 116
    return-object v0

    .line 114
    :cond_0
    const v1, 0x7f0a0085

    goto :goto_0
.end method
