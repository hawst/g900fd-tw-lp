.class public Lcom/google/android/videos/ui/MoviesHelper;
.super Lcom/google/android/videos/store/Database$BaseListener;
.source "MoviesHelper.java"

# interfaces
.implements Lcom/google/android/repolib/observers/Updatable;
.implements Lcom/google/android/videos/activity/HomeFragment$HomeFragmentHelper;
.implements Lcom/google/android/videos/ui/CursorHelper$Listener;
.implements Lcom/google/android/videos/ui/SyncHelper$Listener;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/ui/MoviesHelper$1;,
        Lcom/google/android/videos/ui/MoviesHelper$AssetListener;,
        Lcom/google/android/videos/ui/MoviesHelper$PreorderListener;
    }
.end annotation


# instance fields
.field private final activity:Lcom/google/android/videos/activity/HomeActivity;

.field private final config:Lcom/google/android/videos/Config;

.field private final database:Lcom/google/android/videos/store/Database;

.field private final eventLogger:Lcom/google/android/videos/logging/EventLogger;

.field private final libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;"
        }
    .end annotation
.end field

.field private final libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final listView:Landroid/support/v7/widget/RecyclerView;

.field private final moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

.field private final networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

.field private pendingLibraryCursor:Z

.field private pendingPostersUpdated:Z

.field private pendingPreorderCursor:Z

.field private final pinHelper:Lcom/google/android/videos/ui/PinHelper;

.field private final preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;"
        }
    .end annotation
.end field

.field private final preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

.field private final progressBar:Landroid/view/View;

.field private final syncHelper:Lcom/google/android/videos/ui/SyncHelper;

.field private final uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

.field private final welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/Config;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/activity/HomeActivity;Landroid/content/SharedPreferences;Landroid/view/View;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/ui/CursorHelper;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/store/PosterStore;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/ui/PinHelper;Lcom/google/android/videos/bitmap/BitmapRequesters;Lcom/google/android/videos/api/ApiRequesters;Lcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/utils/NetworkStatus;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V
    .locals 21
    .param p1, "config"    # Lcom/google/android/videos/Config;
    .param p2, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .param p3, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;
    .param p4, "activity"    # Lcom/google/android/videos/activity/HomeActivity;
    .param p5, "preferences"    # Landroid/content/SharedPreferences;
    .param p6, "moviesView"    # Landroid/view/View;
    .param p9, "database"    # Lcom/google/android/videos/store/Database;
    .param p10, "purchaseStoreSync"    # Lcom/google/android/videos/store/PurchaseStoreSync;
    .param p11, "posterStore"    # Lcom/google/android/videos/store/PosterStore;
    .param p12, "eventLogger"    # Lcom/google/android/videos/logging/EventLogger;
    .param p13, "errorHelper"    # Lcom/google/android/videos/utils/ErrorHelper;
    .param p14, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p15, "pinHelper"    # Lcom/google/android/videos/ui/PinHelper;
    .param p16, "bitmapRequesters"    # Lcom/google/android/videos/bitmap/BitmapRequesters;
    .param p17, "apiRequesters"    # Lcom/google/android/videos/api/ApiRequesters;
    .param p18, "parentNode"    # Lcom/google/android/videos/logging/UiElementNode;
    .param p19, "networkStatus"    # Lcom/google/android/videos/utils/NetworkStatus;
    .param p20, "uiEventLoggingHelper"    # Lcom/google/android/videos/logging/UiEventLoggingHelper;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/Config;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/utils/DownloadedOnlyManager;",
            "Lcom/google/android/videos/activity/HomeActivity;",
            "Landroid/content/SharedPreferences;",
            "Landroid/view/View;",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;",
            "Lcom/google/android/videos/store/Database;",
            "Lcom/google/android/videos/store/PurchaseStoreSync;",
            "Lcom/google/android/videos/store/PosterStore;",
            "Lcom/google/android/videos/logging/EventLogger;",
            "Lcom/google/android/videos/utils/ErrorHelper;",
            "Lcom/google/android/videos/ui/SyncHelper;",
            "Lcom/google/android/videos/ui/PinHelper;",
            "Lcom/google/android/videos/bitmap/BitmapRequesters;",
            "Lcom/google/android/videos/api/ApiRequesters;",
            "Lcom/google/android/videos/logging/UiElementNode;",
            "Lcom/google/android/videos/utils/NetworkStatus;",
            "Lcom/google/android/videos/logging/UiEventLoggingHelper;",
            ")V"
        }
    .end annotation

    .prologue
    .line 94
    .local p7, "libraryCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    .local p8, "preorderCursorHelper":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    invoke-direct/range {p0 .. p0}, Lcom/google/android/videos/store/Database$BaseListener;-><init>()V

    .line 95
    move-object/from16 v0, p4

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    .line 96
    move-object/from16 v0, p1

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->config:Lcom/google/android/videos/Config;

    .line 97
    move-object/from16 v0, p9

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->database:Lcom/google/android/videos/store/Database;

    .line 98
    move-object/from16 v0, p12

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    .line 99
    move-object/from16 v0, p20

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    .line 100
    move-object/from16 v0, p14

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    .line 101
    move-object/from16 v0, p15

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    .line 103
    move-object/from16 v0, p7

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    .line 104
    move-object/from16 v0, p8

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    .line 105
    move-object/from16 v0, p19

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    .line 107
    const v2, 0x7f0f0120

    move-object/from16 v0, p4

    invoke-virtual {v0, v2}, Lcom/google/android/videos/activity/HomeActivity;->findViewById(I)Landroid/view/View;

    move-result-object v2

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->progressBar:Landroid/view/View;

    .line 109
    const v2, 0x7f0f0029

    move-object/from16 v0, p6

    invoke-virtual {v0, v2}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v2

    check-cast v2, Landroid/support/v7/widget/RecyclerView;

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    .line 110
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v3}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 112
    new-instance v2, Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-direct {v2}, Lcom/google/android/videos/adapter/MoviesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 113
    new-instance v2, Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-direct {v2}, Lcom/google/android/videos/adapter/MoviesDataSource;-><init>()V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    .line 115
    new-instance v16, Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-interface/range {p16 .. p16}, Lcom/google/android/videos/bitmap/BitmapRequesters;->getBitmapRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v17

    const/16 v18, 0x0

    const/4 v2, 0x2

    new-array v0, v2, [Lcom/google/android/videos/welcome/Welcome;

    move-object/from16 v19, v0

    const/16 v20, 0x0

    new-instance v2, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;

    const-string v3, "movies"

    invoke-virtual/range {p4 .. p4}, Lcom/google/android/videos/activity/HomeActivity;->getStoreStatusMonitor()Lcom/google/android/videos/store/StoreStatusMonitor;

    move-result-object v10

    invoke-interface/range {p17 .. p17}, Lcom/google/android/videos/api/ApiRequesters;->getPromotionsRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v12

    invoke-interface/range {p17 .. p17}, Lcom/google/android/videos/api/ApiRequesters;->getRedeemPromotionRequester()Lcom/google/android/videos/async/Requester;

    move-result-object v13

    const/4 v14, 0x0

    move-object/from16 v4, p4

    move-object/from16 v5, p1

    move-object/from16 v6, p5

    move-object/from16 v7, p2

    move-object/from16 v8, p13

    move-object/from16 v9, p12

    move-object/from16 v11, p10

    invoke-direct/range {v2 .. v14}, Lcom/google/android/videos/welcome/FreeMoviePromoWelcome;-><init>(Ljava/lang/String;Landroid/content/Context;Lcom/google/android/videos/Config;Landroid/content/SharedPreferences;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/utils/ErrorHelper;Lcom/google/android/videos/logging/EventLogger;Lcom/google/android/videos/store/StoreStatusMonitor;Lcom/google/android/videos/store/PurchaseStoreSync;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Z)V

    aput-object v2, v19, v20

    const/4 v2, 0x1

    new-instance v3, Lcom/google/android/videos/welcome/DefaultMoviesWelcome;

    move-object/from16 v0, p4

    move-object/from16 v1, p1

    invoke-direct {v3, v0, v1}, Lcom/google/android/videos/welcome/DefaultMoviesWelcome;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;)V

    aput-object v3, v19, v2

    invoke-direct/range {v16 .. v19}, Lcom/google/android/videos/welcome/WelcomeFlow;-><init>(Lcom/google/android/videos/async/Requester;I[Lcom/google/android/videos/welcome/Welcome;)V

    move-object/from16 v0, v16

    move-object/from16 v1, p0

    iput-object v0, v1, Lcom/google/android/videos/ui/MoviesHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    .line 124
    new-instance v8, Lcom/google/android/videos/ui/MoviesHelper$PreorderListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v8, v0, v2}, Lcom/google/android/videos/ui/MoviesHelper$PreorderListener;-><init>(Lcom/google/android/videos/ui/MoviesHelper;Lcom/google/android/videos/ui/MoviesHelper$1;)V

    .line 125
    .local v8, "preorderClickListener":Lcom/google/android/videos/ui/MoviesHelper$PreorderListener;
    new-instance v9, Lcom/google/android/videos/ui/MoviesHelper$AssetListener;

    const/4 v2, 0x0

    move-object/from16 v0, p0

    invoke-direct {v9, v0, v2}, Lcom/google/android/videos/ui/MoviesHelper$AssetListener;-><init>(Lcom/google/android/videos/ui/MoviesHelper;Lcom/google/android/videos/ui/MoviesHelper$1;)V

    .line 126
    .local v9, "assetListener":Lcom/google/android/videos/ui/MoviesHelper$AssetListener;
    new-instance v11, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;

    const/4 v2, 0x0

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    invoke-interface/range {p1 .. p1}, Lcom/google/android/videos/Config;->allowDownloads()Z

    move-result v3

    new-instance v4, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v5, 0x9

    move-object/from16 v0, p18

    move-object/from16 v1, p20

    invoke-direct {v4, v5, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-direct {v11, v2, v3, v4}, Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;ZLcom/google/android/videos/logging/UiElementNode;)V

    .line 130
    .local v11, "moviesBinder":Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;
    new-instance v10, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;

    const/4 v2, 0x0

    move-object/from16 v0, p11

    invoke-virtual {v0, v2}, Lcom/google/android/videos/store/PosterStore;->getRequester(I)Lcom/google/android/videos/async/Requester;

    move-result-object v2

    new-instance v3, Lcom/google/android/videos/logging/GenericUiElementNode;

    const/16 v4, 0xf

    move-object/from16 v0, p18

    move-object/from16 v1, p20

    invoke-direct {v3, v4, v0, v1}, Lcom/google/android/videos/logging/GenericUiElementNode;-><init>(ILcom/google/android/videos/logging/UiElementNode;Lcom/google/android/videos/logging/UiEventLoggingHelper;)V

    invoke-direct {v10, v2, v3}, Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;-><init>(Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/logging/UiElementNode;)V

    .line 135
    .local v10, "preorderBinder":Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;
    new-instance v2, Lcom/google/android/videos/ui/MoviesFlowHelper;

    move-object/from16 v0, p0

    iget-object v5, v0, Lcom/google/android/videos/ui/MoviesHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    move-object/from16 v0, p0

    iget-object v6, v0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v0, p0

    iget-object v7, v0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    move-object/from16 v3, p4

    move-object/from16 v4, p3

    invoke-direct/range {v2 .. v11}, Lcom/google/android/videos/ui/MoviesFlowHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/utils/DownloadedOnlyManager;Lcom/google/android/videos/welcome/WelcomeFlow;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/adapter/MoviesDataSource;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/ItemsWithHeadingFlow$OnItemClickListener;Lcom/google/android/videos/ui/playnext/MoviePreorderClusterItemView$Binder;Lcom/google/android/videos/ui/playnext/MovieClusterItemView$Binder;)V

    move-object/from16 v0, p0

    iput-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    .line 145
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/MoviesFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    const/4 v3, 0x0

    invoke-virtual {v2, v3}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 147
    new-instance v15, Lcom/google/android/videos/flow/FlowAdapter;

    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/MoviesFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-direct {v15, v2}, Lcom/google/android/videos/flow/FlowAdapter;-><init>(Lcom/google/android/videos/flow/Flow;)V

    .line 148
    .local v15, "flowAdapter":Lcom/google/android/videos/flow/FlowAdapter;
    const/4 v2, 0x1

    invoke-virtual {v15, v2}, Lcom/google/android/videos/flow/FlowAdapter;->setHasStableIds(Z)V

    .line 149
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v3, Lcom/google/android/videos/ui/DebugFlowLayoutManager;

    const-string v4, "MoviesFragment"

    invoke-direct {v3, v4}, Lcom/google/android/videos/ui/DebugFlowLayoutManager;-><init>(Ljava/lang/String;)V

    invoke-virtual {v2, v3}, Landroid/support/v7/widget/RecyclerView;->setLayoutManager(Landroid/support/v7/widget/RecyclerView$LayoutManager;)V

    .line 150
    move-object/from16 v0, p0

    iget-object v2, v0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v2, v15}, Landroid/support/v7/widget/RecyclerView;->setAdapter(Landroid/support/v7/widget/RecyclerView$Adapter;)V

    .line 151
    return-void
.end method

.method static synthetic access$200(Lcom/google/android/videos/ui/MoviesHelper;ILandroid/view/View;)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/MoviesHelper;
    .param p1, "x1"    # I
    .param p2, "x2"    # Landroid/view/View;

    .prologue
    .line 51
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/MoviesHelper;->onPreorderClick(ILandroid/view/View;)V

    return-void
.end method

.method static synthetic access$300(Lcom/google/android/videos/ui/MoviesHelper;Lcom/google/android/videos/adapter/MoviesDataSource;Landroid/view/View;I)V
    .locals 0
    .param p0, "x0"    # Lcom/google/android/videos/ui/MoviesHelper;
    .param p1, "x1"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "x2"    # Landroid/view/View;
    .param p3, "x3"    # I

    .prologue
    .line 51
    invoke-direct {p0, p1, p2, p3}, Lcom/google/android/videos/ui/MoviesHelper;->onAssetClick(Lcom/google/android/videos/adapter/MoviesDataSource;Landroid/view/View;I)V

    return-void
.end method

.method private allowCursorChanges()Z
    .locals 1

    .prologue
    .line 308
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0}, Landroid/support/v7/widget/RecyclerView;->isShown()Z

    move-result v0

    if-nez v0, :cond_1

    :cond_0
    const/4 v0, 0x1

    :goto_0
    return v0

    :cond_1
    const/4 v0, 0x0

    goto :goto_0
.end method

.method private onAssetClick(Lcom/google/android/videos/adapter/MoviesDataSource;Landroid/view/View;I)V
    .locals 10
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/MoviesDataSource;
    .param p2, "view"    # Landroid/view/View;
    .param p3, "position"    # I

    .prologue
    .line 226
    invoke-virtual {p1, p3}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v2

    .line 227
    .local v2, "cursor":Landroid/database/Cursor;
    invoke-virtual {p1, v2}, Lcom/google/android/videos/adapter/MoviesDataSource;->isActive(Landroid/database/Cursor;)Z

    move-result v6

    if-nez v6, :cond_0

    .line 228
    invoke-virtual {p1, v2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v5

    .line 229
    .local v5, "videoId":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v6}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 230
    .local v0, "account":Ljava/lang/String;
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    const/16 v7, 0xf

    invoke-static {v6, v5, v0, v7}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 254
    .end local v0    # "account":Ljava/lang/String;
    .end local v5    # "videoId":Ljava/lang/String;
    :goto_0
    return-void

    .line 234
    :cond_0
    invoke-virtual {p2}, Landroid/view/View;->getId()I

    move-result v6

    const v7, 0x7f0f00d1

    if-ne v6, v7, :cond_1

    .line 235
    invoke-direct {p0, p1, v2}, Lcom/google/android/videos/ui/MoviesHelper;->onPinClicked(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V

    goto :goto_0

    .line 237
    :cond_1
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-static {p2}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v7

    invoke-virtual {v6, v7}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 239
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v7, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v7}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v7

    iget-object v8, p0, Lcom/google/android/videos/ui/MoviesHelper;->config:Lcom/google/android/videos/Config;

    invoke-virtual {p1, v2, v8}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoMetadata(Landroid/database/Cursor;Lcom/google/android/videos/Config;)Lcom/google/android/videos/store/VideoMetadata;

    move-result-object v8

    invoke-virtual {p1, v2}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoDownloadStatus(Landroid/database/Cursor;)Lcom/google/android/videos/store/VideoDownloadStatus;

    move-result-object v9

    invoke-static {v6, v7, v8, v9}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;Lcom/google/android/videos/store/VideoMetadata;Lcom/google/android/videos/store/VideoDownloadStatus;)Landroid/content/Intent;

    move-result-object v3

    .line 242
    .local v3, "intent":Landroid/content/Intent;
    const-string v6, "up_is_back"

    const/4 v7, 0x1

    invoke-virtual {v3, v6, v7}, Landroid/content/Intent;->putExtra(Ljava/lang/String;Z)Landroid/content/Intent;

    .line 243
    sget v6, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v7, 0x15

    if-lt v6, v7, :cond_2

    .line 244
    new-instance v4, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;

    invoke-direct {v4}, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;-><init>()V

    .line 245
    .local v4, "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    const v6, 0x7f0f00c9

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->poster:Landroid/view/View;

    .line 246
    const v6, 0x7f0f00da

    invoke-virtual {p2, v6}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->infoArea:Landroid/view/View;

    .line 247
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v6}, Lcom/google/android/videos/activity/HomeActivity;->getCurrentPlayHeaderListLayout()Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    move-result-object v6

    iput-object v6, v4, Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;->playHeaderListLayout:Lcom/google/android/play/headerlist/PlayHeaderListLayout;

    .line 248
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-static {v6, v3, v4}, Lcom/google/android/videos/activity/MovieDetailsActivity;->createAnimationBundleV21(Landroid/app/Activity;Landroid/content/Intent;Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;)Landroid/os/Bundle;

    move-result-object v1

    .line 249
    .local v1, "bundle":Landroid/os/Bundle;
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-static {v6, v3, v1}, Landroid/support/v4/app/ActivityCompat;->startActivity(Landroid/app/Activity;Landroid/content/Intent;Landroid/os/Bundle;)V

    goto :goto_0

    .line 251
    .end local v1    # "bundle":Landroid/os/Bundle;
    .end local v4    # "shared":Lcom/google/android/videos/activity/MovieDetailsActivity$SharedElements;
    :cond_2
    iget-object v6, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v6, v3}, Lcom/google/android/videos/activity/HomeActivity;->startActivity(Landroid/content/Intent;)V

    goto :goto_0
.end method

.method private onPinClicked(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 266
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v3

    .line 267
    .local v3, "videoId":Ljava/lang/String;
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isPinned(Landroid/database/Cursor;)Z

    move-result v0

    if-eqz v0, :cond_0

    .line 268
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getTitle(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Integer;->intValue()I

    move-result v5

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v6

    invoke-virtual/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->onUnpinClicked(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Integer;)V

    .line 282
    :goto_0
    return-void

    .line 275
    :cond_0
    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatus(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v0

    const/4 v1, 0x4

    invoke-static {v1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-static {v0, v1}, Lcom/google/android/videos/utils/Util;->areEqual(Ljava/lang/Object;Ljava/lang/Object;)Z

    move-result v0

    if-eqz v0, :cond_1

    .line 277
    invoke-direct {p0, p1, p2}, Lcom/google/android/videos/ui/MoviesHelper;->showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V

    goto :goto_0

    .line 279
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pinHelper:Lcom/google/android/videos/ui/PinHelper;

    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v2

    invoke-virtual {v0, v1, v2, v3}, Lcom/google/android/videos/ui/PinHelper;->pinVideo(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;)V

    .line 280
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->eventLogger:Lcom/google/android/videos/logging/EventLogger;

    const/4 v1, 0x0

    invoke-interface {v0, v1}, Lcom/google/android/videos/logging/EventLogger;->onPinClick(Z)V

    goto :goto_0
.end method

.method private onPreorderClick(ILandroid/view/View;)V
    .locals 5
    .param p1, "position"    # I
    .param p2, "view"    # Landroid/view/View;

    .prologue
    .line 257
    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->uiEventLoggingHelper:Lcom/google/android/videos/logging/UiEventLoggingHelper;

    invoke-static {p2}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->findUiElementNode(Landroid/view/View;)Lcom/google/android/videos/logging/UiElementNode;

    move-result-object v3

    invoke-virtual {v2, v3}, Lcom/google/android/videos/logging/UiEventLoggingHelper;->sendClickEvent(Lcom/google/android/videos/logging/UiElementNode;)V

    .line 258
    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v2, p1}, Lcom/google/android/videos/adapter/MoviesDataSource;->getItem(I)Landroid/database/Cursor;

    move-result-object v0

    .line 259
    .local v0, "cursor":Landroid/database/Cursor;
    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v2, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v1

    .line 261
    .local v1, "videoId":Ljava/lang/String;
    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    iget-object v3, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v3}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v3

    const/16 v4, 0x15

    invoke-static {v2, v1, v3, v4}, Lcom/google/android/videos/utils/PlayStoreUtil;->viewMovieDetails(Landroid/app/Activity;Ljava/lang/String;Ljava/lang/String;I)V

    .line 263
    return-void
.end method

.method private refreshLibraryCursor()V
    .locals 2

    .prologue
    .line 206
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->allowCursorChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 207
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingLibraryCursor:Z

    .line 208
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 209
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->updateVisibilities()V

    .line 213
    :goto_0
    return-void

    .line 211
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingLibraryCursor:Z

    goto :goto_0
.end method

.method private refreshPreorderCursor()V
    .locals 2

    .prologue
    .line 216
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->allowCursorChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 217
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPreorderCursor:Z

    .line 218
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/CursorHelper;->getCursor()Landroid/database/Cursor;

    move-result-object v1

    invoke-virtual {v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 219
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->updateVisibilities()V

    .line 223
    :goto_0
    return-void

    .line 221
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPreorderCursor:Z

    goto :goto_0
.end method

.method private showErrorDialog(Lcom/google/android/videos/adapter/VideosDataSource;Landroid/database/Cursor;)V
    .locals 7
    .param p1, "dataSource"    # Lcom/google/android/videos/adapter/VideosDataSource;
    .param p2, "cursor"    # Landroid/database/Cursor;

    .prologue
    .line 285
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v1

    .line 286
    .local v1, "account":Ljava/lang/String;
    if-nez v1, :cond_0

    .line 295
    :goto_0
    return-void

    .line 289
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getVideoId(Landroid/database/Cursor;)Ljava/lang/String;

    move-result-object v2

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningStatusReason(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/Integer;->intValue()I

    move-result v3

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getDownloadSize(Landroid/database/Cursor;)Ljava/lang/Long;

    move-result-object v4

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->getPinningDrmErrorCode(Landroid/database/Cursor;)Ljava/lang/Integer;

    move-result-object v5

    invoke-interface {p1, p2}, Lcom/google/android/videos/adapter/VideosDataSource;->isRental(Landroid/database/Cursor;)Z

    move-result v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/ui/PinHelper;->showErrorDialog(Landroid/support/v4/app/FragmentActivity;Ljava/lang/String;Ljava/lang/String;ILjava/lang/Long;Ljava/lang/Integer;Z)V

    goto :goto_0
.end method

.method private updateVisibilities()V
    .locals 4

    .prologue
    const/4 v3, 0x0

    const/16 v2, 0x8

    .line 312
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    .line 313
    .local v0, "syncState":I
    packed-switch v0, :pswitch_data_0

    .line 343
    :goto_0
    return-void

    .line 317
    :pswitch_0
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 318
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 327
    :pswitch_1
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1}, Lcom/google/android/videos/adapter/MoviesDataSource;->hasCursor()Z

    move-result v1

    if-eqz v1, :cond_1

    .line 328
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v2}, Landroid/view/View;->setVisibility(I)V

    .line 329
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v1

    if-eqz v1, :cond_0

    .line 330
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/MoviesFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v1

    const/4 v2, 0x1

    invoke-virtual {v1, v2}, Lcom/google/android/videos/flow/Flow;->setVisible(Z)V

    .line 331
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v1, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsReadyForTransitionV21(Ljava/lang/Object;)V

    .line 335
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v3}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    goto :goto_0

    .line 333
    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v2}, Lcom/google/android/videos/ui/MoviesFlowHelper;->getFlow()Lcom/google/android/videos/flow/Flow;

    move-result-object v2

    invoke-static {v1, v2}, Lcom/google/android/videos/ui/FlowAnimationUtil;->animateFlowAppearing(Landroid/support/v7/widget/RecyclerView;Lcom/google/android/videos/flow/Flow;)V

    goto :goto_1

    .line 337
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v1, v2}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 338
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v1, v3}, Landroid/view/View;->setVisibility(I)V

    goto :goto_0

    .line 313
    nop

    :pswitch_data_0
    .packed-switch 0x0
        :pswitch_0
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
        :pswitch_0
        :pswitch_1
        :pswitch_1
        :pswitch_1
    .end packed-switch
.end method


# virtual methods
.method public onCursorChanged(Lcom/google/android/videos/ui/CursorHelper;)V
    .locals 1
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Lcom/google/android/videos/ui/CursorHelper",
            "<*>;)V"
        }
    .end annotation

    .prologue
    .line 198
    .local p1, "source":Lcom/google/android/videos/ui/CursorHelper;, "Lcom/google/android/videos/ui/CursorHelper<*>;"
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    if-ne p1, v0, :cond_1

    .line 199
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshLibraryCursor()V

    .line 203
    :cond_0
    :goto_0
    return-void

    .line 200
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    if-ne p1, v0, :cond_0

    .line 201
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshPreorderCursor()V

    goto :goto_0
.end method

.method public onDestroy()V
    .locals 1

    .prologue
    .line 192
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    .line 193
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onDestroy()V

    .line 194
    return-void
.end method

.method public onPosterUpdated(Ljava/lang/String;)V
    .locals 0
    .param p1, "videoId"    # Ljava/lang/String;

    .prologue
    .line 380
    invoke-virtual {p0}, Lcom/google/android/videos/ui/MoviesHelper;->onPostersUpdated()V

    .line 381
    return-void
.end method

.method public onPostersUpdated()V
    .locals 1

    .prologue
    .line 298
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->allowCursorChanges()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 299
    const/4 v0, 0x0

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPostersUpdated:Z

    .line 300
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->notifyChanged()V

    .line 301
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->notifyChanged()V

    .line 305
    :goto_0
    return-void

    .line 303
    :cond_0
    const/4 v0, 0x1

    iput-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPostersUpdated:Z

    goto :goto_0
.end method

.method public final onStart()V
    .locals 2

    .prologue
    .line 155
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0}, Lcom/google/android/videos/activity/HomeActivity;->isTransitioning()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 156
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->activity:Lcom/google/android/videos/activity/HomeActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/activity/HomeActivity;->markAsPreparingForTransitionV21(Ljava/lang/Object;)V

    .line 157
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 159
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->addListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 160
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->addListener(Lcom/google/android/videos/store/Database$Listener;)V

    .line 161
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 162
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 163
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->addListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 164
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStart()V

    .line 165
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshLibraryCursor()V

    .line 166
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshPreorderCursor()V

    .line 167
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->onStart()V

    .line 168
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->addUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 169
    invoke-virtual {p0}, Lcom/google/android/videos/ui/MoviesHelper;->update()V

    .line 171
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/SyncHelper;->getState()I

    move-result v0

    invoke-virtual {p0, v0}, Lcom/google/android/videos/ui/MoviesHelper;->onSyncStateChanged(I)V

    .line 172
    return-void
.end method

.method public onStop()V
    .locals 3

    .prologue
    const/4 v2, 0x0

    const/16 v1, 0x8

    .line 176
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/adapter/MoviesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 177
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v0, v2}, Lcom/google/android/videos/adapter/MoviesDataSource;->changeCursor(Landroid/database/Cursor;)V

    .line 178
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setVisibility(I)V

    .line 179
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->progressBar:Landroid/view/View;

    invoke-virtual {v0, v1}, Landroid/view/View;->setVisibility(I)V

    .line 180
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 181
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 182
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/CursorHelper;->removeListener(Lcom/google/android/videos/ui/CursorHelper$Listener;)V

    .line 183
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderCursorHelper:Lcom/google/android/videos/ui/CursorHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/CursorHelper;->onStop()V

    .line 184
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->database:Lcom/google/android/videos/store/Database;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/store/Database;->removeListener(Lcom/google/android/videos/store/Database$Listener;)Z

    .line 185
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/ui/SyncHelper;->removeListener(Lcom/google/android/videos/ui/SyncHelper$Listener;)V

    .line 186
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->moviesFlowHelper:Lcom/google/android/videos/ui/MoviesFlowHelper;

    invoke-virtual {v0}, Lcom/google/android/videos/ui/MoviesFlowHelper;->onStop()V

    .line 187
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v0, p0}, Lcom/google/android/videos/utils/NetworkStatus;->removeUpdatable(Lcom/google/android/repolib/observers/Updatable;)V

    .line 188
    return-void
.end method

.method public onSyncStateChanged(I)V
    .locals 2
    .param p1, "state"    # I

    .prologue
    .line 371
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->syncHelper:Lcom/google/android/videos/ui/SyncHelper;

    invoke-virtual {v1}, Lcom/google/android/videos/ui/SyncHelper;->getAccount()Ljava/lang/String;

    move-result-object v0

    .line 372
    .local v0, "account":Ljava/lang/String;
    invoke-static {v0}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v1

    if-nez v1, :cond_0

    .line 373
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/welcome/WelcomeFlow;->setAccount(Ljava/lang/String;)V

    .line 375
    :cond_0
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->updateVisibilities()V

    .line 376
    return-void
.end method

.method public onTransitioningChanged(Z)V
    .locals 2
    .param p1, "isTransitioning"    # Z

    .prologue
    .line 347
    if-nez p1, :cond_2

    .line 348
    iget-object v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->listView:Landroid/support/v7/widget/RecyclerView;

    new-instance v1, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;

    invoke-direct {v1}, Lcom/google/android/videos/ui/ShuffleAddItemAnimator;-><init>()V

    invoke-virtual {v0, v1}, Landroid/support/v7/widget/RecyclerView;->setItemAnimator(Landroid/support/v7/widget/RecyclerView$ItemAnimator;)V

    .line 349
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingLibraryCursor:Z

    if-eqz v0, :cond_0

    .line 350
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshLibraryCursor()V

    .line 352
    :cond_0
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPreorderCursor:Z

    if-eqz v0, :cond_1

    .line 353
    invoke-direct {p0}, Lcom/google/android/videos/ui/MoviesHelper;->refreshPreorderCursor()V

    .line 355
    :cond_1
    iget-boolean v0, p0, Lcom/google/android/videos/ui/MoviesHelper;->pendingPostersUpdated:Z

    if-eqz v0, :cond_2

    .line 356
    invoke-virtual {p0}, Lcom/google/android/videos/ui/MoviesHelper;->onPostersUpdated()V

    .line 359
    :cond_2
    return-void
.end method

.method public update()V
    .locals 3

    .prologue
    .line 363
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->networkStatus:Lcom/google/android/videos/utils/NetworkStatus;

    invoke-interface {v1}, Lcom/google/android/videos/utils/NetworkStatus;->isNetworkAvailable()Z

    move-result v0

    .line 364
    .local v0, "isConnected":Z
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->libraryDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->setNetworkConnected(Z)V

    .line 365
    iget-object v1, p0, Lcom/google/android/videos/ui/MoviesHelper;->preorderDataSource:Lcom/google/android/videos/adapter/MoviesDataSource;

    invoke-virtual {v1, v0}, Lcom/google/android/videos/adapter/MoviesDataSource;->setNetworkConnected(Z)V

    .line 366
    iget-object v2, p0, Lcom/google/android/videos/ui/MoviesHelper;->welcomeFlow:Lcom/google/android/videos/welcome/WelcomeFlow;

    if-nez v0, :cond_0

    const/4 v1, 0x1

    :goto_0
    invoke-virtual {v2, v1}, Lcom/google/android/videos/welcome/WelcomeFlow;->setDimmed(Z)V

    .line 367
    return-void

    .line 366
    :cond_0
    const/4 v1, 0x0

    goto :goto_0
.end method
