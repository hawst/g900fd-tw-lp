.class Lcom/google/android/videos/pinning/DownloadNotificationManager$3;
.super Ljava/lang/Object;
.source "DownloadNotificationManager.java"

# interfaces
.implements Ljava/lang/Runnable;


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pinning/DownloadNotificationManager;->dismissNotificationFor(Ljava/lang/String;[Ljava/lang/String;)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

.field final synthetic val$account:Ljava/lang/String;

.field final synthetic val$videoIds:[Ljava/lang/String;


# direct methods
.method constructor <init>(Lcom/google/android/videos/pinning/DownloadNotificationManager;[Ljava/lang/String;Ljava/lang/String;)V
    .locals 0

    .prologue
    .line 512
    iput-object p1, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    iput-object p2, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$videoIds:[Ljava/lang/String;

    iput-object p3, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    return-void
.end method


# virtual methods
.method public run()V
    .locals 14

    .prologue
    .line 519
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$videoIds:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_1

    .line 520
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$videoIds:[Ljava/lang/String;

    aget-object v7, v8, v0

    .line 521
    .local v7, "videoId":Ljava/lang/String;
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$2500(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/store/Database;

    move-result-object v8

    invoke-virtual {v8}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v5

    .line 522
    .local v5, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    const/4 v3, 0x0

    .line 525
    .local v3, "success":Z
    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 526
    .local v6, "values":Landroid/content/ContentValues;
    const-string v8, "pinning_notification_active"

    const/4 v9, 0x0

    invoke-static {v9}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v9

    invoke-virtual {v6, v8, v9}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 527
    const-string v8, "purchased_assets"

    const-string v9, "(account = ? AND asset_type IN (6,20) AND asset_id = ?) AND pinning_notification_active"

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/String;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v7, v10, v11

    invoke-virtual {v5, v8, v6, v9, v10}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result v2

    .line 530
    .local v2, "rowsAffected":I
    if-lez v2, :cond_0

    const/4 v3, 0x1

    .line 532
    :goto_1
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$2500(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/store/Database;

    move-result-object v8

    const/16 v9, 0xb

    const/4 v10, 0x2

    new-array v10, v10, [Ljava/lang/Object;

    const/4 v11, 0x0

    iget-object v12, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    aput-object v12, v10, v11

    const/4 v11, 0x1

    aput-object v7, v10, v11

    invoke-virtual {v8, v5, v3, v9, v10}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 519
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 530
    :cond_0
    const/4 v3, 0x0

    goto :goto_1

    .line 532
    .end local v2    # "rowsAffected":I
    .end local v6    # "values":Landroid/content/ContentValues;
    :catchall_0
    move-exception v8

    iget-object v9, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v9}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$2500(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Lcom/google/android/videos/store/Database;

    move-result-object v9

    const/16 v10, 0xb

    const/4 v11, 0x2

    new-array v11, v11, [Ljava/lang/Object;

    const/4 v12, 0x0

    iget-object v13, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    aput-object v13, v11, v12

    const/4 v12, 0x1

    aput-object v7, v11, v12

    invoke-virtual {v9, v5, v3, v10, v11}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v8

    .line 536
    .end local v3    # "success":Z
    .end local v5    # "transaction":Landroid/database/sqlite/SQLiteDatabase;
    .end local v7    # "videoId":Ljava/lang/String;
    :cond_1
    new-instance v4, Ljava/util/ArrayList;

    invoke-direct {v4}, Ljava/util/ArrayList;-><init>()V

    .line 537
    .local v4, "toCancel":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;>;"
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;
    invoke-static {v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;

    move-result-object v9

    monitor-enter v9

    .line 538
    const/4 v0, 0x0

    :goto_2
    :try_start_1
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$videoIds:[Ljava/lang/String;

    array-length v8, v8

    if-ge v0, v8, :cond_3

    .line 539
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->videoDataMap:Ljava/util/Map;
    invoke-static {v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1800(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Ljava/util/Map;

    move-result-object v8

    iget-object v10, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$account:Ljava/lang/String;

    iget-object v11, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->val$videoIds:[Ljava/lang/String;

    aget-object v11, v11, v0

    invoke-static {v10, v11}, Landroid/util/Pair;->create(Ljava/lang/Object;Ljava/lang/Object;)Landroid/util/Pair;

    move-result-object v10

    invoke-interface {v8, v10}, Ljava/util/Map;->remove(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;

    .line 540
    .local v1, "oldVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    if-eqz v1, :cond_2

    .line 541
    invoke-interface {v4, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 538
    :cond_2
    add-int/lit8 v0, v0, 0x1

    goto :goto_2

    .line 544
    .end local v1    # "oldVideoData":Lcom/google/android/videos/pinning/DownloadNotificationManager$VideoData;
    :cond_3
    monitor-exit v9
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_1

    .line 545
    invoke-interface {v4}, Ljava/util/List;->isEmpty()Z

    move-result v8

    if-nez v8, :cond_4

    .line 546
    iget-object v8, p0, Lcom/google/android/videos/pinning/DownloadNotificationManager$3;->this$0:Lcom/google/android/videos/pinning/DownloadNotificationManager;

    # getter for: Lcom/google/android/videos/pinning/DownloadNotificationManager;->mainHandler:Landroid/os/Handler;
    invoke-static {v8}, Lcom/google/android/videos/pinning/DownloadNotificationManager;->access$1600(Lcom/google/android/videos/pinning/DownloadNotificationManager;)Landroid/os/Handler;

    move-result-object v8

    new-instance v9, Lcom/google/android/videos/pinning/DownloadNotificationManager$3$1;

    invoke-direct {v9, p0, v4}, Lcom/google/android/videos/pinning/DownloadNotificationManager$3$1;-><init>(Lcom/google/android/videos/pinning/DownloadNotificationManager$3;Ljava/util/List;)V

    invoke-virtual {v8, v9}, Landroid/os/Handler;->post(Ljava/lang/Runnable;)Z

    .line 555
    :cond_4
    return-void

    .line 544
    :catchall_1
    move-exception v8

    :try_start_2
    monitor-exit v9
    :try_end_2
    .catchall {:try_start_2 .. :try_end_2} :catchall_1

    throw v8
.end method
