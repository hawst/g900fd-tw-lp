.class Lcom/google/android/videos/player/overlay/StoryboardHelper$2;
.super Landroid/animation/AnimatorListenerAdapter;
.source "StoryboardHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/player/overlay/StoryboardHelper;->updateThumbnailViewVisibility(I)V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

.field final synthetic val$visibility:I


# direct methods
.method constructor <init>(Lcom/google/android/videos/player/overlay/StoryboardHelper;I)V
    .locals 0

    .prologue
    .line 250
    iput-object p1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    iput p2, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->val$visibility:I

    invoke-direct {p0}, Landroid/animation/AnimatorListenerAdapter;-><init>()V

    return-void
.end method

.method private onAnimationStopped()V
    .locals 2

    .prologue
    .line 267
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    # invokes: Lcom/google/android/videos/player/overlay/StoryboardHelper;->restoreThumbnailView()V
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->access$200(Lcom/google/android/videos/player/overlay/StoryboardHelper;)V

    .line 268
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    # getter for: Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->access$100(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/widget/ImageView;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->val$visibility:I

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 269
    iget v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->val$visibility:I

    const/4 v1, 0x4

    if-ne v0, v1, :cond_0

    .line 270
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    # getter for: Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->access$100(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setImageBitmap(Landroid/graphics/Bitmap;)V

    .line 272
    :cond_0
    return-void
.end method


# virtual methods
.method public onAnimationCancel(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 263
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->onAnimationStopped()V

    .line 264
    return-void
.end method

.method public onAnimationEnd(Landroid/animation/Animator;)V
    .locals 0
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 258
    invoke-direct {p0}, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->onAnimationStopped()V

    .line 259
    return-void
.end method

.method public onAnimationStart(Landroid/animation/Animator;)V
    .locals 2
    .param p1, "animation"    # Landroid/animation/Animator;

    .prologue
    .line 253
    iget-object v0, p0, Lcom/google/android/videos/player/overlay/StoryboardHelper$2;->this$0:Lcom/google/android/videos/player/overlay/StoryboardHelper;

    # getter for: Lcom/google/android/videos/player/overlay/StoryboardHelper;->thumbnailView:Landroid/widget/ImageView;
    invoke-static {v0}, Lcom/google/android/videos/player/overlay/StoryboardHelper;->access$100(Lcom/google/android/videos/player/overlay/StoryboardHelper;)Landroid/widget/ImageView;

    move-result-object v0

    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Landroid/widget/ImageView;->setVisibility(I)V

    .line 254
    return-void
.end method
