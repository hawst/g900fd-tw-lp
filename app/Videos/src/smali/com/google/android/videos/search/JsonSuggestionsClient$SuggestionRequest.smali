.class public Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;
.super Ljava/lang/Object;
.source "JsonSuggestionsClient.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/search/JsonSuggestionsClient;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "SuggestionRequest"
.end annotation


# instance fields
.field public final account:Ljava/lang/String;

.field public final country:Ljava/lang/String;

.field public final limit:I

.field public final locale:Ljava/util/Locale;

.field public final query:Ljava/lang/String;


# direct methods
.method public constructor <init>(Ljava/lang/String;Ljava/lang/String;ILjava/util/Locale;Ljava/lang/String;)V
    .locals 0
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "query"    # Ljava/lang/String;
    .param p3, "limit"    # I
    .param p4, "locale"    # Ljava/util/Locale;
    .param p5, "country"    # Ljava/lang/String;

    .prologue
    .line 55
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 56
    iput-object p1, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->account:Ljava/lang/String;

    .line 57
    iput-object p2, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->query:Ljava/lang/String;

    .line 58
    iput p3, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->limit:I

    .line 59
    iput-object p4, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->locale:Ljava/util/Locale;

    .line 60
    iput-object p5, p0, Lcom/google/android/videos/search/JsonSuggestionsClient$SuggestionRequest;->country:Ljava/lang/String;

    .line 61
    return-void
.end method
