.class public Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;
.super Ljava/lang/Object;
.source "SubtitleWindowSettingsTimeline.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x9
    name = "Builder"
.end annotation


# instance fields
.field private final settings:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSettings;",
            ">;"
        }
    .end annotation
.end field

.field private final startTimes:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Ljava/lang/Integer;",
            ">;"
        }
    .end annotation
.end field


# direct methods
.method public constructor <init>()V
    .locals 2

    .prologue
    .line 69
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 70
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    .line 71
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->settings:Ljava/util/List;

    .line 73
    const/4 v0, 0x0

    sget-object v1, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    invoke-virtual {p0, v0, v1}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->addSettings(ILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;

    .line 74
    return-void
.end method


# virtual methods
.method public addSettings(ILcom/google/android/videos/subtitles/SubtitleWindowSettings;)Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;
    .locals 2
    .param p1, "startTimeMillis"    # I
    .param p2, "windowSettings"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    .prologue
    .line 77
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v0}, Ljava/util/List;->isEmpty()Z

    move-result v0

    if-nez v0, :cond_0

    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    add-int/lit8 v1, v1, -0x1

    invoke-interface {v0, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    if-ge p1, v0, :cond_0

    .line 78
    const-string v0, "subtitle settings are not given in non-decreasing start time order"

    invoke-static {v0}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    .line 80
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    invoke-static {p1}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v1

    invoke-interface {v0, v1}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 81
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->settings:Ljava/util/List;

    invoke-interface {v0, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 82
    return-object p0
.end method

.method public build()Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;
    .locals 4

    .prologue
    .line 86
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;

    iget-object v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->startTimes:Ljava/util/List;

    iget-object v2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$Builder;->settings:Ljava/util/List;

    const/4 v3, 0x0

    invoke-direct {v0, v1, v2, v3}, Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline;-><init>(Ljava/util/List;Ljava/util/List;Lcom/google/android/videos/subtitles/SubtitleWindowSettingsTimeline$1;)V

    return-object v0
.end method
