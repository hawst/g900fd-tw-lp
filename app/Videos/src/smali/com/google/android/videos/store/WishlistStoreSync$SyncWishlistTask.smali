.class Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;
.super Lcom/google/android/videos/store/SyncTaskManager$SyncTask;
.source "WishlistStoreSync.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/store/WishlistStoreSync;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "SyncWishlistTask"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Lcom/google/android/videos/store/SyncTaskManager$SyncTask;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/api/GetWishlistRequest;",
        "Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;",
        ">;"
    }
.end annotation


# instance fields
.field private final account:Ljava/lang/String;

.field final synthetic this$0:Lcom/google/android/videos/store/WishlistStoreSync;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/store/WishlistStoreSync;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;)V
    .locals 1
    .param p2, "priority"    # I
    .param p4, "account"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Lcom/google/android/videos/store/SyncTaskManager$SharedStates",
            "<*>;",
            "Ljava/lang/String;",
            ")V"
        }
    .end annotation

    .prologue
    .line 158
    .local p3, "states":Lcom/google/android/videos/store/SyncTaskManager$SharedStates;, "Lcom/google/android/videos/store/SyncTaskManager$SharedStates<*>;"
    iput-object p1, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    .line 159
    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->syncTaskManager:Lcom/google/android/videos/store/SyncTaskManager;
    invoke-static {p1}, Lcom/google/android/videos/store/WishlistStoreSync;->access$000(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/SyncTaskManager;

    move-result-object v0

    invoke-direct {p0, v0, p2, p3}, Lcom/google/android/videos/store/SyncTaskManager$SyncTask;-><init>(Lcom/google/android/videos/store/SyncTaskManager;ILcom/google/android/videos/store/SyncTaskManager$SharedStates;)V

    .line 160
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Ljava/lang/String;

    iput-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    .line 161
    return-void
.end method

.method private loadWishlist(Ljava/util/List;Ljava/util/List;)V
    .locals 11
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .local p1, "movies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p2, "shows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/4 v4, 0x1

    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 201
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "wishlist"

    sget-object v2, Lcom/google/android/videos/store/WishlistStore;->PROJECTION:[Ljava/lang/String;

    const-string v3, "wishlist_account = ? AND wishlist_item_state != 3"

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 205
    .local v8, "cursor":Landroid/database/Cursor;
    :goto_0
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    .line 206
    const/4 v0, 0x1

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getInt(I)I

    move-result v10

    .line 207
    .local v10, "itemType":I
    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;

    move-result-object v9

    .line 208
    .local v9, "itemId":Ljava/lang/String;
    invoke-direct {p0, v10, v9, p1, p2}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->maybeAddItem(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    goto :goto_0

    .line 211
    .end local v9    # "itemId":Ljava/lang/String;
    .end local v10    # "itemType":I
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0

    :cond_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 213
    return-void
.end method

.method private maybeAddItem(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 0
    .param p1, "itemType"    # I
    .param p2, "itemId"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(I",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 273
    .local p3, "movies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "shows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    sparse-switch p1, :sswitch_data_0

    .line 281
    :goto_0
    return-void

    .line 275
    :sswitch_0
    invoke-interface {p3, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 278
    :sswitch_1
    invoke-interface {p4, p2}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    goto :goto_0

    .line 273
    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method private updateWishlist([Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V
    .locals 24
    .param p1, "resourceIds"    # [Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .param p2, "snapshotToken"    # Ljava/lang/String;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "([",
            "Lcom/google/wireless/android/video/magma/proto/AssetResourceId;",
            "Ljava/lang/String;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;",
            "Ljava/util/List",
            "<",
            "Ljava/lang/String;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 217
    .local p3, "movies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    .local p4, "shows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    const/16 v18, 0x1

    move/from16 v0, v18

    new-array v4, v0, [Ljava/lang/String;

    const/16 v18, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v19, v0

    aput-object v19, v4, v18

    .line 218
    .local v4, "accountArg":[Ljava/lang/String;
    const/4 v11, 0x0

    .line 219
    .local v11, "modified":Z
    const/4 v13, 0x0

    .line 220
    .local v13, "success":Z
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v18

    invoke-virtual/range {v18 .. v18}, Lcom/google/android/videos/store/Database;->beginTransaction()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v16

    .line 223
    .local v16, "transaction":Landroid/database/sqlite/SQLiteDatabase;
    :try_start_0
    new-instance v6, Landroid/content/ContentValues;

    invoke-direct {v6}, Landroid/content/ContentValues;-><init>()V

    .line 224
    .local v6, "defaultValues":Landroid/content/ContentValues;
    const-string v18, "wishlist_in_cloud"

    const/16 v19, 0x0

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v6, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 225
    const-string v18, "wishlist"

    const-string v19, "wishlist_account = ?"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v6, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    .line 228
    move-object/from16 v0, p1

    array-length v5, v0

    .line 229
    .local v5, "count":I
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_0
    if-ge v7, v5, :cond_2

    .line 230
    aget-object v12, p1, v7

    .line 231
    .local v12, "resource":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget v9, v12, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    .line 232
    .local v9, "itemType":I
    iget-object v8, v12, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    .line 233
    .local v8, "itemId":Ljava/lang/String;
    new-instance v10, Landroid/content/ContentValues;

    invoke-direct {v10}, Landroid/content/ContentValues;-><init>()V

    .line 234
    .local v10, "itemValues":Landroid/content/ContentValues;
    const-string v18, "wishlist_item_order"

    sub-int v19, v5, v7

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 235
    const-string v18, "wishlist_in_cloud"

    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Boolean;->valueOf(Z)Ljava/lang/Boolean;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Boolean;)V

    .line 238
    const-string v18, "wishlist"

    const-string v19, "wishlist_account = ? AND wishlist_item_id = ? AND wishlist_item_type = ? AND wishlist_item_state != 2"

    const/16 v20, 0x3

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/String;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    const/16 v21, 0x1

    aput-object v8, v20, v21

    const/16 v21, 0x2

    invoke-static {v9}, Ljava/lang/Integer;->toString(I)Ljava/lang/String;

    move-result-object v22

    aput-object v22, v20, v21

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v10, v2, v3}, Landroid/database/sqlite/SQLiteDatabase;->update(Ljava/lang/String;Landroid/content/ContentValues;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 240
    .local v17, "updatedRows":I
    if-lez v17, :cond_1

    .line 241
    const/4 v11, 0x1

    .line 254
    :cond_0
    :goto_1
    move-object/from16 v0, p0

    move-object/from16 v1, p3

    move-object/from16 v2, p4

    invoke-direct {v0, v9, v8, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->maybeAddItem(ILjava/lang/String;Ljava/util/List;Ljava/util/List;)V

    .line 229
    add-int/lit8 v7, v7, 0x1

    goto :goto_0

    .line 245
    :cond_1
    const-string v18, "wishlist_account"

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v19, v0

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 246
    const-string v18, "wishlist_item_id"

    move-object/from16 v0, v18

    invoke-virtual {v10, v0, v8}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/String;)V

    .line 247
    const-string v18, "wishlist_item_type"

    invoke-static {v9}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 248
    const-string v18, "wishlist_item_state"

    const/16 v19, 0x1

    invoke-static/range {v19 .. v19}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v19

    move-object/from16 v0, v18

    move-object/from16 v1, v19

    invoke-virtual {v10, v0, v1}, Landroid/content/ContentValues;->put(Ljava/lang/String;Ljava/lang/Integer;)V

    .line 249
    const-string v18, "wishlist"

    const/16 v19, 0x0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v10}, Landroid/database/sqlite/SQLiteDatabase;->replace(Ljava/lang/String;Ljava/lang/String;Landroid/content/ContentValues;)J

    move-result-wide v14

    .line 250
    .local v14, "rowId":J
    const-wide/16 v18, -0x1

    cmp-long v18, v14, v18

    if-eqz v18, :cond_0

    .line 251
    const/4 v11, 0x1

    goto :goto_1

    .line 257
    .end local v8    # "itemId":Ljava/lang/String;
    .end local v9    # "itemType":I
    .end local v10    # "itemValues":Landroid/content/ContentValues;
    .end local v12    # "resource":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    .end local v14    # "rowId":J
    .end local v17    # "updatedRows":I
    :cond_2
    const-string v18, "wishlist"

    const-string v19, "wishlist_account = ? AND NOT wishlist_in_cloud AND wishlist_item_state != 2"

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, v19

    invoke-virtual {v0, v1, v2, v4}, Landroid/database/sqlite/SQLiteDatabase;->delete(Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;)I

    move-result v17

    .line 258
    .restart local v17    # "updatedRows":I
    if-lez v17, :cond_3

    .line 259
    const/4 v11, 0x1

    .line 262
    :cond_3
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v18, v0

    move-object/from16 v0, v16

    move-object/from16 v1, v18

    move-object/from16 v2, p2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/store/WishlistStoreSync;->storeSnapshotToken(Landroid/database/sqlite/SQLiteDatabase;Ljava/lang/String;Ljava/lang/String;)V
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    .line 264
    const/4 v13, 0x1

    .line 266
    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v19

    if-eqz v11, :cond_4

    const/16 v18, 0x2

    :goto_2
    const/16 v20, 0x1

    move/from16 v0, v20

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v20, v0

    const/16 v21, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v22, v0

    aput-object v22, v20, v21

    move-object/from16 v0, v19

    move-object/from16 v1, v16

    move/from16 v2, v18

    move-object/from16 v3, v20

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    .line 269
    return-void

    .line 266
    :cond_4
    const/16 v18, 0x0

    goto :goto_2

    .end local v5    # "count":I
    .end local v6    # "defaultValues":Landroid/content/ContentValues;
    .end local v7    # "i":I
    .end local v17    # "updatedRows":I
    :catchall_0
    move-exception v18

    move-object/from16 v19, v18

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    move-object/from16 v18, v0

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static/range {v18 .. v18}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v20

    if-eqz v11, :cond_5

    const/16 v18, 0x2

    :goto_3
    const/16 v21, 0x1

    move/from16 v0, v21

    new-array v0, v0, [Ljava/lang/Object;

    move-object/from16 v21, v0

    const/16 v22, 0x0

    move-object/from16 v0, p0

    iget-object v0, v0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    move-object/from16 v23, v0

    aput-object v23, v21, v22

    move-object/from16 v0, v20

    move-object/from16 v1, v16

    move/from16 v2, v18

    move-object/from16 v3, v21

    invoke-virtual {v0, v1, v13, v2, v3}, Lcom/google/android/videos/store/Database;->endTransaction(Landroid/database/sqlite/SQLiteDatabase;ZI[Ljava/lang/Object;)V

    throw v19

    :cond_5
    const/16 v18, 0x0

    goto :goto_3
.end method


# virtual methods
.method protected doSync()V
    .locals 10

    .prologue
    const/4 v7, 0x0

    const/4 v5, 0x0

    .line 166
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->database:Lcom/google/android/videos/store/Database;
    invoke-static {v0}, Lcom/google/android/videos/store/WishlistStoreSync;->access$100(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/Database;

    move-result-object v0

    invoke-virtual {v0}, Lcom/google/android/videos/store/Database;->getReadableDatabase()Landroid/database/sqlite/SQLiteDatabase;

    move-result-object v0

    const-string v1, "user_data"

    sget-object v2, Lcom/google/android/videos/store/WishlistStoreSync$SnapshotTokenQuery;->PROJECTION:[Ljava/lang/String;

    const-string v3, "user_account = ?"

    const/4 v4, 0x1

    new-array v4, v4, [Ljava/lang/String;

    iget-object v6, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    aput-object v6, v4, v7

    move-object v6, v5

    move-object v7, v5

    invoke-virtual/range {v0 .. v7}, Landroid/database/sqlite/SQLiteDatabase;->query(Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;[Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;Ljava/lang/String;)Landroid/database/Cursor;

    move-result-object v8

    .line 170
    .local v8, "cursor":Landroid/database/Cursor;
    :try_start_0
    invoke-interface {v8}, Landroid/database/Cursor;->moveToNext()Z

    move-result v0

    if-eqz v0, :cond_0

    const/4 v0, 0x0

    invoke-interface {v8, v0}, Landroid/database/Cursor;->getString(I)Ljava/lang/String;
    :try_end_0
    .catchall {:try_start_0 .. :try_end_0} :catchall_0

    move-result-object v9

    .line 173
    .local v9, "snapshotToken":Ljava/lang/String;
    :goto_0
    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    .line 175
    iget-object v0, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->wishlistRequester:Lcom/google/android/videos/async/Requester;
    invoke-static {v0}, Lcom/google/android/videos/store/WishlistStoreSync;->access$200(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/async/Requester;

    move-result-object v0

    new-instance v1, Lcom/google/android/videos/api/GetWishlistRequest;

    iget-object v2, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    invoke-direct {v1, v2, v9}, Lcom/google/android/videos/api/GetWishlistRequest;-><init>(Ljava/lang/String;Ljava/lang/String;)V

    invoke-interface {v0, v1, p0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 176
    return-void

    .end local v9    # "snapshotToken":Ljava/lang/String;
    :cond_0
    move-object v9, v5

    .line 170
    goto :goto_0

    .line 173
    :catchall_0
    move-exception v0

    invoke-interface {v8}, Landroid/database/Cursor;->close()V

    throw v0
.end method

.method public onError(Lcom/google/android/videos/api/GetWishlistRequest;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/api/GetWishlistRequest;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 285
    const/16 v0, 0x14

    invoke-virtual {p0, v0, p2}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->onSyncError(ILjava/lang/Exception;)V

    .line 286
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 153
    check-cast p1, Lcom/google/android/videos/api/GetWishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->onError(Lcom/google/android/videos/api/GetWishlistRequest;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/api/GetWishlistRequest;Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)V
    .locals 9
    .param p1, "request"    # Lcom/google/android/videos/api/GetWishlistRequest;
    .param p2, "response"    # Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    .prologue
    .line 181
    new-instance v2, Ljava/util/ArrayList;

    invoke-direct {v2}, Ljava/util/ArrayList;-><init>()V

    .line 182
    .local v2, "movies":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    new-instance v3, Ljava/util/ArrayList;

    invoke-direct {v3}, Ljava/util/ArrayList;-><init>()V

    .line 183
    .local v3, "shows":Ljava/util/List;, "Ljava/util/List<Ljava/lang/String;>;"
    iget-object v4, p1, Lcom/google/android/videos/api/GetWishlistRequest;->snapshotToken:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_1

    iget-object v4, p1, Lcom/google/android/videos/api/GetWishlistRequest;->snapshotToken:Ljava/lang/String;

    iget-object v5, p2, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-virtual {v4, v5}, Ljava/lang/String;->equals(Ljava/lang/Object;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 186
    invoke-direct {p0, v2, v3}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->loadWishlist(Ljava/util/List;Ljava/util/List;)V

    .line 192
    :goto_0
    invoke-interface {v2}, Ljava/util/List;->isEmpty()Z

    move-result v4

    if-nez v4, :cond_0

    .line 193
    iget-object v4, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    invoke-static {v4}, Lcom/google/android/videos/store/WishlistStoreSync;->access$300(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v4

    iget v5, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->priority:I

    iget-object v6, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v7, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    invoke-virtual {v4, v5, v6, v7, v2}, Lcom/google/android/videos/store/AssetStoreSync;->syncMovieMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/util/List;)V

    .line 195
    :cond_0
    const/4 v1, 0x0

    .local v1, "i":I
    invoke-interface {v3}, Ljava/util/List;->size()I

    move-result v0

    .local v0, "count":I
    :goto_1
    if-ge v1, v0, :cond_2

    .line 196
    iget-object v4, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->this$0:Lcom/google/android/videos/store/WishlistStoreSync;

    # getter for: Lcom/google/android/videos/store/WishlistStoreSync;->assetStoreSync:Lcom/google/android/videos/store/AssetStoreSync;
    invoke-static {v4}, Lcom/google/android/videos/store/WishlistStoreSync;->access$300(Lcom/google/android/videos/store/WishlistStoreSync;)Lcom/google/android/videos/store/AssetStoreSync;

    move-result-object v5

    iget v6, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->priority:I

    iget-object v7, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->states:Lcom/google/android/videos/store/SyncTaskManager$SharedStates;

    iget-object v8, p0, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->account:Ljava/lang/String;

    invoke-interface {v3, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v4

    check-cast v4, Ljava/lang/String;

    invoke-virtual {v5, v6, v7, v8, v4}, Lcom/google/android/videos/store/AssetStoreSync;->syncShowMetadata(ILcom/google/android/videos/store/SyncTaskManager$SharedStates;Ljava/lang/String;Ljava/lang/String;)V

    .line 195
    add-int/lit8 v1, v1, 0x1

    goto :goto_1

    .line 188
    .end local v0    # "count":I
    .end local v1    # "i":I
    :cond_1
    iget-object v4, p2, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->resourceIds:[Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    iget-object v5, p2, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;->snapshotToken:Ljava/lang/String;

    invoke-direct {p0, v4, v5, v2, v3}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->updateWishlist([Lcom/google/wireless/android/video/magma/proto/AssetResourceId;Ljava/lang/String;Ljava/util/List;Ljava/util/List;)V

    goto :goto_0

    .line 198
    .restart local v0    # "count":I
    .restart local v1    # "i":I
    :cond_2
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 153
    check-cast p1, Lcom/google/android/videos/api/GetWishlistRequest;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/store/WishlistStoreSync$SyncWishlistTask;->onResponse(Lcom/google/android/videos/api/GetWishlistRequest;Lcom/google/wireless/android/video/magma/proto/WishlistItemListResponse;)V

    return-void
.end method
