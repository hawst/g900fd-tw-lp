.class public final Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;
.super Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;
.source "CursorHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/CursorHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x19
    name = "MoviesLibraryCursorHelper"
.end annotation


# direct methods
.method public constructor <init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "config"    # Lcom/google/android/videos/Config;
    .param p3, "uiHandler"    # Landroid/os/Handler;
    .param p4, "database"    # Lcom/google/android/videos/store/Database;
    .param p5, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p6, "syncHelper"    # Lcom/google/android/videos/ui/SyncHelper;
    .param p7, "downloadedOnlyManager"    # Lcom/google/android/videos/utils/DownloadedOnlyManager;

    .prologue
    .line 403
    invoke-direct/range {p0 .. p7}, Lcom/google/android/videos/ui/CursorHelper$HomeMoviePurchaseCursorHelper;-><init>(Landroid/app/Activity;Lcom/google/android/videos/Config;Landroid/os/Handler;Lcom/google/android/videos/store/Database;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/ui/SyncHelper;Lcom/google/android/videos/utils/DownloadedOnlyManager;)V

    .line 405
    return-void
.end method


# virtual methods
.method protected createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;
    .locals 2
    .param p1, "account"    # Ljava/lang/String;
    .param p2, "downloadedOnly"    # Z

    .prologue
    .line 409
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v0

    invoke-static {p1, p2, v0, v1}, Lcom/google/android/videos/adapter/MoviesDataSource$Query;->createLibraryRequest(Ljava/lang/String;ZJ)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method

.method protected bridge synthetic createCursorRequest(Ljava/lang/String;Z)Ljava/lang/Object;
    .locals 1
    .param p1, "x0"    # Ljava/lang/String;
    .param p2, "x1"    # Z

    .prologue
    .line 398
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/ui/CursorHelper$MoviesLibraryCursorHelper;->createCursorRequest(Ljava/lang/String;Z)Lcom/google/android/videos/store/PurchaseStore$PurchaseRequest;

    move-result-object v0

    return-object v0
.end method
