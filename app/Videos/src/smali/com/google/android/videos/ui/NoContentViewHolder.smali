.class public Lcom/google/android/videos/ui/NoContentViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "NoContentViewHolder.java"


# instance fields
.field public final imageView:Landroid/widget/ImageView;

.field public final noContentView:Landroid/view/View;

.field public final textView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/view/View;)V
    .locals 1
    .param p1, "itemView"    # Landroid/view/View;

    .prologue
    .line 21
    invoke-direct {p0, p1}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 23
    const v0, 0x7f0f018e

    invoke-virtual {p1, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/ui/NoContentViewHolder;->noContentView:Landroid/view/View;

    .line 24
    const v0, 0x7f0f0190

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/ViewUtil;->findViewById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/NoContentViewHolder;->textView:Landroid/widget/TextView;

    .line 25
    const v0, 0x7f0f018f

    invoke-static {p1, v0}, Lcom/google/android/videos/utils/ViewUtil;->findViewById(Landroid/view/View;I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    iput-object v0, p0, Lcom/google/android/videos/ui/NoContentViewHolder;->imageView:Landroid/widget/ImageView;

    .line 26
    return-void
.end method
