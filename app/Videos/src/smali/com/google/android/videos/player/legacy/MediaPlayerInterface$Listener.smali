.class public interface abstract Lcom/google/android/videos/player/legacy/MediaPlayerInterface$Listener;
.super Ljava/lang/Object;
.source "MediaPlayerInterface.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/player/legacy/MediaPlayerInterface;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x609
    name = "Listener"
.end annotation


# virtual methods
.method public abstract onBufferingUpdate(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;I)V
.end method

.method public abstract onCompletion(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
.end method

.method public abstract onError(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
.end method

.method public abstract onInfo(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)Z
.end method

.method public abstract onPrepared(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
.end method

.method public abstract onSeekComplete(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;)V
.end method

.method public abstract onVideoSizeChanged(Lcom/google/android/videos/player/legacy/MediaPlayerInterface;II)V
.end method
