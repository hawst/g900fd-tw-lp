.class public Lcom/google/android/videos/cache/ProtoConverter;
.super Ljava/lang/Object;
.source "ProtoConverter.java"

# interfaces
.implements Lcom/google/android/videos/cache/Converter;


# annotations
.annotation system Ldalvik/annotation/Signature;
    value = {
        "<E:",
        "Lcom/google/protobuf/nano/MessageNano;",
        ">",
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/cache/Converter",
        "<TE;>;"
    }
.end annotation


# instance fields
.field private final constructor:Ljava/lang/reflect/Constructor;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/lang/reflect/Constructor",
            "<TE;>;"
        }
    .end annotation
.end field


# direct methods
.method private constructor <init>(Ljava/lang/reflect/Constructor;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/lang/reflect/Constructor",
            "<TE;>;)V"
        }
    .end annotation

    .prologue
    .line 29
    .local p0, "this":Lcom/google/android/videos/cache/ProtoConverter;, "Lcom/google/android/videos/cache/ProtoConverter<TE;>;"
    .local p1, "constructor":Ljava/lang/reflect/Constructor;, "Ljava/lang/reflect/Constructor<TE;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 30
    iput-object p1, p0, Lcom/google/android/videos/cache/ProtoConverter;->constructor:Ljava/lang/reflect/Constructor;

    .line 31
    return-void
.end method

.method public static create(Ljava/lang/Class;)Lcom/google/android/videos/cache/ProtoConverter;
    .locals 4
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "<E:",
            "Lcom/google/protobuf/nano/MessageNano;",
            ">(",
            "Ljava/lang/Class",
            "<TE;>;)",
            "Lcom/google/android/videos/cache/ProtoConverter",
            "<TE;>;"
        }
    .end annotation

    .prologue
    .line 22
    .local p0, "protoClass":Ljava/lang/Class;, "Ljava/lang/Class<TE;>;"
    :try_start_0
    new-instance v1, Lcom/google/android/videos/cache/ProtoConverter;

    const/4 v2, 0x0

    new-array v2, v2, [Ljava/lang/Class;

    invoke-virtual {p0, v2}, Ljava/lang/Class;->getConstructor([Ljava/lang/Class;)Ljava/lang/reflect/Constructor;

    move-result-object v2

    invoke-direct {v1, v2}, Lcom/google/android/videos/cache/ProtoConverter;-><init>(Ljava/lang/reflect/Constructor;)V
    :try_end_0
    .catch Ljava/lang/NoSuchMethodException; {:try_start_0 .. :try_end_0} :catch_0

    return-object v1

    .line 23
    :catch_0
    move-exception v0

    .line 24
    .local v0, "e":Ljava/lang/NoSuchMethodException;
    new-instance v1, Ljava/lang/IllegalArgumentException;

    new-instance v2, Ljava/lang/StringBuilder;

    invoke-direct {v2}, Ljava/lang/StringBuilder;-><init>()V

    const-string v3, "Cannot find public parameterless constructor from proto class "

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {p0}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v3

    invoke-virtual {v2, v3}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v2

    invoke-virtual {v2}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v2

    invoke-direct {v1, v2}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v1
.end method


# virtual methods
.method public readElement(Ljava/io/InputStream;J)Lcom/google/protobuf/nano/MessageNano;
    .locals 6
    .param p1, "in"    # Ljava/io/InputStream;
    .param p2, "length"    # J
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/io/InputStream;",
            "J)TE;"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .local p0, "this":Lcom/google/android/videos/cache/ProtoConverter;, "Lcom/google/android/videos/cache/ProtoConverter<TE;>;"
    const/4 v5, 0x0

    .line 41
    invoke-static {p1, p2, p3}, Lcom/google/android/videos/cache/ConverterUtils;->readBytes(Ljava/io/InputStream;J)Lcom/google/android/videos/utils/ByteArray;

    move-result-object v0

    .line 44
    .local v0, "byteArray":Lcom/google/android/videos/utils/ByteArray;
    :try_start_0
    iget-object v3, p0, Lcom/google/android/videos/cache/ProtoConverter;->constructor:Ljava/lang/reflect/Constructor;

    const/4 v4, 0x0

    new-array v4, v4, [Ljava/lang/Object;

    invoke-virtual {v3, v4}, Ljava/lang/reflect/Constructor;->newInstance([Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/protobuf/nano/MessageNano;
    :try_end_0
    .catch Ljava/lang/Exception; {:try_start_0 .. :try_end_0} :catch_0

    .line 49
    .local v2, "message":Lcom/google/protobuf/nano/MessageNano;, "TE;"
    iget-object v3, v0, Lcom/google/android/videos/utils/ByteArray;->data:[B

    invoke-virtual {v0}, Lcom/google/android/videos/utils/ByteArray;->limit()I

    move-result v4

    invoke-static {v2, v3, v5, v4}, Lcom/google/protobuf/nano/MessageNano;->mergeFrom(Lcom/google/protobuf/nano/MessageNano;[BII)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v3

    return-object v3

    .line 45
    .end local v2    # "message":Lcom/google/protobuf/nano/MessageNano;, "TE;"
    :catch_0
    move-exception v1

    .line 46
    .local v1, "e":Ljava/lang/Exception;
    new-instance v3, Ljava/io/IOException;

    new-instance v4, Ljava/lang/StringBuilder;

    invoke-direct {v4}, Ljava/lang/StringBuilder;-><init>()V

    const-string v5, "Cannot create an instance of "

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    iget-object v5, p0, Lcom/google/android/videos/cache/ProtoConverter;->constructor:Ljava/lang/reflect/Constructor;

    invoke-virtual {v5}, Ljava/lang/reflect/Constructor;->getDeclaringClass()Ljava/lang/Class;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/Class;->getName()Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v4, v5}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v4

    invoke-virtual {v4}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v4

    invoke-direct {v3, v4}, Ljava/io/IOException;-><init>(Ljava/lang/String;)V

    throw v3
.end method

.method public bridge synthetic readElement(Ljava/io/InputStream;J)Ljava/lang/Object;
    .locals 2
    .param p1, "x0"    # Ljava/io/InputStream;
    .param p2, "x1"    # J
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/videos/cache/ProtoConverter;, "Lcom/google/android/videos/cache/ProtoConverter<TE;>;"
    invoke-virtual {p0, p1, p2, p3}, Lcom/google/android/videos/cache/ProtoConverter;->readElement(Ljava/io/InputStream;J)Lcom/google/protobuf/nano/MessageNano;

    move-result-object v0

    return-object v0
.end method

.method public writeElement(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V
    .locals 1
    .param p2, "out"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(TE;",
            "Ljava/io/OutputStream;",
            ")V"
        }
    .end annotation

    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 35
    .local p0, "this":Lcom/google/android/videos/cache/ProtoConverter;, "Lcom/google/android/videos/cache/ProtoConverter<TE;>;"
    .local p1, "element":Lcom/google/protobuf/nano/MessageNano;, "TE;"
    invoke-static {p1}, Lcom/google/protobuf/nano/MessageNano;->toByteArray(Lcom/google/protobuf/nano/MessageNano;)[B

    move-result-object v0

    invoke-virtual {p2, v0}, Ljava/io/OutputStream;->write([B)V

    .line 36
    return-void
.end method

.method public bridge synthetic writeElement(Ljava/lang/Object;Ljava/io/OutputStream;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/io/OutputStream;
    .annotation system Ldalvik/annotation/Throws;
        value = {
            Ljava/io/IOException;
        }
    .end annotation

    .prologue
    .line 16
    .local p0, "this":Lcom/google/android/videos/cache/ProtoConverter;, "Lcom/google/android/videos/cache/ProtoConverter<TE;>;"
    check-cast p1, Lcom/google/protobuf/nano/MessageNano;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/cache/ProtoConverter;->writeElement(Lcom/google/protobuf/nano/MessageNano;Ljava/io/OutputStream;)V

    return-void
.end method
