.class public final Lcom/google/android/videos/pano/ui/ShowCardView;
.super Landroid/widget/LinearLayout;
.source "ShowCardView.java"


# instance fields
.field private imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

.field private ratingBar:Lcom/google/android/play/layout/StarRatingBar;

.field private titleView:Landroid/widget/TextView;


# direct methods
.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 29
    const v0, 0x7f010104

    invoke-direct {p0, p1, p2, v0}, Landroid/widget/LinearLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;I)V

    .line 30
    return-void
.end method


# virtual methods
.method public getImageView()Landroid/widget/ImageView;
    .locals 1

    .prologue
    .line 63
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/ImageView;

    return-object v0
.end method

.method public hasOverlappingRendering()Z
    .locals 1

    .prologue
    .line 41
    const/4 v0, 0x0

    return v0
.end method

.method protected onFinishInflate()V
    .locals 1

    .prologue
    .line 34
    const v0, 0x7f0f0156

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/ShowCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/ui/RequestingImageView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    .line 35
    const v0, 0x7f0f012e

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/ShowCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->titleView:Landroid/widget/TextView;

    .line 36
    const v0, 0x7f0f0138

    invoke-virtual {p0, v0}, Lcom/google/android/videos/pano/ui/ShowCardView;->findViewById(I)Landroid/view/View;

    move-result-object v0

    invoke-static {v0}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/play/layout/StarRatingBar;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    .line 37
    return-void
.end method

.method public setImageResource(I)V
    .locals 1
    .param p1, "resId"    # I

    .prologue
    .line 45
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageResource(I)V

    .line 46
    return-void
.end method

.method public setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "uri"    # Landroid/net/Uri;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/net/Uri;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 50
    .local p2, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->imageView:Lcom/google/android/videos/pano/ui/RequestingImageView;

    invoke-virtual {v0, p1, p2}, Lcom/google/android/videos/pano/ui/RequestingImageView;->setImageUri(Landroid/net/Uri;Lcom/google/android/videos/async/Requester;)V

    .line 51
    return-void
.end method

.method public setRating(F)V
    .locals 2
    .param p1, "rating"    # F

    .prologue
    .line 58
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    const/4 v0, 0x0

    cmpl-float v0, p1, v0

    if-lez v0, :cond_0

    const/4 v0, 0x0

    :goto_0
    invoke-virtual {v1, v0}, Lcom/google/android/play/layout/StarRatingBar;->setVisibility(I)V

    .line 59
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->ratingBar:Lcom/google/android/play/layout/StarRatingBar;

    invoke-virtual {v0, p1}, Lcom/google/android/play/layout/StarRatingBar;->setRating(F)V

    .line 60
    return-void

    .line 58
    :cond_0
    const/4 v0, 0x4

    goto :goto_0
.end method

.method public setText(Ljava/lang/CharSequence;)V
    .locals 1
    .param p1, "text"    # Ljava/lang/CharSequence;

    .prologue
    .line 54
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowCardView;->titleView:Landroid/widget/TextView;

    invoke-virtual {v0, p1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 55
    return-void
.end method
