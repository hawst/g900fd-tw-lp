.class public final Lcom/google/android/videos/player/PlayerView;
.super Landroid/widget/RelativeLayout;
.source "PlayerView.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/player/PlayerView$LayoutParams;,
        Lcom/google/android/videos/player/PlayerView$PlayerOverlay;,
        Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;
    }
.end annotation


# instance fields
.field private horizontalOverscan:I

.field private makeSafeForOverscan:Z

.field private final overscanSafeOverlays:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;",
            ">;"
        }
    .end annotation
.end field

.field private final playerSurface:Lcom/google/android/videos/player/DefaultPlayerSurface;

.field private systemWindowInsetsListener:Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;

.field private verticalOverscan:I

.field private videoView:Landroid/view/View;


# direct methods
.method public constructor <init>(Landroid/content/Context;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;

    .prologue
    .line 68
    const/4 v0, 0x0

    invoke-direct {p0, p1, v0}, Lcom/google/android/videos/player/PlayerView;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 69
    return-void
.end method

.method public constructor <init>(Landroid/content/Context;Landroid/util/AttributeSet;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 72
    invoke-direct {p0, p1, p2}, Landroid/widget/RelativeLayout;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    .line 73
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/player/PlayerView;->overscanSafeOverlays:Ljava/util/List;

    .line 74
    const/4 v0, 0x1

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/PlayerView;->setFocusable(Z)V

    .line 75
    const/high16 v0, 0x40000

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/PlayerView;->setDescendantFocusability(I)V

    .line 76
    new-instance v0, Lcom/google/android/videos/player/DefaultPlayerSurface;

    invoke-direct {v0, p1}, Lcom/google/android/videos/player/DefaultPlayerSurface;-><init>(Landroid/content/Context;)V

    iput-object v0, p0, Lcom/google/android/videos/player/PlayerView;->playerSurface:Lcom/google/android/videos/player/DefaultPlayerSurface;

    .line 77
    iget-object v0, p0, Lcom/google/android/videos/player/PlayerView;->playerSurface:Lcom/google/android/videos/player/DefaultPlayerSurface;

    invoke-virtual {p0, v0}, Lcom/google/android/videos/player/PlayerView;->setVideoView(Landroid/view/View;)V

    .line 78
    return-void
.end method

.method private updateOverscan()V
    .locals 4

    .prologue
    const/4 v2, 0x0

    const v3, 0x3d99999a    # 0.075f

    .line 191
    iget-boolean v1, p0, Lcom/google/android/videos/player/PlayerView;->makeSafeForOverscan:Z

    if-eqz v1, :cond_1

    invoke-virtual {p0}, Lcom/google/android/videos/player/PlayerView;->getWidth()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v1, v1

    :goto_0
    iput v1, p0, Lcom/google/android/videos/player/PlayerView;->horizontalOverscan:I

    .line 192
    iget-boolean v1, p0, Lcom/google/android/videos/player/PlayerView;->makeSafeForOverscan:Z

    if-eqz v1, :cond_0

    invoke-virtual {p0}, Lcom/google/android/videos/player/PlayerView;->getHeight()I

    move-result v1

    int-to-float v1, v1

    mul-float/2addr v1, v3

    float-to-int v2, v1

    :cond_0
    iput v2, p0, Lcom/google/android/videos/player/PlayerView;->verticalOverscan:I

    .line 193
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_1
    iget-object v1, p0, Lcom/google/android/videos/player/PlayerView;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v1}, Ljava/util/List;->size()I

    move-result v1

    if-ge v0, v1, :cond_2

    .line 194
    iget-object v1, p0, Lcom/google/android/videos/player/PlayerView;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v1, v0}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;

    iget v2, p0, Lcom/google/android/videos/player/PlayerView;->horizontalOverscan:I

    iget v3, p0, Lcom/google/android/videos/player/PlayerView;->verticalOverscan:I

    invoke-interface {v1, v2, v3}, Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;->makeSafeForOverscan(II)V

    .line 193
    add-int/lit8 v0, v0, 0x1

    goto :goto_1

    .end local v0    # "i":I
    :cond_1
    move v1, v2

    .line 191
    goto :goto_0

    .line 196
    .restart local v0    # "i":I
    :cond_2
    return-void
.end method


# virtual methods
.method public varargs addOverlays([Lcom/google/android/videos/player/PlayerView$PlayerOverlay;)V
    .locals 8
    .param p1, "overlays"    # [Lcom/google/android/videos/player/PlayerView$PlayerOverlay;

    .prologue
    .line 104
    const/4 v0, 0x0

    .local v0, "i":I
    :goto_0
    array-length v5, p1

    if-ge v0, v5, :cond_2

    .line 105
    aget-object v1, p1, v0

    .line 106
    .local v1, "overlay":Lcom/google/android/videos/player/PlayerView$PlayerOverlay;
    invoke-interface {v1}, Lcom/google/android/videos/player/PlayerView$PlayerOverlay;->getView()Landroid/view/View;

    move-result-object v4

    .line 107
    .local v4, "view":Landroid/view/View;
    if-nez v4, :cond_0

    .line 108
    new-instance v5, Ljava/lang/IllegalArgumentException;

    new-instance v6, Ljava/lang/StringBuilder;

    invoke-direct {v6}, Ljava/lang/StringBuilder;-><init>()V

    const-string v7, "Overlay "

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/Object;)Ljava/lang/StringBuilder;

    move-result-object v6

    const-string v7, " does not provide a View and LayoutParams"

    invoke-virtual {v6, v7}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v6

    invoke-virtual {v6}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v6

    invoke-direct {v5, v6}, Ljava/lang/IllegalArgumentException;-><init>(Ljava/lang/String;)V

    throw v5

    .line 111
    :cond_0
    instance-of v5, v1, Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;

    if-eqz v5, :cond_1

    move-object v3, v1

    .line 112
    check-cast v3, Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;

    .line 113
    .local v3, "safeOverlay":Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;
    iget v5, p0, Lcom/google/android/videos/player/PlayerView;->horizontalOverscan:I

    iget v6, p0, Lcom/google/android/videos/player/PlayerView;->verticalOverscan:I

    invoke-interface {v3, v5, v6}, Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;->makeSafeForOverscan(II)V

    .line 114
    iget-object v5, p0, Lcom/google/android/videos/player/PlayerView;->overscanSafeOverlays:Ljava/util/List;

    invoke-interface {v5, v3}, Ljava/util/List;->add(Ljava/lang/Object;)Z

    .line 116
    .end local v3    # "safeOverlay":Lcom/google/android/videos/player/overlay/OverscanSafeOverlay;
    :cond_1
    aget-object v5, p1, v0

    invoke-interface {v5}, Lcom/google/android/videos/player/PlayerView$PlayerOverlay;->generateLayoutParams()Lcom/google/android/videos/player/PlayerView$LayoutParams;

    move-result-object v2

    .line 117
    .local v2, "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    invoke-virtual {p0, v4, v2}, Lcom/google/android/videos/player/PlayerView;->addView(Landroid/view/View;Landroid/view/ViewGroup$LayoutParams;)V

    .line 104
    add-int/lit8 v0, v0, 0x1

    goto :goto_0

    .line 119
    .end local v1    # "overlay":Lcom/google/android/videos/player/PlayerView$PlayerOverlay;
    .end local v2    # "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .end local v4    # "view":Landroid/view/View;
    :cond_2
    return-void
.end method

.method protected checkLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Z
    .locals 1
    .param p1, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 215
    instance-of v0, p1, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    return v0
.end method

.method protected fitSystemWindows(Landroid/graphics/Rect;)Z
    .locals 2
    .param p1, "insets"    # Landroid/graphics/Rect;

    .prologue
    .line 128
    iget-object v1, p0, Lcom/google/android/videos/player/PlayerView;->systemWindowInsetsListener:Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;

    if-eqz v1, :cond_0

    .line 131
    new-instance v0, Landroid/graphics/Rect;

    invoke-direct {v0, p1}, Landroid/graphics/Rect;-><init>(Landroid/graphics/Rect;)V

    .line 132
    .local v0, "insetsCopy":Landroid/graphics/Rect;
    iget-object v1, p0, Lcom/google/android/videos/player/PlayerView;->systemWindowInsetsListener:Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;

    invoke-interface {v1, v0}, Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;->onSystemWindowInsets(Landroid/graphics/Rect;)V

    .line 134
    .end local v0    # "insetsCopy":Landroid/graphics/Rect;
    :cond_0
    invoke-super {p0, p1}, Landroid/widget/RelativeLayout;->fitSystemWindows(Landroid/graphics/Rect;)Z

    move-result v1

    return v1
.end method

.method protected generateDefaultLayoutParams()Landroid/view/ViewGroup$LayoutParams;
    .locals 3

    .prologue
    const/4 v2, -0x2

    .line 210
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, v2, v2, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/view/ViewGroup$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/PlayerView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/videos/player/PlayerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method protected generateLayoutParams(Landroid/view/ViewGroup$LayoutParams;)Landroid/view/ViewGroup$LayoutParams;
    .locals 2
    .param p1, "params"    # Landroid/view/ViewGroup$LayoutParams;

    .prologue
    .line 205
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    const/4 v1, 0x0

    invoke-direct {v0, p1, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(Landroid/view/ViewGroup$LayoutParams;Z)V

    return-object v0
.end method

.method public bridge synthetic generateLayoutParams(Landroid/util/AttributeSet;)Landroid/widget/RelativeLayout$LayoutParams;
    .locals 1
    .param p1, "x0"    # Landroid/util/AttributeSet;

    .prologue
    .line 23
    invoke-virtual {p0, p1}, Lcom/google/android/videos/player/PlayerView;->generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/videos/player/PlayerView$LayoutParams;

    move-result-object v0

    return-object v0
.end method

.method public generateLayoutParams(Landroid/util/AttributeSet;)Lcom/google/android/videos/player/PlayerView$LayoutParams;
    .locals 2
    .param p1, "attrs"    # Landroid/util/AttributeSet;

    .prologue
    .line 200
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    invoke-virtual {p0}, Lcom/google/android/videos/player/PlayerView;->getContext()Landroid/content/Context;

    move-result-object v1

    invoke-direct {v0, v1, p1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(Landroid/content/Context;Landroid/util/AttributeSet;)V

    return-object v0
.end method

.method public getPlayerSurface()Lcom/google/android/videos/player/PlayerSurface;
    .locals 1

    .prologue
    .line 81
    iget-object v0, p0, Lcom/google/android/videos/player/PlayerView;->playerSurface:Lcom/google/android/videos/player/DefaultPlayerSurface;

    return-object v0
.end method

.method protected onMeasure(II)V
    .locals 12
    .param p1, "widthMeasureSpec"    # I
    .param p2, "heightMeasureSpec"    # I

    .prologue
    const/high16 v8, -0x80000000

    const v11, 0x3fe374bc    # 1.777f

    const/high16 v10, 0x40000000    # 2.0f

    .line 139
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v6

    .line 140
    .local v6, "widthMode":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getMode(I)I

    move-result v3

    .line 141
    .local v3, "heightMode":I
    invoke-static {p1}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v7

    .line 142
    .local v7, "widthSize":I
    invoke-static {p2}, Landroid/view/View$MeasureSpec;->getSize(I)I

    move-result v4

    .line 146
    .local v4, "heightSize":I
    if-ne v6, v10, :cond_0

    if-ne v3, v10, :cond_0

    .line 147
    move v5, v7

    .line 148
    .local v5, "width":I
    move v2, v4

    .line 173
    .local v2, "height":I
    :goto_0
    invoke-static {v5, p1}, Lcom/google/android/videos/player/PlayerView;->resolveSize(II)I

    move-result v5

    .line 174
    invoke-static {v2, p2}, Lcom/google/android/videos/player/PlayerView;->resolveSize(II)I

    move-result v2

    .line 176
    invoke-static {v5, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v1

    .line 177
    .local v1, "exactWidthSpec":I
    invoke-static {v2, v10}, Landroid/view/View$MeasureSpec;->makeMeasureSpec(II)I

    move-result v0

    .line 179
    .local v0, "exactHeightSpec":I
    invoke-super {p0, v1, v0}, Landroid/widget/RelativeLayout;->onMeasure(II)V

    .line 180
    return-void

    .line 150
    .end local v0    # "exactHeightSpec":I
    .end local v1    # "exactWidthSpec":I
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_0
    if-eq v6, v10, :cond_1

    if-ne v6, v8, :cond_2

    if-nez v3, :cond_2

    .line 152
    :cond_1
    move v5, v7

    .line 153
    .restart local v5    # "width":I
    int-to-float v8, v5

    div-float/2addr v8, v11

    float-to-int v2, v8

    .restart local v2    # "height":I
    goto :goto_0

    .line 154
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_2
    if-eq v3, v10, :cond_3

    if-ne v3, v8, :cond_4

    if-nez v6, :cond_4

    .line 156
    :cond_3
    move v2, v4

    .line 157
    .restart local v2    # "height":I
    int-to-float v8, v2

    mul-float/2addr v8, v11

    float-to-int v5, v8

    .restart local v5    # "width":I
    goto :goto_0

    .line 158
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_4
    if-ne v6, v8, :cond_6

    if-ne v3, v8, :cond_6

    .line 159
    int-to-float v8, v4

    int-to-float v9, v7

    div-float/2addr v9, v11

    cmpg-float v8, v8, v9

    if-gez v8, :cond_5

    .line 160
    int-to-float v8, v4

    mul-float/2addr v8, v11

    float-to-int v5, v8

    .line 161
    .restart local v5    # "width":I
    move v2, v4

    .restart local v2    # "height":I
    goto :goto_0

    .line 163
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_5
    move v5, v7

    .line 164
    .restart local v5    # "width":I
    int-to-float v8, v7

    div-float/2addr v8, v11

    float-to-int v2, v8

    .restart local v2    # "height":I
    goto :goto_0

    .line 168
    .end local v2    # "height":I
    .end local v5    # "width":I
    :cond_6
    const/4 v5, 0x0

    .line 169
    .restart local v5    # "width":I
    const/4 v2, 0x0

    .restart local v2    # "height":I
    goto :goto_0
.end method

.method protected onSizeChanged(IIII)V
    .locals 1
    .param p1, "width"    # I
    .param p2, "height"    # I
    .param p3, "oldWidth"    # I
    .param p4, "oldHeight"    # I

    .prologue
    .line 184
    invoke-super {p0, p1, p2, p3, p4}, Landroid/widget/RelativeLayout;->onSizeChanged(IIII)V

    .line 185
    iget-boolean v0, p0, Lcom/google/android/videos/player/PlayerView;->makeSafeForOverscan:Z

    if-eqz v0, :cond_0

    .line 186
    invoke-direct {p0}, Lcom/google/android/videos/player/PlayerView;->updateOverscan()V

    .line 188
    :cond_0
    return-void
.end method

.method public setMakeSafeForOverscan(Z)V
    .locals 1
    .param p1, "safeForOverscan"    # Z

    .prologue
    .line 94
    iget-boolean v0, p0, Lcom/google/android/videos/player/PlayerView;->makeSafeForOverscan:Z

    if-eq v0, p1, :cond_0

    .line 95
    iput-boolean p1, p0, Lcom/google/android/videos/player/PlayerView;->makeSafeForOverscan:Z

    .line 96
    invoke-direct {p0}, Lcom/google/android/videos/player/PlayerView;->updateOverscan()V

    .line 98
    :cond_0
    return-void
.end method

.method public setSystemWindowInsetsListener(Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;)V
    .locals 0
    .param p1, "listener"    # Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;

    .prologue
    .line 122
    iput-object p1, p0, Lcom/google/android/videos/player/PlayerView;->systemWindowInsetsListener:Lcom/google/android/videos/player/PlayerView$SystemWindowInsetsListener;

    .line 123
    return-void
.end method

.method public setVideoView(Landroid/view/View;)V
    .locals 5
    .param p1, "videoView"    # Landroid/view/View;

    .prologue
    const/4 v4, -0x2

    const/4 v2, 0x0

    .line 85
    iget-object v1, p0, Lcom/google/android/videos/player/PlayerView;->videoView:Landroid/view/View;

    if-nez v1, :cond_0

    const/4 v1, 0x1

    :goto_0
    const-string v3, "videoView has already been set"

    invoke-static {v1, v3}, Lcom/google/android/videos/utils/Preconditions;->checkState(ZLjava/lang/String;)V

    .line 86
    iput-object p1, p0, Lcom/google/android/videos/player/PlayerView;->videoView:Landroid/view/View;

    .line 87
    new-instance v0, Lcom/google/android/videos/player/PlayerView$LayoutParams;

    invoke-direct {v0, v4, v4, v2}, Lcom/google/android/videos/player/PlayerView$LayoutParams;-><init>(IIZ)V

    .line 89
    .local v0, "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    const/16 v1, 0xd

    invoke-virtual {v0, v1}, Lcom/google/android/videos/player/PlayerView$LayoutParams;->addRule(I)V

    .line 90
    invoke-virtual {p0, p1, v2, v0}, Lcom/google/android/videos/player/PlayerView;->addView(Landroid/view/View;ILandroid/view/ViewGroup$LayoutParams;)V

    .line 91
    return-void

    .end local v0    # "params":Lcom/google/android/videos/player/PlayerView$LayoutParams;
    :cond_0
    move v1, v2

    .line 85
    goto :goto_0
.end method
