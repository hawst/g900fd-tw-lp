.class final Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MovieDetailsFlowHelper.java"

# interfaces
.implements Landroid/view/View$OnClickListener;
.implements Lcom/google/android/videos/flow/Bindable;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "StorylineViewHolder"
.end annotation


# instance fields
.field private final storylineView:Landroid/widget/TextView;

.field final synthetic this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V
    .locals 1
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 553
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 554
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 555
    const v0, 0x7f0f017c

    invoke-virtual {p2, v0}, Landroid/view/View;->findViewById(I)Landroid/view/View;

    move-result-object v0

    check-cast v0, Landroid/widget/TextView;

    iput-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;->storylineView:Landroid/widget/TextView;

    .line 556
    invoke-virtual {p2, p0}, Landroid/view/View;->setOnClickListener(Landroid/view/View$OnClickListener;)V

    .line 557
    return-void
.end method


# virtual methods
.method public bind()V
    .locals 2

    .prologue
    .line 561
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;->storylineView:Landroid/widget/TextView;

    iget-object v1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->storyline:Ljava/lang/String;
    invoke-static {v1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$1000(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v1

    invoke-virtual {v0, v1}, Landroid/widget/TextView;->setText(Ljava/lang/CharSequence;)V

    .line 562
    return-void
.end method

.method public onClick(Landroid/view/View;)V
    .locals 1
    .param p1, "v"    # Landroid/view/View;

    .prologue
    .line 566
    iget-object v0, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$StorylineViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->infoPanelItemClickListener:Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;
    invoke-static {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$400(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;

    move-result-object v0

    invoke-interface {v0}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelItemClickListener;->onStorylineClick()V

    .line 567
    return-void
.end method
