.class Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;
.super Ljava/lang/Object;
.source "DefaultKnowledgeClient.java"

# interfaces
.implements Lcom/google/android/videos/async/Callback;


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x2
    name = "ParallelImageWorker"
.end annotation

.annotation system Ldalvik/annotation/Signature;
    value = {
        "Ljava/lang/Object;",
        "Lcom/google/android/videos/async/Callback",
        "<",
        "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
        "Landroid/graphics/Bitmap;",
        ">;"
    }
.end annotation


# instance fields
.field private final images:Ljava/util/List;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;"
        }
    .end annotation
.end field

.field private remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

.field final synthetic this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Ljava/util/List;)V
    .locals 0
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Ljava/util/List",
            "<",
            "Lcom/google/android/videos/tagging/KnowledgeEntity$Image;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 305
    .local p2, "images":Ljava/util/List;, "Ljava/util/List<Lcom/google/android/videos/tagging/KnowledgeEntity$Image;>;"
    iput-object p1, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 306
    iput-object p2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->images:Ljava/util/List;

    .line 307
    return-void
.end method


# virtual methods
.method public onError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "exception"    # Ljava/lang/Exception;

    .prologue
    .line 326
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageRequestError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V
    invoke-static {v0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$500(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    .line 327
    const/4 v0, 0x0

    invoke-virtual {p0, p1, v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->onResponse(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    .line 328
    return-void
.end method

.method public bridge synthetic onError(Ljava/lang/Object;Ljava/lang/Exception;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Exception;

    .prologue
    .line 299
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .end local p1    # "x0":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->onError(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Ljava/lang/Exception;)V

    return-void
.end method

.method public onResponse(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V
    .locals 1
    .param p1, "request"    # Lcom/google/android/videos/tagging/KnowledgeEntity$Image;
    .param p2, "response"    # Landroid/graphics/Bitmap;

    .prologue
    .line 319
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-virtual {v0}, Ljava/util/concurrent/atomic/AtomicInteger;->decrementAndGet()I

    move-result v0

    if-nez v0, :cond_0

    .line 320
    iget-object v0, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # invokes: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->onImageLoadingFinished()V
    invoke-static {v0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$600(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)V

    .line 322
    :cond_0
    return-void
.end method

.method public bridge synthetic onResponse(Ljava/lang/Object;Ljava/lang/Object;)V
    .locals 0
    .param p1, "x0"    # Ljava/lang/Object;
    .param p2, "x1"    # Ljava/lang/Object;

    .prologue
    .line 299
    check-cast p1, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    .end local p1    # "x0":Ljava/lang/Object;
    check-cast p2, Landroid/graphics/Bitmap;

    .end local p2    # "x1":Ljava/lang/Object;
    invoke-virtual {p0, p1, p2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->onResponse(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Landroid/graphics/Bitmap;)V

    return-void
.end method

.method public start()V
    .locals 4

    .prologue
    .line 310
    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->images:Ljava/util/List;

    invoke-interface {v2}, Ljava/util/List;->size()I

    move-result v0

    .line 311
    .local v0, "count":I
    new-instance v2, Ljava/util/concurrent/atomic/AtomicInteger;

    invoke-direct {v2, v0}, Ljava/util/concurrent/atomic/AtomicInteger;-><init>(I)V

    iput-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->remainingImageCount:Ljava/util/concurrent/atomic/AtomicInteger;

    .line 312
    const/4 v1, 0x0

    .local v1, "i":I
    :goto_0
    if-ge v1, v0, :cond_0

    .line 313
    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->this$1:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;

    # getter for: Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->localImageRequester:Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;
    invoke-static {v2}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;->access$700(Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker;)Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;

    move-result-object v3

    iget-object v2, p0, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$Worker$ParallelImageWorker;->images:Ljava/util/List;

    invoke-interface {v2, v1}, Ljava/util/List;->get(I)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/android/videos/tagging/KnowledgeEntity$Image;

    invoke-virtual {v3, v2, p0}, Lcom/google/android/videos/tagging/DefaultKnowledgeClient$ImageCachingRequester;->request(Lcom/google/android/videos/tagging/KnowledgeEntity$Image;Lcom/google/android/videos/async/Callback;)V

    .line 312
    add-int/lit8 v1, v1, 0x1

    goto :goto_0

    .line 315
    :cond_0
    return-void
.end method
