.class Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;
.super Lcom/google/android/videos/pano/ui/ClickableAction;
.source "ShowDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingMethod;
    value = Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->addDetailsSectionActions()V
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

.field final synthetic val$addedToWishlist:Z


# direct methods
.method constructor <init>(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;JLjava/lang/String;Ljava/lang/String;Z)V
    .locals 0
    .param p2, "x0"    # J
    .param p4, "x1"    # Ljava/lang/String;
    .param p5, "x2"    # Ljava/lang/String;

    .prologue
    .line 156
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iput-boolean p6, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->val$addedToWishlist:Z

    invoke-direct {p0, p2, p3, p4, p5}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    return-void
.end method


# virtual methods
.method public onClick(Landroid/app/Activity;)V
    .locals 7
    .param p1, "activity"    # Landroid/app/Activity;

    .prologue
    .line 159
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v0, v0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->context:Landroid/content/Context;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v1, v1, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->account:Ljava/lang/String;

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    iget-object v2, v2, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->itemId:Ljava/lang/String;

    const/16 v3, 0x12

    iget-boolean v4, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->val$addedToWishlist:Z

    if-nez v4, :cond_0

    const/4 v4, 0x1

    :goto_0
    const/4 v5, 0x5

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper$2;->this$0:Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;

    # getter for: Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->contentView:Landroid/view/View;
    invoke-static {v6}, Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;->access$300(Lcom/google/android/videos/pano/ui/ShowDetailsRowHelper;)Landroid/view/View;

    move-result-object v6

    invoke-static/range {v0 .. v6}, Lcom/google/android/videos/store/WishlistService;->requestSetWishlisted(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;IZILandroid/view/View;)V

    .line 161
    return-void

    .line 159
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method
