.class public Lcom/google/android/videos/activity/VideosPreferenceFragment;
.super Landroid/preference/PreferenceFragment;
.source "VideosPreferenceFragment.java"


# direct methods
.method public constructor <init>()V
    .locals 0

    .prologue
    .line 13
    invoke-direct {p0}, Landroid/preference/PreferenceFragment;-><init>()V

    return-void
.end method


# virtual methods
.method public onActivityCreated(Landroid/os/Bundle;)V
    .locals 1
    .param p1, "savedInstanceState"    # Landroid/os/Bundle;

    .prologue
    .line 17
    invoke-super {p0, p1}, Landroid/preference/PreferenceFragment;->onActivityCreated(Landroid/os/Bundle;)V

    .line 18
    invoke-virtual {p0}, Lcom/google/android/videos/activity/VideosPreferenceFragment;->getActivity()Landroid/app/Activity;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/activity/VideosPreferenceActivity;

    invoke-virtual {v0, p0}, Lcom/google/android/videos/activity/VideosPreferenceActivity;->configureSettings(Lcom/google/android/videos/activity/VideosPreferenceFragment;)V

    .line 19
    return-void
.end method
