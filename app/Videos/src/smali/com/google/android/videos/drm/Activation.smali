.class public final Lcom/google/android/videos/drm/Activation;
.super Ljava/lang/Object;
.source "Activation.java"


# instance fields
.field private final activationCurrentTimeMillis:J

.field private final activationElapsedRealtimeMillis:J


# direct methods
.method private constructor <init>(JJ)V
    .locals 1
    .param p1, "currentTimeMillis"    # J
    .param p3, "elapsedRealtime"    # J

    .prologue
    .line 19
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 20
    iput-wide p1, p0, Lcom/google/android/videos/drm/Activation;->activationCurrentTimeMillis:J

    .line 21
    iput-wide p3, p0, Lcom/google/android/videos/drm/Activation;->activationElapsedRealtimeMillis:J

    .line 22
    return-void
.end method

.method public static fromBytes([B)Lcom/google/android/videos/drm/Activation;
    .locals 6
    .param p0, "data"    # [B

    .prologue
    .line 32
    invoke-static {p0}, Ljava/nio/ByteBuffer;->wrap([B)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 33
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getInt()I

    .line 34
    new-instance v1, Lcom/google/android/videos/drm/Activation;

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v2

    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->getLong()J

    move-result-wide v4

    invoke-direct {v1, v2, v3, v4, v5}, Lcom/google/android/videos/drm/Activation;-><init>(JJ)V

    return-object v1
.end method

.method public static now()Lcom/google/android/videos/drm/Activation;
    .locals 6

    .prologue
    .line 25
    new-instance v0, Lcom/google/android/videos/drm/Activation;

    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v2

    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    invoke-direct {v0, v2, v3, v4, v5}, Lcom/google/android/videos/drm/Activation;-><init>(JJ)V

    return-object v0
.end method


# virtual methods
.method public elapsedMillis()J
    .locals 10

    .prologue
    const-wide/16 v8, 0x0

    .line 52
    invoke-static {}, Ljava/lang/System;->currentTimeMillis()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/drm/Activation;->activationCurrentTimeMillis:J

    sub-long v2, v4, v6

    .line 53
    .local v2, "elapsedTimeSinceActivation":J
    cmp-long v4, v2, v8

    if-gez v4, :cond_0

    .line 54
    const-wide/16 v2, 0x1

    .line 57
    :cond_0
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v4

    iget-wide v6, p0, Lcom/google/android/videos/drm/Activation;->activationElapsedRealtimeMillis:J

    sub-long v0, v4, v6

    .line 59
    .local v0, "elapsedRealtimeSinceActivation":J
    cmp-long v4, v0, v8

    if-gez v4, :cond_1

    .line 60
    invoke-static {}, Landroid/os/SystemClock;->elapsedRealtime()J

    move-result-wide v0

    .line 63
    :cond_1
    invoke-static {v2, v3, v0, v1}, Ljava/lang/Math;->max(JJ)J

    move-result-wide v4

    return-wide v4
.end method

.method public toBytes()[B
    .locals 4

    .prologue
    .line 41
    const/16 v1, 0x18

    invoke-static {v1}, Ljava/nio/ByteBuffer;->allocate(I)Ljava/nio/ByteBuffer;

    move-result-object v0

    .line 42
    .local v0, "buffer":Ljava/nio/ByteBuffer;
    const/4 v1, 0x0

    invoke-virtual {v0, v1}, Ljava/nio/ByteBuffer;->putInt(I)Ljava/nio/ByteBuffer;

    .line 43
    iget-wide v2, p0, Lcom/google/android/videos/drm/Activation;->activationCurrentTimeMillis:J

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 44
    iget-wide v2, p0, Lcom/google/android/videos/drm/Activation;->activationElapsedRealtimeMillis:J

    invoke-virtual {v0, v2, v3}, Ljava/nio/ByteBuffer;->putLong(J)Ljava/nio/ByteBuffer;

    .line 45
    invoke-virtual {v0}, Ljava/nio/ByteBuffer;->array()[B

    move-result-object v1

    return-object v1
.end method
