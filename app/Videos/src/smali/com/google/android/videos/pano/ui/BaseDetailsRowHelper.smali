.class public abstract Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.super Ljava/lang/Object;
.source "BaseDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;,
        Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;,
        Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
    }
.end annotation


# instance fields
.field protected final account:Ljava/lang/String;

.field private final actions:Ljava/util/ArrayList;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Ljava/util/ArrayList",
            "<",
            "Landroid/support/v17/leanback/widget/Action;",
            ">;"
        }
    .end annotation
.end field

.field private assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

.field protected final assetsCachingRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;"
        }
    .end annotation
.end field

.field protected final bitmapRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;"
        }
    .end annotation
.end field

.field protected final categoryListRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;"
        }
    .end annotation
.end field

.field protected final configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

.field protected final context:Landroid/content/Context;

.field protected final itemId:Ljava/lang/String;

.field protected final onPurchaseActionListener:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;

.field protected final purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

.field protected final relatedRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

.field protected final relatedRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;"
        }
    .end annotation
.end field

.field protected final reviewsRequester:Lcom/google/android/videos/async/Requester;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;"
        }
    .end annotation
.end field

.field private final wishlistStore:Lcom/google/android/videos/store/WishlistStore;


# direct methods
.method public constructor <init>(Landroid/content/Context;Ljava/lang/String;Ljava/lang/String;Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/api/RecommendationsRequest$Factory;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/async/Requester;Lcom/google/android/videos/store/PurchaseStore;Lcom/google/android/videos/store/WishlistStore;Lcom/google/android/videos/store/ConfigurationStore;Lcom/google/android/videos/async/Requester;)V
    .locals 1
    .param p1, "context"    # Landroid/content/Context;
    .param p2, "account"    # Ljava/lang/String;
    .param p3, "itemId"    # Ljava/lang/String;
    .param p4, "onPurchaseActionListener"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;
    .param p6, "relatedRequestFactory"    # Lcom/google/android/videos/api/RecommendationsRequest$Factory;
    .param p10, "purchaseStore"    # Lcom/google/android/videos/store/PurchaseStore;
    .param p11, "wishlistStore"    # Lcom/google/android/videos/store/WishlistStore;
    .param p12, "configurationStore"    # Lcom/google/android/videos/store/ConfigurationStore;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "(",
            "Landroid/content/Context;",
            "Ljava/lang/String;",
            "Ljava/lang/String;",
            "Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetListResponse;",
            ">;",
            "Lcom/google/android/videos/api/RecommendationsRequest$Factory;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/RecommendationsRequest;",
            "Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/AssetReviewListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;",
            ">;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/api/CategoryListRequest;",
            "Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;",
            ">;",
            "Lcom/google/android/videos/store/PurchaseStore;",
            "Lcom/google/android/videos/store/WishlistStore;",
            "Lcom/google/android/videos/store/ConfigurationStore;",
            "Lcom/google/android/videos/async/Requester",
            "<",
            "Lcom/google/android/videos/async/ControllableRequest",
            "<",
            "Landroid/net/Uri;",
            ">;",
            "Landroid/graphics/Bitmap;",
            ">;)V"
        }
    .end annotation

    .prologue
    .line 106
    .local p5, "assetsCachingRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    .local p7, "relatedRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    .local p8, "reviewsRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/AssetReviewListRequest;Lcom/google/wireless/android/video/magma/proto/AssetReviewListResponse;>;"
    .local p9, "categoryListRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/api/CategoryListRequest;Lcom/google/wireless/android/video/magma/proto/CategoryListResponse;>;"
    .local p13, "bitmapRequester":Lcom/google/android/videos/async/Requester;, "Lcom/google/android/videos/async/Requester<Lcom/google/android/videos/async/ControllableRequest<Landroid/net/Uri;>;Landroid/graphics/Bitmap;>;"
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 107
    iput-object p1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    .line 108
    iput-object p2, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->account:Ljava/lang/String;

    .line 109
    iput-object p3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->itemId:Ljava/lang/String;

    .line 110
    invoke-static {p4}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->onPurchaseActionListener:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$OnPurchaseActionListener;

    .line 111
    invoke-static {p5}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    .line 112
    invoke-static {p6}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->relatedRequestFactory:Lcom/google/android/videos/api/RecommendationsRequest$Factory;

    .line 113
    invoke-static {p7}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->relatedRequester:Lcom/google/android/videos/async/Requester;

    .line 114
    invoke-static {p8}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->reviewsRequester:Lcom/google/android/videos/async/Requester;

    .line 115
    invoke-static {p9}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/async/Requester;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->categoryListRequester:Lcom/google/android/videos/async/Requester;

    .line 116
    invoke-static {p12}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/ConfigurationStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    .line 117
    invoke-static {p10}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/PurchaseStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->purchaseStore:Lcom/google/android/videos/store/PurchaseStore;

    .line 118
    invoke-static {p11}, Lcom/google/android/videos/utils/Preconditions;->checkNotNull(Ljava/lang/Object;)Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Lcom/google/android/videos/store/WishlistStore;

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    .line 119
    new-instance v0, Ljava/util/ArrayList;

    invoke-direct {v0}, Ljava/util/ArrayList;-><init>()V

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->actions:Ljava/util/ArrayList;

    .line 120
    iput-object p13, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    .line 121
    return-void
.end method

.method static synthetic access$100(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/Review;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getReviewerName(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$200(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/Review;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getReviewerImage(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method static synthetic access$300(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;
    .locals 1
    .param p0, "x0"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
    .param p1, "x1"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 65
    invoke-direct {p0, p1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getDetailsIntent(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;

    move-result-object v0

    return-object v0
.end method

.method private getDetailsIntent(Lcom/google/wireless/android/video/magma/proto/AssetResource;)Landroid/content/Intent;
    .locals 3
    .param p1, "asset"    # Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .prologue
    .line 282
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/AssetResource;->resourceId:Lcom/google/wireless/android/video/magma/proto/AssetResourceId;

    .line 283
    .local v0, "resourceId":Lcom/google/wireless/android/video/magma/proto/AssetResourceId;
    iget v1, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->type:I

    sparse-switch v1, :sswitch_data_0

    .line 290
    const/4 v1, 0x0

    :goto_0
    return-object v1

    .line 285
    :sswitch_0
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/pano/activity/MovieDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    .line 287
    :sswitch_1
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    iget-object v2, v0, Lcom/google/wireless/android/video/magma/proto/AssetResourceId;->id:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/pano/activity/ShowDetailsActivity;->createIntent(Landroid/content/Context;Ljava/lang/String;)Landroid/content/Intent;

    move-result-object v1

    goto :goto_0

    .line 283
    nop

    :sswitch_data_0
    .sparse-switch
        0x6 -> :sswitch_0
        0x12 -> :sswitch_1
    .end sparse-switch
.end method

.method private getRelated()[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 6

    .prologue
    const/4 v3, 0x0

    .line 364
    iget-object v4, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->account:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-nez v4, :cond_0

    iget-object v4, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-static {v4}, Landroid/text/TextUtils;->isEmpty(Ljava/lang/CharSequence;)Z

    move-result v4

    if-eqz v4, :cond_1

    .line 378
    :cond_0
    :goto_0
    return-object v3

    .line 367
    :cond_1
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 369
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/RecommendationsRequest;Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;>;"
    iget-object v4, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->relatedRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getRelatedRequest()Lcom/google/android/videos/api/RecommendationsRequest;

    move-result-object v5

    invoke-interface {v4, v5, v0}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 371
    const-wide/16 v4, 0x3a98

    :try_start_0
    invoke-virtual {v0, v4, v5}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v2

    check-cast v2, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;

    .line 372
    .local v2, "response":Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    iget-object v3, v2, Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 373
    .end local v2    # "response":Lcom/google/wireless/android/video/magma/proto/RecommendationListResponse;
    :catch_0
    move-exception v1

    .line 374
    .local v1, "e":Ljava/util/concurrent/TimeoutException;
    const-string v4, "timeout"

    invoke-static {v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 375
    .end local v1    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v1

    .line 376
    .local v1, "e":Ljava/util/concurrent/ExecutionException;
    const-string v4, "Failed to fetch related"

    invoke-virtual {v1}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v5

    invoke-static {v4, v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method

.method private getReviewerImage(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    .locals 3
    .param p1, "review"    # Lcom/google/wireless/android/video/magma/proto/Review;

    .prologue
    .line 302
    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-eqz v1, :cond_0

    iget-object v1, p1, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    iget-object v1, v1, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    if-nez v1, :cond_1

    .line 303
    :cond_0
    const/4 v1, 0x0

    .line 306
    :goto_0
    return-object v1

    .line 305
    :cond_1
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getDefaultPosterHeight(Landroid/content/Context;)I

    move-result v0

    .line 306
    .local v0, "size":I
    new-instance v1, Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    invoke-direct {v1}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;-><init>()V

    invoke-virtual {v1, v0}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->setSize(I)Lcom/google/android/videos/utils/FIFEOptionsBuilder;

    move-result-object v1

    invoke-virtual {v1}, Lcom/google/android/videos/utils/FIFEOptionsBuilder;->build()Ljava/lang/String;

    move-result-object v1

    iget-object v2, p1, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userImage:Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;

    iget-object v2, v2, Lcom/google/wireless/android/video/magma/proto/UserProfile$Image;->url:Ljava/lang/String;

    invoke-static {v1, v2}, Lcom/google/android/videos/utils/FIFEUtil;->setImageUrlOptions(Ljava/lang/String;Ljava/lang/String;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method private getReviewerName(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    .locals 1
    .param p1, "review"    # Lcom/google/wireless/android/video/magma/proto/Review;

    .prologue
    .line 295
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    if-nez v0, :cond_0

    .line 296
    const/4 v0, 0x0

    .line 298
    :goto_0
    return-object v0

    :cond_0
    iget-object v0, p1, Lcom/google/wireless/android/video/magma/proto/Review;->author:Lcom/google/wireless/android/video/magma/proto/UserProfile;

    iget-object v0, v0, Lcom/google/wireless/android/video/magma/proto/UserProfile;->userName:Ljava/lang/String;

    goto :goto_0
.end method

.method private requestAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 8

    .prologue
    const/4 v4, 0x0

    .line 311
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v1

    .line 312
    .local v1, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/api/AssetsRequest;Lcom/google/wireless/android/video/magma/proto/AssetListResponse;>;"
    new-instance v5, Lcom/google/android/videos/api/AssetsRequest$Builder;

    invoke-direct {v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;-><init>()V

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->account:Ljava/lang/String;

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->account(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->configurationStore:Lcom/google/android/videos/store/ConfigurationStore;

    iget-object v7, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->account:Ljava/lang/String;

    invoke-virtual {v6, v7}, Lcom/google/android/videos/store/ConfigurationStore;->getPlayCountry(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v6

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->userCountry(Ljava/lang/String;)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v5

    const/16 v6, 0x1f

    invoke-virtual {v5, v6}, Lcom/google/android/videos/api/AssetsRequest$Builder;->addFlags(I)Lcom/google/android/videos/api/AssetsRequest$Builder;

    move-result-object v0

    .line 318
    .local v0, "builder":Lcom/google/android/videos/api/AssetsRequest$Builder;
    iget-object v5, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getAssetId(Ljava/lang/String;)Ljava/lang/String;

    move-result-object v5

    invoke-virtual {v0, v5}, Lcom/google/android/videos/api/AssetsRequest$Builder;->maybeAddId(Ljava/lang/String;)Z

    move-result v5

    if-nez v5, :cond_1

    .line 319
    new-instance v5, Ljava/lang/StringBuilder;

    invoke-direct {v5}, Ljava/lang/StringBuilder;-><init>()V

    const-string v6, "id too long: "

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->itemId:Ljava/lang/String;

    invoke-virtual {v5, v6}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v5

    invoke-virtual {v5}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v5

    invoke-static {v5}, Lcom/google/android/videos/L;->e(Ljava/lang/String;)V

    .line 335
    :cond_0
    :goto_0
    return-object v4

    .line 323
    :cond_1
    iget-object v5, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->assetsCachingRequester:Lcom/google/android/videos/async/Requester;

    invoke-virtual {v0}, Lcom/google/android/videos/api/AssetsRequest$Builder;->build()Lcom/google/android/videos/api/AssetsRequest;

    move-result-object v6

    invoke-interface {v5, v6, v1}, Lcom/google/android/videos/async/Requester;->request(Ljava/lang/Object;Lcom/google/android/videos/async/Callback;)V

    .line 326
    const-wide/16 v6, 0x3a98

    :try_start_0
    invoke-virtual {v1, v6, v7}, Lcom/google/android/videos/async/SyncCallback;->getResponse(J)Ljava/lang/Object;

    move-result-object v3

    check-cast v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;

    .line 327
    .local v3, "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    iget-object v5, v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    array-length v5, v5

    const/4 v6, 0x1

    if-ne v5, v6, :cond_0

    iget-object v5, v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v6, 0x0

    aget-object v5, v5, v6

    iget-object v5, v5, Lcom/google/wireless/android/video/magma/proto/AssetResource;->metadata:Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;

    if-eqz v5, :cond_0

    .line 328
    iget-object v5, v3, Lcom/google/wireless/android/video/magma/proto/AssetListResponse;->resource:[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    const/4 v6, 0x0

    aget-object v4, v5, v6
    :try_end_0
    .catch Ljava/util/concurrent/TimeoutException; {:try_start_0 .. :try_end_0} :catch_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_1

    goto :goto_0

    .line 330
    .end local v3    # "response":Lcom/google/wireless/android/video/magma/proto/AssetListResponse;
    :catch_0
    move-exception v2

    .line 331
    .local v2, "e":Ljava/util/concurrent/TimeoutException;
    const-string v5, "timeout"

    invoke-static {v5}, Lcom/google/android/videos/L;->w(Ljava/lang/String;)V

    goto :goto_0

    .line 332
    .end local v2    # "e":Ljava/util/concurrent/TimeoutException;
    :catch_1
    move-exception v2

    .line 333
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    const-string v5, "Failed to fetch purchases"

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v6

    invoke-static {v5, v6}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    goto :goto_0
.end method


# virtual methods
.method protected addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "intent"    # Landroid/content/Intent;

    .prologue
    .line 215
    const/4 v7, 0x0

    move-object v1, p0

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    invoke-virtual/range {v1 .. v7}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    .line 216
    return-void
.end method

.method protected addAction(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V
    .locals 9
    .param p1, "id"    # J
    .param p3, "title"    # Ljava/lang/String;
    .param p4, "subtitle"    # Ljava/lang/String;
    .param p5, "intent"    # Landroid/content/Intent;
    .param p6, "requestCode"    # I

    .prologue
    .line 219
    new-instance v1, Lcom/google/android/videos/pano/ui/ClickableAction;

    move-wide v2, p1

    move-object v4, p3

    move-object v5, p4

    move-object v6, p5

    move v7, p6

    invoke-direct/range {v1 .. v7}, Lcom/google/android/videos/pano/ui/ClickableAction;-><init>(JLjava/lang/String;Ljava/lang/String;Landroid/content/Intent;I)V

    invoke-virtual {p0, v1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    .line 220
    return-void
.end method

.method protected addAction(Landroid/support/v17/leanback/widget/Action;)V
    .locals 1
    .param p1, "action"    # Landroid/support/v17/leanback/widget/Action;

    .prologue
    .line 223
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->actions:Ljava/util/ArrayList;

    invoke-virtual {v0, p1}, Ljava/util/ArrayList;->add(Ljava/lang/Object;)Z

    .line 224
    return-void
.end method

.method public abstract addDetailsSectionActions()V
.end method

.method protected addExtra(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;)V
    .locals 0
    .param p1, "overview"    # Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;

    .prologue
    .line 133
    return-void
.end method

.method public getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;
    .locals 1

    .prologue
    .line 208
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    if-nez v0, :cond_0

    .line 209
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->requestAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    iput-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 211
    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->assetResource:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    return-object v0
.end method

.method protected abstract getAssetId(Ljava/lang/String;)Ljava/lang/String;
.end method

.method public getDetailsOverviewRow(Z)Landroid/support/v17/leanback/widget/DetailsOverviewRow;
    .locals 8
    .param p1, "includeActions"    # Z

    .prologue
    .line 143
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v1

    .line 144
    .local v1, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v1, :cond_1

    .line 145
    const/4 v3, 0x0

    .line 166
    :cond_0
    return-object v3

    .line 147
    :cond_1
    new-instance v5, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;

    invoke-direct {v5}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;-><init>()V

    .line 148
    .local v5, "overview":Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;
    iput-object v1, v5, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;->asset:Lcom/google/wireless/android/video/magma/proto/AssetResource;

    .line 149
    invoke-virtual {p0, v5}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->addExtra(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$AssetOverview;)V

    .line 150
    new-instance v3, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;

    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-direct {v3, v6, v5}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;-><init>(Landroid/content/Context;Ljava/lang/Object;)V

    .line 152
    .local v3, "imageLoadingDetailsOverviewRow":Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v1, v6}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetPoster(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v4

    .line 153
    .local v4, "imageUri":Ljava/lang/String;
    if-eqz v4, :cond_2

    .line 154
    invoke-virtual {v3, v4}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setImageUri(Ljava/lang/String;)V

    .line 159
    :goto_0
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->actions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->clear()V

    .line 160
    if-eqz p1, :cond_0

    .line 161
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->addDetailsSectionActions()V

    .line 162
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->actions:Ljava/util/ArrayList;

    invoke-virtual {v6}, Ljava/util/ArrayList;->iterator()Ljava/util/Iterator;

    move-result-object v2

    .local v2, "i$":Ljava/util/Iterator;
    :goto_1
    invoke-interface {v2}, Ljava/util/Iterator;->hasNext()Z

    move-result v6

    if-eqz v6, :cond_0

    invoke-interface {v2}, Ljava/util/Iterator;->next()Ljava/lang/Object;

    move-result-object v0

    check-cast v0, Landroid/support/v17/leanback/widget/Action;

    .line 163
    .local v0, "action":Landroid/support/v17/leanback/widget/Action;
    invoke-virtual {v3, v0}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->addAction(Landroid/support/v17/leanback/widget/Action;)V

    goto :goto_1

    .line 156
    .end local v0    # "action":Landroid/support/v17/leanback/widget/Action;
    .end local v2    # "i$":Ljava/util/Iterator;
    :cond_2
    iget-object v6, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-virtual {v6}, Landroid/content/Context;->getResources()Landroid/content/res/Resources;

    move-result-object v6

    const v7, 0x7f02017e

    invoke-virtual {v6, v7}, Landroid/content/res/Resources;->getDrawable(I)Landroid/graphics/drawable/Drawable;

    move-result-object v6

    invoke-virtual {v3, v6}, Lcom/google/android/videos/pano/ui/ImageLoadingDetailsOverviewRow;->setImageDrawable(Landroid/graphics/drawable/Drawable;)V

    goto :goto_0
.end method

.method protected getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFF)Ljava/lang/String;
    .locals 6
    .param p1, "metadata"    # Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;
    .param p2, "imageType"    # I
    .param p3, "desiredWidthResourceId"    # I
    .param p4, "aspectRatio"    # F
    .param p5, "minScale"    # F

    .prologue
    .line 228
    iget-object v5, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    move-object v0, p1

    move v1, p2

    move v2, p3

    move v3, p4

    move v4, p5

    invoke-static/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/PanoHelper;->getImageUri(Lcom/google/wireless/android/video/magma/proto/AssetResource$Metadata;IIFFLandroid/content/Context;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getRelatedListRow(II)Landroid/support/v17/leanback/widget/ListRow;
    .locals 10
    .param p1, "posterWidth"    # I
    .param p2, "posterHeight"    # I

    .prologue
    const/4 v5, 0x0

    .line 170
    invoke-direct {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getRelated()[Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v9

    .line 171
    .local v9, "related":[Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-eqz v9, :cond_0

    array-length v0, v9

    if-nez v0, :cond_1

    .line 185
    :cond_0
    :goto_0
    return-object v5

    .line 176
    :cond_1
    new-instance v8, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;

    new-instance v0, Lcom/google/android/videos/pano/ui/ItemPresenter;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->bitmapRequester:Lcom/google/android/videos/async/Requester;

    invoke-direct {v0, v1}, Lcom/google/android/videos/pano/ui/ItemPresenter;-><init>(Lcom/google/android/videos/async/Requester;)V

    invoke-direct {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;-><init>(Landroid/support/v17/leanback/widget/Presenter;)V

    .line 179
    .local v8, "itemArrayObjectAdapter":Landroid/support/v17/leanback/widget/ArrayObjectAdapter;
    const/4 v7, 0x0

    .local v7, "i":I
    :goto_1
    array-length v0, v9

    if-ge v7, v0, :cond_2

    .line 180
    new-instance v0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;

    aget-object v2, v9, v7

    move-object v1, p0

    move v3, p2

    move v4, p1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$RelatedItem;-><init>(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/AssetResource;IILcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;)V

    invoke-virtual {v8, v0}, Landroid/support/v17/leanback/widget/ArrayObjectAdapter;->add(Ljava/lang/Object;)V

    .line 179
    add-int/lit8 v7, v7, 0x1

    goto :goto_1

    .line 183
    :cond_2
    new-instance v6, Landroid/support/v17/leanback/widget/HeaderItem;

    const-wide/16 v0, 0x1

    iget-object v2, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    const v3, 0x7f0b00a5

    invoke-virtual {v2, v3}, Landroid/content/Context;->getString(I)Ljava/lang/String;

    move-result-object v2

    invoke-direct {v6, v0, v1, v2, v5}, Landroid/support/v17/leanback/widget/HeaderItem;-><init>(JLjava/lang/String;Ljava/lang/String;)V

    .line 185
    .local v6, "headerItem":Landroid/support/v17/leanback/widget/HeaderItem;
    new-instance v5, Landroid/support/v17/leanback/widget/ListRow;

    invoke-direct {v5, v6, v8}, Landroid/support/v17/leanback/widget/ListRow;-><init>(Landroid/support/v17/leanback/widget/HeaderItem;Landroid/support/v17/leanback/widget/ObjectAdapter;)V

    goto :goto_0
.end method

.method protected abstract getRelatedRequest()Lcom/google/android/videos/api/RecommendationsRequest;
.end method

.method public getWallpaperUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 138
    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getAsset()Lcom/google/wireless/android/video/magma/proto/AssetResource;

    move-result-object v0

    .line 139
    .local v0, "asset":Lcom/google/wireless/android/video/magma/proto/AssetResource;
    if-nez v0, :cond_0

    const/4 v1, 0x0

    :goto_0
    return-object v1

    :cond_0
    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->context:Landroid/content/Context;

    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/PanoHelper;->getAssetWallpaper(Lcom/google/wireless/android/video/magma/proto/AssetResource;Landroid/content/Context;)Ljava/lang/String;

    move-result-object v1

    goto :goto_0
.end method

.method protected abstract getWishlistRequest()Lcom/google/android/videos/store/WishlistStore$WishlistRequest;
.end method

.method protected isAddedToWishlist()Z
    .locals 5

    .prologue
    .line 386
    invoke-static {}, Lcom/google/android/videos/async/SyncCallback;->create()Lcom/google/android/videos/async/SyncCallback;

    move-result-object v0

    .line 387
    .local v0, "callback":Lcom/google/android/videos/async/SyncCallback;, "Lcom/google/android/videos/async/SyncCallback<Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Landroid/database/Cursor;>;"
    iget-object v3, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->wishlistStore:Lcom/google/android/videos/store/WishlistStore;

    invoke-virtual {p0}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getWishlistRequest()Lcom/google/android/videos/store/WishlistStore$WishlistRequest;

    move-result-object v4

    invoke-virtual {v3, v4, v0}, Lcom/google/android/videos/store/WishlistStore;->loadWishlist(Lcom/google/android/videos/store/WishlistStore$WishlistRequest;Lcom/google/android/videos/async/Callback;)V

    .line 389
    :try_start_0
    invoke-virtual {v0}, Lcom/google/android/videos/async/SyncCallback;->getResponse()Ljava/lang/Object;

    move-result-object v1

    check-cast v1, Landroid/database/Cursor;
    :try_end_0
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_0 .. :try_end_0} :catch_0

    .line 391
    .local v1, "cursor":Landroid/database/Cursor;
    :try_start_1
    invoke-interface {v1}, Landroid/database/Cursor;->moveToFirst()Z
    :try_end_1
    .catchall {:try_start_1 .. :try_end_1} :catchall_0

    move-result v3

    .line 393
    :try_start_2
    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    .line 398
    .end local v1    # "cursor":Landroid/database/Cursor;
    :goto_0
    return v3

    .line 393
    .restart local v1    # "cursor":Landroid/database/Cursor;
    :catchall_0
    move-exception v3

    invoke-interface {v1}, Landroid/database/Cursor;->close()V

    throw v3
    :try_end_2
    .catch Ljava/util/concurrent/ExecutionException; {:try_start_2 .. :try_end_2} :catch_0

    .line 395
    .end local v1    # "cursor":Landroid/database/Cursor;
    :catch_0
    move-exception v2

    .line 396
    .local v2, "e":Ljava/util/concurrent/ExecutionException;
    const-string v3, "Failed to fetch wishlist state"

    invoke-virtual {v2}, Ljava/util/concurrent/ExecutionException;->getCause()Ljava/lang/Throwable;

    move-result-object v4

    invoke-static {v3, v4}, Lcom/google/android/videos/L;->w(Ljava/lang/String;Ljava/lang/Throwable;)V

    .line 398
    const/4 v3, 0x0

    goto :goto_0
.end method

.method public onAdapterUpdateStart()V
    .locals 0

    .prologue
    .line 127
    return-void
.end method

.method public abstract onPurchasesUpdated()V
.end method

.method public abstract onWatchTimestampsUpdated(Ljava/lang/String;)V
.end method
