.class final Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelViewHolder;
.super Landroid/support/v7/widget/RecyclerView$ViewHolder;
.source "MovieDetailsFlowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/ui/MovieDetailsFlowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x12
    name = "InfoPanelViewHolder"
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;


# direct methods
.method public constructor <init>(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;Landroid/view/View;)V
    .locals 3
    .param p2, "itemView"    # Landroid/view/View;

    .prologue
    .line 348
    iput-object p1, p0, Lcom/google/android/videos/ui/MovieDetailsFlowHelper$InfoPanelViewHolder;->this$0:Lcom/google/android/videos/ui/MovieDetailsFlowHelper;

    .line 349
    invoke-direct {p0, p2}, Landroid/support/v7/widget/RecyclerView$ViewHolder;-><init>(Landroid/view/View;)V

    .line 350
    sget v0, Lcom/google/android/videos/utils/Util;->SDK_INT:I

    const/16 v1, 0x15

    if-lt v0, v1, :cond_0

    .line 351
    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->activity:Lcom/google/android/videos/activity/MovieDetailsActivity;
    invoke-static {p1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$100(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Lcom/google/android/videos/activity/MovieDetailsActivity;

    move-result-object v0

    const v1, 0x7f0b0075

    # getter for: Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->videoId:Ljava/lang/String;
    invoke-static {p1}, Lcom/google/android/videos/ui/MovieDetailsFlowHelper;->access$200(Lcom/google/android/videos/ui/MovieDetailsFlowHelper;)Ljava/lang/String;

    move-result-object v2

    invoke-static {v0, v1, v2}, Lcom/google/android/videos/ui/TransitionUtil;->encodeTransitionName(Landroid/content/Context;ILjava/lang/String;)Ljava/lang/String;

    move-result-object v0

    invoke-virtual {p2, v0}, Landroid/view/View;->setTransitionName(Ljava/lang/String;)V

    .line 354
    :cond_0
    return-void
.end method
