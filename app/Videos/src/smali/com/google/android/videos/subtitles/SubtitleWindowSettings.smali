.class public final Lcom/google/android/videos/subtitles/SubtitleWindowSettings;
.super Ljava/lang/Object;
.source "SubtitleWindowSettings.java"

# interfaces
.implements Landroid/os/Parcelable;
.implements Ljava/io/Serializable;


# annotations
.annotation system Ldalvik/annotation/MemberClasses;
    value = {
        Lcom/google/android/videos/subtitles/SubtitleWindowSettings$1;,
        Lcom/google/android/videos/subtitles/SubtitleWindowSettings$ParcelableCreator;
    }
.end annotation


# static fields
.field public static final CREATOR:Landroid/os/Parcelable$Creator;
    .annotation system Ldalvik/annotation/Signature;
        value = {
            "Landroid/os/Parcelable$Creator",
            "<",
            "Lcom/google/android/videos/subtitles/SubtitleWindowSettings;",
            ">;"
        }
    .end annotation
.end field

.field public static final DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

.field public static final serialVersionUID:J = -0x7301346ab4ca84eL


# instance fields
.field public final anchorHorizontalPos:I

.field public final anchorPoint:I

.field public final anchorVerticalPos:I

.field private final scrollDirection:Ljava/lang/Integer;

.field public final visible:Z


# direct methods
.method static constructor <clinit>()V
    .locals 6

    .prologue
    .line 40
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings$ParcelableCreator;

    const/4 v1, 0x0

    invoke-direct {v0, v1}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings$ParcelableCreator;-><init>(Lcom/google/android/videos/subtitles/SubtitleWindowSettings$1;)V

    sput-object v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->CREATOR:Landroid/os/Parcelable$Creator;

    .line 42
    new-instance v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    const/16 v1, 0x22

    const/16 v2, 0x32

    const/16 v3, 0x5f

    const/4 v4, 0x1

    const/4 v5, -0x1

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;-><init>(IIIZI)V

    sput-object v0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->DEFAULT_SUBTITLE_WINDOW_SETTINGS:Lcom/google/android/videos/subtitles/SubtitleWindowSettings;

    return-void
.end method

.method public constructor <init>(IIIZI)V
    .locals 6
    .param p1, "anchorPoint"    # I
    .param p2, "anchorHorizontalPos"    # I
    .param p3, "anchorVerticalPos"    # I
    .param p4, "visible"    # Z
    .param p5, "scrollDirection"    # I

    .prologue
    const/16 v5, 0x64

    const/4 v1, 0x1

    const/4 v2, 0x0

    .line 81
    invoke-direct {p0}, Ljava/lang/Object;-><init>()V

    .line 82
    if-ltz p2, :cond_0

    if-gt p2, v5, :cond_0

    move v0, v1

    :goto_0
    new-instance v3, Ljava/lang/StringBuilder;

    invoke-direct {v3}, Ljava/lang/StringBuilder;-><init>()V

    const-string v4, "invalid anchorHorizontalPos: "

    invoke-virtual {v3, v4}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3, p2}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v3

    invoke-virtual {v3}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v3

    invoke-static {v0, v3}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 85
    if-ltz p3, :cond_1

    if-gt p3, v5, :cond_1

    :goto_1
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v2, "invalid anchorVerticalPos: "

    invoke-virtual {v0, v2}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0, p3}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    invoke-static {v1, v0}, Lcom/google/android/videos/utils/Preconditions;->checkArgument(ZLjava/lang/String;)V

    .line 88
    iput p1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorPoint:I

    .line 89
    iput p2, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorHorizontalPos:I

    .line 90
    iput p3, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorVerticalPos:I

    .line 91
    iput-boolean p4, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->visible:Z

    .line 92
    const/4 v0, -0x1

    if-ne p5, v0, :cond_2

    const/4 v0, 0x0

    :goto_2
    iput-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->scrollDirection:Ljava/lang/Integer;

    .line 94
    return-void

    :cond_0
    move v0, v2

    .line 82
    goto :goto_0

    :cond_1
    move v1, v2

    .line 85
    goto :goto_1

    .line 92
    :cond_2
    const/4 v0, 0x3

    invoke-static {v0}, Ljava/lang/Integer;->valueOf(I)Ljava/lang/Integer;

    move-result-object v0

    goto :goto_2
.end method

.method private constructor <init>(Landroid/os/Parcel;)V
    .locals 6
    .param p1, "parcel"    # Landroid/os/Parcel;

    .prologue
    .line 97
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v1

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v2

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v3

    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v0

    if-eqz v0, :cond_0

    const/4 v4, 0x1

    :goto_0
    invoke-virtual {p1}, Landroid/os/Parcel;->readInt()I

    move-result v5

    move-object v0, p0

    invoke-direct/range {v0 .. v5}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;-><init>(IIIZI)V

    .line 99
    return-void

    .line 97
    :cond_0
    const/4 v4, 0x0

    goto :goto_0
.end method

.method synthetic constructor <init>(Landroid/os/Parcel;Lcom/google/android/videos/subtitles/SubtitleWindowSettings$1;)V
    .locals 0
    .param p1, "x0"    # Landroid/os/Parcel;
    .param p2, "x1"    # Lcom/google/android/videos/subtitles/SubtitleWindowSettings$1;

    .prologue
    .line 15
    invoke-direct {p0, p1}, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;-><init>(Landroid/os/Parcel;)V

    return-void
.end method


# virtual methods
.method public describeContents()I
    .locals 1

    .prologue
    .line 118
    const/4 v0, 0x0

    return v0
.end method

.method public getScrollDirection()I
    .locals 1

    .prologue
    .line 122
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->scrollDirection:Ljava/lang/Integer;

    if-nez v0, :cond_0

    const/4 v0, -0x1

    :goto_0
    return v0

    :cond_0
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->scrollDirection:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_0
.end method

.method public toString()Ljava/lang/String;
    .locals 2

    .prologue
    .line 112
    new-instance v0, Ljava/lang/StringBuilder;

    invoke-direct {v0}, Ljava/lang/StringBuilder;-><init>()V

    const-string v1, "ap="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorPoint:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", ah="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorHorizontalPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", av="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorVerticalPos:I

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(I)Ljava/lang/StringBuilder;

    move-result-object v0

    const-string v1, ", vs="

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Ljava/lang/String;)Ljava/lang/StringBuilder;

    move-result-object v0

    iget-boolean v1, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->visible:Z

    invoke-virtual {v0, v1}, Ljava/lang/StringBuilder;->append(Z)Ljava/lang/StringBuilder;

    move-result-object v0

    invoke-virtual {v0}, Ljava/lang/StringBuilder;->toString()Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public writeToParcel(Landroid/os/Parcel;I)V
    .locals 1
    .param p1, "dest"    # Landroid/os/Parcel;
    .param p2, "flags"    # I

    .prologue
    .line 103
    iget v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorPoint:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 104
    iget v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorHorizontalPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 105
    iget v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->anchorVerticalPos:I

    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 106
    iget-boolean v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->visible:Z

    if-eqz v0, :cond_0

    const/4 v0, 0x1

    :goto_0
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 107
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->scrollDirection:Ljava/lang/Integer;

    if-nez v0, :cond_1

    const/4 v0, -0x1

    :goto_1
    invoke-virtual {p1, v0}, Landroid/os/Parcel;->writeInt(I)V

    .line 108
    return-void

    .line 106
    :cond_0
    const/4 v0, 0x0

    goto :goto_0

    .line 107
    :cond_1
    iget-object v0, p0, Lcom/google/android/videos/subtitles/SubtitleWindowSettings;->scrollDirection:Ljava/lang/Integer;

    invoke-virtual {v0}, Ljava/lang/Integer;->intValue()I

    move-result v0

    goto :goto_1
.end method
