.class Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;
.super Lcom/google/android/videos/pano/model/Item;
.source "BaseDetailsRowHelper.java"


# annotations
.annotation system Ldalvik/annotation/EnclosingClass;
    value = Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;
.end annotation

.annotation system Ldalvik/annotation/InnerClass;
    accessFlags = 0x0
    name = null
.end annotation


# instance fields
.field final synthetic this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

.field final synthetic val$posterSize:I

.field final synthetic val$review:Lcom/google/wireless/android/video/magma/proto/Review;


# virtual methods
.method public getImageHeight()I
    .locals 1

    .prologue
    .line 264
    iget v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->val$posterSize:I

    return v0
.end method

.method public getImageUri()Ljava/lang/String;
    .locals 2

    .prologue
    .line 254
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->val$review:Lcom/google/wireless/android/video/magma/proto/Review;

    # invokes: Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getReviewerImage(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->access$200(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public getImageWidth()I
    .locals 1

    .prologue
    .line 259
    iget v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->val$posterSize:I

    return v0
.end method

.method public getStarRating()F
    .locals 1

    .prologue
    .line 269
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->val$review:Lcom/google/wireless/android/video/magma/proto/Review;

    iget v0, v0, Lcom/google/wireless/android/video/magma/proto/Review;->starRating:I

    int-to-float v0, v0

    return v0
.end method

.method public getTitle()Ljava/lang/String;
    .locals 2

    .prologue
    .line 249
    iget-object v0, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->this$0:Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;

    iget-object v1, p0, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper$1;->val$review:Lcom/google/wireless/android/video/magma/proto/Review;

    # invokes: Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->getReviewerName(Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;
    invoke-static {v0, v1}, Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;->access$100(Lcom/google/android/videos/pano/ui/BaseDetailsRowHelper;Lcom/google/wireless/android/video/magma/proto/Review;)Ljava/lang/String;

    move-result-object v0

    return-object v0
.end method

.method public onClick(Landroid/app/Activity;Landroid/support/v17/leanback/widget/Presenter$ViewHolder;)V
    .locals 0
    .param p1, "activity"    # Landroid/app/Activity;
    .param p2, "viewHolder"    # Landroid/support/v17/leanback/widget/Presenter$ViewHolder;

    .prologue
    .line 275
    return-void
.end method
